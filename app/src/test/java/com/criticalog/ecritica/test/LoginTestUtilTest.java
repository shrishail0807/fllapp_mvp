package com.criticalog.ecritica.test;


import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class LoginTestUtilTest {

    LoginTestUtil loginTestUtil = new LoginTestUtil();

    @Test
    public void empty_userid_return_false() {

        boolean result = loginTestUtil.validateregistration("","CLPL0108");


        assertThat(result).isFalse();
    }

    @Test
    public void validinputreturntrue() {
        boolean result = loginTestUtil.validateregistration("10108","CLPL0108");
        assertThat(result).isTrue();
    }

}