package com.criticalog.ecritica.MVPAttendance;

import com.criticalog.ecritica.MVPAttendance.Model.PunchInRequest;
import com.criticalog.ecritica.MVPAttendance.Model.PunchInResponse;

public class PunchInInteractorImpl implements PunchinContract.presenter, PunchinContract.GetPunchIntractor.OnFinishedListener {
    private PunchinContract.MainView mainView;
    private PunchinContract.GetPunchIntractor getPunchIntractor;

    public PunchInInteractorImpl(PunchinContract.MainView mainView,PunchinContract.GetPunchIntractor getPunchIntractor)
    {
        this.mainView=mainView;
        this.getPunchIntractor=getPunchIntractor;
    }


    @Override
    public void onFinished(PunchInResponse punchInResponse) {
        if(mainView != null){
            mainView.setDataToViews(punchInResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPunchButtonClick(PunchInRequest punchInRequest) {
        if(mainView != null){
            mainView.showProgress();
        }
        getPunchIntractor.punchSuccessful(this,punchInRequest);
    }
}
