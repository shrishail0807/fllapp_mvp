package com.criticalog.ecritica.MVPAttendance.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DatumPunch implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("lat_in")
    @Expose
    private String latIn;
    @SerializedName("lng_in")
    @Expose
    private String lngIn;
    @SerializedName("address_in")
    @Expose
    private String addressIn;
    @SerializedName("date_punch_in")
    @Expose
    private String datePunchIn;
    @SerializedName("lat_out")
    @Expose
    private String latOut;
    @SerializedName("lng_out")
    @Expose
    private String lngOut;
    @SerializedName("address_out")
    @Expose
    private String addressOut;
    @SerializedName("date_punch_out")
    @Expose
    private String datePunchOut;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatIn() {
        return latIn;
    }

    public void setLatIn(String latIn) {
        this.latIn = latIn;
    }

    public String getLngIn() {
        return lngIn;
    }

    public void setLngIn(String lngIn) {
        this.lngIn = lngIn;
    }

    public String getAddressIn() {
        return addressIn;
    }

    public void setAddressIn(String addressIn) {
        this.addressIn = addressIn;
    }

    public String getDatePunchIn() {
        return datePunchIn;
    }

    public void setDatePunchIn(String datePunchIn) {
        this.datePunchIn = datePunchIn;
    }

    public String getLatOut() {
        return latOut;
    }

    public void setLatOut(String latOut) {
        this.latOut = latOut;
    }

    public String getLngOut() {
        return lngOut;
    }

    public void setLngOut(String lngOut) {
        this.lngOut = lngOut;
    }

    public String getAddressOut() {
        return addressOut;
    }

    public void setAddressOut(String addressOut) {
        this.addressOut = addressOut;
    }

    public String getDatePunchOut() {
        return datePunchOut;
    }

    public void setDatePunchOut(String datePunchOut) {
        this.datePunchOut = datePunchOut;
    }
}
