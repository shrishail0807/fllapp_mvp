package com.criticalog.ecritica.MVPAttendance;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.criticalog.ecritica.MVPAttendance.Model.PunchInRequest;
import com.criticalog.ecritica.MVPAttendance.Model.PunchInResponse;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPLogin.GetLoginInteractorImpl;
import com.criticalog.ecritica.MVPTATCheck.TATCheckActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class AttendanceActivity extends AppCompatActivity implements View.OnClickListener, PunchinContract.MainView {
    private TextView mLastPunchIn, mDuration, mName, mUserId, mAddressLocPuchOut, mLastPunchOut;
    private ImageView mPunchIn;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0.0;
    private double currentLongitude = 0.0;
    private TextView mAddressLoc, mTopBarText;
    private long mLastClickTime = 0;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private PunchinContract.presenter mPresenter;
    private SimpleArcDialog mProgressDialog1;
    private String userId, name;
    private ImageView mTvBackbutton;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private LocationRequest locationRequest;
    private String address;
    LocationManager manager;
    private static final int REQUEST_LOCATION = 1;

    private FusedLocationProviderClient mfusedLocationproviderClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private LocationCallback locationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_mvp);
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        checkGpsEnabledOrNot();
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }


        getLocation();

        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        userId = mCriticalogSharedPreferences.getData("userId");
        name = mCriticalogSharedPreferences.getData("username");

        mPunchIn = findViewById(R.id.mPunchIn);
        mAddressLoc = findViewById(R.id.mAddressLoc);
        mTopBarText = findViewById(R.id.mTopBarText);
        mLastPunchIn = findViewById(R.id.mLastPunchIn);
        mDuration = findViewById(R.id.mDuration);
        mName = findViewById(R.id.mName);
        mUserId = findViewById(R.id.mUserId);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mAddressLocPuchOut = findViewById(R.id.mAddressLocPuchOut);
        mLastPunchOut = findViewById(R.id.mLastPunchOut);
        if (mCriticalogSharedPreferences.getData("punch_in_out").equalsIgnoreCase("")) {
            calculateHoursPunchOut();
        } else {
            calculateHours();
        }
        mTopBarText.setText("Punch-In and Punch-Out");

        mPunchIn.setOnClickListener(this);

        Date c1 = Calendar.getInstance().getTime();
        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = df3.format(c1);

        SimpleDateFormat df4 = new SimpleDateFormat("HH:mm aa");
        String time1 = df4.format(c1);

        mCriticalogSharedPreferences.saveData("current_time", time1);

        mName.setText(name);
        mUserId.setText(userId);

        String addr = mCriticalogSharedPreferences.getData("last_address");
        String dataTime = mCriticalogSharedPreferences.getData("punchin_time");
        String addr1 = mCriticalogSharedPreferences.getData("last_address1");
        String dataTime1 = mCriticalogSharedPreferences.getData("punchout_time");

        if (!addr.equalsIgnoreCase("")) {
            mAddressLoc.setVisibility(View.VISIBLE);
        }
        if (!addr1.equalsIgnoreCase("")) {
            mAddressLocPuchOut.setVisibility(View.VISIBLE);
        }
        mAddressLocPuchOut.setText(addr1);
        mLastPunchOut.setText("Punch-Out: " + dataTime1);
        mAddressLoc.setText(addr);
        mLastPunchIn.setText("Punch-In: " + dataTime);
        if (mCriticalogSharedPreferences.getData("punch_in_out").equalsIgnoreCase("")) {
            mAddressLocPuchOut.setText(addr1);
            mLastPunchOut.setText("Punch-Out: " + dataTime1);
            mAddressLoc.setText(addr);
            mLastPunchIn.setText("Punch-In: " + dataTime);
        } else {

            mAddressLoc.setText(addr);
            mLastPunchIn.setText("Punch-In: " + dataTime);
        }


        mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(AttendanceActivity.this, HomeActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);

                finish();
            }
        });
    }

    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(AttendanceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(AttendanceActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AttendanceActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(AttendanceActivity.this, location -> {
                if (location != null) {
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, Enable It!!!")
                .setCancelable(false)
                .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        //dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE:


                if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        getLocation();
                    } else {
                        showLocationDialog("");
                    }
                }
                break;
        }
    }

    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        requestPermission();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(AttendanceActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                //   .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }


    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }

    private double meterDistanceBetweenPoints(float lat_a, float lng_a, float lat_b, float lng_b) {
        float pk = (float) (180.f / Math.PI);

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mPunchIn:
                //   getCurrentLocation();
                getLocation();

                Date c1 = Calendar.getInstance().getTime();
                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
                String date1 = df3.format(c1);

                SimpleDateFormat df4 = new SimpleDateFormat("HH:mm aa");
                String time1 = df4.format(c1);

                mCriticalogSharedPreferences.saveData("last_time", time1);

                Geocoder geocoder;
                List<Address> addresses = new ArrayList<>();
                geocoder = new Geocoder(this, Locale.getDefault());

                try {

                    try {

                        if (currentLatitude == 0.0 || currentLatitude == 0.0) {
                            addresses = geocoder.getFromLocation(currentLatitude, currentLatitude, 1);
                        } else {
                            addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);
                        }
                        address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                        if (!address.equals("")) {
                            if (mCriticalogSharedPreferences.getData("punch_in_out").equalsIgnoreCase("")) {
                                mAddressLoc.setText(address);
                                mLastPunchIn.setText("Punch-In: " + date1 + " : " + time1);
                                mCriticalogSharedPreferences.saveData("last_address", address);
                                mCriticalogSharedPreferences.saveData("last_date_time", date1 + " : " + time1);
                                mAddressLocPuchOut.setText("");
                                mLastPunchOut.setText("");
                            } else {
                                mAddressLocPuchOut.setText(address);
                                mLastPunchOut.setText("Punch-Out: " + date1 + " : " + time1);
                                mCriticalogSharedPreferences.saveData("last_address1", address);
                                mCriticalogSharedPreferences.saveData("last_date_time1", date1 + " : " + time1);
                            }
                        }

                        PunchInRequest punchInRequest = new PunchInRequest();
                        punchInRequest.setAction("punch_in_out");
                        if (currentLatitude == 0.0 || currentLongitude == 0.0) {
                            punchInRequest.setLat(String.valueOf(currentLatitude));
                            punchInRequest.setLng(String.valueOf(currentLongitude));
                        } else {
                            punchInRequest.setLat(String.valueOf(currentLatitude));
                            punchInRequest.setLng(String.valueOf(currentLongitude));
                        }
                        punchInRequest.setAddress(address);
                        punchInRequest.setPunchId("");
                        punchInRequest.setUserId(userId);
                        mPresenter = new PunchInInteractorImpl(this, new PunchInPresenterImpl());
                        mPresenter.onPunchButtonClick(punchInRequest);
                    } catch (Exception e) {
                        PunchInRequest punchInRequest = new PunchInRequest();
                        punchInRequest.setAction("punch_in_out");
                        if (currentLatitude == 0.0 || currentLongitude == 0.0) {
                            punchInRequest.setLat(String.valueOf(currentLatitude));
                            punchInRequest.setLng(String.valueOf(currentLongitude));
                        } else {
                            punchInRequest.setLat(String.valueOf(currentLatitude));
                            punchInRequest.setLng(String.valueOf(currentLongitude));
                        }
                        punchInRequest.setAddress(String.valueOf(currentLatitude) + ":" + String.valueOf(currentLongitude));
                        punchInRequest.setPunchId("");
                        punchInRequest.setUserId(userId);
                        mPresenter = new PunchInInteractorImpl(this, new PunchInPresenterImpl());
                        mPresenter.onPunchButtonClick(punchInRequest);

                        if (checkPermission()) {

                            getLocation();

                        } else {
                            requestPermission();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toasty.warning(this, "Location not captured click punch again!!", Toast.LENGTH_LONG, true).show();
                    finish();
                    startActivity(getIntent());
                    if (checkPermission()) {

                        getLocation();

                    } else {
                        requestPermission();
                    }
                }

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API

        if (mCriticalogSharedPreferences.getData("punch_in_out").equalsIgnoreCase("")) {
            calculateHoursPunchOut();
        } else {
            calculateHours();
        }

        getLocation();

    }

    @Override
    protected void onPause() {
        super.onPause();

        getLocation();

    }

    /**
     * If connected get lat and long
     */


    public void calculateHours() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        int days, hours, min;
        try {
            Date date1 = simpleDateFormat.parse(mCriticalogSharedPreferences.getData("last_time"));
            Date date2 = simpleDateFormat.parse(mCriticalogSharedPreferences.getData("current_time"));

            long difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= Hours", " :: " + hours);
            mDuration.setText("Duration completed: " + hours + " Hour" + ": " + min + " Min");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void calculateHoursPunchOut() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        int days, hours, min;
        try {
            Date date1 = simpleDateFormat.parse(mCriticalogSharedPreferences.getData("punchin_time"));
            Date date2 = simpleDateFormat.parse(mCriticalogSharedPreferences.getData("punchout_time"));

            long difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= Hours", " :: " + hours);
            mDuration.setText("Duration completed: " + hours + " Hour" + ": " + min + " Min");
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showProgress() {
        mProgressDialog1.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog1.dismiss();
    }

    @Override
    public void setDataToViews(PunchInResponse punchInResponse) {

        if (punchInResponse.getStatus() == 200) {
            Toasty.success(AttendanceActivity.this, punchInResponse.getMessage(), Toast.LENGTH_SHORT, true).show();

            if (mCriticalogSharedPreferences.getData("punch_in_out").equalsIgnoreCase("")) {
                mCriticalogSharedPreferences.saveData("punch_in_out", "true");

                calculateHours();
            } else {
                mCriticalogSharedPreferences.saveData("punch_in_out", "");
                calculateHoursPunchOut();
            }

            if (punchInResponse.getData().get(0).getDatePunchOut() != null) {
                mAddressLocPuchOut.setVisibility(View.VISIBLE);
                mAddressLocPuchOut.setText(punchInResponse.getData().get(0).getAddressOut());
                mLastPunchOut.setText("Punch-Out: " + punchInResponse.getData().get(0).getDatePunchOut());
            } else {
                mLastPunchOut.setText("");
            }
            if (punchInResponse.getData().get(0).getDatePunchIn() != null) {

                mAddressLoc.setVisibility(View.VISIBLE);
                mAddressLoc.setText(punchInResponse.getData().get(0).getAddressIn());
                mLastPunchIn.setText("Punch-In: " + punchInResponse.getData().get(0).getDatePunchIn());
            }
        } else {
            Toasty.warning(AttendanceActivity.this, punchInResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent newIntent = new Intent(AttendanceActivity.this, HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
        finish();
    }
}

