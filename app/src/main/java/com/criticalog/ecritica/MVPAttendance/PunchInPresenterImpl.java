package com.criticalog.ecritica.MVPAttendance;

import com.criticalog.ecritica.MVPAttendance.Model.PunchInRequest;
import com.criticalog.ecritica.MVPAttendance.Model.PunchInResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PunchInPresenterImpl implements PunchinContract.GetPunchIntractor{
    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);
    @Override
    public void punchSuccessful(OnFinishedListener onFinishedListener, PunchInRequest punchInRequest) {

        Call<PunchInResponse> punchInResponseCall=service.punchInOut(punchInRequest);
        punchInResponseCall.enqueue(new Callback<PunchInResponse>() {
            @Override
            public void onResponse(Call<PunchInResponse> call, Response<PunchInResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PunchInResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }
}
