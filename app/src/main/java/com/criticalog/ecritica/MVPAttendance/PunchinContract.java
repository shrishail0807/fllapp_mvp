package com.criticalog.ecritica.MVPAttendance;

import com.criticalog.ecritica.MVPAttendance.Model.PunchInRequest;
import com.criticalog.ecritica.MVPAttendance.Model.PunchInResponse;

public interface PunchinContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onPunchButtonClick(PunchInRequest punchInRequest);

        //void LoginRequestToServer(LoginPostData loginPostData);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToViews(PunchInResponse punchInResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetPunchIntractor {

        interface OnFinishedListener {
            void onFinished(PunchInResponse punchInResponse);
            void onFailure(Throwable t);
        }
        void punchSuccessful(PunchinContract.GetPunchIntractor.OnFinishedListener onFinishedListener, PunchInRequest punchInRequest);
    }
}
