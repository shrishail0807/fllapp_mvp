package com.criticalog.ecritica.mvpchequedetails.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.mvpchequedetails.chequedetailsrepo.ChequeDetailsRepository;

public class ChequeDetailsViewModel extends AndroidViewModel {

    private ChequeDetailsRepository mChequeDetailsRepository;
    private LiveData<BankListResponse> bankListResponse;

    public ChequeDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        mChequeDetailsRepository = new ChequeDetailsRepository();
        bankListResponse = mChequeDetailsRepository.getVolumesResponseLiveData();
    }

    public void getBankList(BankListRequest bankListRequest) {
        mChequeDetailsRepository.getBankList(bankListRequest);
    }

    public LiveData<BankListResponse> getVolumesResponseLiveData() {
        return bankListResponse;
    }
}
