package com.criticalog.ecritica.mvpchequedetails;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSListContract;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPDRS.DRSPresenterImpl;
import com.criticalog.ecritica.MVPDRS.GetDrsInteractorImpl;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.MVPPickup.PickupCancel.DatumReasons;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsResponse;
import com.criticalog.ecritica.NewCODProcess.ChequeDetails;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.DataBank;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.NewCODProcess.NewCodInteractorImpl;
import com.criticalog.ecritica.NewCODProcess.NewCodPresenterImpl;
import com.criticalog.ecritica.NewCODProcess.NewCodProcessContract;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PhotoData;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.databinding.ActivityChequeDetailsMvvmBinding;
import com.criticalog.ecritica.mvpchequedetails.chequedetailsrepo.ChequeDetailsRepository;
import com.criticalog.ecritica.mvpchequedetails.viewmodel.ChequeDetailsViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChequeDetailsMVVM extends AppCompatActivity {
    private ActivityChequeDetailsMvvmBinding activityChequeDetailsMvvmBinding;
    private ChequeDetailsViewModel chequeDetailsViewModel;
    ArrayList<SampleSearchModel> sampleSearchModelArrayList = new ArrayList<>();
    private String bankcode="", bankName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityChequeDetailsMvvmBinding = DataBindingUtil.setContentView(this, R.layout.activity_cheque_details_mvvm);

        chequeDetailsViewModel = ViewModelProviders.of(this).get(ChequeDetailsViewModel.class);
        chequeDetailsViewModel.init();

        Picasso.with(this)
                .load("https://www.ecritica.co/efreightlive/media/app/chequeImg.png")
                .resize(2200, 400)
                .placeholder(R.drawable.brokenimage)
                .into(activityChequeDetailsMvvmBinding.mChequeRefImageOnline);

        activityChequeDetailsMvvmBinding.mChequeBankName.setFocusable(false);
        activityChequeDetailsMvvmBinding.mChequeBankName.setClickable(true);

        chequeDetailsViewModel.getVolumesResponseLiveData().observe(this, new Observer<BankListResponse>() {
            @Override
            public void onChanged(BankListResponse bankListResponse) {

                Toast.makeText(ChequeDetailsMVVM.this, bankListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                if (bankListResponse.getStatus() == 200) {

                    List<DataBank> dataBankList=bankListResponse.getData();
                    for (int i = 0; i < bankListResponse.getData().size(); i++) {
                        sampleSearchModelArrayList.add(new SampleSearchModel(bankListResponse.getData().get(i).getBankName()));
                    }
                    showOriginDestination(sampleSearchModelArrayList, "Search Bank",dataBankList);
                } else {
                    Toast.makeText(ChequeDetailsMVVM.this, bankListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        activityChequeDetailsMvvmBinding.mChequeBankName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sampleSearchModelArrayList.clear();
                BankListRequest bankListRequest = new BankListRequest();
                bankListRequest.setAction("bank_list");
                bankListRequest.setUserId("11337");
                chequeDetailsViewModel.getBankList(bankListRequest);
            }
        });
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere,  List<DataBank> dataBankList) {

        new SimpleSearchDialogCompat(ChequeDetailsMVVM.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source


                        if(item.getTitle().equalsIgnoreCase("OTHER BANK"))
                        {
                            activityChequeDetailsMvvmBinding.mEnterBankName.setVisibility(View.VISIBLE);
                        }else {
                            activityChequeDetailsMvvmBinding.mEnterBankName.setVisibility(View.GONE);
                            activityChequeDetailsMvvmBinding.mEnterBankName.setText("");
                        }

                        bankName=item.getTitle();
                        activityChequeDetailsMvvmBinding.mChequeBankName.setText(item.getTitle());

                        bankcode=dataBankList.get(position).getBankCode();

                        dialog.dismiss();
                    }
                }).show();
    }
}
