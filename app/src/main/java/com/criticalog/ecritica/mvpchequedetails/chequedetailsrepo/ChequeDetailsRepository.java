package com.criticalog.ecritica.mvpchequedetails.chequedetailsrepo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChequeDetailsRepository {

    private RestServices mRestServices;
    private MutableLiveData<BankListResponse> bankListResponseMutableLiveData;

    public ChequeDetailsRepository() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        bankListResponseMutableLiveData = new MutableLiveData<>();
    }

    public void getBankList(BankListRequest bankListRequest) {

        Call<BankListResponse> bankListResponseCall = mRestServices.getBankList(bankListRequest);
        bankListResponseCall.enqueue(new Callback<BankListResponse>() {
            @Override
            public void onResponse(Call<BankListResponse> call, Response<BankListResponse> response) {

                bankListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<BankListResponse> call, Throwable t) {

                bankListResponseMutableLiveData.setValue(null);
            }
        });
    }

    public LiveData<BankListResponse> getVolumesResponseLiveData() {
        return bankListResponseMutableLiveData;
    }

}
