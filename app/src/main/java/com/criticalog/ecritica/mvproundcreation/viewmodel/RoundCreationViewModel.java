package com.criticalog.ecritica.mvproundcreation.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.mvpdrspreparation.Repository.DRSPreparationRepo;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourRequest;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourResponse;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationRequest;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubResponse;
import com.criticalog.ecritica.mvproundcreation.repository.RoundCreationRepo;

public class RoundCreationViewModel extends AndroidViewModel {

    private RoundCreationRepo roundCreationRepo;
    private LiveData<BaggingOriginDestResponse> baggingOriginDestResponseLiveData;
    private LiveData<UserTypeListResponse> userTypeListResponseLiveData;
    private LiveData<VehicleTypeResponse> vehicleTypeResponseLiveData;
    private LiveData<HubUserListResponse> hubUserListResponseLiveData;
    private LiveData<VehiclesByHubResponse> vehiclesByHubResponseLiveData;
    private LiveData<RoundCreationResponse> roundCreationResponseLiveData;
    private LiveData<GetVehicleTypeTwoOrFourResponse> getVehicleTypeTwoOrFourResponseLiveData;

    public RoundCreationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        roundCreationRepo = new RoundCreationRepo();
        baggingOriginDestResponseLiveData = roundCreationRepo.getHubsLiveData();
        userTypeListResponseLiveData = roundCreationRepo.getUserTypeLiveData();
        vehicleTypeResponseLiveData = roundCreationRepo.getVehicleTypeLiveData();
        hubUserListResponseLiveData = roundCreationRepo.getHubUserListLiveData();
        vehiclesByHubResponseLiveData = roundCreationRepo.getVehiclesByHubLiveData();
        roundCreationResponseLiveData = roundCreationRepo.getRoundCreationLiveData();
        getVehicleTypeTwoOrFourResponseLiveData = roundCreationRepo.getTwoOrFourVehicleType();
    }

    public void getVehicleTypeTwoFour(GetVehicleTypeTwoOrFourRequest getVehicleTypeTwoOrFourRequest) {
        roundCreationRepo.getTwoOrForVehicle(getVehicleTypeTwoOrFourRequest);
    }

    public LiveData<GetVehicleTypeTwoOrFourResponse> getVehicleTypeTwoFouLiveData() {
        return getVehicleTypeTwoOrFourResponseLiveData;
    }

    public void getHubs(BaggingOriginDestRequest baggingOriginDestRequest) {
        roundCreationRepo.getOriginHubs(baggingOriginDestRequest);
    }

    public LiveData<BaggingOriginDestResponse> getHubsLiveData() {
        return baggingOriginDestResponseLiveData;
    }

    public void getUserType(UserTypeListRequest userTypeListRequest) {
        roundCreationRepo.getUserType(userTypeListRequest);
    }

    public LiveData<UserTypeListResponse> getUserTypeLiveData() {
        return userTypeListResponseLiveData;
    }

    public void getVehicleType(VehicleTypeRequest vehicleTypeRequest) {
        roundCreationRepo.getVehicleType(vehicleTypeRequest);
    }

    public LiveData<VehicleTypeResponse> getVehicleTypeLiveData() {
        return vehicleTypeResponseLiveData;
    }

    public void getHubUserList(HubUserListRequest hubUserListRequest) {
        roundCreationRepo.getHubUserList(hubUserListRequest);
    }

    public LiveData<HubUserListResponse> getHubUserListLiveData() {
        return hubUserListResponseLiveData;
    }

    public void getVehiclesListByHub(VehiclesByHubRequest vehiclesByHubRequest) {
        roundCreationRepo.getVehiclesByHub(vehiclesByHubRequest);
    }

    public LiveData<VehiclesByHubResponse> getVehiclesByHubLiveData() {
        return vehiclesByHubResponseLiveData;
    }


    public void roundCreation(RoundCreationRequest roundCreationRequest) {
        roundCreationRepo.roundCreationConfirm(roundCreationRequest);
    }

    public LiveData<RoundCreationResponse> roundCreationLiveData() {
        return roundCreationResponseLiveData;
    }
}
