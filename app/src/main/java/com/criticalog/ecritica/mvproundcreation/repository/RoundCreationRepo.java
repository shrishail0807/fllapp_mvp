package com.criticalog.ecritica.mvproundcreation.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourRequest;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourResponse;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationRequest;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoundCreationRepo {
    private RestServices mRestServices;
    private MutableLiveData<BaggingOriginDestResponse> baggingOriginDestResponseMutableLiveData;
    private MutableLiveData<UserTypeListResponse> userTypeListResponseMutableLiveData;
    private MutableLiveData<VehicleTypeResponse> vehicleTypeResponseMutableLiveData;
    private MutableLiveData<HubUserListResponse> hubUserListResponseMutableLiveData;
    private MutableLiveData<VehiclesByHubResponse> vehiclesByHubResponseMutableLiveData;
    private MutableLiveData<RoundCreationResponse> roundCreationResponseMutableLiveData;
    private MutableLiveData<GetVehicleTypeTwoOrFourResponse> getVehicleTypeTwoOrFourResponseMutableLiveData;

    public RoundCreationRepo() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        baggingOriginDestResponseMutableLiveData = new MutableLiveData<>();
        userTypeListResponseMutableLiveData = new MutableLiveData<>();
        vehicleTypeResponseMutableLiveData = new MutableLiveData<>();
        hubUserListResponseMutableLiveData = new MutableLiveData<>();
        vehiclesByHubResponseMutableLiveData = new MutableLiveData<>();
        roundCreationResponseMutableLiveData = new MutableLiveData<>();
        getVehicleTypeTwoOrFourResponseMutableLiveData = new MutableLiveData<>();
    }

    public void getOriginHubs(BaggingOriginDestRequest baggingOriginDestRequest) {
        Call<BaggingOriginDestResponse> baggingOriginDestResponseCall = mRestServices.baggingOriginDestinationHub(baggingOriginDestRequest);

        baggingOriginDestResponseCall.enqueue(new Callback<BaggingOriginDestResponse>() {
            @Override
            public void onResponse(Call<BaggingOriginDestResponse> call, Response<BaggingOriginDestResponse> response) {
                baggingOriginDestResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<BaggingOriginDestResponse> call, Throwable t) {
                baggingOriginDestResponseMutableLiveData.setValue(null);
            }
        });

    }

    public void getUserType(UserTypeListRequest userTypeListRequest) {
        Call<UserTypeListResponse> userTypeListResponseCall = mRestServices.getUserType(userTypeListRequest);
        userTypeListResponseCall.enqueue(new Callback<UserTypeListResponse>() {
            @Override
            public void onResponse(Call<UserTypeListResponse> call, Response<UserTypeListResponse> response) {
                userTypeListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<UserTypeListResponse> call, Throwable t) {
                userTypeListResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getVehicleType(VehicleTypeRequest vehicleTypeRequest) {
        Call<VehicleTypeResponse> vehicleTypeResponseCall = mRestServices.getVehicleType(vehicleTypeRequest);
        vehicleTypeResponseCall.enqueue(new Callback<VehicleTypeResponse>() {
            @Override
            public void onResponse(Call<VehicleTypeResponse> call, Response<VehicleTypeResponse> response) {
                vehicleTypeResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<VehicleTypeResponse> call, Throwable t) {
                vehicleTypeResponseMutableLiveData.setValue(null);
            }
        });
    }


    public void getHubUserList(HubUserListRequest hubUserListRequest) {
        Call<HubUserListResponse> hubUserListResponseCall = mRestServices.getHubUserList(hubUserListRequest);
        hubUserListResponseCall.enqueue(new Callback<HubUserListResponse>() {
            @Override
            public void onResponse(Call<HubUserListResponse> call, Response<HubUserListResponse> response) {
                hubUserListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<HubUserListResponse> call, Throwable t) {
                hubUserListResponseMutableLiveData.setValue(null);
            }
        });
    }


    public void getVehiclesByHub(VehiclesByHubRequest vehiclesByHubRequest) {
        Call<VehiclesByHubResponse> vehiclesByHubResponseCall = mRestServices.getVehiclesByHubID(vehiclesByHubRequest);
        vehiclesByHubResponseCall.enqueue(new Callback<VehiclesByHubResponse>() {
            @Override
            public void onResponse(Call<VehiclesByHubResponse> call, Response<VehiclesByHubResponse> response) {
                vehiclesByHubResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<VehiclesByHubResponse> call, Throwable t) {
                vehiclesByHubResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void roundCreationConfirm(RoundCreationRequest roundCreationRequest) {

        Call<RoundCreationResponse> roundCreationResponseCall = mRestServices.roundCreation(roundCreationRequest);
        roundCreationResponseCall.enqueue(new Callback<RoundCreationResponse>() {
            @Override
            public void onResponse(Call<RoundCreationResponse> call, Response<RoundCreationResponse> response) {
                roundCreationResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<RoundCreationResponse> call, Throwable t) {
                roundCreationResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getTwoOrForVehicle(GetVehicleTypeTwoOrFourRequest getVehicleTypeTwoOrFourRequest) {
        Call<GetVehicleTypeTwoOrFourResponse> getVehicleTypeTwoOrFourResponseCall = mRestServices.getTwoOrFourVehicleType(getVehicleTypeTwoOrFourRequest);
        getVehicleTypeTwoOrFourResponseCall.enqueue(new Callback<GetVehicleTypeTwoOrFourResponse>() {
            @Override
            public void onResponse(Call<GetVehicleTypeTwoOrFourResponse> call, Response<GetVehicleTypeTwoOrFourResponse> response) {

                getVehicleTypeTwoOrFourResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetVehicleTypeTwoOrFourResponse> call, Throwable t) {
                getVehicleTypeTwoOrFourResponseMutableLiveData.setValue(null);
            }
        });
    }

    public LiveData<GetVehicleTypeTwoOrFourResponse> getTwoOrFourVehicleType() {
        return getVehicleTypeTwoOrFourResponseMutableLiveData;
    }

    public LiveData<RoundCreationResponse> getRoundCreationLiveData() {
        return roundCreationResponseMutableLiveData;
    }

    public LiveData<VehicleTypeResponse> getVehicleTypeLiveData() {
        return vehicleTypeResponseMutableLiveData;
    }

    public LiveData<UserTypeListResponse> getUserTypeLiveData() {
        return userTypeListResponseMutableLiveData;
    }

    public LiveData<BaggingOriginDestResponse> getHubsLiveData() {
        return baggingOriginDestResponseMutableLiveData;
    }

    public LiveData<HubUserListResponse> getHubUserListLiveData() {
        return hubUserListResponseMutableLiveData;
    }

    public LiveData<VehiclesByHubResponse> getVehiclesByHubLiveData() {
        return vehiclesByHubResponseMutableLiveData;
    }
}
