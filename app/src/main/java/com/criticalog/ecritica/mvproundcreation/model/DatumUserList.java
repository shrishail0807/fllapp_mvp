package com.criticalog.ecritica.mvproundcreation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumUserList {
    @SerializedName("fll_id")
    @Expose
    private String fllId;
    @SerializedName("fll_name")
    @Expose
    private String fllName;

    public String getFllId() {
        return fllId;
    }

    public void setFllId(String fllId) {
        this.fllId = fllId;
    }

    public String getFllName() {
        return fllName;
    }

    public void setFllName(String fllName) {
        this.fllName = fllName;
    }

}
