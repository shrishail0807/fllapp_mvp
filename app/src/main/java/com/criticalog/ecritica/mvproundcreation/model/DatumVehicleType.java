package com.criticalog.ecritica.mvproundcreation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumVehicleType {
    @SerializedName("vehicle_types_code")
    @Expose
    private String vehicleTypesCode;
    @SerializedName("vehicle_types_desc")
    @Expose
    private String vehicleTypesDesc;

    public String getVehicleTypesCode() {
        return vehicleTypesCode;
    }

    public void setVehicleTypesCode(String vehicleTypesCode) {
        this.vehicleTypesCode = vehicleTypesCode;
    }

    public String getVehicleTypesDesc() {
        return vehicleTypesDesc;
    }

    public void setVehicleTypesDesc(String vehicleTypesDesc) {
        this.vehicleTypesDesc = vehicleTypesDesc;
    }

}
