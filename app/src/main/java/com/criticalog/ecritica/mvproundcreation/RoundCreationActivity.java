package com.criticalog.ecritica.mvproundcreation;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.OriginHubDetail;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.databinding.MvvmRoundCreationBinding;
import com.criticalog.ecritica.mvpdrspreparation.DRSPreparationActivity;
import com.criticalog.ecritica.mvproundcreation.model.DatumUserList;
import com.criticalog.ecritica.mvproundcreation.model.DatumUserType;
import com.criticalog.ecritica.mvproundcreation.model.DatumVehicleType;
import com.criticalog.ecritica.mvproundcreation.model.DatumVehicles;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourRequest;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourResponse;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationRequest;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubResponse;
import com.criticalog.ecritica.mvproundcreation.viewmodel.RoundCreationViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class RoundCreationActivity extends AppCompatActivity {

    private MvvmRoundCreationBinding mvvmRoundCreationBinding;
    private RoundCreationViewModel roundCreationViewModel;
    private ArrayList<SampleSearchModel> searchHubs = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchUserType = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchVehicleType = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchHubUserList = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchVehiclesByHub = new ArrayList<>();
    ArrayList<String> createDataStringhubs = new ArrayList();
    ArrayList<String> createDataStrinUserNames = new ArrayList();
    ArrayList<String> createStringUserType = new ArrayList();
    ArrayList<String> createStringVehicleType = new ArrayList();
    ArrayList<String> createStringVehicleNames = new ArrayList();

    private List<OriginHubDetail> originHubList = new ArrayList<>();
    private List<DatumUserType> UserTypeList = new ArrayList<>();
    private List<DatumVehicleType> VehicleTypeList = new ArrayList<>();
    private List<DatumUserList> hubUserList = new ArrayList<>();
    private List<DatumVehicles> vehiclesByHubList = new ArrayList<>();
    private SimpleArcDialog mProgressBar;

    private int dialogOpenFlag = 0;
    private int dialogOpenFlagUserType = 0;
    private int dialogAllocatedUser = 0;
    private int dialogOpenFlagVehicleType = 0;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String token, userId;
    private Dialog dialogProductDelivered;

    int vehicleNameEnable = 0;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double latitude = 0.0, longitude = 0.0;
    private String vendorName="";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvvmRoundCreationBinding = DataBindingUtil.setContentView(this, R.layout.mvvm_round_creation);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        getLocation();
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        token = mCriticalogSharedPreferences.getData("token");
        userId = mCriticalogSharedPreferences.getData("userId");
        mCriticalogSharedPreferences.saveData("user_type_code", "");
        mCriticalogSharedPreferences.saveData("vehicle_type", "");
        mCriticalogSharedPreferences.saveData("vehicle_number_send", "");
        mCriticalogSharedPreferences.saveData("hub_code_user", "");
        mCriticalogSharedPreferences.saveData("user_id_hub", "");

        StaticUtils.TOKEN = token;

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        roundCreationViewModel = ViewModelProviders.of(this).get(RoundCreationViewModel.class);
        roundCreationViewModel.init();

        mvvmRoundCreationBinding.mTopBarText.setText("Round Creation");
        mvvmRoundCreationBinding.selectHub.setFocusable(false);
        mvvmRoundCreationBinding.user.setFocusable(false);
        mvvmRoundCreationBinding.vehicleType.setFocusable(false);
        mvvmRoundCreationBinding.allocatedUser.setFocusable(false);
        // mvvmRoundCreationBinding.vehicleName.setFocusable(false);

        callOriginHubs();
        getUserType();
       //  getHubUserList();
         //getVehicleType();
        mvvmRoundCreationBinding.vehicleName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vehicleNameEnable == 0) {
                    if (mCriticalogSharedPreferences.getData("vehicle_type").equalsIgnoreCase("emp")) {
                        searchVehicleType.clear();
                        createStringVehicleType.clear();
                        GetVehicleTypeTwoOrFourRequest getVehicleTypeTwoOrFourRequest = new GetVehicleTypeTwoOrFourRequest();
                        getVehicleTypeTwoOrFourRequest.setAction("get_vehicle_types");
                        getVehicleTypeTwoOrFourRequest.setUserId(userId);

                        roundCreationViewModel.getVehicleTypeTwoFour(getVehicleTypeTwoOrFourRequest);

                    } else {
                        getVehiclesByHubId();
                    }
                } else {
                    //requestFocus(mvvmRoundCreationBinding.vehicleName);
                    mvvmRoundCreationBinding.vehicleNameManual.setVisibility(View.VISIBLE);
                    mvvmRoundCreationBinding.vehicleName.setVisibility(View.GONE);
                }
            }
        });

        mvvmRoundCreationBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mvvmRoundCreationBinding.allocatedUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialogAllocatedUser=1;
                getHubUserList();
            }
        });
        mvvmRoundCreationBinding.vehicleType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVehicleType();

            }
        });
        mvvmRoundCreationBinding.user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOpenFlagUserType=1;
                getUserType();

            }
        });
        mvvmRoundCreationBinding.selectHub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOpenFlag = 1;
                callOriginHubs();

            }
        });

        mvvmRoundCreationBinding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roundCreationConfirm();
            }
        });

        roundCreationViewModel.getVehicleTypeTwoFouLiveData().observe(this, new Observer<GetVehicleTypeTwoOrFourResponse>() {
            @Override
            public void onChanged(GetVehicleTypeTwoOrFourResponse getVehicleTypeTwoOrFourResponse) {
                if (getVehicleTypeTwoOrFourResponse.getStatus() == 200) {
                    for (int i = 0; i < getVehicleTypeTwoOrFourResponse.getData().size(); i++)
                        searchVehicleType.add(new SampleSearchModel(getVehicleTypeTwoOrFourResponse.getData().get(i)));
                }

                showOriginDestination(searchVehicleType, "Vehicle Type Two Four");
            }
        });
        roundCreationViewModel.getVehiclesByHubLiveData().observe(this, new Observer<VehiclesByHubResponse>() {
            @Override
            public void onChanged(VehiclesByHubResponse vehiclesByHubResponse) {
                mProgressBar.dismiss();
                Toast.makeText(RoundCreationActivity.this, vehiclesByHubResponse.getMessage(), Toast.LENGTH_SHORT).show();
                if (vehiclesByHubResponse != null) {
                    if (vehiclesByHubResponse.getStatus() == 200) {
                        for (int i = 0; i < vehiclesByHubResponse.getData().size(); i++) {
                           // searchVehiclesByHub.add(new SampleSearchModel(vehiclesByHubResponse.getData().get(i).getVehicleType() + "-" + vehiclesByHubResponse.getData().get(i).getVehicleNumber() + "-" + vehiclesByHubResponse.getData().get(i).getVendorName()));
                         //   createStringVehicleNames.add(vehiclesByHubResponse.getData().get(i).getVehicleType() + "-" + vehiclesByHubResponse.getData().get(i).getVehicleNumber() + "-" + vehiclesByHubResponse.getData().get(i).getVendorName());
                            searchVehiclesByHub.add(new SampleSearchModel(vehiclesByHubResponse.getData().get(i).getVendorName()));
                            createStringVehicleNames.add(vehiclesByHubResponse.getData().get(i).getVendorName());
                        }
                        vehiclesByHubList = vehiclesByHubResponse.getData();
                        showOriginDestination(searchVehiclesByHub, "Vehicles Name");
                    } else {
                        mvvmRoundCreationBinding.vehicleName.clearFocus();
                        mvvmRoundCreationBinding.vehicleName.requestFocus();
                        vehicleNameEnable = 1;
                        vehiclesByHubList.clear();
                    }
                }
            }
        });
        roundCreationViewModel.getVehicleTypeLiveData().observe(this, new Observer<VehicleTypeResponse>() {
            @Override
            public void onChanged(VehicleTypeResponse vehicleTypeResponse) {

                if (vehicleTypeResponse != null) {
                    if (vehicleTypeResponse.getStatus() == 200) {
                        for (int i = 0; i < vehicleTypeResponse.getData().size(); i++) {
                            searchVehicleType.add(new SampleSearchModel(vehicleTypeResponse.getData().get(i).getVehicleTypesDesc()));
                            createStringVehicleType.add(vehicleTypeResponse.getData().get(i).getVehicleTypesDesc());
                            //mvvmRoundCreationBinding.vehicleType.setText(vehicleTypeResponse.getData().get(0).getVehicleTypesDesc());
                            mCriticalogSharedPreferences.saveData("vehicle_type", vehicleTypeResponse.getData().get(0).getVehicleTypesCode());
                        }

                        VehicleTypeList = vehicleTypeResponse.getData();
                        showOriginDestination(searchVehicleType, "Vehicle Type");

                    }
                }


            }
        });
        roundCreationViewModel.getUserTypeLiveData().observe(this, new Observer<UserTypeListResponse>() {
            @Override
            public void onChanged(UserTypeListResponse userTypeListResponse) {
                if (userTypeListResponse != null) {
                    if (userTypeListResponse.getStatus() == 200) {
                        for (int i = 0; i < userTypeListResponse.getData().size(); i++) {
                            searchUserType.add(new SampleSearchModel(userTypeListResponse.getData().get(i).getUserTypeDesc()));

                            createStringUserType.add(userTypeListResponse.getData().get(i).getUserTypeDesc());
                            mvvmRoundCreationBinding.user.setText(userTypeListResponse.getData().get(0).getUserTypeDesc());

                            mCriticalogSharedPreferences.saveData("user_type_code", userTypeListResponse.getData().get(0).getUserTypeCode());
                        }

                        UserTypeList = userTypeListResponse.getData();
                       // showOriginDestination(searchUserType, "User Type");
                        if (dialogOpenFlagUserType == 1) {
                            showOriginDestination(searchUserType, "User Type");
                        }
                    }
                }
            }
        });

        roundCreationViewModel.getHubsLiveData().observe(this, new Observer<BaggingOriginDestResponse>() {
            @Override
            public void onChanged(BaggingOriginDestResponse baggingOriginDestResponse) {

                if (baggingOriginDestResponse != null) {
                    if (baggingOriginDestResponse.getStatus() == 200) {
                        for (int i = 0; i < baggingOriginDestResponse.getData().getHubDetails().size(); i++) {
                            searchHubs.add(new SampleSearchModel(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode()));
                            createDataStringhubs.add(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode());

                            if (baggingOriginDestResponse.getData().getHubDetails().get(i).getHubDefault().equalsIgnoreCase("1")) {
                                mvvmRoundCreationBinding.selectHub.setText(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode());

                                mCriticalogSharedPreferences.saveData("hub_code_user", baggingOriginDestResponse.getData().getHubDetails().get(i).getHubId());
                            }
                        }

                        originHubList = baggingOriginDestResponse.getData().getHubDetails();
                        // showOriginDestination(searchHubs, "Origin Hub");
                        if (dialogOpenFlag == 1) {
                            showOriginDestination(searchHubs, "Origin Hub");
                        }
                    }
                }
            }
        });

        roundCreationViewModel.getHubUserListLiveData().observe(this, new Observer<HubUserListResponse>() {
            @Override
            public void onChanged(HubUserListResponse hubUserListResponse) {
                mProgressBar.dismiss();
                if (hubUserListResponse != null) {
                    Toast.makeText(RoundCreationActivity.this, hubUserListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    if (hubUserListResponse.getStatus() == 200) {
                        for (int i = 0; i < hubUserListResponse.getData().size(); i++) {
                            searchHubUserList.add(new SampleSearchModel(hubUserListResponse.getData().get(i).getFllName() + "(" + hubUserListResponse.getData().get(i).getFllId() + ")"));
                            createDataStrinUserNames.add(hubUserListResponse.getData().get(i).getFllName() + "(" + hubUserListResponse.getData().get(i).getFllId() + ")");
                         //   mCriticalogSharedPreferences.saveData("user_id_hub", hubUserListResponse.getData().get(0).getFllId());

                      //     mvvmRoundCreationBinding.allocatedUser.setText(hubUserListResponse.getData().get(i).getFllName() + "(" + hubUserListResponse.getData().get(i).getFllId() + ")");
                        }
                        hubUserList = hubUserListResponse.getData();
                        showOriginDestination(searchHubUserList, "Select User");
                       /* if(dialogAllocatedUser==1)
                        {
                            showOriginDestination(searchHubUserList, "Select User");
                        }*/

                    }
                }

            }
        });

        roundCreationViewModel.roundCreationLiveData().observe(this, new Observer<RoundCreationResponse>() {
            @Override
            public void onChanged(RoundCreationResponse roundCreationResponse) {
                mProgressBar.dismiss();
                if (roundCreationResponse.getStatus() == 200) {
                    roundCreationDialogMesssage(roundCreationResponse.getMessage());
                } else {
                    Toasty.warning(RoundCreationActivity.this, roundCreationResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(RoundCreationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(RoundCreationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RoundCreationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(RoundCreationActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void roundCreationDialogMesssage(String message) {
        //Dialog Manual Entry
        TextView mDialogText, mOkDialog;
        dialogProductDelivered = new Dialog(RoundCreationActivity.this);
        dialogProductDelivered.setContentView(R.layout.dialog_pod_scan_not_done);
        mOkDialog = dialogProductDelivered.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered.findViewById(R.id.mDialogText);
        mDialogText.setText(message);
        mDialogText.setTextColor(Color.GREEN);
        dialogProductDelivered.setCancelable(false);
        dialogProductDelivered.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProductDelivered.dismiss();
                finish();

            }
        });
    }

    void roundCreationConfirm() {

        String vehicleNumber1 = mvvmRoundCreationBinding.vehicleNumber.getText().toString();
        String startKm = mvvmRoundCreationBinding.startKm.getText().toString();
        String userType = mCriticalogSharedPreferences.getData("user_type_code");
        String vehicleType = (mCriticalogSharedPreferences.getData("vehicle_type"));
        String vehicleName = mCriticalogSharedPreferences.getData("vehicle_number_send");
        String hubCode = mCriticalogSharedPreferences.getData("hub_code_user");
        String assignUserCode = mCriticalogSharedPreferences.getData("user_id_hub");

        if (userType.equalsIgnoreCase("3rd Agent")) {
            if (hubCode.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select Hub Code", Toast.LENGTH_SHORT, true).show();
            } else if (userType.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select USer Type", Toast.LENGTH_SHORT, true).show();
            } else if (assignUserCode.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select FLL", Toast.LENGTH_SHORT, true).show();
            } else {
                mProgressBar.show();
                RoundCreationRequest roundCreationRequest = new RoundCreationRequest();
                roundCreationRequest.setAction("create_fll_round");
                roundCreationRequest.setHubCode(hubCode);
                roundCreationRequest.setUserId(userId);
                roundCreationRequest.setUserType(userType);
                roundCreationRequest.setVehicleType("");
                roundCreationRequest.setUserCode(assignUserCode);
                roundCreationRequest.setVehicleName("");
                roundCreationRequest.setVehicleNumber("");
                roundCreationRequest.setStart_km(startKm);
                roundCreationRequest.setVendor_name(vendorName);
                roundCreationRequest.setLatitude(String.valueOf(latitude));
                roundCreationRequest.setLongitude(String.valueOf(longitude));
               roundCreationViewModel.roundCreation(roundCreationRequest);
            }

        } else {

            if (hubCode.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select Hub Code", Toast.LENGTH_SHORT, true).show();
            } else if (userType.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select User Type", Toast.LENGTH_SHORT, true).show();
            } else if (vehicleType.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select Vehicle Type", Toast.LENGTH_SHORT, true).show();
            } else if (assignUserCode.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select FLL", Toast.LENGTH_SHORT, true).show();
            } else if (vehicleName.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Select/Enter Vehicle Name", Toast.LENGTH_SHORT, true).show();
            } else if((isVehicleNumber1(vehicleNumber1)==false)&&(isVehicleNumber2(vehicleNumber1)==false)&&(isVehicleNumber3(vehicleNumber1)==false)&&(isVehicleNumber4(vehicleNumber1)==false))
            {
                Toasty.warning(RoundCreationActivity.this, "Enter Valid Vehicle Number", Toast.LENGTH_SHORT, true).show();
            }else if (startKm.equalsIgnoreCase("")) {
                Toasty.warning(RoundCreationActivity.this, "Enter Start KM", Toast.LENGTH_SHORT, true).show();
            } else {
                if (vehicleType.equalsIgnoreCase("mkt")) {
                    vehicleName = mvvmRoundCreationBinding.vehicleNameManual.getText().toString();
                }
                mProgressBar.show();
                RoundCreationRequest roundCreationRequest = new RoundCreationRequest();
                roundCreationRequest.setAction("create_fll_round");
                roundCreationRequest.setHubCode(hubCode);
                roundCreationRequest.setUserId(userId);
                roundCreationRequest.setUserType(userType);
                roundCreationRequest.setVehicleType(vehicleType);
                roundCreationRequest.setUserCode(assignUserCode);
                roundCreationRequest.setVehicleName(vehicleName);
                roundCreationRequest.setVehicleNumber(vehicleNumber1);
                roundCreationRequest.setStart_km(startKm);
                roundCreationRequest.setVendor_name(vendorName);
                roundCreationRequest.setLatitude(String.valueOf(latitude));
                roundCreationRequest.setLongitude(String.valueOf(longitude));

                roundCreationViewModel.roundCreation(roundCreationRequest);
            }
        }
    }

    void getHubUserList() {
        searchHubUserList.clear();
        hubUserList.clear();
        createDataStrinUserNames.clear();
        createDataStringhubs.clear();
        HubUserListRequest hubUserListRequest = new HubUserListRequest();
        hubUserListRequest.setAction("get_hub_users_by_user_type");
        hubUserListRequest.setUserId(userId);
        hubUserListRequest.setHubCode(mCriticalogSharedPreferences.getData("hub_code_user"));
        hubUserListRequest.setUserType(mCriticalogSharedPreferences.getData("user_type_code"));
        roundCreationViewModel.getHubUserList(hubUserListRequest);
        mProgressBar.show();
    }

    void getVehiclesByHubId() {
        searchVehiclesByHub.clear();
        vehiclesByHubList.clear();
        createStringVehicleNames.clear();
        VehiclesByHubRequest vehiclesByHubRequest = new VehiclesByHubRequest();
        vehiclesByHubRequest.setAction("get_vehicles_by_hub_id");
        vehiclesByHubRequest.setUserId(userId);
        vehiclesByHubRequest.setFllId(mCriticalogSharedPreferences.getData("user_id_hub"));
        vehiclesByHubRequest.setVehicleType(mCriticalogSharedPreferences.getData("vehicle_type"));
        vehiclesByHubRequest.setHubCode(mCriticalogSharedPreferences.getData("hub_code_user"));
        mProgressBar.show();
        roundCreationViewModel.getVehiclesListByHub(vehiclesByHubRequest);
    }

    void getVehicleType() {
        searchVehicleType.clear();
        VehicleTypeRequest vehicleTypeRequest = new VehicleTypeRequest();
        vehicleTypeRequest.setAction("get_vehicle_type_list");
        vehicleTypeRequest.setUserId(userId);
        roundCreationViewModel.getVehicleType(vehicleTypeRequest);

    }

    void getUserType() {
        searchUserType.clear();
        createStringUserType.clear();
        UserTypeListRequest userTypeListRequest = new UserTypeListRequest();
        userTypeListRequest.setAction("get_user_type_list");
        userTypeListRequest.setUserId(userId);
        roundCreationViewModel.getUserType(userTypeListRequest);
    }

    void callOriginHubs() {
        searchHubs.clear();
        createDataStringhubs.clear();
        originHubList.clear();
        BaggingOriginDestRequest baggingOriginDestRequest = new BaggingOriginDestRequest();
        baggingOriginDestRequest.setAction("bagging_orghub");
        baggingOriginDestRequest.setUserId(userId);
        baggingOriginDestRequest.setLatitude("0.0");
        baggingOriginDestRequest.setLongitude("0.0");
        roundCreationViewModel.getHubs(baggingOriginDestRequest);
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(RoundCreationActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {

                        if (fromWhere.equalsIgnoreCase("Origin Hub")) {
                            int indexO = createDataStringhubs.indexOf(item.getTitle());
                            String hubCode = originHubList.get(indexO).getHubId();
                            mCriticalogSharedPreferences.saveData("hub_code_user", hubCode);
                            mvvmRoundCreationBinding.selectHub.setText(item.getTitle());
                        } else if (fromWhere.equalsIgnoreCase("User Type")) {
                            int indexUTY = createStringUserType.indexOf(item.getTitle());
                            String userTypeCode = UserTypeList.get(indexUTY).getUserTypeCode();
                            mCriticalogSharedPreferences.saveData("user_type_code", userTypeCode);
                            mvvmRoundCreationBinding.user.setText(item.getTitle());

                            mvvmRoundCreationBinding.allocatedUser.setText("");
                            mCriticalogSharedPreferences.saveData("user_id_hub", "");
                            mCriticalogSharedPreferences.saveData("vehicle_number_send", "");
                            mCriticalogSharedPreferences.saveData("vehicle_type", "");

                            mvvmRoundCreationBinding.vehicleName.setText("");
                            mvvmRoundCreationBinding.vehicleNumber.setText("");
                            mvvmRoundCreationBinding.startKm.setText("");
                            mvvmRoundCreationBinding.vehicleNameManual.setText("");
                            mvvmRoundCreationBinding.allocatedUser.setText("");
                            mvvmRoundCreationBinding.vehicleType.setText("");

                            if (item.getTitle().equalsIgnoreCase("3rd Party Agent")) {
                                mvvmRoundCreationBinding.editTextVehicleTypeLay.setVisibility(View.GONE);
                                mvvmRoundCreationBinding.editTextVehilcenameLay.setVisibility(View.GONE);
                                mvvmRoundCreationBinding.editTextVehilenumberLay.setVisibility(View.GONE);
                            } else {
                                mvvmRoundCreationBinding.editTextVehicleTypeLay.setVisibility(View.VISIBLE);
                                mvvmRoundCreationBinding.editTextVehilcenameLay.setVisibility(View.VISIBLE);
                                mvvmRoundCreationBinding.editTextVehilenumberLay.setVisibility(View.VISIBLE);
                            }
                        } else if (fromWhere.equalsIgnoreCase("Vehicle Type")) {
                            int indexVtyp = createStringVehicleType.indexOf(item.getTitle());
                            String vehicleCode = VehicleTypeList.get(indexVtyp).getVehicleTypesCode();

                            mCriticalogSharedPreferences.saveData("vehicle_type", vehicleCode);
                            mvvmRoundCreationBinding.vehicleType.setText(item.getTitle());
                            mvvmRoundCreationBinding.vehicleName.setText("");
                            mvvmRoundCreationBinding.vehicleNumber.setText("");
                            mvvmRoundCreationBinding.startKm.setText("");
                            mvvmRoundCreationBinding.vehicleNameManual.setText("");
                            vehicleNameEnable = 0;

                            if (vehicleCode.equalsIgnoreCase("emp")) {
                                vendorName="";
                                mvvmRoundCreationBinding.vehicleName.setFocusable(false);
                                mvvmRoundCreationBinding.vehicleNameManual.setVisibility(View.GONE);
                                mvvmRoundCreationBinding.vehicleName.setVisibility(View.VISIBLE);
                            } else if (vehicleCode.equalsIgnoreCase("mkt")) {
                                vendorName="";
                                mvvmRoundCreationBinding.vehicleNameManual.setVisibility(View.VISIBLE);
                                mvvmRoundCreationBinding.vehicleName.setVisibility(View.GONE);
                            } else if (vehicleCode.equalsIgnoreCase("con")) {
                                mvvmRoundCreationBinding.vehicleName.setFocusable(false);
                                mvvmRoundCreationBinding.vehicleNameManual.setVisibility(View.GONE);
                                mvvmRoundCreationBinding.vehicleName.setVisibility(View.VISIBLE);
                            }

                        } else if (fromWhere.equalsIgnoreCase("Select User")) {
                            int indexU = createDataStrinUserNames.indexOf(item.getTitle());
                            String userId = hubUserList.get(indexU).getFllId();
                            mCriticalogSharedPreferences.saveData("user_id_hub", userId);
                            mvvmRoundCreationBinding.allocatedUser.setText(item.getTitle());

                            mCriticalogSharedPreferences.saveData("vehicle_number_send", "");
                            mCriticalogSharedPreferences.saveData("vehicle_type", "");

                            mvvmRoundCreationBinding.vehicleName.setText("");
                            mvvmRoundCreationBinding.vehicleNumber.setText("");
                            mvvmRoundCreationBinding.startKm.setText("");
                            mvvmRoundCreationBinding.vehicleNameManual.setText("");
                            mvvmRoundCreationBinding.vehicleType.setText("");
                        } else if (fromWhere.equalsIgnoreCase("Vehicles Name")) {
                            int indexVN = createStringVehicleNames.indexOf(item.getTitle());
                            String vehicleNumber = vehiclesByHubList.get(indexVN).getVehicleNumber();
                            mCriticalogSharedPreferences.saveData("vehicle_number_send", vehicleNumber);
                            mvvmRoundCreationBinding.vehicleName.setText(item.getTitle());
                            mvvmRoundCreationBinding.vehicleNumber.setText(vehiclesByHubList.get(indexVN).getVehicle());
                            vendorName=vehiclesByHubList.get(indexVN).getVendorName();
                        } else if (fromWhere.equalsIgnoreCase("Vehicle Type Two Four")) {
                            mCriticalogSharedPreferences.saveData("vehicle_number_send", item.getTitle());
                            mvvmRoundCreationBinding.vehicleName.setText(item.getTitle());
                        }
                        dialog.dismiss();
                    }
                }).show();
    }


    private boolean isVehicleNumber1(String vehicleNumber) {
        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,3}[0-9]{1}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }
    private boolean isVehicleNumber2(String vehicleNumber) {

        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,3}[0-9]{2}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }
    private boolean isVehicleNumber3(String vehicleNumber) {
        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,3}[0-9]{3}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }
    private boolean isVehicleNumber4(String vehicleNumber) {
        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,3}[0-9]{4}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();
    }
}
