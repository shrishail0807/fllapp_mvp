package com.criticalog.ecritica.services;

import android.annotation.SuppressLint;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.speech.tts.TextToSpeech;
import android.util.Log;


import java.util.Locale;

@SuppressLint("OverrideAbstract")
public class PushNotificationListener extends NotificationListenerService {
    private TextToSpeech textToSpeech;
    private String mContent;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("pushnotificationlisten","created");
        textToSpeech = new TextToSpeech(getApplicationContext(), new     TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
        });
    }


    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        if (sbn.getPackageName().equals("com.criticalog.ecritica")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mContent = sbn.getNotification().extras.getString("android.text");
                textToSpeech.speak(mContent, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }
}

