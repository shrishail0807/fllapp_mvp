package com.criticalog.ecritica.services;

import static com.criticalog.ecritica.CriticallogApplication.synchedtoserver;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Interface.CancelWsCalled;
import com.criticalog.ecritica.ModelClasses.CancelTask;
import com.criticalog.ecritica.ModelClasses.DeliveryDetails;
import com.criticalog.ecritica.ModelClasses.DeliveryService;
import com.criticalog.ecritica.Utils.NetworkClass;
import com.criticalog.ecritica.Utils.SignalStrengthChecker;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.zain.android.internetconnectivitylibrary.ConnectionUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ProductDeliveredService extends IntentService  implements CancelWsCalled {
//public class ProductDeliveredService extends JobIntentService implements CancelWsCalled, PickUpSuccessWS {

    private NetworkClass mNetworkClass;
    private ConnectionUtil connectionUtil;
    private DatabaseHelper dbHelper;
    private SignalStrengthChecker mSignalStrengthChecker;
    private Timer myTimer;
    private TimerTask checkSignalStrength;
    private DeliveryService deliveryDetails;
    private File uploadImageFile,uploadImageFile1,uploadPodImageFile;

    private DeliveryDetails mDeliveryDetails;

    List<DeliveryDetails> taskList = null;
    private List<CancelTask> cancelTaskList;
    private CancelTask mCancelTask;

    private Handler handler;


    private ProgressDialog progressDialog;

    private static final int JOB_ID = 2;

   /* public static void enqueueWork(Context context, Intent serviceintent) {
        enqueueWork(context,ProductDeliveredService.class,JOB_ID,serviceintent);
    }*/

     public ProductDeliveredService() {
         super("ProductDeliveredService");

     }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        Log.i("productdeliver service","created");
    }

    /*@Override
    protected void onHandleWork(@NonNull Intent intent) {

        Log.d("inside","productdeliverservice");
        mNetworkClass = new NetworkClass(ProductDeliveredService.this);
        Log.d("prpductdeliver service","network class");
        // Create the Database helper object
        dbHelper = new DatabaseHelper(ProductDeliveredService.this);
        Log.d("prpductdeliver service","dbhelper");
        mSignalStrengthChecker = new SignalStrengthChecker(ProductDeliveredService.this);
        Log.d("prpductdeliver service","signalstrengthchecker");

        myTimer = new Timer();
        int delay = 0;   // delay for o sec
        int period = 5000;  // repeat every 5 min .
        checkSignalStrength = new TimerTask() {
            public void run() {

                if (mNetworkClass.isNetworkAvailable()) {

                    NetworkInfo networkInfo = NetworkClass.getNetworkInfo(ProductDeliveredService.this);
                    int networktype = networkInfo.getType();
                    if (networktype == ConnectivityManager.TYPE_WIFI) {
                        boolean isConnectionFast = mNetworkClass.isConnectionFast(networktype, ProductDeliveredService.this);
                        Log.d("inside service","networktype wifi");
                        if (isConnectionFast) {
                            deliverProduct();
                            cancelTask();
                            // pickUpSucccess();
                            myTimer.cancel();
                        }
                    } else if (networktype == ConnectivityManager.TYPE_MOBILE) {

                        int signalStrength = mSignalStrengthChecker.getSignalStrength();

                        if (signalStrength > -99) {

                            deliverProduct();
                            cancelTask();
                            //   pickUpSucccess();
                            myTimer.cancel();
                        }
                    }
                }
            }
        };

        //myTimer.schedule(checkSignalStrength, delay);

        myTimer.scheduleAtFixedRate(checkSignalStrength, delay, period);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");

    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("productdeliver service","destroyed");
    }


        @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Log.d("inside","productdeliverservice");

        mNetworkClass = new NetworkClass(ProductDeliveredService.this);



        // Create the Database helper object
        dbHelper = new DatabaseHelper(ProductDeliveredService.this);

        mSignalStrengthChecker = new SignalStrengthChecker(ProductDeliveredService.this);

        myTimer = new Timer();
        int delay = 0;   // delay for o sec
        int period = 300000;  // repeat every 5 min .
        checkSignalStrength = new TimerTask() {
            public void run() {

              /*  if (connectionUtil.isOnline()) {
                    int networktype = connectionUtil.getActiveNetwork();
                    if (networktype == 1){
                        boolean isConnectionFast = mNetworkClass.isConnectionFast(networktype, ProductDeliveredService.this);

                        if (isConnectionFast) {
                            deliverProduct();
                            cancelTask();
                            myTimer.cancel();
                        }
                    }else if (networktype == 0) {
                        int signalStrength = mSignalStrengthChecker.getSignalStrength();

                        if (signalStrength >= 2) {

                            deliverProduct();
                            cancelTask();
                            myTimer.cancel();
                        }
                    }

                }*/

                if (mNetworkClass.isNetworkAvailable()) {

                    NetworkInfo networkInfo = NetworkClass.getNetworkInfo(ProductDeliveredService.this);
                    int networktype = networkInfo.getType();

                    if (networktype == ConnectivityManager.TYPE_WIFI) {
                        boolean isConnectionFast = mNetworkClass.isConnectionFast(networktype, ProductDeliveredService.this);

                        if (isConnectionFast) {
                           // deliverProduct();
                            cancelTask();
                            myTimer.cancel();
                        }
                    } else if (networktype == ConnectivityManager.TYPE_MOBILE) {

                        int signalStrength = mSignalStrengthChecker.getSignalStrength();

                        if (signalStrength > -99) {

                           // deliverProduct();
                            cancelTask();
                            myTimer.cancel();
                        }
                    }
                }



            }
        };

        //myTimer.schedule(checkSignalStrength, delay);

        myTimer.scheduleAtFixedRate(checkSignalStrength, delay, period);
      /*  progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");*/

    }




    private void cancelTask() {

        try {
            cancelTaskList = dbHelper.getCancelTasks();

            if (cancelTaskList != null) {

                for (int j = 0; j < cancelTaskList.size(); j++) {
                    mCancelTask = cancelTaskList.get(j);

                    Log.e("mCancelTask size", "" + cancelTaskList.size());

                    if (mCancelTask != null || cancelTaskList.size()>0) {
                     //   mCancelVolley = new CanceldropVolley(getApplication(), mCancelTask.getTaskId(), mCancelTask.getUserId(), mCancelTask.getRefCount(), mCancelTask.getBulkFlag(),mCancelTask.getRefNumber(), mCancelTask.getDate(), mCancelTask.getTime(), mCancelTask.getReason(),mCancelTask.getLatitude(),mCancelTask.getLongitude(),ProductDeliveredService.this);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private File createImageFile() throws IOException {

        // Create an image file name
       // String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String task_id = mDeliveryDetails.getTaskId() + ".jpeg-";
        //String imageFileName = task_id;

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                task_id,  /* prefix */
                "",         /* suffix */
                storageDir      /* directory */
        );

        Log.d("inside service","image file"+image);
        String mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void CancelWSSuccessFailure(JSONObject response) {


        try {
            if (response.getString("status").equals(StaticUtils.SUCCESS_STATUS)) {
                //dbHelper.deleteCancelDetails();
                dbHelper.deleteCancelDetails(response.getString("task_id"));
                synchedtoserver = true;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(ProductDeliveredService.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(ProductDeliveredService.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}