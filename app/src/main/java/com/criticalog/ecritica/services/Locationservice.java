package com.criticalog.ecritica.services;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
/*import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;*/

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.recievers.Restartservice;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;



import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.criticalog.ecritica.CriticallogApplication.CHANNEL_ID;

public class Locationservice extends Service {
    double wayLatitude = 0.0;
    double wayLongitude = 0.0;
    Timer timer;
    TimerTask timerTask;
    String TAG = "Timers";
    int Your_X_SECS = 60;
    private DatabaseHelper db;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private double start_lat, start_long;
   // private static WorkRequest periodicWork;
    private static final String TAG1 = "periodic-work-request";

    @Override
    public void onCreate() {
        super.onCreate();
        startTimer();

        Log.d("service", "created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        startTimer();
        Log.d("inside", "onstartcommannd");
    //    requestLocationUpdates();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestLocationUpdates();
            }
        }, 300000);

        //do heavy work on a background thread
        //stopSelf();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("inside", "service ondestroy");
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("RestartService");
        broadcastIntent.setClass(this, Restartservice.class);
        this.sendBroadcast(broadcastIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    final Handler handler = new Handler();

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 300000, 300000); //
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        //createNotification() ;

                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                            createNotification();
                            requestLocationUpdates();

                        } else
                            startForeground(1, new Notification());
                    }
                });
            }
        };
    }

    private void createNotification() {
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, HomeActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);


        Notification builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle("This app is using gps")
                .setContentText("Please enable location for pud claim")
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .build();

        startForeground(1, builder);



    }


    private void requestLocationUpdates() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(300000);
        // locationRequest.setFastestInterval(300000);


        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        fn_update(wayLatitude, wayLongitude);
                     //   Toast.makeText(getApplicationContext(),"location"+ wayLatitude +"\n"+ wayLongitude,Toast.LENGTH_LONG).show();
                    }
                }
            }
        };


        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);

        }

    }

    private void fn_update(double latitude, double longitude) {

        db = new DatabaseHelper(this);
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
      /*  if (!(criticalogSharedPreferences.getData("start_lat") == null || criticalogSharedPreferences.getData("start_lat").equalsIgnoreCase(""))) {
            start_lat = Double.parseDouble((criticalogSharedPreferences.getData("start_lat")));
        }

        if (!(criticalogSharedPreferences.getData("start_long") == null || criticalogSharedPreferences.getData("start_long").equalsIgnoreCase(""))) {
            start_long = Double.parseDouble((criticalogSharedPreferences.getData("start_long")));
        }*/

        // currentLocationVolley = new CurrentLocationVolley(user_id, latitude, longitude);

        Date c1 = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c1);

        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = df3.format(c1);

        SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
        String time1 = df4.format(c1);
        Log.d("time =>", time1);

        ArrayList<PodClaimPojo> latlonglist = db.getAllLatLongList();



        if (latlonglist.size() > 0) {
            int listsize = latlonglist.size();
            PodClaimPojo podClaimPojo;
          /*  if (latlonglist.get(listsize - 1).getEdn_lat() == 0 || latlonglist.get(listsize - 1).getEnd_long() == 0 || latitude == 0 || longitude == 0) {
                podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "0", date1, time1, "0", "0", "hour based", c1.toString(), 10);
            } else {
                double distnce = distance(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "K");
                String convertedstring = converttorealnumber(distnce);
                podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring, date1, time1, "0", "0", "hour based", c1.toString(), 10);
            }*/

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                PodClaimPojo previousobj = latlonglist.get(listsize - 1);

                Date dtfetch = sdf.parse(previousobj.getDttime());
                long diff = c1.getTime() - dtfetch.getTime();
                long diffmin = diff / (60 * 1000);
                Log.d("diff ", "in time min  :" + diffmin);


                /*latlonglist.add(podClaimPojo);
                db.insertLatLOngDetails(podClaimPojo);*/


                if (latlonglist.size() > 0) {

                    double distnce = distance(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude,"K");
                    String convertedstring = converttorealnumber(distnce);

                    podClaimPojo = new PodClaimPojo( latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring,"", date1, time1, "service", "0", "", date1+" "+time1, 10);

                    Log.e("double_value", String.valueOf(distnce));

                    db.insertLatLOngDetails(podClaimPojo);
                    //dbHelper.insertLatLOngDetails(podClaimPojo);

                }else {
                    podClaimPojo = new PodClaimPojo( 0.0, 0.0, latitude,longitude, "0","0", date1, time1, "service", "0", "", date1+" "+time1, 10);
                    db.insertLatLOngDetails(podClaimPojo);
                }
              /*  if (!previousobj.getTasktype().equalsIgnoreCase("hour based")) {
                    if (diffmin > 5) {

                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                latlonglist.add(podClaimPojo);
                                db.insertLatLOngDetails(podClaimPojo);
                            }
                        });

                        t.start();
                    }

                } else {
                    if (diffmin > 25) {

                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                latlonglist.add(podClaimPojo);
                                db.insertLatLOngDetails(podClaimPojo);
                            }
                        });

                        t.start();
                    }

                }*/


            } catch (ParseException e) {
                e.printStackTrace();
            }


        } /*else {
            PodClaimPojo podClaimPojo;
            if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {
                podClaimPojo = new PodClaimPojo(start_lat, start_long, latitude, longitude, "0", date1, time1, "0", "0", "hour based",c1.toString());
            } else {
                double distnce = distance(start_lat, start_long, latitude, longitude, "K");
                String convertedstring = converttorealnumber(distnce);
                podClaimPojo = new PodClaimPojo(start_lat, start_long, latitude, longitude, convertedstring, date1, time1, "1", "0", "hour based",c1.toString());
            }

            latlonglist.add(podClaimPojo);
            db.insertLatLOngDetails(podClaimPojo);

        }*/

        Log.d("mylocation service", "list size" + latlonglist.size());
        // Toast.makeText(this,"list size"+ latlonglist.size(),Toast.LENGTH_LONG).show();


    }

    public String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist / 0.62137;

       /* String num = String.valueOf(dist);
        new BigDecimal(num).toPlainString();
        return new BigDecimal(num).toPlainString();*/

        if (unit.equals("K")) {
            dist = dist * 1.609344;
        } else if (unit.equals("N")) {
            dist = dist * 0.8684;
        }

        return dist;

    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
