package com.criticalog.ecritica.MVPLogin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity implements LoginContract.MainView {
    EditText userName;
    TextInputEditText password;
    TextView login, mErrorText;
    private SimpleArcDialog mProgressBar;
    LoginContract.presenter presenterLogin;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    String uniqueId;
    private int click = 1;
    String newToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        StaticUtils.BASE_URL_DYNAMIC=criticalogSharedPreferences.getData("base_url_dynamic");
        getFirebaseToken();

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        userName = findViewById(R.id.xtv_username);
        password = findViewById(R.id.xtv_password);
        login = findViewById(R.id.xlogin_button);
        mErrorText = findViewById(R.id.mErrorText);

        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;


                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                      /*  mpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        mpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0);*/
                        click = click + 1;
                        if (click % 2 == 0) {
                            password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0);
                        } else {
                            password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pwd_show, 0);
                        }

                        return true;
                    }
                }
                return false;
            }


        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (criticalogSharedPreferences.haveNetworkConnection() == true) {
                    // Toast.makeText(this, "Network Connected!", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Error")
                                .setMessage("Internet not available, Cross check your internet connectivity and try again later...")
                                .setCancelable(false)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).show();
                    } catch (Exception e) {
                        //    Log.d(Constants.TAG, "Show Dialog: " + e.getMessage());
                    }

                }
                String username = userName.getText().toString();
                String pass = password.getText().toString();


                if (TextUtils.isEmpty(username) || username.matches("")) {
                    //   userName.setError("Please enter the username");
                    Toasty.warning(LoginActivity.this, "Please enter the username", Toast.LENGTH_LONG, true).show();
                    mErrorText.setVisibility(View.VISIBLE);
                    mErrorText.setText("Please enter the username");
                    //  musername.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    //password.setError("Please enter the password");
                    Toasty.warning(LoginActivity.this, "Please enter the password", Toast.LENGTH_LONG, true).show();
                    // mpassword.requestFocus();
                    mErrorText.setVisibility(View.VISIBLE);
                    mErrorText.setText("Please enter the password");
                    return;
                }
                uniqueId = getUniquePsuedoID();

                criticalogSharedPreferences.saveData("deviceID", uniqueId);
                mErrorText.setVisibility(View.GONE);
                LoginPostData loginPostData = new LoginPostData();
                loginPostData.setUserId(username);
                loginPostData.setMobileId(uniqueId);
                loginPostData.setPassword(pass);
                loginPostData.setAction("login");
                loginPostData.setToken("1");
                loginPostData.setFirebase_token(newToken);
                Log.e("firebase_token", newToken);
                presenterLogin = new LoginPresenterImpl(LoginActivity.this, new GetLoginInteractorImpl());
                presenterLogin.onLoginButtonClick(loginPostData);

                /*if (newToken.equalsIgnoreCase("")) {
                    Toasty.warning(LoginActivity.this, "Firebase Token Not Captured!!", Toast.LENGTH_LONG, true).show();
                    finish();
                    startActivity(getIntent());
                } else {
                    LoginPostData loginPostData = new LoginPostData();
                    loginPostData.setUserId(username);
                    loginPostData.setMobileId(uniqueId);
                    loginPostData.setPassword(pass);
                    loginPostData.setAction("login");
                    loginPostData.setToken("1");
                    loginPostData.setFirebase_token(newToken);
                    Log.e("firebase_token", newToken);
                    presenterLogin = new LoginPresenterImpl(LoginActivity.this, new GetLoginInteractorImpl());
                    presenterLogin.onLoginButtonClick(loginPostData);
                }*/
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getFirebaseToken();
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setDataToViews(LoginResponseData loginResponseData) {

        if (loginResponseData.getStatus() == 200) {
            mErrorText.setVisibility(View.GONE);
            criticalogSharedPreferences.saveData("login_status", "true");
            criticalogSharedPreferences.saveData("userId", loginResponseData.getData().getDetails().getUserId());
            criticalogSharedPreferences.saveData("username", loginResponseData.getData().getDetails().getUsername());
            criticalogSharedPreferences.saveData("designation", loginResponseData.getData().getDetails().getDesignation());
            criticalogSharedPreferences.saveData("location", loginResponseData.getData().getDetails().getLocation());
            criticalogSharedPreferences.saveIntData("device_staus", loginResponseData.getStatus());
            criticalogSharedPreferences.saveData("device_message", loginResponseData.getMessage());
            criticalogSharedPreferences.saveData("token", loginResponseData.getData().getToken());

            criticalogSharedPreferences.saveData("XHUBID", loginResponseData.getData().getDetails().getXuhlhubid());
            criticalogSharedPreferences.saveData("LCCS", loginResponseData.getData().getDetails().getOtherModule().getLccs());
            criticalogSharedPreferences.saveData("is_rgsa", loginResponseData.getData().getDetails().getIsRgsa());

            criticalogSharedPreferences.saveData("prs_flag", loginResponseData.getData().getDetails().getOtherModule().getPrs());
            criticalogSharedPreferences.saveData("drs_flag", loginResponseData.getData().getDetails().getOtherModule().getDrs());
            criticalogSharedPreferences.saveData("inscan_flag", loginResponseData.getData().getDetails().getOtherModule().getInscan());
            criticalogSharedPreferences.saveData("bagging_flag", loginResponseData.getData().getDetails().getOtherModule().getBagging());
            criticalogSharedPreferences.saveData("linehaul_flag", loginResponseData.getData().getDetails().getOtherModule().getLccs());
            criticalogSharedPreferences.saveData("sector_flag", loginResponseData.getData().getDetails().getOtherModule().getSectorPickup());
            criticalogSharedPreferences.saveData("edp_sdc_flag", loginResponseData.getData().getDetails().getOtherModule().getEdpSdc());
            criticalogSharedPreferences.saveData("negative_flag", loginResponseData.getData().getDetails().getOtherModule().getNegativeStatus());
            criticalogSharedPreferences.saveData("pickupconfirm_flag", loginResponseData.getData().getDetails().getOtherModule().getPickupConfirm());
            criticalogSharedPreferences.saveData("punch_flag", loginResponseData.getData().getDetails().getOtherModule().getPunchInOut());
            criticalogSharedPreferences.saveData("tatcheck_flag", loginResponseData.getData().getDetails().getOtherModule().getTatcheck());
            criticalogSharedPreferences.saveData("conenquiry_flag", loginResponseData.getData().getDetails().getOtherModule().getConnoteEnquiry());
            criticalogSharedPreferences.saveData("pod", loginResponseData.getData().getDetails().getOtherModule().getPOD());
            criticalogSharedPreferences.saveData("new_inscan",loginResponseData.getData().getDetails().getOtherModule().getNEW_INSCAN());

            criticalogSharedPreferences.saveData("ROUND_CREATION",loginResponseData.getData().getDetails().getOtherModule().getROUND_CREATION());
            criticalogSharedPreferences.saveData("DRS_PREPARATION",loginResponseData.getData().getDetails().getOtherModule().getDRS_PREPARATION());
            criticalogSharedPreferences.saveData("ADDITIONAL_CLAIM",loginResponseData.getData().getDetails().getOtherModule().getADDITIONAL_CLAIM());

            criticalogSharedPreferences.saveData("SEND_MAIL",loginResponseData.getData().getDetails().getOtherModule().getSEND_MAIL());
            criticalogSharedPreferences.saveData("URGENT_SHIPMENT",loginResponseData.getData().getDetails().getOtherModule().getURGENT_SHIPMENT());

            StaticUtils.TOKEN = loginResponseData.getData().getToken();

            // Toast.makeText(this, loginResponseData.getData().getDetails().getDesignation(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            //Toast.makeText(getApplication(), loginResponseData.getMessage(), Toast.LENGTH_SHORT).show();
            Toasty.error(LoginActivity.this, loginResponseData.getMessage(), Toast.LENGTH_LONG, true).show();
            mErrorText.setVisibility(View.VISIBLE);
            mErrorText.setText(loginResponseData.getMessage());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        //   Toast.makeText(this, "Cant Login in Multiple Device", Toast.LENGTH_SHORT).show();
        mErrorText.setVisibility(View.VISIBLE);
        mErrorText.setText(R.string.re_try);
    }

    public void getFirebaseToken() {

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            newToken = instanceIdResult.getToken();
            // Log.e("newToken", newToken);
            //  getActivity().getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });

    }

    public static String getUniquePsuedoID() {
        // If all else fails, if the user does have lower than API 9 (lower
        // than Gingerbread), has reset their device or 'Secure.ANDROID_ID'
        // returns 'null', then simply the ID returned will be solely based
        // off their Android device information. This is where the collisions
        // can happen.
        // Thanks http://www.pocketmagic.net/?p=1662!
        // Try not to use DISPLAY, HOST or ID - these items could change.
        // If there are collisions, there will be overlapping data
        String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);
        // Thanks to @Roman SL!
        // https://stackoverflow.com/a/4789483/950427
        // Only devices with API >= 9 have android.os.Build.SERIAL
        // http://developer.android.com/reference/android/os/Build.html#SERIAL
        // If a user upgrades software or roots their device, there will be a duplicate entry
        String serial = null;
        try {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();
            // Go ahead and return the serial for api => 9
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            // String needs to be initialized
            serial = "serial"; // some value
        }
        // Thanks @Joe!
        // https://stackoverflow.com/a/2853253/950427
        // Finally, combine the values we have found by using the UUID class to create a unique identifier
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }
}
