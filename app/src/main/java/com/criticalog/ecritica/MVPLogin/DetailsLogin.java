package com.criticalog.ecritica.MVPLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DetailsLogin implements Serializable {
    @SerializedName("mobile_id")
    @Expose
    private String mobileId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("MUSRPASSWD")
    @Expose
    private String musrpasswd;
    @SerializedName("default_hub")
    @Expose
    private String defaultHub;
    @SerializedName("XUHLHUBID")
    @Expose
    private String xuhlhubid;
    @SerializedName("DEPARTMENT")
    @Expose
    private String department;
    @SerializedName("other_module")
    @Expose
    private OtherModule otherModule;

    @SerializedName("is_rgsa")
    @Expose
    private String isRgsa;

    public String getIsRgsa() {
        return isRgsa;
    }

    public void setIsRgsa(String isRgsa) {
        this.isRgsa = isRgsa;
    }

    public String getMobileId() {
        return mobileId;
    }

    public void setMobileId(String mobileId) {
        this.mobileId = mobileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMusrpasswd() {
        return musrpasswd;
    }

    public void setMusrpasswd(String musrpasswd) {
        this.musrpasswd = musrpasswd;
    }

    public String getDefaultHub() {
        return defaultHub;
    }

    public void setDefaultHub(String defaultHub) {
        this.defaultHub = defaultHub;
    }

    public String getXuhlhubid() {
        return xuhlhubid;
    }

    public void setXuhlhubid(String xuhlhubid) {
        this.xuhlhubid = xuhlhubid;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public OtherModule getOtherModule() {
        return otherModule;
    }

    public void setOtherModule(OtherModule otherModule) {
        this.otherModule = otherModule;
    }
  /*  @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("location")
    @Expose
    private String location;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }*/
}
