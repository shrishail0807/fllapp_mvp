package com.criticalog.ecritica.MVPLogin;


public interface LoginContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onLoginButtonClick(LoginPostData loginPostData);

        //void LoginRequestToServer(LoginPostData loginPostData);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToViews(LoginResponseData loginResponseData);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetLoginIntractor {

        interface OnFinishedListener {
            void onFinished(LoginResponseData loginResponseData);
            void onFailure(Throwable t);
        }
        void loginSuccessful(OnFinishedListener onFinishedListener, LoginPostData loginPostData);
    }
}
