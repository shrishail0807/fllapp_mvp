package com.criticalog.ecritica.MVPLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtherModule implements Serializable {
    @SerializedName("LCCS")
    @Expose
    private String lccs;
    @SerializedName("PRS")
    @Expose
    private String prs;
    @SerializedName("DRS")
    @Expose
    private String drs;
    @SerializedName("INSCAN")
    @Expose
    private String inscan;
    @SerializedName("BAGGING")
    @Expose
    private String bagging;
    @SerializedName("SECTOR_PICKUP")
    @Expose
    private String sectorPickup;
    @SerializedName("EDP_SDC")
    @Expose
    private String edpSdc;
    @SerializedName("NEGATIVE_STATUS")
    @Expose
    private String negativeStatus;
    @SerializedName("PICKUP_CONFIRM")
    @Expose
    private String pickupConfirm;
    @SerializedName("PUNCH_IN_OUT")
    @Expose
    private String punchInOut;
    @SerializedName("TATCHECK")
    @Expose
    private String tatcheck;
    @SerializedName("CONNOTE_ENQUIRY")
    @Expose
    private String connoteEnquiry;

    @SerializedName("DRS_PREPARATION")
    @Expose
    private String DRS_PREPARATION;

    @SerializedName("ROUND_CREATION")
    @Expose
    private String ROUND_CREATION;

    @SerializedName("ADDITIONAL_CLAIM")
    @Expose
    private String ADDITIONAL_CLAIM;

    @SerializedName("SEND_MAIL")
    @Expose
    private String SEND_MAIL;

    @SerializedName("URGENT_SHIPMENT")
    @Expose
    private String URGENT_SHIPMENT;

    public String getSEND_MAIL() {
        return SEND_MAIL;
    }

    public void setSEND_MAIL(String SEND_MAIL) {
        this.SEND_MAIL = SEND_MAIL;
    }

    public String getURGENT_SHIPMENT() {
        return URGENT_SHIPMENT;
    }

    public void setURGENT_SHIPMENT(String URGENT_SHIPMENT) {
        this.URGENT_SHIPMENT = URGENT_SHIPMENT;
    }

    public String getADDITIONAL_CLAIM() {
        return ADDITIONAL_CLAIM;
    }

    public void setADDITIONAL_CLAIM(String ADDITIONAL_CLAIM) {
        this.ADDITIONAL_CLAIM = ADDITIONAL_CLAIM;
    }

    public String getDRS_PREPARATION() {
        return DRS_PREPARATION;
    }

    public void setDRS_PREPARATION(String DRS_PREPARATION) {
        this.DRS_PREPARATION = DRS_PREPARATION;
    }

    public String getROUND_CREATION() {
        return ROUND_CREATION;
    }

    public void setROUND_CREATION(String ROUND_CREATION) {
        this.ROUND_CREATION = ROUND_CREATION;
    }

    public String getNEW_INSCAN() {
        return NEW_INSCAN;
    }

    public void setNEW_INSCAN(String NEW_INSCAN) {
        this.NEW_INSCAN = NEW_INSCAN;
    }

    @SerializedName("INSCAN_NEW")
    @Expose
    private String NEW_INSCAN;

    public String getPOD() {
        return POD;
    }

    public void setPOD(String POD) {
        this.POD = POD;
    }

    @SerializedName("POD")
    @Expose
    private String POD;

    public String getLccs() {
        return lccs;
    }

    public void setLccs(String lccs) {
        this.lccs = lccs;
    }

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }

    public String getDrs() {
        return drs;
    }

    public void setDrs(String drs) {
        this.drs = drs;
    }

    public String getInscan() {
        return inscan;
    }

    public void setInscan(String inscan) {
        this.inscan = inscan;
    }

    public String getBagging() {
        return bagging;
    }

    public void setBagging(String bagging) {
        this.bagging = bagging;
    }

    public String getSectorPickup() {
        return sectorPickup;
    }

    public void setSectorPickup(String sectorPickup) {
        this.sectorPickup = sectorPickup;
    }

    public String getEdpSdc() {
        return edpSdc;
    }

    public void setEdpSdc(String edpSdc) {
        this.edpSdc = edpSdc;
    }

    public String getNegativeStatus() {
        return negativeStatus;
    }

    public void setNegativeStatus(String negativeStatus) {
        this.negativeStatus = negativeStatus;
    }

    public String getPickupConfirm() {
        return pickupConfirm;
    }

    public void setPickupConfirm(String pickupConfirm) {
        this.pickupConfirm = pickupConfirm;
    }

    public String getPunchInOut() {
        return punchInOut;
    }

    public void setPunchInOut(String punchInOut) {
        this.punchInOut = punchInOut;
    }

    public String getTatcheck() {
        return tatcheck;
    }

    public void setTatcheck(String tatcheck) {
        this.tatcheck = tatcheck;
    }

    public String getConnoteEnquiry() {
        return connoteEnquiry;
    }

    public void setConnoteEnquiry(String connoteEnquiry) {
        this.connoteEnquiry = connoteEnquiry;
    }
}
