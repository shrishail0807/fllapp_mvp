package com.criticalog.ecritica.MVPLogin;

import android.util.Log;

import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetLoginInteractorImpl implements LoginContract.GetLoginIntractor {

    @Override
    public void loginSuccessful(OnFinishedListener onFinishedListener, LoginPostData loginPostData) {
        /** Create handle for the RetrofitInstance interface*/
        RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

        /** Call the method with parameter in the interface to get the notice data*/
        Call<LoginResponseData> call = service.login("0",loginPostData);

        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<LoginResponseData>() {
            @Override
            public void onResponse(Call<LoginResponseData> call, Response<LoginResponseData> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<LoginResponseData> call, Throwable t) {
                onFinishedListener.onFailure(t);


            }
        });
    }
}
