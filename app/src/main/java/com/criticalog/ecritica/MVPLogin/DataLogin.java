package com.criticalog.ecritica.MVPLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataLogin implements Serializable {
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("details")
    @Expose
    private DetailsLogin details;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DetailsLogin getDetails() {
        return details;
    }

    public void setDetails(DetailsLogin details) {
        this.details = details;
    }
}
