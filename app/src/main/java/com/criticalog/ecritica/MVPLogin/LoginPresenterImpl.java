package com.criticalog.ecritica.MVPLogin;

public class LoginPresenterImpl implements LoginContract.presenter, LoginContract.GetLoginIntractor.OnFinishedListener{
    private LoginContract.MainView mainView;
    private LoginContract.GetLoginIntractor getNoticeIntractor;

    public LoginPresenterImpl(LoginContract.MainView mainView, LoginContract.GetLoginIntractor getNoticeIntractor) {
        this.mainView = mainView;
        this.getNoticeIntractor = getNoticeIntractor;
    }
    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void onLoginButtonClick(LoginPostData loginPostData) {
        if(mainView != null){
            mainView.showProgress();
        }
        getNoticeIntractor.loginSuccessful(this, loginPostData);
    }

    @Override
    public void onFinished(LoginResponseData loginResponseData) {
        if(mainView != null){
            mainView.setDataToViews(loginResponseData);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
