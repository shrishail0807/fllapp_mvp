package com.criticalog.ecritica;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.Activities.ZoomImageActivity;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PODimageFragment extends Fragment {


    String podimage;
    View view;
    ImageView podimageview;
    public PODimageFragment(String podimage1) {
        this.podimage = podimage1;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_podimage, container, false);
        podimageview = view.findViewById(R.id.podimage);
        TextView text = view.findViewById(R.id.text);
        String url = podimage;
        if (podimage !=null) {
            Glide.with(getActivity()).load(url).centerCrop().placeholder(R.drawable.brokenimage).into(podimageview);

            podimageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(getActivity(), ZoomImageActivity.class);
                    intent.putExtra("imageurl",url);
                    startActivity(intent);


                }
            });
        }else {
            text.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load("").centerCrop().placeholder(R.drawable.brokenimage).into(podimageview);
        }



        Log.d("image url",url);


        return view;
    }

}
