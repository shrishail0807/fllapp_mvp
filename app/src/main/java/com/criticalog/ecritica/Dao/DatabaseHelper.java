package com.criticalog.ecritica.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.criticalog.ecritica.MVPHomeScreen.Model.Detail;
import com.criticalog.ecritica.ModelClasses.CancelTask;
import com.criticalog.ecritica.ModelClasses.DeliveryDetails;
import com.criticalog.ecritica.ModelClasses.PickupSuccess;
import com.criticalog.ecritica.Utils.PodClaimPojo;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    //private static final String DATABASE_NAME="eCriticaDelivery";
    private static final String DATABASE_NAME = "eCriticaDatabase";
    private static final int DATABASE_VERSION = 28;

    //You will declare all your table names here.
    private static final String DELIVERY_DETAILS_TABLE = "DeliveryDetails";
    private static final String CANCEL_DETAILS_TABLE = "Cancel";
    private static final String PICKUP_SUCCESS_TABLE = "PickupSuccess";
    private static final String LAT_LONG_TABLE = "LatLongDetails";
    private static final String SAVE_LAST_LAT_LONG = "SaveLastLatLong";
    private static final String RIDER_LOG = "Riderlog";

    private static final String QC_STATUS_TABLE = "Qcstatustable";
    private static final String QC_STATUS_TABLE_POSITION = "Qcstatustablepostion";
    private static final String QC_STATUS_TABLE_STATUS = "Qcstatustablestatus";

    // Students Table Columns names
    private static final String KEY_DELIVERY_ID = "userId";
    private static final String KEY_DELIVERY_USER_ID = "userId";
    private static final String KEY_DELIVERY_TASKID = "taskId";
    private static final String KEY_DELIVERY_RECIVERNAME = "recieverName";
    private static final String KEY_DELIVERY_STATUS = "status";
    private static final String KEY_DELIVERY_REFCOUNT = "refCount";
    private static final String KEY_DELIVERY_BULKFLAG = "bulkFlag";
    private static final String KEY_DELIVERY_DATE = "date";
    private static final String KEY_DELIVERY_TIME = "time";
    private static final String KEY_DELIVERY_FILE_ = "file";
    private static final String KEY_DELIVERY_FILE1_ = "file1";
    private static final String KEY_DELIVERY_PODFILE_ = "podfile";
    private static final String KEY_DELIVERY_LATITUDE = "latitude";
    private static final String KEY_DELIVERY_LONGITUDE = "longitude";
    private static final String KEY_DELIVERY_SWAPDETAILS = "swap";
    private static final String KEY_DELIVERY_CHEQUE = "chequeno";
    private static final String KEY_DELIVERY_AMOUNT = "amount";
    private static final String KEY_COD_TASKID = "codtaskid";
    private static final String KEY_DELIVERY_MODEOFPAY = "modeofpay";
    private static final String KEY_DELIVERY_CODAMOUNT = "codamount";
    private static final String KEY_DELIVERY_codpaytype = "codpaytype";
    private static final String KEY_DELIVERY_upirefno = "upirefno";
    private static final String KEY_DELIVERY_DATETIME = "datetime";


    //
    private static final String KEY_CANCEL_TASKID = "taskId";
    private static final String KEY_CANCEL_USER_ID = "userId";
    private static final String KEY_CANCEL_REFCOUNT = "refCount";
    private static final String KEY_CANCEL_BULKFLAG = "bulkFlag";
    private static final String KEY_CANCEL_REFNUMBER = "refNumber";
    private static final String KEY_CANCEL_DATE = "date";
    private static final String KEY_CANCEL_TIME = "time";
    private static final String KEY_CANCEL_REASON_ = "reason";
    private static final String KEY_CANCEL_LATITUDE_ = "latitude";
    private static final String KEY_CANCEL_LONGITUDE_ = "longitude";

    private static final String KEY_LATLONG_STARTLAT = "startlat";
    private static final String KEY_LATLONG_STARTLONG = "startlong";
    private static final String KEY_LATLONG_ENDLAT = "endlat";
    private static final String KEY_LATLONG_ENDLONG = "endlong";
    private static final String KEY_LATLONG_DISTANCE = "distance";
    private static final String KEY_LATLONG_DISTANCE_G = "distance_g";
    private static final String KEY_LATLONG_DATE = "date";
    private static final String KEY_LATLONG_TIME = "time";
    private static final String KEY_LATLONG_REMARK_ = "remark";
    private static final String KEY_LATLONG_TASKID = "taskid";
    private static final String KEY_LATLONG_TASKTYPE = "tsaktype";
    private static final String KEY_LATLONG_DATETIME = "datetime";
    private static final String KEY_LATLONG_RESPONSECODE = "responsecode";


    private static final String KEY_PICKUP_DOCKETNO = "docketNo";
    private static final String KEY_PICKUP_USERID = "userId";
    private static final String KEY_PICKUP_CLIENTCODE = "clientCode";
    private static final String KEY_PICKUP_DATE = "date";
    private static final String KEY_PICKUP_TIME = "time";
    private static final String KEY_PICKUP_PRS = "prs";
    private static final String KEY_PICKUP_BOX = "box";


    private static final String DELIVERYDETAILS_TABLE = "create table if not exists " + DELIVERY_DETAILS_TABLE + "(taskId TEXT primary key,userId TEXT,recieverName TEXT,status TEXT,refCount TEXT,bulkFlag TEXT,date TEXT,time TEXT,file BLOB,file1 BLOB,podfile BLOB,latitude TEXT,longitude TEXT,swap TEXT,chequeno TEXT,amount TEXT,codtaskid TEXT,modeofpay TEXT,codamount TEXT,codpaytype TEXT,upirefno TEXT,datetime TEXT)";

    private static final String CANCEL_TABLE = "create table if not exists " + CANCEL_DETAILS_TABLE + "(taskId TEXT primary key,userId TEXT,refCount TEXT,bulkFlag TEXT,refNumber TEXT,date TEXT,time TEXT,reason TEXT,latitude TEXT,longitude TEXT,cheque TEXT,amount TEXT)";

    private static final String LATLONG_TABLE = "create table if not exists " + LAT_LONG_TABLE + "(startlat TEXT,startlong TEXT,endlat TEXT,endlong TEXT,distance TEXT,distance_g TEXT,date TEXT,time TEXT,remark TEXT,taskid TEXT,tsaktype TEXT,datetime TEXT,responsecode TEXT)";

    private static final String LATLONG_TABLE_LAST = "create table if not exists " + SAVE_LAST_LAT_LONG + "(userId TEXT primary key,startlat TEXT,startlong TEXT)";

    private static final String PICKUP_TABLE = "create table if not exists " + PICKUP_SUCCESS_TABLE + "(docketNo TEXT primary key,userId TEXT,clientCode TEXT,box TEXT,prs TEXT)";

    private static final String QCSTATUS_TABLE = "create table if not exists " + QC_STATUS_TABLE + "(position TEXT,status TEXT)";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {

            db.execSQL(DELIVERYDETAILS_TABLE);
            db.execSQL(CANCEL_TABLE);
            db.execSQL(PICKUP_TABLE);
            db.execSQL(LATLONG_TABLE);
            db.execSQL(LATLONG_TABLE_LAST);
            db.execSQL(QCSTATUS_TABLE);

            Log.e("SHRIHAIL1", "Table created");
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("SHRIHAIL2", e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //This method will be called only if there is change in DATABASE_VERSION.

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DELIVERY_DETAILS_TABLE);

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + CANCEL_DETAILS_TABLE);


        db.execSQL("DROP TABLE IF EXISTS " + PICKUP_SUCCESS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + LAT_LONG_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SAVE_LAST_LAT_LONG);
        db.execSQL("DROP TABLE IF EXISTS " + QC_STATUS_TABLE);



       /* if (newVersion > oldVersion) {
            db.execSQL("ALTER TABLE CANCEL_DETAILS_TABLE ADD COLUMN KEY_CANCEL_REFNUMBER TEXT ");
        }*/
        // Create tables again
        onCreate(db);
    }


    // Inser last lat long details
    public void insertLastLatLong(String id, String lastLat, String lastLong) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //Content values use KEY-VALUE pair concept
            ContentValues values = new ContentValues();
            values.put(KEY_PICKUP_USERID, id);
            values.put(KEY_LATLONG_STARTLAT, lastLat);
            values.put(KEY_LATLONG_STARTLONG, lastLong);

            db.insert(SAVE_LAST_LAT_LONG, null, values);
            //  db.replace(SAVE_LAST_LAT_LONG, null, values);
            db.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Get Last lat long details
    public String getLastLatLongDetails() {

        String lastLatLong = "";
        String lat = "", llong = "";


        try {

            String query = "select * from " + SAVE_LAST_LAT_LONG;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    lat = cursor.getString(1);
                    llong = cursor.getString(2);

                } while (cursor.moveToNext());
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return lat + "-" + llong;
    }

    // Add Delivery Details
    public void insertDelliveryDetails(DeliveryDetails deliveryDetails) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            Log.d("Database helper", "" + deliveryDetails.getFileBytesPod().toString());
            //Content values use KEY-VALUE pair concept
            ContentValues values = new ContentValues();
            values.put(KEY_DELIVERY_TASKID, deliveryDetails.getTaskId());
            values.put(KEY_DELIVERY_USER_ID, deliveryDetails.getUserId());
            values.put(KEY_DELIVERY_RECIVERNAME, deliveryDetails.getReceiverName());
            values.put(KEY_DELIVERY_STATUS, deliveryDetails.getStatus());
            values.put(KEY_DELIVERY_REFCOUNT, deliveryDetails.getRefCount());
            values.put(KEY_DELIVERY_BULKFLAG, deliveryDetails.getBulkFlag());
            values.put(KEY_DELIVERY_DATE, deliveryDetails.getDate());
            values.put(KEY_DELIVERY_TIME, deliveryDetails.getTime());
            values.put(KEY_DELIVERY_FILE_, deliveryDetails.getFileBytes());
            values.put(KEY_DELIVERY_FILE1_, deliveryDetails.getFileBytes1());
            values.put(KEY_DELIVERY_PODFILE_, deliveryDetails.getFileBytesPod());
            values.put(KEY_DELIVERY_LATITUDE, deliveryDetails.getLatitude());
            values.put(KEY_DELIVERY_LONGITUDE, deliveryDetails.getLongitude());
            values.put(KEY_DELIVERY_SWAPDETAILS, deliveryDetails.getSwap());
            values.put(KEY_DELIVERY_CHEQUE, deliveryDetails.getCheque());
            values.put(KEY_DELIVERY_AMOUNT, deliveryDetails.getAmount());
            values.put(KEY_COD_TASKID, deliveryDetails.getCodtaskId());
            values.put(KEY_DELIVERY_MODEOFPAY, deliveryDetails.getModeofpay());
            values.put(KEY_DELIVERY_CODAMOUNT, deliveryDetails.getCodamount());
            values.put(KEY_DELIVERY_codpaytype, deliveryDetails.getCodpaytype());
            values.put(KEY_DELIVERY_upirefno, deliveryDetails.getUpirefno());
            values.put(KEY_DELIVERY_DATETIME, deliveryDetails.getDttime());

            db.insert(DELIVERY_DETAILS_TABLE, null, values);
            db.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableData() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(LAT_LONG_TABLE, null, null);

    }

    public void clearLastLatLongTableData() {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.delete(SAVE_LAST_LAT_LONG, null, null);
        } catch (Exception e) {

        }
    }


    public void insertLatLOngDetails(PodClaimPojo deliveryDetails) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //Content values use KEY-VALUE pair concept
            ContentValues values = new ContentValues();
            values.put(KEY_LATLONG_STARTLAT, deliveryDetails.getStart_lat());
            values.put(KEY_LATLONG_STARTLONG, deliveryDetails.getStart_long());
            values.put(KEY_LATLONG_ENDLAT, deliveryDetails.getEdn_lat());
            values.put(KEY_LATLONG_ENDLONG, deliveryDetails.getEnd_long());
            values.put(KEY_LATLONG_DISTANCE, deliveryDetails.getDistance());
            values.put(KEY_LATLONG_DISTANCE_G, deliveryDetails.getDistance_g());
            values.put(KEY_LATLONG_DATE, deliveryDetails.getDate());
            values.put(KEY_LATLONG_TIME, deliveryDetails.getTime());
            values.put(KEY_LATLONG_REMARK_, deliveryDetails.getRemark());
            values.put(KEY_LATLONG_TASKID, deliveryDetails.getTaskid());
            values.put(KEY_LATLONG_TASKTYPE, deliveryDetails.getTasktype());
            values.put(KEY_LATLONG_DATETIME, deliveryDetails.getDttime());
            values.put(KEY_LATLONG_RESPONSECODE, deliveryDetails.getResponsecode());


            db.insert(LAT_LONG_TABLE, null, values);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insertLatLOngDetailsNew(Detail deliveryDetails) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            //Content values use KEY-VALUE pair concept
            ContentValues values = new ContentValues();
            values.put(KEY_LATLONG_STARTLAT, deliveryDetails.getStartLat());
            values.put(KEY_LATLONG_STARTLONG, deliveryDetails.getStartLong());
            values.put(KEY_LATLONG_ENDLAT, deliveryDetails.getEndLat());
            values.put(KEY_LATLONG_ENDLONG, deliveryDetails.getEndLong());
            values.put(KEY_LATLONG_DISTANCE, deliveryDetails.getDistance());
            values.put(KEY_LATLONG_DATE, deliveryDetails.getDate());
            values.put(KEY_LATLONG_TIME, deliveryDetails.getTime());
            values.put(KEY_LATLONG_REMARK_, deliveryDetails.getRemark());
            values.put(KEY_LATLONG_TASKID, deliveryDetails.getTaskid());
            values.put(KEY_LATLONG_TASKTYPE, deliveryDetails.getTasktype());
            values.put(KEY_LATLONG_DATETIME, deliveryDetails.getTime());
            values.put(KEY_LATLONG_RESPONSECODE, deliveryDetails.getStatuscode());

            db.insert(LAT_LONG_TABLE, null, values);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Add Delivery Details
    public void insertCancelDetails(CancelTask cancelTask) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            Log.d("dbhelper", "inside cancel");
            //Content values use KEY-VALUE pair concept
            ContentValues values = new ContentValues();
            values.put(KEY_CANCEL_TASKID, cancelTask.getTaskId());
            values.put(KEY_CANCEL_USER_ID, cancelTask.getUserId());
            values.put(KEY_CANCEL_REFCOUNT, cancelTask.getRefCount());
            values.put(KEY_CANCEL_BULKFLAG, cancelTask.getBulkFlag());
            values.put(KEY_CANCEL_REFNUMBER, cancelTask.getRefNumber());
            values.put(KEY_CANCEL_DATE, cancelTask.getDate());
            values.put(KEY_CANCEL_TIME, cancelTask.getTime());
            values.put(KEY_CANCEL_REASON_, cancelTask.getReason());
            values.put(KEY_CANCEL_LATITUDE_, cancelTask.getLatitude());
            values.put(KEY_CANCEL_LONGITUDE_, cancelTask.getLongitude());


            Log.d("dbhelper", "collected data");
            db.insert(CANCEL_DETAILS_TABLE, null, values);
            db.close();

            Toast.makeText(context, "data inserted", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Add PICKUP Success Details

    public void insertPickUpSuccessDetails(PickupSuccess pickupSussess) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_PICKUP_USERID, pickupSussess.getUserId());
            values.put(KEY_PICKUP_DOCKETNO, pickupSussess.getDocketNo());
            values.put(KEY_PICKUP_CLIENTCODE, pickupSussess.getClientCode());
            values.put(KEY_PICKUP_BOX, pickupSussess.getBoxNo());
           /* values.put(KEY_PICKUP_DATE, pickupSussess.getDate());
            values.put(KEY_PICKUP_TIME, pickupSussess.getTime());*/
            values.put(KEY_PICKUP_PRS, pickupSussess.getPrs());

            db.insert(PICKUP_SUCCESS_TABLE, null, values);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Getting single student details through ID
    public void deleteProductDetails(String taskid) {
        Log.d("taskid", "inside db" + taskid);
        SQLiteDatabase db = this.getReadableDatabase();
        //  db.execSQL("DELETE FROM "+DELIVERY_DETAILS_TABLE+" WHERE "+KEY_DELIVERY_TASKID+"= '" + taskid+ "'");
        Log.d("DELETE FROM ", DELIVERY_DETAILS_TABLE + " WHERE " + KEY_DELIVERY_TASKID + "= '" + taskid + "'");
        db.execSQL("DELETE FROM " + DELIVERY_DETAILS_TABLE + " WHERE " + KEY_DELIVERY_TASKID + "='" + taskid + "'");
        db.close();

    }

    // Getting All Pickup Details
    public List<PickupSuccess> getAllPickUpDetails() {
        List<PickupSuccess> pickupSuccessesList = null;
        //String query = "select * from "+PICKUP_SUCCESS_TABLE;

        String query = "select * from " + PICKUP_SUCCESS_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        try {
            pickupSuccessesList = new ArrayList<>();
            if (cursor.moveToFirst())
                do {
                    PickupSuccess pickupSuccess = new PickupSuccess();
                    pickupSuccess.setDocketNo(cursor.getString(0));
                    pickupSuccess.setUserId(cursor.getString(1));
                    pickupSuccess.setClientCode(cursor.getString(2));
                    pickupSuccess.setBoxNo(cursor.getString(3));
              /*  pickupSuccess.setDate(cursor.getString(3));
                pickupSuccess.setTime(cursor.getString(4));*/
                    pickupSuccess.setPrs(cursor.getString(4));

                    pickupSuccessesList.add(pickupSuccess);
                } while (cursor.moveToNext());
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return pickupSuccessesList;
    }


    // Getting All Students
    public List<DeliveryDetails> getAllTasks() {

        List<DeliveryDetails> taskList = null;
        try {
            taskList = new ArrayList<>();

            // Select All Query
            // String selectQuery = "SELECT  * FROM " + DELIVERY_DETAILS_TABLE;

            //SQLiteDatabase db = this.getWritableDatabase();
            //Cursor cursor = db.rawQuery(selectQuery, null);
            String query = "select * from " + DELIVERY_DETAILS_TABLE;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    DeliveryDetails deliveryDetails = new DeliveryDetails();
                    deliveryDetails.setTaskId(cursor.getString(0));
                    deliveryDetails.setUserId(cursor.getString(1));
                    deliveryDetails.setReceiverName(cursor.getString(2));
                    deliveryDetails.setStatus(cursor.getString(3));
                    deliveryDetails.setRefCount(cursor.getString(4));
                    deliveryDetails.setBulkFlag(cursor.getString(5));
                    deliveryDetails.setDate(cursor.getString(6));
                    deliveryDetails.setTime(cursor.getString(7));
                    deliveryDetails.setFileBytes(cursor.getBlob(8));
                    deliveryDetails.setFileBytes1(cursor.getBlob(9));
                    deliveryDetails.setFileBytesPod(cursor.getBlob(10));
                    deliveryDetails.setLatitude(cursor.getDouble(11));
                    deliveryDetails.setLongitude(cursor.getDouble(12));
                    deliveryDetails.setSwap(cursor.getString(13));
                    deliveryDetails.setCheque(cursor.getString(14));
                    deliveryDetails.setAmount(cursor.getString(15));
                    deliveryDetails.setCodtaskId(cursor.getString(16));
                    deliveryDetails.setModeofpay(cursor.getString(17));
                    deliveryDetails.setCodamount(cursor.getString(18));
                    deliveryDetails.setCodpaytype(cursor.getString(19));
                    deliveryDetails.setUpirefno(cursor.getString(20));
                    deliveryDetails.setDttime(cursor.getString(21));

                    taskList.add(deliveryDetails);
                } while (cursor.moveToNext());
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // return student list
        return taskList;
    }


    //Getting all latlong details

    public ArrayList<PodClaimPojo> getAllLatLongList() {

        ArrayList<PodClaimPojo> taskList = null;
        try {
            taskList = new ArrayList<>();

            // Select All Query
            // String selectQuery = "SELECT  * FROM " + DELIVERY_DETAILS_TABLE;

            //SQLiteDatabase db = this.getWritableDatabase();
            //Cursor cursor = db.rawQuery(selectQuery, null);
            String query = "select * from " + LAT_LONG_TABLE;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    PodClaimPojo deliveryDetails = new PodClaimPojo();
                    deliveryDetails.setStart_lat(cursor.getFloat(0));
                    deliveryDetails.setStart_long(cursor.getFloat(1));
                    deliveryDetails.setEdn_lat(cursor.getFloat(2));
                    deliveryDetails.setEnd_long(cursor.getFloat(3));
                    deliveryDetails.setDistance(cursor.getString(4));
                    deliveryDetails.setDate(cursor.getString(5));
                    deliveryDetails.setTime(cursor.getString(6));
                    deliveryDetails.setRemark(cursor.getString(7));
                    deliveryDetails.setTaskid(cursor.getString(8));
                    deliveryDetails.setTasktype(cursor.getString(9));
                    deliveryDetails.setDttime(cursor.getString(10));
                    deliveryDetails.setResponsecode(cursor.getInt(11));


                    taskList.add(deliveryDetails);
                } while (cursor.moveToNext());
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // return student list
        return taskList;
    }


    public ArrayList<Detail> getAllLatLongListNew() {

        ArrayList<Detail> taskList = null;
        try {
            taskList = new ArrayList<>();

            // Select All Query
            // String selectQuery = "SELECT  * FROM " + DELIVERY_DETAILS_TABLE;

            //SQLiteDatabase db = this.getWritableDatabase();
            //Cursor cursor = db.rawQuery(selectQuery, null);
            String query = "select * from " + LAT_LONG_TABLE;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Detail deliveryDetails = new Detail();
                    deliveryDetails.setStartLat(Double.valueOf(cursor.getFloat(0)));
                    deliveryDetails.setStartLong(Double.valueOf(cursor.getFloat(1)));
                    deliveryDetails.setEndLat(Double.valueOf(cursor.getFloat(2)));
                    deliveryDetails.setEndLong(Double.valueOf(cursor.getFloat(3)));
                    deliveryDetails.setDistance(cursor.getString(4));
                    deliveryDetails.setDistance_g(cursor.getString(5));
                    deliveryDetails.setDate(cursor.getString(6));
                    deliveryDetails.setTime(cursor.getString(7));
                    deliveryDetails.setRemark(cursor.getString(8));
                    deliveryDetails.setTaskid(cursor.getString(9));
                    deliveryDetails.setTasktype(cursor.getString(10));
                    deliveryDetails.setTime(cursor.getString(11));
                    deliveryDetails.setStatuscode(cursor.getInt(12));


                    taskList.add(deliveryDetails);
                } while (cursor.moveToNext());
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // return student list
        return taskList;
    }


    // Getting All Students
    public List<CancelTask> getCancelTasks() {

        List<CancelTask> taskList = null;
        try {
            taskList = new ArrayList<>();

            String query = "select * from " + CANCEL_DETAILS_TABLE;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    CancelTask cancelTask = new CancelTask();
                    cancelTask.setTaskId(cursor.getString(0));
                    cancelTask.setUserId(cursor.getString(1));
                    cancelTask.setRefCount(cursor.getString(2));
                    cancelTask.setBulkFlag(cursor.getString(3));
                    cancelTask.setRefNumber(cursor.getString(4));
                    cancelTask.setDate(cursor.getString(5));
                    cancelTask.setTime(cursor.getString(6));
                    cancelTask.setReason(cursor.getString(7));
                    cancelTask.setLatitude(cursor.getDouble(8));
                    cancelTask.setLongitude(cursor.getDouble(9));


                    taskList.add(cancelTask);
                } while (cursor.moveToNext());
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // return student list
        return taskList;
    }


    // Getting single student details through ID
    public void deleteDeliveryDetails() {

        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL(("delete from " + DELIVERY_DETAILS_TABLE));
    }

    //delete latlongdetails table
    public void deleteLatLongDetails() {

        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL(("delete from " + LAT_LONG_TABLE));
        db.close();
    }


    // Getting single student details through ID
    public void deleteCancelDetails(String taskid) {

        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DELETE FROM " + CANCEL_DETAILS_TABLE + " WHERE " + KEY_CANCEL_TASKID + "='" + taskid + "'");
        db.close();


    }

    public void deletePickUpSuccessDetails(String docketNo) {

        SQLiteDatabase db = this.getReadableDatabase();

        //db.execSQL(("delete from "+CANCEL_DETAILS_TABLE +"where taskId =?+taskid));

        db.execSQL("DELETE FROM " + PICKUP_SUCCESS_TABLE + " WHERE " + KEY_PICKUP_DOCKETNO + "='" + docketNo + "'");
        db.close();

    }

    // Getting single student details through ID
    public void deleteCancelDetails() {

        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL(("delete from " + CANCEL_DETAILS_TABLE));
        db.close();
    }

}
