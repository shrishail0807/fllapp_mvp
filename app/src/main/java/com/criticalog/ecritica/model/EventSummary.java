package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EventSummary implements Serializable {
    @Expose
    @SerializedName("eventtime")
    private String eventtime;

    @Expose
    @SerializedName("event")
    private String event;


    @Expose
    @SerializedName("hub_location")
    private String hub_location;


    public EventSummary(String eventtime, String event, String hub_location) {

        this.eventtime = eventtime;
        this.event = event;
        this.hub_location = hub_location;
    }

    public String getEventtime() {
        return eventtime;
    }

    public String getEvent() {
        return event;
    }

    public String getHub_location() {
        return hub_location;
    }

    @Override
    public String toString() {
        return "EventSummary{" +
                "eventtime='" + eventtime + '\'' +
                ", event='" + event + '\'' +
                ", hub_location='" + hub_location + '\'' +
                '}';
    }
}
