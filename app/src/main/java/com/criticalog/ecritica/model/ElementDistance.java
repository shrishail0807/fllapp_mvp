package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElementDistance {
    @SerializedName("duration")
    @Expose
    private DurationBillion duration;
    @SerializedName("distance")
    @Expose
    private DistanceBillion distance;

    public DurationBillion getDuration() {
        return duration;
    }

    public void setDuration(DurationBillion duration) {
        this.duration = duration;
    }

    public DistanceBillion getDistance() {
        return distance;
    }

    public void setDistance(DistanceBillion distance) {
        this.distance = distance;
    }
}
