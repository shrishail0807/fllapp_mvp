package com.criticalog.ecritica.model;

public class MBagOrMAwbDetails {


    private String number;
    private String fromcity;
    private String tocity;
    private String connoteno;

    public MBagOrMAwbDetails(String mbagnumber, String fromcity, String tocity, String connoteno) {
        this.number = mbagnumber;
        this.fromcity = fromcity;
        this.tocity = tocity;
        this.connoteno = connoteno;
    }

    public String getMbagnumber() {
        return number;
    }

    public void setMbagnumber(String mbagnumber) {
        this.number = mbagnumber;
    }

    public String getFromcity() {
        return fromcity;
    }

    public void setFromcity(String fromcity) {
        this.fromcity = fromcity;
    }

    public String getTocity() {
        return tocity;
    }

    public void setTocity(String tocity) {
        this.tocity = tocity;
    }

    public String getConnoteno() {
        return connoteno;
    }

    public void setConnoteno(String connoteno) {
        this.connoteno = connoteno;
    }
}
