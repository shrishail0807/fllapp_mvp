package com.criticalog.ecritica.model;

public class DocketDetails {
    private String totalbox;
    private String actual_value;
    private String source_hub;
    private String dest_hub;

    public String getTotalbox() {
        return totalbox;
    }

    public String getActual_value() {
        return actual_value;
    }

    public String getSource_hub() {
        return source_hub;
    }

    public String getDest_hub() {
        return dest_hub;
    }

    @Override
    public String toString() {
        return "DocketDetails{" +
                "totalbox='" + totalbox + '\'' +
                ", actual_value='" + actual_value + '\'' +
                ", source_hub='" + source_hub + '\'' +
                ", dest_hub='" + dest_hub + '\'' +
                '}';
    }
}
