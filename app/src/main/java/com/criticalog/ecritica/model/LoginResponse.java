package com.criticalog.ecritica.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoginResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private JsonObject data;
   /* @SerializedName("data")
    @Expose

    private List<LoginDetails> loginList;
*/
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    public LoginResponse(JsonObject data, String status, String message) {
        this.data = data;
        this.status = status;
        this.message = message;
    }

    public JsonObject getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
