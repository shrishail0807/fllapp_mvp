package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrsRule {
    @SerializedName("rule")
    @Expose
    private String rule;


    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }
}
