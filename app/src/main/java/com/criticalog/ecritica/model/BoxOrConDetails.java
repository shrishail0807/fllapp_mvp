package com.criticalog.ecritica.model;

import androidx.annotation.NonNull;

public class BoxOrConDetails {


    private String number;
    private String cust_code;
    private String cust_name;
    private String consignee_add;

    public BoxOrConDetails(String number, String cust_code, String cust_name, String consignee_add) {
        this.number = number;
        this.cust_code = cust_code;
        this.cust_name = cust_name;
        this.consignee_add = consignee_add;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCust_code() {
        return cust_code;
    }

    public void setCust_code(String cust_code) {
        this.cust_code = cust_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getConsignee_add() {
        return consignee_add;
    }

    public void setConsignee_add(String consignee_add) {
        this.consignee_add = consignee_add;
    }


    @Override
    public String toString() {
        return number;
    }
}
