package com.criticalog.ecritica.model;

public class Baggingdetails {
    private String boxno,docketno,dest;

    public Baggingdetails(String boxno, String docketno, String dest) {
        this.boxno = boxno;
        this.docketno = docketno;
        this.dest = dest;
    }

    public String getBoxno() {
        return boxno;
    }

    public void setBoxno(String boxno) {
        this.boxno = boxno;
    }

    public String getDocketno() {
        return docketno;
    }

    public void setDocketno(String docketno) {
        this.docketno = docketno;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    @Override
    public String toString() {
        return boxno ;
    }
}
