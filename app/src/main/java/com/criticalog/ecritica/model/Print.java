package com.criticalog.ecritica.model;

import java.util.ArrayList;
import java.util.List;

public class Print {

    private LabelPrint labelPrint;
    private List<Integer> boxList = new ArrayList<>();

    public Print(LabelPrint labelPrint, List<Integer> boxList) {
        this.labelPrint = labelPrint;
        this.boxList = boxList;
    }

    public LabelPrint getLabelPrint() {
        return labelPrint;
    }

    public void setLabelPrint(LabelPrint labelPrint) {
        this.labelPrint = labelPrint;
    }

    public List<Integer> getBoxList() {
        return boxList;
    }

    public void setBoxList(List<Integer> boxList) {
        this.boxList = boxList;
    }
}
