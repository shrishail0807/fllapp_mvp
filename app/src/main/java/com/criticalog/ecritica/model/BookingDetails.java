package com.criticalog.ecritica.model;

import com.google.gson.annotations.SerializedName;

public class BookingDetails {

    @SerializedName("booking_id")
    private String bookingnumber;
    @SerializedName("picktype")
    private String picktype;
    @SerializedName("unique_id")
    private String unique_id;
    @SerializedName("booking_type")
    private int booking_type;
    @SerializedName("source_pin")
    private String sourcepin;
    @SerializedName("connote_number")
    private String connoteno;
    @SerializedName("pickup_regular_id")
    private String pickup_regular_id;
    @SerializedName("book_client_code")
    private String book_client_code;


    public BookingDetails(String bookingnumber, String picktype, String unique_id, int booking_type, String sourcepin, String connoteno, String pickup_regular_id, String book_client_code) {
        this.bookingnumber = bookingnumber;
        this.picktype = picktype;
        this.unique_id = unique_id;
        this.booking_type = booking_type;
        this.sourcepin = sourcepin;
        this.connoteno = connoteno;
        this.pickup_regular_id = pickup_regular_id;
        this.book_client_code = book_client_code;
    }

    public String getBookingnumber() {
        return bookingnumber;
    }

    public String getPicktype() {
        return picktype;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public int getBooking_type() {
        return booking_type;
    }

    public String getSourcepin() {
        return sourcepin;
    }

    public String getConnoteno() {
        return connoteno;
    }

    public String getPickup_regular_id() {
        return pickup_regular_id;
    }

    public String getBook_client_code() {
        return book_client_code;
    }

    @Override
    public String toString() {
        return "BookingDetails{" +
                "bookingnumber='" + bookingnumber + '\'' +
                ", picktype='" + picktype + '\'' +
                ", unique_id='" + unique_id + '\'' +
                ", booking_type=" + booking_type +
                ", sourcepin='" + sourcepin + '\'' +
                ", connoteno='" + connoteno + '\'' +
                ", pickup_regular_id='" + pickup_regular_id + '\'' +
                ", book_client_code='" + book_client_code + '\'' +
                '}';
    }
}
