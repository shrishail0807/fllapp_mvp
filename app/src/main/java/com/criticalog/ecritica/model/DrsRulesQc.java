package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrsRulesQc {
    @SerializedName("rule")
    @Expose
    private String rule;
    @SerializedName("pass_factor")
    @Expose
    private String passFactor;
    @SerializedName("image")
    @Expose
    private String image;

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getPassFactor() {
        return passFactor;
    }

    public void setPassFactor(String passFactor) {
        this.passFactor = passFactor;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
