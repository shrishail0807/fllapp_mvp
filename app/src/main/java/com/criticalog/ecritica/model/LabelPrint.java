package com.criticalog.ecritica.model;

public class LabelPrint {

    private String docketno;
    private String source;
    private String destination;

    public LabelPrint(String docketno, String source, String destination) {
        this.docketno = docketno;
        this.source = source;
        this.destination = destination;
    }

    public String getDocketno() {
        return docketno;
    }

    public void setDocketno(String docketno) {
        this.docketno = docketno;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
