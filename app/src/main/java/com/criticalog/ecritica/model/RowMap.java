package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RowMap {
    @SerializedName("elements")
    @Expose
    private List<ElementMap> elements = null;

    public List<ElementMap> getElements() {
        return elements;
    }

    public void setElements(List<ElementMap> elements) {
        this.elements = elements;
    }
}
