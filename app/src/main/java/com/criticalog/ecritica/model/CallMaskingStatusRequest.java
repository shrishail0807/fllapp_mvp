package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallMaskingStatusRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("prs_no")
    @Expose
    private String prsNo;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("drs_no")
    @Expose
    private String drsNo;
    @SerializedName("connote_no")
    @Expose
    private String connoteNo;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPrsNo() {
        return prsNo;
    }

    public void setPrsNo(String prsNo) {
        this.prsNo = prsNo;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getDrsNo() {
        return drsNo;
    }

    public void setDrsNo(String drsNo) {
        this.drsNo = drsNo;
    }

    public String getConnoteNo() {
        return connoteNo;
    }

    public void setConnoteNo(String connoteNo) {
        this.connoteNo = connoteNo;
    }

}
