package com.criticalog.ecritica.model;

import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PickupDetails {

    @SerializedName("PRS")
    @Expose
    private String prs;

    @SerializedName("Round")
    @Expose
    private String round;

    @SerializedName("PRS_ID")
    private String prsid;

    @SerializedName("Latitude")
    @Expose
    private String latitude;

    @SerializedName("Longitude")
    @Expose
    private String longitude;

    @SerializedName("Hub")
    @Expose
    private String hub;

    @SerializedName("hub_type")
    @Expose
    private String hubtype;

    @SerializedName("client_details")
    private ArrayList<Client> clients;



    public PickupDetails(String prs, String round, String prsid, String latitude, String longitude, String hub, String hubtype, ArrayList<Client> clients) {
        this.prs = prs;
        this.round = round;
        this.prsid = prsid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.hub = hub;
        this.hubtype = hubtype;
        this.clients = clients;
    }

    public String getPrs() {
        return prs;
    }

    public String getRound() {
        return round;
    }

    public String getPrsid() {
        return prsid;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getHub() {
        return hub;
    }

    public String getHubtype() {
        return hubtype;
    }

    public ArrayList<Client> getClients() {
        return clients;
    }




    @Override
    public String toString() {
        return "PickupDetails{" +
                "prs='" + prs + '\'' +
                ", round='" + round + '\'' +
                ", prsid='" + prsid + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", hub='" + hub + '\'' +
                ", hubtype='" + hubtype + '\'' +
                ", clients=" + clients +
                '}';
    }
}
