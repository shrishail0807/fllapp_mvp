package com.criticalog.ecritica.model;

public class PickupStaus {
    private String actualVlaue;
    private String sourcePin;
    private String destinatonPin;
    private String totalBox;
    private String airwayValue, arwayLimit;

    public PickupStaus(String totalBox, String actualVlaue, String sourcePin, String destinatonPin, String airwayValue) {
        this.actualVlaue = actualVlaue;
        this.sourcePin = sourcePin;
        this.destinatonPin = destinatonPin;
        this.airwayValue = airwayValue;
        this.totalBox = totalBox;
    }

    public String getActualVlaue() {
        return actualVlaue;
    }

    public void setActualVlaue(String actualVlaue) {
        this.actualVlaue = actualVlaue;
    }

    public String getSourcePin() {
        return sourcePin;
    }

    public void setSourcePin(String sourcePin) {
        this.sourcePin = sourcePin;
    }

    public String getDestinatonPin() {
        return destinatonPin;
    }

    public void setDestinatonPin(String destinatonPin) {
        this.destinatonPin = destinatonPin;
    }

    public String getAirwayValue() {
        return airwayValue;
    }

    public void setAirwayValue(String airwayValue) {
        this.airwayValue = airwayValue;
    }

    public String getArwayLimit() {
        return arwayLimit;
    }

    public void setArwayLimit(String arwayLimit) {
        this.arwayLimit = arwayLimit;
    }

    public String getTotalBox() {
        return totalBox;
    }

    public void setTotalBox(String totalBox) {
        this.totalBox = totalBox;
    }
}
