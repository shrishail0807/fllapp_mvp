package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CallRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("to_number")
    @Expose
    private String to_number;

    @SerializedName("drs")
    @Expose
    private String drs;
    @SerializedName("connote_num")
    @Expose
    private String connote_num;

    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("longitude")
    @Expose
    private String longitute;

    @SerializedName("prs_id")
    @Expose
    private String prs_id;

    @SerializedName("booking_id")
    @Expose
    private String booking_id;

    @SerializedName("unique_id")
    @Expose
    private String unique_id;

    @SerializedName("prs_no")
    @Expose
    private String prs_no;

    @SerializedName("drs_no")
    @Expose
    private String drs_no;

    public String getPrs_no() {
        return prs_no;
    }

    public void setPrs_no(String prs_no) {
        this.prs_no = prs_no;
    }

    public String getDrs_no() {
        return drs_no;
    }

    public void setDrs_no(String drs_no) {
        this.drs_no = drs_no;
    }

    public String getPrs_id() {
        return prs_id;
    }

    public void setPrs_id(String prs_id) {
        this.prs_id = prs_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitute() {
        return longitute;
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }

    public String getConnote_num() {
        return connote_num;
    }

    public void setConnote_num(String connote_num) {
        this.connote_num = connote_num;
    }

    public String getDrs() {
        return drs;
    }

    public void setDrs(String drs) {
        this.drs = drs;
    }

    public String getTo_number() {
        return to_number;
    }

    public void setTo_number(String to_number) {
        this.to_number = to_number;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
