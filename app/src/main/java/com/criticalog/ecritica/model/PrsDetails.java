package com.criticalog.ecritica.model;

import java.util.ArrayList;
import java.util.Arrays;



public class PrsDetails  {
    private String prs;
    private String date;
    private String time;
    private String routeCode;
    ArrayList<Client>[] clientsExtract1 = new ArrayList[100];


    public PrsDetails() {
    }

    public ArrayList<Client>[] getClientsExtract1() {
        return clientsExtract1;
    }

    public void setClientsExtract1(ArrayList<Client>[] clientsExtract1) {
        this.clientsExtract1 = clientsExtract1;
    }

    public PrsDetails(String prs, String date, String time, String routeCode, ArrayList<Client>[] clientsExtract1) {
        this.prs = prs;
        this.date = date;
        this.time = time;
        this.routeCode = routeCode;
        this.clientsExtract1 = clientsExtract1;

    }

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    @Override
    public String toString() {
        return "PrsDetails{" +
                "prs='" + prs + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", routeCode='" + routeCode + '\'' +
                ", clientsExtract1=" + Arrays.toString(clientsExtract1) +
                '}';
    }
}
