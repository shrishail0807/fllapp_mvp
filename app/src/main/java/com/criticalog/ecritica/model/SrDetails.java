package com.criticalog.ecritica.model;

import java.io.Serializable;

public class SrDetails implements Serializable {
    private String datetime;
    private String raised_by;
    private String comments;

    public SrDetails(String datetime, String raised_by, String comments) {
        this.datetime = datetime;
        this.raised_by = raised_by;
        this.comments = comments;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getRaised_by() {
        return raised_by;
    }

    public String getComments() {
        return comments;
    }

    @Override
    public String toString() {
        return "SrDetails{" +
                "datetime='" + datetime + '\'' +
                ", raised_by='" + raised_by + '\'' +
                ", comments='" + comments + '\'' +
                '}';
    }
}
