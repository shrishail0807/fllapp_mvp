package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventResponse implements Serializable {

    private String date;

    @SerializedName("ITEMS")
    @Expose
    private List<EventSummary> eventSummaries;

    public String getDate() {
        return date;
    }

    public List<EventSummary> getEventSummaries() {
        return eventSummaries;
    }

    public EventResponse(String date, List<EventSummary> eventSummaries) {
        this.date = date;
        this.eventSummaries = eventSummaries;
    }

    @Override
    public String toString() {
        return "EventResponse{" +
                "date='" + date + '\'' +
                ", eventSummaries=" + eventSummaries +
                '}';
    }
}
