package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BillionDistanceResponse implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rows")
    @Expose
    private List<RowDistance> rows = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RowDistance> getRows() {
        return rows;
    }

    public void setRows(List<RowDistance> rows) {
        this.rows = rows;
    }
}
