package com.criticalog.ecritica.model;

public class HRHistory {
    private String subject;
    private String message;
    private String status;
    private String raised_date;
    private String updated_date;
    private int row_id;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    private int rating;

    public int getRow_id() {
        return row_id;
    }

    public void setRow_id(int row_id) {
        this.row_id = row_id;
    }



    public HRHistory(String subject, String message, String status, String raised_date, String updated_date,int row_id,int rating) {
        this.subject = subject;
        this.message = message;
        this.status = status;
        this.raised_date = raised_date;
        this.updated_date = updated_date;
        this.row_id = row_id;
        this.rating = rating;
    }



    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRaised_date() {
        return raised_date;
    }

    public void setRaised_date(String raised_date) {
        this.raised_date = raised_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }


}
