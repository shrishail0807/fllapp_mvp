package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RowDistance {
    @SerializedName("elements")
    @Expose
    private List<ElementDistance> elements = null;

    public List<ElementDistance> getElements() {
        return elements;
    }

    public void setElements(List<ElementDistance> elements) {
        this.elements = elements;
    }
}
