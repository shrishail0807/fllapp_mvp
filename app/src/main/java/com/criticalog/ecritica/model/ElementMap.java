package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElementMap {
    @SerializedName("distance")
    @Expose
    private DistanceMap distance;
    @SerializedName("duration")
    @Expose
    private DurationMap duration;
    @SerializedName("status")
    @Expose
    private String status;

    public DistanceMap getDistance() {
        return distance;
    }

    public void setDistance(DistanceMap distance) {
        this.distance = distance;
    }

    public DurationMap getDuration() {
        return duration;
    }

    public void setDuration(DurationMap duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
