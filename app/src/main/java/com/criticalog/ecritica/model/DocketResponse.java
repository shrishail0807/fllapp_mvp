package com.criticalog.ecritica.model;

public class DocketResponse {
    private String status;
    private String message ;
    private String type;
    private DocketDetails data;


    public DocketResponse(String status, String message, String type, DocketDetails data) {
        this.status = status;
        this.message = message;
        this.type = type;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public DocketDetails getData() {
        return data;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DocketResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", type='" + type + '\'' +
                ", data=" + data +
                '}';
    }
}
