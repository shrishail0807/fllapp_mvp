package com.criticalog.ecritica.model;

public class OperationMenuDetails {

    private String name ;
    private String color;

    public OperationMenuDetails(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
