package com.criticalog.ecritica.model;

import com.criticalog.ecritica.ModelClasses.OpenImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataFLL {
    @SerializedName("open_pick_up")
    @Expose
    private String openPickUp;
    @SerializedName("open_delivery")
    @Expose
    private String openDelivery;
    @SerializedName("sdc_mandatory")
    @Expose
    private String sdcMandatory;
    @SerializedName("qr_cod_img")
    @Expose
    private String qrCodImg;
    @SerializedName("cit_reprint")
    @Expose
    private String citReprint;
    @SerializedName("open_img_count")
    @Expose
    private Integer openImgCount;
    @SerializedName("open_images")
    @Expose
    private List<OpenImage> openImages;
    @SerializedName("drs_rule")
    @Expose
    private List<DrsRule> drsRule;
    @SerializedName("prs_rule")
    @Expose
    private List<PrsRule> prsRule;
    @SerializedName("prs_response_flag")
    @Expose
    private String prsResponseFlag;
    @SerializedName("drs_response_flag")
    @Expose
    private String drsResponseFlag;
    @SerializedName("prs_rules_qc")
    @Expose
    private List<PrsRulesQc> prsRulesQc;
    @SerializedName("drs_rules_qc")
    @Expose
    private List<DrsRulesQc> drsRulesQc;

    public String getOpenPickUp() {
        return openPickUp;
    }

    public void setOpenPickUp(String openPickUp) {
        this.openPickUp = openPickUp;
    }

    public String getOpenDelivery() {
        return openDelivery;
    }

    public void setOpenDelivery(String openDelivery) {
        this.openDelivery = openDelivery;
    }

    public String getSdcMandatory() {
        return sdcMandatory;
    }

    public void setSdcMandatory(String sdcMandatory) {
        this.sdcMandatory = sdcMandatory;
    }

    public String getQrCodImg() {
        return qrCodImg;
    }

    public void setQrCodImg(String qrCodImg) {
        this.qrCodImg = qrCodImg;
    }

    public String getCitReprint() {
        return citReprint;
    }

    public void setCitReprint(String citReprint) {
        this.citReprint = citReprint;
    }

    public Integer getOpenImgCount() {
        return openImgCount;
    }

    public void setOpenImgCount(Integer openImgCount) {
        this.openImgCount = openImgCount;
    }

    public List<OpenImage> getOpenImages() {
        return openImages;
    }

    public void setOpenImages(List<OpenImage> openImages) {
        this.openImages = openImages;
    }

    public List<DrsRule> getDrsRule() {
        return drsRule;
    }

    public void setDrsRule(List<DrsRule> drsRule) {
        this.drsRule = drsRule;
    }

    public List<PrsRule> getPrsRule() {
        return prsRule;
    }

    public void setPrsRule(List<PrsRule> prsRule) {
        this.prsRule = prsRule;
    }

    public String getPrsResponseFlag() {
        return prsResponseFlag;
    }

    public void setPrsResponseFlag(String prsResponseFlag) {
        this.prsResponseFlag = prsResponseFlag;
    }

    public String getDrsResponseFlag() {
        return drsResponseFlag;
    }

    public void setDrsResponseFlag(String drsResponseFlag) {
        this.drsResponseFlag = drsResponseFlag;
    }

    public List<PrsRulesQc> getPrsRulesQc() {
        return prsRulesQc;
    }

    public void setPrsRulesQc(List<PrsRulesQc> prsRulesQc) {
        this.prsRulesQc = prsRulesQc;
    }

    public List<DrsRulesQc> getDrsRulesQc() {
        return drsRulesQc;
    }

    public void setDrsRulesQc(List<DrsRulesQc> drsRulesQc) {
        this.drsRulesQc = drsRulesQc;
    }
}
 /*   @SerializedName("open_pick_up")
    @Expose
    private String openPickUp;
    @SerializedName("open_delivery")
    @Expose
    private String openDelivery;
    @SerializedName("sdc_mandatory")
    @Expose
    private String sdcMandatory;
    @SerializedName("qr_cod_img")
    @Expose
    private String qrCodImg;
    @SerializedName("cit_reprint")
    @Expose
    private String citReprint;

    @SerializedName("prs_response_flag")
    @Expose
    private String prs_response_flag;

    public String getPrs_response_flag() {
        return prs_response_flag;
    }

    public void setPrs_response_flag(String prs_response_flag) {
        this.prs_response_flag = prs_response_flag;
    }

    public String getDrs_response_flag() {
        return drs_response_flag;
    }

    public void setDrs_response_flag(String drs_response_flag) {
        this.drs_response_flag = drs_response_flag;
    }

    @SerializedName("drs_response_flag")
    @Expose
    private String drs_response_flag;

    @SerializedName("open_img_count")
    @Expose
    private Integer openImgCount;
    @SerializedName("open_images")
    @Expose
    private List<OpenImage> openImages;
    @SerializedName("prs_rule")
    @Expose
    private List<PrsRule> prsRule;
    @SerializedName("drs_rule")
    @Expose
    private List<DrsRule> drsRule;

    public String getOpenPickUp() {
        return openPickUp;
    }

    public void setOpenPickUp(String openPickUp) {
        this.openPickUp = openPickUp;
    }

    public String getOpenDelivery() {
        return openDelivery;
    }

    public void setOpenDelivery(String openDelivery) {
        this.openDelivery = openDelivery;
    }

    public String getSdcMandatory() {
        return sdcMandatory;
    }

    public void setSdcMandatory(String sdcMandatory) {
        this.sdcMandatory = sdcMandatory;
    }

    public String getQrCodImg() {
        return qrCodImg;
    }

    public void setQrCodImg(String qrCodImg) {
        this.qrCodImg = qrCodImg;
    }

    public String getCitReprint() {
        return citReprint;
    }

    public void setCitReprint(String citReprint) {
        this.citReprint = citReprint;
    }

    public Integer getOpenImgCount() {
        return openImgCount;
    }

    public void setOpenImgCount(Integer openImgCount) {
        this.openImgCount = openImgCount;
    }

    public List<OpenImage> getOpenImages() {
        return openImages;
    }

    public void setOpenImages(List<OpenImage> openImages) {
        this.openImages = openImages;
    }

    public List<PrsRule> getPrsRule() {
        return prsRule;
    }

    public void setPrsRule(List<PrsRule> prsRule) {
        this.prsRule = prsRule;
    }

    public List<DrsRule> getDrsRule() {
        return drsRule;
    }

    public void setDrsRule(List<DrsRule> drsRule) {
        this.drsRule = drsRule;
    }*/


