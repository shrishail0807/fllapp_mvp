package com.criticalog.ecritica.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NDCOtpRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("round_no")
    @Expose
    private String roundNo;
    @SerializedName("consignee")
    @Expose
    private String consignee;
    @SerializedName("prs_drs_no")
    @Expose
    private String prsDrsNo;
    @SerializedName("booking_no")
    @Expose
    private String bookingNo;
    @SerializedName("send_otp")
    @Expose
    private String sendOtp;
    @SerializedName("for_type")
    @Expose
    private String for_type;

    public String getFor_type() {
        return for_type;
    }

    public void setFor_type(String for_type) {
        this.for_type = for_type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getRoundNo() {
        return roundNo;
    }

    public void setRoundNo(String roundNo) {
        this.roundNo = roundNo;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getPrsDrsNo() {
        return prsDrsNo;
    }

    public void setPrsDrsNo(String prsDrsNo) {
        this.prsDrsNo = prsDrsNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getSendOtp() {
        return sendOtp;
    }

    public void setSendOtp(String sendOtp) {
        this.sendOtp = sendOtp;
    }
}
