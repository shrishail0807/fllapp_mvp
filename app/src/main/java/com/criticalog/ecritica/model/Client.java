package com.criticalog.ecritica.model;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;



public class Client  implements Serializable {
    @SerializedName("client_code")
    private String clientcode;
    @SerializedName("client_name")
    private String clientname;
    @SerializedName("location")
    private String clientadd;
    @SerializedName("Booking_details")
    private ArrayList<BookingDetails> bookingdetails;
    @SerializedName("actual_limit")
    private String actual_limit;



    public Client(String clientcode, String clientname, String clientadd, ArrayList<BookingDetails> bookingnumber, String actual_limit) {
        this.clientcode = clientcode;
        this.clientname = clientname;
        this.clientadd = clientadd;
        this.bookingdetails = bookingnumber;
        this.actual_limit = actual_limit;
    }

    public String getClientcode() {
        return clientcode;
    }

    public String getClientname() {
        return clientname;
    }

    public String getClientadd() {
        return clientadd;
    }

    public ArrayList<BookingDetails> getBookingdetails() {
        return bookingdetails;
    }

    public String getActual_limit() {
        return actual_limit;
    }



    @Override
    public String toString() {
        return "Client{" +
                "clientcode='" + clientcode + '\'' +
                ", clientname='" + clientname + '\'' +
                ", clientadd='" + clientadd + '\'' +
                ", bookingnumber=" + bookingdetails +
                ", actual_limit='" + actual_limit + '\'' +
                '}';
    }
}
