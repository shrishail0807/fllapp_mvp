package com.criticalog.ecritica.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SrResponse implements Serializable {

    private String status;
    private String message;
    private ArrayList<SrDetails> data;


    public SrResponse(String status, String message, ArrayList<SrDetails> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<SrDetails> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "SrResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
