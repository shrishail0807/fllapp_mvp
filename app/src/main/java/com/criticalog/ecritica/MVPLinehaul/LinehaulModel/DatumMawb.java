package com.criticalog.ecritica.MVPLinehaul.LinehaulModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumMawb {
    @SerializedName("mwab_no")
    @Expose
    private String mwabNo;
    @SerializedName("mode")
    @Expose
    private String mode;

    public String getMwabNo() {
        return mwabNo;
    }

    public void setMwabNo(String mwabNo) {
        this.mwabNo = mwabNo;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
