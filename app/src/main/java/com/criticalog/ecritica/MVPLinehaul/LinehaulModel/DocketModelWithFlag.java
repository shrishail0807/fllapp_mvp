package com.criticalog.ecritica.MVPLinehaul.LinehaulModel;

public class DocketModelWithFlag {
    private String docketNumber;
    private String docketFalg;
    public DocketModelWithFlag()
    {
    }
    public DocketModelWithFlag(String docketNumber, String docketFalg) {
        this.docketNumber = docketNumber;
        this.docketFalg = docketFalg;
    }

    public String getDocketNumber() {
        return docketNumber;
    }

    public void setDocketNumber(String docketNumber) {
        this.docketNumber = docketNumber;
    }

    public String getDocketFalg() {
        return docketFalg;
    }

    public void setDocketFalg(String docketFalg) {
        this.docketFalg = docketFalg;
    }
}
