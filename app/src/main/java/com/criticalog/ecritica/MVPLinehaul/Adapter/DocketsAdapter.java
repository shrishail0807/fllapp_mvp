package com.criticalog.ecritica.MVPLinehaul.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketModelWithFlag;
import com.criticalog.ecritica.R;

import java.util.ArrayList;

public class DocketsAdapter extends RecyclerView.Adapter<DocketsAdapter.DocketsViewHolder> {
    Context context;
    ArrayList<DocketModelWithFlag> docketArrayList;
    int boxBag;
    public DocketsAdapter(Context context, ArrayList<DocketModelWithFlag> docketArrayList, int boxBag) {
        this.context = context;
        this.docketArrayList = docketArrayList;
        this.boxBag=boxBag;

    }

    @NonNull
    @Override
    public DocketsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dockets_item_mvp, parent, false);
        return new DocketsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DocketsViewHolder holder, int position) {

        holder.docketNumber.setText(String.valueOf(docketArrayList.get(position).getDocketNumber()));

        if(docketArrayList.get(position).getDocketFalg().equalsIgnoreCase("true"))
        {
            holder.docketNumber.setTextColor(Color.WHITE);
            holder.mRightMark.setVisibility(View.VISIBLE);
            holder.mDocketLinearLayout.setBackgroundResource(R.drawable.rounded_bacground_green);

        }else {
            holder.docketNumber.setTextColor(Color.BLACK);
            //  holder.mRightMark.setVisibility(View.VISIBLE);
            holder.mDocketLinearLayout.setBackgroundResource(R.drawable.rounded_background_border);
        }

        if(boxBag==1)
        {
            holder.docketNumber.setTextColor(Color.BLACK);
            //  holder.mRightMark.setVisibility(View.VISIBLE);
            holder.mDocketLinearLayout.setBackgroundResource(R.drawable.rounded_background_border);

            if(docketArrayList.get(position).getDocketFalg().equalsIgnoreCase("1"))
            {
                holder.docketNumber.setText(String.valueOf(docketArrayList.get(position).getDocketNumber())+":"+"Transit");
            }else {
                holder.docketNumber.setText(String.valueOf(docketArrayList.get(position).getDocketNumber())+":"+"Normal");
            }

        }
    }

    @Override
    public int getItemCount() {
        return docketArrayList.size();
    }

    public class DocketsViewHolder extends RecyclerView.ViewHolder {
        TextView docketNumber;
        ImageView mRightMark;
        LinearLayout mDocketLinearLayout;

        public DocketsViewHolder(@NonNull View itemView) {
            super(itemView);

            docketNumber = itemView.findViewById(R.id.mDockeetNumber);
            mRightMark = itemView.findViewById(R.id.mRightMark);
            mDocketLinearLayout=itemView.findViewById(R.id.mDocketLinearLayout);
        }
    }
}