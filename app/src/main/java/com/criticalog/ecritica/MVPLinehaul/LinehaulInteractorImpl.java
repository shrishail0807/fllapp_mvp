package com.criticalog.ecritica.MVPLinehaul;

import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivePostData;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivedResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LinehaulInteractorImpl implements LinehaulCotract.LinehaulOriginIntractor, LinehaulCotract.LinehaulDestinationIntractor, LinehaulCotract.LinehaulMawbIntractor,
LinehaulCotract.LinehaulDocketNumberIntractor,LinehaulCotract.LinehaulReceivedIntractor {
    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void getOriginsSuccess(LinehaulCotract.LinehaulOriginIntractor.OnFinishedListener onFinishedListener, String token, OriginRequest originRequest) {
        Call<OriginResponse> originResponseCall = service.getAllOrigins(originRequest);
        originResponseCall.enqueue(new Callback<OriginResponse>() {
            @Override
            public void onResponse(Call<OriginResponse> call, Response<OriginResponse> response) {

                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<OriginResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getDestinationsSuccess(LinehaulCotract.LinehaulDestinationIntractor.OnFinishedListener onFinishedListener, String token, DestinationsRequest destinationsRequest) {
        Call<DestinationsResponse> destinationsResponseCall = service.getAllDestinations(destinationsRequest);
        destinationsResponseCall.enqueue(new Callback<DestinationsResponse>() {
            @Override
            public void onResponse(Call<DestinationsResponse> call, Response<DestinationsResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DestinationsResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getMawbNumbersSuccess(LinehaulCotract.LinehaulMawbIntractor.OnFinishedListener onFinishedListener, String token, GetMawbRequest getMawbRequest) {
        Call<GetMawbResponse> getMawbResponseCall = service.getMawbNumber(getMawbRequest);
        getMawbResponseCall.enqueue(new Callback<GetMawbResponse>() {
            @Override
            public void onResponse(Call<GetMawbResponse> call, Response<GetMawbResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<GetMawbResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getDocketNumbersSuccess(LinehaulCotract.LinehaulDocketNumberIntractor.OnFinishedListener onFinishedListener, String token, DocketListRequest docketListRequest) {

        Call<DocketListResponse> docketListResponseCall = service.getDocketNumbers(docketListRequest);
        docketListResponseCall.enqueue(new Callback<DocketListResponse>() {
            @Override
            public void onResponse(Call<DocketListResponse> call, Response<DocketListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DocketListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getLinehaulReceivedSuccess(LinehaulCotract.LinehaulReceivedIntractor.OnFinishedListener onFinishedListener, String token, LinehaulReceivePostData linehaulReceivePostData) {

        Call<LinehaulReceivedResponse> linehaulReceivedResponseCall = service.linehaulReceived(linehaulReceivePostData);

        linehaulReceivedResponseCall.enqueue(new Callback<LinehaulReceivedResponse>() {
            @Override
            public void onResponse(Call<LinehaulReceivedResponse> call, Response<LinehaulReceivedResponse> response) {

                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<LinehaulReceivedResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }
}
