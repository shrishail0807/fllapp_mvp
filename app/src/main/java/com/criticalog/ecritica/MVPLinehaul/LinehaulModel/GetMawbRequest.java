package com.criticalog.ecritica.MVPLinehaul.LinehaulModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMawbRequest {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("origin_hub")
    @Expose
    private String originHub;
    @SerializedName("dest_hub")
    @Expose
    private String destHub;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getOriginHub() {
        return originHub;
    }

    public void setOriginHub(String originHub) {
        this.originHub = originHub;
    }

    public String getDestHub() {
        return destHub;
    }

    public void setDestHub(String destHub) {
        this.destHub = destHub;
    }
}
