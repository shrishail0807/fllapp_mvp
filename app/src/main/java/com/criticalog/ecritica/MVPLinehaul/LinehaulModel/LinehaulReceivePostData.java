package com.criticalog.ecritica.MVPLinehaul.LinehaulModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LinehaulReceivePostData implements Serializable {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("airway_bill")
    @Expose
    private String airwayBill;
    @SerializedName("air_surface")
    @Expose
    private String airSurface;
    @SerializedName("docket_list")
    @Expose
    private List<String> docketList = null;
    @SerializedName("origin_hub")
    @Expose
    private String originHub;
    @SerializedName("dest_hub")
    @Expose
    private String destHub;
    @SerializedName("flight_no")
    @Expose
    private String flightNo;
    @SerializedName("etd")
    @Expose
    private String etd;
    @SerializedName("eta")
    @Expose
    private String eta;
    @SerializedName("image_data")
    @Expose
    private String imageData;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAirwayBill() {
        return airwayBill;
    }

    public void setAirwayBill(String airwayBill) {
        this.airwayBill = airwayBill;
    }

    public String getAirSurface() {
        return airSurface;
    }

    public void setAirSurface(String airSurface) {
        this.airSurface = airSurface;
    }

    public List<String> getDocketList() {
        return docketList;
    }

    public void setDocketList(List<String> docketList) {
        this.docketList = docketList;
    }

    public String getOriginHub() {
        return originHub;
    }

    public void setOriginHub(String originHub) {
        this.originHub = originHub;
    }

    public String getDestHub() {
        return destHub;
    }

    public void setDestHub(String destHub) {
        this.destHub = destHub;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }
}
