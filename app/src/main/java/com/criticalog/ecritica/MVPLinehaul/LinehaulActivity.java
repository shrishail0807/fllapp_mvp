package com.criticalog.ecritica.MVPLinehaul;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPLinehaul.Adapter.DocketsAdapter;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.Datum;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DatumDocket;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DatumMawb;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DatumOrigin;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketModelWithFlag;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivePostData;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivedResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LinehaulActivity extends AppCompatActivity implements View.OnClickListener, LinehaulCotract.MainView {
    private EditText mOrigin, mDestination, mAirwayBill;
    private TextView mReceived;
    private LinehaulCotract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String token, userId;
    SimpleArcDialog mProgressBar;
    TimePicker timePicker;

    ArrayList<SampleSearchModel> createOriginData = new ArrayList();
    ArrayList<String> createOriginDataString = new ArrayList();
    ArrayList<SampleSearchModel> createDestData = new ArrayList();
    ArrayList<String> createDestDataString = new ArrayList();

    ArrayList<SampleSearchModel> createMawbData = new ArrayList();

    List<DatumOrigin> datumOrigins = new ArrayList<>();
    List<Datum> datumDestination = new ArrayList<>();
    ArrayList<String> bagsList = new ArrayList<>();
    ArrayList<String> bagsListFromAPI = new ArrayList<>();
    Button mAddBag;
    List<DatumMawb> mawbNumbersList = new ArrayList<>();
    TextView mFlightText, mATAText, mATDText, mBagNumberLayoutText, mListOfDocketsText;
    EditText ATD, ATA, mFlightNo, mBagNumber;
    private LinearLayout mBagEnterLayout;
    private RecyclerView mRvDockets;
    ArrayList<DocketModelWithFlag> docketModelWithFlags = new ArrayList<>();

    ImageView mBarcodeScanBag;
    private static int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private String base64CapturedImage;
    TextView mBack;
    ImageView captureImage;
    File photoFile = null;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude=0, longitude = 0;
    private RestServices mRestServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linehaul_mvp);



        mOrigin = findViewById(R.id.mOrigin);
        mDestination = findViewById(R.id.mDestination);
        mAirwayBill = findViewById(R.id.mAirwayBill);
        mReceived = findViewById(R.id.mReceived);
        ATA = findViewById(R.id.mAta);
        ATD = findViewById(R.id.mAtd);
        mFlightNo = findViewById(R.id.mFlightNo);
        mBack=findViewById(R.id.mBack);

        mFlightText = findViewById(R.id.mFlightText);
        mATAText = findViewById(R.id.mATAText);
        mATDText = findViewById(R.id.mATDText);
        mBagNumberLayoutText = findViewById(R.id.mBagNumberLayoutText);
        mListOfDocketsText = findViewById(R.id.mListOfDocketsText);
        mBagEnterLayout = findViewById(R.id.mBagEnterLayout);
        mBagNumber = findViewById(R.id.mBagNumber);
        mAddBag = findViewById(R.id.mAddBag);
        mRvDockets = findViewById(R.id.mRvDockets);
        mBarcodeScanBag = findViewById(R.id.mBarcodeScanBag);


        mOrigin.setOnClickListener(this);
        mDestination.setOnClickListener(this);
        mAirwayBill.setOnClickListener(this);
        mReceived.setOnClickListener(this);
        mAddBag.setOnClickListener(this);
        mBarcodeScanBag.setOnClickListener(this);
        mBack.setOnClickListener(this);

        mOrigin.setFocusable(false);
        mDestination.setFocusable(false);
        mAirwayBill.setFocusable(false);
        ATD.setFocusable(false);
        ATA.setFocusable(false);
        ATD.setClickable(true);
        ATA.setClickable(true);

        ATD.setOnClickListener(this);
        ATA.setOnClickListener(this);

        //Loader
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mRestServices= RestClient.getRetrofitInstance().create(RestServices.class);
        token = mCriticalogSharedPreferences.getData("token");
        userId = mCriticalogSharedPreferences.getData("userId");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=mCriticalogSharedPreferences.getData("distance_api_key");

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        mBagNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String bagNumber = s.toString();

                if (bagNumber.length() == 7) {
                    if (!bagNumber.equals("")) {
                        if (bagsListFromAPI.contains(bagNumber)) {

                            if (bagsList.contains(bagNumber)) {
                                Toast.makeText(LinehaulActivity.this, "Bag already Scaned", Toast.LENGTH_SHORT).show();
                                mBagNumber.setText("");
                              //  mBagNumber.requestFocus();
                                requestFocus(mBagNumber);
                            } else {
                                bagsList.add(bagNumber);
                                mBagNumber.setText("");
                                int rightMarkIndex = bagsListFromAPI.indexOf(bagNumber);

                                docketModelWithFlags.remove(rightMarkIndex);

                                docketModelWithFlags.add(rightMarkIndex, new DocketModelWithFlag(bagNumber, "true"));

                                DocketsAdapter docketsAdapter = new DocketsAdapter(LinehaulActivity.this, docketModelWithFlags,0);
                                mRvDockets.setLayoutManager(new GridLayoutManager(LinehaulActivity.this, 2));
                                //mRvDockets.setAdapter(docketsAdapter);
                                docketsAdapter.notifyDataSetChanged();

                                Toast.makeText(LinehaulActivity.this, bagNumber + " Bag Added", Toast.LENGTH_SHORT).show();
                                requestFocus(mBagNumber);
                            }
                        } else {
                            Toast.makeText(LinehaulActivity.this, "Bag not belongs to mawb", Toast.LENGTH_SHORT).show();

                           // bagsList.add(bagNumber);
                            mBagNumber.setText("");
                            requestFocus(mBagNumber);
                        }
                    } else {
                        Toast.makeText(LinehaulActivity.this, "Enter Bag Number", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public void onBackPressed() {
      /*  Intent intent = new Intent(new Intent(LinehaulActivity.this, HomeActivity.class));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
        finish();
    }

    public void userRiderLog() {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;

        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo("");
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId("");
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo("");
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("8");
        riderLogRequest.setmLastKm("");
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("3");
        riderLogRequest.setActionNumber(8);
        riderLogRequest.setOsVersion(androidOS);
        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(LinehaulActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(LinehaulActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LinehaulActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(LinehaulActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("LONG_LINE",String.valueOf(latitude)+""+String.valueOf(longitude));

                } else {
                    // mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void getOrigins(String action) {
        OriginRequest originRequest = new OriginRequest();
        originRequest.setAction("get_origins");
        originRequest.setUserId(userId);
        originRequest.setLatitude(String.valueOf(latitude));
        originRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new LinehaulPresenterImpl(this, new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl());
        mPresenter.getAllOrigins(token, originRequest);
    }

    public void getDestinations() {
        DestinationsRequest destinationsRequest = new DestinationsRequest();
        destinationsRequest.setAction("get_destinations");
        destinationsRequest.setUserId(userId);
        destinationsRequest.setLatitude(String.valueOf(latitude));
        destinationsRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new LinehaulPresenterImpl(this, new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl());
        mPresenter.getAllDestinations(token, destinationsRequest);
    }

    public void getMawbNumbers() {
        GetMawbRequest getMawbRequest = new GetMawbRequest();
        getMawbRequest.setAction("get_mwab_no");
        getMawbRequest.setOriginHub(mCriticalogSharedPreferences.getData("origin_hub_1"));
        getMawbRequest.setDestHub(mCriticalogSharedPreferences.getData("dest_hub_1"));
        getMawbRequest.setUserId(userId);
        getMawbRequest.setLatitude(String.valueOf(latitude));
        getMawbRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new LinehaulPresenterImpl(this, new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl());
        mPresenter.getMawbNumbers(token, getMawbRequest);
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(LinehaulActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source
                        Toast.makeText(LinehaulActivity.this, item.getTitle(),
                                Toast.LENGTH_SHORT).show();
                        if (fromWhere.equalsIgnoreCase("origin")) {
                            mOrigin.setText(item.getTitle() + ": " + datumOrigins.get(createOriginDataString.indexOf(item.getTitle())).getHubCode());
                            mCriticalogSharedPreferences.saveData("origin_hub_1", datumOrigins.get(createOriginDataString.indexOf(item.getTitle())).getHubCode());
                        } else if (fromWhere.equalsIgnoreCase("destination")) {
                            mDestination.setText(item.getTitle() + ": " + datumDestination.get(createOriginDataString.indexOf(item.getTitle())).getHubCode());
                            mCriticalogSharedPreferences.saveData("dest_hub_1", datumDestination.get(createOriginDataString.indexOf(item.getTitle())).getHubCode());
                        } else {
                            mAirwayBill.setText(item.getTitle() + ": " + mawbNumbersList.get(createOriginDataString.indexOf(item.getTitle())).getMode());
                            mCriticalogSharedPreferences.saveData("mawb_num_1", mawbNumbersList.get(createOriginDataString.indexOf(item.getTitle())).getMwabNo());

                            mCriticalogSharedPreferences.saveData("air_surface", mawbNumbersList.get(createOriginDataString.indexOf(item.getTitle())).getMode());
                            mCriticalogSharedPreferences.saveData("image_captured", "");

                            if (mawbNumbersList.get(createOriginDataString.indexOf(item.getTitle())).getMode().equalsIgnoreCase("A")) {
                                mFlightText.setVisibility(View.VISIBLE);
                                mATAText.setVisibility(View.VISIBLE);
                                mATDText.setVisibility(View.VISIBLE);
                                mFlightNo.setVisibility(View.VISIBLE);
                                ATA.setVisibility(View.VISIBLE);
                                ATD.setVisibility(View.VISIBLE);
                                /*mBagNumberLayoutText.setVisibility(View.GONE);
                                mBagEnterLayout.setVisibility(View.GONE);
                                mRvDockets.setVisibility(View.GONE);
                                mListOfDocketsText.setVisibility(View.GONE);*/
                                mBagNumberLayoutText.setVisibility(View.VISIBLE);
                                mBagEnterLayout.setVisibility(View.VISIBLE);
                                mRvDockets.setVisibility(View.VISIBLE);
                                mListOfDocketsText.setVisibility(View.VISIBLE);

                                bagsListFromAPI.clear();
                                docketModelWithFlags.clear();
                                bagsList.clear();
                                getDocketListAPI();

                            } else {
                                mFlightText.setVisibility(View.GONE);
                                mATAText.setVisibility(View.GONE);
                                mATDText.setVisibility(View.GONE);
                                mFlightNo.setVisibility(View.GONE);
                                ATA.setVisibility(View.GONE);
                                ATD.setVisibility(View.GONE);
                                mBagNumberLayoutText.setVisibility(View.VISIBLE);
                                mBagEnterLayout.setVisibility(View.VISIBLE);
                                mRvDockets.setVisibility(View.VISIBLE);
                                mListOfDocketsText.setVisibility(View.VISIBLE);

                                bagsListFromAPI.clear();
                                docketModelWithFlags.clear();
                                bagsList.clear();

                                getDocketListAPI();

                                Toast.makeText(LinehaulActivity.this, "Surface", Toast.LENGTH_SHORT).show();
                            }
                        }

                        dialog.dismiss();
                    }
                }).show();
    }

    public void getDocketListAPI() {
        DocketListRequest docketListPostData = new DocketListRequest();
        docketListPostData.setAction("get_docket_list");
        docketListPostData.setUserId(userId);
        docketListPostData.setAirwayBill(mCriticalogSharedPreferences.getData("mawb_num_1"));
        docketListPostData.setLatitude(String.valueOf(latitude));
        docketListPostData.setLongitude(String.valueOf(longitude));

        mPresenter = new LinehaulPresenterImpl(this, new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl());
        mPresenter.getDocketNumbers(token, docketListPostData);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mOrigin:
                createOriginData.clear();
                datumOrigins.clear();
                createOriginDataString.clear();
                mCriticalogSharedPreferences.saveData("from_where", "origin");
                getOrigins("get_origins");
                break;
            case R.id.mDestination:
                createOriginData.clear();
                datumOrigins.clear();
                createOriginDataString.clear();
                mCriticalogSharedPreferences.saveData("from_where", "destination");
                getDestinations();
                break;
            case R.id.mAirwayBill:
                mawbNumbersList.clear();
                createMawbData.clear();
                createOriginData.clear();
                createOriginDataString.clear();
                getMawbNumbers();

                break;

            case R.id.mBack:
               /* Intent intent = new Intent(LinehaulActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                finish();
                break;

            case R.id.mAddBag:
                String bagNumber = mBagNumber.getText().toString();

                if (!bagNumber.equals("")) {
                    if (bagsListFromAPI.contains(bagNumber)) {

                        if (bagsList.contains(bagNumber)) {
                            Toast.makeText(LinehaulActivity.this, "Bag already Scaned", Toast.LENGTH_SHORT).show();
                        } else {
                            bagsList.add(bagNumber);
                            mBagNumber.setText("");
                            int rightMarkIndex = bagsListFromAPI.indexOf(bagNumber);

                            docketModelWithFlags.remove(rightMarkIndex);

                            docketModelWithFlags.add(rightMarkIndex, new DocketModelWithFlag(bagNumber, "true"));

                            DocketsAdapter docketsAdapter = new DocketsAdapter(LinehaulActivity.this, docketModelWithFlags,0);
                            mRvDockets.setLayoutManager(new GridLayoutManager(LinehaulActivity.this, 2));
                            //mRvDockets.setAdapter(docketsAdapter);
                            docketsAdapter.notifyDataSetChanged();

                            Toast.makeText(LinehaulActivity.this, bagNumber + " Bag Added", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LinehaulActivity.this, "Bag not belongs to mawb", Toast.LENGTH_SHORT).show();

                        bagsList.add(bagNumber);
                        mBagNumber.setText("");
                    }
                } else {
                    Toast.makeText(LinehaulActivity.this, "Enter Bag Number", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.mBarcodeScanBag:
                startBarcodeScan();
                break;
            case R.id.mAta:
                showTimePickerDialog("ATA");
                break;
            case R.id.mAtd:
                showTimePickerDialog("ATD");
                break;

            case R.id.mReceived:
                mReceived.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mOrigin.getText().toString().equals("")) {
                            Toast.makeText(LinehaulActivity.this, "Select Origin", Toast.LENGTH_SHORT).show();
                        } else if (mDestination.getText().toString().equals("")) {
                            Toast.makeText(LinehaulActivity.this, "Select Destination", Toast.LENGTH_SHORT).show();
                        } else if (mAirwayBill.getText().toString().equals("")) {
                            Toast.makeText(LinehaulActivity.this, "Select MAWB Number", Toast.LENGTH_SHORT).show();
                        } else if (bagsList.size() == 0) {
                            Toast.makeText(LinehaulActivity.this, "Select Bags", Toast.LENGTH_SHORT).show();
                        } else if (mCriticalogSharedPreferences.getData("air_surface").equalsIgnoreCase("S")) {

                            if (bagsList.size() < bagsListFromAPI.size()) {
                                // lineHaulBagMissDialog("");

                                if (mCriticalogSharedPreferences.getData("image_captured").equalsIgnoreCase("")) {
                                    lineHaulBagMissDialog("");
                                    Toast.makeText(LinehaulActivity.this, "Enable Dialog", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(LinehaulActivity.this, "Linehaul Received", Toast.LENGTH_SHORT).show();
                                    lineHaulReceivedAPI("");
                                }
                            } else {
                                Toast.makeText(LinehaulActivity.this, "Linehaul Received", Toast.LENGTH_SHORT).show();
                                lineHaulReceivedAPI("");
                            }

                        } else {
                            if (mFlightNo.getText().toString().equals("")) {
                                Toast.makeText(LinehaulActivity.this, "Enter Flight Number", Toast.LENGTH_SHORT).show();
                            } else if (ATD.getText().toString().equals("")) {
                                Toast.makeText(LinehaulActivity.this, "Enter Expected Departure", Toast.LENGTH_SHORT).show();
                            } else if (ATA.getText().toString().equals("")) {
                                Toast.makeText(LinehaulActivity.this, "Enter Expected Arrival", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LinehaulActivity.this, "Linehaul Received", Toast.LENGTH_SHORT).show();
                                lineHaulReceivedAPI(mFlightNo.getText().toString());
                            }
                        }
                    }
                });
                break;

        }
    }

    public void lineHaulReceivedAPI(String flightNo) {
        LinehaulReceivePostData linehaulReceivePostData = new LinehaulReceivePostData();
        linehaulReceivePostData.setAction("update_linehaul_received");
        linehaulReceivePostData.setAirSurface(mCriticalogSharedPreferences.getData("air_surface"));
        linehaulReceivePostData.setAirwayBill(mCriticalogSharedPreferences.getData("mawb_num_1"));
        linehaulReceivePostData.setDocketList(bagsList);
        linehaulReceivePostData.setDestHub(mCriticalogSharedPreferences.getData("dest_hub_1"));
        linehaulReceivePostData.setImageData(base64CapturedImage);
        linehaulReceivePostData.setUserId(userId);
        linehaulReceivePostData.setEta(mCriticalogSharedPreferences.getData("ATA"));
        linehaulReceivePostData.setEtd(mCriticalogSharedPreferences.getData("ATD"));
        linehaulReceivePostData.setFlightNo(flightNo);
        linehaulReceivePostData.setOriginHub(mCriticalogSharedPreferences.getData("origin_hub_1"));
        linehaulReceivePostData.setLatitude(String.valueOf(latitude));
        linehaulReceivePostData.setLongitude(String.valueOf(longitude));
        mPresenter = new LinehaulPresenterImpl(this, new LinehaulInteractorImpl(), new LinehaulInteractorImpl(), new LinehaulInteractorImpl(),
                new LinehaulInteractorImpl(), new LinehaulInteractorImpl());
        mPresenter.linehaulReceived(token, linehaulReceivePostData);
    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Box Number");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setOriginDataToViews(OriginResponse originResponse) {
        if (originResponse.getStatus() == 200) {
            datumOrigins = originResponse.getData();
            if (datumOrigins.size() != 0) {
                for (int i = 0; i < datumOrigins.size(); i++) {
                    createOriginData.add(new SampleSearchModel(datumOrigins.get(i).getHubName()));
                    createOriginDataString.add(datumOrigins.get(i).getHubName());

                }
                showOriginDestination(createOriginData, mCriticalogSharedPreferences.getData("from_where"));
            }
        }
    }

    @Override
    public void setDestiationDataToViews(DestinationsResponse destinationsResponse) {
        if (destinationsResponse.getStatus() == 200) {
            datumDestination = destinationsResponse.getData();

            if (datumDestination.size() != 0) {
                for (int i = 0; i < datumDestination.size(); i++) {
                    createOriginData.add(new SampleSearchModel(datumDestination.get(i).getHubName()));
                    createOriginDataString.add(datumDestination.get(i).getHubName());

                }
                showOriginDestination(createOriginData, mCriticalogSharedPreferences.getData("from_where"));
            }
        }
    }

    @Override
    public void setMawbDataToViews(GetMawbResponse mawbDataToViews) {

        if (mawbDataToViews.getStatus() == 200) {
            mawbNumbersList = mawbDataToViews.getData();

            if (mawbNumbersList.size() != 0) {
                for (int i = 0; i < mawbNumbersList.size(); i++) {
                    createMawbData.add(new SampleSearchModel(mawbNumbersList.get(i).getMwabNo()));
                    createOriginDataString.add(mawbNumbersList.get(i).getMwabNo());
                }
                showOriginDestination(createMawbData, "MAWB Number");
            }
        } else {

            mAirwayBill.setText("");
            Toast.makeText(LinehaulActivity.this, "No Mawb Numbers", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void setDocketNumbersToViews(DocketListResponse docketListResponse) {


        if (docketListResponse.getStatus() == 200) {
            ArrayList<MultiSelectModel> listOfDockets = new ArrayList<>();
            List<DatumDocket> datumDockets = docketListResponse.getData().getDockets();
            for (int i = 0; i < datumDockets.size(); i++) {
                listOfDockets.add(new MultiSelectModel(i + 1, String.valueOf(datumDockets.get(i).getDocketNo())));
                bagsListFromAPI.add(String.valueOf(datumDockets.get(i).getDocketNo()));
                Log.e("BAG_NUMBER", bagsListFromAPI.get(0));

                DocketModelWithFlag docketModelWithFlag = new DocketModelWithFlag();
                docketModelWithFlag.setDocketNumber(String.valueOf(datumDockets.get(i).getDocketNo()));
                docketModelWithFlag.setDocketFalg("");

                docketModelWithFlags.add(docketModelWithFlag);

               /* DocketsAdapter docketsAdapter = new DocketsAdapter(LinehaulActivity.this, docketModelWithFlags,0);
                mRvDockets.setLayoutManager(new GridLayoutManager(LinehaulActivity.this, 2));
                mRvDockets.setAdapter(docketsAdapter);*/
            }

            DocketsAdapter docketsAdapter = new DocketsAdapter(LinehaulActivity.this, docketModelWithFlags,0);
            mRvDockets.setLayoutManager(new GridLayoutManager(LinehaulActivity.this, 2));
            mRvDockets.setAdapter(docketsAdapter);

            if (mCriticalogSharedPreferences.getData("air_surface").equalsIgnoreCase("A")) {
             //   showMultiSelectDialog(listOfDockets);



                mFlightNo.setText(docketListResponse.getData().getFlightDetails().getFlightNo());
                ATA.setText(docketListResponse.getData().getFlightDetails().getArrivalTime());
                ATD.setText(docketListResponse.getData().getFlightDetails().getDepartureTime());
            }
        }

//-----------------------------
    }

    public void showMultiSelectDialog(ArrayList<MultiSelectModel> listOfDockets) {
        //MultiSelectModel
        ArrayList<Integer> alreadySelectedCountries = new ArrayList<>();


        MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .title("Select Bag Numbers") //setting title for dialog
                .titleSize(25)
                .positiveText("Done")
                .negativeText("Cancel")
                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                .setMaxSelectionLimit(listOfDockets.size()) //you can set maximum checkbox selection limit (Optional)
                .preSelectIDsList(alreadySelectedCountries) //List of ids that you need to be selected
                .multiSelectList(listOfDockets) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        //will return list of selected IDS
                        for (int i = 0; i < selectedIds.size(); i++) {
                           /* Toast.makeText(LinehaulActivity.this, "Selected Ids : " + selectedIds.get(i) + "\n" +
                                    "Selected Names : " + selectedNames.get(i) + "\n" +
                                    "DataString : " + dataString, Toast.LENGTH_SHORT).show();*/

                            bagsList = selectedNames;
                        }
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LinehaulActivity.this, "Not Selected any Bags", Toast.LENGTH_SHORT).show();
                        //  Log.d(TAG,"Dialog cancelled");
                    }
                });

        multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
    }

    @Override
    public void setLinehaulReceivedDataToViews(LinehaulReceivedResponse linehaulReceivedDataToViews) {
        if (linehaulReceivedDataToViews.getStatus() == 200) {

            Toast.makeText(LinehaulActivity.this, linehaulReceivedDataToViews.getMessage(), Toast.LENGTH_SHORT).show();
            userRiderLog();
            startActivity(new Intent(LinehaulActivity.this, HomeActivity.class));
        } else {
            Toast.makeText(LinehaulActivity.this, linehaulReceivedDataToViews.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(LinehaulActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1888) {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                //  Bitmap photo = (Bitmap) data.getExtras().get("data");

                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    captureImage.setImageBitmap(myBitmap);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CapturedImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                }
                mCriticalogSharedPreferences.saveData("image_captured", "true");
            }
        } else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

            if (result.getContents() == null) {
                Toast.makeText(LinehaulActivity.this, "Barcode Scan Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                String bagNumber = result.getContents().toString();
                if (!bagNumber.equals("")) {
                    if (bagsListFromAPI.contains(bagNumber)) {

                        if (bagsList.contains(bagNumber)) {
                            Toast.makeText(LinehaulActivity.this, "Bag already Scaned", Toast.LENGTH_SHORT).show();
                        } else {
                            bagsList.add(bagNumber);
                            mBagNumber.setText("");

                            int rightMarkIndex = bagsListFromAPI.indexOf(bagNumber);

                            docketModelWithFlags.remove(rightMarkIndex);

                            docketModelWithFlags.add(rightMarkIndex, new DocketModelWithFlag(bagNumber, "true"));

                            DocketsAdapter docketsAdapter = new DocketsAdapter(LinehaulActivity.this, docketModelWithFlags,0);
                            mRvDockets.setLayoutManager(new GridLayoutManager(LinehaulActivity.this, 2));
                            docketsAdapter.notifyDataSetChanged();
                            // mRvDockets.setAdapter(docketsAdapter);
                            Toast.makeText(LinehaulActivity.this, bagNumber + " Bag Added", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LinehaulActivity.this, "Bag not belongs to mawb", Toast.LENGTH_SHORT).show();

                     /*   bagsList.add(bagNumber);
                        mBagNumber.setText("");*/
                    }
                } else {
                    Toast.makeText(LinehaulActivity.this, "Enter Bag Number", Toast.LENGTH_SHORT).show();
                }

            }


        }

    }


    public void lineHaulBagMissDialog(String fromATAATD) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);

        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_linehaulbagmiss);

        captureImage = dialog.findViewById(R.id.mTakePicture);
        Button ok = dialog.findViewById(R.id.mOk);

        TextView textBagsScanned = dialog.findViewById(R.id.mBagAnalyzeText);

        textBagsScanned.setText(String.valueOf(bagsList.size()) + " Bag Scanned Out Of " + String.valueOf(bagsListFromAPI.size()));

        dialog.setCancelable(false);
        dialog.show();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        captureImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                checkRunTimePermission();

                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    openCameraIntent();

                }
            }
        });
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {

            openCameraIntent();
            // if already permition granted
            // PUT YOUR ACTION (Like Open cemara etc..)
        }
    }

    public void showTimePickerDialog(String fromATAATD) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        TextView OK;
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_timepicker);

        OK = dialog.findViewById(R.id.mOk);
        timePicker = dialog.findViewById(R.id.timePicker);
        dialog.show();
        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                int hour = timePicker.getCurrentHour();
                int minute = timePicker.getCurrentMinute();

                if (fromATAATD.equals("ATA")) {
                    ATA.setText(String.valueOf(hour) + ": " + String.valueOf(minute));
                    mCriticalogSharedPreferences.saveData("ATA", String.valueOf(hour) + ": " + String.valueOf(minute));
                } else {
                    ATD.setText(String.valueOf(hour) + ": " + String.valueOf(minute));
                    mCriticalogSharedPreferences.saveData("ATD", String.valueOf(hour) + ": " + String.valueOf(minute));
                }

                Toast.makeText(LinehaulActivity.this, String.valueOf(hour) + ":" + String.valueOf(minute), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                //Uri photoURI = FileProvider.getUriForFile(this,".provider",photoFile);

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

}
