package com.criticalog.ecritica.MVPLinehaul;

import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivePostData;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivedResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginResponse;

public interface LinehaulCotract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void getAllOrigins(String token, OriginRequest originRequest);

        void getAllDestinations(String token, DestinationsRequest destinationsRequest);

        void getMawbNumbers(String token, GetMawbRequest getMawbRequest);

        void getDocketNumbers(String token,DocketListRequest docketListRequest);

        void linehaulReceived(String token, LinehaulReceivePostData linehaulReceivePostData);


    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setOriginDataToViews(OriginResponse originResponse);

        void setDestiationDataToViews(DestinationsResponse destinationsResponse);

        void setMawbDataToViews(GetMawbResponse mawbDataToViews);

        void setDocketNumbersToViews(DocketListResponse docketListResponse);

        void setLinehaulReceivedDataToViews(LinehaulReceivedResponse linehaulReceivedDataToViews);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface LinehaulOriginIntractor {

        interface OnFinishedListener {
            void onFinished(OriginResponse originResponse);

            void onFailure(Throwable t);
        }

        void getOriginsSuccess(LinehaulCotract.LinehaulOriginIntractor.OnFinishedListener onFinishedListener, String token, OriginRequest originRequest);
    }

    interface LinehaulDestinationIntractor {

        interface OnFinishedListener {
            void onFinished(DestinationsResponse destinationsResponse);

            void onFailure(Throwable t);
        }

        void getDestinationsSuccess(LinehaulCotract.LinehaulDestinationIntractor.OnFinishedListener onFinishedListener, String token, DestinationsRequest destinationsRequest);
    }

    interface LinehaulMawbIntractor {

        interface OnFinishedListener {
            void onFinished(GetMawbResponse getMawbResponse);

            void onFailure(Throwable t);
        }

        void getMawbNumbersSuccess(LinehaulCotract.LinehaulMawbIntractor.OnFinishedListener onFinishedListener, String token, GetMawbRequest getMawbRequest);
    }

    interface LinehaulDocketNumberIntractor {
        interface OnFinishedListener {
            void onFinished(DocketListResponse docketListResponse);

            void onFailure(Throwable t);
        }

        void getDocketNumbersSuccess(LinehaulCotract.LinehaulDocketNumberIntractor.OnFinishedListener onFinishedListener, String token, DocketListRequest docketListRequest);
    }
    interface LinehaulReceivedIntractor {
        interface OnFinishedListener {
            void onFinished(LinehaulReceivedResponse linehaulReceivedResponse);

            void onFailure(Throwable t);
        }

        void getLinehaulReceivedSuccess(LinehaulCotract.LinehaulReceivedIntractor.OnFinishedListener onFinishedListener, String token, LinehaulReceivePostData linehaulReceivePostData);
    }
}
