package com.criticalog.ecritica.MVPLinehaul.LinehaulModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumDocket {
    @SerializedName("docket_no")
    @Expose
    private String docketNo;

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }
}
