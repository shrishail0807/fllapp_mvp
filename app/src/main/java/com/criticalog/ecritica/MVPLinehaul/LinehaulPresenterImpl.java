package com.criticalog.ecritica.MVPLinehaul;

import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivePostData;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivedResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginResponse;

public class LinehaulPresenterImpl implements LinehaulCotract.presenter, LinehaulCotract.LinehaulOriginIntractor.OnFinishedListener,
        LinehaulCotract.LinehaulDestinationIntractor.OnFinishedListener, LinehaulCotract.LinehaulMawbIntractor.OnFinishedListener, LinehaulCotract.LinehaulDocketNumberIntractor.OnFinishedListener, LinehaulCotract.LinehaulReceivedIntractor.OnFinishedListener {
    LinehaulCotract.MainView mainView;
    LinehaulCotract.LinehaulOriginIntractor linehaulIntractor;
    LinehaulCotract.LinehaulDestinationIntractor linehaulDestinationIntractor;
    LinehaulCotract.LinehaulMawbIntractor linehaulMawbIntractor;
    LinehaulCotract.LinehaulDocketNumberIntractor linehaulDocketNumberIntractor;
    LinehaulCotract.LinehaulReceivedIntractor linehaulReceivedIntractor;

    public LinehaulPresenterImpl(LinehaulCotract.MainView mainView, LinehaulCotract.LinehaulOriginIntractor linehaulIntractor,
                                 LinehaulCotract.LinehaulDestinationIntractor linehaulDestinationIntractor, LinehaulCotract.LinehaulMawbIntractor linehaulMawbIntractor,
                                 LinehaulCotract.LinehaulDocketNumberIntractor linehaulDocketNumberIntractor,
                                 LinehaulCotract.LinehaulReceivedIntractor linehaulReceivedIntractor) {
        this.mainView = mainView;
        this.linehaulIntractor = linehaulIntractor;
        this.linehaulDestinationIntractor = linehaulDestinationIntractor;
        this.linehaulMawbIntractor = linehaulMawbIntractor;
        this.linehaulDocketNumberIntractor = linehaulDocketNumberIntractor;
        this.linehaulReceivedIntractor=linehaulReceivedIntractor;
    }

    @Override
    public void onDestroy() {

        mainView = null;
    }

    @Override
    public void getAllOrigins(String token, OriginRequest originRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        linehaulIntractor.getOriginsSuccess(this, token, originRequest);
    }

    @Override
    public void getAllDestinations(String token, DestinationsRequest destinationsRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        linehaulDestinationIntractor.getDestinationsSuccess(this, token, destinationsRequest);
    }

    @Override
    public void getMawbNumbers(String token, GetMawbRequest getMawbRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        linehaulMawbIntractor.getMawbNumbersSuccess(this, token, getMawbRequest);
    }

    @Override
    public void getDocketNumbers(String token, DocketListRequest docketListRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        linehaulDocketNumberIntractor.getDocketNumbersSuccess(this, token, docketListRequest);
    }

    @Override
    public void linehaulReceived(String token, LinehaulReceivePostData linehaulReceivePostData) {
        if (mainView != null) {
            mainView.showProgress();
        }
        linehaulReceivedIntractor.getLinehaulReceivedSuccess(this, token, linehaulReceivePostData);
    }

    @Override
    public void onFinished(OriginResponse originResponse) {
        if (mainView != null) {
            mainView.setOriginDataToViews(originResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(DestinationsResponse destinationsResponse) {
        if (mainView != null) {
            mainView.setDestiationDataToViews(destinationsResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(GetMawbResponse getMawbResponse) {
        if (mainView != null) {
            mainView.setMawbDataToViews(getMawbResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(DocketListResponse docketListResponse) {
        if (mainView != null) {
            mainView.setDocketNumbersToViews(docketListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(LinehaulReceivedResponse linehaulReceivedResponse) {
        if (mainView != null) {
            mainView.setLinehaulReceivedDataToViews(linehaulReceivedResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
