package com.criticalog.ecritica.MVPLinehaul.LinehaulModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {
    @SerializedName("hub_id")
    @Expose
    private String hubId;
    @SerializedName("hub_code")
    @Expose
    private String hubCode;
    @SerializedName("hub_name")
    @Expose
    private String hubName;
    @SerializedName("is_default")
    @Expose
    private String isDefault;

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getHubCode() {
        return hubCode;
    }

    public void setHubCode(String hubCode) {
        this.hubCode = hubCode;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

}
