package com.criticalog.ecritica.MVPLinehaul.model;

import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DatumDocket;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.FlightDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataDocketResponse {
    @SerializedName("dockets")
    @Expose
    private List<DatumDocket> dockets = null;
    @SerializedName("flight_details")
    @Expose
    private FlightDetails flightDetails;

    public List<DatumDocket> getDockets() {
        return dockets;
    }

    public void setDockets(List<DatumDocket> dockets) {
        this.dockets = dockets;
    }

    public FlightDetails getFlightDetails() {
        return flightDetails;
    }

    public void setFlightDetails(FlightDetails flightDetails) {
        this.flightDetails = flightDetails;
    }
}
