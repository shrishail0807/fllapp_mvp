package com.criticalog.ecritica.MVPPickupConfirm;

import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSARequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSAResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickupContractContractInteractorImpl implements PickupConfirmContract.GetVendorListIntractor,PickupConfirmContract.GetPickupRgsaListIntractor,
PickupConfirmContract.GetRGSAPickupConfirmIntractor, PickupConfirmContract.GetRGSABaggingValidateIntractor{
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);


    @Override
    public void vendorList(PickupConfirmContract.GetVendorListIntractor.OnFinishedListener onFinishedListener, VendorListRequest vendorListRequest) {
        Call<VendorListResponse> vendorListResponseCall=services.vendorList(vendorListRequest);

        vendorListResponseCall.enqueue(new Callback<VendorListResponse>() {
            @Override
            public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<VendorListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void pickupRgsaList(PickupConfirmContract.GetPickupRgsaListIntractor.OnFinishedListener onFinishedListener, PickupRGSARequest pickupRGSARequest) {

        Call<PickupRGSAResponse> pickupRGSAResponseCall=services.rgsaPickupList(pickupRGSARequest);
        pickupRGSAResponseCall.enqueue(new Callback<PickupRGSAResponse>() {
            @Override
            public void onResponse(Call<PickupRGSAResponse> call, Response<PickupRGSAResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PickupRGSAResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void rgsaPickupConfirm(PickupConfirmContract.GetRGSAPickupConfirmIntractor.OnFinishedListener onFinishedListener, RGSAPickupConfirmRequest rgsaPickupConfirmRequest) {
        Call<RGSAPickupConfirmResponse> rgsaPickupConfirmResponseCall=services.rgsaPickupConfirm(rgsaPickupConfirmRequest);
        rgsaPickupConfirmResponseCall.enqueue(new Callback<RGSAPickupConfirmResponse>() {
            @Override
            public void onResponse(Call<RGSAPickupConfirmResponse> call, Response<RGSAPickupConfirmResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<RGSAPickupConfirmResponse> call, Throwable t) {
             onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void rgsaBaggingCheck(PickupConfirmContract.GetRGSABaggingValidateIntractor.OnFinishedListener onFinishedListener, BagCheckRequest bagCheckRequest) {
        Call<BagCheckResponse> bagCheckResponseCall=services.baggingBag(bagCheckRequest);
        bagCheckResponseCall.enqueue(new Callback<BagCheckResponse>() {
            @Override
            public void onResponse(Call<BagCheckResponse> call, Response<BagCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<BagCheckResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
