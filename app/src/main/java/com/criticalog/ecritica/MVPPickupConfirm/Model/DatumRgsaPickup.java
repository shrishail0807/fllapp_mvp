package com.criticalog.ecritica.MVPPickupConfirm.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DatumRgsaPickup implements Serializable {
    @SerializedName("PICK_TYPE")
    @Expose
    private String pickType;
    @SerializedName("PRS")
    @Expose
    private String prs;
    @SerializedName("PICKUP_ID")
    @Expose
    private String pickupId;
    @SerializedName("BOOKING_ID")
    @Expose
    private String bookingId;
    @SerializedName("XDKTNO")
    @Expose
    private String xdktno;
    @SerializedName("XCLIENT")
    @Expose
    private String xclient;
    @SerializedName("XBOXNUM")
    @Expose
    private String xboxnum;
    @SerializedName("ORIGINPINCODE")
    @Expose
    private String originpincode;
    @SerializedName("DESTPINCODE")
    @Expose
    private String destpincode;
    @SerializedName("XACTUALVAL")
    @Expose
    private String xactualval;
    @SerializedName("XCREUSERDT")
    @Expose
    private String xcreuserdt;

    public String getPickType() {
        return pickType;
    }

    public void setPickType(String pickType) {
        this.pickType = pickType;
    }

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }

    public String getPickupId() {
        return pickupId;
    }

    public void setPickupId(String pickupId) {
        this.pickupId = pickupId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getXdktno() {
        return xdktno;
    }

    public void setXdktno(String xdktno) {
        this.xdktno = xdktno;
    }

    public String getXclient() {
        return xclient;
    }

    public void setXclient(String xclient) {
        this.xclient = xclient;
    }

    public String getXboxnum() {
        return xboxnum;
    }

    public void setXboxnum(String xboxnum) {
        this.xboxnum = xboxnum;
    }

    public String getOriginpincode() {
        return originpincode;
    }

    public void setOriginpincode(String originpincode) {
        this.originpincode = originpincode;
    }

    public String getDestpincode() {
        return destpincode;
    }

    public void setDestpincode(String destpincode) {
        this.destpincode = destpincode;
    }

    public String getXactualval() {
        return xactualval;
    }

    public void setXactualval(String xactualval) {
        this.xactualval = xactualval;
    }

    public String getXcreuserdt() {
        return xcreuserdt;
    }

    public void setXcreuserdt(String xcreuserdt) {
        this.xcreuserdt = xcreuserdt;
    }
}
