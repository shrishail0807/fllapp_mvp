package com.criticalog.ecritica.MVPPickupConfirm.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Adapters.RulesAdapterWithFlag;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPDRS.Adapter.RulesAdapter;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.MVPPickupConfirm.Model.DatumRgsaPickup;
import com.criticalog.ecritica.MVPPickupConfirm.PickupConfirmActivity;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

public class PickupConfirmAdapter extends RecyclerView.Adapter<PickupConfirmAdapter.ViewHolder> {
    private Context context;
    int checkAll;
    List<DatumRgsaPickup> datumRgsaPickupList;
    RulesClicked rulesClicked;

    public PickupConfirmAdapter(Context context, int checkAll,List<DatumRgsaPickup> datumRgsaPickupList,RulesClicked rulesClicked) {
        this.context = context;
        this.checkAll = checkAll;
        this.datumRgsaPickupList=datumRgsaPickupList;
        this.rulesClicked=rulesClicked;
    }

    @NonNull
    @Override
    public PickupConfirmAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pickup_confirm_row, parent, false);
        return new PickupConfirmAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PickupConfirmAdapter.ViewHolder holder, int position) {

        holder.mDocketNo.setText(datumRgsaPickupList.get(position).getXdktno());
        holder.mOrigin.setText(datumRgsaPickupList.get(position).getOriginpincode());
        holder.mDestination.setText(datumRgsaPickupList.get(position).getDestpincode());
        if (checkAll == 1) {
            holder.mCheckYes.setChecked(true);
            rulesClicked.onclickRulesList("yes",position);
        } else if(checkAll ==0){
            holder.mCheckYes.setChecked(false);
            rulesClicked.onclickRulesList("no",position);
        }



        holder.mCheckYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {

                    holder.mCheckYes.setChecked(true);
                    rulesClicked.onclickRulesList("",position);

                } else {
                    holder.mCheckYes.setChecked(false);
                    rulesClicked.onclickRulesList("remove",position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datumRgsaPickupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mDocketNo,mOrigin,mDestination;
        CheckBox mCheckYes;

        public ViewHolder(View itemView) {
            super(itemView);
            mDocketNo = itemView.findViewById(R.id.mDocketNo);
            mCheckYes = itemView.findViewById(R.id.mCheckYes);
            mOrigin=itemView.findViewById(R.id.mOrigin);
            mDestination=itemView.findViewById(R.id.mDestination);
        }
    }


}
