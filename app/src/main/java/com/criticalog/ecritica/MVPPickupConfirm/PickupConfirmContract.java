package com.criticalog.ecritica.MVPPickupConfirm;

import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSARequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSAResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListResponse;

public interface PickupConfirmContract {
    interface presenter{

        void onDestroy();

        void getVendorList(VendorListRequest vendorListRequest);

        void getRgsaPickupList(PickupRGSARequest pickupRGSARequest);

        void rgsaPickupConfirmRequest(RGSAPickupConfirmRequest rgsaPickupConfirmRequest);

        void rgsaBagValidateRequest(BagCheckRequest bagCheckRequest);

        //void LoginRequestToServer(LoginPostData loginPostData);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setVendorDataToViews(VendorListResponse vendorDataToViews);

        void setPickupRgsaListToViews(PickupRGSAResponse pickupRGSAResponse);

        void setRGSAPickupConfirmResponse(RGSAPickupConfirmResponse rgsaPickupConfirmResponse);

        void setRgsaBaggingValidateResponse(BagCheckResponse bagCheckResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetVendorListIntractor {

        interface OnFinishedListener {
            void onFinished(VendorListResponse vendorListResponse);
            void onFailure(Throwable t);
        }
        void vendorList(OnFinishedListener onFinishedListener, VendorListRequest vendorListRequest);
    }

    interface GetPickupRgsaListIntractor {

        interface OnFinishedListener {
            void onFinished(PickupRGSAResponse pickupRGSAResponse);
            void onFailure(Throwable t);
        }
        void pickupRgsaList(OnFinishedListener onFinishedListener, PickupRGSARequest pickupRGSARequest);
    }

    interface GetRGSAPickupConfirmIntractor {

        interface OnFinishedListener {
            void onFinished(RGSAPickupConfirmResponse rgsaPickupConfirmResponse);
            void onFailure(Throwable t);
        }
        void rgsaPickupConfirm(OnFinishedListener onFinishedListener, RGSAPickupConfirmRequest rgsaPickupConfirmRequest);
    }

    interface GetRGSABaggingValidateIntractor {

        interface OnFinishedListener {
            void onFinished(BagCheckResponse bagCheckResponse);
            void onFailure(Throwable t);
        }
        void rgsaBaggingCheck(OnFinishedListener onFinishedListener, BagCheckRequest bagCheckRequest);
    }
}
