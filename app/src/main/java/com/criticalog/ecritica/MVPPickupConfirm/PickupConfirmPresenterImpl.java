package com.criticalog.ecritica.MVPPickupConfirm;

import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSARequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSAResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListResponse;

public class PickupConfirmPresenterImpl implements PickupConfirmContract.presenter, PickupConfirmContract.GetVendorListIntractor.OnFinishedListener, PickupConfirmContract.GetPickupRgsaListIntractor.OnFinishedListener, PickupConfirmContract.GetRGSAPickupConfirmIntractor.OnFinishedListener, PickupConfirmContract.GetRGSABaggingValidateIntractor.OnFinishedListener {

    private PickupConfirmContract.MainView mainView;
    private PickupConfirmContract.GetVendorListIntractor getVendorListIntractor;
    private PickupConfirmContract.GetPickupRgsaListIntractor getPickupRgsaListIntractor;
    private PickupConfirmContract.GetRGSAPickupConfirmIntractor getRGSAPickupConfirmIntractor;
    private PickupConfirmContract.GetRGSABaggingValidateIntractor getRGSABaggingValidateIntractor;

    public PickupConfirmPresenterImpl(PickupConfirmContract.MainView mainView,PickupConfirmContract.GetVendorListIntractor getVendorListIntractor,
                                      PickupConfirmContract.GetPickupRgsaListIntractor getPickupRgsaListIntractor,
                                      PickupConfirmContract.GetRGSAPickupConfirmIntractor getRGSAPickupConfirmIntractor,
                                      PickupConfirmContract.GetRGSABaggingValidateIntractor getRGSABaggingValidateIntractor)
    {
        this.mainView=mainView;
        this.getVendorListIntractor=getVendorListIntractor;
        this.getPickupRgsaListIntractor=getPickupRgsaListIntractor;
        this.getRGSAPickupConfirmIntractor=getRGSAPickupConfirmIntractor;
        this.getRGSABaggingValidateIntractor=getRGSABaggingValidateIntractor;
    }
    @Override
    public void onDestroy() {

    }

    @Override
    public void getVendorList(VendorListRequest vendorListRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getVendorListIntractor.vendorList(this,vendorListRequest);
    }

    @Override
    public void getRgsaPickupList(PickupRGSARequest pickupRGSARequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getPickupRgsaListIntractor.pickupRgsaList(this,pickupRGSARequest);
    }

    @Override
    public void rgsaPickupConfirmRequest(RGSAPickupConfirmRequest rgsaPickupConfirmRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getRGSAPickupConfirmIntractor.rgsaPickupConfirm(this,rgsaPickupConfirmRequest);
    }

    @Override
    public void rgsaBagValidateRequest(BagCheckRequest bagCheckRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getRGSABaggingValidateIntractor.rgsaBaggingCheck(this,bagCheckRequest);
    }

    @Override
    public void onFinished(VendorListResponse vendorListResponse) {

        if(mainView!=null)
        {
            mainView.setVendorDataToViews(vendorListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(PickupRGSAResponse pickupRGSAResponse) {
        if(mainView!=null)
        {
            mainView.setPickupRgsaListToViews(pickupRGSAResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(RGSAPickupConfirmResponse rgsaPickupConfirmResponse) {
        if(mainView!=null)
        {
            mainView.setRGSAPickupConfirmResponse(rgsaPickupConfirmResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(BagCheckResponse bagCheckResponse) {
        if(mainView!=null)
        {
            mainView.setRgsaBaggingValidateResponse(bagCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
