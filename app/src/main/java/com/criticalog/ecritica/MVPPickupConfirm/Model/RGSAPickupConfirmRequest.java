package com.criticalog.ecritica.MVPPickupConfirm.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RGSAPickupConfirmRequest implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("dockets")
    @Expose
    private List<Integer> dockets = null;
    @SerializedName("bags")
    @Expose
    private String bags;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<Integer> getDockets() {
        return dockets;
    }

    public void setDockets(List<Integer> dockets) {
        this.dockets = dockets;
    }

    public String getBags() {
        return bags;
    }

    public void setBags(String bags) {
        this.bags = bags;
    }

   /* @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("dockets")
    @Expose
    private List<Integer> dockets = null;
    @SerializedName("bags")
    @Expose
    private List<Integer> bags = null;
    @SerializedName("user_id")
    @Expose
    private String userId;
  *//*  @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("vendor_name")
    @Expose
    private String vendorName;*//*
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("etd")
    @Expose
    private String etd;
    @SerializedName("eta")
    @Expose
    private String eta;
    @SerializedName("comment")
    @Expose
    private String comment;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<Integer> getDockets() {
        return dockets;
    }

    public void setDockets(List<Integer> dockets) {
        this.dockets = dockets;
    }

    public List<Integer> getBags() {
        return bags;
    }

    public void setBags(List<Integer> bags) {
        this.bags = bags;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }*/

}
