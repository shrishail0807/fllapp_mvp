package com.criticalog.ecritica.MVPPickupConfirm;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Adapters.InscanBoxAdapter;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.MVPPickupConfirm.Adapter.PickupConfirmAdapter;
import com.criticalog.ecritica.MVPPickupConfirm.Model.DatumRgsaPickup;
import com.criticalog.ecritica.MVPPickupConfirm.Model.DatumVendor;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSARequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSAResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class PickupConfirmActivity extends AppCompatActivity implements View.OnClickListener, PickupConfirmContract.MainView, RulesClicked {
    private RecyclerView mRVPickupListConfirm;
    private PickupConfirmAdapter mPickupConfirmAdapter;
    private CheckBox mCheckYes;
    private TextView mTopBarText;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    EditText mSelectVendor;
    PickupConfirmContract.presenter mPresenter;
    ArrayList<DatumVendor> datumVendorList = new ArrayList<>();
    ArrayList<SampleSearchModel> vendorSearchList = new ArrayList();
    ArrayList<DatumRgsaPickup> datumRgsaPickupArrayList = new ArrayList<>();


    private String vehicleNumber="";
    ArrayList<String> stringArrayListVendror = new ArrayList<>();
    private LinearLayout mDocketLabelLay;
    SimpleArcDialog mProgressDialog1;
    private Button submit;
    private List<Integer> docketList = new ArrayList<>();
    private List<Integer> docketListSelect = new ArrayList<>();
    private List<Integer> bagList = new ArrayList<>();
    private CheckBox mCheckYesAll;
    private EditText mVehicleNumber;
    private ImageView mTvBackbutton;
    private Dialog dialogOtherEntry;
    private RecyclerView mRvBox;
    private InscanBoxAdapter mDocketsAdapter;
    private ArrayList scannedboxlist = new ArrayList();
    private Dialog dialogManualEntry;
    private RelativeLayout mRLManualBag, mQrScan;
    private String bagNumber = "";
    private LinearLayout mManualScanLayBag;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude = 0, longitude = 0;
    String bagNum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mvp_pickup_confirm);

        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);

        Toasty.warning(PickupConfirmActivity.this, "Kindly Please Make Sure the Clients are Closed.", Toast.LENGTH_LONG, true).show();

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        userId = mCriticalogSharedPreferences.getData("userId");
        mRVPickupListConfirm = findViewById(R.id.mRVPickupListConfirm);
        mTopBarText = findViewById(R.id.mTopBarText);
        mSelectVendor = findViewById(R.id.mSelectVendor);
        mDocketLabelLay = findViewById(R.id.mDocketLabelLay);
        submit = findViewById(R.id.submit);
        mCheckYesAll = findViewById(R.id.mCheckYesAll);
        mVehicleNumber = findViewById(R.id.mVehicleNumber);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mRvBox = findViewById(R.id.mRvBox);
        mRLManualBag = findViewById(R.id.mRLManualBag);
        mQrScan = findViewById(R.id.mQrScan);
        mManualScanLayBag = findViewById(R.id.mManualScanLayBag);

        submit.setOnClickListener(this);
        mCheckYesAll.setOnClickListener(this);

        mSelectVendor.setOnClickListener(this);
        mTvBackbutton.setOnClickListener(this);
        mRLManualBag.setOnClickListener(this);
        mQrScan.setOnClickListener(this);
        mSelectVendor.setFocusable(false);

        mTopBarText.setText("Confirm Pickup(RGSA)");
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        callRGSAPickupListAPI();
        mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mVehicleNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                vehicleNumber = s.toString();

                if (vehicleNumber.length() > 7  ) {
                    if((isVehicleNumber1(vehicleNumber)==false)&&(isVehicleNumber2(vehicleNumber)==false)&&(isVehicleNumber3(vehicleNumber)==false)&&(isVehicleNumber4(vehicleNumber)==false))
                    {
                        Toasty.warning(PickupConfirmActivity.this, "Enter Valid Vehicle Number", Toast.LENGTH_SHORT, true).show();
                    }

                }
            }
        });
        mCheckYesAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    if (datumRgsaPickupArrayList.size() != 0) {
                        mPickupConfirmAdapter = new PickupConfirmAdapter(PickupConfirmActivity.this, 1, datumRgsaPickupArrayList, PickupConfirmActivity.this);
                        mRVPickupListConfirm.setLayoutManager(new LinearLayoutManager(PickupConfirmActivity.this));
                        mRVPickupListConfirm.setAdapter(mPickupConfirmAdapter);
                        mPickupConfirmAdapter.notifyDataSetChanged();
                    }


                } else {
                    if (datumRgsaPickupArrayList.size() != 0) {
                        mPickupConfirmAdapter = new PickupConfirmAdapter(PickupConfirmActivity.this, 0, datumRgsaPickupArrayList, PickupConfirmActivity.this);
                        mRVPickupListConfirm.setLayoutManager(new LinearLayoutManager(PickupConfirmActivity.this));
                        mRVPickupListConfirm.setAdapter(mPickupConfirmAdapter);
                        mPickupConfirmAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(PickupConfirmActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(PickupConfirmActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PickupConfirmActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(PickupConfirmActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("LAT LONG_BAG", String.valueOf(latitude) + "" + String.valueOf(longitude));

                } else {
                    // mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }


    public void callBagValidate(String bagNumber) {
        bagList.clear();
        BagCheckRequest bagCheckRequest = new BagCheckRequest();
        bagCheckRequest.setAction("bagging_bag");
        bagCheckRequest.setBagNo(bagNumber);
        bagCheckRequest.setDestHub("");
        bagCheckRequest.setUserId(userId);
        bagCheckRequest.setOrgHub("");
        mPresenter = new PickupConfirmPresenterImpl(this, new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl());
        mPresenter.rgsaBagValidateRequest(bagCheckRequest);
    }

    public void manualDialogBag() {
        EditText mBagNum;
        TextView mSubmit;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(PickupConfirmActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_bag);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);


        mBagNum = dialogManualEntry.findViewById(R.id.mBagNum);
        mSubmit = dialogManualEntry.findViewById(R.id.mSubmit);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mBagNum.setHint("Enter Bag Number");
        mSubmit.setText("Submit");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String bagNumber = mBagNum.getText().toString();
                if (bagNumber.equalsIgnoreCase("")) {
                    Toasty.warning(PickupConfirmActivity.this, "Enter Valid Bag Number", Toast.LENGTH_LONG, true).show();
                } else if (bagNumber.length() == 7) {
                    callBagValidate(bagNumber);
                } else {
                    Toasty.warning(PickupConfirmActivity.this, "Enter Valid Bag Number", Toast.LENGTH_LONG, true).show();
                }
            }
        });

        /*mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketno = mDocketEntry.getText().toString();

                if (docketno.length() == 9) {
                    callInscanDocketCheck(docketno);
                } else {
                    Toasty.warning(InScanActivity.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });*/


    }

    public void manualDialogBox() {
        EditText mBoxNum;
        TextView mSubmit;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(PickupConfirmActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_box);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);


        mBoxNum = dialogManualEntry.findViewById(R.id.mBoxNum);
        mSubmit = dialogManualEntry.findViewById(R.id.mSubmit);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mBoxNum.setHint("Enter Bag Number");
        mSubmit.setText("Submit");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });


        /*mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketno = mDocketEntry.getText().toString();

                if (docketno.length() == 9) {
                    callInscanDocketCheck(docketno);
                } else {
                    Toasty.warning(InScanActivity.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });*/


    }

    public void otherDialog() {
        ImageView mCancelDialog;
        EditText mVendorName;
        TextView mSubmit;
        //Dialog Other Vendor Entry
        dialogOtherEntry = new Dialog(PickupConfirmActivity.this);
        dialogOtherEntry.setContentView(R.layout.dialog_other_vendor);
        dialogOtherEntry.setCanceledOnTouchOutside(false);
        dialogOtherEntry.setCancelable(false);
        mCancelDialog = dialogOtherEntry.findViewById(R.id.mCancelDialog);
        mVendorName = dialogOtherEntry.findViewById(R.id.mVendorName);
        mSubmit = dialogOtherEntry.findViewById(R.id.mSubmit);

        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOtherEntry.dismiss();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vendorName = mVendorName.getText().toString();

                if (vendorName.equalsIgnoreCase("")) {
                    Toasty.warning(PickupConfirmActivity.this, "Enter Vendor Name", Toast.LENGTH_LONG, true).show();
                } else {
                    mCriticalogSharedPreferences.saveData("vendor_name", vendorName);
                    dialogOtherEntry.dismiss();
                    callRGSAPickupListAPI();
                }

            }
        });
        dialogOtherEntry.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mSelectVendor:
                vendorSearchList.clear();
                datumVendorList.clear();
                VendorListRequest vendorListRequest = new VendorListRequest();
                vendorListRequest.setAction("get_vendor_list");
                vendorListRequest.setUserId(userId);

                mPresenter = new PickupConfirmPresenterImpl(this, new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl());
                mPresenter.getVendorList(vendorListRequest);
                break;
            case R.id.submit:
                vehicleNumber = mVehicleNumber.getText().toString();
               /* if (vehicleNumber.equalsIgnoreCase("")) {
                    Toasty.warning(this, "Enter Vehicle Number", Toast.LENGTH_LONG, true).show();
                }else if((isVehicleNumber1(vehicleNumber)==false)&&(isVehicleNumber2(vehicleNumber)==false)&&(isVehicleNumber3(vehicleNumber)==false)&&(isVehicleNumber4(vehicleNumber)==false)){
                    Toasty.warning(this, "Enter Valid Vehicle Number", Toast.LENGTH_SHORT, true).show();
                } else*/ if (docketListSelect.size() == 0) {
                    Toasty.warning(this, "You have not selected any dockets!!", Toast.LENGTH_LONG, true).show();
                } else {
                    RGSAPickupConfirmRequest rgsaPickupConfirmRequest = new RGSAPickupConfirmRequest();
                    rgsaPickupConfirmRequest.setAction("rgsa_confirm_bagging");
                    rgsaPickupConfirmRequest.setUserId(userId);
                   // rgsaPickupConfirmRequest.setVehicleNo(vehicleNumber);
                    rgsaPickupConfirmRequest.setDockets(docketListSelect);
                   // rgsaPickupConfirmRequest.setVendorName(mCriticalogSharedPreferences.getData("vendor_name"));
                   // rgsaPickupConfirmRequest.setVendorId(mCriticalogSharedPreferences.getData("vendor_id"));
                    rgsaPickupConfirmRequest.setBags(bagNum);
                   /* rgsaPickupConfirmRequest.setLat(String.valueOf(latitude));
                    rgsaPickupConfirmRequest.setLng(String.valueOf(longitude));*/

                    mPresenter = new PickupConfirmPresenterImpl(this, new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl());
                    mPresenter.rgsaPickupConfirmRequest(rgsaPickupConfirmRequest);
                }
                break;

            case R.id.mTvBackbutton:
                finish();
                break;

            case R.id.mRLManualBag:
                manualDialogBag();
                break;

            case R.id.mQrScan:
                mCriticalogSharedPreferences.saveData("scan_what", "bag");
                startBarcodeScan();
                break;

        }
    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String scanwhat = mCriticalogSharedPreferences.getData("scan_what");
        if (result != null) {
            try {
                String MobilePattern = "[0-9]{9}";
                bagNumber = result.getContents();

                if (scanwhat.equalsIgnoreCase("bag")) {
                    if (bagNumber.length() == 7) {
                        //  callInscanDocketCheck(docketno);
                        mManualScanLayBag.setVisibility(View.GONE);
                        callBagValidate(bagNumber);
                        Toast.makeText(PickupConfirmActivity.this, "Validate Bag", Toast.LENGTH_SHORT).show();

                    } else {

                        Toasty.warning(PickupConfirmActivity.this, "Scan Valid Bag", Toast.LENGTH_LONG, true).show();
                    }
                } else if (scanwhat.equalsIgnoreCase("box")) {

                    if (bagNumber.length() == 9) {
                        //  callInscanDocketCheck(docketno);
                        mManualScanLayBag.setVisibility(View.GONE);

                        Toast.makeText(PickupConfirmActivity.this, "Validate Box", Toast.LENGTH_SHORT).show();

                    } else {

                        Toasty.warning(PickupConfirmActivity.this, "Scan Valid BOX", Toast.LENGTH_LONG, true).show();
                    }

                } else {


                }

            } catch (Exception e) {
                e.printStackTrace();
                //that means the encoded format not matches
                //in this case you can display whatever data is available on the qrcode

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void showProgress() {

        if (mProgressDialog1 != null) {
            mProgressDialog1.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog1 != null) {
            mProgressDialog1.dismiss();
        }
    }

    @Override
    public void setVendorDataToViews(VendorListResponse vendorListResponse) {

        if (vendorListResponse.getStatus() == 200) {
            datumVendorList = (ArrayList<DatumVendor>) vendorListResponse.getData();
            for (int i = 0; i < datumVendorList.size(); i++) {
                vendorSearchList.add(new SampleSearchModel(datumVendorList.get(i).getVendorName()));
                stringArrayListVendror.add(datumVendorList.get(i).getVendorName());
            }
            showOriginDestination(vendorSearchList, "Vendor");
            Toast.makeText(this, vendorListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, vendorListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setPickupRgsaListToViews(PickupRGSAResponse pickupRGSAResponse) {

        if (pickupRGSAResponse.getStatus() == 200) {
            datumRgsaPickupArrayList = (ArrayList<DatumRgsaPickup>) pickupRGSAResponse.getData();

            if (datumRgsaPickupArrayList.size() != 0) {
                //submit.setVisibility(View.VISIBLE);
               /* mDocketLabelLay.setVisibility(View.VISIBLE);
                mRVPickupListConfirm.setVisibility(View.VISIBLE);*/
                mManualScanLayBag.setVisibility(View.VISIBLE);
                mPickupConfirmAdapter = new PickupConfirmAdapter(PickupConfirmActivity.this, 0, datumRgsaPickupArrayList, this);
                mRVPickupListConfirm.setLayoutManager(new LinearLayoutManager(PickupConfirmActivity.this));
                mRVPickupListConfirm.setAdapter(mPickupConfirmAdapter);
                mPickupConfirmAdapter.notifyDataSetChanged();

                for (int i = 0; i < datumRgsaPickupArrayList.size(); i++) {
                    docketList.add(Integer.parseInt(datumRgsaPickupArrayList.get(i).getXdktno()));
                }
            }
        } else {
            Toast.makeText(PickupConfirmActivity.this, pickupRGSAResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setRGSAPickupConfirmResponse(RGSAPickupConfirmResponse rgsaPickupConfirmResponse) {

        if (rgsaPickupConfirmResponse.getStatus() == 200) {
            Toasty.success(this, rgsaPickupConfirmResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        } else {
            Toasty.warning(this, rgsaPickupConfirmResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void setRgsaBaggingValidateResponse(BagCheckResponse bagCheckResponse) {

        if (bagCheckResponse.getStatus() == 200) {
            mDocketLabelLay.setVisibility(View.VISIBLE);
            mRVPickupListConfirm.setVisibility(View.VISIBLE);
            mManualScanLayBag.setVisibility(View.GONE);
            submit.setVisibility(View.VISIBLE);
             bagNum = bagCheckResponse.getData().get(0).getBagNo();
            bagList.add(Integer.parseInt(bagNum));
            if (dialogManualEntry != null) {
                if (dialogManualEntry.isShowing()) {
                    dialogManualEntry.dismiss();
                }
            }
            Toasty.success(this, bagCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        } else {
            mManualScanLayBag.setVisibility(View.VISIBLE);
            Toasty.warning(this, bagCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.warning(this, R.string.re_try, Toast.LENGTH_LONG, true).show();
    }


    public void callRGSAPickupListAPI() {
        PickupRGSARequest pickupRGSARequest = new PickupRGSARequest();
        pickupRGSARequest.setAction("get_my_pickups");
        pickupRGSARequest.setUserId(userId);
        pickupRGSARequest.setLatitude(String.valueOf(latitude));
        pickupRGSARequest.setLongitude(String.valueOf(longitude));
        mPresenter = new PickupConfirmPresenterImpl(PickupConfirmActivity.this, new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl());
        mPresenter.getRgsaPickupList(pickupRGSARequest);
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(PickupConfirmActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source
                        /*Toast.makeText(PickupConfirmActivity.this, item.getTitle(),
                                Toast.LENGTH_SHORT).show();*/
                        {
                            if (fromWhere.equalsIgnoreCase("vendor")) {
                                mSelectVendor.setText(item.getTitle());

                                mCriticalogSharedPreferences.saveData("vendor_id", datumVendorList.get(stringArrayListVendror.indexOf(item.getTitle())).getVendorId());

                                mCriticalogSharedPreferences.saveData("vendor_name", item.getTitle());

                                if (item.getTitle().equalsIgnoreCase("Other")) {
                                    otherDialog();
                                } else {
                                    callRGSAPickupListAPI();
                                   /* PickupRGSARequest pickupRGSARequest = new PickupRGSARequest();
                                    pickupRGSARequest.setAction("get_my_pickups");
                                    pickupRGSARequest.setUserId(userId);
                                    mPresenter = new PickupConfirmPresenterImpl(PickupConfirmActivity.this, new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl());
                                    mPresenter.getRgsaPickupList(pickupRGSARequest);*/
                                }
                            }

                            dialog.dismiss();
                        }

                    }
                }).show();
    }

    @Override
    public void onclickRulesList(String yesNo, int position) {

        if (yesNo.equalsIgnoreCase("yes")) {
            docketListSelect = docketList;
        } else if (yesNo.equalsIgnoreCase("no")) {

            docketListSelect=new ArrayList<>();
        } else if (yesNo.equalsIgnoreCase("remove")) {
            if (docketListSelect.size() > 0) {
                if(position<docketList.size())
                {
                    if (docketListSelect.contains(docketList.get(position))) {
                        int index = docketListSelect.indexOf(docketList.get(position));
                        docketListSelect.remove(index);
                    }
                }
            }
        } else {
            docketListSelect.add(Integer.parseInt(datumRgsaPickupArrayList.get(position).getXdktno()));
        }
    }

    private boolean isVehicleNumber1(String vehicleNumber) {

       // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{1}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }
    private boolean isVehicleNumber2(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{2}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }
    private boolean isVehicleNumber3(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{3}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }
    private boolean isVehicleNumber4(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING="^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{4}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }

}
