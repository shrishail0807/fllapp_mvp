package com.criticalog.ecritica.MVPCashHandover.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DatumCashList {
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("handed_to")
    @Expose
    private String handedTo;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("docket_json")
    @Expose
    private List<DocketJson> docketJson = null;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getHandedTo() {
        return handedTo;
    }

    public void setHandedTo(String handedTo) {
        this.handedTo = handedTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DocketJson> getDocketJson() {
        return docketJson;
    }

    public void setDocketJson(List<DocketJson> docketJson) {
        this.docketJson = docketJson;
    }

}
