package com.criticalog.ecritica.MVPCashHandover;

import com.criticalog.ecritica.MVPCashHandover.model.AllCashListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPResponse;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitRequest;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.NewCODProcess.NewCodProcessContract;

public interface CashHandoverContract {
    interface presenter {

        void onDestroy();

        void getAllCashList(AllCashListRequest allCashListRequest);

        void getCashListByDate(CashListByDateRequest cashListByDateRequest);

        void getCashierList(CashierListRequest cashierListRequest);

        void getCashierOTP(CashierOTPRequest cashierOTPRequest);

        void cashHandoverSubmit(HandoverSubmitRequest handoverSubmitRequest);


    }

    interface MainView {

        void showProgress();

        void hideProgress();

        void setCashListToViews(AllCashListResponse allCashListResponse);

        void setCashListByDateToViews(CashListByDateResponse cashListByDateToViews);

        void setCashilerListToViews(CashierListResponse cashierListResponse);

        void setCashierOTPDataToViews(CashierOTPResponse cashierOTPResponse);

        void setHandoverSubmitToViews(HandoverSubmitResponse handoverSubmitResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetCashListIntractor {

        interface OnFinishedListener {
            void onFinished(AllCashListResponse allCashListResponse);

            void onFailure(Throwable t);
        }

        void cashListSuccessful(CashHandoverContract.GetCashListIntractor.OnFinishedListener onFinishedListener, AllCashListRequest allCashListRequest);
    }
    interface GetCashListByDateIntractor {

        interface OnFinishedListener {
            void onFinished(CashListByDateResponse cashListByDateResponse);

            void onFailure(Throwable t);
        }

        void cashListByDateSuccessful(CashHandoverContract.GetCashListByDateIntractor.OnFinishedListener onFinishedListener, CashListByDateRequest cashListByDateRequest);
    }
    interface GetCashierListIntractor {

        interface OnFinishedListener {
            void onFinished(CashierListResponse cashierListResponse);

            void onFailure(Throwable t);
        }

        void cashierListSuccessful(CashHandoverContract.GetCashierListIntractor.OnFinishedListener onFinishedListener, CashierListRequest cashierListRequest);
    }

    interface GetCashierOTPIntractor {

        interface OnFinishedListener {
            void onFinished(CashierOTPResponse cashierOTPResponse);

            void onFailure(Throwable t);
        }

        void cashierOTPSuccessful(CashHandoverContract.GetCashierOTPIntractor.OnFinishedListener onFinishedListener, CashierOTPRequest cashierOTPRequest);
    }

    interface GetHandoverSubmitIntractor {

        interface OnFinishedListener {
            void onFinished(HandoverSubmitResponse handoverSubmitResponse);

            void onFailure(Throwable t);
        }

        void handoverSubmitSuccessful(CashHandoverContract.GetHandoverSubmitIntractor.OnFinishedListener onFinishedListener, HandoverSubmitRequest handoverSubmitRequest);
    }
}
