package com.criticalog.ecritica.MVPCashHandover.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.FinishWSCalled;
import com.criticalog.ecritica.Interface.TaskWsCalled;
import com.criticalog.ecritica.MVPCashHandover.model.ConAmountModel;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashListByDate;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.StaticUtils;

import java.util.ArrayList;
import java.util.List;

public class ConnoteAmountAdapter extends RecyclerView.Adapter<ConnoteAmountAdapter.ConnoteAmountAdapterViewHolder>{
    Context context;
    List<DatumCashListByDate> conAmountModelArrayList;
    FinishWSCalled taskWsCalled;
    public ConnoteAmountAdapter(Context contex, List<DatumCashListByDate> conAmountModelArrayList,FinishWSCalled taskWsCalled)
    {
        this.context=contex;
        this.conAmountModelArrayList=conAmountModelArrayList;
        this.taskWsCalled=taskWsCalled;
    }
    @NonNull
    @Override
    public ConnoteAmountAdapter.ConnoteAmountAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.con_amount_item, parent, false);
        return new ConnoteAmountAdapter.ConnoteAmountAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ConnoteAmountAdapter.ConnoteAmountAdapterViewHolder holder, int position) {

        holder.mConnote.setText(conAmountModelArrayList.get(position).getDocketNo());
        holder.mConAmount.setText("₹ "+conAmountModelArrayList.get(position).getPaymentValue());

        holder.mDockekCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.mDockekCheckbox.isChecked())
                {
                    taskWsCalled.finishSuccessFailure("1",conAmountModelArrayList.get(position).getCashId(),conAmountModelArrayList.get(position).getPaymentValue());
                }else {
                    taskWsCalled.finishSuccessFailure("0",conAmountModelArrayList.get(position).getCashId(),conAmountModelArrayList.get(position).getPaymentValue());

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return conAmountModelArrayList.size();
    }
    public class ConnoteAmountAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mConnote,mConAmount;
        private CheckBox mDockekCheckbox;
        public ConnoteAmountAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            mConnote=itemView.findViewById(R.id.mConnote);
            mConAmount=itemView.findViewById(R.id.mConAmount);
            mDockekCheckbox=itemView.findViewById(R.id.mDockekCheckbox);
        }
    }
}
