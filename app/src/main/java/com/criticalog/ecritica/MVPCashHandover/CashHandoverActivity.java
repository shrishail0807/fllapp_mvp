package com.criticalog.ecritica.MVPCashHandover;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Interface.FinishWSCalled;
import com.criticalog.ecritica.Interface.HandOverOnClick;
import com.criticalog.ecritica.MVPCashHandover.Adapters.ConnoteAmountAdapter;
import com.criticalog.ecritica.MVPCashHandover.Adapters.HandedOverDetailsNewAdapter;
import com.criticalog.ecritica.MVPCashHandover.Adapters.HandoverDetailsAdapter;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPResponse;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashList;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashListByDate;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashierList;
import com.criticalog.ecritica.MVPCashHandover.model.DocketJson;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitRequest;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitResponse;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.kal.rackmonthpicker.RackMonthPicker;
import com.kal.rackmonthpicker.listener.DateMonthDialogListener;
import com.kal.rackmonthpicker.listener.OnCancelMonthDialogListener;
import com.leo.simplearcloader.SimpleArcDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OtpTextView;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class CashHandoverActivity extends AppCompatActivity implements HandOverOnClick, CashHandoverContract.MainView, FinishWSCalled {

    private TextView mTopBarText, mEnterOtpText, mCalender;
    RelativeLayout mMainLayout;
    RecyclerView mPayemntDetailRV;
    private HandoverDetailsAdapter handoverDetailsAdapter;
    private ConnoteAmountAdapter connoteAmountAdapter;
    Dialog cashDetailsDialog;
    private EditText mHandOverName;
    private ProgressDialog progressDialog;
    OtpTextView mOtpTextView;
    Button mSubmit;
    ImageView mTvBackbutton;
    private int year, month, day;
    private CashHandoverContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token;
    private SimpleArcDialog mProgressDialog;
    private LinearLayout mHeaderLayout, mLayoutList;
    private Dialog dialogProductDelivered,dialogCameraGallery;
    String statusHandOver = "";
    private int slipFlag = 0;
    ArrayList<String> cashIdsCheckBoxSelected = new ArrayList<>();
    ArrayList<String> cashIds = new ArrayList<>();
    private List<DatumCashList> dataListCash = new ArrayList<>();
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int PERMISSION_REQUEST_CODE_GALLERY = 300;
    File photoFile = null;
    ImageView mDepositSlipImage, mCancelDialogCashHandover;
    private String base64DepositSlip = "";
    TextView mTotalAmountText, mHandedOverText, mMessageTextNoList;
    private String collectType = "cash";
    Button mUpdate, mBtnCashHandOver, mBtnCheckHandover;
    static double collectiveAmount;
    RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_handover);

        mProgressDialog = new SimpleArcDialog(this);
        mProgressDialog.setTitle("Loading......");
        mProgressDialog.setCancelable(false);

        cashDetailsDialog = new Dialog(CashHandoverActivity.this);
        cashDetailsDialog.setContentView(R.layout.handover_details_dialog);
        Window window = cashDetailsDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");


        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("OTP is sent to Registered Mobile Number");
        progressDialog.setCancelable(false);

        mPresenter = new CashHnadoverPresenterImpl(this, new CashHandoverInteractorImpl(), new CashHandoverInteractorImpl(), new CashHandoverInteractorImpl(), new CashHandoverInteractorImpl(), new CashHandoverInteractorImpl());
        mTopBarText = findViewById(R.id.mTopBarText);
        mMainLayout = findViewById(R.id.mMainLayout);
        mPayemntDetailRV = findViewById(R.id.mPayemntDetailRV);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mCalender = findViewById(R.id.mCalenderDate);
        mHeaderLayout = findViewById(R.id.mHeaderLayout);

        mTopBarText.setText("Cash Handover");

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);

        getAllCashList(month + 1, year);

        mCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker("", 1);
            }
        });

        mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        checkGpsEnabledOrNot();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200) {
            if (requestCode == PERMISSION_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64DepositSlip = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mDepositSlipImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                }
            }
        }else {
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();

                try
                {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64DepositSlip = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mDepositSlipImage.setImageURI(selectedImage);
                }
                catch (Exception e)
                {
                    //handle exception
                }

            }
        }
    }

    private void openCameraIntent(int CAMERA_REQUEST, String cameraGallaery) {
        Intent pictureIntent;
        if(cameraGallaery.equalsIgnoreCase("camera"))
        {
             pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
        }else {
             pictureIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        }
       /* Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);*/

       /* Intent pictureIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);*/
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();
    }

    public void getAllCashList(int month, int year) {
        AllCashListRequest allCashListRequest = new AllCashListRequest();
        allCashListRequest.setAction("get_all_cash_list_new");
        allCashListRequest.setMonth(String.valueOf(month));
        allCashListRequest.setUserId(userId);
        allCashListRequest.setYear(String.valueOf(year));
        mPresenter.getAllCashList(allCashListRequest);
    }

    private void datePicker(String reasonCode, int which) {

        new RackMonthPicker(this)
                .setLocale(Locale.ENGLISH)
                .setPositiveButton(new DateMonthDialogListener() {
                    @Override
                    public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                        mCalender.setText(monthLabel);

                        getAllCashList(month, year);

                    }
                })
                .setNegativeButton(new OnCancelMonthDialogListener() {
                    @Override
                    public void onCancel(AlertDialog dialog) {

                    }
                }).show();
    }

    public void dialogHandoverDetails(List<DatumCashListByDate> datumCashListByDates) {
        RecyclerView mConlistPayStatsRV;

        mUpdate = cashDetailsDialog.findViewById(R.id.mUpdate);
        mEnterOtpText = cashDetailsDialog.findViewById(R.id.mEnterOtpText);
        mHandOverName = cashDetailsDialog.findViewById(R.id.mHandOverName);
        mOtpTextView = cashDetailsDialog.findViewById(R.id.mOtpTextView);
        mHandedOverText = cashDetailsDialog.findViewById(R.id.mHandedOverText);
        radioGroup = cashDetailsDialog.findViewById(R.id.radioGroup);
        mDepositSlipImage = cashDetailsDialog.findViewById(R.id.mDepositSlipImage);
        mTotalAmountText = cashDetailsDialog.findViewById(R.id.mTotalAmountText);
        mConlistPayStatsRV = cashDetailsDialog.findViewById(R.id.mConlistPayStatsRV);
        mSubmit = cashDetailsDialog.findViewById(R.id.mSubmit);
        mLayoutList = cashDetailsDialog.findViewById(R.id.mLayoutList);
        mMessageTextNoList = cashDetailsDialog.findViewById(R.id.mMessageTextNoList);
        mBtnCashHandOver = cashDetailsDialog.findViewById(R.id.mBtnCashHandOver);
        mBtnCheckHandover = cashDetailsDialog.findViewById(R.id.mBtnCheckHandover);
        mCancelDialogCashHandover = cashDetailsDialog.findViewById(R.id.mCancelDialogCashHandover);
        cashDetailsDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        cashDetailsDialog.setCanceledOnTouchOutside(false);
        cashDetailsDialog.setCancelable(false);
        mDepositSlipImage.setImageBitmap(null);
        mDepositSlipImage.setImageResource(R.drawable.outline_photo_camera_24);
        mDepositSlipImage.setVisibility(View.GONE);

        //((RadioButton) radioGroup.getChildAt(1)).setText("Cheque Handover to \n Hub Manager");
        ((RadioButton) radioGroup.getChildAt(0)).setText("Cash Handover to \n Hub Manager");

        mCancelDialogCashHandover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtnCheckHandover.setBackgroundResource(R.drawable.square_red_back);
                mBtnCheckHandover.setTextColor(Color.BLUE);
                mBtnCashHandOver.setBackgroundResource(R.drawable.square_green_back);
                mBtnCashHandOver.setTextColor(Color.WHITE);
                cashDetailsDialog.dismiss();
                //   finish();
            }
        });

        if (datumCashListByDates.size() == 0) {

            radioGroup.setVisibility(View.GONE);
            mTotalAmountText.setVisibility(View.GONE);
            mConlistPayStatsRV.setVisibility(View.GONE);
            mHandedOverText.setVisibility(View.GONE);
            mHandOverName.setVisibility(View.GONE);
            mOtpTextView.setVisibility(View.GONE);
            mLayoutList.setVisibility(View.GONE);
            mSubmit.setVisibility(View.GONE);
            mMessageTextNoList.setVisibility(View.VISIBLE);

            mHandOverName.setText("");
            mHandOverName.setHint("Select Name");
            mOtpTextView.setOTP("");

        } else {
            cashIds.clear();
            radioGroup.setVisibility(View.VISIBLE);
            mTotalAmountText.setVisibility(View.VISIBLE);
            mConlistPayStatsRV.setVisibility(View.VISIBLE);
            mHandedOverText.setVisibility(View.VISIBLE);
            mHandOverName.setVisibility(View.VISIBLE);
            mOtpTextView.setVisibility(View.VISIBLE);
            mLayoutList.setVisibility(View.VISIBLE);
            mMessageTextNoList.setVisibility(View.GONE);
            mSubmit.setVisibility(View.VISIBLE);
            mHandOverName.setText("");
            mHandOverName.setHint("Select Name");
            mOtpTextView.setOTP("");
            for (DatumCashListByDate datumCashListByDate : datumCashListByDates) {
                if (datumCashListByDate.getAction().equalsIgnoreCase("2")) {
                    cashIds.add(datumCashListByDate.getCashId());
                }
            }
            collectiveAmount = Double.parseDouble(datumCashListByDates.get(0).getTotal_value());
            mTotalAmountText.setText("Total Amount: " + "₹ " + datumCashListByDates.get(0).getTotal_value());
            connoteAmountAdapter = new ConnoteAmountAdapter(this, datumCashListByDates, this);
            mConlistPayStatsRV.setLayoutManager(new LinearLayoutManager(this));
            mConlistPayStatsRV.setAdapter(connoteAmountAdapter);
        }
        mHandOverName.setFocusable(false);
        mHandOverName.setClickable(true);

        mBtnCheckHandover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collectType = "cheque";

                ((RadioButton) radioGroup.getChildAt(0)).setText("Cheque Handover to \n Hub Manager");
                mDepositSlipImage.setVisibility(View.GONE);
                mBtnCheckHandover.setBackgroundResource(R.drawable.square_green_back);
                mBtnCashHandOver.setBackgroundResource(R.drawable.square_red_back);
                mBtnCheckHandover.setTextColor(Color.WHITE);
                mBtnCashHandOver.setTextColor(Color.BLUE);

                CashListByDateRequest cashListByDateRequest = new CashListByDateRequest();
                cashListByDateRequest.setAction("get_cash_list_by_date_new");
                cashListByDateRequest.setUserId(userId);
                cashListByDateRequest.setDate(mCriticalogSharedPreferences.getData("date_cash"));
                cashListByDateRequest.setStatus("pending");
                cashListByDateRequest.setCollect_type("cheque");
                mPresenter.getCashListByDate(cashListByDateRequest);
            }
        });

        mBtnCashHandOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collectType = "cash";
                ((RadioButton) radioGroup.getChildAt(0)).setText("Cash Handover to \n Hub Manager");
                mDepositSlipImage.setVisibility(View.GONE);
                mBtnCheckHandover.setBackgroundResource(R.drawable.square_red_back);
                mBtnCheckHandover.setTextColor(Color.BLUE);
                mBtnCashHandOver.setBackgroundResource(R.drawable.square_green_back);
                mBtnCashHandOver.setTextColor(Color.WHITE);

                CashListByDateRequest cashListByDateRequest = new CashListByDateRequest();
                cashListByDateRequest.setAction("get_cash_list_by_date_new");
                cashListByDateRequest.setUserId(userId);
                cashListByDateRequest.setDate(mCriticalogSharedPreferences.getData("date_cash"));
                cashListByDateRequest.setStatus("pending");
                cashListByDateRequest.setCollect_type("cash");
                mPresenter.getCashListByDate(cashListByDateRequest);

            }
        });
        mDepositSlipImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCameraGallery();
              /*  openCameraIntent(PERMISSION_REQUEST_CODE, "camera");*/
            }
        });
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String OTP = mOtpTextView.getOTP().toString();
                if (OTP.equalsIgnoreCase("")) {
                    Toasty.error(CashHandoverActivity.this, "Enter OTP", Toast.LENGTH_SHORT, true).show();
                } else if (OTP.length() != 4) {
                    Toasty.error(CashHandoverActivity.this, "Enter Valid OTP", Toast.LENGTH_SHORT, true).show();
                } else {
                    if (slipFlag == 1) {
                        if (cashIds.size() == 0) {
                            Toast.makeText(CashHandoverActivity.this, "Select Connote", Toast.LENGTH_SHORT).show();
                        } else if (base64DepositSlip.equalsIgnoreCase("")) {
                            Toast.makeText(CashHandoverActivity.this, "Capture Slip Image", Toast.LENGTH_SHORT).show();
                        } else {
                            HandoverSubmitRequest handoverSubmitRequest = new HandoverSubmitRequest();
                            handoverSubmitRequest.setAction("handover_submit_new");
                            handoverSubmitRequest.setUserId(userId);
                            handoverSubmitRequest.setCashierId(mCriticalogSharedPreferences.getData("userid_cashier"));
                            handoverSubmitRequest.setMobileNo(mCriticalogSharedPreferences.getData("mobile_cahier"));
                            handoverSubmitRequest.setOtp(OTP);
                            // handoverSubmitRequest.setTotalAmount(mCriticalogSharedPreferences.getData("total_amount"));
                            handoverSubmitRequest.setCashIds(cashIds);
                            handoverSubmitRequest.setCashier_name(mCriticalogSharedPreferences.getData("name_cashier"));
                            handoverSubmitRequest.setImage_slip(base64DepositSlip);
                            handoverSubmitRequest.setHandover_type(collectType);
                            handoverSubmitRequest.setWith_slip("yes");
                            mPresenter.cashHandoverSubmit(handoverSubmitRequest);
                        }
                    } else {
                        if (cashIds.size() == 0) {
                            Toast.makeText(CashHandoverActivity.this, "Select Connote", Toast.LENGTH_SHORT).show();
                        } else {
                            HandoverSubmitRequest handoverSubmitRequest = new HandoverSubmitRequest();
                            handoverSubmitRequest.setAction("handover_submit_new");
                            handoverSubmitRequest.setUserId(userId);
                            handoverSubmitRequest.setCashierId(mCriticalogSharedPreferences.getData("userid_cashier"));
                            handoverSubmitRequest.setMobileNo(mCriticalogSharedPreferences.getData("mobile_cahier"));
                            handoverSubmitRequest.setOtp(OTP);
                            //  handoverSubmitRequest.setTotalAmount(mCriticalogSharedPreferences.getData("total_amount"));
                            handoverSubmitRequest.setCashIds(cashIds);
                            handoverSubmitRequest.setHandover_type(collectType);
                            handoverSubmitRequest.setWith_slip("no");
                            handoverSubmitRequest.setCashier_name(mCriticalogSharedPreferences.getData("name_cashier"));
                            mPresenter.cashHandoverSubmit(handoverSubmitRequest);
                        }
                    }
                }
            }
        });

        mHandOverName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CashierListRequest cashierListRequest = new CashierListRequest();
                cashierListRequest.setAction("get_cashier_list");
                cashierListRequest.setUserId(userId);
                mPresenter.getCashierList(cashierListRequest);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            int id;

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radioButtonCashHandover:
                        slipFlag = 0;
                        mDepositSlipImage.setVisibility(View.GONE);
                        break;

                    case R.id.radioButtonDepositSlip:
                        slipFlag = 1;
                        mDepositSlipImage.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });


        if (cashDetailsDialog.isShowing()) {
            //  mTotalAmountText.setText("Total Amount: " + "₹ " + datumCashListByDates.get(0).getTotal_value());
            connoteAmountAdapter = new ConnoteAmountAdapter(this, datumCashListByDates, this);
            mConlistPayStatsRV.setLayoutManager(new LinearLayoutManager(this));
            mConlistPayStatsRV.setAdapter(connoteAmountAdapter);
        } else {
            cashDetailsDialog.show();
        }
        /* cashDetailsDialog.show();*/
    }


    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere, List<DatumCashierList> cashierListList) {


        new SimpleSearchDialogCompat(CashHandoverActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {

                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source
                        mHandOverName.setText(item.getTitle());

                        cashierListList.get(position).getUserId();
                        cashierListList.get(position).getMobile_no();
                        mCriticalogSharedPreferences.saveData("userid_cashier", cashierListList.get(position).getUserId());
                        mCriticalogSharedPreferences.saveData("mobile_cahier", cashierListList.get(position).getMobile_no());
                        mCriticalogSharedPreferences.saveData("name_cashier", item.getTitle());
                        // progressDialog.show();

                        productDeliveredDialog("Generate OTP to Handover Cash");

                        dialog.dismiss();
                    }
                }).show();

    }
    public void dialogCameraGallery() {
        EditText mEnterReason;
        TextView mCamera,mGallery;
        //Dialog Manual Entry
        dialogCameraGallery= new Dialog(CashHandoverActivity.this);
        dialogCameraGallery.setContentView(R.layout.dialog_camera_gallery);
        dialogCameraGallery.setCanceledOnTouchOutside(false);
        dialogCameraGallery.setCancelable(false);

        mCamera = dialogCameraGallery.findViewById(R.id.mCamera);
        mGallery = dialogCameraGallery.findViewById(R.id.mGallery);

        Window window = dialogCameraGallery.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        dialogCameraGallery.show();


        mCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraIntent(PERMISSION_REQUEST_CODE, "camera");
                dialogCameraGallery.dismiss();
            }
        });
        mGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraIntent(PERMISSION_REQUEST_CODE_GALLERY, "gallery");
                dialogCameraGallery.dismiss();
            }
        });

    }
    public void showHandedOverDetailsDialog(List<DocketJson> datumCashListByDates) {

        //Dialog Manual Entry
        TextView mDialogText, mOkDialog, mDate;
        RecyclerView mRvHandedOver;
        dialogProductDelivered = new Dialog(CashHandoverActivity.this);
        dialogProductDelivered.setContentView(R.layout.show_details_handed_over);
        mOkDialog = dialogProductDelivered.findViewById(R.id.mOk);
        mDate = dialogProductDelivered.findViewById(R.id.mDate);
        mDate.setText("Date: " + mCriticalogSharedPreferences.getData("date_cash"));

        mDialogText = dialogProductDelivered.findViewById(R.id.mDialogText);
        mRvHandedOver = dialogProductDelivered.findViewById(R.id.mRvHandedOverDetails);

        Window window = dialogProductDelivered.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialogProductDelivered.show();

        HandedOverDetailsNewAdapter handedOverDetailsNewAdapter = new HandedOverDetailsNewAdapter(this, datumCashListByDates);
        mRvHandedOver.setLayoutManager(new LinearLayoutManager(this));
        mRvHandedOver.setAdapter(handedOverDetailsNewAdapter);

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProductDelivered.dismiss();

            }
        });
    }


    public void productDeliveredDialog(String message) {
        //Dialog Manual Entry
        TextView mDialogText, mOkDialog;
        dialogProductDelivered = new Dialog(CashHandoverActivity.this);
        dialogProductDelivered.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = dialogProductDelivered.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered.findViewById(R.id.mDialogText);
        mDialogText.setText(message);
        dialogProductDelivered.show();
        mOkDialog.setText("Generate OTP");

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProductDelivered.dismiss();
                CashierOTPRequest cashierOTPRequest = new CashierOTPRequest();
                cashierOTPRequest.setAction("handover_otp");
                cashierOTPRequest.setUserId(userId);
                cashierOTPRequest.setCashierId(mCriticalogSharedPreferences.getData("userid_cashier"));
                cashierOTPRequest.setMobileNo(mCriticalogSharedPreferences.getData("mobile_cahier"));
                cashierOTPRequest.setCashierName(mCriticalogSharedPreferences.getData("name_cashier"));
                cashierOTPRequest.setFor_type("ch");

                mPresenter.getCashierOTP(cashierOTPRequest);
            }
        });
    }

    @Override
    public void showProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void setCashListToViews(AllCashListResponse allCashListResponse) {

        if (allCashListResponse.getStatus() == 200) {
            dataListCash = allCashListResponse.getData();
            mHeaderLayout.setVisibility(View.VISIBLE);
            mPayemntDetailRV.setVisibility(View.VISIBLE);
            handoverDetailsAdapter = new HandoverDetailsAdapter(CashHandoverActivity.this, this, allCashListResponse.getData());
            mPayemntDetailRV.setLayoutManager(new LinearLayoutManager(this));
            mPayemntDetailRV.setAdapter(handoverDetailsAdapter);
        } else {
            mHeaderLayout.setVisibility(View.GONE);
            mPayemntDetailRV.setVisibility(View.GONE);
            Toast.makeText(this, allCashListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setCashListByDateToViews(CashListByDateResponse cashListByDateResponse) {


        if (cashListByDateResponse.getStatus() == 200) {
            dialogHandoverDetails(cashListByDateResponse.getData());
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
            if (collectType.equalsIgnoreCase("cheque")) {
                ((RadioButton) radioGroup.getChildAt(0)).setText("Cheque Handover to \n Hub Manager");
            }
        } else {
            List<DatumCashListByDate> datumCashListByDates = new ArrayList<>();
            dialogHandoverDetails(datumCashListByDates);
        }
    }

    @Override
    public void setCashilerListToViews(CashierListResponse cashierListResponse) {
        ArrayList<SampleSearchModel> sampleSearchModelArrayList = new ArrayList<>();
        if (cashierListResponse.getStatus() == 200) {
            for (DatumCashierList datumCashierList : cashierListResponse.getData()) {

                SampleSearchModel sampleSearchModel = new SampleSearchModel(datumCashierList.getUsername());

                sampleSearchModelArrayList.add(sampleSearchModel);
            }

            showOriginDestination(sampleSearchModelArrayList, "", cashierListResponse.getData());
        }
    }

    @Override
    public void setCashierOTPDataToViews(CashierOTPResponse cashierOTPResponse) {
        if (cashierOTPResponse.getStatus() == 200) {
            Toast.makeText(this, cashierOTPResponse.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, cashierOTPResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setHandoverSubmitToViews(HandoverSubmitResponse handoverSubmitResponse) {

        if (handoverSubmitResponse.getStatus() == 200) {
            Toasty.success(this, handoverSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();

            if (cashDetailsDialog != null) {
                mBtnCheckHandover.setBackgroundResource(R.drawable.square_red_back);
                mBtnCheckHandover.setTextColor(Color.BLUE);
                mBtnCashHandOver.setBackgroundResource(R.drawable.square_green_back);
                mBtnCashHandOver.setTextColor(Color.WHITE);
                cashDetailsDialog.dismiss();
            }
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            getAllCashList(month + 1, year);
            //  startActivity(new Intent(this, Cas.class));
        } else {
            Toasty.error(this, handoverSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }

    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    @Override
    public void onClick(List<DocketJson> docketJsons, String totalAmount, int position, String status) {
        cashIds.clear();
        if (!status.equalsIgnoreCase("pending")) {
            if (docketJsons.isEmpty()) {
                Toast.makeText(this, "No Details Available!!", Toast.LENGTH_SHORT).show();
            } else {
                showHandedOverDetailsDialog(docketJsons);
            }

        } else {
            statusHandOver = "pending";
            //showHandedOverDetailsDialog("");
            mCriticalogSharedPreferences.saveData("total_amount", totalAmount);


            CashListByDateRequest cashListByDateRequest = new CashListByDateRequest();
            cashListByDateRequest.setAction("get_cash_list_by_date_new");
            cashListByDateRequest.setUserId(userId);
            cashListByDateRequest.setDate(mCriticalogSharedPreferences.getData("date_cash"));
            cashListByDateRequest.setStatus("pending");
            cashListByDateRequest.setCollect_type("cash");
            mPresenter.getCashListByDate(cashListByDateRequest);
        }
    }


    @Override
    public void finishSuccessFailure(String result, String connote, String amount) {

        if (result.equalsIgnoreCase("1")) {
            if (!cashIds.contains(connote)) {
                cashIds.add(connote);
                //Toast.makeText(this, "Added: " + String.valueOf(cashIds.size()), Toast.LENGTH_SHORT).show();

                collectiveAmount = collectiveAmount + Double.valueOf(amount);
                mTotalAmountText.setText("Total Amount: " + "₹ " + String.valueOf(collectiveAmount));
            }
        } else {
            if (cashIds.size() > 0) {

                int indexOfId = cashIds.indexOf(connote);

                cashIds.remove(indexOfId);

                // Toast.makeText(this, "Removed: " + String.valueOf(cashIds.size()), Toast.LENGTH_SHORT).show();
                collectiveAmount = collectiveAmount - Double.valueOf(amount);
                mTotalAmountText.setText("Total Amount: " + "₹ " + String.valueOf(collectiveAmount));
            }
        }
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }
}
