package com.criticalog.ecritica.MVPCashHandover.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocketJson {
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("payment_value")
    @Expose
    private String paymentValue;

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getPaymentValue() {
        return paymentValue;
    }

    public void setPaymentValue(String paymentValue) {
        this.paymentValue = paymentValue;
    }

}
