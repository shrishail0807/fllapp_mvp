package com.criticalog.ecritica.MVPCashHandover;

import com.criticalog.ecritica.MVPCashHandover.model.AllCashListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPResponse;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitRequest;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CashHandoverInteractorImpl implements CashHandoverContract.GetCashListIntractor,CashHandoverContract.GetCashListByDateIntractor,
CashHandoverContract.GetCashierListIntractor,CashHandoverContract.GetCashierOTPIntractor,
CashHandoverContract.GetHandoverSubmitIntractor{
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void cashListSuccessful(CashHandoverContract.GetCashListIntractor.OnFinishedListener onFinishedListener, AllCashListRequest allCashListRequest) {
        Call<AllCashListResponse> allCashListResponseCall=services.getAllCashList(allCashListRequest);
        allCashListResponseCall.enqueue(new Callback<AllCashListResponse>() {
            @Override
            public void onResponse(Call<AllCashListResponse> call, Response<AllCashListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<AllCashListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void cashListByDateSuccessful(CashHandoverContract.GetCashListByDateIntractor.OnFinishedListener onFinishedListener, CashListByDateRequest cashListByDateRequest) {

        Call<CashListByDateResponse> cashListByDateResponseCall=services.getAllCashListByDate(cashListByDateRequest);
        cashListByDateResponseCall.enqueue(new Callback<CashListByDateResponse>() {
            @Override
            public void onResponse(Call<CashListByDateResponse> call, Response<CashListByDateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CashListByDateResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void cashierListSuccessful(CashHandoverContract.GetCashierListIntractor.OnFinishedListener onFinishedListener, CashierListRequest cashierListRequest) {
        Call<CashierListResponse> cashierListResponseCall=services.getCashierList(cashierListRequest);
        cashierListResponseCall.enqueue(new Callback<CashierListResponse>() {
            @Override
            public void onResponse(Call<CashierListResponse> call, Response<CashierListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CashierListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void cashierOTPSuccessful(CashHandoverContract.GetCashierOTPIntractor.OnFinishedListener onFinishedListener, CashierOTPRequest cashierOTPRequest) {
        Call<CashierOTPResponse> cashierOTPResponseCall=services.getOTPForCashier(cashierOTPRequest);
        cashierOTPResponseCall.enqueue(new Callback<CashierOTPResponse>() {
            @Override
            public void onResponse(Call<CashierOTPResponse> call, Response<CashierOTPResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CashierOTPResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void handoverSubmitSuccessful(CashHandoverContract.GetHandoverSubmitIntractor.OnFinishedListener onFinishedListener, HandoverSubmitRequest handoverSubmitRequest) {

        Call<HandoverSubmitResponse> handoverSubmitResponseCall=services.cashHandoverSubmit(handoverSubmitRequest);

        handoverSubmitResponseCall.enqueue(new Callback<HandoverSubmitResponse>() {
            @Override
            public void onResponse(Call<HandoverSubmitResponse> call, Response<HandoverSubmitResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<HandoverSubmitResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }
}
