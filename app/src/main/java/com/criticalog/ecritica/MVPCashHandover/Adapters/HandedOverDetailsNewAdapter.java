package com.criticalog.ecritica.MVPCashHandover.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashList;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashListByDate;
import com.criticalog.ecritica.MVPCashHandover.model.DocketJson;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;

import java.util.List;

public class HandedOverDetailsNewAdapter extends RecyclerView.Adapter<HandedOverDetailsNewAdapter.HandoverDetailsAdapterNewViewHolder> {
    private RulesClicked rulesClicked;
    List<DocketJson> conAmountModelArrayList;
    private Context mContext;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;

    public HandedOverDetailsNewAdapter(Context mContext, List<DocketJson> conAmountModelArrayList) {
        this.rulesClicked = rulesClicked;
        this.conAmountModelArrayList = conAmountModelArrayList;
        this.mContext = mContext;
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(mContext);
    }

    @NonNull
    @Override
    public HandoverDetailsAdapterNewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_details_item_new, parent, false);
        return new HandedOverDetailsNewAdapter.HandoverDetailsAdapterNewViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HandedOverDetailsNewAdapter.HandoverDetailsAdapterNewViewHolder holder, int position) {

        holder.mConnote.setText(String.valueOf(position + 1) + ") " + conAmountModelArrayList.get(position).getDocketNo());
        holder.mTotalAmount.setText("₹ " + conAmountModelArrayList.get(position).getPaymentValue());

    }

    @Override
    public int getItemCount() {
        return conAmountModelArrayList.size();

    }

    public class HandoverDetailsAdapterNewViewHolder extends RecyclerView.ViewHolder {
        TextView mConnote, mTotalAmount;
        LinearLayout mLayoutList;

        public HandoverDetailsAdapterNewViewHolder(@NonNull View itemView) {
            super(itemView);
            mConnote = itemView.findViewById(R.id.mConnote);
            mTotalAmount = itemView.findViewById(R.id.mTotalAmount);
            mLayoutList = itemView.findViewById(R.id.mLayoutList);
        }
    }
}
