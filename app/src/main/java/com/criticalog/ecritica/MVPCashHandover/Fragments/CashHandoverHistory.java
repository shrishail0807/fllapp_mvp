package com.criticalog.ecritica.MVPCashHandover.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.criticalog.ecritica.R;

public class CashHandoverHistory extends Fragment {
    TextView mScreenName;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        View view = inflater.inflate(R.layout.fragment_cash_handover, null);

     /*   mScreenName=view.findViewById(R.id.mScreenName);

        mScreenName.setText("History");*/


        return view;
    }
}
