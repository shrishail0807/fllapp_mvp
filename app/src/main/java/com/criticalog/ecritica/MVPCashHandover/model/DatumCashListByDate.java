package com.criticalog.ecritica.MVPCashHandover.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumCashListByDate {
    @SerializedName("cash_id")
    @Expose
    private String cashId;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("payment_value")
    @Expose
    private String paymentValue;
    @SerializedName("payment_rec_type_date")
    @Expose
    private String paymentRecTypeDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("action")
    @Expose
    private String action;

    public String getTotal_value() {
        return total_value;
    }

    public void setTotal_value(String total_value) {
        this.total_value = total_value;
    }

    @SerializedName("total_value")
    @Expose
    private String total_value;


    public String getCashId() {
        return cashId;
    }

    public void setCashId(String cashId) {
        this.cashId = cashId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getPaymentValue() {
        return paymentValue;
    }

    public void setPaymentValue(String paymentValue) {
        this.paymentValue = paymentValue;
    }

    public String getPaymentRecTypeDate() {
        return paymentRecTypeDate;
    }

    public void setPaymentRecTypeDate(String paymentRecTypeDate) {
        this.paymentRecTypeDate = paymentRecTypeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
