package com.criticalog.ecritica.MVPCashHandover.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPCashHandover.Adapters.HandoverDetailsAdapter;
import com.criticalog.ecritica.R;

public class CashHandoverDetailsFragment extends Fragment {
    RelativeLayout mMainLayout;
    RecyclerView mPayemntDetailRV;
    private TextView mTopBarText;
    private HandoverDetailsAdapter handoverDetailsAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        View view = inflater.inflate(R.layout.fragment_cash_handover, null);
        mMainLayout=view.findViewById(R.id.mMainLayout);
        mPayemntDetailRV=view.findViewById(R.id.mPayemntDetailRV);
        addEditView();
        return view;
    }

    private void addEditView() {
        // TODO Auto-generated method stub
        LinearLayout li=new LinearLayout(getContext());
        EditText et=new EditText(getContext());
        Button b=new Button(getContext());

       /* handoverDetailsAdapter=new HandoverDetailsAdapter();
        mPayemntDetailRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPayemntDetailRV.setAdapter(handoverDetailsAdapter);*/

        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                int pos=(Integer) v.getTag();
                mMainLayout.removeViewAt(pos);

            }
        });

        b.setTag((mMainLayout.getChildCount()+1));
    }
}
