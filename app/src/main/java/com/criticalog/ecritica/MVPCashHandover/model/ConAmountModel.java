package com.criticalog.ecritica.MVPCashHandover.model;

public class ConAmountModel {
    private String con;
    private String amount;

    public ConAmountModel(String con, String amount) {
        this.con = con;
        this.amount = amount;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
