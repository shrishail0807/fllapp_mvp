package com.criticalog.ecritica.MVPCashHandover.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.HandOverOnClick;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPCashHandover.model.DatumCashList;
import com.criticalog.ecritica.MVPDRS.Adapter.DRSTaskAdapter;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;

import java.util.List;

public class HandoverDetailsAdapter extends RecyclerView.Adapter<HandoverDetailsAdapter.HandoverDetailsAdapterViewHolder> {
    private List<DatumCashList> datumCashListList;
    private Context mContext;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private HandOverOnClick handOverOnClick;
    public HandoverDetailsAdapter(Context mContext,HandOverOnClick handOverOnClick,List<DatumCashList> datumCashListList) {
        this.handOverOnClick = handOverOnClick;
        this.datumCashListList=datumCashListList;
        this.mContext=mContext;
        mCriticalogSharedPreferences=CriticalogSharedPreferences.getInstance(mContext);
    }

    @NonNull
    @Override
    public HandoverDetailsAdapter.HandoverDetailsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_details_item, parent, false);
        return new HandoverDetailsAdapter.HandoverDetailsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HandoverDetailsAdapter.HandoverDetailsAdapterViewHolder holder, int position) {
       /* if (position == 0) {
            holder.mDate.setText("25");
            holder.mHandover.setText("Handover Pending");
            holder.mTotalAmount.setText("Rs.8000");
        } else if (position == 1) {
            holder.mDate.setText("26");
            holder.mHandover.setText("Ravi(11223) \n (27/9/2022)");
        } else {
            holder.mDate.setText("27");
            holder.mHandover.setText("Handover Pending");
        }*/

        holder.mDate.setText(datumCashListList.get(position).getDate());
        if(datumCashListList.get(position).getStatus().equalsIgnoreCase("Handed Over"))
        {
            holder.mHandover.setText(datumCashListList.get(position).getHandedTo());
            holder.mHandover.setPaintFlags(holder.mHandover.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

         //   holder.mHandover.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else {
            holder.mHandover.setTextColor(Color.WHITE);
            holder.mHandover.setText(datumCashListList.get(position).getStatus());
            holder.mHandover.setBackgroundResource(R.drawable.rounded_bachground_red);
           // holder.mHandover.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.baseline_play_circle_filled_24, 0);
        }

        holder.mTotalAmount.setText("₹ "+datumCashListList.get(position).getTotal());
        holder.mLayoutList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("date_cash",datumCashListList.get(position).getDate());
                if(!datumCashListList.get(position).getStatus().equalsIgnoreCase("Handed Over"))
                {
                    handOverOnClick.onClick(datumCashListList.get(position).getDocketJson(),datumCashListList.get(position).getTotal(), position,datumCashListList.get(position).getStatus());
                }else {
                    handOverOnClick.onClick(datumCashListList.get(position).getDocketJson(),datumCashListList.get(position).getTotal(), position,datumCashListList.get(position).getStatus());
                  //  Toast.makeText(mContext, "Already Cash Handed Over", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return datumCashListList.size();

    }

    public class HandoverDetailsAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView mHandover, mDate, mTotalAmount;
        LinearLayout mLayoutList;

        public HandoverDetailsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            mHandover = itemView.findViewById(R.id.mHandover);
            mDate = itemView.findViewById(R.id.mDate);
            mTotalAmount = itemView.findViewById(R.id.mTotalAmount);
            mLayoutList = itemView.findViewById(R.id.mLayoutList);
        }
    }
}
