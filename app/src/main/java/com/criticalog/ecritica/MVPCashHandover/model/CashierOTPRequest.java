package com.criticalog.ecritica.MVPCashHandover.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashierOTPRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("cashier_id")
    @Expose
    private String cashierId;
    @SerializedName("cashier_name")
    @Expose
    private String cashierName;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;

    @SerializedName("for_type")
    @Expose
    private String for_type;

    public String getFor_type() {
        return for_type;
    }

    public void setFor_type(String for_type) {
        this.for_type = for_type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
