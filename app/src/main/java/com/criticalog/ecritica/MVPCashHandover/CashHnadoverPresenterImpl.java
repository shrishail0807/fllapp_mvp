package com.criticalog.ecritica.MVPCashHandover;

import com.criticalog.ecritica.MVPCashHandover.model.AllCashListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPResponse;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitRequest;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitResponse;

public class CashHnadoverPresenterImpl implements CashHandoverContract.presenter, CashHandoverContract.GetCashListIntractor.OnFinishedListener, CashHandoverContract.GetCashListByDateIntractor.OnFinishedListener, CashHandoverContract.GetCashierListIntractor.OnFinishedListener, CashHandoverContract.GetCashierOTPIntractor.OnFinishedListener, CashHandoverContract.GetHandoverSubmitIntractor.OnFinishedListener {
    private CashHandoverContract.MainView mainView;
    private CashHandoverContract.GetCashListIntractor getCashListIntractor;
    private CashHandoverContract.GetCashListByDateIntractor getCashListByDateIntractor;
    private CashHandoverContract.GetCashierListIntractor getCashierListIntractor;
    private CashHandoverContract.GetCashierOTPIntractor getCashierOTPIntractor;
    private CashHandoverContract.GetHandoverSubmitIntractor getHandoverSubmitIntractor;

    public CashHnadoverPresenterImpl(CashHandoverContract.MainView mainView, CashHandoverContract.GetCashListIntractor getCashListIntractor,
                                     CashHandoverContract.GetCashListByDateIntractor getCashListByDateIntractor,
                                     CashHandoverContract.GetCashierListIntractor getCashierListIntractor,
                                     CashHandoverContract.GetCashierOTPIntractor getCashierOTPIntractor,
                                     CashHandoverContract.GetHandoverSubmitIntractor getHandoverSubmitIntractor) {
        this.mainView = mainView;
        this.getCashListIntractor = getCashListIntractor;
        this.getCashListByDateIntractor=getCashListByDateIntractor;
        this.getCashierListIntractor=getCashierListIntractor;
        this.getCashierOTPIntractor=getCashierOTPIntractor;
        this.getHandoverSubmitIntractor=getHandoverSubmitIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void getAllCashList(AllCashListRequest allCashListRequest) {

        if (mainView != null) {
            mainView.showProgress();

        }
        getCashListIntractor.cashListSuccessful(this, allCashListRequest);
    }

    @Override
    public void getCashListByDate(CashListByDateRequest cashListByDateRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getCashListByDateIntractor.cashListByDateSuccessful(this,cashListByDateRequest);
    }

    @Override
    public void getCashierList(CashierListRequest cashierListRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getCashierListIntractor.cashierListSuccessful(this,cashierListRequest);
    }

    @Override
    public void getCashierOTP(CashierOTPRequest cashierOTPRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getCashierOTPIntractor.cashierOTPSuccessful(this,cashierOTPRequest);
    }

    @Override
    public void cashHandoverSubmit(HandoverSubmitRequest handoverSubmitRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getHandoverSubmitIntractor.handoverSubmitSuccessful(this,handoverSubmitRequest);
    }

    @Override
    public void onFinished(AllCashListResponse allCashListResponse) {
        if (mainView != null) {
            mainView.setCashListToViews(allCashListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CashListByDateResponse cashListByDateResponse) {
        if(mainView!=null)
        {
            mainView.setCashListByDateToViews(cashListByDateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CashierListResponse cashierListResponse) {
        if(mainView!=null)
        {
            mainView.setCashilerListToViews(cashierListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CashierOTPResponse cashierOTPResponse) {
        if(mainView!=null)
        {
            mainView.setCashierOTPDataToViews(cashierOTPResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(HandoverSubmitResponse handoverSubmitResponse) {
        if(mainView!=null)
        {
            mainView.setHandoverSubmitToViews(handoverSubmitResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
