package com.criticalog.ecritica.MVPCashHandover.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
public class HandoverSubmitRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("cashier_id")
    @Expose
    private String cashierId;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("cash_ids")
    @Expose
    private List<String> cashIds = null;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;

    @SerializedName("cashier_name")
    @Expose
    private String cashier_name;

    @SerializedName("handover_type")
    @Expose
    private String handover_type;

    @SerializedName("with_slip")
    @Expose
    private String with_slip;

    @SerializedName("image_1")
    @Expose
    private String image_slip;

    public String getWith_slip() {
        return with_slip;
    }

    public void setWith_slip(String with_slip) {
        this.with_slip = with_slip;
    }

    public String getHandover_type() {
        return handover_type;
    }

    public void setHandover_type(String handover_type) {
        this.handover_type = handover_type;
    }

    public String getImage_slip() {
        return image_slip;
    }

    public void setImage_slip(String image_slip) {
        this.image_slip = image_slip;
    }

    public String getCashier_name() {
        return cashier_name;
    }

    public void setCashier_name(String cashier_name) {
        this.cashier_name = cashier_name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public List<String> getCashIds() {
        return cashIds;
    }

    public void setCashIds(List<String> cashIds) {
        this.cashIds = cashIds;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}
