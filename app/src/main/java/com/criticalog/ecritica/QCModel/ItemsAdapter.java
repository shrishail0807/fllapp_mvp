package com.criticalog.ecritica.QCModel;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.QCitemAdaterClick;
import com.criticalog.ecritica.MVPBarcodeScan.model.DatumItemQc;
import com.criticalog.ecritica.R;

import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>{
    Context context;
    List<DatumItemQc> qcItemList;
    QCitemAdaterClick qCitemAdaterClick;
    public ItemsAdapter(Context context, List<DatumItemQc> qcItemList,  QCitemAdaterClick qCitemAdaterClick)
    {
        this.context=context;
        this.qcItemList=qcItemList;
        this.qCitemAdaterClick=qCitemAdaterClick;
    }
    @NonNull
    @Override
    public ItemsAdapter.ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemview, parent, false);
        return new ItemsAdapter.ItemsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsAdapter.ItemsViewHolder holder, int position) {

        String first = qcItemList.get(position).getProductCode();
        String next = "<font color='#2C4E72'><b>Product Code : </b></font>";
        holder.mProductCode.setText(Html.fromHtml(next + first));

        String first1 = qcItemList.get(position).getProductDesc();
        String next1 = "<font color='#2C4E72'><b>Product Description : </b></font>";
        holder.mProductDescription.setText(Html.fromHtml(next1 + first1));


        String first2 =qcItemList.get(position).getInvoiceNumber();
        String next2 = "<font color='#2C4E72'><b>Invoice No. : </b></font>";
        holder.mSplitPcs.setText(Html.fromHtml(next2 + first2));


        String first3 = qcItemList.get(position).getLength();
        String next3 = "<font color='#2C4E72'><b>Length : </b></font>";
        holder.mLength.setText(Html.fromHtml(next3 + first3));


        String first4 = qcItemList.get(position).getBreadth();
        String next4 = "<font color='#2C4E72'><b>Breadth : </b></font>";
        holder.mB.setText(Html.fromHtml(next4 + first4));


        String first5 = qcItemList.get(position).getHeight();
        String next5 = "<font color='#2C4E72'><b>Height : </b></font>";
        holder.mH.setText(Html.fromHtml(next5 + first5));

        if(qcItemList.get(position).getQc_status().equalsIgnoreCase("Completed"))
        {
            holder.mStatusQc.setText("Completed");
            holder.mStatusQc.setTextColor(Color.GREEN);
           // holder.mStatusQc.setBackgroundResource(R.drawable.rounded_bacground_green);
        }else {
            holder.mStatusQc.setText("QC Pending");
            holder.mStatusQc.setTextColor(Color.RED);
           // holder.mStatusQc.setBackgroundResource(R.drawable.rounded_bachground_red);
        }

        if(qcItemList.get(position).getQc_pass_fail()==null)
        {
            holder.mClickhere.setText("Start QC");
        }else if(qcItemList.get(position).getQc_pass_fail().equalsIgnoreCase("0")){
            holder.mClickhere.setText("QC Fail");
        }else if(qcItemList.get(position).getQc_pass_fail().equalsIgnoreCase("1"))
        {
            holder.mClickhere.setText("QC Pass");
        }

        holder.mClickhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qCitemAdaterClick.adapterClick(position,qcItemList.get(position).getQcItemId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return qcItemList.size();
    }

    public class ItemsViewHolder extends RecyclerView.ViewHolder {
        TextView mProductCode,mProductDescription,mSplitPcs,mLength,mB,mH,mClickhere,mStatusQc;
        public ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            mProductCode=itemView.findViewById(R.id.mProductCode);
            mProductDescription=itemView.findViewById(R.id.mProductDescription);
            mSplitPcs=itemView.findViewById(R.id.mSplitPcs);
            mLength=itemView.findViewById(R.id.mLength);
            mB=itemView.findViewById(R.id.mB);
            mH=itemView.findViewById(R.id.mH);
            mClickhere=itemView.findViewById(R.id.mClickhere);
            mStatusQc=itemView.findViewById(R.id.mStatusQc);
        }
    }
}
