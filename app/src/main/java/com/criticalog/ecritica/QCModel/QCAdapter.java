package com.criticalog.ecritica.QCModel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Interface.QCAdapterClicked;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRule;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class QCAdapter extends RecyclerView.Adapter<QCAdapter.QCViewHolder> {
    Context context;
    QCAdapterClicked iAdapterClickListener;
    List<QcRule> qcRuleList;
    DatabaseHelper mDatabaseHelper;
    CriticalogSharedPreferences mCriticalogSharedPreferences;


    public QCAdapter(Context context, QCAdapterClicked iAdapterClickListener, List<QcRule> qcRuleList) {
        this.context = context;
        this.iAdapterClickListener = iAdapterClickListener;
        this.qcRuleList = qcRuleList;
        mDatabaseHelper = new DatabaseHelper(context);
        mDatabaseHelper.getWritableDatabase();
        mCriticalogSharedPreferences=CriticalogSharedPreferences.getInstance(context);
    }

    @NonNull
    @Override
    public QCAdapter.QCViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.qc_itemview, parent, false);
        return new QCAdapter.QCViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull QCAdapter.QCViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.mLabel.setText(qcRuleList.get(position).getQcLabel());

        if (qcRuleList.get(position).getQcType().equalsIgnoreCase("1")) {
            holder.mRadioLay.setVisibility(View.VISIBLE);
            if (qcRuleList.get(position).getQcUpload().equalsIgnoreCase("1")) {
                holder.mImageCapture.setVisibility(View.VISIBLE);
            }

            holder.mImageCapture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iAdapterClickListener.qcAdpaterClick(qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcType(), "yes", "upload", "");
                }
            });

            if (qcRuleList.get(position).getQcBoolResponse().equalsIgnoreCase("1")) {
                holder.radioMale.setChecked(true);
            }

            if (qcRuleList.get(position).getQcBoolResponse().equalsIgnoreCase("0")) {
                holder.radioFemale.setChecked(true);
            }

            if(!qcRuleList.get(position).getQcValueToCheck().equalsIgnoreCase(""))
            {
                if(qcRuleList.get(position).getQcValueToCheck().toString().contains("http"))
                {
                    if (!qcRuleList.get(position).getQcValueToCheck().toString().equalsIgnoreCase("")) {
                        holder.mUploadQcLay.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(qcRuleList.get(position).getQcValueToCheck().toString())
                                .resize(400, 400)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .placeholder(R.drawable.brokenimage)
                                .into(holder.mDisplayImage);
                    }

                }else {
                    holder.mSerialNumberCheck.setVisibility(View.VISIBLE);
                    holder.mSerialNumberCheck.setEnabled(false);
                    holder.mSerialNumberCheck.setText(qcRuleList.get(position).getQcValueToCheck().toString());
                }
            }

            holder.radioMale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iAdapterClickListener.qcAdpaterClick(qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcType(), "1", "", "");
                }
            });

            holder.radioFemale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    iAdapterClickListener.qcAdpaterClick(qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcType(), "0", "", "");
                }
            });
        }
        if (qcRuleList.get(position).getQcType().equalsIgnoreCase("2")) {
            holder.mSerialNumber.setVisibility(View.VISIBLE);

            //    holder.mSerialNumber.setText(qcRuleList.get(position).getQcInputResponse());
            if(!qcRuleList.get(position).getQcInputResponse().equalsIgnoreCase(""))
            {
                holder.mSerialNumber.setText(qcRuleList.get(position).getQcInputResponse());
            }
            if(!qcRuleList.get(position).getQcValueToCheck().equalsIgnoreCase(""))
            {
               // holder.mSerialNumber.setText(qcRuleList.get(position).getQcValueToCheck());
                if(!qcRuleList.get(position).getQcValueToCheck().toString().contains("http"))
                {
                    holder.mSerialNumber.setText(qcRuleList.get(position).getQcValueToCheck());
                }

            }
        }
        holder.mImageCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAdapterClickListener.qcAdpaterClick(qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcType(), "yes", "upload", "");
            }
        });
        if (qcRuleList.get(position).getQcType().equalsIgnoreCase("3")) {
            if (!qcRuleList.get(position).getQcUploadedImg().equalsIgnoreCase("")) {

                holder.mUploadQcLay.setVisibility(View.VISIBLE);
                Picasso.with(context).invalidate(qcRuleList.get(position).getQcUploadedImg());
                Picasso.with(context).load(qcRuleList.get(position).getQcUploadedImg()).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);
                Picasso.with(context)
                        .load(qcRuleList.get(position).getQcUploadedImg())
                        .resize(400, 400)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .placeholder(R.drawable.brokenimage)
                        .into(holder.mDisplayImage);
            }
            if (qcRuleList.get(position).getQcUpload().equalsIgnoreCase("1")) {
                holder.mImageCapture.setVisibility(View.VISIBLE);
            }




            if (!qcRuleList.get(position).getQcValueToCheck().toString().equalsIgnoreCase("")) {

                if(qcRuleList.get(position).getQcValueToCheck().toString().contains("http"))
                {
                    holder.mUploadQcLay.setVisibility(View.VISIBLE);
                    Picasso.with(context)
                            .load(qcRuleList.get(position).getQcValueToCheck().toString())
                            .resize(400, 400)
                            .placeholder(R.drawable.brokenimage)
                            .into(holder.mDisplayImage);
                }
              /*  holder.mUploadQcLay.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(qcRuleList.get(position).getQcValueToCheck().toString())
                        .resize(400, 400)
                        .placeholder(R.drawable.brokenimage)
                        .into(holder.mDisplayImage);*/
            }
        }
        holder.mSerialNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (holder.mSerialNumber.getRight() - holder.mSerialNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        String textToSend=holder.mSerialNumber.getText().toString();
                        if(textToSend.equalsIgnoreCase(""))
                        {
                            Toasty.warning(context, "Enter the Details!!", Toast.LENGTH_LONG, true).show();
                        }/*else if(!qcRuleList.get(position).getQcValueToCheck().equalsIgnoreCase(textToSend))
                        {
                            Toasty.warning(context, "Enter Valid Details!!", Toast.LENGTH_LONG, true).show();
                        }*/else{
                            iAdapterClickListener.qcAdpaterClick(qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcType(), "", "", textToSend);
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getItemCount() {
        return qcRuleList.size();
    }

    public class QCViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mRadioLay, mUploadQcLay;
        EditText mSerialNumber,mSerialNumberCheck;
        TextView mLabel, mImageCapture;
        ImageView mImageUpload, mDisplayImage;
        RadioButton radioMale, radioFemale;

        public QCViewHolder(@NonNull View itemView) {
            super(itemView);
            mRadioLay = itemView.findViewById(R.id.mRadioLay);
            mUploadQcLay = itemView.findViewById(R.id.mUploadQcLay);
            mSerialNumber = itemView.findViewById(R.id.mSerialNumber);
            mLabel = itemView.findViewById(R.id.mLabel);
            mImageUpload = itemView.findViewById(R.id.mImageUpload);
            mDisplayImage = itemView.findViewById(R.id.mDisplayImage);
            radioMale = itemView.findViewById(R.id.radioMale);
            radioFemale = itemView.findViewById(R.id.radioFemale);
            mImageCapture = itemView.findViewById(R.id.mImageCapture);
            mSerialNumberCheck=itemView.findViewById(R.id.mSerialNumberCheck);
        }
    }
}
