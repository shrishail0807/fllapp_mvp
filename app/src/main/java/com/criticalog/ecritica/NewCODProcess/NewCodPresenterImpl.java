package com.criticalog.ecritica.NewCODProcess;

import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PayTypeListResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.ChequeCollectedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;

public class NewCodPresenterImpl implements NewCodProcessContract.presenter, NewCodProcessContract.GetBankListIntractor.OnFinishedListener, NewCodProcessContract.GetPaymentLinkIntractor.OnFinishedListener, NewCodProcessContract.GetQRCodeLinkIntractor.OnFinishedListener,
        NewCodProcessContract.GetCodProofUploadIntractor.OnFinishedListener, NewCodProcessContract.GetRefreshUPIIntractor.OnFinishedListener, NewCodProcessContract.GetCodChequeReceivedIntractor.OnFinishedListener,
        NewCodProcessContract.GetCodChequeCollectionIntractor.OnFinishedListener, NewCodProcessContract.NewCodChequeCollectionIntractor.OnFinishedListener {
    NewCodProcessContract.MainView mainView;
    NewCodProcessContract.GetBankListIntractor getBankListIntractor;
    private NewCodProcessContract.GetPaymentLinkIntractor getPaymentLinkIntractor;
    private NewCodProcessContract.GetQRCodeLinkIntractor getQRCodeLinkIntractor;
    private NewCodProcessContract.GetCodProofUploadIntractor getCodProofUploadIntractor;
    private NewCodProcessContract.GetRefreshUPIIntractor getRefreshUPIIntractor;
    private NewCodProcessContract.GetCodChequeReceivedIntractor getCodChequeReceivedIntractor;
    private NewCodProcessContract.GetCodChequeCollectionIntractor getCodChequeCollectionIntractor;
    private NewCodProcessContract.NewCodChequeCollectionIntractor newCodChequeCollectionIntractor;

    public NewCodPresenterImpl(NewCodProcessContract.MainView mainView, NewCodProcessContract.GetBankListIntractor getBankListIntractor,
                               NewCodProcessContract.GetPaymentLinkIntractor getPaymentLinkIntractor,
                               NewCodProcessContract.GetQRCodeLinkIntractor getQRCodeLinkIntractor,
                               NewCodProcessContract.GetCodProofUploadIntractor getCodProofUploadIntractor,
                               NewCodProcessContract.GetRefreshUPIIntractor getRefreshUPIIntractor,
                               NewCodProcessContract.GetCodChequeReceivedIntractor getCodChequeReceivedIntractor,
                               NewCodProcessContract.GetCodChequeCollectionIntractor getCodChequeCollectionIntractor,
                               NewCodProcessContract.NewCodChequeCollectionIntractor newCodChequeCollectionIntractor) {
        this.mainView = mainView;
        this.getBankListIntractor = getBankListIntractor;
        this.getPaymentLinkIntractor = getPaymentLinkIntractor;
        this.getQRCodeLinkIntractor = getQRCodeLinkIntractor;
        this.getCodProofUploadIntractor = getCodProofUploadIntractor;
        this.getRefreshUPIIntractor = getRefreshUPIIntractor;
        this.getCodChequeReceivedIntractor = getCodChequeReceivedIntractor;
        this.getCodChequeCollectionIntractor=getCodChequeCollectionIntractor;
        this.newCodChequeCollectionIntractor=newCodChequeCollectionIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void onBankListRequest(BankListRequest bankListRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getBankListIntractor.bankListSuccessful(this, bankListRequest);
    }

    @Override
    public void paymentLinkRequest(PaymentLinkRequest paymentLinkRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getPaymentLinkIntractor.paymentLinkSuccessful(this, paymentLinkRequest);
    }

    @Override
    public void generateQRCodeLink(QRCodeLinkRequest qrCodeLinkRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getQRCodeLinkIntractor.paymentLinkSuccessful(this, qrCodeLinkRequest);
    }

    @Override
    public void uploadCodProofImage(UploadCodProofRequest uploadCodProofRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getCodProofUploadIntractor.uploadCodProofSuccessful(this, uploadCodProofRequest);
    }

    @Override
    public void refreshUPIRequest(RefreshUPIRequest refreshUPIRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getRefreshUPIIntractor.uploadCodProofSuccessful(this, refreshUPIRequest);
    }

    @Override
    public void codChequeReceivedRequest(CodChequeReceivedRequest codChequeReceivedRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getCodChequeReceivedIntractor.codChequeRecivedSuccessful(this, codChequeReceivedRequest);
    }

    @Override
    public void codChequeCollectionRequest(CodChequeCollectionRequest codChequeCollectionRequest) {
        if(mainView!=null)
        {
           mainView.showProgress();
        }

        getCodChequeCollectionIntractor.codChequeCollectionSuccessful(this,codChequeCollectionRequest );
    }

    @Override
    public void newChequeCollectedRequest(ChequeCollectedRequest chequeCollectedRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }
        newCodChequeCollectionIntractor.newCodChequeCollectionSuccessful(this,chequeCollectedRequest);
    }


    @Override
    public void onFinished(BankListResponse bankListResponse) {
        if (mainView != null) {
            mainView.setBankListDataToViews(bankListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(PaymentLinkResponse paymentLinkResponse) {
        if (mainView != null) {
            mainView.paymentLinkResponse(paymentLinkResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(QRCodeLinkResponse qrCodeLinkResponse) {
        if (mainView != null) {
            mainView.qrCodeLinkResponse(qrCodeLinkResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(UploadCodProofResponse uploadCodProofResponse) {
        if (mainView != null) {
            mainView.codProofResponse(uploadCodProofResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(RefreshUPIResponse refreshUPIResponse) {
        if (mainView != null) {
            mainView.refershUpiResponse(refreshUPIResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CodChequeReceivedResponse codChequeReceivedResponse) {
        if (mainView != null) {
            mainView.codChequeReceivedResponse(codChequeReceivedResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CodChequeCollectionResponse codChequeCollectionResponse) {
        if(mainView!=null)
        {
            mainView.codChequeCollectionResponse(codChequeCollectionResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
