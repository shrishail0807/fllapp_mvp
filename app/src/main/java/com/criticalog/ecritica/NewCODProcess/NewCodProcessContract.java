package com.criticalog.ecritica.NewCODProcess;

import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.ChequeCollectedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;

public interface NewCodProcessContract {
    interface presenter {

        void onDestroy();

        void onBankListRequest(BankListRequest bankListRequest);

        void paymentLinkRequest(PaymentLinkRequest paymentLinkRequest);

        void generateQRCodeLink(QRCodeLinkRequest qrCodeLinkRequest);

        void uploadCodProofImage(UploadCodProofRequest uploadCodProofRequest);

        void refreshUPIRequest(RefreshUPIRequest refreshUPIRequest);

        void codChequeReceivedRequest(CodChequeReceivedRequest codChequeReceivedRequest);

        void codChequeCollectionRequest(CodChequeCollectionRequest codChequeCollectionRequest);

        void newChequeCollectedRequest(ChequeCollectedRequest chequeCollectedRequest);


    }

    interface MainView {

        void showProgress();

        void hideProgress();

        void setBankListDataToViews(BankListResponse bankListResponse);

        void paymentLinkResponse(PaymentLinkResponse paymentLinkResponse);

        void qrCodeLinkResponse(QRCodeLinkResponse qrCodeLinkResponse);

        void codProofResponse(UploadCodProofResponse uploadCodProofResponse);

        void refershUpiResponse(RefreshUPIResponse refreshUPIResponse);

        void codChequeReceivedResponse(CodChequeReceivedResponse codChequeReceivedResponse);

        void codChequeCollectionResponse(CodChequeCollectionResponse codChequeCollectionResponse);

        void newChequeCollectedResponse(CodChequeCollectionResponse codChequeCollectionResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetBankListIntractor {

        interface OnFinishedListener {
            void onFinished(BankListResponse bankListResponse);

            void onFailure(Throwable t);
        }

        void bankListSuccessful(NewCodProcessContract.GetBankListIntractor.OnFinishedListener onFinishedListener, BankListRequest bankListRequest);
    }

    interface GetPaymentLinkIntractor {
        interface OnFinishedListener {
            void onFinished(PaymentLinkResponse paymentLinkResponse);

            void onFailure(Throwable t);
        }

        void paymentLinkSuccessful(NewCodProcessContract.GetPaymentLinkIntractor.OnFinishedListener onFinishedListener, PaymentLinkRequest paymentLinkRequest);
    }

    interface GetQRCodeLinkIntractor {
        interface OnFinishedListener {
            void onFinished(QRCodeLinkResponse qrCodeLinkResponse);

            void onFailure(Throwable t);
        }

        void paymentLinkSuccessful(NewCodProcessContract.GetQRCodeLinkIntractor.OnFinishedListener onFinishedListener, QRCodeLinkRequest qrCodeLinkRequest);
    }

    interface GetCodProofUploadIntractor {
        interface OnFinishedListener {
            void onFinished(UploadCodProofResponse uploadCodProofResponse);

            void onFailure(Throwable t);
        }

        void uploadCodProofSuccessful(NewCodProcessContract.GetCodProofUploadIntractor.OnFinishedListener onFinishedListener, UploadCodProofRequest uploadCodProofRequest);
    }

    interface GetRefreshUPIIntractor {
        interface OnFinishedListener {
            void onFinished(RefreshUPIResponse refreshUPIResponse);

            void onFailure(Throwable t);
        }

        void uploadCodProofSuccessful(NewCodProcessContract.GetRefreshUPIIntractor.OnFinishedListener onFinishedListener, RefreshUPIRequest refreshUPIRequest);
    }

    interface GetCodChequeReceivedIntractor {
        interface OnFinishedListener {
            void onFinished(CodChequeReceivedResponse codChequeReceivedResponse);

            void onFailure(Throwable t);
        }

        void codChequeRecivedSuccessful(NewCodProcessContract.GetCodChequeReceivedIntractor.OnFinishedListener onFinishedListener, CodChequeReceivedRequest codChequeReceivedRequest);
    }

    interface GetCodChequeCollectionIntractor {
        interface OnFinishedListener {
            void onFinished(CodChequeCollectionResponse codChequeCollectionResponse);

            void onFailure(Throwable t);
        }

        void codChequeCollectionSuccessful(NewCodProcessContract.GetCodChequeCollectionIntractor.OnFinishedListener onFinishedListener, CodChequeCollectionRequest codChequeCollectionRequest);
    }

    interface NewCodChequeCollectionIntractor {
        interface OnFinishedListener {
            void onFinished(CodChequeCollectionResponse codChequeCollectionResponse);

            void onFailure(Throwable t);
        }

        void newCodChequeCollectionSuccessful(NewCodProcessContract.NewCodChequeCollectionIntractor.OnFinishedListener onFinishedListener, ChequeCollectedRequest chequeCollectedRequest);
    }
}
