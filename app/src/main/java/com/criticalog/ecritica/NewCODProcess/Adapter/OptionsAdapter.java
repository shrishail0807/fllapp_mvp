package com.criticalog.ecritica.NewCODProcess.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.OptionsAdapterClicked;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.NewCODProcess.Model.DatumPayType;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.OptionsVh> {

    List<DatumPayType> scannedresult;
    Context context;
    OptionsAdapterClicked adapterClick;
    CriticalogSharedPreferences mCriticalogSharedPreferences;

    public OptionsAdapter(List<DatumPayType> scannedresult, Context context, OptionsAdapterClicked adapterClick) {
        this.scannedresult = scannedresult;
        this.context = context;
        this.adapterClick = adapterClick;
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
    }

    @NonNull
    @Override
    public OptionsAdapter.OptionsVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.options_tems, parent, false);
        return new OptionsAdapter.OptionsVh(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionsVh holder, int position) {

        holder.textView.setText(scannedresult.get(position).getDisplayName());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("payment_qr_link", scannedresult.get(position).getQrLink().toString());
                adapterClick.onclickAdapter(scannedresult.get(position).getValue(), holder.textView.getText().toString());
            }
        });
    }

  /*  @Override
    public void onBindViewHolder(@NonNull InscanBoxAdapter.InscanVh holder, int position) {

        holder.textView.setText(scannedresult.get(position).toString());
    }*/

    @Override
    public int getItemCount() {
        return scannedresult.size();
    }

    public class OptionsVh extends RecyclerView.ViewHolder {
        TextView textView;

        public OptionsVh(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textView3);
        }
    }
}
