package com.criticalog.ecritica.NewCODProcess;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class ShowQrCodeActivity extends AppCompatActivity implements NewCodProcessContract.MainView {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private ImageView mPaymentImage;
    File photoFile = null;
    private static int CAMERA_REQUEST = 1888;
    private String base64PayemntRefImage = "";
    NewCodProcessContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String coonoteNum, payValue, Drs_no, task_id, userId, token, partPayment;
    private TextView mSubmit, mImageCaptureNumber;
    SimpleArcDialog mProgressDialog;
    ArrayList<String> imagesList = new ArrayList<>();
    private TextView mTopBarText;
    private ImageView mTvBackbutton;
    private EditText mFtcRefNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showqr);

        mProgressDialog = new SimpleArcDialog(this);
        mProgressDialog.setTitle("Loading......");
        mProgressDialog.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        mPaymentImage = findViewById(R.id.mPaymentImage);
        mSubmit = findViewById(R.id.mSubmit);
        mTopBarText=findViewById(R.id.mTopBarText);
        mTvBackbutton=findViewById(R.id.mTvBackbutton);
        mImageCaptureNumber = findViewById(R.id.mImageCaptureNumber);
        mFtcRefNumber=findViewById(R.id.mFtcRefNumber);
        mPresenter = new NewCodPresenterImpl(ShowQrCodeActivity.this,new NewCodInteractorImpl(), new NewCodInteractorImpl(),new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl());
        if (checkPermission()) {
            //main logic or main code
            // . write your main code to execute, It will execute if the permission is already given.
        } else {
            requestPermission();
        }

        mTopBarText.setText("Scan QR Code to Pay");
        coonoteNum = mCriticalogSharedPreferences.getData("task_id");
        payValue = mCriticalogSharedPreferences.getData("payment_amount");
        Drs_no = mCriticalogSharedPreferences.getData("drs_number");
        task_id = mCriticalogSharedPreferences.getData("task_id");

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        partPayment = mCriticalogSharedPreferences.getData("part_payment");

        ImageView qrcode = findViewById(R.id.mQRImage);
        String linkFromAPI = mCriticalogSharedPreferences.getData("payment_qr_link");
        if (linkFromAPI.equalsIgnoreCase("")) {

            // Glide.with(this).load("https://ecritica.co/eFreightLive/qr/codaccount.PNG").error(R.drawable.criticalog_qr1).into(qrcode);
            Picasso.with(this)
                    .load("https://ecritica.co/eFreightLive/qr/codaccount.PNG")
                    .resize(1500, 2800)
                    .placeholder(R.drawable.brokenimage)
                    .into(qrcode);
        } else {
            //Glide.with(this).load(linkFromAPI).error(R.drawable.criticalog_qr1).into(qrcode);

            Picasso.with(this)
                    .load(linkFromAPI)
                    .resize(1500, 2800)
                    .placeholder(R.drawable.brokenimage)
                    .into(qrcode);
        }

        mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mPaymentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = imagesList.size();
                if (size == 5) {
                    Toasty.warning(ShowQrCodeActivity.this, "You Can't Capture More than 5 Images!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    openCameraIntent();
                }

            }
        });
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = imagesList.size();
                String ftcRefNum=mFtcRefNumber.getText().toString();
                if(ftcRefNum.equalsIgnoreCase(""))
                {
                    Toast.makeText(ShowQrCodeActivity.this, "Enter Payment Reference Number!", Toast.LENGTH_SHORT).show();
                }else {
                    callImageUpload(ftcRefNum);
                }

            }
        });


    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                //Uri photoURI = FileProvider.getUriForFile(this,".provider",photoFile);
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the intent to camera or gallery worked then the code is

        if (requestCode == 1888) {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                //  Bitmap photo = (Bitmap) data.getExtras().get("data");

                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    mPaymentImage.setImageBitmap(myBitmap);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64PayemntRefImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                    if (partPayment.equalsIgnoreCase("true")) {
                        imagesList.add(base64PayemntRefImage);
                    } else {
                        base64PayemntRefImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    }

                    if (imagesList.size() > 0) {
                        mImageCaptureNumber.setText("Image Captured: " + String.valueOf(imagesList.size()));
                    }
                }
                //mCriticalogSharedPreferences.saveData("image_captured", "true");
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }


    private boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void showProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void setBankListDataToViews(BankListResponse bankListResponse) {

    }

    @Override
    public void paymentLinkResponse(PaymentLinkResponse paymentLinkResponse) {

    }

    @Override
    public void qrCodeLinkResponse(QRCodeLinkResponse qrCodeLinkResponse) {

    }

    @Override
    public void codProofResponse(UploadCodProofResponse uploadCodProofResponse) {

        /*if (uploadCodProofResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(ShowQrCodeActivity.this, uploadCodProofResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            startActivity(new Intent(this, DRSCloseActivity.class));
            finish();
        } else {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "");
            Toasty.warning(ShowQrCodeActivity.this, uploadCodProofResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }*/
    }

    @Override
    public void refershUpiResponse(RefreshUPIResponse refreshUPIResponse) {

    }

    @Override
    public void codChequeReceivedResponse(CodChequeReceivedResponse codChequeReceivedResponse) {

    }

    @Override
    public void codChequeCollectionResponse(CodChequeCollectionResponse codChequeCollectionResponse) {
        if (codChequeCollectionResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(ShowQrCodeActivity.this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            startActivity(new Intent(this, DRSCloseActivity.class));
            finish();
        } else {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "");
            Toasty.warning(ShowQrCodeActivity.this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void newChequeCollectedResponse(CodChequeCollectionResponse codChequeCollectionResponse) {

    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.success(ShowQrCodeActivity.this, "Try Again!!", Toast.LENGTH_SHORT, true).show();
    }

    public void callImageUpload(String ftcRefNum) {

        int payTypeValue=mCriticalogSharedPreferences.getIntData("paytype_value");
        CodChequeCollectionRequest codChequeCollectionRequest=new CodChequeCollectionRequest();
        codChequeCollectionRequest.setAction("cod_collection_received");
        codChequeCollectionRequest.setPaymentRecType(String.valueOf(payTypeValue));
        codChequeCollectionRequest.setDocketNo(task_id);
        codChequeCollectionRequest.setUserId(userId);
        codChequeCollectionRequest.setUpirefno(ftcRefNum);
        codChequeCollectionRequest.setCodImage(base64PayemntRefImage);
        codChequeCollectionRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
        codChequeCollectionRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
        mPresenter.codChequeCollectionRequest(codChequeCollectionRequest);
     /*   UploadCodProofRequest uploadCodProofRequest = new UploadCodProofRequest();
        uploadCodProofRequest.setAction("upload_cod_proof");
        uploadCodProofRequest.setDocketNo(task_id);
        uploadCodProofRequest.setUserId(userId);
        uploadCodProofRequest.setCodImage(base64PayemntRefImage);

        mPresenter.uploadCodProofImage(uploadCodProofRequest);*/
    }


}
