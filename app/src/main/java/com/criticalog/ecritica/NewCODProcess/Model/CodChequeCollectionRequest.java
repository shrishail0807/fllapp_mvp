package com.criticalog.ecritica.NewCODProcess.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CodChequeCollectionRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("payment_rec_type")
    @Expose
    private String paymentRecType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("bank_code")
    @Expose
    private String bankCode;
    @SerializedName("cheque_amount")
    @Expose
    private String chequeAmount;
    @SerializedName("cheque_number")
    @Expose
    private String chequeNumber;
    @SerializedName("cheque_date")
    @Expose
    private String chequeDate;
    @SerializedName("cod_image")
    @Expose
    private String codImage;
    @SerializedName("upirefno")
    @Expose
    private String upirefno;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("other_bank_name")
    @Expose
    private String other_bank_name;

    public String getOther_bank_name() {
        return other_bank_name;
    }

    public void setOther_bank_name(String other_bank_name) {
        this.other_bank_name = other_bank_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPaymentRecType() {
        return paymentRecType;
    }

    public void setPaymentRecType(String paymentRecType) {
        this.paymentRecType = paymentRecType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getChequeAmount() {
        return chequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        this.chequeAmount = chequeAmount;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getCodImage() {
        return codImage;
    }

    public void setCodImage(String codImage) {
        this.codImage = codImage;
    }

    public String getUpirefno() {
        return upirefno;
    }

    public void setUpirefno(String upirefno) {
        this.upirefno = upirefno;
    }
}
