package com.criticalog.ecritica.NewCODProcess.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentLinkRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
