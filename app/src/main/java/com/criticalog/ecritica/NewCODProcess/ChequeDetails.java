package com.criticalog.ecritica.NewCODProcess;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSListContract;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPDRS.DRSPresenterImpl;
import com.criticalog.ecritica.MVPDRS.GetDrsInteractorImpl;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.MVPPickup.PickupCancel.DatumReasons;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.Cheque;
import com.criticalog.ecritica.NewCODProcess.Model.ChequeCollectedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.DataBank;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PhotoData;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChequeDetails extends AppCompatActivity implements View.OnClickListener, NewCodProcessContract.MainView, DRSListContract.MainView {
    private TextView mTopBarText, mCheckCollected, mChequeNotCollected;
    private EditText mChequeBankName, mCheckAmount, mChekNumber, mDate;
    private int year, month, day;
    private static final int CAMERA_INTENT = 111;
    private static final int GALLERY_INTENT = 222;
    private PhotoData photoData;
    private ImageView mChequeImage, mTvBackbutton;
    File photoFile = null;
    private static int CAMERA_REQUEST = 1888;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private NewCodProcessContract.presenter mPresenter;
    DRSListContract.presenter mPresenterNDC;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token, base64ChequeImage = "", Drs_no, task_id, paymentAmount, maxCheque;
    SimpleArcDialog mProgressDialog;
    ArrayList<SampleSearchModel> sampleSearchModelArrayList = new ArrayList<>();
    private List<DatumReasons> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    int mHour;
    int mMinute;
    private double latitude = 0, longitude = 0;
    private RestServices mRestServices;
    private String reasonCode = "", refno, bulkFlag;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private String bankcode = "", bankName = "";
    private LocationCallback locationCallback;
    private ImageView mChequeRefImageOnline;
    private EditText mEnterBankName;
    private String enteredBankName = "";
    private TextView mChequeCollecteNumber, mScanNextCheque, mChequeTotalAmount, mUpdateAlreadyExistData;
    private List<Cheque> chequeList = new ArrayList<>();
    String chequeAmt;
    String checkNumber;
    String date;
    String chequeAmount;
    String otherBankName;
    ImageView mBackwardArrow, mForwardArraow;
    private int indexFlag = 0;
    private static int enteredAmount = 0;
    private ArrayList<String> chequeNumListString = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_details);
        mTopBarText = findViewById(R.id.mTopBarText);
        mChequeBankName = findViewById(R.id.mChequeBankName);
        mCheckAmount = findViewById(R.id.mCheckAmount);
        mChekNumber = findViewById(R.id.mChekNumber);
        mDate = findViewById(R.id.mDate);
        mChequeImage = findViewById(R.id.mChequeImage);
        mCheckCollected = findViewById(R.id.mCheckCollected);
        mChequeNotCollected = findViewById(R.id.mChequeNotCollected);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mChequeRefImageOnline = findViewById(R.id.mChequeRefImageOnline);
        mEnterBankName = findViewById(R.id.mEnterBankName);
        mChequeCollecteNumber = findViewById(R.id.mChequeCollecteNumber);
        mScanNextCheque = findViewById(R.id.mScanNextCheque);
        mChequeTotalAmount = findViewById(R.id.mChequeTotalAmount);
        mForwardArraow = findViewById(R.id.mForwardArraow);
        mBackwardArrow = findViewById(R.id.mBackwardArrow);
        mUpdateAlreadyExistData = findViewById(R.id.mUpdateAlreadyExistData);

        if (checkPermission()) {
            //main logic or main code
            // . write your main code to execute, It will execute if the permission is already given.
        } else {
            requestPermission();
        }
        mProgressDialog = new SimpleArcDialog(this);
        mProgressDialog.setTitle("Loading......");
        mProgressDialog.setCancelable(false);
        mChequeBankName.setFocusable(false);
        mChequeBankName.setClickable(true);

        mDate.setFocusable(false);
        mDate.setClickable(true);
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds


        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);

        mPresenter = new NewCodPresenterImpl(ChequeDetails.this, new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl());
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        Drs_no = mCriticalogSharedPreferences.getData("drs_number");
        task_id = mCriticalogSharedPreferences.getData("task_id");
        paymentAmount = mCriticalogSharedPreferences.getData("payment_amount");
        maxCheque = mCriticalogSharedPreferences.getData("max_cheque_num");

        mChequeTotalAmount.setText("Total Cheque Amount: " + paymentAmount);

        // mCheckAmount.setEnabled(false);
        //  mCheckAmount.setText(paymentAmount);
        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        //askForPermissions();
        photoData = new PhotoData(this);

        mTopBarText.setText("Cheque Details");

        mChequeBankName.setOnClickListener(this);
        mDate.setOnClickListener(this);
        mChequeImage.setOnClickListener(this);
        mCheckCollected.setOnClickListener(this);
        mChequeNotCollected.setOnClickListener(this);
        mTvBackbutton.setOnClickListener(this);
        mScanNextCheque.setOnClickListener(this);
        mBackwardArrow.setOnClickListener(this);
        mForwardArraow.setOnClickListener(this);
        mUpdateAlreadyExistData.setOnClickListener(this);

        Picasso.with(this)
                .load("https://www.ecritica.co/efreightlive/media/app/chequeImg.png")
                .resize(2200, 400)
                .placeholder(R.drawable.brokenimage)
                .into(mChequeRefImageOnline);
    }


    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }

    }

    private boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere, List<DataBank> dataBankList) {

        new SimpleSearchDialogCompat(ChequeDetails.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source
                        if (item.getTitle().equalsIgnoreCase("OTHER BANK")) {
                            mEnterBankName.setVisibility(View.VISIBLE);
                        } else {
                            mEnterBankName.setVisibility(View.GONE);
                            mEnterBankName.setText("");
                        }

                        bankName = item.getTitle();
                        mChequeBankName.setText(item.getTitle());

                        bankcode = dataBankList.get(position).getBankCode();

                        dialog.dismiss();
                    }
                }).show();
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void takePhoto() {
        File photo = null;
        try {
            photo = photoData.createImageFile();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            photoData.saveUriPath(Uri.fromFile(photo).getPath());
            startActivityForResult(intent, CAMERA_INTENT);
        } catch (IOException e) {
            Log.e("EXC", e.toString());
            //TODO warn the user the photo fail
        }
    }

    /*public void askForPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivity(intent);
                return;
            }
            //createDir();
        }
    }*/

    private void datePicker(String task_id, int which) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        //  mCriticalogSharedPreferences.saveData("R_DATE", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        Log.e("DATE_FORMAT", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        mDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        //tiemPicker((year + "-" + (monthOfYear + 1) + "-" + dayOfMonth), task_id, which);
                    }
                }, year, month, day);
        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the intent to camera or gallery worked then the code is

        if (requestCode == 1888) {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                if (chequeList.contains(mChekNumber.getText().toString())) {
                    Toasty.error(this, "Cheque Number Already Exist", Toast.LENGTH_SHORT, true).show();
                } else {

                }

                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64ChequeImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                    if (chequeList.size() > 0) {
                        mChequeCollecteNumber.setText("Cheque Collected: " + String.valueOf(chequeList.size()));
                        //  mScanNextCheque.setVisibility(View.VISIBLE);
                    }
                    if (chequeList.size() == Integer.parseInt(maxCheque)) {
                        //mScanNextCheque.setVisibility(View.GONE);
                    }


                    bankName = mChequeBankName.getText().toString();
                    chequeAmt = mCheckAmount.getText().toString();
                    checkNumber = mChekNumber.getText().toString();
                    date = mDate.getText().toString();
                    if (bankName.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                    } else if (chequeAmt.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.length() != 6) {
                        Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (date.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                    } else if (base64ChequeImage.equalsIgnoreCase("")) {
                        Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                    } else {
                        CodChequeCollectionRequest codChequeCollectionRequest1 = new CodChequeCollectionRequest();
                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                            enteredBankName = mEnterBankName.getText().toString();

                            if (enteredBankName.equalsIgnoreCase("")) {
                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                            } else {
                                mChequeImage.setImageBitmap(myBitmap);

                                Cheque cheque = new Cheque();
                                cheque.setChequeDate(mDate.getText().toString());
                                cheque.setChequeAmount(mCheckAmount.getText().toString());
                                cheque.setBankCode(bankcode);
                                cheque.setCodImage(base64ChequeImage);
                                cheque.setOther_bank(enteredBankName);
                                cheque.setChequeNumber(mChekNumber.getText().toString());

                                if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                                    // Toast.makeText(this, "Already Cheque Number Exist......only updating image", Toast.LENGTH_SHORT).show();
                                    cheque.setCodImage(base64ChequeImage);
                                    chequeList.set(indexFlag, cheque);
                                } else {
                                    chequeNumListString.add(mChekNumber.getText().toString());
                                    chequeList.add(cheque);
                                    indexFlag = chequeList.size() - 1;
                                }

                            }
                        } else {
                            mChequeImage.setImageBitmap(myBitmap);

                            Cheque cheque = new Cheque();
                            cheque.setChequeDate(mDate.getText().toString());
                            cheque.setChequeAmount(mCheckAmount.getText().toString());
                            cheque.setBankCode(bankcode);
                            cheque.setCodImage(base64ChequeImage);
                            cheque.setOther_bank(enteredBankName);
                            cheque.setChequeNumber(mChekNumber.getText().toString());

                            if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                                //  Toast.makeText(this, "Index  " + String.valueOf(indexFlag), Toast.LENGTH_SHORT).show();
                                cheque.setCodImage(base64ChequeImage);
                                chequeList.set(indexFlag, cheque);

                                //Toasty.error(this, "Cheque Number Already Exist...data replaced", Toast.LENGTH_SHORT, true).show();
                            } else {
                                chequeNumListString.add(mChekNumber.getText().toString());
                                chequeList.add(cheque);
                                indexFlag = chequeList.size() - 1;
                            }

                        }
                    }
                    if (chequeList.size() > 0) {
                        mChequeCollecteNumber.setText("Cheque Collected: " + String.valueOf(chequeList.size()));

                        mUpdateAlreadyExistData.setVisibility(View.VISIBLE);
                    }

                    if (indexFlag == 0) {
                        mForwardArraow.setVisibility(View.VISIBLE);
                        mBackwardArrow.setVisibility(View.INVISIBLE);
                    } else if (indexFlag == chequeList.size() - 1) {
                        mBackwardArrow.setVisibility(View.VISIBLE);
                        mForwardArraow.setVisibility(View.VISIBLE);
                    } else {
                        mForwardArraow.setVisibility(View.VISIBLE);
                        mBackwardArrow.setVisibility(View.VISIBLE);
                    }
                    if (chequeList.size() == Integer.parseInt(maxCheque)) {
                        mScanNextCheque.setVisibility(View.GONE);
                    }

                }
            }
        }
    }

    public void checkAndStoreDetails() {
        bankName = mChequeBankName.getText().toString();
        chequeAmt = mCheckAmount.getText().toString();
        checkNumber = mChekNumber.getText().toString();
        date = mDate.getText().toString();
        if (bankName.equalsIgnoreCase("OTHER BANK")) {
            otherBankName = mEnterBankName.getText().toString();
        } else {
            otherBankName = "";
        }
        if (bankName.equalsIgnoreCase("")) {
            Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
        } else if (chequeAmt.equalsIgnoreCase("")) {
            Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
        } else if (checkNumber.equalsIgnoreCase("")) {
            Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
        } else if (checkNumber.length() != 6) {
            Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
        } else if (date.equalsIgnoreCase("")) {
            Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
        } else {
            Cheque cheque = new Cheque();
            if (indexFlag == chequeList.size()) {
                if (base64ChequeImage.equalsIgnoreCase("")) {
                    Toast.makeText(this, "CAPTURE CHEQUE IMAGE", Toast.LENGTH_SHORT).show();
                } else {
                    cheque.setChequeDate(date);
                    cheque.setChequeAmount(chequeAmt);

                    cheque.setCodImage(base64ChequeImage);
                    if (!otherBankName.equalsIgnoreCase("")) {
                        cheque.setBankCode("OTHER BANK");
                    } else {
                        cheque.setBankCode(bankName);
                    }

                    cheque.setOther_bank(otherBankName);
                    // cheque.setBankCode(bankName);
                    cheque.setChequeNumber(checkNumber);
                    if (!otherBankName.equalsIgnoreCase("")) {
                        cheque.setOther_bank(mEnterBankName.getText().toString());
                    } else {
                        cheque.setOther_bank("");
                    }
                    chequeList.add(cheque);
                }
            } else {
                cheque.setCodImage(chequeList.get(indexFlag).getCodImage());
                cheque.setChequeDate(date);
                cheque.setChequeAmount(chequeAmt);
                if (!otherBankName.equalsIgnoreCase("")) {
                    cheque.setBankCode("OTHER BANK");
                } else {
                    cheque.setBankCode(bankName);
                }

                cheque.setOther_bank(otherBankName);
                // cheque.setBankCode(bankName);
                cheque.setChequeNumber(checkNumber);
                if (!otherBankName.equalsIgnoreCase("")) {
                    cheque.setOther_bank(mEnterBankName.getText().toString());
                } else {
                    cheque.setOther_bank("");
                }
                chequeList.set(indexFlag, cheque);
                Toasty.success(this, "Data Updated Successfully", Toast.LENGTH_SHORT, true).show();
            }


            // Toast.makeText(this, "INDEX SAVE"+String.valueOf(indexFlag), Toast.LENGTH_SHORT).show();

           /* cheque.setChequeDate(date);
            cheque.setChequeAmount(chequeAmt);
            if (!otherBankName.equalsIgnoreCase("")) {
                cheque.setBankCode("Other");
            } else {
                cheque.setBankCode(bankName);
            }

            cheque.setOther_bank(otherBankName);
            // cheque.setBankCode(bankName);
            cheque.setChequeNumber(checkNumber);
            if (!otherBankName.equalsIgnoreCase("")) {
                cheque.setOther_bank(mEnterBankName.getText().toString());
            } else {
                cheque.setOther_bank("");
            }
            chequeList.set(indexFlag, cheque);
            Toasty.success(this, "Data Updated Successfully", Toast.LENGTH_SHORT, true).show();*/

        }

    }

    public void connoteNotCollectedNDC() {
        PickupReasonsRequest pickupReasonsRequest = new PickupReasonsRequest();
        pickupReasonsRequest.setUserId(userId);
        pickupReasonsRequest.setProductStatus("delivery");
        pickupReasonsRequest.setAction("reasonscancel");
        pickupReasonsRequest.setLatitude(String.valueOf(latitude));
        pickupReasonsRequest.setLongitude(String.valueOf(longitude));
        mProgressDialog.show();
        reasons.clear();
        reasonList.clear();
        Call<PickupReasonsResponse> pickupReasonsResponseCall = mRestServices.pickupCancelReasons(pickupReasonsRequest);

        pickupReasonsResponseCall.enqueue(new Callback<PickupReasonsResponse>() {
            @Override
            public void onResponse(Call<PickupReasonsResponse> call, Response<PickupReasonsResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        ;
                        reasons = response.body().getData();

                        for (int i = 0; i < reasons.size(); i++) {

                            reasonList.add(reasons.get(i).getReasondesc());
                        }
                        showCancelDialog();
                    } else {
                        mProgressDialog.dismiss();
                    }
                } else {
                    mProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PickupReasonsResponse> call, Throwable t) {

            }
        });
    }

    private void showCancelDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(ChequeDetails.this)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        reasonCode = reasons.get(which).getReasoncode();
                        Toast.makeText(ChequeDetails.this, reasonCode, Toast.LENGTH_SHORT).show();
                        if (reasonCode.equalsIgnoreCase("CI")) {
                            //  datePicker(taskNo, which);
                        } else if (reasonCode.equalsIgnoreCase("LD")) {
                            //datePicker(taskNo, which);
                        } else {
                            mCriticalogSharedPreferences.saveData("R_DATE", "");
                            mCriticalogSharedPreferences.saveData("R_TIME", "");
                            if (which == 0 || which == 3) {
                                //  getReferenceNo(which, taskNo);
                                callDRSCancel();
                            } else {
                                callDRSCancel();
                            }
                        }
                        return true; // allow selection
                    }
                })
                .show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ChequeDetails.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ChequeDetails.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ChequeDetails.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(ChequeDetails.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void callDRSCancel() {
        getLocation();
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c);

        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        String time = df1.format(c);
        if (mCriticalogSharedPreferences.getData("bulk_flag").equalsIgnoreCase("true")) {
            bulkFlag = "1";
        } else {
            bulkFlag = "0";
        }
        DrsCancelRequest drsCancelRequest = new DrsCancelRequest();
        drsCancelRequest.setAction("drs_cancel");
        drsCancelRequest.setBulkFlag(bulkFlag);
        drsCancelRequest.setDate(date);
        drsCancelRequest.setTime(time);
        drsCancelRequest.setrDate(mCriticalogSharedPreferences.getData("R_DATE"));
        drsCancelRequest.setrTime(mCriticalogSharedPreferences.getData("R_TIME"));
        drsCancelRequest.setLatitude(String.valueOf(latitude));
        drsCancelRequest.setLongitude(String.valueOf(longitude));
        drsCancelRequest.setReasons(reasonCode);
        drsCancelRequest.setRefcount(mCriticalogSharedPreferences.getData("drs_number"));
        drsCancelRequest.setReferencenumber("");
        drsCancelRequest.setTaskId(mCriticalogSharedPreferences.getData("task_id"));
        drsCancelRequest.setUserId(userId);
        mPresenterNDC = new DRSPresenterImpl(ChequeDetails.this, new GetDrsInteractorImpl(), new GetDrsInteractorImpl());
        mPresenterNDC.drsCancel(token, drsCancelRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mChequeBankName:
                sampleSearchModelArrayList.clear();
                BankListRequest bankListRequest = new BankListRequest();
                bankListRequest.setAction("bank_list");
                bankListRequest.setUserId(userId);
                mPresenter.onBankListRequest(bankListRequest);
                break;

            case R.id.mDate:

                if (mCheckAmount.getText().toString().equalsIgnoreCase("")) {
                    Toasty.error(this, "Amount Cannot be Empty!", Toast.LENGTH_SHORT, true).show();
                } else {
                    enteredAmount = Integer.parseInt(mCheckAmount.getText().toString());
                    enteredAmount = +enteredAmount;
                    if (enteredAmount > Double.parseDouble(paymentAmount)) {
                        //  Toast.makeText(this, "Amount Exceeded", Toast.LENGTH_SHORT).show();
                        Toasty.error(this, "Amount Exceeded", Toast.LENGTH_SHORT, true).show();
                        mDate.setText("");
                        mDate.setHint("Select Date");
                    } else {
                        datePicker("", 1);
                    }
                }

                break;

            case R.id.mChequeImage:
                if (indexFlag == Integer.parseInt(maxCheque)) {
                    Toasty.error(this, "Maximum Cheques Collected!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    openCameraIntent();
                }
                break;
            case R.id.mForwardArraow:

                if (indexFlag + 1 == Integer.parseInt(maxCheque)) {
                    Toasty.error(this, "Maximum Cheques Collected!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    if (indexFlag == chequeList.size() - 1) {
                        //Toast.makeText(this, "Move Forward", Toast.LENGTH_SHORT).show();
                        mChequeBankName.setText("");
                        mChekNumber.setText("");
                        mDate.setText("");
                        mEnterBankName.setText("");
                        mCheckAmount.setText("");
                        mCheckAmount.setHint("Cheque Amount");
                        mChequeBankName.setHint("Cheque Bank Name");
                        mChekNumber.setHint("Cheque Number");
                        mDate.setHint("Cheque Date");
                        mEnterBankName.setHint("Enter Bank Name");
                        mEnterBankName.setVisibility(View.GONE);
                        mChequeImage.setImageResource(R.drawable.image_capture);
                        base64ChequeImage = "";
                        enteredBankName = "";
                        indexFlag++;
                        mBackwardArrow.setVisibility(View.VISIBLE);
                        mForwardArraow.setVisibility(View.VISIBLE);
                    } else {
                        //  Toast.makeText(this, "Index " + String.valueOf(indexFlag), Toast.LENGTH_SHORT).show();
                        if (chequeList.size() > 0) {

                            if (indexFlag < chequeList.size()) {
                                indexFlag++;
                                forwardNavigate(indexFlag);
                            } else {
                                Toast.makeText(this, "No Next Elements", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }
                break;


            case R.id.mUpdateAlreadyExistData:
                //  Toast.makeText(this, "Index " + String.valueOf(indexFlag), Toast.LENGTH_SHORT).show();
                checkAndStoreDetails();
                break;
            case R.id.mBackwardArrow:
                if (indexFlag == 0) {
                    Toast.makeText(this, "No Backward Items", Toast.LENGTH_SHORT).show();
                } else {
                    if (indexFlag <= chequeList.size()) {
                        indexFlag--;
                        reverseNavigate(indexFlag);
                    }
                }

                break;
            case R.id.mScanNextCheque:
                int totalAmt1 = 0;
                if (chequeList.size() > 0) {
                    for (int i = 0; i < chequeList.size(); i++) {
                        totalAmt1 = totalAmt1 + Integer.parseInt(chequeList.get(i).getChequeAmount());
                    }
                    if (totalAmt1 >= Double.parseDouble(paymentAmount)) {
                        Toasty.error(this, "All Cheque collected: " + String.valueOf(totalAmt1), Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (chequeList.size() > 0) {
                            mForwardArraow.setVisibility(View.VISIBLE);
                            mBackwardArrow.setVisibility(View.VISIBLE);
                            mUpdateAlreadyExistData.setVisibility(View.VISIBLE);
                        }

                        if (chequeList.size() > 0) {
                            bankName = mChequeBankName.getText().toString();
                            chequeAmt = mCheckAmount.getText().toString();
                            checkNumber = mChekNumber.getText().toString();
                            date = mDate.getText().toString();
                            if (bankName.equalsIgnoreCase("")) {
                                Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                            } else if (chequeAmt.equalsIgnoreCase("")) {
                                Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                            } else if (checkNumber.equalsIgnoreCase("")) {
                                Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                            } else if (checkNumber.length() != 6) {
                                Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                            } else if (date.equalsIgnoreCase("")) {
                                Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                            } else if (base64ChequeImage.equalsIgnoreCase("")) {
                                Toast.makeText(this, "Capture Image", Toast.LENGTH_SHORT).show();
                            } else {
                                if (bankName.equalsIgnoreCase("OTHER BANK")) {
                                    enteredBankName = mEnterBankName.getText().toString();
                                    if (enteredBankName.equalsIgnoreCase("")) {
                                        Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mChequeBankName.setText("");
                                        mChekNumber.setText("");
                                        mDate.setText("");
                                        mEnterBankName.setText("");
                                        mCheckAmount.setText("");
                                        mCheckAmount.setHint("Cheque Amount");
                                        mChequeBankName.setHint("Cheque Bank Name");
                                        mChekNumber.setHint("Cheque Number");
                                        mDate.setHint("Cheque Date");
                                        mEnterBankName.setHint("Enter Bank Name");
                                        mEnterBankName.setVisibility(View.GONE);
                                        mChequeImage.setImageResource(R.drawable.image_capture);
                                        base64ChequeImage = "";
                                        enteredBankName = "";
                                    }
                                } else {
                                    mChequeBankName.setText("");
                                    mChekNumber.setText("");
                                    mDate.setText("");
                                    mCheckAmount.setText("");
                                    mCheckAmount.setHint("Cheque Amount");
                                    mChequeBankName.setHint("Cheque Bank Name");
                                    mChekNumber.setHint("Cheque Number");
                                    mDate.setHint("Cheque Date");
                                    mEnterBankName.setHint("Enter Bank Name");
                                    mEnterBankName.setVisibility(View.GONE);
                                    mChequeImage.setImageResource(R.drawable.image_capture);
                                    base64ChequeImage = "";
                                    enteredBankName = "";
                                }
                            }
                        }
                    }
                }
                break;

            case R.id.mCheckCollected:
                int totalAmt = 0;
                chequeList = chequeList;
                if (chequeList.size() > 0) {
                    //  Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < chequeList.size(); i++) {
                        totalAmt = totalAmt + Integer.parseInt(chequeList.get(i).getChequeAmount());
                    }
                    if (totalAmt != Double.parseDouble(paymentAmount)) {
                        Toasty.error(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT, true).show();
                        // Toast.makeText(this, "Cheque amount not matching with collected amount: "+String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                    } else {
                        ChequeCollectedRequest chequeCollectedRequest = new ChequeCollectedRequest();
                        chequeCollectedRequest.setAction("cheque_collection_received");
                        chequeCollectedRequest.setDocketNo(task_id);
                        chequeCollectedRequest.setUserId(userId);
                        chequeCollectedRequest.setCheques(chequeList);
                        chequeCollectedRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                        chequeCollectedRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                        mPresenter.newChequeCollectedRequest(chequeCollectedRequest);
                    }
                } else {

                    bankName = mChequeBankName.getText().toString();
                    chequeAmt = mCheckAmount.getText().toString();
                    checkNumber = mChekNumber.getText().toString();
                    chequeAmount = mCheckAmount.getText().toString();
                    date = mDate.getText().toString();
                    if (bankName.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                    } else if (chequeAmt.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.length() != 6) {
                        Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (chequeAmount.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                    } else if (date.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                    } else if (base64ChequeImage.equalsIgnoreCase("")) {
                        Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                            enteredBankName = mEnterBankName.getText().toString();
                            if (enteredBankName.equalsIgnoreCase("")) {
                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                            } else {

                                for (int i = 0; i < chequeList.size(); i++) {
                                    totalAmt = totalAmt + Integer.parseInt(chequeList.get(i).getChequeAmount());
                                }
                                if (totalAmt != Double.parseDouble(paymentAmount)) {
                                    Toast.makeText(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                                } else {
                                    ChequeCollectedRequest chequeCollectedRequest = new ChequeCollectedRequest();
                                    chequeCollectedRequest.setAction("cheque_collection_received");
                                    chequeCollectedRequest.setDocketNo(task_id);
                                    chequeCollectedRequest.setUserId(userId);
                                    chequeCollectedRequest.setCheques(chequeList);
                                    chequeCollectedRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                                    chequeCollectedRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                                    mPresenter.newChequeCollectedRequest(chequeCollectedRequest);
                                }

                            }
                        } else {

                            for (int i = 0; i < chequeList.size(); i++) {
                                totalAmt = totalAmt + Integer.parseInt(chequeList.get(i).getChequeAmount());
                            }
                            if (totalAmt != Double.parseDouble(paymentAmount)) {
                                Toasty.error(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT, true).show();
                                //   Toast.makeText(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                            } else {
                                ChequeCollectedRequest chequeCollectedRequest = new ChequeCollectedRequest();
                                chequeCollectedRequest.setAction("cheque_collection_received");
                                chequeCollectedRequest.setDocketNo(task_id);
                                chequeCollectedRequest.setUserId(userId);
                                chequeCollectedRequest.setCheques(chequeList);
                                chequeCollectedRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                                chequeCollectedRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                                mPresenter.newChequeCollectedRequest(chequeCollectedRequest);
                            }
                        }
                    }
                }
                break;

            case R.id.mChequeNotCollected:
                connoteNotCollectedNDC();
                break;

            case R.id.mTvBackbutton:
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Closing Cheque Collection")
                        .setMessage("Are you sure you want to close this Process!!....Data will be cleared")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Cheque Collection")
                .setMessage("Are you sure you want to close this Process!!....Data will be cleared")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    public void forwardNavigate(int position) {

        mChequeBankName.setText(chequeList.get(position).getBankCode());
        mChekNumber.setText(chequeList.get(position).getChequeNumber());
        mCheckAmount.setText(chequeList.get(position).getChequeAmount());
        if (!chequeList.get(position).getOther_bank().equalsIgnoreCase("")) {
            mEnterBankName.setVisibility(View.VISIBLE);
            mEnterBankName.setText(chequeList.get(position).getOther_bank());
            mChequeBankName.setText("OTHER BANK");
        } else {
            mEnterBankName.setVisibility(View.GONE);
        }
        mDate.setText(chequeList.get(position).getChequeDate());

        byte[] decodedString = Base64.decode(chequeList.get(position).getCodImage(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        mChequeImage.setImageBitmap(decodedByte);
        mUpdateAlreadyExistData.setVisibility(View.VISIBLE);

        if (indexFlag == 0) {
            mForwardArraow.setVisibility(View.VISIBLE);
            mBackwardArrow.setVisibility(View.INVISIBLE);
        } else if (indexFlag == chequeList.size() - 1) {
            mBackwardArrow.setVisibility(View.VISIBLE);
            mForwardArraow.setVisibility(View.VISIBLE);
        } else {
            mForwardArraow.setVisibility(View.VISIBLE);
            mBackwardArrow.setVisibility(View.VISIBLE);
        }
    }

    public void reverseNavigate(int position) {

        mChequeBankName.setText(chequeList.get(position).getBankCode());
        mChekNumber.setText(chequeList.get(position).getChequeNumber());
        mCheckAmount.setText(chequeList.get(position).getChequeAmount());
        if (!chequeList.get(position).getOther_bank().equalsIgnoreCase("")) {
            mEnterBankName.setVisibility(View.VISIBLE);
            mEnterBankName.setText(chequeList.get(position).getOther_bank());
            mChequeBankName.setText("OTHER BANK");
        } else {
            mEnterBankName.setVisibility(View.GONE);
        }
        mDate.setText(chequeList.get(position).getChequeDate());

        byte[] decodedString = Base64.decode(chequeList.get(position).getCodImage(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        mChequeImage.setImageBitmap(decodedByte);
     /*   mForwardArraow.setVisibility(View.VISIBLE);
        mBackwardArrow.setVisibility(View.VISIBLE);*/
        mUpdateAlreadyExistData.setVisibility(View.VISIBLE);

        if (indexFlag == 0) {
            mForwardArraow.setVisibility(View.VISIBLE);
            mBackwardArrow.setVisibility(View.INVISIBLE);
        } else if (indexFlag == chequeList.size() - 1) {
            mBackwardArrow.setVisibility(View.VISIBLE);
            mForwardArraow.setVisibility(View.VISIBLE);
        } else {
            mForwardArraow.setVisibility(View.VISIBLE);
            mBackwardArrow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void setDataToViews(DRSListResponse drsListResponse) {

    }

    @Override
    public void drsCancelCallSuceess(DrsCancelResponse drsCancelResponse) {
        if (drsCancelResponse.getStatus() == 200) {
            Toasty.warning(ChequeDetails.this, drsCancelResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        } else {
            Toasty.warning(ChequeDetails.this, drsCancelResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void setBankListDataToViews(BankListResponse bankListResponse) {

        if (bankListResponse.getStatus() == 200) {
            Toast.makeText(this, bankListResponse.getMessage(), Toast.LENGTH_SHORT).show();

            List<DataBank> dataBankList = bankListResponse.getData();
            for (int i = 0; i < bankListResponse.getData().size(); i++) {
                sampleSearchModelArrayList.add(new SampleSearchModel(bankListResponse.getData().get(i).getBankName()));
            }
            showOriginDestination(sampleSearchModelArrayList, "Search Bank", dataBankList);
        } else {
            Toast.makeText(this, bankListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void paymentLinkResponse(PaymentLinkResponse paymentLinkResponse) {

    }

    @Override
    public void qrCodeLinkResponse(QRCodeLinkResponse qrCodeLinkResponse) {

    }

    @Override
    public void codProofResponse(UploadCodProofResponse uploadCodProofResponse) {

    }

    @Override
    public void refershUpiResponse(RefreshUPIResponse refreshUPIResponse) {

    }

    @Override
    public void codChequeReceivedResponse(CodChequeReceivedResponse codChequeReceivedResponse) {
       /* if (codChequeReceivedResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeReceivedResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            startActivity(new Intent(this, DRSCloseActivity.class));
            finish();
        } else {
            Toasty.warning(this, codChequeReceivedResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }*/
    }

    @Override
    public void codChequeCollectionResponse(CodChequeCollectionResponse codChequeCollectionResponse) {
        if (codChequeCollectionResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            //  startActivity(new Intent(this, DRSCloseActivity.class));

            Intent a = new Intent(this, DRSCloseActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            finish();
        } else {
            Toasty.error(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void newChequeCollectedResponse(CodChequeCollectionResponse codChequeCollectionResponse) {
        if (codChequeCollectionResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            //  startActivity(new Intent(this, DRSCloseActivity.class));

            Intent a = new Intent(this, DRSCloseActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            finish();
        } else {
            Toasty.error(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}

/*
package com.criticalog.ecritica.NewCODProcess;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSListContract;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPDRS.DRSPresenterImpl;
import com.criticalog.ecritica.MVPDRS.GetDrsInteractorImpl;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.MVPPickup.PickupCancel.DatumReasons;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.Cheque;
import com.criticalog.ecritica.NewCODProcess.Model.ChequeCollectedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.DataBank;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PhotoData;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChequeDetails extends AppCompatActivity implements View.OnClickListener, NewCodProcessContract.MainView, DRSListContract.MainView {
    private TextView mTopBarText, mCheckCollected, mChequeNotCollected;
    private EditText mChequeBankName, mCheckAmount, mChekNumber, mDate;
    private int year, month, day;
    private static final int CAMERA_INTENT = 111;
    private static final int GALLERY_INTENT = 222;
    private PhotoData photoData;
    private ImageView mChequeImage, mTvBackbutton;
    File photoFile = null;
    private static int CAMERA_REQUEST = 1888;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private NewCodProcessContract.presenter mPresenter;
    DRSListContract.presenter mPresenterNDC;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token, base64ChequeImage = "", Drs_no, task_id, paymentAmount, maxCheque;
    SimpleArcDialog mProgressDialog;
    ArrayList<SampleSearchModel> sampleSearchModelArrayList = new ArrayList<>();
    private List<DatumReasons> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    int mHour;
    int mMinute;
    private double latitude = 0, longitude = 0;
    private RestServices mRestServices;
    private String reasonCode = "", refno, bulkFlag;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private String bankcode = "", bankName = "";
    private LocationCallback locationCallback;
    private ImageView mChequeRefImageOnline;
    private EditText mEnterBankName;
    private String enteredBankName = "";
    private TextView mChequeCollecteNumber, mScanNextCheque, mChequeTotalAmount, mUpdateAlreadyExistData;
    private List<Cheque> chequeList = new ArrayList<>();
    String chequeAmt;
    String checkNumber;
    String date;
    String chequeAmount;
    String otherBankName;
    ImageView mBackwardArrow, mForwardArraow;
    private int indexFlag = 0;
    private static int enteredAmount = 0;

    private ArrayList<String> chequeNumListString = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_details);
        mTopBarText = findViewById(R.id.mTopBarText);
        mChequeBankName = findViewById(R.id.mChequeBankName);
        mCheckAmount = findViewById(R.id.mCheckAmount);
        mChekNumber = findViewById(R.id.mChekNumber);
        mDate = findViewById(R.id.mDate);
        mChequeImage = findViewById(R.id.mChequeImage);
        mCheckCollected = findViewById(R.id.mCheckCollected);
        mChequeNotCollected = findViewById(R.id.mChequeNotCollected);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mChequeRefImageOnline = findViewById(R.id.mChequeRefImageOnline);
        mEnterBankName = findViewById(R.id.mEnterBankName);
        mChequeCollecteNumber = findViewById(R.id.mChequeCollecteNumber);
        mScanNextCheque = findViewById(R.id.mScanNextCheque);
        mChequeTotalAmount = findViewById(R.id.mChequeTotalAmount);
        mForwardArraow = findViewById(R.id.mForwardArraow);
        mBackwardArrow = findViewById(R.id.mBackwardArrow);
        mUpdateAlreadyExistData = findViewById(R.id.mUpdateAlreadyExistData);

        if (checkPermission()) {
            //main logic or main code
            // . write your main code to execute, It will execute if the permission is already given.
        } else {
            requestPermission();
        }
        mProgressDialog = new SimpleArcDialog(this);
        mProgressDialog.setTitle("Loading......");
        mProgressDialog.setCancelable(false);
        mChequeBankName.setFocusable(false);
        mChequeBankName.setClickable(true);

        mDate.setFocusable(false);
        mDate.setClickable(true);
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds


        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);

        mPresenter = new NewCodPresenterImpl(ChequeDetails.this, new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl());
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        Drs_no = mCriticalogSharedPreferences.getData("drs_number");
        task_id = mCriticalogSharedPreferences.getData("task_id");
        paymentAmount = mCriticalogSharedPreferences.getData("payment_amount");
        maxCheque = mCriticalogSharedPreferences.getData("max_cheque_num");

        mChequeTotalAmount.setText("Total Cheque Amount: " + paymentAmount);

        // mCheckAmount.setEnabled(false);
        //  mCheckAmount.setText(paymentAmount);
        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        //askForPermissions();
        photoData = new PhotoData(this);

        mTopBarText.setText("Cheque Details");

        mChequeBankName.setOnClickListener(this);
        mDate.setOnClickListener(this);
        mChequeImage.setOnClickListener(this);
        mCheckCollected.setOnClickListener(this);
        mChequeNotCollected.setOnClickListener(this);
        mTvBackbutton.setOnClickListener(this);
        mScanNextCheque.setOnClickListener(this);
        mBackwardArrow.setOnClickListener(this);
        mForwardArraow.setOnClickListener(this);
        mUpdateAlreadyExistData.setOnClickListener(this);

        Picasso.with(this)
                .load("https://www.ecritica.co/efreightlive/media/app/chequeImg.png")
                .resize(2200, 400)
                .placeholder(R.drawable.brokenimage)
                .into(mChequeRefImageOnline);
    }


    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }

    }

    private boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere, List<DataBank> dataBankList) {

        new SimpleSearchDialogCompat(ChequeDetails.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source
                        if (item.getTitle().equalsIgnoreCase("OTHER BANK")) {
                            mEnterBankName.setVisibility(View.VISIBLE);
                        } else {
                            mEnterBankName.setVisibility(View.GONE);
                            mEnterBankName.setText("");
                        }

                        bankName = item.getTitle();
                        mChequeBankName.setText(item.getTitle());

                        bankcode = dataBankList.get(position).getBankCode();

                        dialog.dismiss();
                    }
                }).show();
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void takePhoto() {
        File photo = null;
        try {
            photo = photoData.createImageFile();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            photoData.saveUriPath(Uri.fromFile(photo).getPath());
            startActivityForResult(intent, CAMERA_INTENT);
        } catch (IOException e) {
            Log.e("EXC", e.toString());
            //TODO warn the user the photo fail
        }
    }

    public void askForPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivity(intent);
                return;
            }
            //createDir();
        }
    }

    private void datePicker(String task_id, int which) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        //  mCriticalogSharedPreferences.saveData("R_DATE", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        Log.e("DATE_FORMAT", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        mDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        //tiemPicker((year + "-" + (monthOfYear + 1) + "-" + dayOfMonth), task_id, which);
                    }
                }, year, month, day);
        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the intent to camera or gallery worked then the code is

        if (requestCode == 1888) {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                if (chequeList.contains(mChekNumber.getText().toString())) {
                    Toasty.error(this, "Cheque Number Already Exist", Toast.LENGTH_SHORT, true).show();
                } else {

                }

                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64ChequeImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                    if (chequeList.size() > 0) {
                        mChequeCollecteNumber.setText("Cheque Collected: " + String.valueOf(chequeList.size()));
                        mScanNextCheque.setVisibility(View.VISIBLE);
                    }
                    if (chequeList.size() == Integer.parseInt(maxCheque)) {
                        mScanNextCheque.setVisibility(View.GONE);
                    }


                    bankName = mChequeBankName.getText().toString();
                    chequeAmt = mCheckAmount.getText().toString();
                    checkNumber = mChekNumber.getText().toString();
                    date = mDate.getText().toString();
                    if (bankName.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                    } else if (chequeAmt.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.length() != 6) {
                        Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (date.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                    } else if (base64ChequeImage.equalsIgnoreCase("")) {
                        Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                    } else {
                        CodChequeCollectionRequest codChequeCollectionRequest1 = new CodChequeCollectionRequest();
                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                            enteredBankName = mEnterBankName.getText().toString();

                            if (enteredBankName.equalsIgnoreCase("")) {
                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                            } else {
                                mChequeImage.setImageBitmap(myBitmap);

                                Cheque cheque = new Cheque();
                                cheque.setChequeDate(mDate.getText().toString());
                                cheque.setChequeAmount(mCheckAmount.getText().toString());
                                cheque.setBankCode(bankcode);
                                cheque.setCodImage(base64ChequeImage);
                                cheque.setOther_bank(enteredBankName);
                                cheque.setChequeNumber(mChekNumber.getText().toString());


                              */
/*  if(chequeList.contains(mChekNumber.getText().toString()))
                                {
                                    Toasty.error(this, "Cheque Number Already Exist", Toast.LENGTH_SHORT, true).show();
                                }else {
                                    if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                                        // Toast.makeText(this, "Already Cheque Number Exist......only updating image", Toast.LENGTH_SHORT).show();
                                        cheque.setCodImage(base64ChequeImage);
                                        chequeList.set(indexFlag, cheque);
                                    } else {
                                        chequeNumListString.add(mChekNumber.getText().toString());
                                        chequeList.add(cheque);
                                        indexFlag = chequeList.size() - 1;
                                    }
                                }*//*



                                if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                                    // Toast.makeText(this, "Already Cheque Number Exist......only updating image", Toast.LENGTH_SHORT).show();
                                    cheque.setCodImage(base64ChequeImage);
                                    chequeList.set(indexFlag, cheque);
                                } else {
                                    chequeNumListString.add(mChekNumber.getText().toString());
                                    chequeList.add(cheque);
                                    indexFlag = chequeList.size() - 1;
                                }

                            }
                        } else {
                            mChequeImage.setImageBitmap(myBitmap);

                            Cheque cheque = new Cheque();
                            cheque.setChequeDate(mDate.getText().toString());
                            cheque.setChequeAmount(mCheckAmount.getText().toString());
                            cheque.setBankCode(bankcode);
                            cheque.setCodImage(base64ChequeImage);
                            cheque.setOther_bank(enteredBankName);
                            cheque.setChequeNumber(mChekNumber.getText().toString());


                          */
/*  if(chequeList.contains(mChekNumber.getText().toString()))
                            {
                                Toasty.error(this, "Cheque Number Already Exist", Toast.LENGTH_SHORT, true).show();
                            }else {
                                if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                                    // Toast.makeText(this, "Already Cheque Number Exist......only updating image", Toast.LENGTH_SHORT).show();
                                    cheque.setCodImage(base64ChequeImage);
                                    chequeList.set(indexFlag, cheque);
                                } else {
                                    chequeNumListString.add(mChekNumber.getText().toString());
                                    chequeList.add(cheque);
                                    indexFlag = chequeList.size() - 1;
                                }
                            }*//*


                            if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                                //  Toast.makeText(this, "Index  " + String.valueOf(indexFlag), Toast.LENGTH_SHORT).show();
                                cheque.setCodImage(base64ChequeImage);
                                chequeList.set(indexFlag, cheque);

                                Toasty.error(this, "Cheque Number Already Exist...data replaced", Toast.LENGTH_SHORT, true).show();
                            } else {
                                chequeNumListString.add(mChekNumber.getText().toString());
                                chequeList.add(cheque);
                                indexFlag = chequeList.size() - 1;
                            }

                        }
                    }
                    if (chequeList.size() > 0) {
                        mChequeCollecteNumber.setText("Cheque Collected: " + String.valueOf(chequeList.size()));
                        mScanNextCheque.setVisibility(View.VISIBLE);

                        //  mForwardArraow.setVisibility(View.VISIBLE);
                        //    mBackwardArrow.setVisibility(View.VISIBLE);
                        mUpdateAlreadyExistData.setVisibility(View.VISIBLE);

                    }
                    if (chequeList.size() == Integer.parseInt(maxCheque)) {
                        mScanNextCheque.setVisibility(View.GONE);
                    }

                }
                //mCriticalogSharedPreferences.saveData("image_captured", "true");
            }
        }
    }

    public void checkAndStoreDetails() {
        bankName = mChequeBankName.getText().toString();
        chequeAmt = mCheckAmount.getText().toString();
        checkNumber = mChekNumber.getText().toString();
        date = mDate.getText().toString();
        if (bankName.equalsIgnoreCase("other")) {
            otherBankName = mEnterBankName.getText().toString();
        } else {
            otherBankName = "";
        }
        if (bankName.equalsIgnoreCase("")) {
            Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
        } else if (chequeAmt.equalsIgnoreCase("")) {
            Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
        } else if (checkNumber.equalsIgnoreCase("")) {
            Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
        } else if (checkNumber.length() != 6) {
            Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
        } else if (date.equalsIgnoreCase("")) {
            Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
        } else {
            Cheque cheque = new Cheque();
            cheque.setChequeDate(date);
            cheque.setChequeAmount(chequeAmt);
            if (!otherBankName.equalsIgnoreCase("")) {
                cheque.setBankCode("Other");
            } else {
                cheque.setBankCode(bankcode);
            }
            cheque.setCodImage(chequeList.get(indexFlag).getCodImage());
            cheque.setOther_bank(otherBankName);
            cheque.setChequeNumber(checkNumber);
            if (!otherBankName.equalsIgnoreCase("")) {
                cheque.setOther_bank(mEnterBankName.getText().toString());
            } else {
                cheque.setOther_bank("");
            }
            chequeList.set(indexFlag, cheque);
            Toasty.success(this, "Data Updated Successfully", Toast.LENGTH_SHORT, true).show();
           */
/* if (chequeNumListString.contains(mChekNumber.getText().toString())) {
                Toast.makeText(this, "Index  " + String.valueOf(indexFlag), Toast.LENGTH_SHORT).show();
                chequeList.set(indexFlag, cheque);
            }*//*

        }

    }

    public void connoteNotCollectedNDC() {
        PickupReasonsRequest pickupReasonsRequest = new PickupReasonsRequest();
        pickupReasonsRequest.setUserId(userId);
        pickupReasonsRequest.setProductStatus("delivery");
        pickupReasonsRequest.setAction("reasonscancel");
        pickupReasonsRequest.setLatitude(String.valueOf(latitude));
        pickupReasonsRequest.setLongitude(String.valueOf(longitude));
        mProgressDialog.show();
        reasons.clear();
        reasonList.clear();
        Call<PickupReasonsResponse> pickupReasonsResponseCall = mRestServices.pickupCancelReasons(pickupReasonsRequest);

        pickupReasonsResponseCall.enqueue(new Callback<PickupReasonsResponse>() {
            @Override
            public void onResponse(Call<PickupReasonsResponse> call, Response<PickupReasonsResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        ;
                        reasons = response.body().getData();

                        for (int i = 0; i < reasons.size(); i++) {

                            reasonList.add(reasons.get(i).getReasondesc());
                        }
                        showCancelDialog();
                    } else {
                        mProgressDialog.dismiss();
                    }
                } else {
                    mProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PickupReasonsResponse> call, Throwable t) {

            }
        });
    }

    private void showCancelDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(ChequeDetails.this)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        reasonCode = reasons.get(which).getReasoncode();
                        Toast.makeText(ChequeDetails.this, reasonCode, Toast.LENGTH_SHORT).show();
                        if (reasonCode.equalsIgnoreCase("CI")) {
                            //  datePicker(taskNo, which);
                        } else if (reasonCode.equalsIgnoreCase("LD")) {
                            // datePicker(taskNo, which);
                        } else {
                            mCriticalogSharedPreferences.saveData("R_DATE", "");
                            mCriticalogSharedPreferences.saveData("R_TIME", "");
                            if (which == 0 || which == 3) {
                                //  getReferenceNo(which, taskNo);
                                callDRSCancel();
                            } else {
                                callDRSCancel();

                            }
                        }
                        return true; // allow selection
                    }
                })
                .show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ChequeDetails.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ChequeDetails.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ChequeDetails.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(ChequeDetails.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void callDRSCancel() {
        getLocation();
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c);

        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        String time = df1.format(c);
        if (mCriticalogSharedPreferences.getData("bulk_flag").equalsIgnoreCase("true")) {
            bulkFlag = "1";
        } else {
            bulkFlag = "0";
        }
        DrsCancelRequest drsCancelRequest = new DrsCancelRequest();
        drsCancelRequest.setAction("drs_cancel");
        drsCancelRequest.setBulkFlag(bulkFlag);
        drsCancelRequest.setDate(date);
        drsCancelRequest.setTime(time);
        drsCancelRequest.setrDate(mCriticalogSharedPreferences.getData("R_DATE"));
        drsCancelRequest.setrTime(mCriticalogSharedPreferences.getData("R_TIME"));
        drsCancelRequest.setLatitude(String.valueOf(latitude));
        drsCancelRequest.setLongitude(String.valueOf(longitude));
        drsCancelRequest.setReasons(reasonCode);
        drsCancelRequest.setRefcount(mCriticalogSharedPreferences.getData("drs_number"));
        drsCancelRequest.setReferencenumber("");
        drsCancelRequest.setTaskId(mCriticalogSharedPreferences.getData("task_id"));
        drsCancelRequest.setUserId(userId);
        mPresenterNDC = new DRSPresenterImpl(ChequeDetails.this, new GetDrsInteractorImpl(), new GetDrsInteractorImpl());
        mPresenterNDC.drsCancel(token, drsCancelRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mChequeBankName:
                sampleSearchModelArrayList.clear();
                BankListRequest bankListRequest = new BankListRequest();
                bankListRequest.setAction("bank_list");
                bankListRequest.setUserId(userId);
                mPresenter.onBankListRequest(bankListRequest);
                break;

            case R.id.mDate:

                if (mCheckAmount.getText().toString().equalsIgnoreCase("")) {
                    Toasty.error(this, "Amount Cannot be Empty!", Toast.LENGTH_SHORT, true).show();
                } else {
                    enteredAmount = Integer.parseInt(mCheckAmount.getText().toString());
                    enteredAmount = +enteredAmount;
                    if (enteredAmount > Double.parseDouble(paymentAmount)) {
                        //  Toast.makeText(this, "Amount Exceeded", Toast.LENGTH_SHORT).show();
                        Toasty.error(this, "Amount Exceeded", Toast.LENGTH_SHORT, true).show();
                        mDate.setText("");
                        mDate.setHint("Select Date");
                    } else {
                        datePicker("", 1);
                    }
                }

                break;

            case R.id.mChequeImage:
                if (chequeList.size() == Integer.parseInt(maxCheque)) {
                    Toasty.error(this, "Maximum Cheques Collected!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    openCameraIntent();
                }
                break;
            case R.id.mForwardArraow:

                if (chequeList.size() > 0) {

                    if (indexFlag > chequeList.size()) {
                        indexFlag--;
                    } else {
                        indexFlag++;
                    }

                    if (indexFlag == chequeList.size()) {
                        indexFlag--;
                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                            enteredBankName = mEnterBankName.getText().toString();
                            if (enteredBankName.equalsIgnoreCase("")) {
                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                            } else {
                                mChequeBankName.setText("");
                                mChekNumber.setText("");
                                mDate.setText("");
                                mEnterBankName.setText("");
                                mCheckAmount.setText("");
                                mCheckAmount.setHint("Cheque Amount");
                                mChequeBankName.setHint("Cheque Bank Name");
                                mChekNumber.setHint("Cheque Number");
                                mDate.setHint("Cheque Date");
                                mEnterBankName.setHint("Enter Bank Name");
                                mEnterBankName.setVisibility(View.GONE);
                                mChequeImage.setImageResource(R.drawable.image_capture);
                                base64ChequeImage = "";
                                enteredBankName = "";
                            }
                        } else {
                            mChequeBankName.setText("");
                            mChekNumber.setText("");
                            mDate.setText("");
                            mCheckAmount.setText("");
                            mCheckAmount.setHint("Cheque Amount");
                            mChequeBankName.setHint("Cheque Bank Name");
                            mChekNumber.setHint("Cheque Number");
                            mDate.setHint("Cheque Date");
                            mEnterBankName.setHint("Enter Bank Name");
                            mEnterBankName.setVisibility(View.GONE);
                            mChequeImage.setImageResource(R.drawable.image_capture);
                            base64ChequeImage = "";
                            enteredBankName = "";
                        }
                    } else {
                        mChequeBankName.setText(chequeList.get(indexFlag).getBankCode());
                        mChekNumber.setText(chequeList.get(indexFlag).getChequeNumber());
                        mCheckAmount.setText(chequeList.get(indexFlag).getChequeAmount());
                        if (!chequeList.get(indexFlag).getOther_bank().equalsIgnoreCase("")) {
                            mEnterBankName.setVisibility(View.VISIBLE);
                            mEnterBankName.setText(chequeList.get(indexFlag).getOther_bank());
                            mChequeBankName.setText("Other");
                        } else {
                            mEnterBankName.setVisibility(View.GONE);
                        }
                        mDate.setText(chequeList.get(indexFlag).getChequeDate());

                        byte[] decodedString = Base64.decode(chequeList.get(indexFlag).getCodImage(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        mChequeImage.setImageBitmap(decodedByte);
                        mForwardArraow.setVisibility(View.VISIBLE);
                        mBackwardArrow.setVisibility(View.VISIBLE);
                        mUpdateAlreadyExistData.setVisibility(View.VISIBLE);
                    }
                }
                break;

            case R.id.mUpdateAlreadyExistData:
                checkAndStoreDetails();
                break;
            case R.id.mBackwardArrow:

                if (chequeList.size() > 0) {
                    mChequeBankName.setText(chequeList.get(indexFlag).getBankCode());
                    mChekNumber.setText(chequeList.get(indexFlag).getChequeNumber());
                    mCheckAmount.setText(chequeList.get(indexFlag).getChequeAmount());
                    if (!chequeList.get(indexFlag).getOther_bank().equalsIgnoreCase("")) {
                        mEnterBankName.setVisibility(View.VISIBLE);
                        mEnterBankName.setText(chequeList.get(indexFlag).getOther_bank());
                        mChequeBankName.setText("Other");
                    } else {
                        mEnterBankName.setVisibility(View.GONE);
                    }
                    mDate.setText(chequeList.get(indexFlag).getChequeDate());

                    byte[] decodedString = Base64.decode(chequeList.get(indexFlag).getCodImage(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    mChequeImage.setImageBitmap(decodedByte);
                    mForwardArraow.setVisibility(View.VISIBLE);
                    mBackwardArrow.setVisibility(View.VISIBLE);
                    mUpdateAlreadyExistData.setVisibility(View.VISIBLE);
                }
                if (indexFlag >= 1) {
                    indexFlag--;
                }
                break;
            case R.id.mScanNextCheque:
                int totalAmt1 = 0;
                //  Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                if (chequeList.size() > 0) {
                    for (int i = 0; i < chequeList.size(); i++) {
                        totalAmt1 = totalAmt1 + Integer.parseInt(chequeList.get(i).getChequeAmount());
                    }
                    if (totalAmt1 >= Double.parseDouble(paymentAmount)) {
                        Toasty.error(this, "All Cheque collected: " + String.valueOf(totalAmt1), Toast.LENGTH_SHORT, true).show();
                        // Toast.makeText(this, "Cheque amount not matching with collected amount: "+String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                    } else {
                        if (chequeList.size() > 0) {
                            mForwardArraow.setVisibility(View.VISIBLE);
                            mBackwardArrow.setVisibility(View.VISIBLE);
                            mUpdateAlreadyExistData.setVisibility(View.VISIBLE);
                        }

                        if (chequeList.size() > 0) {
                            bankName = mChequeBankName.getText().toString();
                            chequeAmt = mCheckAmount.getText().toString();
                            checkNumber = mChekNumber.getText().toString();
                            date = mDate.getText().toString();
                            if (bankName.equalsIgnoreCase("")) {
                                Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                            } else if (chequeAmt.equalsIgnoreCase("")) {
                                Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                            } else if (checkNumber.equalsIgnoreCase("")) {
                                Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                            } else if (checkNumber.length() != 6) {
                                Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                            } else if (date.equalsIgnoreCase("")) {
                                Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                            } else if (base64ChequeImage.equalsIgnoreCase("")) {

                                if(indexFlag==chequeList.size()-1)
                                {
                                    Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                                }else {
                                    if (chequeList.size() > 0) {
                                        base64ChequeImage = chequeList.get(indexFlag).getCodImage();
                                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                                            enteredBankName = mEnterBankName.getText().toString();
                                            if (enteredBankName.equalsIgnoreCase("")) {
                                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                                            } else {
                                                mChequeBankName.setText("");
                                                mChekNumber.setText("");
                                                mDate.setText("");
                                                mEnterBankName.setText("");
                                                mCheckAmount.setText("");
                                                mCheckAmount.setHint("Cheque Amount");
                                                mChequeBankName.setHint("Cheque Bank Name");
                                                mChekNumber.setHint("Cheque Number");
                                                mDate.setHint("Cheque Date");
                                                mEnterBankName.setHint("Enter Bank Name");
                                                mEnterBankName.setVisibility(View.GONE);
                                                mChequeImage.setImageResource(R.drawable.image_capture);
                                                base64ChequeImage = "";
                                                enteredBankName = "";
                                            }
                                        } else {
                                            mChequeBankName.setText("");
                                            mChekNumber.setText("");
                                            mDate.setText("");
                                            mCheckAmount.setText("");
                                            mCheckAmount.setHint("Cheque Amount");
                                            mChequeBankName.setHint("Cheque Bank Name");
                                            mChekNumber.setHint("Cheque Number");
                                            mDate.setHint("Cheque Date");
                                            mEnterBankName.setHint("Enter Bank Name");
                                            mEnterBankName.setVisibility(View.GONE);
                                            mChequeImage.setImageResource(R.drawable.image_capture);
                                            base64ChequeImage = "";
                                            enteredBankName = "";
                                        }
                                    } else {
                                        Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                                    }
                                }
                            } else {
                                if (bankName.equalsIgnoreCase("OTHER BANK")) {
                                    enteredBankName = mEnterBankName.getText().toString();
                                    if (enteredBankName.equalsIgnoreCase("")) {
                                        Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mChequeBankName.setText("");
                                        mChekNumber.setText("");
                                        mDate.setText("");
                                        mEnterBankName.setText("");
                                        mCheckAmount.setText("");
                                        mCheckAmount.setHint("Cheque Amount");
                                        mChequeBankName.setHint("Cheque Bank Name");
                                        mChekNumber.setHint("Cheque Number");
                                        mDate.setHint("Cheque Date");
                                        mEnterBankName.setHint("Enter Bank Name");
                                        mEnterBankName.setVisibility(View.GONE);
                                        mChequeImage.setImageResource(R.drawable.image_capture);
                                        base64ChequeImage = "";
                                        enteredBankName = "";
                                    }
                                } else {
                                    mChequeBankName.setText("");
                                    mChekNumber.setText("");
                                    mDate.setText("");
                                    mCheckAmount.setText("");
                                    mCheckAmount.setHint("Cheque Amount");
                                    mChequeBankName.setHint("Cheque Bank Name");
                                    mChekNumber.setHint("Cheque Number");
                                    mDate.setHint("Cheque Date");
                                    mEnterBankName.setHint("Enter Bank Name");
                                    mEnterBankName.setVisibility(View.GONE);
                                    mChequeImage.setImageResource(R.drawable.image_capture);
                                    base64ChequeImage = "";
                                    enteredBankName = "";
                                }
                            }
                        }


                        //CodChequeCollectionRequest codChequeCollectionRequest1=new CodChequeCollectionRequest();
                       */
/* if (chequeList.size() > 0) {
                            bankName = mChequeBankName.getText().toString();
                            chequeAmt = mCheckAmount.getText().toString();
                            checkNumber = mChekNumber.getText().toString();
                            date = mDate.getText().toString();
                            if (bankName.equalsIgnoreCase("")) {
                                Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                            } else if (chequeAmt.equalsIgnoreCase("")) {
                                Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                            } else if (checkNumber.equalsIgnoreCase("")) {
                                Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                            } else if (checkNumber.length() != 6) {
                                Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                            } else if (date.equalsIgnoreCase("")) {
                                Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                            } else if (base64ChequeImage.equalsIgnoreCase("")) {

                                if(indexFlag==chequeList.size()-1)
                                {
                                    Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                                }else {

                                    if (chequeList.size() > 0) {
                                        base64ChequeImage = chequeList.get(indexFlag).getCodImage();
                                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                                            enteredBankName = mEnterBankName.getText().toString();
                                            if (enteredBankName.equalsIgnoreCase("")) {
                                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                                            } else {
                                                mChequeBankName.setText("");
                                                mChekNumber.setText("");
                                                mDate.setText("");
                                                mEnterBankName.setText("");
                                                mCheckAmount.setText("");
                                                mCheckAmount.setHint("Cheque Amount");
                                                mChequeBankName.setHint("Cheque Bank Name");
                                                mChekNumber.setHint("Cheque Number");
                                                mDate.setHint("Cheque Date");
                                                mEnterBankName.setHint("Enter Bank Name");
                                                mEnterBankName.setVisibility(View.GONE);
                                                mChequeImage.setImageResource(R.drawable.image_capture);
                                                base64ChequeImage = "";
                                                enteredBankName = "";
                                            }
                                        } else {
                                            mChequeBankName.setText("");
                                            mChekNumber.setText("");
                                            mDate.setText("");
                                            mCheckAmount.setText("");
                                            mCheckAmount.setHint("Cheque Amount");
                                            mChequeBankName.setHint("Cheque Bank Name");
                                            mChekNumber.setHint("Cheque Number");
                                            mDate.setHint("Cheque Date");
                                            mEnterBankName.setHint("Enter Bank Name");
                                            mEnterBankName.setVisibility(View.GONE);
                                            mChequeImage.setImageResource(R.drawable.image_capture);
                                            base64ChequeImage = "";
                                            enteredBankName = "";
                                        }
                                    } else {
                                        Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                                    }
                                }

                             *//*
 */
/*   if (chequeList.size() > 0) {
                                    base64ChequeImage = chequeList.get(indexFlag).getCodImage();
                                    if (bankName.equalsIgnoreCase("OTHER BANK")) {
                                        enteredBankName = mEnterBankName.getText().toString();
                                        if (enteredBankName.equalsIgnoreCase("")) {
                                            Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                                        } else {
                                            mChequeBankName.setText("");
                                            mChekNumber.setText("");
                                            mDate.setText("");
                                            mEnterBankName.setText("");
                                            mCheckAmount.setText("");
                                            mCheckAmount.setHint("Cheque Amount");
                                            mChequeBankName.setHint("Cheque Bank Name");
                                            mChekNumber.setHint("Cheque Number");
                                            mDate.setHint("Cheque Date");
                                            mEnterBankName.setHint("Enter Bank Name");
                                            mEnterBankName.setVisibility(View.GONE);
                                            mChequeImage.setImageResource(R.drawable.image_capture);
                                            base64ChequeImage = "";
                                            enteredBankName = "";
                                        }
                                    } else {
                                        mChequeBankName.setText("");
                                        mChekNumber.setText("");
                                        mDate.setText("");
                                        mCheckAmount.setText("");
                                        mCheckAmount.setHint("Cheque Amount");
                                        mChequeBankName.setHint("Cheque Bank Name");
                                        mChekNumber.setHint("Cheque Number");
                                        mDate.setHint("Cheque Date");
                                        mEnterBankName.setHint("Enter Bank Name");
                                        mEnterBankName.setVisibility(View.GONE);
                                        mChequeImage.setImageResource(R.drawable.image_capture);
                                        base64ChequeImage = "";
                                        enteredBankName = "";
                                    }
                                } else {
                                    Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                                }*//*
 */
/*

                            } else {
                                if (bankName.equalsIgnoreCase("OTHER BANK")) {
                                    enteredBankName = mEnterBankName.getText().toString();
                                    if (enteredBankName.equalsIgnoreCase("")) {
                                        Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mChequeBankName.setText("");
                                        mChekNumber.setText("");
                                        mDate.setText("");
                                        mEnterBankName.setText("");
                                        mCheckAmount.setText("");
                                        mCheckAmount.setHint("Cheque Amount");
                                        mChequeBankName.setHint("Cheque Bank Name");
                                        mChekNumber.setHint("Cheque Number");
                                        mDate.setHint("Cheque Date");
                                        mEnterBankName.setHint("Enter Bank Name");
                                        mEnterBankName.setVisibility(View.GONE);
                                        mChequeImage.setImageResource(R.drawable.image_capture);
                                        base64ChequeImage = "";
                                        enteredBankName = "";
                                    }
                                } else {
                                    mChequeBankName.setText("");
                                    mChekNumber.setText("");
                                    mDate.setText("");
                                    mCheckAmount.setText("");
                                    mCheckAmount.setHint("Cheque Amount");
                                    mChequeBankName.setHint("Cheque Bank Name");
                                    mChekNumber.setHint("Cheque Number");
                                    mDate.setHint("Cheque Date");
                                    mEnterBankName.setHint("Enter Bank Name");
                                    mEnterBankName.setVisibility(View.GONE);
                                    mChequeImage.setImageResource(R.drawable.image_capture);
                                    base64ChequeImage = "";
                                    enteredBankName = "";
                                }
                            }
                        }*//*

                    }
                }
                break;

            case R.id.mCheckCollected:
                int totalAmt = 0;
                chequeList = chequeList;
                if (chequeList.size() > 0) {
                    //  Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < chequeList.size(); i++) {
                        totalAmt = totalAmt + Integer.parseInt(chequeList.get(i).getChequeAmount());
                    }
                    if (totalAmt != Double.parseDouble(paymentAmount)) {
                        Toasty.error(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT, true).show();
                        // Toast.makeText(this, "Cheque amount not matching with collected amount: "+String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                    } else {
                        ChequeCollectedRequest chequeCollectedRequest = new ChequeCollectedRequest();
                        chequeCollectedRequest.setAction("cheque_collection_received");
                        chequeCollectedRequest.setDocketNo(task_id);
                        chequeCollectedRequest.setUserId(userId);
                        chequeCollectedRequest.setCheques(chequeList);
                        chequeCollectedRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                        chequeCollectedRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                        mPresenter.newChequeCollectedRequest(chequeCollectedRequest);
                    }
                } else {

                    bankName = mChequeBankName.getText().toString();
                    chequeAmt = mCheckAmount.getText().toString();
                    checkNumber = mChekNumber.getText().toString();
                    chequeAmount = mCheckAmount.getText().toString();
                    date = mDate.getText().toString();
                    if (bankName.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Bank Name", Toast.LENGTH_SHORT, true).show();
                    } else if (chequeAmt.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (checkNumber.length() != 6) {
                        Toasty.error(this, "Enter Valid Cheque Number", Toast.LENGTH_SHORT, true).show();
                    } else if (chequeAmount.equalsIgnoreCase("")) {
                        Toasty.error(this, "Enter Cheque Amount", Toast.LENGTH_SHORT, true).show();
                    } else if (date.equalsIgnoreCase("")) {
                        Toasty.error(this, "Select Cheque Date", Toast.LENGTH_SHORT, true).show();
                    } else if (base64ChequeImage.equalsIgnoreCase("")) {
                        Toasty.error(this, "Capture Cheque Image", Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (bankName.equalsIgnoreCase("OTHER BANK")) {
                            enteredBankName = mEnterBankName.getText().toString();
                            if (enteredBankName.equalsIgnoreCase("")) {
                                Toast.makeText(this, "Enter Bank Name", Toast.LENGTH_SHORT).show();
                            } else {

                                for (int i = 0; i < chequeList.size(); i++) {
                                    totalAmt = totalAmt + Integer.parseInt(chequeList.get(i).getChequeAmount());
                                }
                                if (totalAmt != Double.parseDouble(paymentAmount)) {
                                    Toast.makeText(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                                } else {
                                    ChequeCollectedRequest chequeCollectedRequest = new ChequeCollectedRequest();
                                    chequeCollectedRequest.setAction("cheque_collection_received");
                                    chequeCollectedRequest.setDocketNo(task_id);
                                    chequeCollectedRequest.setUserId(userId);
                                    chequeCollectedRequest.setCheques(chequeList);
                                    chequeCollectedRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                                    chequeCollectedRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                                    mPresenter.newChequeCollectedRequest(chequeCollectedRequest);
                                }

                            }
                        } else {

                            for (int i = 0; i < chequeList.size(); i++) {
                                totalAmt = totalAmt + Integer.parseInt(chequeList.get(i).getChequeAmount());
                            }
                            if (totalAmt != Double.parseDouble(paymentAmount)) {
                                Toasty.error(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT, true).show();
                                //   Toast.makeText(this, "Cheque amount not matching with collected amount: " + String.valueOf(totalAmt), Toast.LENGTH_SHORT).show();
                            } else {
                                ChequeCollectedRequest chequeCollectedRequest = new ChequeCollectedRequest();
                                chequeCollectedRequest.setAction("cheque_collection_received");
                                chequeCollectedRequest.setDocketNo(task_id);
                                chequeCollectedRequest.setUserId(userId);
                                chequeCollectedRequest.setCheques(chequeList);
                                chequeCollectedRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                                chequeCollectedRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                                mPresenter.newChequeCollectedRequest(chequeCollectedRequest);
                            }
                        }
                    }
                }
                break;

            case R.id.mChequeNotCollected:
                connoteNotCollectedNDC();
                break;

            case R.id.mTvBackbutton:
                finish();
                break;
        }
    }


    @Override
    public void showProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void setDataToViews(DRSListResponse drsListResponse) {

    }

    @Override
    public void drsCancelCallSuceess(DrsCancelResponse drsCancelResponse) {
        if (drsCancelResponse.getStatus() == 200) {
            Toasty.warning(ChequeDetails.this, drsCancelResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        } else {
            Toasty.warning(ChequeDetails.this, drsCancelResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void setBankListDataToViews(BankListResponse bankListResponse) {

        if (bankListResponse.getStatus() == 200) {
            Toast.makeText(this, bankListResponse.getMessage(), Toast.LENGTH_SHORT).show();

            List<DataBank> dataBankList = bankListResponse.getData();
            for (int i = 0; i < bankListResponse.getData().size(); i++) {
                sampleSearchModelArrayList.add(new SampleSearchModel(bankListResponse.getData().get(i).getBankName()));
            }
            showOriginDestination(sampleSearchModelArrayList, "Search Bank", dataBankList);
        } else {
            Toast.makeText(this, bankListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void paymentLinkResponse(PaymentLinkResponse paymentLinkResponse) {

    }

    @Override
    public void qrCodeLinkResponse(QRCodeLinkResponse qrCodeLinkResponse) {

    }

    @Override
    public void codProofResponse(UploadCodProofResponse uploadCodProofResponse) {

    }

    @Override
    public void refershUpiResponse(RefreshUPIResponse refreshUPIResponse) {

    }

    @Override
    public void codChequeReceivedResponse(CodChequeReceivedResponse codChequeReceivedResponse) {
       */
/* if (codChequeReceivedResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeReceivedResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            startActivity(new Intent(this, DRSCloseActivity.class));
            finish();
        } else {
            Toasty.warning(this, codChequeReceivedResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }*//*

    }

    @Override
    public void codChequeCollectionResponse(CodChequeCollectionResponse codChequeCollectionResponse) {
        if (codChequeCollectionResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            //  startActivity(new Intent(this, DRSCloseActivity.class));

            Intent a = new Intent(this, DRSCloseActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            finish();
        } else {
            Toasty.error(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void newChequeCollectedResponse(CodChequeCollectionResponse codChequeCollectionResponse) {
        if (codChequeCollectionResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            //  startActivity(new Intent(this, DRSCloseActivity.class));

            Intent a = new Intent(this, DRSCloseActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            finish();
        } else {
            Toasty.error(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
*/
