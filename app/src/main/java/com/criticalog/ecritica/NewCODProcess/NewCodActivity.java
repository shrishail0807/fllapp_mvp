package com.criticalog.ecritica.NewCODProcess;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.leo.simplearcloader.SimpleArcDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class NewCodActivity extends AppCompatActivity implements NewCodProcessContract.MainView {
    TextView mCheckCollected, mTopBarText, mConsineeName, mAddress, mConnoteNumber, mCodAmount, mRefresh;
    ImageView mTvBackbutton, mQrForPayment;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String receiverName, addresss1, address2, address3, pincode, coonoteNum, payValue, userId, token, Drs_no, task_id;
    NewCodProcessContract.presenter mPresenter;
    EditText mSerialNumber;
    private SimpleArcDialog mProgressDialog;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    private Dialog codProofDialog;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int PERMISSION_REQUEST_POD_IMAGE = 1888;
    File photoFile = null;
    private String base64CodProofImage = "";
    TextView mCash, mCheque, mSubmitCodProof, mPaymentStatus;
    ImageView mPaymentProofImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_cod);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mCheckCollected = findViewById(R.id.mCheckCollected);
        mTopBarText = findViewById(R.id.mTopBarText);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mQrForPayment = findViewById(R.id.mQrForPayment);
        mAddress = findViewById(R.id.mAddress);
        mConsineeName = findViewById(R.id.mConsineeName);
        mConnoteNumber = findViewById(R.id.mConnoteNumber);
        mCodAmount = findViewById(R.id.mCodAmount);
        mSerialNumber = findViewById(R.id.mSerialNumber);
        mRefresh = findViewById(R.id.mRefresh);
        mPaymentStatus = findViewById(R.id.mPaymentStatus);

        //  getLocation();

        if (checkPermission()) {
            //mLinearLayMain.setVisibility(View.VISIBLE);
            //main logic or main code
            // . write your main code to execute, It will execute if the permission is already given.
        } else {
            requestPermission();
        }

        mProgressDialog = new SimpleArcDialog(this);
        mProgressDialog.setTitle("Loading......");
        mProgressDialog.setCancelable(false);


        receiverName = mCriticalogSharedPreferences.getData("receiver_name");
        addresss1 = mCriticalogSharedPreferences.getData("address_consignee1");
        address2 = mCriticalogSharedPreferences.getData("address_consignee2");
        address3 = mCriticalogSharedPreferences.getData("address_consignee3");
        pincode = mCriticalogSharedPreferences.getData("pincode_consignee");
        coonoteNum = mCriticalogSharedPreferences.getData("task_id");
        payValue = mCriticalogSharedPreferences.getData("payment_amount");
        Drs_no = mCriticalogSharedPreferences.getData("drs_number");
        task_id = mCriticalogSharedPreferences.getData("task_id");

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");

        mPresenter = new NewCodPresenterImpl(NewCodActivity.this, new NewCodInteractorImpl(),new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl(), new NewCodInteractorImpl());
        mConnoteNumber.setText(coonoteNum);
        mCodAmount.setText("COD Amount: " + payValue);

        mTopBarText.setText("COD Process");
        mConsineeName.setText(receiverName);
        mAddress.setText(addresss1 + "\n" + address2 + "\n" + address3 + "-" + pincode);

        mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mQrForPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QRCodeLinkRequest qrCodeLinkRequest = new QRCodeLinkRequest();
                qrCodeLinkRequest.setAction("get_qr_code_link");
                qrCodeLinkRequest.setDocketNo(task_id);
                qrCodeLinkRequest.setUserId(userId);
                mPresenter.generateQRCodeLink(qrCodeLinkRequest);
                /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.ecritica.co/eFreightLive/v2/paymentGateway/custom/index.php?connote_no=MjMwMzgzNTM2MzgzNDMwMzkz"));
                startActivity(browserIntent);*/
            }
        });
        mCheckCollected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewCodActivity.this, ChequeDetails.class));
            }
        });


        mSerialNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;


                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (mSerialNumber.getRight() - mSerialNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        String mobile = mSerialNumber.getText().toString();

                        if (!mobile.matches(regexStr)) {
                            Toast.makeText(NewCodActivity.this, "Enter Valid Mobile Number!!", Toast.LENGTH_SHORT).show();
                        } else {
                            PaymentLinkRequest paymentLinkRequest = new PaymentLinkRequest();
                            paymentLinkRequest.setAction("resend_payment_link");
                            paymentLinkRequest.setDocketNo(coonoteNum);
                            paymentLinkRequest.setUserId(userId);
                            paymentLinkRequest.setMobileNo(mobile);

                            mPresenter.paymentLinkRequest(paymentLinkRequest);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPaymentRefresh();
              /*  RefreshUPIRequest refreshUPIRequest = new RefreshUPIRequest();
                refreshUPIRequest.setAction("refreshupi");
                refreshUPIRequest.setUserId(userId);
                refreshUPIRequest.setTaskId(task_id);
                mPresenter.refreshUPIRequest(refreshUPIRequest);*/
                //codProofDilaog();
            }
        });
        callPaymentRefresh();
    }

    public void callPaymentRefresh() {
        RefreshUPIRequest refreshUPIRequest = new RefreshUPIRequest();
        refreshUPIRequest.setAction("refreshupi");
        refreshUPIRequest.setUserId(userId);
        refreshUPIRequest.setTaskId(task_id);
        mPresenter.refreshUPIRequest(refreshUPIRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1888) {
            if (requestCode == PERMISSION_REQUEST_POD_IMAGE && resultCode == Activity.RESULT_OK) {
                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CodProofImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mPaymentProofImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                }
            }
        }
    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    public void codProofDilaog() {
        //Dialog Manual Entry

        codProofDialog = new Dialog(NewCodActivity.this);
        codProofDialog.setContentView(R.layout.cod_proof_image_dialoge);
       /* mCash = codProofDialog.findViewById(R.id.mCash);
        mCheque = codProofDialog.findViewById(R.id.mCheque);*/
        mPaymentProofImage = codProofDialog.findViewById(R.id.mPaymentProofImage);
        mSubmitCodProof = codProofDialog.findViewById(R.id.mSubmitCodProof);
        // dialogProductDelivered.setCancelable(false);
        codProofDialog.show();
        codProofDialog.setCanceledOnTouchOutside(false);

        Window window = codProofDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        mPaymentProofImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // dialogProductDelivered.dismiss();
                //  startActivity(new Intent(NewCodActivity.this, NewCodActivity.class));
                //finish();
                openCameraIntent(PERMISSION_REQUEST_POD_IMAGE);
            }
        });

        mSubmitCodProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (base64CodProofImage.equalsIgnoreCase("")) {
                    Toasty.warning(NewCodActivity.this, R.string.capture_payment_ref_image, Toast.LENGTH_SHORT, true).show();
                } else {
                    int payTypeValue = mCriticalogSharedPreferences.getIntData("paytype_value");
                    CodChequeCollectionRequest codChequeCollectionRequest = new CodChequeCollectionRequest();
                    codChequeCollectionRequest.setAction("cod_collection_received");
                    codChequeCollectionRequest.setPaymentRecType(String.valueOf(payTypeValue));
                    codChequeCollectionRequest.setDocketNo(task_id);
                    codChequeCollectionRequest.setUserId(userId);
                    codChequeCollectionRequest.setCodImage(base64CodProofImage);
                    codChequeCollectionRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                    codChequeCollectionRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                    mPresenter.codChequeCollectionRequest(codChequeCollectionRequest);
                  /*  UploadCodProofRequest uploadCodProofRequest = new UploadCodProofRequest();
                    uploadCodProofRequest.setAction("upload_cod_proof");
                    uploadCodProofRequest.setDocketNo(task_id);
                    uploadCodProofRequest.setUserId(userId);
                    uploadCodProofRequest.setCodImage(base64CodProofImage);

                    mPresenter.uploadCodProofImage(uploadCodProofRequest);*/
                }
                //  startActivity(new Intent(NewCodActivity.this, ChequeDetails.class));
            }
        });
    }

    @Override
    public void showProgress() {

        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void setBankListDataToViews(BankListResponse bankListResponse) {

    }

    @Override
    public void paymentLinkResponse(PaymentLinkResponse paymentLinkResponse) {
        if (paymentLinkResponse.getStatus() == 200) {
            mSerialNumber.setText("");
            Toasty.success(this, paymentLinkResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        } else {
            Toasty.warning(this, paymentLinkResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void qrCodeLinkResponse(QRCodeLinkResponse qrCodeLinkResponse) {
        if (qrCodeLinkResponse.getStatus() == 200) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(qrCodeLinkResponse.getData()));
            startActivity(browserIntent);
        } else {
            Toasty.warning(this, qrCodeLinkResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void codProofResponse(UploadCodProofResponse uploadCodProofResponse) {
        if (uploadCodProofResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, uploadCodProofResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            //  startActivity(new Intent(this, DRSCloseActivity.class));
            Intent intent = new Intent(this, DRSCloseActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toasty.warning(this, uploadCodProofResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void refershUpiResponse(RefreshUPIResponse refreshUPIResponse) {
        if (refreshUPIResponse.getStatus() == 200) {
            mRefresh.setBackgroundResource(R.drawable.rounded_bacground_green);
            mPaymentStatus.setBackgroundResource(R.color.SeaGreen);
            mPaymentStatus.setText("COD Payment Received");
            Toasty.success(this, refreshUPIResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            codProofDilaog();
        } else {
            Toasty.warning(this, refreshUPIResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void codChequeReceivedResponse(CodChequeReceivedResponse codChequeReceivedResponse) {

    }

    @Override
    public void codChequeCollectionResponse(CodChequeCollectionResponse codChequeCollectionResponse) {
        if (codChequeCollectionResponse.getStatus() == 200) {
            mCriticalogSharedPreferences.saveData("from_qr_payment", "true");
            Toasty.success(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            //  startActivity(new Intent(this, DRSCloseActivity.class));
            Intent intent = new Intent(this, DRSCloseActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toasty.warning(this, codChequeCollectionResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void newChequeCollectedResponse(CodChequeCollectionResponse codChequeCollectionResponse) {

    }

    @Override
    public void onResponseFailure(Throwable throwable) {

        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
