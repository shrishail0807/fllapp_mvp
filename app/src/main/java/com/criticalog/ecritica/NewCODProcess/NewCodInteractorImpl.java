package com.criticalog.ecritica.NewCODProcess;

import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.ChequeCollectedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCodInteractorImpl implements NewCodProcessContract.GetBankListIntractor, NewCodProcessContract.GetPaymentLinkIntractor,
        NewCodProcessContract.GetQRCodeLinkIntractor, NewCodProcessContract.GetCodProofUploadIntractor, NewCodProcessContract.GetRefreshUPIIntractor,
        NewCodProcessContract.GetCodChequeReceivedIntractor, NewCodProcessContract.GetCodChequeCollectionIntractor, NewCodProcessContract.NewCodChequeCollectionIntractor {
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);
    @Override
    public void bankListSuccessful(NewCodProcessContract.GetBankListIntractor.OnFinishedListener onFinishedListener, BankListRequest bankListRequest) {

        Call<BankListResponse> bankListResponseCall = services.getBankList(bankListRequest);
        bankListResponseCall.enqueue(new Callback<BankListResponse>() {
            @Override
            public void onResponse(Call<BankListResponse> call, Response<BankListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<BankListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void paymentLinkSuccessful(NewCodProcessContract.GetPaymentLinkIntractor.OnFinishedListener onFinishedListener, PaymentLinkRequest paymentLinkRequest) {

        Call<PaymentLinkResponse> paymentLinkResponseCall=services.getPaymentLinkFromMessage(paymentLinkRequest);
        paymentLinkResponseCall.enqueue(new Callback<PaymentLinkResponse>() {
            @Override
            public void onResponse(Call<PaymentLinkResponse> call, Response<PaymentLinkResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PaymentLinkResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void paymentLinkSuccessful(NewCodProcessContract.GetQRCodeLinkIntractor.OnFinishedListener onFinishedListener, QRCodeLinkRequest qrCodeLinkRequest) {
        Call<QRCodeLinkResponse> qrCodeLinkResponseCall=services.getQrPaymenyLink(qrCodeLinkRequest);
        qrCodeLinkResponseCall.enqueue(new Callback<QRCodeLinkResponse>() {
            @Override
            public void onResponse(Call<QRCodeLinkResponse> call, Response<QRCodeLinkResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<QRCodeLinkResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void uploadCodProofSuccessful(NewCodProcessContract.GetCodProofUploadIntractor.OnFinishedListener onFinishedListener, UploadCodProofRequest uploadCodProofRequest) {

       Call<UploadCodProofResponse> uploadCodProofResponseCall=services.uploadCodProofImage(uploadCodProofRequest);
        uploadCodProofResponseCall.enqueue(new Callback<UploadCodProofResponse>() {
            @Override
            public void onResponse(Call<UploadCodProofResponse> call, Response<UploadCodProofResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<UploadCodProofResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void uploadCodProofSuccessful(NewCodProcessContract.GetRefreshUPIIntractor.OnFinishedListener onFinishedListener, RefreshUPIRequest refreshUPIRequest) {
        Call<RefreshUPIResponse> refreshUPIResponseCall=services.refreshUPI(refreshUPIRequest);
        refreshUPIResponseCall.enqueue(new Callback<RefreshUPIResponse>() {
            @Override
            public void onResponse(Call<RefreshUPIResponse> call, Response<RefreshUPIResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<RefreshUPIResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void codChequeRecivedSuccessful(NewCodProcessContract.GetCodChequeReceivedIntractor.OnFinishedListener onFinishedListener, CodChequeReceivedRequest codChequeReceivedRequest) {

        Call<CodChequeReceivedResponse> codChequeReceivedResponseCall=services.codChequeReceived(codChequeReceivedRequest);
        codChequeReceivedResponseCall.enqueue(new Callback<CodChequeReceivedResponse>() {
            @Override
            public void onResponse(Call<CodChequeReceivedResponse> call, Response<CodChequeReceivedResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CodChequeReceivedResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void codChequeCollectionSuccessful(NewCodProcessContract.GetCodChequeCollectionIntractor.OnFinishedListener onFinishedListener, CodChequeCollectionRequest codChequeCollectionRequest) {

        Call<CodChequeCollectionResponse> codChequeCollectionResponseCall=services.CodChequeCollection(codChequeCollectionRequest);
        codChequeCollectionResponseCall.enqueue(new Callback<CodChequeCollectionResponse>() {
            @Override
            public void onResponse(Call<CodChequeCollectionResponse> call, Response<CodChequeCollectionResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CodChequeCollectionResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void newCodChequeCollectionSuccessful(NewCodProcessContract.NewCodChequeCollectionIntractor.OnFinishedListener onFinishedListener, ChequeCollectedRequest chequeCollectedRequest) {

        Call<CodChequeCollectionResponse> codChequeCollectionResponseCall= services.NewCodChequeCollection(chequeCollectedRequest);
        codChequeCollectionResponseCall.enqueue(new Callback<CodChequeCollectionResponse>() {
            @Override
            public void onResponse(Call<CodChequeCollectionResponse> call, Response<CodChequeCollectionResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CodChequeCollectionResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
