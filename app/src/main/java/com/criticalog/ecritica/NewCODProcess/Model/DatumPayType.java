package com.criticalog.ecritica.NewCODProcess.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumPayType {
    @SerializedName("pay_type")
    @Expose
    private String payType;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("qr_link")
    @Expose
    private String qrLink;

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getQrLink() {
        return qrLink;
    }

    public void setQrLink(String qrLink) {
        this.qrLink = qrLink;
    }
}
