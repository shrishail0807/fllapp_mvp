package com.criticalog.ecritica.NewCODProcess.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadCodProofRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("cod_image")
    @Expose
    private String codImage;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getCodImage() {
        return codImage;
    }

    public void setCodImage(String codImage) {
        this.codImage = codImage;
    }
}
