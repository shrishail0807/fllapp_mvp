package com.criticalog.ecritica.NewCODProcess.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cheque {
    @SerializedName("bank_code")
    @Expose
    private String bankCode;
    @SerializedName("cheque_amount")
    @Expose
    private String chequeAmount;
    @SerializedName("cheque_number")
    @Expose
    private String chequeNumber;
    @SerializedName("cheque_date")
    @Expose
    private String chequeDate;
    @SerializedName("cod_image")
    @Expose
    private String codImage;

    @SerializedName("other_bank")
    @Expose
    private String other_bank;
    public String getOther_bank() {
        return other_bank;
    }

    public void setOther_bank(String other_bank) {
        this.other_bank = other_bank;
    }




    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getChequeAmount() {
        return chequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        this.chequeAmount = chequeAmount;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getCodImage() {
        return codImage;
    }

    public void setCodImage(String codImage) {
        this.codImage = codImage;
    }

}
