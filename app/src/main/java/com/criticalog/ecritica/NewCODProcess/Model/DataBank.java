package com.criticalog.ecritica.NewCODProcess.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataBank {
    @SerializedName("bank_code")
    @Expose
    private String bankCode;
    @SerializedName("bank_name")
    @Expose
    private String bankName;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
