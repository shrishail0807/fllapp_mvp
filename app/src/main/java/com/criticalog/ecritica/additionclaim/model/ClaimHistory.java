package com.criticalog.ecritica.additionclaim.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClaimHistory {
    @SerializedName("claim_date")
    @Expose
    private String claimDate;
    @SerializedName("parking_rupees")
    @Expose
    private String parkingRupees;
    @SerializedName("administrative_work")
    @Expose
    private String administrativeWork;
    @SerializedName("retrieval_from_airport")
    @Expose
    private String retrievalFromAirport;
    @SerializedName("connection_to_airport")
    @Expose
    private String connectionToAirport;
    @SerializedName("deliver_done_by_bus")
    @Expose
    private String deliverDoneByBus;
    @SerializedName("image")
    @Expose
    private List<String> image;

    public String getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(String claimDate) {
        this.claimDate = claimDate;
    }

    public String getParkingRupees() {
        return parkingRupees;
    }

    public void setParkingRupees(String parkingRupees) {
        this.parkingRupees = parkingRupees;
    }

    public String getAdministrativeWork() {
        return administrativeWork;
    }

    public void setAdministrativeWork(String administrativeWork) {
        this.administrativeWork = administrativeWork;
    }

    public String getRetrievalFromAirport() {
        return retrievalFromAirport;
    }

    public void setRetrievalFromAirport(String retrievalFromAirport) {
        this.retrievalFromAirport = retrievalFromAirport;
    }

    public String getConnectionToAirport() {
        return connectionToAirport;
    }

    public void setConnectionToAirport(String connectionToAirport) {
        this.connectionToAirport = connectionToAirport;
    }

    public String getDeliverDoneByBus() {
        return deliverDoneByBus;
    }

    public void setDeliverDoneByBus(String deliverDoneByBus) {
        this.deliverDoneByBus = deliverDoneByBus;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }
}
