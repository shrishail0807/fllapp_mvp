package com.criticalog.ecritica.additionclaim.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AdditionalClaimRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("claim_date")
    @Expose
    private String claimDate;
    @SerializedName("parking_rupees")
    @Expose
    private String parkingRupees;
    @SerializedName("administrative_work")
    @Expose
    private String administrativeWork;
    @SerializedName("retrieval_from_airport")
    @Expose
    private String retrievalFromAirport;
    @SerializedName("connection_to_airport")
    @Expose
    private String connectionToAirport;
    @SerializedName("deliver_done_by_bus")
    @Expose
    private String deliverDoneByBus;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("claims")
    @Expose
    private List<Claim> claims;

    @SerializedName("check_status")
    @Expose
    private String check_status;

    public String getCheck_status() {
        return check_status;
    }

    public void setCheck_status(String check_status) {
        this.check_status = check_status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(String claimDate) {
        this.claimDate = claimDate;
    }

    public String getParkingRupees() {
        return parkingRupees;
    }

    public void setParkingRupees(String parkingRupees) {
        this.parkingRupees = parkingRupees;
    }

    public String getAdministrativeWork() {
        return administrativeWork;
    }

    public void setAdministrativeWork(String administrativeWork) {
        this.administrativeWork = administrativeWork;
    }

    public String getRetrievalFromAirport() {
        return retrievalFromAirport;
    }

    public void setRetrievalFromAirport(String retrievalFromAirport) {
        this.retrievalFromAirport = retrievalFromAirport;
    }

    public String getConnectionToAirport() {
        return connectionToAirport;
    }

    public void setConnectionToAirport(String connectionToAirport) {
        this.connectionToAirport = connectionToAirport;
    }

    public String getDeliverDoneByBus() {
        return deliverDoneByBus;
    }

    public void setDeliverDoneByBus(String deliverDoneByBus) {
        this.deliverDoneByBus = deliverDoneByBus;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Claim> getClaims() {
        return claims;
    }

    public void setClaims(List<Claim> claims) {
        this.claims = claims;
    }
}
