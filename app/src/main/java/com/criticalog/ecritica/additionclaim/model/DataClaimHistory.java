package com.criticalog.ecritica.additionclaim.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataClaimHistory {
    @SerializedName("claims")
    @Expose
    private List<ClaimHistory> claims;

    public List<ClaimHistory> getClaims() {
        return claims;
    }

    public void setClaims(List<ClaimHistory> claims) {
        this.claims = claims;
    }
}
