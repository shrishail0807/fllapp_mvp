package com.criticalog.ecritica.additionclaim.interfaces;

import java.util.List;

public interface ClaimHistoryImageLinkClick {
    public void ClaimHistoryImageLinkClick(int position, List<String> images);
}
