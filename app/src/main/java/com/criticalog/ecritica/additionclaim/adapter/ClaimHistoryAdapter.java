package com.criticalog.ecritica.additionclaim.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPBarcodeScan.Adapter.DocketWithOriginDestAdapter;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.additionclaim.interfaces.ClaimHistoryImageLinkClick;
import com.criticalog.ecritica.additionclaim.model.ClaimHistory;

import java.util.ArrayList;
import java.util.List;

public class ClaimHistoryAdapter extends RecyclerView.Adapter<ClaimHistoryAdapter.ClaimHistoryViewHolder> {

    private Context context;
    private List<ClaimHistory> claimHistoryList;
    private ClaimHistoryImageLinkClick claimHistoryImageLinkClick;

    public ClaimHistoryAdapter(Context context, List<ClaimHistory> claimHistoryList,ClaimHistoryImageLinkClick claimHistoryImageLinkClick) {
        this.context = context;
        this.claimHistoryList = claimHistoryList;
        this.claimHistoryImageLinkClick=claimHistoryImageLinkClick;
        this.context=context;
    }

    @NonNull
    @Override
    public ClaimHistoryAdapter.ClaimHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.claim_history_item, parent, false);
        return new ClaimHistoryAdapter.ClaimHistoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ClaimHistoryAdapter.ClaimHistoryViewHolder holder, int position) {

        String claimDate = "<b>" + "Claim Date: " + "</b> " + "₹"+claimHistoryList.get(position).getClaimDate();
        holder.mClaimDate.setText(Html.fromHtml(claimDate));

        String parkingFees = "<b>" + "Parking Rupees: " + "</b> " + "₹"+claimHistoryList.get(position).getParkingRupees();
        holder.mParkingAmount.setText(Html.fromHtml(parkingFees));

        String administrative = "<b>" + "Administrative Work: " + "</b> " +claimHistoryList.get(position).getAdministrativeWork()+" KM";
        holder.mAdministrativeWork.setText(Html.fromHtml(administrative));

        String retrievalFromAirport = "<b>" + "Retrieval From Airport: " + "</b> " +claimHistoryList.get(position).getRetrievalFromAirport()+" KM";
        holder.mRetrievalFromAirport.setText(Html.fromHtml(retrievalFromAirport));

        String connectionToAirport = "<b>" + "Connection to Airport: " + "</b> " +claimHistoryList.get(position).getConnectionToAirport()+" KM";
        holder.mConnectionToAirport.setText(Html.fromHtml(connectionToAirport));

        String deliveryByBusOrTrain = "<b>" + "Delivery by Bus or Train: " + "</b> " + "₹"+claimHistoryList.get(position).getDeliverDoneByBus();
        holder.mDeliveryByBusTrain.setText(Html.fromHtml(deliveryByBusOrTrain));

      /*  holder.mClaimDate.setText("Claim Date: " + "₹"+claimHistoryList.get(position).getClaimDate());
        holder.mParkingAmount.setText("Parking Rupees: " + "₹"+ claimHistoryList.get(position).getParkingRupees());
        holder.mAdministrativeWork.setText("Administrative Work: " + "₹"+ claimHistoryList.get(position).getAdministrativeWork());
        holder.mRetrievalFromAirport.setText("Retrieval From Airport: " + "₹"+ claimHistoryList.get(position).getRetrievalFromAirport());
        holder.mConnectionToAirport.setText("Connection to Airport: " + "₹"+ claimHistoryList.get(position).getConnectionToAirport());
        holder.mDeliveryByBusTrain.setText("Delivery by Bus or Train: " + "₹"+ claimHistoryList.get(position).getDeliverDoneByBus());*/

        holder.mClickHereForImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                claimHistoryImageLinkClick.ClaimHistoryImageLinkClick(position,claimHistoryList.get(position).getImage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return claimHistoryList.size();
    }

    public class ClaimHistoryViewHolder extends RecyclerView.ViewHolder {
        TextView mClaimDate, mParkingAmount, mAdministrativeWork, mRetrievalFromAirport,
                mConnectionToAirport, mDeliveryByBusTrain, mClickHereForImages;

        public ClaimHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mClaimDate = itemView.findViewById(R.id.mClaimDate);
            mParkingAmount = itemView.findViewById(R.id.mParkingAmount);
            mAdministrativeWork = itemView.findViewById(R.id.mAdministrativeWork);
            mRetrievalFromAirport = itemView.findViewById(R.id.mRetrievalFromAirport);
            mConnectionToAirport = itemView.findViewById(R.id.mConnectionToAirport);
            mDeliveryByBusTrain = itemView.findViewById(R.id.mDeliveryByBusTrain);
            mClickHereForImages = itemView.findViewById(R.id.mClickHereForImages);
        }
    }
}
