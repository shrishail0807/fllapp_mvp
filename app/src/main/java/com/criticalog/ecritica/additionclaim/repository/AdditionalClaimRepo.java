package com.criticalog.ecritica.additionclaim.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryResponse;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdditionalClaimRepo {
    private RestServices mRestServices;
    private MutableLiveData<AdditionalClaimResponse> additionalClaimResponseMutableLiveData;
    private MutableLiveData<AdditionalClaimHistoryResponse> additionalClaimHistoryResponseMutableLiveData;

    public AdditionalClaimRepo() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        additionalClaimResponseMutableLiveData = new MutableLiveData<>();
        additionalClaimHistoryResponseMutableLiveData = new MutableLiveData<>();
    }

    //UPDATE ADDITIONAL CLAIM API CALL
    public void additionalClaimRequest(AdditionalClaimRequest additionalClaimRequest) {
        Call<AdditionalClaimResponse> additionalClaimResponseCall = mRestServices.additionalClaim(additionalClaimRequest);
        additionalClaimResponseCall.enqueue(new Callback<AdditionalClaimResponse>() {
            @Override
            public void onResponse(Call<AdditionalClaimResponse> call, Response<AdditionalClaimResponse> response) {

                additionalClaimResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<AdditionalClaimResponse> call, Throwable t) {
                additionalClaimResponseMutableLiveData.setValue(null);
            }
        });
    }

    //ADDITION CLAIM HISTORY API CALL
    public void additionalClaimHistoryRequest(AdditionalClaimHistoryRequest additionalClaimHistoryRequest) {

        Call<AdditionalClaimHistoryResponse> additionalClaimHistoryResponseCall = mRestServices.additionalClaimHistory(additionalClaimHistoryRequest);
        additionalClaimHistoryResponseCall.enqueue(new Callback<AdditionalClaimHistoryResponse>() {
            @Override
            public void onResponse(Call<AdditionalClaimHistoryResponse> call, Response<AdditionalClaimHistoryResponse> response) {
                additionalClaimHistoryResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<AdditionalClaimHistoryResponse> call, Throwable t) {
                additionalClaimHistoryResponseMutableLiveData.setValue(null);
            }
        });
    }

    //ADDITION CLAIM UPDATE
    public LiveData<AdditionalClaimResponse> getAdditionClaimLiveData() {
        return additionalClaimResponseMutableLiveData;
    }

    //ADDITIONAL CLAIM HISTORY
    public LiveData<AdditionalClaimHistoryResponse> additionalClaimHistoryResponseLiveData() {
        return additionalClaimHistoryResponseMutableLiveData;
    }
}
