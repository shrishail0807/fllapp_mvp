package com.criticalog.ecritica.additionclaim.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryResponse;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimResponse;
import com.criticalog.ecritica.additionclaim.repository.AdditionalClaimRepo;

public class AdditionalClaimViewModel extends AndroidViewModel {
    private AdditionalClaimRepo additionalClaimRepo;
    private LiveData<AdditionalClaimResponse> additionalClaimResponseLiveData;
    private LiveData<AdditionalClaimHistoryResponse> additionalClaimHistoryResponseLiveData;

    public AdditionalClaimViewModel(@NonNull Application application) {
        super(application);
    }

    //VIEWMODEL INITIALIZATION
    public void init() {
        additionalClaimRepo = new AdditionalClaimRepo();
        additionalClaimResponseLiveData = additionalClaimRepo.getAdditionClaimLiveData();
        additionalClaimHistoryResponseLiveData = additionalClaimRepo.additionalClaimHistoryResponseLiveData();
    }

    //ADDITIONAL CLAIM
    public void additionalClaimReq(AdditionalClaimRequest additionalClaimRequest) {
        additionalClaimRepo.additionalClaimRequest(additionalClaimRequest);
    }

    public LiveData<AdditionalClaimResponse> additionalClaimLiveData() {
        return additionalClaimResponseLiveData;
    }

    //ADDITIONAL CLAIM HISTORY
    public void additionalClaimHistoryRequest(AdditionalClaimHistoryRequest additionalClaimHistoryRequest) {
        additionalClaimRepo.additionalClaimHistoryRequest(additionalClaimHistoryRequest);
    }

    public LiveData<AdditionalClaimHistoryResponse> additionalClaimHistoryResponseLiveData() {
        return additionalClaimHistoryResponseLiveData;
    }

}
