package com.criticalog.ecritica.additionclaim;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimResponse;
import com.criticalog.ecritica.additionclaim.model.Claim;
import com.criticalog.ecritica.additionclaim.viewmodel.AdditionalClaimViewModel;
import com.criticalog.ecritica.databinding.ActivityAdditionalClaimBinding;
import com.criticalog.ecritica.mvpdrspreparation.DRSPreparationActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.theartofdev.edmodo.cropper.CropImage;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class AdditionalClaimActivity extends AppCompatActivity {
    private int year, month, day;
    private ActivityAdditionalClaimBinding activityAdditionalClaimBinding;
    File photoFile = null;
    private static int CAMERA_REQUEST_PROOF1 = 1890;
    private static int CAMERA_REQUEST_PROOF2 = 1891;
    private static int CAMERA_REQUEST_PROOF3 = 1892;
    private String base64CaptureProof1 = "", base64CaptureProof2 = "",
            base64CaptureProof3 = "", claimDate = "";
    private AdditionalClaimViewModel additionalClaimViewModel;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double latitude = 0.0, longitude = 0.0;
    private static final int PERMISSION_REQUEST_CODE_ADDITIONAL_CLAIM = 1888;
    List<Claim> claimImages = new ArrayList<>();
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    private SimpleArcDialog mProgressBar;
    private String checkClaimStatus="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityAdditionalClaimBinding = DataBindingUtil.setContentView(this, R.layout.activity_additional_claim);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");
        activityAdditionalClaimBinding.mHistory.setPaintFlags(activityAdditionalClaimBinding.mHistory.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        userId = mCriticalogSharedPreferences.getData("userId");



        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        additionalClaimViewModel = ViewModelProviders.of(this).get(AdditionalClaimViewModel.class);
        additionalClaimViewModel.init();
        activityAdditionalClaimBinding.mClaimDate.setFocusable(false);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };
        getLocation();
        additionalClaimViewModel.additionalClaimLiveData().observe(this, new Observer<AdditionalClaimResponse>() {
            @Override
            public void onChanged(AdditionalClaimResponse additionalClaimResponse) {
                mProgressBar.dismiss();

                if(checkClaimStatus.equalsIgnoreCase("true"))
                {
                    if (additionalClaimResponse.getStatus() == 200) {
                        activityAdditionalClaimBinding.mClaimDate.setText("");
                        activityAdditionalClaimBinding.mClaimDate.setHint("Claim Date");
                        Toasty.error(AdditionalClaimActivity.this, additionalClaimResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    } else {
                        /*activityAdditionalClaimBinding.mClaimDate.setText("");
                        activityAdditionalClaimBinding.mClaimDate.setHint("Claim Date");
                        Toasty.error(AdditionalClaimActivity.this, additionalClaimResponse.getMessage(), Toast.LENGTH_SHORT, true).show();*/
                    }
                }else {
                    if (additionalClaimResponse.getStatus() == 200) {
                        Toasty.success(AdditionalClaimActivity.this, additionalClaimResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        finish();
                    } else {
                        Toasty.error(AdditionalClaimActivity.this, additionalClaimResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    }
                }
            }
        });
        activityAdditionalClaimBinding.mHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdditionalClaimActivity.this,ClaimHistoryActivity.class));
            }
        });
        activityAdditionalClaimBinding.mClaimDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker("claim_date");
            }
        });

        activityAdditionalClaimBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        activityAdditionalClaimBinding.mProofImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraIntent(CAMERA_REQUEST_PROOF1);
            }
        });
        activityAdditionalClaimBinding.mProofImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraIntent(CAMERA_REQUEST_PROOF2);
            }
        });
        activityAdditionalClaimBinding.mProofImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraIntent(CAMERA_REQUEST_PROOF3);
            }
        });
        activityAdditionalClaimBinding.mSubmitClaim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parking = activityAdditionalClaimBinding.mParkingAmount.getText().toString();
                String administrativeWork = activityAdditionalClaimBinding.mAdministrativeWork.getText().toString();
                String retrievalFromAirport = activityAdditionalClaimBinding.mRerievalsFromAirPort.getText().toString();
                String connectionToAirport = activityAdditionalClaimBinding.mConnectionToAirport.getText().toString();
                String deliverDoneByBus = activityAdditionalClaimBinding.mDeliveryByBusTrain.getText().toString();
                if (claimDate.equalsIgnoreCase("")) {
                    Toasty.error(AdditionalClaimActivity.this, "Select Claim Date!!", Toast.LENGTH_SHORT, true).show();
                } else if (parking.equalsIgnoreCase("") && administrativeWork.equalsIgnoreCase("") && retrievalFromAirport.equalsIgnoreCase("") &&
                        connectionToAirport.equalsIgnoreCase("") && deliverDoneByBus.equalsIgnoreCase("")) {
                    Toasty.error(AdditionalClaimActivity.this, "Enter At Least One Detail!!", Toast.LENGTH_SHORT, true).show();
                } else if (claimImages.size() == 0) {
                    Toasty.error(AdditionalClaimActivity.this, "Capture Proof Image!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    getLocation();
                    AdditionalClaimRequest additionalClaimRequest = new AdditionalClaimRequest();
                    additionalClaimRequest.setAction("update_additional_claim");
                    additionalClaimRequest.setClaimDate(claimDate);
                    additionalClaimRequest.setParkingRupees(parking);
                    additionalClaimRequest.setAdministrativeWork(administrativeWork);
                    additionalClaimRequest.setRetrievalFromAirport(retrievalFromAirport);
                    additionalClaimRequest.setConnectionToAirport(connectionToAirport);
                    additionalClaimRequest.setDeliverDoneByBus(deliverDoneByBus);
                    additionalClaimRequest.setClaims(claimImages);
                    additionalClaimRequest.setLatitude(String.valueOf(latitude));
                    additionalClaimRequest.setLongitude(String.valueOf(longitude));
                    additionalClaimRequest.setUserId(userId);
                    additionalClaimViewModel.additionalClaimReq(additionalClaimRequest);
                    checkClaimStatus="";
                    mProgressBar.show();
                }
            }
        });
    }


    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE_ADDITIONAL_CLAIM);
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(AdditionalClaimActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(AdditionalClaimActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AdditionalClaimActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(AdditionalClaimActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    private void datePicker(String fromClck) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        claimDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        activityAdditionalClaimBinding.mClaimDate.setText(claimDate);

                        AdditionalClaimRequest additionalClaimRequest = new AdditionalClaimRequest();
                        additionalClaimRequest.setAction("update_additional_claim");
                        additionalClaimRequest.setClaimDate(claimDate);
                        additionalClaimRequest.setLatitude(String.valueOf(latitude));
                        additionalClaimRequest.setLongitude(String.valueOf(longitude));
                        additionalClaimRequest.setUserId(userId);
                        additionalClaimRequest.setCheck_status("1");
                        additionalClaimViewModel.additionalClaimReq(additionalClaimRequest);

                        checkClaimStatus="true";
                    }
                }, year, month, day);
        //  datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //useVision="0";
        Bitmap photo = null;
        if (requestCode == 1890) {
            if (requestCode == CAMERA_REQUEST_PROOF1 && resultCode == Activity.RESULT_OK) {

                if (photoFile != null) {

                    File absoluteFile = photoFile.getAbsoluteFile();
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CaptureProof1 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    activityAdditionalClaimBinding.mProofImage1.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));

                    Claim claim = new Claim();
                    claim.setImage(base64CaptureProof1);
                    claimImages.add(claim);
                }
            }
        } else if (requestCode == 1891) {
            if (requestCode == CAMERA_REQUEST_PROOF2 && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CaptureProof2 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    activityAdditionalClaimBinding.mProofImage2.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    Claim claim = new Claim();
                    claim.setImage(base64CaptureProof2);
                    claimImages.add(claim);
                }
            }
        } else if (requestCode == 1892) {
            if (requestCode == CAMERA_REQUEST_PROOF3 && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CaptureProof3 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    activityAdditionalClaimBinding.mProofImage3.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));

                    Claim claim = new Claim();
                    claim.setImage(base64CaptureProof3);
                    claimImages.add(claim);
                }
            }
        }
    }
}
