package com.criticalog.ecritica.additionclaim.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ClaimImagesAdapter extends RecyclerView.Adapter<ClaimImagesAdapter.ClaimViewHolder> {
    private Context context;
    private List<String> imagesList;

    public ClaimImagesAdapter(Context context, List<String> imagesList) {
        this.context = context;
        this.imagesList = imagesList;

    }

    @NonNull
    @Override
    public ClaimImagesAdapter.ClaimViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.claim_image_item, parent, false);
        return new ClaimImagesAdapter.ClaimViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ClaimImagesAdapter.ClaimViewHolder holder, int position) {


        Picasso.with(context)
                .load(imagesList.get(position).toString())
                .resize(400, 400)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.brokenimage)
                .into(holder.mImagesViewProof);
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class ClaimViewHolder extends RecyclerView.ViewHolder {
        ImageView mImagesViewProof;

        public ClaimViewHolder(@NonNull View itemView) {
            super(itemView);
            mImagesViewProof = itemView.findViewById(R.id.mImagesViewProof);
        }
    }
}
