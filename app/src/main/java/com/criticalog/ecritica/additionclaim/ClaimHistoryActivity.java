package com.criticalog.ecritica.additionclaim;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.additionclaim.adapter.ClaimHistoryAdapter;
import com.criticalog.ecritica.additionclaim.adapter.ClaimImagesAdapter;
import com.criticalog.ecritica.additionclaim.interfaces.ClaimHistoryImageLinkClick;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryResponse;
import com.criticalog.ecritica.additionclaim.viewmodel.AdditionalClaimViewModel;
import com.criticalog.ecritica.databinding.ActivityClaimHistoryBinding;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class ClaimHistoryActivity extends AppCompatActivity implements ClaimHistoryImageLinkClick {
    private ActivityClaimHistoryBinding activityClaimHistoryBinding;
    private AdditionalClaimViewModel additionalClaimViewModel;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    private ClaimHistoryAdapter claimHistoryAdapter;
    private Dialog dialogClaimImages;
    private ClaimImagesAdapter claimImagesAdapter;
    private SimpleArcDialog mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityClaimHistoryBinding = DataBindingUtil.setContentView(this, R.layout.activity_claim_history);


        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        //VIEWMODEL INITIALIZATION
        additionalClaimViewModel = ViewModelProviders.of(this).get(AdditionalClaimViewModel.class);
        additionalClaimViewModel.init();



        activityClaimHistoryBinding.mTopBarText.setText("Cliam History");
        activityClaimHistoryBinding.mHistory.setVisibility(View.INVISIBLE);
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        userId = mCriticalogSharedPreferences.getData("userId");

        AdditionalClaimHistoryRequest additionalClaimHistoryRequest = new AdditionalClaimHistoryRequest();
        additionalClaimHistoryRequest.setAction("additional_claims_history");
        additionalClaimHistoryRequest.setUserId(userId);
        additionalClaimViewModel.additionalClaimHistoryRequest(additionalClaimHistoryRequest);

        mProgressBar.show();

        activityClaimHistoryBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        additionalClaimViewModel.additionalClaimHistoryResponseLiveData().observe(this, new Observer<AdditionalClaimHistoryResponse>() {
            @Override
            public void onChanged(AdditionalClaimHistoryResponse additionalClaimHistoryResponse) {
                mProgressBar.dismiss();
                if (additionalClaimHistoryResponse.getStatus() == 200) {
                    Toasty.success(ClaimHistoryActivity.this, additionalClaimHistoryResponse.getMessage(), Toasty.LENGTH_SHORT, true).show();
                    claimHistoryAdapter = new ClaimHistoryAdapter(ClaimHistoryActivity.this, additionalClaimHistoryResponse.getData().getClaims(), ClaimHistoryActivity.this);
                    activityClaimHistoryBinding.mRvClaimHistory.setLayoutManager(new LinearLayoutManager(ClaimHistoryActivity.this));
                    activityClaimHistoryBinding.mRvClaimHistory.setAdapter(claimHistoryAdapter);
                } else {
                    Toasty.error(ClaimHistoryActivity.this, additionalClaimHistoryResponse.getMessage(), Toasty.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    @Override
    public void ClaimHistoryImageLinkClick(int position, List<String> imagesList) {
        if (imagesList.size() > 0) {
            productDeliveredDialog(imagesList);
        }
    }

    public void productDeliveredDialog(List<String> imagesList) {
        //Dialog Manual Entry
        RecyclerView mRvClaimImages;
        ImageView mCloseDialog;
        dialogClaimImages = new Dialog(ClaimHistoryActivity.this);
        dialogClaimImages.setContentView(R.layout.dialog_claim_images);
        mRvClaimImages = dialogClaimImages.findViewById(R.id.MRVIMages);
        mCloseDialog = dialogClaimImages.findViewById(R.id.mCloseDialog);
        dialogClaimImages.setCancelable(true);
        Window window = dialogClaimImages.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        claimImagesAdapter = new ClaimImagesAdapter(this, imagesList);
        mRvClaimImages.setLayoutManager(new LinearLayoutManager(this));
        mRvClaimImages.setAdapter(claimImagesAdapter);

        mCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogClaimImages.dismiss();
            }
        });

        dialogClaimImages.show();
    }
}
