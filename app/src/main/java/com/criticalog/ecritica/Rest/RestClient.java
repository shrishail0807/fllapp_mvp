package com.criticalog.ecritica.Rest;

import com.criticalog.ecritica.Utils.StaticUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static Retrofit retrofit;

    //TEST BASE URL
    private static final String BASE_URL = "https://test.criticalogistics.com/Test/ecritica_api/";

    //LIVE BASE URL
    //private static final String BASE_URL = "https://www.ecritica.co/efreightlive/ecritica_api/";

    //LIVE BASE URL SUBHASH
   // private static final String BASE_URL = "http://api.ecritica.co/";

    public static Retrofit getRetrofitInstance() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //INTERCEPTOR for Passing token in Header
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(40, TimeUnit.SECONDS)
                .writeTimeout(40, TimeUnit.SECONDS)
                .connectTimeout(40, TimeUnit.SECONDS)
               // .addInterceptor(logging)                     //Turn Off for production build(Only added for debug purpose)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("token", StaticUtils.TOKEN)
                                .addHeader("header_app_version", StaticUtils.APP_VERSION)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();

        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .client(client)
                    .baseUrl(StaticUtils.BASE_URL_DYNAMIC)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
