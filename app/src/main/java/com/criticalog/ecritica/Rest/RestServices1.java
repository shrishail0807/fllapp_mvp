package com.criticalog.ecritica.Rest;

import com.criticalog.ecritica.model.BillionDistanceResponse;
import com.criticalog.ecritica.model.CallRequest;
import com.criticalog.ecritica.model.CallResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestServices1 {
    @GET("distancematrix/json")
    Call<BillionDistanceResponse> getDistanceBetween(@Query("origins") String origins, @Query("destinations") String destinations,
                                                     @Query("key") String key);
    @Headers("Content-Type: application/json")
    @POST("call.php?action=call_masking")
    Call<CallResponse> callInitiate(@Body CallRequest callRequest);
}
