package com.criticalog.ecritica.Rest;
import com.criticalog.ecritica.MVPAttendance.Model.PunchInRequest;
import com.criticalog.ecritica.MVPAttendance.Model.PunchInResponse;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitResponse;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationRequest;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidationRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateChecklistYesNoResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateCheclistYesNoRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateResponse;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.AllCashListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashListByDateResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierListResponse;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPRequest;
import com.criticalog.ecritica.MVPCashHandover.model.CashierOTPResponse;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitRequest;
import com.criticalog.ecritica.MVPCashHandover.model.HandoverSubmitResponse;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PayTypeListResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredResponse;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.GetMyBagResponse;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.GetMyBagsRequest;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.RGSAConfirmHubRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.ConnoteShortInfoRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadResponse;
import com.criticalog.ecritica.MVPEdpSdcCopy.model.ConnoteShortInfoResponse;
import com.criticalog.ecritica.MVPHistory.Model.HistoryRequest;
import com.criticalog.ecritica.MVPHistory.Model.HistoryResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripResponse;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanRequest;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckRequest;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegativeStatusRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InscanNegativeStatusResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegativeStatusRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateResponseNew;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DestinationsResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketListResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.GetMawbResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivePostData;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivedResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginRequest;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.OriginResponse;
import com.criticalog.ecritica.MVPLogin.LoginPostData;
import com.criticalog.ecritica.MVPLogin.LoginResponseData;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateResponse;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel.PickupCancelRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel.PickupCancelResponse;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsResponse;
import com.criticalog.ecritica.MVPPickup.PickupListResponse;
import com.criticalog.ecritica.MVPPickup.PickupPostData;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSARequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSAResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListResponse;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadRequest;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadResponse;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckResponse;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupResponse;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckRequest;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckResponse;
import com.criticalog.ecritica.NewCODProcess.Model.BankListRequest;
import com.criticalog.ecritica.NewCODProcess.Model.BankListResponse;
import com.criticalog.ecritica.NewCODProcess.Model.ChequeCollectedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeReceivedResponse;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkRequest;
import com.criticalog.ecritica.NewCODProcess.Model.QRCodeLinkResponse;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofRequest;
import com.criticalog.ecritica.NewCODProcess.Model.UploadCodProofResponse;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimHistoryResponse;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimRequest;
import com.criticalog.ecritica.additionclaim.model.AdditionalClaimResponse;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationRequest;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationResponse;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesRequest;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesResponse;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHRequest;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHResponse;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeRequest;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeResponse;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameRequest;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameResponse;
import com.criticalog.ecritica.model.CallMaskingStatusRequest;
import com.criticalog.ecritica.model.CallMaskingStatusResponse;
import com.criticalog.ecritica.model.LocationRequest;
import com.criticalog.ecritica.model.LocationResponse;
import com.criticalog.ecritica.model.NDCOtpRequest;
import com.criticalog.ecritica.model.NDCOtpResponse;
import com.criticalog.ecritica.model.RulesRequest;
import com.criticalog.ecritica.model.RulesResponse;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSResponse;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsResponse;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourRequest;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourResponse;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationRequest;
import com.criticalog.ecritica.mvproundcreation.model.RoundCreationResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehicleTypeResponse;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubRequest;
import com.criticalog.ecritica.mvproundcreation.model.VehiclesByHubResponse;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataRequest;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataResponse;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.NFOPRSStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFOPRSStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentPRSRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentPRSResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusResponse;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentResponse;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentsRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RestServices {
    @Headers("Content-Type: application/json")
    @POST("user.php")
    Call<LoginResponseData> login(@Header("token") String headerToken, @Body LoginPostData loginPostData);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=prsdrs")
    Call<PickupListResponse> pickupList(@Body PickupPostData pickupPostData);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=reasonscancel")
    Call<PickupReasonsResponse> pickupCancelReasons(@Body PickupReasonsRequest pickupReasonsRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=pickupcancel_new")
    Call<PickupCancelResponse> pickupCancel(@Body PickupCancelRequest pickupCancelRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=docketstatus")
    Call<DocketValidateResponse> docketValidation(@Body DocketValidationRequest docketValidationRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=pickupsuccess")
    Call<PickupSuccessResponse> pickupSuccess(@Body PickupSuccessRequest pickupSuccessRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action?=pincode_validation")
    Call<PincodeValidateResponse> pincodeValidation(@Body PincodeValidateRequest pincodeValidateRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=closedocket")
    Call<CloseDocketResponse> closeDocket(@Body CloseDocketRequest closeDocketRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=closeclient_new")
    Call<CloseClientResponse> closeClient(@Body CloseClientRequest closeClientRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=drslist")
    Call<DRSListResponse> getDRSList(@Body DRSListRequest drsListRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=refreshupi")
    Call<RefreshUPIResponse> refreshUPI(@Body RefreshUPIRequest refreshUPIRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=drs_cancel")
    Call<DrsCancelResponse> drsCancel(@Body DrsCancelRequest drsCancelRequest);

    @Headers("Content-Type: application/json")
    @POST("linehaul.php?action=get_origins")
    Call<OriginResponse> getAllOrigins(@Body OriginRequest getOriginPostData);

    @Headers("Content-Type: application/json")
    @POST("linehaul.php?action=get_destinations")
    Call<DestinationsResponse> getAllDestinations(@Body DestinationsRequest destinationsRequest);

    @Headers("Content-Type: application/json")
    @POST("linehaul.php?action=get_mwab_no")
    Call<GetMawbResponse> getMawbNumber(@Body GetMawbRequest getMawbPostData);

    @Headers("Content-Type: application/json")
    @POST("linehaul.php?action=get_docket_list")
    Call<DocketListResponse> getDocketNumbers(@Body DocketListRequest docketListRequest);

    @Headers("Content-Type: application/json")
    @POST("linehaul.php?action=update_linehaul_received")
    Call<LinehaulReceivedResponse> linehaulReceived(@Body LinehaulReceivePostData linehaulReceivePostData);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=tat_check")
    Call<TatCheckResponse> tatCheck(@Body TatCheckRequest tatCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=prs_drs_history")
    Call<HistoryResponse> history(@Body HistoryRequest historyRequest);

    //INSCAN
    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_docket_check")
    Call<DocketCheckResponse> inscanDocketCheck(@Body DocketCheckRequest docketCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_pincode_validation")
    Call<OriginPincodeValidateResponse> originPincodeValidate(@Body OriginPincodeValidateRequest originPincodeValidateRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_actualvalue")
    Call<ActualValueInscanResponse> actualValueInscan(@Body ActualValueInscanRequest actualValueInscanRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscanbox_number")
    Call<InScanBoxResponse> inScanBox(@Body InScanBoxRequest inScanBoxRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_submit_with_doc")
    Call<InScanSubmitResponse> inScanSubmit(@Body InScanSubmitRequest inScanSubmitRequest);

    //INSCAN NEW CHANGE BELOW

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inspection_failed")
    Call<InspectionNegSubmitResponseNew> inspectionNegativeSubmitNew(@Body InspectionNegSubmitRequestNew inspectionNegSubmitRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=negative_status_inspection")
    Call<InscanNegativeStatusResponseNew> inspectionNegativeStatusNew(@Body InspectionNegativeStatusRequestNew inspectionNegativeStatusRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inspection_done")
    Call<InspectionDoneResponseNew> inspectionDoneNew(@Body InspectionDoneRequestNew inspectionDoneRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_docket_check_new")
    Call<DocketCheckResponseNew> inscanDocketCheckNew(@Body DocketCheckRequestNew docketCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_pincode_validation_new")
    Call<OriginPincodeValidateResponseNew> originPincodeValidateNew(@Body OriginPincodeValidateRequestNew originPincodeValidateRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_actualvalue_new")
    Call<ActualValueInscanResponseNew> actualValueInscanNew(@Body ActualValueInscanRequestNew actualValueInscanRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscanbox_number")
    Call<InScanBoxResponseNew> inScanBoxNew(@Body InScanBoxRequestNew inScanBoxRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_submit_with_doc_new")
    Call<InScanSubmitResponseNew> inScanSubmitNew(@Body InScanSubmitRequestNew inScanSubmitRequest);

    //INSCAN
    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=product_delivered")
    Call<ProductDeliveredResponse> productDelivered(@Body ProductDeliveredRequest productDeliveredRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=bagging_orghub")
    Call<BaggingOriginDestResponse> baggingOriginDestinationHub(@Body BaggingOriginDestRequest baggingOriginDestRequest);


    @Headers("Content-Type: application/json")
    @POST("operation.php?action=negative_orghub")
    Call<NegativeOriginHubResponse> negativeOriginHub(@Body NegativeOriginHubRequest negativeOriginHubRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=negative_negstatus")
    Call<NDCReasonsResponse> getNDCReasons(@Body NDCReasonsRequest ndcReasonsRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=negative_validate")
    Call<NegativeValidateResponse> negativeValidate(@Body NegativeValidateRequest negativeValidateRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=negative_submit_bag")
    Call<NegativeSubmitResponse> negativeSubmit(@Body NegativeSubmitRequest negativeSubmitRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=connote_enquiry")
    Call<ConnoteEnquiryResponse> connoteEnquiry(@Body ConnoteEnquiryRequest connoteEnquiryRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=bagging_bag")
    Call<BagCheckResponse> baggingBag(@Body BagCheckRequest bagCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=bagging_boxnumber")
    Call<BoxNumberValidationResponse> boxValidationBagging(@Body BoxNumberValidationRequest boxNumberValidationRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=bagging_submit")
    Call<BaggingSubmitResponse> baggingSubmit(@Body BaggingSubmitRequest baggingSubmitRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=rider_log")
    Call<RiderLogResponse> userRiderLog(@Body RiderLogRequest riderLogRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=fll_trip")
    Call<TripResponse> tripDataSubmit(@Body TripRequest tripRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=start_and_stop_ride")
    Call<StartStopRideResponse> startStopRide(@Body StartStopRideRequest startStopRideRequest);

    //SECTOR PICKUP
    @Headers("Content-Type: application/json")
    @POST("user.php?action=awb_check")
    Call<AWBCheckResponse> awbCheck(@Body AWBCheckRequest awbCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=sector_pickup")
    Call<SectorPickupResponse> sectorPickup(@Body SectorPickupRequest sectorPickupRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=service")
    Call<SrServiceResponse> srSeriveRequest(@Body SrServiceRequest srServiceRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=service_update")
    Call<SrUpdateResponse> srUpdateRequest(@Body SrUpdateRequest srUpdateRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=update_my_location")
    Call<LocationResponse> locationUpdate(@Body LocationRequest locationRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=banner")
    Call<BannerResponse> bannerCheck(@Body BannerRequest bannerRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=client_fll_rules")
    Call<RulesResponse> getClientRules(@Body RulesRequest rulesRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=validate_otp")
    Call<OTPResponse> otpRequestVerify(@Body OTPRequest otpRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=validate_ewaybill_with_doc_portal")
    Call<EwayBillValidateResponse> ewayBillValidate(@Body EwayBillValidateRequest ewayBillValidateRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=validate_ewaybill_with_doc_portal")
    Call<EwayBillValidateResponseNew> ewayBillValidate(@Body EwayBillValidateRequestNew ewayBillValidateRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=milomtr_update")
    Call<MilometerUpdateResponse> milometerUpdate(@Body MilometerUpdateRequest milometerUpdateRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=upload_senders_copy_edp")
    Call<EDPUploadResponse> edpUpload(@Body EDPUploadRequest edpUploadRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=connote_enquiry_short")
    Call<ConnoteShortInfoResponse> connoteShortInfo(@Body ConnoteShortInfoRequest connoteShortInfoRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inspection_done")
    Call<InspectionDoneResponse> inspectionDone(@Body InspectionDoneRequest inspectionDoneRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=negative_status_inspection")
    Call<InscanNegativeStatusResponse> inspectionNegativeStatus(@Body InspectionNegativeStatusRequest inspectionNegativeStatusRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inspection_failed")
    Call<InspectionNegSubmitResponse> inspectionNegativeSubmit(@Body InspectionNegSubmitRequest inspectionNegSubmitRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=punch_in_out")
    Call<PunchInResponse> punchInOut(@Body PunchInRequest punchInRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=check_punch_in_status_new")
    Call<PunchStatusResponse> punchStatus(@Body PunchStatusRequest punchStatusRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_vendor_list")
    Call<VendorListResponse> vendorList(@Body VendorListRequest vendorListRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_my_pickups")
    Call<PickupRGSAResponse> rgsaPickupList(@Body PickupRGSARequest pickupRGSARequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=rgsa_confirm_bagging")
    Call<RGSAPickupConfirmResponse> rgsaPickupConfirm(@Body RGSAPickupConfirmRequest rgsaPickupConfirmRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=rgsa_confirm_pickup")
    Call<RGSAPickupConfirmResponse> rgsaPickupHubConfirm(@Body RGSAConfirmHubRequest rgsaConfirmHubRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_my_bags")
    Call<GetMyBagResponse> getMyBagsList(@Body GetMyBagsRequest getMyBagsRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=get_qc_rules_new")
    Call<QCRulesResponse> getQcRules(@Body QCRulesRequest qcRulesRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=get_qc_items")
    Call<ItemListResponse> getItemsList(@Body ItemListRequest itemListRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=qc_rule_check")
    Call<QcRulesCheckResponse> qcCheckSubmit(@Body QcRulesCheckRequest qcRulesCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=qc_rule_update_new")
    Call<QcRulesUpdateResponse> qcUpdate(@Body QcRulesUpdateRequest qcRulesUpdateRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=qc_pass_fail_update")
    Call<QCPassFailResponse> qcNDCUpdate(@Body QCPassFailRequest qcPassFailRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=upload_pod")
    Call<PODUploadResponse> podUpload(@Body PODUploadRequest podUploadRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=bank_list")
    Call<BankListResponse> getBankList(@Body BankListRequest bankListRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=resend_payment_link")
    Call<PaymentLinkResponse> getPaymentLinkFromMessage(@Body PaymentLinkRequest paymentLinkRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_qr_code_link")
    Call<QRCodeLinkResponse> getQrPaymenyLink(@Body QRCodeLinkRequest qrCodeLinkRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=upload_cod_proof")
    Call<UploadCodProofResponse> uploadCodProofImage(@Body UploadCodProofRequest uploadCodProofRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=cod_cheque_received")
    Call<CodChequeReceivedResponse> codChequeReceived(@Body CodChequeReceivedRequest codChequeReceivedRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=client_paytype")
    Call<PayTypeListResponse> getPayTypeList(@Body PaytypeListRequest paytypeListRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=cod_collection_received")
    Call<CodChequeCollectionResponse> CodChequeCollection(@Body CodChequeCollectionRequest codChequeCollectionRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=cheque_collection_received")
    Call<CodChequeCollectionResponse> NewCodChequeCollection(@Body ChequeCollectedRequest chequeCollectedRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=validate_otp_on_ndc")
    Call<NDCOtpResponse> getNDCOtp(@Body NDCOtpRequest ndcOtpRequest);

    @Headers("Content-Type: application/json")
    @POST("call.php?action=is_call_masking_done")
    Call<CallMaskingStatusResponse> getCallMaskStatus(@Body CallMaskingStatusRequest callMaskingStatusRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_all_cash_list_new")
    Call<AllCashListResponse> getAllCashList(@Body AllCashListRequest allCashListRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_cash_list_by_date_new")
    Call<CashListByDateResponse> getAllCashListByDate(@Body CashListByDateRequest cashListByDateRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_cashier_list")
    Call<CashierListResponse> getCashierList(@Body CashierListRequest cashierListRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=handover_otp")
    Call<CashierOTPResponse> getOTPForCashier(@Body CashierOTPRequest cashierOTPRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=handover_submit_new")
    Call<HandoverSubmitResponse> cashHandoverSubmit(@Body HandoverSubmitRequest handoverSubmitRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_docket_list")
    Call<DocketListWithOriginDestResponse> getDocketListWithOriginDest(@Body DocketListWithOriginDestRequest docketListWithOriginDestRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_user_type_list")
    Call<UserTypeListResponse> getUserType(@Body UserTypeListRequest userTypeListRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_vehicle_type_list")
    Call<VehicleTypeResponse> getVehicleType(@Body VehicleTypeRequest vehicleTypeRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_hub_users_by_user_type")
    Call<HubUserListResponse> getHubUserList(@Body HubUserListRequest hubUserListRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_vehicles_by_hub_id")
    Call<VehiclesByHubResponse> getVehiclesByHubID(@Body VehiclesByHubRequest vehiclesByHubRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=create_fll_round")
    Call<RoundCreationResponse> roundCreation(@Body RoundCreationRequest roundCreationRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_get_item_details")
    Call<InscanGetItemDetailsResponse> inscanGetItemDetails(@Body InscanGetItemDetailsRequest inscanGetItemDetailsRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_docket_details_for_drs")
    Call<DRSPrepConnoteDetailsResponse> DRSPrepConDetails(@Body DRSPrepConnoteDetailsRequest drsPrepConnoteDetailsRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=assign_drs")
    Call<AssignDRSResponse> assignDRS(@Body AssignDRSRequest assignDRSRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=get_vehicle_types")
    Call<GetVehicleTypeTwoOrFourResponse> getTwoOrFourVehicleType(@Body GetVehicleTypeTwoOrFourRequest getVehicleTypeTwoOrFourRequest);

    @Headers("Content-Type: application/json")
    @POST("operation.php?action=inscan_actualvalue_new")
    Call<ActualValueCheckResponse> actualValueCheck(@Body ActualValueCheckRequest actualValueCheckRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=qc_rule_update")
    Call<QCTataResponse> qcTataUpate(@Body QCTataRequest qcTataRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=update_additional_claim")
    Call<AdditionalClaimResponse> additionalClaim(@Body AdditionalClaimRequest additionalClaimRequest);

    @Headers("Content-Type: application/json")
    @POST("prsdrs.php?action=validate_otp")
    Call<OTPResponse> otpValidationRequest(@Body OTPRequest otpRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=additional_claims_history")
    Call<AdditionalClaimHistoryResponse> additionalClaimHistory(@Body AdditionalClaimHistoryRequest additionalClaimHistoryRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=generate_cit_against_brn")
    Call<ConnoteGenerationResponse> connoteGeneration(@Body ConnoteGenerationRequest connoteGenerationRequest);

    @Headers("Content-Type: application/json")
    @POST("user.php?action=criticalog_services_type")
    Call<CriticalogServicesResponse> criticalogServices(@Body CriticalogServicesRequest criticalogServicesRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=get_consignee_by_name")
    Call<GetConsigneeByNameResponse> getConsigneeByName(@Body GetConsigneeByNameRequest getConsigneeByNameRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_dockets_by_bookingid")
    Call<GetDocketByBookingNumberResponse> getDoccketListByBokkingId(@Body GetDocketByBookingNumberRequest getDocketByBookingNumberRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=send_cit_attachments")
    Call<GetCITAttachmentResponse> sendCitAttachments(@Body GetCITAttachmentsRequest getCITAttachmentsRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=get_client_paytype")
    Call<GetClientPaytypeResponse> getPayType(@Body GetClientPaytypeRequest getCITAttachmentsRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_auto_boxes")
    Call<GetAutoLBHResponse> getAutoBoxType(@Body GetAutoLBHRequest getAutoLBHRequest);

    @Headers("Content-Type: application/json")
    @POST("client.php?action=validate_response_checklist")
    Call<ValidateChecklistYesNoResponse> validateChecklistYesNo(@Body ValidateCheclistYesNoRequest validateCheclistYesNoRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_urgent_shipment_drs")
    Call<NFODrsStatusListResponse> getNfoDrsStatusList(@Body NFODrsStatusListRequest nfoDrsStatusListRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=update_urgent_shipment_drs")
    Call<UpdateUrgentShipmentStatusResponse> updateUrgentShipmentStatus(@Body UpdateUrgentShipmentStatusRequest updateUrgentShipmentStatusRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=get_urgent_shipment_prs")
    Call<NFOPRSStatusListResponse> getNfoPRSStatusList(@Body NFOPRSStatusListRequest nfoprsStatusListRequest);

    @Headers("Content-Type: application/json")
    @POST("other.php?action=update_urgent_shipment_prs")
    Call<UpdateUrgentShipmentPRSResponse> updateUrgentShipmentPRSStatus(@Body UpdateUrgentShipmentPRSRequest updateUrgentShipmentPRSRequest);
}