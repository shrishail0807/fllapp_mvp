package com.criticalog.ecritica.MVPPickup.PickupCancel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PickupReasonsRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("product_status")
    @Expose
    private String productStatus;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("picktype")
    @Expose
    private String picktype;

    public String getPicktype() {
        return picktype;
    }

    public void setPicktype(String picktype) {
        this.picktype = picktype;
    }

    public String getBookig_id() {
        return bookig_id;
    }

    public void setBookig_id(String bookig_id) {
        this.bookig_id = bookig_id;
    }

    @SerializedName("booking_id")
    @Expose
    private String bookig_id;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }
}
