package com.criticalog.ecritica.MVPPickup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickupData {
    @SerializedName("details")
    @Expose
    private PickupDetails details;

    public PickupDetails getDetails() {
        return details;
    }

    public void setDetails(PickupDetails details) {
        this.details = details;
    }
}
