package com.criticalog.ecritica.MVPPickup;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Interface.CallMaskInterface;
import com.criticalog.ecritica.MVPBarcodeScan.BarcodeScanActivity;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSPresenterImpl;
import com.criticalog.ecritica.MVPDRS.GetDrsInteractorImpl;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPPickup.Adapter.PickupListAdapter;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class PickupActivity extends AppCompatActivity implements PickupContract.MainView, SearchView.OnQueryTextListener, CallMaskInterface {
    PickupContract.presenter pickupPresenter;
    SimpleArcDialog mProgressBar;
    CriticalogSharedPreferences criticalogSharedPreferences;
    private String user_id;
    PickupListAdapter pickupAdapter;
    RecyclerView recyclerviewPickup;
    private LinearLayoutManager mLayoutManager;
    ImageView xtv_backbutton;
    SearchView searchView;
    private TextView mPrsNo;
    TextView mUserId;

    SwipeRefreshLayout mSwipeRefreshLayout;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double latitude, longitude = 0.0;
    private String androidOS,versionName;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_prs);

        androidOS = Build.VERSION.RELEASE;
        versionName = BuildConfig.VERSION_NAME;

        StaticUtils.APP_VERSION=BuildConfig.VERSION_NAME;

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = criticalogSharedPreferences.getData("base_url_dynamic");


        StaticUtils.TOKEN=criticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=criticalogSharedPreferences.getData("distance_api_key");
        StaticUtils.distance_api_url=criticalogSharedPreferences.getData("distance_api_url");

        user_id = criticalogSharedPreferences.getData("userId");
        recyclerviewPickup = findViewById(R.id.recyclerviewPickup);
        xtv_backbutton = findViewById(R.id.xtv_backbutton);
        mUserId=findViewById(R.id.mUserId);
        mPrsNo = findViewById(R.id.mPrsNo);
        mSwipeRefreshLayout=findViewById(R.id.swipeRefreshLayout);

        mUserId.setText(user_id);
        searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);

        checkGpsEnabledOrNot();

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds


        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };


        getLocation();

        xtv_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        callPickupListAPI();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                callPickupListAPI();
            }
        });
    }
    public void checkGpsEnabledOrNot()
    {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {


            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }
    public void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }
    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults.length > 0 && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                } else {
                    showLocationDialog("");

                }
                break;
        }
    }
    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                    /*    dialog.cancel();
                        finish();*/
                        requestPermission();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();

    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(PickupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(PickupActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PickupActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(PickupActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    criticalogSharedPreferences.saveData("call_pick_lat",String.valueOf(latitude));
                    criticalogSharedPreferences.saveData("call_pick_long",String.valueOf(longitude));
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                      mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    public void callPickupListAPI()
    {
        PickupPostData pickupPostData = new PickupPostData();
        pickupPostData.setAction("pickuplist");
        pickupPostData.setToken(criticalogSharedPreferences.getData("token"));
        pickupPostData.setUserId(user_id);
        pickupPostData.setLatitude(String.valueOf(latitude));
        pickupPostData.setLongitude(String.valueOf(longitude));
        pickupPostData.setApp_ver_name(versionName);

        Log.e("LATLONG_PICK",String.valueOf(latitude));

        pickupPresenter = new PickupPresenterImpl(PickupActivity.this, new GetPickupnteractorImpl());
        pickupPresenter.onLoadPickupList(criticalogSharedPreferences.getData("token"), pickupPostData);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setDataToViews(PickupListResponse pickupListResponse) {


        mLayoutManager = new LinearLayoutManager(this);
        recyclerviewPickup.setLayoutManager(mLayoutManager);

        if (pickupListResponse.getStatus() == 200) {
            Toasty.warning(this, pickupListResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            String latitude = pickupListResponse.getData().getDetails().getLatitude().toString();
            String longitude = pickupListResponse.getData().getDetails().getLongitude().toString();
            String hub_id = pickupListResponse.getData().getDetails().getHub();
            criticalogSharedPreferences.saveData("start_lat", latitude);
            criticalogSharedPreferences.saveData("start_long", longitude);

            criticalogSharedPreferences.saveData("hub_id", hub_id);

            criticalogSharedPreferences.saveData("prs", pickupListResponse.getData().getDetails().getPrs());
            criticalogSharedPreferences.saveData("round", pickupListResponse.getData().getDetails().getRound());
            criticalogSharedPreferences.saveData("hub_type", pickupListResponse.getData().getDetails().getHubType());
            criticalogSharedPreferences.saveData("prs_id", pickupListResponse.getData().getDetails().getPrsId());

            mPrsNo.setText(pickupListResponse.getData().getDetails().getPrs() + "\n" + "Date :" + pickupListResponse.getData().getDetails().getPrsDate());
            ArrayList<String> listtofilter = new ArrayList();


            for (int i = 0; i < pickupListResponse.getData().getDetails().getClientDetails().size(); i++) {

                for (int j = 0; j < pickupListResponse.getData().getDetails().getClientDetails().get(i).getBookingDetails().size(); j++) {
                    listtofilter.add(pickupListResponse.getData().getDetails().getClientDetails().get(i).getBookingDetails().get(j).getBookingId());
                    listtofilter.add(pickupListResponse.getData().getDetails().getClientDetails().get(i).getBookingDetails().get(j).getConnoteNumber().toString());
                    listtofilter.add(pickupListResponse.getData().getDetails().getClientDetails().get(i).getClientCode().toString());
                    listtofilter.add(pickupListResponse.getData().getDetails().getClientDetails().get(i).getClientName().toString());
                }

            }

            pickupAdapter = new PickupListAdapter(this, pickupListResponse.getData().getDetails().getClientDetails(), user_id, listtofilter,this);
            recyclerviewPickup.setAdapter(pickupAdapter);
            pickupAdapter.notifyDataSetChanged();

        } else {
            Toasty.warning(this, pickupListResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            // Toast.makeText(PickupActivity.this,response.getString("message"),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(PickupActivity.this,R.string.re_try,Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (pickupAdapter != null) {
            pickupAdapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public void callMaskClick(int pos, String mobileNum, String drsNum, String taskId) {
        getLocation();
    }
}
