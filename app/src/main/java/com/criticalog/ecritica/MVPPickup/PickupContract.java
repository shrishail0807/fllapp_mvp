package com.criticalog.ecritica.MVPPickup;

import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;

public interface PickupContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onLoadPickupList(String token,PickupPostData pickupPostData);

        //void LoginRequestToServer(LoginPostData loginPostData);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToViews(PickupListResponse pickupListResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetPickupIntractor {

        interface OnFinishedListener {
            void onFinished(PickupListResponse pickupListResponse);
            void onFailure(Throwable t);
        }
        void pickupListSuccessful(PickupContract.GetPickupIntractor.OnFinishedListener onFinishedListener,String token, PickupPostData pickupPostData);
    }
}
