package com.criticalog.ecritica.MVPPickup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PickupDetails {
    @SerializedName("PRS")
    @Expose
    private String prs;
    @SerializedName("PRS_ID")
    @Expose
    private String prsId;
    @SerializedName("prs_date")
    @Expose
    private String prsDate;
    @SerializedName("Round")
    @Expose
    private String round;
    @SerializedName("Latitude")
    @Expose
    private Integer latitude;
    @SerializedName("Longitude")
    @Expose
    private Integer longitude;
    @SerializedName("Hub")
    @Expose
    private String hub;
    @SerializedName("hub_type")
    @Expose
    private String hubType;
    @SerializedName("client_details")
    @Expose
    private List<ClientDetail> clientDetails = null;

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getPrsDate() {
        return prsDate;
    }

    public void setPrsDate(String prsDate) {
        this.prsDate = prsDate;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getHubType() {
        return hubType;
    }

    public void setHubType(String hubType) {
        this.hubType = hubType;
    }

    public List<ClientDetail> getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(List<ClientDetail> clientDetails) {
        this.clientDetails = clientDetails;
    }
}
