package com.criticalog.ecritica.MVPPickup;

import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPickupnteractorImpl implements PickupContract.GetPickupIntractor {
    @Override
    public void pickupListSuccessful(OnFinishedListener onFinishedListener, String token, PickupPostData pickupPostData) {
        /** Create handle for the RetrofitInstance interface*/
        RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

        Call<PickupListResponse> pickupListResponseCall= service.pickupList(pickupPostData);

        pickupListResponseCall.enqueue(new Callback<PickupListResponse>() {
            @Override
            public void onResponse(Call<PickupListResponse> call, Response<PickupListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PickupListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

}
