package com.criticalog.ecritica.MVPPickup.PickupCancel;

import com.criticalog.ecritica.MVPPickup.HeaderData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PickupReasonsResponse {
    @SerializedName("header")
    @Expose
    private HeaderData header;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DatumReasons> data = null;

    public HeaderData getHeader() {
        return header;
    }

    public void setHeader(HeaderData header) {
        this.header = header;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumReasons> getData() {
        return data;
    }

    public void setData(List<DatumReasons> data) {
        this.data = data;
    }
   /* @SerializedName("header")
    @Expose
    private HeaderReasons header;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DatumReasons> data = null;

    public HeaderReasons getHeader() {
        return header;
    }

    public void setHeader(HeaderReasons header) {
        this.header = header;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumReasons> getData() {
        return data;
    }

    public void setData(List<DatumReasons> data) {
        this.data = data;
    }*/
}
