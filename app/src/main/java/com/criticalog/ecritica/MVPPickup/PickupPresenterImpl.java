package com.criticalog.ecritica.MVPPickup;

public class PickupPresenterImpl implements PickupContract.presenter, PickupContract.GetPickupIntractor.OnFinishedListener {
    private PickupContract.MainView mainView;
    private PickupContract.GetPickupIntractor getNoticeIntractor;

    public PickupPresenterImpl(PickupContract.MainView mainView, PickupContract.GetPickupIntractor getNoticeIntractor) {
        this.mainView = mainView;
        this.getNoticeIntractor = getNoticeIntractor;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onLoadPickupList(String token, PickupPostData pickupPostData) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getNoticeIntractor.pickupListSuccessful(this, token, pickupPostData);
    }

    @Override
    public void onFinished(PickupListResponse pickupListResponse) {
        if (mainView != null) {
            mainView.setDataToViews(pickupListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
