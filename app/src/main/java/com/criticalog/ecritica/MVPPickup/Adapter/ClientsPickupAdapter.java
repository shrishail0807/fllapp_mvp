package com.criticalog.ecritica.MVPPickup.Adapter;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.MVPBarcodeScan.BarcodeScanActivity;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPPickup.BookingDetail;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.MVPPickup.PickupCancel.DatumReasons;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel.PickupCancelRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel.PickupCancelResponse;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestClient1;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Rest.RestServices1;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.GpsUtils;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.CallMaskingStatusRequest;
import com.criticalog.ecritica.model.CallMaskingStatusResponse;
import com.criticalog.ecritica.model.CallRequest;
import com.criticalog.ecritica.model.CallResponse;
import com.criticalog.ecritica.model.NDCOtpRequest;
import com.criticalog.ecritica.model.NDCOtpResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.hanks.htextview.rainbow.RainbowTextView;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientsPickupAdapter extends RecyclerView.Adapter<ClientsPickupAdapter.ClientsViewHolder> implements Filterable {
    private Context context;
    List<BookingDetail> clients;
    private String prs, clientId;
    private String userId;
    private List<DatumReasons> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    String convertedString = "";

    private AlertDialog DialogFinish;
    private ClientsPickupAdapter.ClientsViewHolder mHolder;
    private CriticalogSharedPreferences criticalogSharedPreferences;

    private LocationRequest locationRequest;

    private FusedLocationProviderClient mFusedLocationClient;
    private boolean isGPS = false;
    private LocationCallback locationCallback;
    private double latitude = 0.0, longitude = 0.0;

    double start_lat, start_long;
    RestServices mRestServices;
    RestServices1 mRestServices1;

    private List<BookingDetail> mfilteredlist;
    String user_id, token;
    private ProgressDialog mProgressDialog;
    private String reasonCode;
    DatabaseHelper dbHelper;

    private int year, month, day;
    int mHour;
    int mMinute;
    private Dialog manualMobileNumberDialog, dialogCallAlert;
    Location gps_loc;
    Location network_loc;
    Location final_loc;

    public ClientsPickupAdapter(Context context, String userId, List<BookingDetail> clients) {
        this.context = context;
        this.clients = clients;
        this.userId = userId;
        this.mfilteredlist = clients;

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        mRestServices1 = RestClient1.getRetrofitInstance().create(RestServices1.class);

        token = criticalogSharedPreferences.getData("token");

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle("loading...");
        mProgressDialog.setCancelable(false);

        dbHelper = new DatabaseHelper(context);
        dbHelper.getWritableDatabase();
    }

    @NonNull
    @Override
    public ClientsPickupAdapter.ClientsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_client_items_mvp, parent, false);

        context = parent.getContext();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        new GpsUtils(context).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        Toast.makeText(context, "lat" + latitude + "long" + longitude, Toast.LENGTH_SHORT).show();

                        if (mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        return new ClientsPickupAdapter.ClientsViewHolder(itemView);
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener((Activity) context, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    start_lat = Double.parseDouble((criticalogSharedPreferences.getData("start_lat")));
                    start_long = Double.parseDouble((criticalogSharedPreferences.getData("start_long")));
                    double distance = 0;
                    if (criticalogSharedPreferences.getData("firstactivitydone").equalsIgnoreCase("yes")) {
                        distance = distance(criticalogSharedPreferences.getDoubleData("last_activity_lat"), criticalogSharedPreferences.getDoubleData("last_activity_long"), latitude, longitude, "k");
                        convertedString = converttorealnumber(distance);
                    } else {
                        distance = distance(criticalogSharedPreferences.getDoubleData("last_activity_lat"), criticalogSharedPreferences.getDoubleData("last_activity_long"), latitude, longitude, "k");
                        convertedString = converttorealnumber(distance);
                    }
                } else {
                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    @Override
    public void onBindViewHolder(@NonNull ClientsPickupAdapter.ClientsViewHolder holder, int position) {

        mHolder = holder;

        String bookingno = clients.get(position).getBookingId();

        holder.bookingno.setText(bookingno);


       /* if(!clients.get(position).getNumber_of_con().equalsIgnoreCase("0"))
        {
            holder.mConNoLabel.setText("No of Cons: ");
            holder.connote.setText(clients.get(position).getNumber_of_con().toString());
            criticalogSharedPreferences.saveData("cons_count",clients.get(position).getNumber_of_con().toString());
        }else {
            criticalogSharedPreferences.saveData("cons_count",clients.get(position).getNumber_of_con().toString());
            holder.connote.setText(clients.get(position).getConnoteNumber().toString());
        }*/
        holder.connote.setText(clients.get(position).getConnoteNumber().toString());
        String isTrial = String.valueOf(clients.get(position).getIs_trial());
        if (clients.get(position).getScannedAnyOneDocket().equalsIgnoreCase("1")) {
            holder.mClientCancel.setVisibility(View.INVISIBLE);
            holder.closeClientOnList.setVisibility(View.VISIBLE);
        } else {
            holder.mClientCancel.setVisibility(View.VISIBLE);
            holder.closeClientOnList.setVisibility(View.INVISIBLE);
        }
        if (isTrial.equalsIgnoreCase("1")) {
            holder.mTrialShipment.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load("https://www.ecritica.co/efreightlive/v2/images/trail-ship.png")
                    .resize(2200, 400)
                    .placeholder(R.drawable.brokenimage)
                    .into(holder.mTrialShipment);

        } else {
            holder.mTrialShipment.setImageBitmap(null);
        }
        if (clients.get(position).getAuto_assigned() != null) {
            if (clients.get(position).getAuto_assigned().equalsIgnoreCase("1")) {
                holder.mAutoAssignmentText.setVisibility(View.VISIBLE);
                holder.mAutoAssignmentText.setText("Auto Assignment");
            } else {
                holder.mAutoAssignmentText.setVisibility(View.GONE);
            }
        }

        holder.mNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String address = clients.get(position).getNavigationAddress();
                    String uri = "http://maps.google.co.in/maps?daddr=" + address;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    context.startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(context, "Google Map Not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.closeClientOnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clientFinish(position);

            }
        });
        holder.mManualEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                criticalogSharedPreferences.saveData("navigationStatus", "manual");
                criticalogSharedPreferences.saveData("scanNextDocket", "false");

                criticalogSharedPreferences.saveData("client_code", clients.get(position).getBookClientCode());
                criticalogSharedPreferences.saveIntData("booking_type", clients.get(position).getBookingType());
                criticalogSharedPreferences.saveData("srcPin", clients.get(position).getSourcePin());
                criticalogSharedPreferences.saveData("connote_number", clients.get(position).getConnoteNumber().toString());
                criticalogSharedPreferences.saveData("client_uniqueid", clients.get(position).getUniqueId());
                criticalogSharedPreferences.saveData("picktype", clients.get(position).getPicktype());
                criticalogSharedPreferences.saveData("bookingno", clients.get(position).getBookingId());
                criticalogSharedPreferences.saveData("pickup_regular_id", clients.get(position).getPickupRegularId());
                criticalogSharedPreferences.saveData("unique_id", clients.get(position).getUniqueId());
                criticalogSharedPreferences.saveData("open_pickup_flag", clients.get(position).getOpenPickUp());
                criticalogSharedPreferences.saveData("force_prs_check", clients.get(position).getForcePrsCheck());
                prs = criticalogSharedPreferences.getData("prs");
                criticalogSharedPreferences.saveData("force_prs_checklist_otp", clients.get(position).getForce_prs_checklist_otp());
                criticalogSharedPreferences.saveData("generate_cit", clients.get(position).getGenerate_cit());
                criticalogSharedPreferences.saveData("cnor_code", clients.get(position).getCnor_code());
                criticalogSharedPreferences.saveData("gatepass_upload", clients.get(position).getGatepass_upload());
                criticalogSharedPreferences.saveData("mobile_number_for_otp", clients.get(position).getConsigneeMobile());
                if (!clients.get(position).getNumber_of_con().equalsIgnoreCase("0")) {
                    criticalogSharedPreferences.saveData("cons_count", clients.get(position).getNumber_of_con().toString());
                } else {
                    criticalogSharedPreferences.saveData("cons_count", clients.get(position).getNumber_of_con().toString());
                }

                Intent intent = new Intent(context, BarcodeScanActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });

        holder.mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("scanner", "clicked" + position);
                criticalogSharedPreferences.saveData("navigationStatus", "");
                criticalogSharedPreferences.saveData("scanNextDocket", "false");

                criticalogSharedPreferences.saveData("client_code", clients.get(position).getBookClientCode());
                criticalogSharedPreferences.saveIntData("booking_type", clients.get(position).getBookingType());
                criticalogSharedPreferences.saveData("srcPin", clients.get(position).getSourcePin());
                criticalogSharedPreferences.saveData("connote_number", clients.get(position).getConnoteNumber().toString());
                criticalogSharedPreferences.saveData("client_uniqueid", clients.get(position).getUniqueId());
                criticalogSharedPreferences.saveData("picktype", clients.get(position).getPicktype());
                criticalogSharedPreferences.saveData("bookingno", clients.get(position).getBookingId());
                criticalogSharedPreferences.saveData("unique_id", clients.get(position).getUniqueId());
                criticalogSharedPreferences.saveData("pickup_regular_id", clients.get(position).getPickupRegularId());
                criticalogSharedPreferences.saveData("open_pickup_flag", clients.get(position).getOpenPickUp());
                criticalogSharedPreferences.saveData("force_prs_check", clients.get(position).getForcePrsCheck());
                prs = criticalogSharedPreferences.getData("prs");
                criticalogSharedPreferences.saveData("force_prs_checklist_otp", clients.get(position).getForce_prs_checklist_otp());
                criticalogSharedPreferences.saveData("generate_cit", clients.get(position).getGenerate_cit());
                criticalogSharedPreferences.saveData("gatepass_upload", clients.get(position).getGatepass_upload());
                criticalogSharedPreferences.saveData("cnor_code", clients.get(position).getCnor_code());
                criticalogSharedPreferences.saveData("mobile_number_for_otp", clients.get(position).getConsigneeMobile());

                Intent intent = new Intent(context, BarcodeScanActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                context.startActivity(intent);
            }
        });

        holder.mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNumber = clients.get(position).getConsigneeMobile();
                if (mobileNumber.equalsIgnoreCase("")) {
                    codProofDilaog(clients.get(position).getBookingId().toString(), clients.get(position).getUniqueId().toString(), "");
                } else {
                    criticalogSharedPreferences.saveData("mobile_number_for_otp", mobileNumber);
                    prs = criticalogSharedPreferences.getData("prs");
                    codProofDilaog(clients.get(position).getBookingId().toString(), clients.get(position).getUniqueId().toString(), mobileNumber);

                    String prs_id = criticalogSharedPreferences.getData("prs_id");
                }
            }
        });

        holder.mClientCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reasonList.clear();
                getLocation();
                prs = criticalogSharedPreferences.getData("prs");
                criticalogSharedPreferences.saveIntData("booking_type", clients.get(position).getBookingType());
                criticalogSharedPreferences.saveData("srcPin", clients.get(position).getSourcePin());
                criticalogSharedPreferences.saveData("connote_number", clients.get(position).getConnoteNumber().toString());
                criticalogSharedPreferences.saveData("client_uniqueid", clients.get(position).getUniqueId());
                criticalogSharedPreferences.saveData("picktype", clients.get(position).getPicktype());
                criticalogSharedPreferences.saveData("bookingno", clients.get(position).getBookingId());
                criticalogSharedPreferences.saveData("client_code", clients.get(position).getBookClientCode());
                criticalogSharedPreferences.saveData("mobile_num", clients.get(position).getConsigneeMobile());
                criticalogSharedPreferences.saveData("mobile_number_for_otp", clients.get(position).getConsigneeMobile());

                PickupReasonsRequest pickupReasonsRequest = new PickupReasonsRequest();
                pickupReasonsRequest.setAction("reasonscancel");
                pickupReasonsRequest.setUserId(criticalogSharedPreferences.getData("userId"));
                pickupReasonsRequest.setProductStatus("pickup");
                pickupReasonsRequest.setLongitude(String.valueOf(longitude));
                pickupReasonsRequest.setLatitude(String.valueOf(latitude));
                pickupReasonsRequest.setPicktype(clients.get(position).getPicktype());
                pickupReasonsRequest.setBookig_id(clients.get(position).getBookingId());

                mProgressDialog.show();

                Call<PickupReasonsResponse> pickupReasonsResponseCall = mRestServices.pickupCancelReasons(pickupReasonsRequest);
                pickupReasonsResponseCall.enqueue(new Callback<PickupReasonsResponse>() {
                    @Override
                    public void onResponse(Call<PickupReasonsResponse> call, Response<PickupReasonsResponse> response) {
                        mProgressDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 200) {

                                reasons = response.body().getData();

                                for (int i = 0; i < reasons.size(); i++) {

                                    reasonList.add(reasons.get(i).getReasondesc());
                                }

                                showCancelDialog();
                            } else {
                                mProgressDialog.dismiss();
                            }
                        } else {
                            mProgressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<PickupReasonsResponse> call, Throwable t) {
                        Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    }
                });
            }
        });

        if (clients.get(position).getScannedDocs() != null) {
            if (clients.get(position).getScannedDocs().size() > 0) {
                holder.connScannedSpinner.setVisibility(View.VISIBLE);
                holder.connote.setVisibility(View.GONE);
                // Create an ArrayAdapter object that will be used to display the items in the spinner
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, clients.get(position).getScannedDocs());
                // Set the ArrayAdapter object to the Spinner object using the setAdapter() method
                holder.connScannedSpinner.setAdapter(adapter);
            } else {
                holder.connScannedSpinner.setVisibility(View.GONE);
                holder.connote.setVisibility(View.VISIBLE);
                holder.connote.setText(clients.get(position).getConnoteNumber().toString());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mfilteredlist.size();
    }

    private void clientFinish(int position) {
        LayoutInflater finishDialog = LayoutInflater.from(context);
        final View finishDialogView = finishDialog.inflate(R.layout.finish_dialog, null);
        DialogFinish = new AlertDialog.Builder(context).create();
        DialogFinish.setView(finishDialogView);
        DialogFinish.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DialogFinish.show();
        DialogFinish.setCanceledOnTouchOutside(false);

        finishDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFinish.dismiss();
                mProgressDialog.show();
                CloseClientRequest closeClientRequest = new CloseClientRequest();
                closeClientRequest.setAction("closeclient_new");
                closeClientRequest.setClientCode(clients.get(position).getBookClientCode());
                closeClientRequest.setBookingId(clients.get(position).getBookingId());
                closeClientRequest.setPrs(criticalogSharedPreferences.getData("prs"));
                closeClientRequest.setLatitude(String.valueOf(latitude));
                closeClientRequest.setLongitude(String.valueOf(longitude));
                closeClientRequest.setPickupRegularId(clients.get(position).getPickupRegularId());
                closeClientRequest.setPrsId(criticalogSharedPreferences.getData("prs_id"));
                closeClientRequest.setUniqueId(clients.get(position).getUniqueId());
                closeClientRequest.setUserId(userId);
                Call<CloseClientResponse> closeClientResponseCall = mRestServices.closeClient(closeClientRequest);
                closeClientResponseCall.enqueue(new Callback<CloseClientResponse>() {
                    @Override
                    public void onResponse(Call<CloseClientResponse> call, Response<CloseClientResponse> response) {
                        mProgressDialog.dismiss();
                        if (response.body().getStatus() == 200) {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            ((Activity) context).finish();
                            Intent intent = new Intent(context, PickupActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CloseClientResponse> call, Throwable t) {
                        mProgressDialog.dismiss();
                    }
                });
                getLocation();
            }
        });
        finishDialogView.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFinish.dismiss();
            }
        });
    }

    public void callInitiate(String mobileNumber, String bookingId, String uniqueId) {
        prs = criticalogSharedPreferences.getData("prs");
        String prs_id = criticalogSharedPreferences.getData("prs_id");
        CallRequest callRequest = new CallRequest();
        callRequest.setAction("call_masking");
        callRequest.setUserId(criticalogSharedPreferences.getData("userId"));
        callRequest.setTo_number(mobileNumber);
        callRequest.setDrs("");
        callRequest.setConnote_num("");
        callRequest.setPrs_id(prs_id);
        callRequest.setBooking_id(bookingId);
        callRequest.setUnique_id(uniqueId);
        callRequest.setLatitude(criticalogSharedPreferences.getData("call_pick_lat"));
        callRequest.setLongitute(criticalogSharedPreferences.getData("call_pick_long"));
        callRequest.setPrs_no(prs);


        mProgressDialog.setTitle("Please wait we are connecting the Call...Receive the call it will connect to end user");
        mProgressDialog.show();
        Call<CallResponse> callResponseCall = mRestServices1.callInitiate(callRequest);
        callResponseCall.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.body()!=null)
                {
                    if (response.body().getStatus() == 200) {
                        final Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mProgressDialog.dismiss();
                                manualMobileNumberDialog.dismiss();
                                Log.e("FAILURE_SUCCESS", response.body().getMessage());
                                //Do something after 100ms
                            }
                        }, 2000);
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("FAILURE_CALL", response.body().getMessage());
                    }
                }else {
                    Toast.makeText(context,"Response Cannot be Null", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.e("FAILURE_CALL", t.toString());
            }
        });
    }

    public void otpDilaog(String bookingId, String uniqueId) {
        //Dialog Manual Entry

        EditText mMobileNumber;
        TextView mCall, mDialogText;
        TextView mDirectCall;
        ImageView mCanceDialog;
        manualMobileNumberDialog = new Dialog(context);
        manualMobileNumberDialog.setContentView(R.layout.manual_entry_mobiel_dialoge);
        mMobileNumber = manualMobileNumberDialog.findViewById(R.id.mMobileNumber);
        mCall = manualMobileNumberDialog.findViewById(R.id.mCall);
        mDialogText = manualMobileNumberDialog.findViewById(R.id.mDialogText);
        mDirectCall = manualMobileNumberDialog.findViewById(R.id.mDirectCall);
        mCanceDialog = manualMobileNumberDialog.findViewById(R.id.mCanceDialog);
        // manualMobileNumberDialog.setCancelable(false);
        manualMobileNumberDialog.setCanceledOnTouchOutside(false);
        mMobileNumber.setHint("Enter OTP");
        mDialogText.setText("Enter OTP");
        mCall.setText("Verify OTP");
        mDirectCall.setVisibility(View.GONE);
        manualMobileNumberDialog.show();

        Window window = manualMobileNumberDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        mCanceDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualMobileNumberDialog.dismiss();
            }
        });
        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = mMobileNumber.getText().toString();
                if (otp.equalsIgnoreCase("")) {
                    Toast.makeText(context, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
                } else if (otp.length() != 4) {
                    Toast.makeText(context, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
                } else {
                    callGenerateOTP(otp, "no");
                }
            }
        });
    }

    public void codProofDilaog(String bookingId, String uniqueId, String mobileNum) {
        //Dialog Manual Entry
        EditText mMobileNumber;
        TextView mCall, mDirectCall;
        ImageView mCanceDialog;
        manualMobileNumberDialog = new Dialog(context);
        manualMobileNumberDialog.setContentView(R.layout.manual_entry_mobiel_dialoge);
        mMobileNumber = manualMobileNumberDialog.findViewById(R.id.mMobileNumber);
        mCall = manualMobileNumberDialog.findViewById(R.id.mCall);
        mDirectCall = manualMobileNumberDialog.findViewById(R.id.mDirectCall);
        mCanceDialog = manualMobileNumberDialog.findViewById(R.id.mCanceDialog);
        manualMobileNumberDialog.setCanceledOnTouchOutside(false);
        manualMobileNumberDialog.show();

        Window window = manualMobileNumberDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        mCanceDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualMobileNumberDialog.dismiss();
            }
        });
        mDirectCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobileNum.equalsIgnoreCase("")) {
                    Toasty.warning(context, "Enter Mobile number and call manually!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    callInitiate(mobileNum, bookingId, uniqueId);
                }
            }
        });

        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNumber = mMobileNumber.getText().toString();
                if (mobileNumber.equalsIgnoreCase("")) {
                    Toasty.warning(context, R.string.enter_mobile_number, Toast.LENGTH_SHORT, true).show();
                } else if (mobileNumber.length() != 10) {
                    Toasty.warning(context, R.string.enter_mobile_number, Toast.LENGTH_SHORT, true).show();
                } else {
                    callInitiate(mobileNumber, bookingId, uniqueId);
                }
            }
        });
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mfilteredlist = clients;
                    Log.e("Client Adapter", mfilteredlist.toString());
                } else {
                    ArrayList<BookingDetail> filteredbookingList = new ArrayList<>();
                    Log.e("Client Adapter", "else");
                    try {
                        Log.e("Client Adapter", mfilteredlist.toString());
                        for (BookingDetail prsDetails : clients) {

                            String bookingno = prsDetails.getBookingId();
                            String connoteno = prsDetails.getConnoteNumber().toString();
                            if (bookingno.contains(charString) || connoteno.contains(charString)) {
                                filteredbookingList.add(prsDetails);
                            }
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    mfilteredlist = filteredbookingList;
                    Log.e("Client Adapter", "mfilteredlist :" + mfilteredlist.size());
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mfilteredlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mfilteredlist = (ArrayList<BookingDetail>) results.values;
                notifyDataSetChanged();

                Log.e("Client Adapter", "notifydatasetchanged :" + mfilteredlist.size());
            }

        };
    }

    public class ClientsViewHolder extends RecyclerView.ViewHolder {

        TextView bookingno, connote, closeClientOnList, mClientType, id, actualLimit, clientName, clientAddress, bookingid, connoteno,
                mConNoLabel;
        LinearLayout mClientView, mLayoutAddr, mTrialLayout;
        ImageView mClientCancel, mPlayButton, mManualEntry, mTrialShipment;
        private LinearLayout progresslayout;
        RainbowTextView mAutoAssignmentText;
        ImageButton mCall;
        ImageView mNavigation;
        Spinner connScannedSpinner;

        public ClientsViewHolder(View itemView) {
            super(itemView);

            bookingno = itemView.findViewById(R.id.bookingno);
            mClientCancel = itemView.findViewById(R.id.imageView3);
            mPlayButton = itemView.findViewById(R.id.imageView2);
            connote = itemView.findViewById(R.id.conn);
            mManualEntry = itemView.findViewById(R.id.mManualEntry);
            mAutoAssignmentText = itemView.findViewById(R.id.mAutoAssignmentText);
            mTrialShipment = itemView.findViewById(R.id.mTrialShipment);
            mTrialLayout = itemView.findViewById(R.id.mTrialLayout);
            mCall = itemView.findViewById(R.id.mCall);
            mConNoLabel = itemView.findViewById(R.id.mConNoLabel);
            mNavigation = itemView.findViewById(R.id.mNavigation);
            closeClientOnList = itemView.findViewById(R.id.mCloseClientOnList);
            connScannedSpinner = itemView.findViewById(R.id.connScannedSpinner);
        }
    }

    private void datePicker(String reasonCode, int which) {

        // Get Current Date
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        criticalogSharedPreferences.saveData("R_DATE_PICKUP", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                        tiemPicker((year + "-" + (monthOfYear + 1) + "-" + dayOfMonth), reasonCode, which);


                    }
                }, year, month, day);
        c.add(Calendar.DATE, 2);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        datePickerDialog.show();
    }

    private void tiemPicker(String date_time, String reasonCode, int which) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;
                        criticalogSharedPreferences.saveData("R_TIME_PICKUP", hourOfDay + ":" + minute);

                        prsCancelCall();
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void showCancelDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                        if (which < reasons.size()) {
                            reasonCode = reasons.get(which).getReasoncode();
                            String otpFlag = reasons.get(which).getOtpFlag();
                            String callFlag = reasons.get(which).getCallmaskFlag();

                            if (callFlag.equalsIgnoreCase("1") && otpFlag.equalsIgnoreCase("1")) {
                                checkCallMaskStatus("0");
                            } else if (callFlag.equalsIgnoreCase("1") && otpFlag.equalsIgnoreCase("0")) {
                                checkCallMaskStatus("1");

                            } else if (callFlag.equalsIgnoreCase("0") && otpFlag.equalsIgnoreCase("1")) {
                                callGenerateOTP("", "Yes");
                            } else {
                                if (reasonCode.equalsIgnoreCase("PPO")) {
                                    datePicker(reasonCode, which);
                                } else {
                                    criticalogSharedPreferences.saveData("R_DATE_PICKUP", "");
                                    criticalogSharedPreferences.saveData("R_TIME_PICKUP", "");
                                    prsCancelCall();
                                }
                            }
                        } else {
                            Toast.makeText(context, "Please refresh the reasons", Toast.LENGTH_SHORT).show();
                        }

                        return true; // allow selection
                    }
                })
                .show();
    }

    public void dialogCallAlert(String message) {
        //Dialog Manual Entry
        TextView mDialogText, mOkDialog;
        ImageView mRightMark;
        dialogCallAlert = new Dialog(context);
        dialogCallAlert.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = dialogCallAlert.findViewById(R.id.mOk);
        mDialogText = dialogCallAlert.findViewById(R.id.mDialogText);
        mRightMark = dialogCallAlert.findViewById(R.id.mRightMark);
        mRightMark.setVisibility(View.GONE);
        mDialogText.setText(message);
        dialogCallAlert.setCancelable(false);
        dialogCallAlert.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCallAlert.dismiss();

            }
        });
    }

    public void checkCallMaskStatus(String onlyCallMask) {
        CallMaskingStatusRequest callMaskingStatusRequest = new CallMaskingStatusRequest();
        callMaskingStatusRequest.setAction("is_call_masking_done");
        callMaskingStatusRequest.setUserId(userId);
        callMaskingStatusRequest.setBookingId(criticalogSharedPreferences.getData("bookingno"));
        callMaskingStatusRequest.setPrsNo(criticalogSharedPreferences.getData("prs"));

        Call<CallMaskingStatusResponse> callMaskingStatusResponseCall = mRestServices.getCallMaskStatus(callMaskingStatusRequest);

        mProgressDialog.show();
        callMaskingStatusResponseCall.enqueue(new Callback<CallMaskingStatusResponse>() {
            @Override
            public void onResponse(Call<CallMaskingStatusResponse> call, Response<CallMaskingStatusResponse> response) {
                mProgressDialog.dismiss();
                if (response.body().getStatus() == 200) {
                    if (onlyCallMask.equalsIgnoreCase("1")) {
                        prsCancelCall();
                    } else {
                        callGenerateOTP("", "Yes");
                    }
                } else {
                    dialogCallAlert(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CallMaskingStatusResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    public void callGenerateOTP(String otp, String otpSendFlag) {
        NDCOtpRequest ndcOtpRequest = new NDCOtpRequest();
        ndcOtpRequest.setAction("validate_otp_on_ndc");
        ndcOtpRequest.setOtp(otp);
        ndcOtpRequest.setBookingNo(criticalogSharedPreferences.getData("bookingno"));
        ndcOtpRequest.setClientCode(criticalogSharedPreferences.getData("client_code"));
        ndcOtpRequest.setSendOtp(otpSendFlag);
        ndcOtpRequest.setConsignee("");
        ndcOtpRequest.setUserId(userId);
        ndcOtpRequest.setMobile(criticalogSharedPreferences.getData("mobile_num"));
        ndcOtpRequest.setPrsDrsNo(criticalogSharedPreferences.getData("prs"));
        ndcOtpRequest.setRoundNo(criticalogSharedPreferences.getData("ROUND_NO"));
        ndcOtpRequest.setFor_type("prs");
        mProgressDialog.show();
        Call<NDCOtpResponse> ndcOtpResponseCall = mRestServices.getNDCOtp(ndcOtpRequest);
        ndcOtpResponseCall.enqueue(new Callback<NDCOtpResponse>() {
            @Override
            public void onResponse(Call<NDCOtpResponse> call, Response<NDCOtpResponse> response) {
                mProgressDialog.dismiss();
                if (response.body().getStatus() == 200) {
                    if (otpSendFlag.equalsIgnoreCase("Yes")) {
                        otpDilaog("", "");
                    } else {
                        if (manualMobileNumberDialog != null) {
                            if (manualMobileNumberDialog.isShowing()) {
                                manualMobileNumberDialog.dismiss();
                            }
                        }
                        prsCancelCall();
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NDCOtpResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    public void prsCancelCall() {
        PickupCancelRequest pickupCancelRequest = new PickupCancelRequest();
        pickupCancelRequest.setAction("pickupcancel_new");
        pickupCancelRequest.setClientcode(criticalogSharedPreferences.getData("client_code"));
        pickupCancelRequest.setPicktype(criticalogSharedPreferences.getData("picktype"));
        pickupCancelRequest.setBookingId(criticalogSharedPreferences.getData("bookingno"));
        pickupCancelRequest.setLatitude(String.valueOf(latitude));
        pickupCancelRequest.setLongitude(String.valueOf(latitude));
        pickupCancelRequest.setPrs(criticalogSharedPreferences.getData("prs"));
        pickupCancelRequest.setPrsId(criticalogSharedPreferences.getData("prs_id"));
        pickupCancelRequest.setReasoncode(reasonCode);
        pickupCancelRequest.setUniqueId(criticalogSharedPreferences.getData("client_uniqueid"));
        pickupCancelRequest.setUserId(criticalogSharedPreferences.getData("userId"));
        pickupCancelRequest.setScheduleDate(criticalogSharedPreferences.getData("R_DATE_PICKUP"));
        pickupCancelRequest.setScheduleTime(criticalogSharedPreferences.getData("R_TIME_PICKUP"));

        mProgressDialog.show();
        Call<PickupCancelResponse> pickupCancelResponseCall = mRestServices.pickupCancel(pickupCancelRequest);

        pickupCancelResponseCall.enqueue(new Callback<PickupCancelResponse>() {
            @Override
            public void onResponse(Call<PickupCancelResponse> call, Response<PickupCancelResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //   Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Toasty.success(context, response.body().getMessage(), Toast.LENGTH_LONG, true).show();
                        Date c = Calendar.getInstance().getTime();
                        System.out.println("Current time => " + c);

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String date = df.format(c);

                        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
                        String time = df1.format(c);

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<PodClaimPojo> latlonglist = dbHelper.getAllLatLongList();

                                criticalogSharedPreferences.saveData("pickup_open", "pickup");

                                if (latlonglist.size() > 0) {
                                    int listsize = latlonglist.size();
                                    PodClaimPojo podClaimPojo;
                                    if (latlonglist.get(listsize - 1).getEdn_lat() == 0 || latlonglist.get(listsize - 1).getEnd_long() == 0 || latitude == 0 || longitude == 0) {
                                        podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "0", "0", date, time, "1", criticalogSharedPreferences.getData("client_code"), "prs cancel", c.toString(), 200);
                                    } else {
                                        double distnce = distanceCalc(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                                        String convertedstring = converttorealnumber(distnce);
                                        String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                                        if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                            convertedstring_g = convertedstring;
                                        }

                                        podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", criticalogSharedPreferences.getData("client_code"), "prs cancel", c.toString(), 200);

                                    }
                                    dbHelper.insertLatLOngDetails(podClaimPojo);

                                } else {

                                    PodClaimPojo podClaimPojo = null;
                                    if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {

                                        String lastLatLongList = dbHelper.getLastLatLongDetails();
                                        if (!lastLatLongList.equals("-")) {
                                            String[] bits = lastLatLongList.split("-");
                                            String lastOne = bits[bits.length - 1];
                                            double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                            String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);

                                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                                convertedstring_g = String.valueOf(distnce);
                                            }
                                            podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, String.valueOf(distnce), convertedstring_g, date, time, "1", criticalogSharedPreferences.getData("client_code"), "prs cancel", c.toString(), 200);
                                        }

                                    } else {
                                        String lastLatLongList = dbHelper.getLastLatLongDetails();
                                        if (!lastLatLongList.equals("-")) {
                                            String[] bits = lastLatLongList.split("-");
                                            String lastOne = bits[bits.length - 1];

                                            double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);

                                            String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);

                                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                                convertedstring_g = String.valueOf(distnce);
                                            }
                                            podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, String.valueOf(distnce), convertedstring_g, date, time, "1", criticalogSharedPreferences.getData("client_code"), "prs cancel", c.toString(), 200);
                                        }

                                    }

                                    latlonglist.add(podClaimPojo);
                                    Log.d("obj", "prs" + podClaimPojo);
                                    dbHelper.insertLatLOngDetails(podClaimPojo);

                                    criticalogSharedPreferences.saveData("start_date", date);
                                    criticalogSharedPreferences.saveData("start_time", time);

                                }
                            }
                        });
                        userRiderLog();
                        thread.start();

                        //  userRiderLog();
                        criticalogSharedPreferences.saveDoubleData("last_activity_lat", latitude);
                        criticalogSharedPreferences.saveDoubleData("last_activity_long", longitude);

                        Intent newIntent = new Intent(context, PickupActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(newIntent);
                    } else {

                        Toasty.warning(context, response.body().getMessage(), Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PickupCancelResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public String getDistanceFromGoogleAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        String distnace = "0";

        float distanceInKm = 0;
        String nextBillionHitStatus = criticalogSharedPreferences.getData("distance_api_status");

        if (nextBillionHitStatus.equalsIgnoreCase("1")) {

            distnace = criticalogSharedPreferences.getDistanceFromGoogelBillionAPI(lat1, lon1, lat2, lon2);

            distanceInKm = convertMeterToKilometer(Float.valueOf(distnace));
        } else {
            distanceInKm = 0;
        }

        return String.valueOf(distanceInKm);

    }

    public static float convertMeterToKilometer(float meter) {
        return (float) (meter * 0.001);
    }

    private double distanceCalc(double lat1, double lon1, double lat2, double lon2) {
        double distance;
        double distance1;
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(lon1);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(lon2);

        distance = startPoint.distanceTo(endPoint);

        distance1 = distance / 1000;

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        System.out.println(numberFormat.format(distance1));

        return Double.parseDouble(numberFormat.format(distance1));
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist / 0.62137;

        return dist;
    }

    private String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private boolean checktaskalreadyadded(ArrayList<PodClaimPojo> latlonglist, String docket) {
        boolean status = false;
        for (int i = 0; i < latlonglist.size(); i++) {
            PodClaimPojo podClaimPojo = latlonglist.get(i);
            if (podClaimPojo.getTaskid().equalsIgnoreCase(docket)) {
                if (podClaimPojo.getResponsecode() == 200)
                    status = true;
            }

        }
        return status;
    }

    public void userRiderLog() {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;
        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo(criticalogSharedPreferences.getData("bookingno"));
        riderLogRequest.setRoundId(criticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId(criticalogSharedPreferences.getData("client_code"));
        riderLogRequest.setDeviceId(criticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(criticalogSharedPreferences.getData("connote_number"));
        riderLogRequest.setHubId(criticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("2");
        riderLogRequest.setmLastKm("");
        riderLogRequest.setmStartStopKm(convertedString);
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("2");
        riderLogRequest.setActionNumber(2);
        riderLogRequest.setOsVersion(androidOS);

        dbHelper.insertLastLatLong(userId, String.valueOf(latitude), String.valueOf(longitude));

        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }
}
