package com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeaderCancel {
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("termsofService")
    @Expose
    private String termsofService;
    @SerializedName("generator")
    @Expose
    private String generator;
    @SerializedName("policycode")
    @Expose
    private String policycode;
    @SerializedName("created_time")
    @Expose
    private String createdTime;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTermsofService() {
        return termsofService;
    }

    public void setTermsofService(String termsofService) {
        this.termsofService = termsofService;
    }

    public String getGenerator() {
        return generator;
    }

    public void setGenerator(String generator) {
        this.generator = generator;
    }

    public String getPolicycode() {
        return policycode;
    }

    public void setPolicycode(String policycode) {
        this.policycode = policycode;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
