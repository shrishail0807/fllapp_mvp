package com.criticalog.ecritica.MVPPickup.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.CallMaskInterface;
import com.criticalog.ecritica.MVPDRS.DRSModel.DataDRS;
import com.criticalog.ecritica.MVPPickup.ClientDetail;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;


import java.util.ArrayList;
import java.util.List;

public class PickupListAdapter extends RecyclerView.Adapter<PickupListAdapter.PickupViewHolder> implements Filterable {

    private List<ClientDetail> drop_row;
    LinearLayout xdropdownLinearlayout;
    private List<ClientDetail> mfilteredlist = new ArrayList<>();
    PickupListAdapter.PickupViewHolder mHolder = null;

    private Context context;

    private String user_id;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private int count = 0;

    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;
    private List<String> listtofilter = new ArrayList<>();

    private CallMaskInterface callMaskInterface;

    public PickupListAdapter(Context mcontext, List<ClientDetail> drop_row, String userId, List<String> listtofilter,CallMaskInterface callMaskInterface) {
        this.drop_row = drop_row;
        this.mfilteredlist = drop_row;
        this.context = mcontext;
        this.user_id = userId;
        this.listtofilter = listtofilter;
        this.callMaskInterface=callMaskInterface;
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);


    }

    @Override
    public PickupListAdapter.PickupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pickup_new_row, parent, false);
        return new PickupListAdapter.PickupViewHolder(itemView);

    }

    @SuppressLint({"ResourceAsColor", "RecyclerView"})
    @Override
    public void onBindViewHolder(final PickupViewHolder holder, int position) {

        mHolder = holder;
        final ClientDetail client = mfilteredlist.get(position);
        holder.clientcode.setText(client.getClientCode());
        holder.location.setText(client.getLocation());
        final boolean isExpanded = position == mExpandedPosition;
        holder.xRvClients.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.mdropdown_icon.setActivated(isExpanded);

        if (isExpanded) {
            previousExpandedPosition = position;
            holder.mdropdown_icon.setImageResource(R.drawable.expand_less_black_24dp);
        } else {
            holder.mdropdown_icon.setImageResource(R.drawable.expand_more_black_24dp);
        }

        holder.mMainPickupLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   criticalogSharedPreferences.saveData("mobile_num",client.getBookingDetails().get(position).getConsigneeMobile());
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);
                callMaskInterface.callMaskClick(position,"","","");
/*
                holder.xRvClients.setLayoutManager(new LinearLayoutManager(context));
                holder.xRvClients.setAdapter(new ClientsPickupAdapter(context, user_id, client.getBookingDetails()));
                holder.clientname.setText(client.getClientName());*/
                criticalogSharedPreferences.saveData("actualValLimit", client.getActualLimit());
                criticalogSharedPreferences.saveData("clientCode", client.getClientCode());
                criticalogSharedPreferences.saveData("client_name", client.getClientName());
                criticalogSharedPreferences.saveData("prs", client.getPrs());
                criticalogSharedPreferences.saveData("prs_id", client.getPrsId());
                criticalogSharedPreferences.saveData("auto_box",client.getAuto_box());


                //Toast.makeText(context, criticalogSharedPreferences.getData("prs"), Toast.LENGTH_SHORT).show();
            }
        });

        holder.mdropdown_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);
                callMaskInterface.callMaskClick(position,"","","");

                criticalogSharedPreferences.saveData("actualValLimit", client.getActualLimit());
                criticalogSharedPreferences.saveData("clientCode", client.getClientCode());
                criticalogSharedPreferences.saveData("client_name", client.getClientName());
                criticalogSharedPreferences.saveData("prs", client.getPrs());
                criticalogSharedPreferences.saveData("prs_id", client.getPrsId());
                criticalogSharedPreferences.saveData("auto_box",client.getAuto_box());


              //  Toast.makeText(context, criticalogSharedPreferences.getData("prs"), Toast.LENGTH_SHORT).show();

            }
        });
        holder.xRvClients.setLayoutManager(new LinearLayoutManager(context));
        holder.xRvClients.setAdapter(new ClientsPickupAdapter(context, user_id, client.getBookingDetails()));
        holder.clientname.setText(client.getClientName());
      /*  criticalogSharedPreferences.saveData("actualValLimit", client.getActualLimit());
        criticalogSharedPreferences.saveData("clientCode", client.getClientCode());
        criticalogSharedPreferences.saveData("client_name", client.getClientName());
        criticalogSharedPreferences.saveData("prs", client.getPrs());
        criticalogSharedPreferences.saveData("prs_id", client.getPrsId());

        Toast.makeText(context, criticalogSharedPreferences.getData("prs"), Toast.LENGTH_SHORT).show();*/

    }

    @Override
    public int getItemCount() {
        //  Log.e(TAG,""+mfilteredlist.size());
        return mfilteredlist.size();
    }


    @Override
    public Filter getFilter() {

        Log.d("inside pickupadapter", "filter");
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mfilteredlist = drop_row;
                } else {
                    List<ClientDetail> filteredList = new ArrayList<>();
                    for (ClientDetail row : drop_row) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getClientCode().toLowerCase().contains(charString.toLowerCase()) || row.getClientName().toLowerCase().contains(charString.toLowerCase()) || row.getClientName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mfilteredlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mfilteredlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mfilteredlist = (ArrayList<ClientDetail>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };

    }

    public class PickupViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView clientcode, location, clientname;
        private ImageView mdropdown_icon;
        private LinearLayout mdropdown_linearlayout;
        private RelativeLayout mdroprow;
        public RecyclerView xRvClients;
        ConstraintLayout mMainPickupLay;

        @SuppressLint("ResourceAsColor")
        public PickupViewHolder(View itemView) {
            super(itemView);

            clientcode = (TextView) itemView.findViewById(R.id.clientcode);
            mdropdown_icon = (ImageView) itemView.findViewById(R.id.x_dropdown_icon);
            location = (TextView) itemView.findViewById(R.id.location);
            clientname = itemView.findViewById(R.id.clientname);
            mMainPickupLay=itemView.findViewById(R.id.mMainPickupLay);
            // mdropdown_linearlayout = (LinearLayout) itemView.findViewById(R.id.xdropdown_linearlayout);
            xRvClients = (RecyclerView) itemView.findViewById(R.id.xRvClients);

            mdropdown_icon.setOnClickListener(this);

        }


        @SuppressLint("ResourceAsColor")
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.x_dropdown_icon:

                    int pos = getAdapterPosition();
                    Log.d("position", "clicked" + pos);

                    if (xRvClients.isShown()) {
                        Log.d("isshown", "" + xRvClients.isShown());
                        xRvClients.setVisibility(View.GONE);
                        mdropdown_icon.setImageResource(R.drawable.expand_more_black_24dp);
                    } else {
                        xRvClients.setVisibility(View.VISIBLE);
                        mdropdown_icon.setImageResource(R.drawable.expand_less_black_24dp);
                    }
                    break;

            }
        }
    }
}
