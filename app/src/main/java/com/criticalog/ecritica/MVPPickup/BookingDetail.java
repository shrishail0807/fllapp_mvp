package com.criticalog.ecritica.MVPPickup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingDetail {

    @SerializedName("auto_assigned")
    @Expose
    private String auto_assigned;

    public String getAuto_assigned() {
        return auto_assigned;
    }

    public void setAuto_assigned(String auto_assigned) {
        this.auto_assigned = auto_assigned;
    }

    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("booking_type")
    @Expose
    private Integer bookingType;
    @SerializedName("picktype")
    @Expose
    private String picktype;
    @SerializedName("book_client_code")
    @Expose
    private String bookClientCode;
    @SerializedName("pickup_regular_id")
    @Expose
    private String pickupRegularId;
    @SerializedName("source_pin")
    @Expose
    private String sourcePin;
    @SerializedName("connote_number")
    @Expose
    private Integer connoteNumber;
    @SerializedName("open_pick_up")
    @Expose
    private String openPickUp;
    @SerializedName("force_prs_check")
    @Expose
    private String forcePrsCheck;
  /*  @SerializedName("scanned")
    @Expose
    private Integer scanned;*/
    @SerializedName("isNFO")
    @Expose
    private Integer isNFO;
    @SerializedName("is_trial")
    @Expose
    private Integer is_trial;

    @SerializedName("consigneeMobile")
    @Expose
    private String consigneeMobile;

    @SerializedName("navigationAddress")
    @Expose
    private String navigationAddress;

    @SerializedName("force_prs_checklist_otp")
    @Expose
    private String force_prs_checklist_otp;

    @SerializedName("generate_cit")
    @Expose
    private String generate_cit;

    @SerializedName("cnor_code")
    @Expose
    private String cnor_code;

    @SerializedName("scanned")
    @Expose
    private String scannedAnyOneDocket;

    @SerializedName("gatepass_upload")
    @Expose
    private String gatepass_upload;

    public String getGatepass_upload() {
        return gatepass_upload;
    }

    public void setGatepass_upload(String gatepass_upload) {
        this.gatepass_upload = gatepass_upload;
    }

    @SerializedName("scanned_docs")
    @Expose
    private List<String> scannedDocs;

    public List<String> getScannedDocs() {
        return scannedDocs;
    }

    public void setScannedDocs(List<String> scannedDocs) {
        this.scannedDocs = scannedDocs;
    }

    public String getScannedAnyOneDocket() {
        return scannedAnyOneDocket;
    }

    public void setScannedAnyOneDocket(String scannedAnyOneDocket) {
        this.scannedAnyOneDocket = scannedAnyOneDocket;
    }

    public String getCnor_code() {
        return cnor_code;
    }

    public void setCnor_code(String cnor_code) {
        this.cnor_code = cnor_code;
    }

    public String getGenerate_cit() {
        return generate_cit;
    }

    public void setGenerate_cit(String generate_cit) {
        this.generate_cit = generate_cit;
    }

    public String getForce_prs_checklist_otp() {
        return force_prs_checklist_otp;
    }

    public void setForce_prs_checklist_otp(String force_prs_checklist_otp) {
        this.force_prs_checklist_otp = force_prs_checklist_otp;
    }

    public String getNavigationAddress() {
        return navigationAddress;
    }

    public void setNavigationAddress(String navigationAddress) {
        this.navigationAddress = navigationAddress;
    }

    public String getNumber_of_con() {
        return number_of_con;
    }

    public void setNumber_of_con(String number_of_con) {
        this.number_of_con = number_of_con;
    }

    @SerializedName("number_of_con")
    @Expose
    private String number_of_con;


    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public Integer getIs_trial() {
        return is_trial;
    }

    public void setIs_trial(Integer is_trial) {
        this.is_trial = is_trial;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getBookingType() {
        return bookingType;
    }

    public void setBookingType(Integer bookingType) {
        this.bookingType = bookingType;
    }

    public String getPicktype() {
        return picktype;
    }

    public void setPicktype(String picktype) {
        this.picktype = picktype;
    }

    public String getBookClientCode() {
        return bookClientCode;
    }

    public void setBookClientCode(String bookClientCode) {
        this.bookClientCode = bookClientCode;
    }

    public String getPickupRegularId() {
        return pickupRegularId;
    }

    public void setPickupRegularId(String pickupRegularId) {
        this.pickupRegularId = pickupRegularId;
    }

    public String getSourcePin() {
        return sourcePin;
    }

    public void setSourcePin(String sourcePin) {
        this.sourcePin = sourcePin;
    }

    public Integer getConnoteNumber() {
        return connoteNumber;
    }

    public void setConnoteNumber(Integer connoteNumber) {
        this.connoteNumber = connoteNumber;
    }

    public String getOpenPickUp() {
        return openPickUp;
    }

    public void setOpenPickUp(String openPickUp) {
        this.openPickUp = openPickUp;
    }

    public String getForcePrsCheck() {
        return forcePrsCheck;
    }

    public void setForcePrsCheck(String forcePrsCheck) {
        this.forcePrsCheck = forcePrsCheck;
    }

   /* public Integer getScanned() {
        return scanned;
    }

    public void setScanned(Integer scanned) {
        this.scanned = scanned;
    }*/

    public Integer getIsNFO() {
        return isNFO;
    }

    public void setIsNFO(Integer isNFO) {
        this.isNFO = isNFO;
    }

    /*@SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("booking_type")
    @Expose
    private Integer bookingType;
    @SerializedName("picktype")
    @Expose
    private String picktype;
    @SerializedName("book_client_code")
    @Expose
    private String bookClientCode;
    @SerializedName("pickup_regular_id")
    @Expose
    private String pickupRegularId;
    @SerializedName("source_pin")
    @Expose
    private String sourcePin;
    @SerializedName("connote_number")
    @Expose
    private Integer connoteNumber;
    @SerializedName("open_pick_up")
    @Expose
    private String openPickUp;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getBookingType() {
        return bookingType;
    }

    public void setBookingType(Integer bookingType) {
        this.bookingType = bookingType;
    }

    public String getPicktype() {
        return picktype;
    }

    public void setPicktype(String picktype) {
        this.picktype = picktype;
    }

    public String getBookClientCode() {
        return bookClientCode;
    }

    public void setBookClientCode(String bookClientCode) {
        this.bookClientCode = bookClientCode;
    }

    public String getPickupRegularId() {
        return pickupRegularId;
    }

    public void setPickupRegularId(String pickupRegularId) {
        this.pickupRegularId = pickupRegularId;
    }

    public String getSourcePin() {
        return sourcePin;
    }

    public void setSourcePin(String sourcePin) {
        this.sourcePin = sourcePin;
    }

    public Integer getConnoteNumber() {
        return connoteNumber;
    }

    public void setConnoteNumber(Integer connoteNumber) {
        this.connoteNumber = connoteNumber;
    }

    public String getOpenPickUp() {
        return openPickUp;
    }

    public void setOpenPickUp(String openPickUp) {
        this.openPickUp = openPickUp;
    }*/

}
