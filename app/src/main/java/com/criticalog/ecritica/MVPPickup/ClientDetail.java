package com.criticalog.ecritica.MVPPickup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClientDetail {
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("actual_limit")
    @Expose
    private String actualLimit;
    @SerializedName("PRS")
    @Expose
    private String prs;
    @SerializedName("PRS_ID")
    @Expose
    private String prsId;

    public String getAuto_box() {
        return auto_box;
    }

    public void setAuto_box(String auto_box) {
        this.auto_box = auto_box;
    }

    @SerializedName("auto_box")
    @Expose
    private String auto_box;


    @SerializedName("booking_details")
    @Expose
    private List<BookingDetail> bookingDetails = null;



    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualLimit() {
        return actualLimit;
    }

    public void setActualLimit(String actualLimit) {
        this.actualLimit = actualLimit;
    }

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public List<BookingDetail> getBookingDetails() {
        return bookingDetails;
    }

    public void setBookingDetails(List<BookingDetail> bookingDetails) {
        this.bookingDetails = bookingDetails;
    }


}
