package com.criticalog.ecritica.MVPPickup.PickupCancel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DatumReasons implements Serializable {
    @SerializedName("reasondesc")
    @Expose
    private String reasondesc;
    @SerializedName("reasoncode")
    @Expose
    private String reasoncode;
    @SerializedName("OTP_FLAG")
    @Expose
    private String otpFlag;
    @SerializedName("CALLMASK_FLAG")
    @Expose
    private String callmaskFlag;

    public String getReasondesc() {
        return reasondesc;
    }

    public void setReasondesc(String reasondesc) {
        this.reasondesc = reasondesc;
    }

    public String getReasoncode() {
        return reasoncode;
    }

    public void setReasoncode(String reasoncode) {
        this.reasoncode = reasoncode;
    }

    public String getOtpFlag() {
        return otpFlag;
    }

    public void setOtpFlag(String otpFlag) {
        this.otpFlag = otpFlag;
    }

    public String getCallmaskFlag() {
        return callmaskFlag;
    }

    public void setCallmaskFlag(String callmaskFlag) {
        this.callmaskFlag = callmaskFlag;
    }

   /* @SerializedName("reasondesc")
    @Expose
    private String reasondesc;
    @SerializedName("reasoncode")
    @Expose
    private String reasoncode;

    public String getReasondesc() {
        return reasondesc;
    }

    public void setReasondesc(String reasondesc) {
        this.reasondesc = reasondesc;
    }

    public String getReasoncode() {
        return reasoncode;
    }

    public void setReasoncode(String reasoncode) {
        this.reasoncode = reasoncode;
    }*/
}
