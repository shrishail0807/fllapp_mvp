package com.criticalog.ecritica.VolleyClasses;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.IHrSuggestionWsCalled;
import com.criticalog.ecritica.Utils.StaticUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HrSuggestionVolley {

    private Context mContext;
    private IHrSuggestionWsCalled hrSuggestionWsCalled;
    private String userId,messagetype,subject,department,description;

    public HrSuggestionVolley(Context mContext, IHrSuggestionWsCalled hrSuggestionWsCalled, String userId, String messagetype, String subject, String department, String description) {
        this.mContext = mContext;
        this.hrSuggestionWsCalled = hrSuggestionWsCalled;
        this.userId = userId;
        this.messagetype = messagetype;
        this.subject = subject;
        this.department = department;
        this.description = description;
        sendRequest();
    }

    private void sendRequest() {

        Map jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id",userId);
        jsonParams.put("message_type",messagetype);
        jsonParams.put("subject",subject);
        jsonParams.put("dept",department);
        jsonParams.put("message",description);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, StaticUtils.HR_SUGGESTION,

                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Log.d("getting ","response" + response);
                            String status = response.getString("status");
                            String message = response.getString("message");
                            if (status.equals("success")) {
                                hrSuggestionWsCalled.hrSuggestionWsSuccessFailure(true,message);

                            } else {
                                Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();
                                hrSuggestionWsCalled.hrSuggestionWsSuccessFailure(false,message);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("error res",error.toString());
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (error instanceof NetworkError) {
                                    Toast.makeText(mContext,
                                            "Oops. Network error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof ServerError) {
                                    Toast.makeText(mContext,
                                            "Oops. Server error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof AuthFailureError) {
                                    Toast.makeText(mContext,
                                            "Oops. AuthFail error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof ParseError) {
                                    Toast.makeText(mContext,
                                            "Oops. Parse error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof NoConnectionError) {
                                    Toast.makeText(mContext,
                                            "Oops. NoConnection error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof TimeoutError) {
                                    Toast.makeText(mContext,
                                            "Oops. Timeout error!",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        /*RetryPolicy policy = new DefaultRetryPolicy(20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);*/
        postRequest.setRetryPolicy(MySingleton.getInstance(mContext).retryPolicy);
        MySingleton.getInstance(mContext).addToRequestQueue(postRequest);

    }
}
