package com.criticalog.ecritica.VolleyClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.IOtpVerify;
import com.criticalog.ecritica.Utils.StaticUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VerifyOtpVolley {

    private Context context;
    private String userid,taskid,refcount,otp,otp_mobile,resend,clientcode,round,consignee;
    IOtpVerify otpVerify;

    public VerifyOtpVolley(Context context, String userid, String tskid, String refcount, String otp, String otp_mobile, IOtpVerify otpVerify,String resend,String clientcode,String round,String consignee) {
        this.context = context;
        this.userid = userid;
        this.taskid = tskid;
        this.refcount = refcount;
        this.otp = otp;
        this.otp_mobile = otp_mobile;
        this.otpVerify = otpVerify;
        this.resend = resend;
        this.clientcode = clientcode;
        this.round = round;
        this.consignee = consignee;
        verifyotp();
    }

    private void verifyotp() {
        Map<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("otp_mobile", otp_mobile);
        jsonParams.put("otp", otp);
        jsonParams.put("is_resend", resend);
        jsonParams.put("client_code", clientcode);
        jsonParams.put("round", round);
        jsonParams.put("consignee",consignee);
        jsonParams.put("refcount",refcount);



        Log.e("verify otp","jsonparam"+jsonParams);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUtils.OTP_VALIDATE,
                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("otpverify","req"+jsonParams);

                        otpVerify.verifyOtp(response);
                        Log.i("res", response.toString());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                otpVerify.verifyOtp(null);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (error instanceof NetworkError) {
                            Toast.makeText(context,
                                    "Oops. Network error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context,
                                    "Oops. Server error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(context,
                                    "Oops. AuthFail error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(context,
                                    "Oops. Parse error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context,
                                    "Oops. NoConnection error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context,
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        request.setRetryPolicy(MySingleton.getInstance(context).retryPolicy);
        // requestQueue=CustomVolleyRequestQueue.getInstance(mcontext).getRequestQueue();
        MySingleton.getInstance(context).addToRequestQueue(request);

    }
}
