package com.criticalog.ecritica.VolleyClasses;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.ISrMessagetoDisplay;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.DocketDetails;
import com.criticalog.ecritica.model.DocketResponse;
import com.criticalog.ecritica.model.SrDetails;
import com.criticalog.ecritica.model.SrResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SrMessageVolley {
    private Context context;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private ISrMessagetoDisplay srMessagetoDisplay;
    private String connoteno;

    public SrMessageVolley(Context context,ISrMessagetoDisplay srMessagetoDisplay,String connoteno) {
        this.context = context;
        this.connoteno = connoteno ;
        this.srMessagetoDisplay = srMessagetoDisplay;
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        sendrequest();
    }

    private void sendrequest() {
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("task_id", connoteno);


        Log.e("srmessage req","jsonparam"+jsonParams);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUtils.SRMESSAGE,
                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson=new Gson();
                        SrResponse name=gson.fromJson(response.toString(),SrResponse.class);

                       srMessagetoDisplay.displaySrmessage(name);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                srMessagetoDisplay.displaySrmessage(null);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (error instanceof NetworkError) {
                            Toast.makeText(context,
                                    "Oops. Network error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context,
                                    "Oops. Server error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(context,
                                    "Oops. AuthFail error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(context,
                                    "Oops. Parse error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context,
                                    "Oops. NoConnection error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context,
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        request.setRetryPolicy(MySingleton.getInstance(context).retryPolicy);
        // requestQueue=CustomVolleyRequestQueue.getInstance(mcontext).getRequestQueue();
        MySingleton.getInstance(context).addToRequestQueue(request);
    }

}
