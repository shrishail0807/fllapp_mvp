package com.criticalog.ecritica.VolleyClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



public class GetFirebaseToken  {

    private Context mContext;
    private String userId, token;

    public GetFirebaseToken(Context mContext, String userId, String token) {
        this.mContext = mContext;
        this.userId = userId;
        this.token = token;


        getfirebasetoken();
    }

    private void getfirebasetoken() {

        Map<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", userId);
        jsonParams.put("token", token);

        Log.d("get token",jsonParams.toString());


        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, StaticUtils.GET_FIREBASE_TOKEN,

                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("firebase response",response.toString());
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Log.d("firebase token","successfully sent to server"+token);
                            } else {

                                Log.d("firebase token","response fail");

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (error instanceof NetworkError) {
                                    Toast.makeText(mContext,
                                            "Oops. Network error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof ServerError) {
                                    Toast.makeText(mContext,
                                            "Oops. Server error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof AuthFailureError) {
                                    Toast.makeText(mContext,
                                            "Oops. AuthFail error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof ParseError) {
                                    Toast.makeText(mContext,
                                            "Oops. Parse error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof NoConnectionError) {
                                    Toast.makeText(mContext,
                                            "Oops. NoConnection error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof TimeoutError) {
                                    Toast.makeText(mContext,
                                            "Oops. Timeout error!",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

       /* RetryPolicy policy = new DefaultRetryPolicy(20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);*/


        postRequest.setRetryPolicy(MySingleton.getInstance(mContext).retryPolicy);
        // postRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(mContext).addToRequestQueue(postRequest);

    }


}
