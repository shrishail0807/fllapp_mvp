package com.criticalog.ecritica.VolleyClasses;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.ILabelWsCalled;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.LabelPrint;
import com.criticalog.ecritica.model.Print;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//import com.example.criticalog.Utils.CriticalogSharedPreferences;

/**
 * Created by DELL1 on 6/27/2018.
 */

public class LabelPrintVolley {

    private Context mContext;
    private ILabelWsCalled labelWsCalled;
    private String userId, PRS, clientCode, slips, docketNo, source, dest,docket;

    private List<LabelPrint> mLabelList;
    private Print mPrint;
    private String pinCode;
    private String actualvalue;
    private JSONArray boxarray = null;
    public static int printCount = 1;
    private List boxList = new ArrayList<>();
    private boolean ischecked;



    // private String token;*/

    public LabelPrintVolley(Context mContext, ILabelWsCalled labelWsCalled, String userId, String prs, String clientCode, String slips, String docketNo, String pin_code, String actualvalue, boolean ischecked) {

        Log.d("inside","labelprint");
        this.mContext = mContext;
        this.labelWsCalled = labelWsCalled;
        this.userId = userId;
        this.PRS = prs;
        this.clientCode = clientCode;
        this.slips = slips;
        this.docketNo = docketNo;
        this.pinCode = pin_code;
        this.ischecked = ischecked;
        this.actualvalue = actualvalue;
        mLabelList = new ArrayList<>();


        getDetails();
    }

    public List<LabelPrint> getLabelDetails() {

        return mLabelList;
    }

    public List<Integer> getBoxList() {

        return boxList;
    }

    public Print getPrint() {

        return mPrint;
    }

    private void getDetails() {


        Map jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", userId);
        jsonParams.put("prs", PRS);
        jsonParams.put("client_code", clientCode);
        jsonParams.put("slips", slips);
        jsonParams.put("docket_no", docketNo);
        jsonParams.put("pincode",pinCode);
        jsonParams.put("actual_value",actualvalue);
        jsonParams.put("ischecked",ischecked);

        Log.d("label req",jsonParams.toString());


        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, StaticUtils.LABELPRINT_URL,

                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Log.d("label res",response.toString());


                            Log.d("getting ","response");
                            String status = response.getString("status");
                            if (status.equals("success")) {


                                labelWsCalled.LabelPrintWSSuccessFailure(true,response);


                            } else {
                                Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();
                                labelWsCalled.LabelPrintWSSuccessFailure(false,response);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "Something went wrong... JSON parsing" , Toast.LENGTH_SHORT).show();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (error instanceof NetworkError) {
                                    Toast.makeText(mContext,
                                            "Oops. Network error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof ServerError) {
                                    Toast.makeText(mContext,
                                            "Oops. Server error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof AuthFailureError) {
                                    Toast.makeText(mContext,
                                            "Oops. AuthFail error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof ParseError) {
                                    Toast.makeText(mContext,
                                            "Oops. Parse error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof NoConnectionError) {
                                    Toast.makeText(mContext,
                                            "Oops. NoConnection error!",
                                            Toast.LENGTH_LONG).show();
                                } else if (error instanceof TimeoutError) {
                                    Toast.makeText(mContext,
                                            "Oops. Timeout error!",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });


                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        /*RetryPolicy policy = new DefaultRetryPolicy(20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);*/
        postRequest.setRetryPolicy(MySingleton.getInstance(mContext).retryPolicy);
        MySingleton.getInstance(mContext).addToRequestQueue(postRequest);

    }

}
