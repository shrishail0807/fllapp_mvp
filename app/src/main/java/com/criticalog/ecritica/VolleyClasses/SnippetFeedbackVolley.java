package com.criticalog.ecritica.VolleyClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.ISnippetFeedback;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SnippetFeedbackVolley {

    ISnippetFeedback snippetFeedback;
    private Context context;
    private CriticalogSharedPreferences criticalogSharedPreferences;

    public SnippetFeedbackVolley(ISnippetFeedback snippetFeedback, Context context) {
        this.snippetFeedback = snippetFeedback;
        this.context = context;
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        sendFeedback();
    }

    private void sendFeedback() {

        Log.e("inside","feedbackvolley");
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", criticalogSharedPreferences.getData("userId"));
        jsonParams.put("feedback", String.valueOf(criticalogSharedPreferences.getIntData("snippet_rating")));
        jsonParams.put("row_id", String.valueOf(criticalogSharedPreferences.getIntData("snippet_row_id")));
        jsonParams.put("snippet_id",String.valueOf(criticalogSharedPreferences.getIntData("snippet_id_feedback")));

        Log.e("feedback snippet req",jsonParams.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUtils.FRRDBACK_SNIPPET,
                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            String  status = response.getString("status");
                            Log.e("flag",""+status);


                            snippetFeedback.snippetFeedbackSuccessFailure(status);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("res", response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
                Log.i("res", error.toString());

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (error instanceof NetworkError) {
                            Toast.makeText(context,
                                    "Oops. Network error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context,
                                    "Oops. Server error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(context,
                                    "Oops. AuthFail error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(context,
                                    "Oops. Parse error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context,
                                    "Oops. NoConnection error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context,
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        request.setRetryPolicy(MySingleton.getInstance(context).retryPolicy);
        // requestQueue=CustomVolleyRequestQueue.getInstance(mcontext).getRequestQueue();
        MySingleton.getInstance(context).addToRequestQueue(request);


    }

}
