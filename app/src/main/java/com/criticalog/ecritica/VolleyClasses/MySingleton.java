package com.criticalog.ecritica.VolleyClasses;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.criticalog.ecritica.ssl.TLSSocketFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLSocketFactory;

/**
 * Created by DELL1 on 6/29/2018.
 */

public class MySingleton {
    private static MySingleton minstance;
    private RequestQueue requestQueue;
    private static Context mcontext;
    public static final String PUBLIC_KEY = "30820122300d06092a864886f70d01010105000382010f003082010a0282010100baf08c2d2f2a75381c45013c1b4b6f0e750f87c70de4234edbc76329ec49eddec4f967e8c90ac533ac70d6b7e44c7fc9e39372c0b8c825390b3ce39ab6d244e803271ee26abaf7e53fcca347a91106d54db66c8c0887fa429b8abfa4688cfe93e4e000e511bd27a830505db78eaac0f70c5195a5ede9f1b76504e30a55da0efdc48a4ff0b4d0a811e41cc9f045ac5a9fb5e3cdf7e7f3ca1bbfbf0b1a301ebae7b21283c468f1d7417bfe447ebb67bf506f5a19cd60132d0e92ab54ccf4bfa51a7bdfb1bd9e2649ea70b501a4a32061afa0a10d959c0b8a6f4b060ab9d1d2f0c06365098fbd7c7065b557f9915174eacdc3acfed2a64f0b988ce502214b9450d30203010001";
    public MySingleton(Context context) {
        mcontext = context;
        requestQueue = getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
           // requestQueue = Volley.newRequestQueue(mcontext.getApplicationContext());
            requestQueue = Volley.newRequestQueue(mcontext, new HurlStack(null, pinnedSSLSocketFactory()));

        }
        return requestQueue;
    }

    public static synchronized MySingleton getInstance(Context context) {
        if (minstance == null) {
            minstance = new MySingleton(context);
        }
        return minstance;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        requestQueue.add(request);

    }

    public RetryPolicy retryPolicy = new DefaultRetryPolicy(30*1000,
            0,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            );


    private SSLSocketFactory pinnedSSLSocketFactory() {
        try {
            return new TLSSocketFactory(PUBLIC_KEY);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


}

