package com.criticalog.ecritica.VolleyClasses;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.IUpdateSr;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.SrResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SrUpdateVolley {

    private Context context;
    private IUpdateSr iUpdateSr;
    private String taskid,userid,comment;
    private CriticalogSharedPreferences criticalogSharedPreferences;

    public SrUpdateVolley(Context context, IUpdateSr iUpdateSr,String taskid,String userid,String comment) {
        this.context = context;
        this.iUpdateSr = iUpdateSr;
        this.taskid = taskid;
        this.userid = userid;
        this.comment = comment;
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        sendrequest();
    }

    private void sendrequest() {
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("task_id", taskid);
        jsonParams.put("user_id",userid);
        jsonParams.put("comment",comment);
        jsonParams.put("client_id",criticalogSharedPreferences.getData("clientcodeforsrupdate"));


        Log.e("update sr  req","jsonparam"+jsonParams);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUtils.SRUPDATE,
                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        iUpdateSr.updateSR(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                iUpdateSr.updateSR(null);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (error instanceof NetworkError) {
                            Toast.makeText(context,
                                    "Oops. Network error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context,
                                    "Oops. Server error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(context,
                                    "Oops. AuthFail error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(context,
                                    "Oops. Parse error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context,
                                    "Oops. NoConnection error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context,
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        request.setRetryPolicy(MySingleton.getInstance(context).retryPolicy);
        // requestQueue=CustomVolleyRequestQueue.getInstance(mcontext).getRequestQueue();
        MySingleton.getInstance(context).addToRequestQueue(request);
    }
}
