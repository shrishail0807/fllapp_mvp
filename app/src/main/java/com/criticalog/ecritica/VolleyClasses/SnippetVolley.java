package com.criticalog.ecritica.VolleyClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.criticalog.ecritica.Interface.SnippetWSCalled;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SnippetVolley {
    private Context context;
    SnippetWSCalled snippetWSCalled;

    private CriticalogSharedPreferences criticalogSharedPreferences;

    public SnippetVolley(Context context,SnippetWSCalled snippetWSCalled) {
        this.context = context;
        this.snippetWSCalled = snippetWSCalled;
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);

        getSnippet();
    }

    private void getSnippet() {

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", criticalogSharedPreferences.getData("userId"));
        jsonParams.put("snippet_id", String.valueOf(criticalogSharedPreferences.getIntData("snippet_id")));
        jsonParams.put("opt_type",criticalogSharedPreferences.getData("opt_typeId"));

        Log.e("snippetvolley","req"+jsonParams);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUtils.SNIPPET_URL,
                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                      //  progressDialog.dismiss();
                        Log.e("res", response.toString());
                        String status = null;
                        int type;
                        try {
                            status = response.getString("status");
                            type = response.getInt("type");
                            snippetWSCalled.snippetWsSuccessFailure(status,type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  progressDialog.dismiss();
                Log.i("res", error.toString());

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (error instanceof NetworkError) {
                            Toast.makeText(context,
                                    "Oops. Network error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context,
                                    "Oops. Server error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(context,
                                    "Oops. AuthFail error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(context,
                                    "Oops. Parse error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context,
                                    "Oops. NoConnection error!",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context,
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        request.setRetryPolicy(MySingleton.getInstance(context).retryPolicy);
        // requestQueue=CustomVolleyRequestQueue.getInstance(mcontext).getRequestQueue();
        MySingleton.getInstance(context).addToRequestQueue(request);


    }


}
