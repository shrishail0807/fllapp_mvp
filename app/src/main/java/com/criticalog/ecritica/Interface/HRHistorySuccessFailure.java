package com.criticalog.ecritica.Interface;


import org.json.JSONObject;

public interface HRHistorySuccessFailure {
     void hrhistorywscalled(String status, JSONObject response);
}
