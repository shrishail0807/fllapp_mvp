package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IdocketCheckWS {

    public void docketCheckSuccessFailure(JSONObject response);
}

