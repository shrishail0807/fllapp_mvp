package com.criticalog.ecritica.Interface;

import org.json.JSONException;
import org.json.JSONObject;

public interface IRoundChecksuccessFailure {
    void roundchecksuccessfailure(JSONObject response) throws JSONException;
}
