package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IInscandestPincodeWs {
    void inscandestpincodevalidation(JSONObject jsonObject);

}
