package com.criticalog.ecritica.Interface;


import org.json.JSONObject;

public interface IHrSuggestionWsCalled {

    public void hrSuggestionWsSuccessFailure(boolean status, String message);
}
