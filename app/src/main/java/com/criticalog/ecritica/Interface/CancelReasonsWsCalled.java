package com.criticalog.ecritica.Interface;

/**
 * Created by DELL1 on 8/20/2018.
 */

public interface CancelReasonsWsCalled {
    public void CancelReasonsSuccessFailure(Boolean result, String message);
}
