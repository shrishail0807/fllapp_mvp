package com.criticalog.ecritica.Interface;

public interface IFeedbackIdea {
    void feedbackIdeaSuccessFailure(String status);
}
