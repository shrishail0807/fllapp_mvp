package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IHtGeotaglog {
    void updateGeotagLog(JSONObject response);
}
