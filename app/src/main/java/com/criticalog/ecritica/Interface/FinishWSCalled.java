package com.criticalog.ecritica.Interface;

public interface FinishWSCalled {
    public void finishSuccessFailure(String result, String connote, String amount);
}
