package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface ISectorPickupSuccessFailure {
    public void sectorpickup(JSONObject response);
}
