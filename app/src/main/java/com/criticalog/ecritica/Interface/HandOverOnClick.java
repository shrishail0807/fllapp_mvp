package com.criticalog.ecritica.Interface;

import com.criticalog.ecritica.MVPCashHandover.model.DocketJson;

import java.util.List;

public interface HandOverOnClick {
    public void onClick(List<DocketJson> docketJsons,String totalAmount,int position,String status);
}
