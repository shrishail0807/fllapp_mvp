package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IInscanReqWS {
    void inscanresponse(JSONObject response);
}
