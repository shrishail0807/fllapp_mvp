package com.criticalog.ecritica.Interface;

import com.criticalog.ecritica.model.PickupResponse;

import org.json.JSONObject;

public interface PickUpListWSCalled {

    public void pickUpWsCalled(JSONObject response);
}
