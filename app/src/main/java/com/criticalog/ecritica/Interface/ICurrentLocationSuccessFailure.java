package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface ICurrentLocationSuccessFailure {
    void locationupdateSuccessFailure(JSONObject response);
}
