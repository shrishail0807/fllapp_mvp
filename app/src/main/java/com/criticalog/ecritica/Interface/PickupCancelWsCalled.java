package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface PickupCancelWsCalled {

    public void pickUpCancelScccessFailure(JSONObject response);
}
