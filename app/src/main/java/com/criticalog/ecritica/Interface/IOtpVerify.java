package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IOtpVerify {
    void verifyOtp(JSONObject response);
}
