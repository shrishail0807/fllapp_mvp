package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IDrsEventVolleyWs {

    void updateDrsEvent(JSONObject jsonObject);
}
