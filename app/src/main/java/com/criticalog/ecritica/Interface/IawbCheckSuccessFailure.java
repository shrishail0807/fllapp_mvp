package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IawbCheckSuccessFailure {
    public void awbCheck(JSONObject response);
}
