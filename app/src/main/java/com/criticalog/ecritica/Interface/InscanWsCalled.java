package com.criticalog.ecritica.Interface;



import org.json.JSONObject;

public interface InscanWsCalled {
    void docketCheckResponse(JSONObject object);
}
