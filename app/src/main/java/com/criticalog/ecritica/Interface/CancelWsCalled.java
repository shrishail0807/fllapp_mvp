package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

/**
 * Created by DELL1 on 7/20/2018.
 */

public interface CancelWsCalled {
    public void CancelWSSuccessFailure(JSONObject response);
}
