package com.criticalog.ecritica.Interface;

public interface ISnippetFeedback {
    void snippetFeedbackSuccessFailure(String status);
}
