package com.criticalog.ecritica.Interface;

public interface IAdapterClickListener {
    public void onClickAdapter(int position);
}
