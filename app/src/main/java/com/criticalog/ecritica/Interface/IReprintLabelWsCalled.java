package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IReprintLabelWsCalled {
    public void LabelRePrintWSSuccessFailure(Boolean result, JSONObject jsonObject);
}
