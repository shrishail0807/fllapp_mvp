package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IBaggingSubmitWs {
    void  baggingsubmitresponse(JSONObject response);
}
