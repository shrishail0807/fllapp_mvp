package com.criticalog.ecritica.Interface;

public interface BulkListWsCalled {
    public void BulkListSuccessFailure(Boolean result, String message);
}
