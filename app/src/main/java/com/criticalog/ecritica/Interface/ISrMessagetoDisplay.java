package com.criticalog.ecritica.Interface;

import com.criticalog.ecritica.model.SrResponse;

import org.json.JSONObject;

public interface ISrMessagetoDisplay {
    void displaySrmessage(SrResponse response);
}
