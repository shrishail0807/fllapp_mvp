package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface INegativeStatusDetailsWs {
    public void negativestatusdetails(JSONObject response);
}
