package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IInscanboxWs {
    void inscanboxvalidationresponse(JSONObject jsonObject);
}
