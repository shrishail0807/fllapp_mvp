package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IInscanactualvalueWS {
    void inscanactualvalueresponse(JSONObject response);
}
