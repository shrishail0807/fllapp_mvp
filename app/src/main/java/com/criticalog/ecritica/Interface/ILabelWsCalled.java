package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface ILabelWsCalled {

    public void LabelPrintWSSuccessFailure(Boolean result, JSONObject response);
}
