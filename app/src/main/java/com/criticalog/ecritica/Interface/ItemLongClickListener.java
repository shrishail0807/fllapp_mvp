package com.criticalog.ecritica.Interface;

import android.view.View;

public interface ItemLongClickListener {
    void onItemLongClick(View v, int pos);
}
