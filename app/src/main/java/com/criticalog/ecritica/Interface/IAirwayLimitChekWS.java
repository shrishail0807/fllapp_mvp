package com.criticalog.ecritica.Interface;

public interface IAirwayLimitChekWS {

    public void AirwayLimitSuccessFailure(String status, String message, String airwayLimit);
}
