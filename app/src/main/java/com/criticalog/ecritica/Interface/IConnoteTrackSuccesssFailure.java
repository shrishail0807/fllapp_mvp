package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IConnoteTrackSuccesssFailure {
     void connoteTrackSuccessFailure(JSONObject response);
}
