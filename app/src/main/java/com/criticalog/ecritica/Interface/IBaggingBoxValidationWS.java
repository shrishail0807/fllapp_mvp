package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IBaggingBoxValidationWS {
    void baggingboxvalidation(JSONObject reponse);
}
