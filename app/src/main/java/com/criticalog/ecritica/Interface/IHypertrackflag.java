package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IHypertrackflag {
    void hypertrackflag(JSONObject response);
}
