package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IUpdateSr {
    void updateSR(JSONObject response);
}
