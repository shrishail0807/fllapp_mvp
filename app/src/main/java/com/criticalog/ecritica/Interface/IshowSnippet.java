package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IshowSnippet {
    public void showSnippetWsSuccessFailure(int flag, JSONObject response);
}
