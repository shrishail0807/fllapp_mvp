package com.criticalog.ecritica.Interface;

import java.util.ArrayList;

public interface PickUpSuccessWS {
    public void PickUpSuccesFailure(String status, String pickupStatus, String docketNo, ArrayList<String> duplicateboxlist);
}
