package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface INegativeStatusWs {
    void negativestatus(JSONObject response);
}
