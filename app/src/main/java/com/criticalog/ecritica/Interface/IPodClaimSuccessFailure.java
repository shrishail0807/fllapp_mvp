package com.criticalog.ecritica.Interface;

import org.json.JSONObject;

public interface IPodClaimSuccessFailure {
    void podClaimSuccessFailure(JSONObject response);
}
