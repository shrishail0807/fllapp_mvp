package com.criticalog.ecritica.mvpdrspreparation;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Interface.IOnClickOnDRSCon;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.OriginHubDetail;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPInscan.InScanActivity;
import com.criticalog.ecritica.MVPLinehaul.LinehaulActivity;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.mvpdrspreparation.Adapter.DocketDrsAdapter;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSResponse;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsResponse;
import com.criticalog.ecritica.mvpdrspreparation.model.DataDRSPrep;
import com.criticalog.ecritica.mvpdrspreparation.viewmodel.DrsPreparationViewModel;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.databinding.MvpDrsPreparationBinding;
import com.criticalog.ecritica.mvproundcreation.RoundCreationActivity;
import com.criticalog.ecritica.mvproundcreation.model.DatumUserList;
import com.criticalog.ecritica.mvproundcreation.model.DatumUserType;
import com.criticalog.ecritica.mvproundcreation.model.DatumVehicleType;
import com.criticalog.ecritica.mvproundcreation.model.DatumVehicles;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;
import com.criticalog.ecritica.mvproundcreation.viewmodel.RoundCreationViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class DRSPreparationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, IOnClickOnDRSCon {
    private TextView mTopBarText;
    private MvpDrsPreparationBinding mvpDrsPreparationBinding;
    private DrsPreparationViewModel drsPreparationViewModel;
    private int count;
    private DocketDrsAdapter mDocketDrsAdapter;
    String[] userType = {"OWN/RGSA", "Agent"};

    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String token, userId, docketno;
    private int dialogOpenFlag = 0;
    private int dialogOpenFlagUserType = 0;
    private int dialogOpenFlagVehicleType = 0;

    private ArrayList<SampleSearchModel> searchHubs = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchUserType = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchVehicleType = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchHubUserList = new ArrayList<>();
    private ArrayList<SampleSearchModel> searchVehiclesByHub = new ArrayList<>();

    private List<OriginHubDetail> originHubList = new ArrayList<>();
    private List<DatumUserType> UserTypeList = new ArrayList<>();
    private List<DatumVehicleType> VehicleTypeList = new ArrayList<>();
    private List<DatumUserList> hubUserList = new ArrayList<>();
    private List<DatumVehicles> vehiclesByHubList = new ArrayList<>();
    private List<DataDRSPrep> dataDRSPrepList = new ArrayList<>();
    private List<String> dataDRSPrepListConString = new ArrayList<>();
    private Dialog dialogManualEntry;

    ArrayList<String> createDataStringhubs = new ArrayList();
    ArrayList<String> createDataStrinUserNames = new ArrayList();
    ArrayList<String> createStringUserType = new ArrayList();
    private SimpleArcDialog mProgressBar;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double latitude = 0.0, longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpDrsPreparationBinding = DataBindingUtil.setContentView(this, R.layout.mvp_drs_preparation);
        mvpDrsPreparationBinding.mTopBarText.setText("DRS Preparation");
        drsPreparationViewModel = ViewModelProviders.of(this).get(DrsPreparationViewModel.class);
        drsPreparationViewModel.init();

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        getLocation();
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");

        token = mCriticalogSharedPreferences.getData("token");
        userId = mCriticalogSharedPreferences.getData("userId");

        mCriticalogSharedPreferences.saveData("hub_code_user", "");
        mCriticalogSharedPreferences.saveData("user_type_code", "");
        mCriticalogSharedPreferences.saveData("user_id_assign_to", "");

        callOriginHubs();
        getUserType();

        mvpDrsPreparationBinding.mHub.setFocusable(false);
        mvpDrsPreparationBinding.userType.setFocusable(false);
        mvpDrsPreparationBinding.allocatedUser.setFocusable(false);


        mvpDrsPreparationBinding.confirmDRS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCriticalogSharedPreferences.getData("hub_code_user").equalsIgnoreCase("")) {
                    Toasty.warning(DRSPreparationActivity.this, "Select HUB", Toast.LENGTH_SHORT, true).show();
                } else if (mCriticalogSharedPreferences.getData("user_type_code").equalsIgnoreCase("")) {
                    Toasty.warning(DRSPreparationActivity.this, "Select User Type!!", Toast.LENGTH_SHORT, true).show();
                } else if (mCriticalogSharedPreferences.getData("user_id_assign_to").equalsIgnoreCase("")) {
                    Toasty.warning(DRSPreparationActivity.this, "Select User to Assign DRS", Toast.LENGTH_SHORT, true).show();
                } else if (dataDRSPrepListConString.size() == 0) {
                    Toasty.warning(DRSPreparationActivity.this, "Scan Dockets !!", Toast.LENGTH_SHORT, true).show();
                } else {
                    mProgressBar.show();
                    AssignDRSRequest assignDRSRequest = new AssignDRSRequest();
                    assignDRSRequest.setAction("assign_drs");
                    assignDRSRequest.setUserId(userId);
                    assignDRSRequest.setAssignTo((mCriticalogSharedPreferences.getData("user_id_assign_to")));
                    assignDRSRequest.setHubId(mCriticalogSharedPreferences.getData("hub_code_user"));
                    assignDRSRequest.setUserType(mCriticalogSharedPreferences.getData("user_type_code"));
                    assignDRSRequest.setConnotes(dataDRSPrepListConString);
                    assignDRSRequest.setLatitude(String.valueOf(latitude));
                    assignDRSRequest.setLongitude(String.valueOf(longitude));
                    drsPreparationViewModel.assignDRS(assignDRSRequest);
                }
            }
        });
        mvpDrsPreparationBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mvpDrsPreparationBinding.enterConnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualDialog();
            }
        });
        mvpDrsPreparationBinding.scanQrConnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBarcodeScan();
            }
        });
        mvpDrsPreparationBinding.userType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchUserType != null) {
                    if (searchUserType.size() > 0) {
                        showOriginDestination(searchUserType, "User Type");
                    } else {
                        dialogOpenFlagUserType = 1;
                        getUserType();
                    }
                }
            }
        });

        mvpDrsPreparationBinding.mHub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOpenFlag = 1;
                callOriginHubs();

             /* if (searchHubs != null) {
                    if (searchHubs.size() > 0) {
                        showOriginDestination(searchHubs, "Origin Hub");
                    } else {
                        dialogOpenFlag = 1;
                        callOriginHubs();
                    }
                }*/
            }
        });

        mvpDrsPreparationBinding.allocatedUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHubUserList();
            }
        });

        drsPreparationViewModel.assignDRSLiveData().observe(this, new Observer<AssignDRSResponse>() {
            @Override
            public void onChanged(AssignDRSResponse assignDRSResponse) {
                mProgressBar.dismiss();
                if (assignDRSResponse.getStatus() == 200) {
                    Toasty.success(DRSPreparationActivity.this, assignDRSResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    finish();
                } else {
                    Toasty.warning(DRSPreparationActivity.this, assignDRSResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
            }
        });
        drsPreparationViewModel.getConDetailsLiveData().observe(this, new Observer<DRSPrepConnoteDetailsResponse>() {
            @Override
            public void onChanged(DRSPrepConnoteDetailsResponse drsPrepConnoteDetailsResponse) {
                // Toasty.warning(DRSPreparationActivity.this, "Con Already Scanned", Toast.LENGTH_SHORT, true).show();
                mProgressBar.dismiss();

                if (drsPrepConnoteDetailsResponse.getStatus() == 200) {
                    if (dialogManualEntry != null) {
                        if (dialogManualEntry.isShowing()) {
                            dialogManualEntry.dismiss();
                        }
                    }
                    dataDRSPrepList.add(drsPrepConnoteDetailsResponse.getData());
                    dataDRSPrepListConString.add(docketno);
                } else {
                    Toasty.warning(DRSPreparationActivity.this, drsPrepConnoteDetailsResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
                mDocketDrsAdapter = new DocketDrsAdapter(DRSPreparationActivity.this, dataDRSPrepList, DRSPreparationActivity.this);
                mvpDrsPreparationBinding.mRvDocketDetails.setLayoutManager(new LinearLayoutManager(DRSPreparationActivity.this));
                mvpDrsPreparationBinding.mRvDocketDetails.setAdapter(mDocketDrsAdapter);
            }
        });
        drsPreparationViewModel.getHubsLiveData().observe(this, new Observer<BaggingOriginDestResponse>() {
            @Override
            public void onChanged(BaggingOriginDestResponse baggingOriginDestResponse) {
                if (baggingOriginDestResponse != null) {
                    if (baggingOriginDestResponse.getStatus() == 200) {
                        for (int i = 0; i < baggingOriginDestResponse.getData().getHubDetails().size(); i++) {
                            searchHubs.add(new SampleSearchModel(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode()));
                            createDataStringhubs.add(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode());
                            if (baggingOriginDestResponse.getData().getHubDetails().get(i).getHubDefault().equalsIgnoreCase("1")) {

                                if (dialogOpenFlag == 0) {
                                    mvpDrsPreparationBinding.mHub.setText(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode());
                                }
                                //  mvpDrsPreparationBinding.mHub.setText(baggingOriginDestResponse.getData().getHubDetails().get(i).getHubName() + "-" + baggingOriginDestResponse.getData().getHubDetails().get(i).getHubCode());

                                mCriticalogSharedPreferences.saveData("hub_code_user", baggingOriginDestResponse.getData().getHubDetails().get(0).getHubId());
                            }
                        }
                        originHubList = baggingOriginDestResponse.getData().getHubDetails();

                        if (dialogOpenFlag == 1) {
                            showOriginDestination(searchHubs, "Origin Hub");
                        }
                    }
                }
            }
        });

        drsPreparationViewModel.getUserTypeLiveData().observe(this, new Observer<UserTypeListResponse>() {
            @Override
            public void onChanged(UserTypeListResponse userTypeListResponse) {
                if (userTypeListResponse != null) {
                    if (userTypeListResponse.getStatus() == 200) {
                        for (int i = 0; i < userTypeListResponse.getData().size(); i++) {
                            searchUserType.add(new SampleSearchModel(userTypeListResponse.getData().get(i).getUserTypeDesc()));

                            createStringUserType.add(userTypeListResponse.getData().get(i).getUserTypeDesc());
                            mvpDrsPreparationBinding.userType.setText(userTypeListResponse.getData().get(0).getUserTypeDesc());

                            mCriticalogSharedPreferences.saveData("user_type_code", userTypeListResponse.getData().get(0).getUserTypeCode());
                        }

                        UserTypeList = userTypeListResponse.getData();
                        if (dialogOpenFlagUserType == 1) {
                            showOriginDestination(searchUserType, "User Type");
                        }
                    }
                }
            }
        });

        drsPreparationViewModel.getHubUserListLiveData().observe(this, new Observer<HubUserListResponse>() {
            @Override
            public void onChanged(HubUserListResponse hubUserListResponse) {
                mProgressBar.dismiss();
                if (hubUserListResponse != null) {
                    Toast.makeText(DRSPreparationActivity.this, hubUserListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    if (hubUserListResponse.getStatus() == 200) {
                        for (int i = 0; i < hubUserListResponse.getData().size(); i++) {
                            searchHubUserList.add(new SampleSearchModel(hubUserListResponse.getData().get(i).getFllName() + "(" + hubUserListResponse.getData().get(i).getFllId() + ")"));

                            createDataStrinUserNames.add(hubUserListResponse.getData().get(i).getFllName() + "(" + hubUserListResponse.getData().get(i).getFllId() + ")");
                            mCriticalogSharedPreferences.saveData("user_id_assign_to", hubUserListResponse.getData().get(0).getFllId());
                        }
                        hubUserList = hubUserListResponse.getData();

                        showOriginDestination(searchHubUserList, "Select User");
                    }
                } else {
                    Toast.makeText(DRSPreparationActivity.this, "Parse Error", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(DRSPreparationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DRSPreparationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DRSPreparationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(DRSPreparationActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String scanwhat = mCriticalogSharedPreferences.getData("scan_what");
        if (result != null) {
            try {
                String MobilePattern = "[0-9]{9}";
                docketno = result.getContents();
                if (docketno.length() == 9) {
                    if (dataDRSPrepListConString.contains(docketno)) {
                        Toasty.warning(DRSPreparationActivity.this, "Con Already Scanned", Toast.LENGTH_SHORT, true).show();
                    } else {
                        getConDetails(docketno);
                    }
                } else {
                    Toasty.warning(DRSPreparationActivity.this, "Scan Valid Docket!!", Toast.LENGTH_SHORT, true).show();
                }


            } catch (Exception e) {
                e.printStackTrace();
                //that means the encoded format not matches
                //in this case you can display whatever data is available on the qrcode

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void manualDialog() {
        EditText mDocketEntry;
        TextView mAddDocket;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(DRSPreparationActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_scan);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);

        mDocketEntry = dialogManualEntry.findViewById(R.id.mDocketEntry);
        mAddDocket = dialogManualEntry.findViewById(R.id.mAddDocket);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mDocketEntry.setHint("Enter Connote Number");
        mAddDocket.setText("Check");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });


        mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketno = mDocketEntry.getText().toString();
                if (docketno.length() == 9) {
                    if (dataDRSPrepListConString.contains(docketno)) {
                        Toast.makeText(DRSPreparationActivity.this, "Already docket Exist", Toast.LENGTH_SHORT).show();
                    } else {
                        getConDetails(docketno);
                    }

                } else {
                    Toasty.warning(DRSPreparationActivity.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });

    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();

    }

    void callOriginHubs() {
        //    searchHubs.clear();
        searchHubs.clear();
        createDataStringhubs.clear();
        originHubList.clear();
        BaggingOriginDestRequest baggingOriginDestRequest = new BaggingOriginDestRequest();
        baggingOriginDestRequest.setAction("bagging_orghub");
        baggingOriginDestRequest.setUserId(userId);
        baggingOriginDestRequest.setLatitude(String.valueOf(latitude));
        baggingOriginDestRequest.setLongitude(String.valueOf(longitude));
        drsPreparationViewModel.getHubs(baggingOriginDestRequest);
    }

    void getConDetails(String docketno) {
        mProgressBar.show();
        DRSPrepConnoteDetailsRequest drsPrepConnoteDetailsRequest = new DRSPrepConnoteDetailsRequest();
        drsPrepConnoteDetailsRequest.setAction("get_docket_details_for_drs");
        drsPrepConnoteDetailsRequest.setUserId(userId);
        drsPrepConnoteDetailsRequest.setDocketNo(docketno);
        drsPrepConnoteDetailsRequest.setHub_id(mCriticalogSharedPreferences.getData("hub_code_user"));
        drsPreparationViewModel.getConDetails(drsPrepConnoteDetailsRequest);
    }

    void getUserType() {
        searchUserType.clear();
        UserTypeList.clear();
        UserTypeListRequest userTypeListRequest = new UserTypeListRequest();
        userTypeListRequest.setAction("get_user_type_list");
        userTypeListRequest.setUserId(userId);
        drsPreparationViewModel.getUserType(userTypeListRequest);

    }

    void getHubUserList() {
        createDataStrinUserNames.clear();
        mProgressBar.show();
        searchHubUserList.clear();
        HubUserListRequest hubUserListRequest = new HubUserListRequest();
        hubUserListRequest.setAction("get_hub_users_by_user_type");
        hubUserListRequest.setUserId(userId);
        hubUserListRequest.setHubCode(mCriticalogSharedPreferences.getData("hub_code_user"));
        hubUserListRequest.setUserType(mCriticalogSharedPreferences.getData("user_type_code"));
        drsPreparationViewModel.getHubUserList(hubUserListRequest);
        // mProgressBar.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, userType[position], Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(DRSPreparationActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {

                        if (fromWhere.equalsIgnoreCase("Origin Hub")) {
                            int indedOfHub = createDataStringhubs.indexOf(item.getTitle());
                            String hubCode = originHubList.get(indedOfHub).getHubId();
                            mCriticalogSharedPreferences.saveData("hub_code_user", hubCode);
                            mvpDrsPreparationBinding.mHub.setText(item.getTitle());
                            Toast.makeText(DRSPreparationActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                        } else if (fromWhere.equalsIgnoreCase("User Type")) {
                            int indedOUserType = createStringUserType.indexOf(item.getTitle());
                            String userTypeCOde = UserTypeList.get(indedOUserType).getUserTypeCode();
                            mvpDrsPreparationBinding.userType.setText(item.getTitle());
                            mCriticalogSharedPreferences.saveData("user_type_code", userTypeCOde);
                            mCriticalogSharedPreferences.saveData("user_id_assign_to", "");
                            mvpDrsPreparationBinding.allocatedUser.setText("");
                        } else if (fromWhere.equalsIgnoreCase("Select User")) {
                            int indexUser = createDataStrinUserNames.indexOf(item.getTitle());
                            String userId = hubUserList.get(indexUser).getFllId();
                            mCriticalogSharedPreferences.saveData("user_id_assign_to", userId);
                            mvpDrsPreparationBinding.allocatedUser.setText(item.getTitle());
                        }
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void onclickDRSPrepRemoveCon(int position, String message) {
        dataDRSPrepList.remove(position);
        dataDRSPrepListConString.remove(position);
        mDocketDrsAdapter.notifyDataSetChanged();
    }
}
