package com.criticalog.ecritica.mvpdrspreparation.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.IOnClickOnDRSCon;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.mvpdrspreparation.model.DataDRSPrep;

import org.w3c.dom.Text;

import java.util.List;

public class DocketDrsAdapter extends RecyclerView.Adapter<DocketDrsAdapter.DocketDRSViewHolder> {
    private Context context;
    List<DataDRSPrep> dataDRSPrepList;
    private IOnClickOnDRSCon iOnClickOnDRSCon;

    public DocketDrsAdapter(Context context, List<DataDRSPrep> dataDRSPrepList, IOnClickOnDRSCon iOnClickOnDRSCon) {
        this.context = context;
        this.dataDRSPrepList = dataDRSPrepList;
        this.iOnClickOnDRSCon = iOnClickOnDRSCon;
    }

    @NonNull
    @Override
    public DocketDrsAdapter.DocketDRSViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.drs_docket_item, parent, false);
        return new DocketDrsAdapter.DocketDRSViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull DocketDrsAdapter.DocketDRSViewHolder holder, int position) {

        // holder.consigneeNo.setText("Docket No: "+dataDRSPrepList.get(position).getDocketNo());
        // holder.clientCode.setText("Client Code: "+dataDRSPrepList.get(position).getClientCode());
        // holder.clientName.setText("Client Name: "+dataDRSPrepList.get(position).getClientName());
        // holder.serviceType.setText("Serice Type: "+dataDRSPrepList.get(position).getServiceType());
        //  holder.productMode.setText("Product Mode: "+dataDRSPrepList.get(position).getProductMode());
        //holder.consignmentCode.setText("Pay Type: "+dataDRSPrepList.get(position).getConsignmentCode());
        //holder.consignmetAddress.setText("Address: "+dataDRSPrepList.get(position).getConsigneeAddress());

        String sourceString = "<b>" + "Docket No: " + "</b> " + dataDRSPrepList.get(position).getDocketNo();
        holder.consigneeNo.setText(Html.fromHtml(sourceString));

        String sourceString1 = "<b>" + "Client Code: " + "</b> " + dataDRSPrepList.get(position).getClientCode();
        holder.clientCode.setText(Html.fromHtml(sourceString1));

        String sourceString2 = "<b>" + "Client Name: " + "</b> " + dataDRSPrepList.get(position).getClientName();
        holder.clientName.setText(Html.fromHtml(sourceString2));

        String sourceString3 = "<b>" + "Service Type: " + "</b> " + dataDRSPrepList.get(position).getServiceType();
        holder.serviceType.setText(Html.fromHtml(sourceString3));

        String sourceString4 = "<b>" + "Product Mode: " + "</b> " + dataDRSPrepList.get(position).getProductMode();
        holder.productMode.setText(Html.fromHtml(sourceString4));

        String sourceString5 = "<b>" + "Pay Type: " + "</b> " + dataDRSPrepList.get(position).getConsignmentCode();
        holder.consignmentCode.setText(Html.fromHtml(sourceString5));

        String sourceString6 = "<b>" + "Address: " + "</b> " + dataDRSPrepList.get(position).getConsigneeAddress();
        holder.consignmetAddress.setText(Html.fromHtml(sourceString6));

        holder.cancel_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnClickOnDRSCon.onclickDRSPrepRemoveCon(position, "");
            }
        });

    }


    @Override
    public int getItemCount() {
        return dataDRSPrepList.size();
    }

    public class DocketDRSViewHolder extends RecyclerView.ViewHolder {
        TextView consigneeNo, clientCode, clientName, serviceType, productMode, consignmentCode, consignmetAddress;
        ImageView cancel_remove;

        public DocketDRSViewHolder(@NonNull View itemView) {
            super(itemView);

            consigneeNo = itemView.findViewById(R.id.mConsignmentNo);
            clientCode = itemView.findViewById(R.id.mCustomereCode);
            clientName = itemView.findViewById(R.id.mCustomerName);
            serviceType = itemView.findViewById(R.id.mServiceType);
            productMode = itemView.findViewById(R.id.mProductMode);
            consignmentCode = itemView.findViewById(R.id.mConsignmentCode);
            consignmetAddress = itemView.findViewById(R.id.mConsignmentAddr);
            cancel_remove = itemView.findViewById(R.id.cancel_remove);

        }
    }
}
