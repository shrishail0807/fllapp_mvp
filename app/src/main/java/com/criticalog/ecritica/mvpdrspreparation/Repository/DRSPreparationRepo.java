package com.criticalog.ecritica.mvpdrspreparation.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSResponse;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsResponse;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DRSPreparationRepo {
    private RestServices mRestServices;
    private MutableLiveData<BaggingOriginDestResponse> baggingOriginDestResponseMutableLiveData;
    private MutableLiveData<UserTypeListResponse> userTypeListResponseMutableLiveData;
    private MutableLiveData<HubUserListResponse> hubUserListResponseMutableLiveData;
    private MutableLiveData<DRSPrepConnoteDetailsResponse> drsPrepConnoteDetailsResponseMutableLiveData;
    private MutableLiveData<AssignDRSResponse> assignDRSResponseMutableLiveData;


    public DRSPreparationRepo() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        baggingOriginDestResponseMutableLiveData = new MutableLiveData<>();
        userTypeListResponseMutableLiveData = new MutableLiveData<>();
        hubUserListResponseMutableLiveData = new MutableLiveData<>();
        drsPrepConnoteDetailsResponseMutableLiveData = new MutableLiveData<>();
        assignDRSResponseMutableLiveData=new MutableLiveData<>();

    }

    public void getOriginHubs(BaggingOriginDestRequest baggingOriginDestRequest) {
        Call<BaggingOriginDestResponse> baggingOriginDestResponseCall = mRestServices.baggingOriginDestinationHub(baggingOriginDestRequest);

        baggingOriginDestResponseCall.enqueue(new Callback<BaggingOriginDestResponse>() {
            @Override
            public void onResponse(Call<BaggingOriginDestResponse> call, Response<BaggingOriginDestResponse> response) {
                baggingOriginDestResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<BaggingOriginDestResponse> call, Throwable t) {
                baggingOriginDestResponseMutableLiveData.setValue(null);
            }
        });

    }

    public void getUserType(UserTypeListRequest userTypeListRequest) {
        Call<UserTypeListResponse> userTypeListResponseCall = mRestServices.getUserType(userTypeListRequest);
        userTypeListResponseCall.enqueue(new Callback<UserTypeListResponse>() {
            @Override
            public void onResponse(Call<UserTypeListResponse> call, Response<UserTypeListResponse> response) {
                userTypeListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<UserTypeListResponse> call, Throwable t) {
                userTypeListResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getHubUserList(HubUserListRequest hubUserListRequest) {
        Call<HubUserListResponse> hubUserListResponseCall = mRestServices.getHubUserList(hubUserListRequest);
        hubUserListResponseCall.enqueue(new Callback<HubUserListResponse>() {
            @Override
            public void onResponse(Call<HubUserListResponse> call, Response<HubUserListResponse> response) {
                hubUserListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<HubUserListResponse> call, Throwable t) {
                hubUserListResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getDRSConDetails(DRSPrepConnoteDetailsRequest drsPrepConnoteDetailsRequest) {
        Call<DRSPrepConnoteDetailsResponse> drsPrepConnoteDetailsResponseCall = mRestServices.DRSPrepConDetails(drsPrepConnoteDetailsRequest);
        drsPrepConnoteDetailsResponseCall.enqueue(new Callback<DRSPrepConnoteDetailsResponse>() {
            @Override
            public void onResponse(Call<DRSPrepConnoteDetailsResponse> call, Response<DRSPrepConnoteDetailsResponse> response) {
                drsPrepConnoteDetailsResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<DRSPrepConnoteDetailsResponse> call, Throwable t) {
                drsPrepConnoteDetailsResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void assignDRS(AssignDRSRequest assignDRSRequest){
        Call<AssignDRSResponse> assignDRSResponseCall=mRestServices.assignDRS(assignDRSRequest);
        assignDRSResponseCall.enqueue(new Callback<AssignDRSResponse>() {
            @Override
            public void onResponse(Call<AssignDRSResponse> call, Response<AssignDRSResponse> response) {
                assignDRSResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<AssignDRSResponse> call, Throwable t) {
                assignDRSResponseMutableLiveData.setValue(null);
            }
        });

    }
    public LiveData<AssignDRSResponse> assignDRSLiveData() {
        return assignDRSResponseMutableLiveData;
    }
    public LiveData<DRSPrepConnoteDetailsResponse> getDRSConDetailsLiveData() {
        return drsPrepConnoteDetailsResponseMutableLiveData;
    }
    public LiveData<BaggingOriginDestResponse> getHubsLiveData() {
        return baggingOriginDestResponseMutableLiveData;
    }

    public LiveData<UserTypeListResponse> getUserTypeLiveData() {
        return userTypeListResponseMutableLiveData;
    }

    public LiveData<HubUserListResponse> getHubUserListLiveData() {
        return hubUserListResponseMutableLiveData;
    }
}
