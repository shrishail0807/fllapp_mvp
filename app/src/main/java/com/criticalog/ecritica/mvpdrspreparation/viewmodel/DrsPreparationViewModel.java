package com.criticalog.ecritica.mvpdrspreparation.viewmodel;
import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.mvpdrspreparation.Repository.DRSPreparationRepo;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSResponse;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsRequest;
import com.criticalog.ecritica.mvpdrspreparation.model.DRSPrepConnoteDetailsResponse;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListRequest;
import com.criticalog.ecritica.mvproundcreation.model.HubUserListResponse;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListRequest;
import com.criticalog.ecritica.mvproundcreation.model.UserTypeListResponse;

public class DrsPreparationViewModel extends AndroidViewModel {
    private DRSPreparationRepo bookRepository;
    private LiveData<BaggingOriginDestResponse> baggingOriginDestResponseLiveData;
    private LiveData<UserTypeListResponse> userTypeListResponseLiveData;
    private LiveData<HubUserListResponse> hubUserListResponseLiveData;
    private LiveData<DRSPrepConnoteDetailsResponse> drsPrepConnoteDetailsResponseLiveData;
    private LiveData<AssignDRSResponse> assignDRSResponseLiveData;

    public DrsPreparationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        bookRepository = new DRSPreparationRepo();
        baggingOriginDestResponseLiveData = bookRepository.getHubsLiveData();
        userTypeListResponseLiveData = bookRepository.getUserTypeLiveData();
        hubUserListResponseLiveData = bookRepository.getHubUserListLiveData();
        drsPrepConnoteDetailsResponseLiveData = bookRepository.getDRSConDetailsLiveData();
        assignDRSResponseLiveData = bookRepository.assignDRSLiveData();
    }

    public void assignDRS(AssignDRSRequest assignDRSRequest) {
        bookRepository.assignDRS(assignDRSRequest);
    }

    public LiveData<AssignDRSResponse> assignDRSLiveData() {
        return assignDRSResponseLiveData;
    }

    public void getHubs(BaggingOriginDestRequest baggingOriginDestRequest) {
        bookRepository.getOriginHubs(baggingOriginDestRequest);
    }

    public LiveData<BaggingOriginDestResponse> getHubsLiveData() {
        return baggingOriginDestResponseLiveData;
    }

    public void getUserType(UserTypeListRequest userTypeListRequest) {
        bookRepository.getUserType(userTypeListRequest);
    }

    public LiveData<UserTypeListResponse> getUserTypeLiveData() {
        return userTypeListResponseLiveData;
    }

    public void getHubUserList(HubUserListRequest hubUserListRequest) {
        bookRepository.getHubUserList(hubUserListRequest);
    }

    public LiveData<HubUserListResponse> getHubUserListLiveData() {
        return hubUserListResponseLiveData;
    }

    public void getConDetails(DRSPrepConnoteDetailsRequest drsPrepConnoteDetailsRequest) {
        bookRepository.getDRSConDetails(drsPrepConnoteDetailsRequest);
    }

    public LiveData<DRSPrepConnoteDetailsResponse> getConDetailsLiveData() {
        return drsPrepConnoteDetailsResponseLiveData;
    }
}


