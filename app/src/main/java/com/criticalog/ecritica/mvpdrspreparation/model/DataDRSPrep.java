package com.criticalog.ecritica.mvpdrspreparation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataDRSPrep implements Serializable {
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("product_mode")
    @Expose
    private String productMode;
    @SerializedName("consignment_code")
    @Expose
    private String consignmentCode;
    @SerializedName("consignee_name")
    @Expose
    private String consigneeName;
    @SerializedName("consignee_address")
    @Expose
    private String consigneeAddress;

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getProductMode() {
        return productMode;
    }

    public void setProductMode(String productMode) {
        this.productMode = productMode;
    }

    public String getConsignmentCode() {
        return consignmentCode;
    }

    public void setConsignmentCode(String consignmentCode) {
        this.consignmentCode = consignmentCode;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }
}
