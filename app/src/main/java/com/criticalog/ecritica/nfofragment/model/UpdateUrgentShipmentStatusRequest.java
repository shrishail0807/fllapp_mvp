package com.criticalog.ecritica.nfofragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUrgentShipmentStatusRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("us_id")
    @Expose
    private String usId;
    @SerializedName("button_type")
    @Expose
    private String buttonType;
    @SerializedName("connote_no")
    @Expose
    private String connoteNo;
    @SerializedName("handover_to_id")
    @Expose
    private String handoverToId;
    @SerializedName("handover_to_name")
    @Expose
    private String handoverToName;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsId() {
        return usId;
    }

    public void setUsId(String usId) {
        this.usId = usId;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getConnoteNo() {
        return connoteNo;
    }

    public void setConnoteNo(String connoteNo) {
        this.connoteNo = connoteNo;
    }

    public String getHandoverToId() {
        return handoverToId;
    }

    public void setHandoverToId(String handoverToId) {
        this.handoverToId = handoverToId;
    }

    public String getHandoverToName() {
        return handoverToName;
    }

    public void setHandoverToName(String handoverToName) {
        this.handoverToName = handoverToName;
    }
}
