package com.criticalog.ecritica.nfofragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NFOPRSStatusListRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
