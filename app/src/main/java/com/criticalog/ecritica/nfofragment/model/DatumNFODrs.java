package com.criticalog.ecritica.nfofragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumNFODrs {
    @SerializedName("us_id")
    @Expose
    private String usId;
    @SerializedName("connote_no")
    @Expose
    private String connoteNo;
    @SerializedName("allocated_id")
    @Expose
    private String allocatedId;
    @SerializedName("allocated_name")
    @Expose
    private String allocatedName;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("accept")
    @Expose
    private String accept;
    @SerializedName("retrieve")
    @Expose
    private String retrieve;
    @SerializedName("handover_to_id")
    @Expose
    private String handoverToId;
    @SerializedName("handover_to_name")
    @Expose
    private String handoverToName;
    @SerializedName("drs")
    @Expose
    private String drs;
    @SerializedName("close")
    @Expose
    private String close;
    @SerializedName("handover")
    @Expose
    private String handover;
    @SerializedName("drs_link")
    @Expose
    private String drsLink;
    @SerializedName("accept_button")
    @Expose
    private String acceptButton;
    @SerializedName("retrieve_button")
    @Expose
    private String retrieveButton;
    @SerializedName("drs_button")
    @Expose
    private String drsButton;
    @SerializedName("close_button")
    @Expose
    private String closeButton;

    @SerializedName("handover_button")
    @Expose
    private String handover_button;

    @SerializedName("accept_button_send")
    @Expose
    private String accept_button_send;

    @SerializedName("retrieve_button_send")
    @Expose
    private String retrieve_button_send;

    @SerializedName("handover_button_send")
    @Expose
    private String handover_button_send;

    @SerializedName("drs_button_send")
    @Expose
    private String drs_button_send;

    @SerializedName("close_button_send")
    @Expose
    private String close_button_send;

    public String getAccept_button_send() {
        return accept_button_send;
    }

    public void setAccept_button_send(String accept_button_send) {
        this.accept_button_send = accept_button_send;
    }

    public String getRetrieve_button_send() {
        return retrieve_button_send;
    }

    public void setRetrieve_button_send(String retrieve_button_send) {
        this.retrieve_button_send = retrieve_button_send;
    }

    public String getHandover_button_send() {
        return handover_button_send;
    }

    public void setHandover_button_send(String handover_button_send) {
        this.handover_button_send = handover_button_send;
    }

    public String getDrs_button_send() {
        return drs_button_send;
    }

    public void setDrs_button_send(String drs_button_send) {
        this.drs_button_send = drs_button_send;
    }

    public String getClose_button_send() {
        return close_button_send;
    }

    public void setClose_button_send(String close_button_send) {
        this.close_button_send = close_button_send;
    }

    public String getHandover_button() {
        return handover_button;
    }

    public void setHandover_button(String handover_button) {
        this.handover_button = handover_button;
    }

    public String getUsId() {
        return usId;
    }

    public void setUsId(String usId) {
        this.usId = usId;
    }

    public String getConnoteNo() {
        return connoteNo;
    }

    public void setConnoteNo(String connoteNo) {
        this.connoteNo = connoteNo;
    }

    public String getAllocatedId() {
        return allocatedId;
    }

    public void setAllocatedId(String allocatedId) {
        this.allocatedId = allocatedId;
    }

    public String getAllocatedName() {
        return allocatedName;
    }

    public void setAllocatedName(String allocatedName) {
        this.allocatedName = allocatedName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getRetrieve() {
        return retrieve;
    }

    public void setRetrieve(String retrieve) {
        this.retrieve = retrieve;
    }

    public String getHandoverToId() {
        return handoverToId;
    }

    public void setHandoverToId(String handoverToId) {
        this.handoverToId = handoverToId;
    }

    public String getHandoverToName() {
        return handoverToName;
    }

    public void setHandoverToName(String handoverToName) {
        this.handoverToName = handoverToName;
    }

    public String getDrs() {
        return drs;
    }

    public void setDrs(String drs) {
        this.drs = drs;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getHandover() {
        return handover;
    }

    public void setHandover(String handover) {
        this.handover = handover;
    }

    public String getDrsLink() {
        return drsLink;
    }

    public void setDrsLink(String drsLink) {
        this.drsLink = drsLink;
    }

    public String getAcceptButton() {
        return acceptButton;
    }

    public void setAcceptButton(String acceptButton) {
        this.acceptButton = acceptButton;
    }

    public String getRetrieveButton() {
        return retrieveButton;
    }

    public void setRetrieveButton(String retrieveButton) {
        this.retrieveButton = retrieveButton;
    }

    public String getDrsButton() {
        return drsButton;
    }

    public void setDrsButton(String drsButton) {
        this.drsButton = drsButton;
    }

    public String getCloseButton() {
        return closeButton;
    }

    public void setCloseButton(String closeButton) {
        this.closeButton = closeButton;
    }

}
