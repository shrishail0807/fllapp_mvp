package com.criticalog.ecritica.nfofragment.interfaces;

public interface OnclickOfNfo {

    public void onclickNFOShipmet(String us_id,
                                  String buttonType,
                                  String connoteNumber,
                                  String handoverName,
                                  String handoverUserId);
}
