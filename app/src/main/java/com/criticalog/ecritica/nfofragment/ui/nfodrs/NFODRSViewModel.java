package com.criticalog.ecritica.nfofragment.ui.nfodrs;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourRequest;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourResponse;
import com.criticalog.ecritica.mvproundcreation.repository.RoundCreationRepo;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusResponse;
import com.criticalog.ecritica.nfofragment.ui.nfodrs.repository.NFODrsRepository;

public class NFODRSViewModel extends AndroidViewModel {
    private NFODrsRepository nfoDrsRepository;
    private LiveData<NFODrsStatusListResponse> nfoDrsStatusListResponseLiveData;
    private LiveData<UpdateUrgentShipmentStatusResponse> updateUrgentShipmentStatusResponseLiveData;

    public NFODRSViewModel(@NonNull Application application) {
        super(application);
    }

    void init() {
        nfoDrsRepository=new NFODrsRepository();
        nfoDrsStatusListResponseLiveData=nfoDrsRepository.getNFODrsStatusList();
        updateUrgentShipmentStatusResponseLiveData=nfoDrsRepository.updateUrgentShipmet();
    }

    //Get DRS NFO List
    public void getNfoStatusList(NFODrsStatusListRequest nfoDrsStatusListRequest) {
        nfoDrsRepository.getNfoDrsStatusList(nfoDrsStatusListRequest);
    }

    public LiveData<NFODrsStatusListResponse> getNFODrsStatusListLiveData() {
        return nfoDrsStatusListResponseLiveData;
    }

    //Update DRS NFO
    public void updateStatusOfNFO(UpdateUrgentShipmentStatusRequest updateUrgentShipmentStatusRequest) {
        nfoDrsRepository.updateUrgentShipmentStatus(updateUrgentShipmentStatusRequest);
    }

    public LiveData<UpdateUrgentShipmentStatusResponse> updateStattusNFOLiveData() {
        return updateUrgentShipmentStatusResponseLiveData;
    }

}