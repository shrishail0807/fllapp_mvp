package com.criticalog.ecritica.nfofragment.ui.nfodrs;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.databinding.FragmentNfodrsBinding;
import com.criticalog.ecritica.nfofragment.adapter.NFOPrsDrsAdapter;
import com.criticalog.ecritica.nfofragment.interfaces.OnclickOfNfo;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class NFODRSFragment extends Fragment implements OnclickOfNfo {

    private FragmentNfodrsBinding binding;
    private NFOPrsDrsAdapter nfoPrsDrsAdapter;
    private NFODRSViewModel nfodrsViewModel;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    private DatabaseHelper dbHelper;
    private double start_lat, start_long;
    private double latitude = 0.0, longitude = 0.0;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private SimpleArcDialog mProgressBar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        nfodrsViewModel = ViewModelProviders.of(this).get(NFODRSViewModel.class);
        nfodrsViewModel.init();
        binding = FragmentNfodrsBinding.inflate(inflater, container, false);

        mProgressBar = new SimpleArcDialog(getContext());
        mProgressBar.setConfiguration(new ArcConfiguration(getContext()));
        mProgressBar.setCancelable(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(getContext());
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };
        dbHelper = new DatabaseHelper(getContext());
        dbHelper.getWritableDatabase();
        userId = mCriticalogSharedPreferences.getData("userId");

        getLocation();
        getNFODrsStatusList();

        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.swipeRefreshLayout.setRefreshing(false);
                getNFODrsStatusList();
            }
        });
        //  storeLatLong();
        nfodrsViewModel.updateStattusNFOLiveData().observe(getActivity(), new Observer<UpdateUrgentShipmentStatusResponse>() {
            @Override
            public void onChanged(UpdateUrgentShipmentStatusResponse updateUrgentShipmentStatusResponse) {
                mProgressBar.dismiss();
                if (updateUrgentShipmentStatusResponse.getStatus() == 200) {
                    Toasty.success(getActivity(), updateUrgentShipmentStatusResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                    getNFODrsStatusList();
                    storeLatLong();
                } else {
                    Toasty.error(getActivity(), updateUrgentShipmentStatusResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                }
            }
        });
        nfodrsViewModel.getNFODrsStatusListLiveData().observe(getActivity(), new Observer<NFODrsStatusListResponse>() {
            @Override
            public void onChanged(NFODrsStatusListResponse nfoDrsStatusListResponse) {

                mProgressBar.dismiss();

                if (nfoDrsStatusListResponse.getStatus() == 200) {
                    binding.mRvNfoDRs.setVisibility(View.VISIBLE);
                    nfoPrsDrsAdapter = new NFOPrsDrsAdapter(getContext(), 1, nfoDrsStatusListResponse.getData(), NFODRSFragment.this);
                    binding.mRvNfoDRs.setLayoutManager(new LinearLayoutManager(getContext()));
                    binding.mRvNfoDRs.setAdapter(nfoPrsDrsAdapter);
                } else {
                    binding.mRvNfoDRs.setVisibility(View.GONE);
                    binding.mErrorText.setVisibility(View.VISIBLE);
                    binding.mErrorText.setText(nfoDrsStatusListResponse.getMessage());
                    Toast.makeText(getContext(), nfoDrsStatusListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        View root = binding.getRoot();
        return root;
    }

    public void getNFODrsStatusList() {
        getLocation();
        NFODrsStatusListRequest nfoDrsStatusListRequest = new NFODrsStatusListRequest();
        nfoDrsStatusListRequest.setAction("get_urgent_shipment_drs");
        nfoDrsStatusListRequest.setUserId(userId);
        nfoDrsStatusListRequest.setLatitude(String.valueOf(latitude));
        nfoDrsStatusListRequest.setLongitude(String.valueOf(longitude));
        nfodrsViewModel.getNfoStatusList(nfoDrsStatusListRequest);

        mProgressBar.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onclickNFOShipmet(String us_id, String buttonType, String connoteNumber, String handoverName, String handoverUserId) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        String message = "Are you sure want to click on" + " " + buttonType;
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getLocation();
                        UpdateUrgentShipmentStatusRequest updateUrgentShipmentStatusRequest = new UpdateUrgentShipmentStatusRequest();
                        updateUrgentShipmentStatusRequest.setAction("update_urgent_shipment_drs");
                        updateUrgentShipmentStatusRequest.setUserId(userId);
                        updateUrgentShipmentStatusRequest.setUsId(us_id);
                        updateUrgentShipmentStatusRequest.setButtonType(buttonType);
                        updateUrgentShipmentStatusRequest.setConnoteNo(connoteNumber);
                        updateUrgentShipmentStatusRequest.setHandoverToId(handoverUserId);
                        updateUrgentShipmentStatusRequest.setHandoverToName(handoverName);
                        updateUrgentShipmentStatusRequest.setLatitude(String.valueOf(latitude));
                        updateUrgentShipmentStatusRequest.setLongitude(String.valueOf(longitude));
                        nfodrsViewModel.updateStatusOfNFO(updateUrgentShipmentStatusRequest);

                        mProgressBar.show();

                        mCriticalogSharedPreferences.saveData("connote_no", connoteNumber);
                        mCriticalogSharedPreferences.saveData("task_type", buttonType);
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }


    public void storeLatLong() {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c);

        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        String time = df1.format(c);


        ArrayList<PodClaimPojo> latlonglist = dbHelper.getAllLatLongList();


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String convertedstring = "";
                ArrayList<PodClaimPojo> latlonglist = dbHelper.getAllLatLongList();

                if (latlonglist.size() > 0) {
                    int listsize = latlonglist.size();
                    PodClaimPojo podClaimPojo;

                    double distnce = distanceCalc(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                    convertedstring = converttorealnumber(distnce);
                    String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);

                    if (convertedstring_g.equalsIgnoreCase("0.0")) {
                        convertedstring_g = convertedstring;
                    }
                    podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("connote_no"), mCriticalogSharedPreferences.getData("task_type"), c.toString(), 200);

                    Log.e("STORED_LAT_LNG", String.valueOf(latitude) + "" + String.valueOf(longitude));

                    latlonglist.add(podClaimPojo);
                    dbHelper.insertLatLOngDetails(podClaimPojo);

                } else {
                    PodClaimPojo podClaimPojo = null;
                    if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {
                        String lastLatLongList = dbHelper.getLastLatLongDetails();
                        if (!lastLatLongList.equals("-")) {
                            String[] bits = lastLatLongList.split("-");
                            String lastOne = bits[bits.length - 1];

                            double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                            convertedstring = converttorealnumber(distnce);
                            String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                convertedstring_g = convertedstring;
                            }
                            podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("connote_no"), mCriticalogSharedPreferences.getData("task_type"), c.toString(), 200);

                        }
                    } else {
                        String lastLatLongList = dbHelper.getLastLatLongDetails();
                        if (!lastLatLongList.equals("-")) {
                            String[] bits = lastLatLongList.split("-");
                            String lastOne = bits[bits.length - 1];
                            double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                            convertedstring = converttorealnumber(distnce);
                            String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                convertedstring_g = convertedstring;
                            }
                            podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("connote_no"), mCriticalogSharedPreferences.getData("task_type"), c.toString(), 200);
                        }
                    }

                    latlonglist.add(podClaimPojo);
                    dbHelper.insertLatLOngDetails(podClaimPojo);


                    mCriticalogSharedPreferences.saveData("start_date", date);
                    mCriticalogSharedPreferences.saveData("start_time", time);
                }
                mCriticalogSharedPreferences.saveDoubleData("last_activity_lat", latitude);
                mCriticalogSharedPreferences.saveDoubleData("last_activity_long", longitude);
            }
        });

        thread.start();
    }

    public String getDistanceFromGoogleAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(getContext());
        String distnace = "0";

        float distanceInKm = 0;
        String nextBillionHitStatus = mCriticalogSharedPreferences.getData("distance_api_status");
        if (nextBillionHitStatus.equalsIgnoreCase("1")) {
            distnace = mCriticalogSharedPreferences.getDistanceFromGoogelBillionAPI(lat1, lon1, lat2, lon2);

            distanceInKm = convertMeterToKilometer(Float.valueOf(distnace));
        } else {
            distanceInKm = 0;
        }
        return String.valueOf(distanceInKm);
    }

    public static float convertMeterToKilometer(float meter) {
        return (float) (meter * 0.001);
    }

    private double distanceCalc(double lat1, double lon1, double lat2, double lon2) {
        double distance;
        double distance1;
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(lon1);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(lon2);

        distance = startPoint.distanceTo(endPoint);

        distance1 = distance / 1000;

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        System.out.println(numberFormat.format(distance1));

        return Double.parseDouble(numberFormat.format(distance1));
    }

    public String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });
        }
    }
}