package com.criticalog.ecritica.nfofragment.ui.nfoprs.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.NFOPRSStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFOPRSStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentPRSRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentPRSResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NFOPrsRepository {
    private RestServices mRestServices;
    private MutableLiveData<NFOPRSStatusListResponse> nfoprsStatusListResponseMutableLiveData;
    private MutableLiveData<UpdateUrgentShipmentPRSResponse> updateUrgentShipmentPRSResponseMutableLiveData;

    public NFOPrsRepository() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        nfoprsStatusListResponseMutableLiveData = new MutableLiveData<>();
        updateUrgentShipmentPRSResponseMutableLiveData = new MutableLiveData<>();
    }

    public void getNFOPrsStatusList(NFOPRSStatusListRequest nfoprsStatusListRequest) {
        Call<NFOPRSStatusListResponse> nfoprsStatusListResponseCall = mRestServices.getNfoPRSStatusList(nfoprsStatusListRequest);
        nfoprsStatusListResponseCall.enqueue(new Callback<NFOPRSStatusListResponse>() {
            @Override
            public void onResponse(Call<NFOPRSStatusListResponse> call, Response<NFOPRSStatusListResponse> response) {
                nfoprsStatusListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<NFOPRSStatusListResponse> call, Throwable t) {
                nfoprsStatusListResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void updateUrgentShipmentPRS(UpdateUrgentShipmentPRSRequest updateUrgentShipmentPRSRequest) {
        Call<UpdateUrgentShipmentPRSResponse> updateUrgentShipmentPRSResponseCall = mRestServices.updateUrgentShipmentPRSStatus(updateUrgentShipmentPRSRequest);
        updateUrgentShipmentPRSResponseCall.enqueue(new Callback<UpdateUrgentShipmentPRSResponse>() {
            @Override
            public void onResponse(Call<UpdateUrgentShipmentPRSResponse> call, Response<UpdateUrgentShipmentPRSResponse> response) {
                updateUrgentShipmentPRSResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<UpdateUrgentShipmentPRSResponse> call, Throwable t) {
                updateUrgentShipmentPRSResponseMutableLiveData.setValue(null);
            }
        });
    }

    public LiveData<NFOPRSStatusListResponse> getNFOStatusList() {
        return nfoprsStatusListResponseMutableLiveData;
    }

    public LiveData<UpdateUrgentShipmentPRSResponse> updateNFOPrsStatus() {
        return updateUrgentShipmentPRSResponseMutableLiveData;
    }
}
