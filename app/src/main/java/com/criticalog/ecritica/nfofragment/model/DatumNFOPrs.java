package com.criticalog.ecritica.nfofragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumNFOPrs {
    @SerializedName("us_id")
    @Expose
    private String usId;
    @SerializedName("brn_no")
    @Expose
    private String brnNo;
    @SerializedName("allocated_id")
    @Expose
    private String allocatedId;
    @SerializedName("allocated_name")
    @Expose
    private String allocatedName;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("accept")
    @Expose
    private String accept;
    @SerializedName("handover_to_id")
    @Expose
    private String handoverToId;
    @SerializedName("handover_to_name")
    @Expose
    private String handoverToName;
    @SerializedName("prs")
    @Expose
    private String prs;
    @SerializedName("close")
    @Expose
    private String close;
    @SerializedName("handover")
    @Expose
    private String handover;
    @SerializedName("prs_link")
    @Expose
    private String prsLink;
    @SerializedName("handover_from_id")
    @Expose
    private String handoverFromId;
    @SerializedName("handover_from_name")
    @Expose
    private String handoverFromName;
    @SerializedName("connect")
    @Expose
    private String connect;
    @SerializedName("accept_button_send")
    @Expose
    private String acceptButtonSend;
    @SerializedName("connect_button_send")
    @Expose
    private String connectButtonSend;
    @SerializedName("handover_button_send")
    @Expose
    private String handoverButtonSend;
    @SerializedName("prs_button_send")
    @Expose
    private String prsButtonSend;
    @SerializedName("close_button_send")
    @Expose
    private String closeButtonSend;
    @SerializedName("accept_button")
    @Expose
    private String acceptButton;
    @SerializedName("connect_button")
    @Expose
    private String connectButton;
    @SerializedName("handover_button")
    @Expose
    private String handoverButton;
    @SerializedName("prs_button")
    @Expose
    private String prsButton;
    @SerializedName("close_button")
    @Expose
    private String closeButton;

    @SerializedName("cancelled_status")
    @Expose
    private String cancelled_status;

    @SerializedName("cancelled_status_button")
    @Expose
    private String cancelled_status_button;

    public String getCancelled_status() {
        return cancelled_status;
    }

    public void setCancelled_status(String cancelled_status) {
        this.cancelled_status = cancelled_status;
    }

    public String getCancelled_status_button() {
        return cancelled_status_button;
    }

    public void setCancelled_status_button(String cancelled_status_button) {
        this.cancelled_status_button = cancelled_status_button;
    }

    public String getUsId() {
        return usId;
    }

    public void setUsId(String usId) {
        this.usId = usId;
    }

    public String getBrnNo() {
        return brnNo;
    }

    public void setBrnNo(String brnNo) {
        this.brnNo = brnNo;
    }

    public String getAllocatedId() {
        return allocatedId;
    }

    public void setAllocatedId(String allocatedId) {
        this.allocatedId = allocatedId;
    }

    public String getAllocatedName() {
        return allocatedName;
    }

    public void setAllocatedName(String allocatedName) {
        this.allocatedName = allocatedName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getHandoverToId() {
        return handoverToId;
    }

    public void setHandoverToId(String handoverToId) {
        this.handoverToId = handoverToId;
    }

    public String getHandoverToName() {
        return handoverToName;
    }

    public void setHandoverToName(String handoverToName) {
        this.handoverToName = handoverToName;
    }

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getHandover() {
        return handover;
    }

    public void setHandover(String handover) {
        this.handover = handover;
    }

    public String getPrsLink() {
        return prsLink;
    }

    public void setPrsLink(String prsLink) {
        this.prsLink = prsLink;
    }

    public String getHandoverFromId() {
        return handoverFromId;
    }

    public void setHandoverFromId(String handoverFromId) {
        this.handoverFromId = handoverFromId;
    }

    public String getHandoverFromName() {
        return handoverFromName;
    }

    public void setHandoverFromName(String handoverFromName) {
        this.handoverFromName = handoverFromName;
    }

    public String getConnect() {
        return connect;
    }

    public void setConnect(String connect) {
        this.connect = connect;
    }

    public String getAcceptButtonSend() {
        return acceptButtonSend;
    }

    public void setAcceptButtonSend(String acceptButtonSend) {
        this.acceptButtonSend = acceptButtonSend;
    }

    public String getConnectButtonSend() {
        return connectButtonSend;
    }

    public void setConnectButtonSend(String connectButtonSend) {
        this.connectButtonSend = connectButtonSend;
    }

    public String getHandoverButtonSend() {
        return handoverButtonSend;
    }

    public void setHandoverButtonSend(String handoverButtonSend) {
        this.handoverButtonSend = handoverButtonSend;
    }

    public String getPrsButtonSend() {
        return prsButtonSend;
    }

    public void setPrsButtonSend(String prsButtonSend) {
        this.prsButtonSend = prsButtonSend;
    }

    public String getCloseButtonSend() {
        return closeButtonSend;
    }

    public void setCloseButtonSend(String closeButtonSend) {
        this.closeButtonSend = closeButtonSend;
    }

    public String getAcceptButton() {
        return acceptButton;
    }

    public void setAcceptButton(String acceptButton) {
        this.acceptButton = acceptButton;
    }

    public String getConnectButton() {
        return connectButton;
    }

    public void setConnectButton(String connectButton) {
        this.connectButton = connectButton;
    }

    public String getHandoverButton() {
        return handoverButton;
    }

    public void setHandoverButton(String handoverButton) {
        this.handoverButton = handoverButton;
    }

    public String getPrsButton() {
        return prsButton;
    }

    public void setPrsButton(String prsButton) {
        this.prsButton = prsButton;
    }

    public String getCloseButton() {
        return closeButton;
    }

    public void setCloseButton(String closeButton) {
        this.closeButton = closeButton;
    }
}
