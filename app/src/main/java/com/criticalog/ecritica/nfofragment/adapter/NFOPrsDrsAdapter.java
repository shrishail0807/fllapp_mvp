package com.criticalog.ecritica.nfofragment.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.nfofragment.interfaces.OnclickOfNfo;
import com.criticalog.ecritica.nfofragment.model.DatumNFODrs;

import java.util.List;

public class NFOPrsDrsAdapter extends RecyclerView.Adapter<NFOPrsDrsAdapter.NFOPrsDrsAdapterViewHolder> {
    Context context;
    int flag;
    RecyclerView.SmoothScroller smoothScroller;
    List<DatumNFODrs> datumNFODrs;
    OnclickOfNfo onclickOfNfo;

    public NFOPrsDrsAdapter(Context context, int falg, List<DatumNFODrs> datumNFODrs, OnclickOfNfo onclickOfNfo) {
        this.context = context;
        this.flag = falg;
        this.datumNFODrs = datumNFODrs;
        this.onclickOfNfo = onclickOfNfo;
    }

    @NonNull
    @Override
    public NFOPrsDrsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_nfo_prs_drs, parent, false);
        return new NFOPrsDrsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NFOPrsDrsAdapterViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.mDrsConnoteNumber.setText("Connote Number: " + datumNFODrs.get(position).getConnoteNo());

        if (datumNFODrs.get(position).getAcceptButton().equalsIgnoreCase("Accepted") || datumNFODrs.get(position).getAcceptButton().equalsIgnoreCase("Accept")) {
            holder.mAccepted.setText(datumNFODrs.get(position).getAcceptButton());
        } else {
            holder.mAccepted.setText(datumNFODrs.get(position).getAcceptButton());
            holder.mAccepted.setTextSize(14);
            holder.mAccepted.getLayoutParams().width = 400;
        }

        holder.mRetrieve.setText(datumNFODrs.get(position).getRetrieveButton());
        holder.mDrsLink.setText(datumNFODrs.get(position).getDrsButton());
        holder.mClose.setText(datumNFODrs.get(position).getCloseButton());
        holder.mHandover.setText(datumNFODrs.get(position).getHandover_button());

        holder.mAccepted.setPadding(15, 15, 15, 15);
        holder.mRetrieve.setPadding(15, 15, 15, 15);
        holder.mDrsLink.setPadding(15, 15, 15, 15);
        holder.mClose.setPadding(15, 15, 15, 15);
        holder.mHandover.setPadding(15, 15, 15, 15);

        if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("0")) {
            holder.mAccepted.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("-1")) {

            holder.mAcceptedLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("0")) {
            holder.mRetrieve.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("-1")) {

            holder.mRetrieveLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getDrs().equalsIgnoreCase("0")) {
            holder.mDrsLink.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getDrs().equalsIgnoreCase("-1")) {

            holder.mDrsLinkLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getClose().equalsIgnoreCase("0")) {
            holder.mClose.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getClose().equalsIgnoreCase("-1")) {

            holder.mCloseLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("0")) {
            holder.mHandover.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("-1")) {

            holder.mHandoverLay.setVisibility(View.GONE);
        }

        holder.mAccepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                            datumNFODrs.get(position).getAccept_button_send(),
                            datumNFODrs.get(position).getConnoteNo(),
                            datumNFODrs.get(position).getHandoverToId(),
                            datumNFODrs.get(position).getHandoverToName());
                } else if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("0")) {
                    Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.mRetrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous step", Toast.LENGTH_SHORT).show();
                } else {
                    if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getRetrieve_button_send(),
                                datumNFODrs.get(position).getConnoteNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        holder.mDrsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else {
                    if (datumNFODrs.get(position).getDrsLink().equalsIgnoreCase("1")) {
                        Intent intent = new Intent(context, DRSActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    } else if (datumNFODrs.get(position).getDrs().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getDrs_button_send(),
                                datumNFODrs.get(position).getConnoteNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getDrs().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }

                /*holder.mHandover.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "Complete previous Step!", Toast.LENGTH_SHORT).show();
                        } else if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "Complete previous Step!", Toast.LENGTH_SHORT).show();
                        } else {
                            if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                                onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                        datumNFODrs.get(position).getCloseButton(),
                                        datumNFODrs.get(position).getConnoteNo(),
                                        datumNFODrs.get(position).getHandoverToId(),
                                        datumNFODrs.get(position).getHandoverToName());
                            } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("0")) {
                                Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });*/
            }
        });

        holder.mHandover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else {
                    if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getHandover_button_send(),
                                datumNFODrs.get(position).getConnoteNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        holder.mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getRetrieve().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getDrs().equalsIgnoreCase("1")) {

                    if (datumNFODrs.get(position).getDrsLink().equalsIgnoreCase("1")) {
                        Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (datumNFODrs.get(position).getClose().equalsIgnoreCase("1")) {
                            onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                    datumNFODrs.get(position).getClose_button_send(),
                                    datumNFODrs.get(position).getConnoteNo(),
                                    datumNFODrs.get(position).getHandoverToId(),
                                    datumNFODrs.get(position).getHandoverToName());
                        } else if (datumNFODrs.get(position).getClose().equalsIgnoreCase("0")) {
                            Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    if (datumNFODrs.get(position).getClose().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getClose_button_send(),
                                datumNFODrs.get(position).getConnoteNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getClose().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return datumNFODrs.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class NFOPrsDrsAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView xclientname, xconsignee, mReachedAirport, clientcode, mDrsConnoteNumber,
                mAccepted, mRetrieve, mDrsLink, mClose, mHandover;
        ImageView mArrow1, mArrow2, mArrow3;
        private RecyclerView mRvEvents;
        private RelativeLayout mAcceptedLay, mRetrieveLay, mDrsLinkLay, mCloseLay, mHandoverLay;

        public NFOPrsDrsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            mDrsConnoteNumber = itemView.findViewById(R.id.mDrsConnoteNumber);
            mAccepted = itemView.findViewById(R.id.mAccepted);
            mRetrieve = itemView.findViewById(R.id.mRetrieve);
            mDrsLink = itemView.findViewById(R.id.mDrsConnote);
            mClose = itemView.findViewById(R.id.mClose);
            mAcceptedLay = itemView.findViewById(R.id.mAcceptedLay);
            mRetrieveLay = itemView.findViewById(R.id.mRetrieveLay);
            mDrsLinkLay = itemView.findViewById(R.id.mDrsLinkLay);
            mCloseLay = itemView.findViewById(R.id.mCloseLay);
            mHandoverLay = itemView.findViewById(R.id.mHandoverLay);
            mHandover = itemView.findViewById(R.id.mHandover);
        }
    }
}
