package com.criticalog.ecritica.nfofragment.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.nfofragment.interfaces.OnclickOfNfo;
import com.criticalog.ecritica.nfofragment.model.DatumNFOPrs;

import java.util.List;

public class NFOPRSAdapter extends RecyclerView.Adapter<NFOPRSAdapter.NFOPrsAdapterViewHolder> {
    Context context;
    int flag;
    List<DatumNFOPrs> datumNFODrs;
    OnclickOfNfo onclickOfNfo;

    public NFOPRSAdapter(Context context, int falg, List<DatumNFOPrs> datumNFODrs, OnclickOfNfo onclickOfNfo) {
        this.context = context;
        this.flag = falg;
        this.datumNFODrs = datumNFODrs;
        this.onclickOfNfo = onclickOfNfo;
    }

    @NonNull
    @Override
    public NFOPRSAdapter.NFOPrsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_nfo_prs, parent, false);
        return new NFOPRSAdapter.NFOPrsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NFOPRSAdapter.NFOPrsAdapterViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.mPRSBrnNumber.setText("BRN Number: " + datumNFODrs.get(position).getBrnNo());
        if (datumNFODrs.get(position).getAcceptButton().equalsIgnoreCase("Accepted") || datumNFODrs.get(position).getAcceptButton().equalsIgnoreCase("Accept")) {
            holder.mAccepted.setText(datumNFODrs.get(position).getAcceptButton());
        } else {
            holder.mAccepted.setText(datumNFODrs.get(position).getAcceptButton());
            holder.mAccepted.setTextSize(14);
            holder.mAccepted.getLayoutParams().width = 400;
        }

        holder.mReachedAirport.setText(datumNFODrs.get(position).getConnectButton());
        holder.mPRSLink.setText(datumNFODrs.get(position).getPrsButton());
        holder.mClose.setText(datumNFODrs.get(position).getCloseButton());
        holder.mHandover.setText(datumNFODrs.get(position).getHandoverButton());

        holder.mAccepted.setPadding(15, 15, 15, 15);
        holder.mReachedAirport.setPadding(15, 15, 15, 15);
        holder.mPRSLink.setPadding(15, 15, 15, 15);
        holder.mClose.setPadding(15, 15, 15, 15);
        holder.mHandover.setPadding(15, 15, 15, 15);

        if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("0")) {
            holder.mAccepted.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("-1")) {

            holder.mAcceptedLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getConnect().equalsIgnoreCase("0")) {
            holder.mReachedAirport.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getConnect().equalsIgnoreCase("-1")) {

            holder.mReachedAirportLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getPrs().equalsIgnoreCase("0")) {
            holder.mPRSLink.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getPrs().equalsIgnoreCase("-1")) {

            holder.mPRSLinkLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getClose().equalsIgnoreCase("0")) {
            holder.mClose.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getClose().equalsIgnoreCase("-1")) {

            holder.mCloseLay.setVisibility(View.GONE);
        }
        if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("0")) {
            holder.mHandover.setBackgroundResource(R.drawable.rounded_bacground_green_nfo);

        } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("-1")) {

            holder.mHandoverLay.setVisibility(View.GONE);
        }

        if (datumNFODrs.get(position).getCancelled_status().equalsIgnoreCase("1")) {
            holder.mAcceptedLay.setVisibility(View.GONE);
            holder.mReachedAirportLay.setVisibility(View.GONE);
            holder.mPRSLinkLay.setVisibility(View.GONE);
            holder.mCloseLay.setVisibility(View.GONE);
            holder.mHandoverLay.setVisibility(View.GONE);
            holder.mPrsCancelDoneMessageLay.setVisibility(View.VISIBLE);
            holder.mPrsCancelMessage.setText(datumNFODrs.get(position).getCancelled_status_button());
        }
        holder.mAccepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                            datumNFODrs.get(position).getAcceptButtonSend(),
                            datumNFODrs.get(position).getBrnNo(),
                            datumNFODrs.get(position).getHandoverToId(),
                            datumNFODrs.get(position).getHandoverToName());
                } else if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("0")) {
                    Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.mReachedAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous step", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getPrsLink().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous step", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous step", Toast.LENGTH_SHORT).show();
                } else {
                    if (datumNFODrs.get(position).getConnect().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getConnectButtonSend(),
                                datumNFODrs.get(position).getBrnNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getConnect().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        holder.mPRSLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else {
                    if (datumNFODrs.get(position).getPrsLink().equalsIgnoreCase("1")) {
                        Intent intent = new Intent(context, PickupActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    } else if (datumNFODrs.get(position).getPrs().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getPrsButtonSend(),
                                datumNFODrs.get(position).getBrnNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getPrs().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        holder.mHandover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getPrsLink().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else {
                    if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getHandoverButtonSend(),
                                datumNFODrs.get(position).getBrnNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        holder.mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumNFODrs.get(position).getAccept().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getHandover().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getConnect().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                } else if (datumNFODrs.get(position).getPrs().equalsIgnoreCase("1")) {
                    if (datumNFODrs.get(position).getPrsLink().equalsIgnoreCase("1")) {
                        Toast.makeText(context, "Complete Previous Step!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (datumNFODrs.get(position).getClose().equalsIgnoreCase("1")) {
                            onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                    datumNFODrs.get(position).getCloseButtonSend(),
                                    datumNFODrs.get(position).getBrnNo(),
                                    datumNFODrs.get(position).getHandoverToId(),
                                    datumNFODrs.get(position).getHandoverToName());
                        } else if (datumNFODrs.get(position).getClose().equalsIgnoreCase("0")) {
                            Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    if (datumNFODrs.get(position).getClose().equalsIgnoreCase("1")) {
                        onclickOfNfo.onclickNFOShipmet(datumNFODrs.get(position).getUsId(),
                                datumNFODrs.get(position).getCloseButtonSend(),
                                datumNFODrs.get(position).getBrnNo(),
                                datumNFODrs.get(position).getHandoverToId(),
                                datumNFODrs.get(position).getHandoverToName());
                    } else if (datumNFODrs.get(position).getClose().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Status Updated Already!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datumNFODrs.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class NFOPrsAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mReachedAirport, mPRSBrnNumber,
                mAccepted, mPRSLink, mClose, mHandover, mPrsCancelMessage;
        ImageView mArrow1, mArrow2, mArrow3;
        private RecyclerView mRvEvents;
        private RelativeLayout mAcceptedLay, mPRSLinkLay, mCloseLay, mHandoverLay, mReachedAirportLay,
                mPrsCancelDoneMessageLay;

        public NFOPrsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            mPRSBrnNumber = itemView.findViewById(R.id.mPRSBrnNumber);
            mAccepted = itemView.findViewById(R.id.mAccepted);
            mPRSLink = itemView.findViewById(R.id.mPRSlink);
            mClose = itemView.findViewById(R.id.mClose);
            mAcceptedLay = itemView.findViewById(R.id.mAcceptedLay);
            mPRSLinkLay = itemView.findViewById(R.id.mPRSLinkLay);
            mCloseLay = itemView.findViewById(R.id.mCloseLay);
            mHandoverLay = itemView.findViewById(R.id.mHandoverLay);
            mHandover = itemView.findViewById(R.id.mHandover);
            mReachedAirportLay = itemView.findViewById(R.id.mReachedAirportLay);
            mReachedAirport = itemView.findViewById(R.id.mReachedAirport);
            mPrsCancelDoneMessageLay = itemView.findViewById(R.id.mPrsCancelDoneMessageLay);
            mPrsCancelMessage = itemView.findViewById(R.id.mPrsCancelMessage);
        }
    }
}
