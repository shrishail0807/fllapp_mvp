package com.criticalog.ecritica.nfofragment.ui.nfoprs;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.NFOPRSStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFOPRSStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentPRSRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentPRSResponse;
import com.criticalog.ecritica.nfofragment.ui.nfodrs.repository.NFODrsRepository;
import com.criticalog.ecritica.nfofragment.ui.nfoprs.repository.NFOPrsRepository;

public class NFOPRSViewModel extends AndroidViewModel {
    private NFOPrsRepository nfoPrsRepository;
    private LiveData<NFOPRSStatusListResponse> nfoprsStatusListResponseLiveData;
    private LiveData<UpdateUrgentShipmentPRSResponse> updateUrgentShipmentPRSResponseLiveData;

    public NFOPRSViewModel(@NonNull Application application) {
        super(application);
    }

    void init() {
        nfoPrsRepository = new NFOPrsRepository();
        nfoprsStatusListResponseLiveData = nfoPrsRepository.getNFOStatusList();
        updateUrgentShipmentPRSResponseLiveData = nfoPrsRepository.updateNFOPrsStatus();

        // updateUrgentShipmentStatusResponseLiveData=nfoDrsRepository.updateUrgentShipmet();
    }

    //Get PRS NFO List
    public void getNfoPRSStatusList(NFOPRSStatusListRequest nfoprsStatusListRequest) {
        nfoPrsRepository.getNFOPrsStatusList(nfoprsStatusListRequest);
    }

    public LiveData<NFOPRSStatusListResponse> getNFOPRSStatusListLiveData() {
        return nfoprsStatusListResponseLiveData;
    }

    //Update PRS NFO
    public void updateNFOPrsStatus(UpdateUrgentShipmentPRSRequest updateUrgentShipmentPRSRequest) {
        nfoPrsRepository.updateUrgentShipmentPRS(updateUrgentShipmentPRSRequest);
    }

    public LiveData<UpdateUrgentShipmentPRSResponse> updateNFOPRSStatusLiveData() {
        return updateUrgentShipmentPRSResponseLiveData;
    }
}