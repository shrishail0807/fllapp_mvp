package com.criticalog.ecritica.nfofragment.ui.nfodrs.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.mvproundcreation.model.GetVehicleTypeTwoOrFourResponse;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListRequest;
import com.criticalog.ecritica.nfofragment.model.NFODrsStatusListResponse;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusRequest;
import com.criticalog.ecritica.nfofragment.model.UpdateUrgentShipmentStatusResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NFODrsRepository {
    private RestServices mRestServices;
    private MutableLiveData<NFODrsStatusListResponse> nfoDrsStatusListResponseMutableLiveData;
    private MutableLiveData<UpdateUrgentShipmentStatusResponse> updateUrgentShipmentStatusResponseMutableLiveData;

    public NFODrsRepository() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        nfoDrsStatusListResponseMutableLiveData = new MutableLiveData<>();
        updateUrgentShipmentStatusResponseMutableLiveData = new MutableLiveData<>();

    }

    public void getNfoDrsStatusList(NFODrsStatusListRequest nfoDrsStatusListRequest) {
        Call<NFODrsStatusListResponse> nfoDrsStatusListResponseCall = mRestServices.getNfoDrsStatusList(nfoDrsStatusListRequest);
        nfoDrsStatusListResponseCall.enqueue(new Callback<NFODrsStatusListResponse>() {
            @Override
            public void onResponse(Call<NFODrsStatusListResponse> call, Response<NFODrsStatusListResponse> response) {
                nfoDrsStatusListResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<NFODrsStatusListResponse> call, Throwable t) {
                nfoDrsStatusListResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void updateUrgentShipmentStatus(UpdateUrgentShipmentStatusRequest updateUrgentShipmentStatusRequest) {
        Call<UpdateUrgentShipmentStatusResponse> updateUrgentShipmentStatusResponseCall = mRestServices.updateUrgentShipmentStatus(updateUrgentShipmentStatusRequest);
        updateUrgentShipmentStatusResponseCall.enqueue(new Callback<UpdateUrgentShipmentStatusResponse>() {
            @Override
            public void onResponse(Call<UpdateUrgentShipmentStatusResponse> call, Response<UpdateUrgentShipmentStatusResponse> response) {
                updateUrgentShipmentStatusResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<UpdateUrgentShipmentStatusResponse> call, Throwable t) {
                updateUrgentShipmentStatusResponseMutableLiveData.setValue(null);
            }
        });
    }

    public LiveData<NFODrsStatusListResponse> getNFODrsStatusList() {
        return nfoDrsStatusListResponseMutableLiveData;
    }

    public LiveData<UpdateUrgentShipmentStatusResponse> updateUrgentShipmet() {
        return updateUrgentShipmentStatusResponseMutableLiveData;
    }
}
