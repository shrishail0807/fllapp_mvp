package com.criticalog.ecritica.network;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by anujgupta on 26/12/17.
 */

public class NetworkClient {

    public static Retrofit retrofit;

    public void NetworkClient(){

    }

    public static Retrofit getRetrofit(){

        if(retrofit==null){
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            OkHttpClient okHttpClient = builder.build();

            retrofit = new Retrofit.Builder()

                       // .baseUrl("http://criticalogistics.com/Agasthya/criticagit/criticalog-api/api/v1/user/")
                         .baseUrl(" http://192.168.43.129/criticagit/criticalog-api/api/v1/user/")
                        .addConverterFactory(GsonConverterFactory.create())
                       // .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                        .client(okHttpClient)
                        .build();
        }

        return retrofit;
    }
}
