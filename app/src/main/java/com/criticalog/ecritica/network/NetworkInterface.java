package com.criticalog.ecritica.network;


import com.criticalog.ecritica.model.LoginResponse;
import com.criticalog.ecritica.model.PickupResponse;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface NetworkInterface {

    @Headers("Content-Type: application/json")
    @POST("login")
    Observable<LoginResponse> signin(@Body JsonObject login);

    @Headers("Content-Type: application/json")
    @POST("pickuplist")
    Observable<PickupResponse> getPickupList(@Body JsonObject pickup);



/*    @GET("search/movie")
    Observable<MovieResponse> getMoviesBasedOnQuery(@Query("api_key") String api_key, @Query("query") String q);*/
}
