package com.criticalog.ecritica.MVPPodScan;

import com.criticalog.ecritica.MVPPodScan.Model.PODUploadRequest;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadResponse;

public class PODPresenterImpl implements PodUploadContract.presenter, PodUploadContract.GetPODUploadIntractor.OnFinishedListener {

    private PodUploadContract.MainView mainView;
    private PodUploadContract.GetPODUploadIntractor getPODUploadIntractor;

    public PODPresenterImpl(PodUploadContract.MainView mainView, PodUploadContract.GetPODUploadIntractor getPODUploadIntractor)
    {
        this.mainView=mainView;
        this.getPODUploadIntractor=getPODUploadIntractor;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void podUpload(PODUploadRequest podUploadRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        getPODUploadIntractor.podUploadRequest(this,podUploadRequest);
    }

    @Override
    public void onFinished(PODUploadResponse podUploadResponse) {
        if(mainView!=null)
        {
            mainView.setPODResponseToViews(podUploadResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
