package com.criticalog.ecritica.MVPPodScan;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadRequest;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.BlurPercentageDetection;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.tensorflow.lite.Interpreter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class PodScanActivity extends AppCompatActivity implements View.OnClickListener, PodUploadContract.MainView {

    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;
    EditText mEdpDocketNumber;
    ImageView mPODImage, xtv_backbutton;
    TextView mPODUpload;
    private static final int PERMISSION_REQUEST_POD_IMAGE = 1888;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private String docketNumber = "", base64PODmage = "";
    File photoFile = null;
    private LinearLayout mLinearLayMain;
    private PodUploadContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    private SimpleArcDialog mProgressDialog;
    private LocationCallback locationCallback;
    private BarcodeDetector detector;
    private Dialog dialogProductDelivered1, dialogManualEntry;
    private TextView mDialogText;
    private TextView mOkDialog, mPodImageText;
    private String useVision = "";
    private LinearLayout mRLManualConnote, mQrScan;
    private String docketno = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pod_scan);

        dialogProductDelivered1 = new Dialog(PodScanActivity.this);
        dialogProductDelivered1.setContentView(R.layout.dialog_pod_scan_not_done);

        detector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.ALL_FORMATS | Barcode.ALL_FORMATS)
                .build();

        if (!detector.isOperational()) {
            Toast.makeText(this, "Detector initialisation failed", Toast.LENGTH_SHORT).show();
            return;
        }
        //Loader
        mProgressDialog = new SimpleArcDialog(this);
        mProgressDialog.setConfiguration(new ArcConfiguration(this));
        mProgressDialog.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mPresenter = new PODPresenterImpl(this, new PODInteractorImpl());

        useVision = mCriticalogSharedPreferences.getData("image_vision");

        useVision = "0";

        mEdpDocketNumber = findViewById(R.id.mEdpDocketNumber);
        mPODImage = findViewById(R.id.mPODImage);
        mPODUpload = findViewById(R.id.mPODUpload);
        mLinearLayMain = findViewById(R.id.mLinearLayMain);
        xtv_backbutton = findViewById(R.id.xtv_backbutton);
        mRLManualConnote = findViewById(R.id.mRLManualConnote);
        mQrScan = findViewById(R.id.mQrScan);
        mPodImageText = findViewById(R.id.mPodImageText);

        mEdpDocketNumber.setOnClickListener(this);
        mPODImage.setOnClickListener(this);
        mPODUpload.setOnClickListener(this);
        xtv_backbutton.setOnClickListener(this);
        mRLManualConnote.setOnClickListener(this);
        mQrScan.setOnClickListener(this);

        checkGpsEnabledOrNot();

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(100); // 10 seconds
        locationRequest.setFastestInterval(100); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        userId = mCriticalogSharedPreferences.getData("userId");


        getLocation();

        mRLManualConnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualDialog();

            }
        });

        mQrScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBarcodeScan();
            }
        });
        mEdpDocketNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 9) {


                }
            }
        });

    }


    private boolean isImageBlurred(Bitmap bitmap) {
        // Create a frame from the bitmap
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();

        // Create a TextRecognizer to detect blur
        TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();

        // Detect if the image is blurred
        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
        for (int i = 0; i < textBlocks.size(); i++) {
            TextBlock textBlock = textBlocks.valueAt(i);
            if (textBlock != null && textBlock.getComponents().size() > 0) {
                return false; // The image is not blurred
            }
        }
        return true; // The image is blurred
    }

    public void manualDialog() {
        EditText mDocketEntry;
        TextView mAddDocket;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(PodScanActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_scan);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);

        mDocketEntry = dialogManualEntry.findViewById(R.id.mDocketEntry);
        mAddDocket = dialogManualEntry.findViewById(R.id.mAddDocket);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mDocketEntry.setHint("Enter Connote Number");
        mAddDocket.setText("Check");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });


        mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketNumber = mDocketEntry.getText().toString();
                if (docketNumber.length() == 9) {

                    dialogManualEntry.dismiss();
                } else {
                    Toasty.warning(PodScanActivity.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });

    }



    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();

    }

    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(PodScanActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(PodScanActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PodScanActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(PodScanActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }


    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1888) {
            if (requestCode == PERMISSION_REQUEST_POD_IMAGE && resultCode == Activity.RESULT_OK) {
                File absoluteFile = photoFile.getAbsoluteFile();

                if (useVision.equalsIgnoreCase("1")) {
                    CropImage.activity(Uri.fromFile(photoFile))
                            .start(this);
                } else {
                    if (absoluteFile.exists()) {
                        mProgressDialog.show();
                        Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                 /*       // 1. Load the TensorFlow Lite model
                        Interpreter tflite = null;
                        try {
                            tflite = new Interpreter(loadModelFromAsset("model.tflite"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        // 3. Run the prediction
                        float[][] output = new float[1][2]; // Assuming output shape is (1, 2)
                        tflite.run(myBitmap, output);

// 4. Determine if the image is blurred
                        float probabilityBlur = output[0][0];
                        boolean isBlurred = probabilityBlur > 0.5; // Adjust the threshold as needed

// 5. Display the result or perform further actions
                        if (isBlurred) {
                            mPodImageText.setText("Image is blurred");
                        } else {
                            mPodImageText.setText("Image is not blurred");
                        }*/

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {

                                double percentage = BlurPercentageDetection.calculateBlurPercentage(PodScanActivity.this, myBitmap);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        // Update your view here
                                        // This code will run on the main thread

                                        mPodImageText.setText(String.valueOf(percentage));
                                        mProgressDialog.dismiss();
                                    }
                                });


                            }
                        });
                        thread.start();

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64PODmage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        mPODImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));

                    }
                }

            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            File absoluteFile = null;
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                absoluteFile = new File(resultUri.getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }

            try {
                Bitmap bitmap = decodeBitmapUri(this, Uri.fromFile(absoluteFile.getAbsoluteFile()));


              /*  boolean isBlurr = isImageBlurred(bitmap);
               if(isBlurr==true)
               {
                   Toasty.error(PodScanActivity.this, "BLUR IMAGE CAPTURE AGAIN", Toast.LENGTH_SHORT, true).show();
               }else {
                   Toast.makeText(this, "NOT A BLUR IMAGE", Toast.LENGTH_SHORT).show();
               }*/
                if (detector.isOperational() && bitmap != null) {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<Barcode> barcodes = detector.detect(frame);
                    for (int index = 0; index < barcodes.size(); index++) {
                        Barcode code = barcodes.valueAt(index);

                        Toast.makeText(this, code.displayValue, Toast.LENGTH_SHORT).show();
                        //txtResultBody.setText(txtResultBody.getText() + "\n" + code.displayValue + "\n");

                        // txtResultBody.setText(code.displayValue);

                        //imageView.setImageBitmap(bitmap);

                        Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64PODmage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        mPODImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));


                    }
                    if (barcodes.size() == 0) {
                        // txtResultBody.setText("No barcode could be detected. Please try again.");
                        //    Toast.makeText(this, "No barcode could be detected. Please try again.", Toast.LENGTH_SHORT).show();
                        mPODImage.setImageURI(null);
                        mPODImage.setImageResource(R.drawable.outline_photo_camera_24);
                        podScanAlertMesssage("Image captured was not clear!! Scan again");
                    }
                } else {
                    Toast.makeText(this, "Detector initialisation failed", Toast.LENGTH_SHORT).show();
                    // txtResultBody.setText("Detector initialisation failed");
                    podScanAlertMesssage("Detector initialisation failed");
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Failed to load Image", Toast.LENGTH_SHORT)
                        .show();
                Log.e("TAG", e.toString());
            }

        } else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            String scanwhat = mCriticalogSharedPreferences.getData("scan_what");
            if (result != null) {
                try {
                    String MobilePattern = "[0-9]{9}";
                    docketNumber = result.getContents();
                    if (docketNumber.length() == 9) {

                    } else {
                        Toasty.warning(PodScanActivity.this, "Scan Valid Docket!!", Toast.LENGTH_SHORT, true).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode

                }
            }
        }
    }

    private MappedByteBuffer loadModelFromAsset(String filename) throws IOException {
        AssetFileDescriptor fileDescriptor = this.getAssets().openFd(filename);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();

        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();

        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    public void podScanAlertMesssage(String message) {
        //Dialog Manual Entry
        mOkDialog = dialogProductDelivered1.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered1.findViewById(R.id.mDialogText);
        mDialogText.setText(message);
        dialogProductDelivered1.setCancelable(false);
        dialogProductDelivered1.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProductDelivered1.dismiss();
            }
        });
    }

    private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
        int targetW = 600;
        int targetH = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeStream(ctx.getContentResolver()
                .openInputStream(uri), null, bmOptions);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults.length > 0 && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    showLocationDialog("");

                }
                break;
        }
    }

    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finish();
                        // redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica");


                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                    /*    dialog.cancel();
                        finish();*/
                        requestPermission();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mPODUpload:
                if (docketNumber.length() != 9) {
                    Toasty.warning(this, "Enter Valid Docket Number", Toast.LENGTH_LONG, true).show();
                } else if (base64PODmage.equals("")) {
                    Toasty.warning(this, "Upload EDP Image!!", Toast.LENGTH_LONG, true).show();
                } else {
                    PODUploadRequest podUploadRequest = new PODUploadRequest();
                    podUploadRequest.setAction("upload_pod");
                    podUploadRequest.setDocketNo(docketNumber);
                    podUploadRequest.setUserId(userId);
                    podUploadRequest.setImage1(base64PODmage);
                    podUploadRequest.setLatitude(String.valueOf(latitude));
                    podUploadRequest.setLongitude(String.valueOf(longitude));
                    mPresenter.podUpload(podUploadRequest);
                }
                break;
            case R.id.mPODImage:
                if (docketNumber.equals("")) {

                    Toasty.warning(this, "Please Enter OR Scan the Docket Number", Toast.LENGTH_LONG, true).show();
                } else if (docketNumber.length() != 9) {
                    Toasty.warning(this, "Enter Valid Docket Number", Toast.LENGTH_LONG, true).show();
                } else {
                    String useVision = "0";
                    if (useVision.equalsIgnoreCase("1")) {
                        CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).start(PodScanActivity.this);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_POD_IMAGE);
                    }
                }
                break;

            case R.id.xtv_backbutton:
                finish();
                break;
        }
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.dismiss();
    }

    @Override
    public void setPODResponseToViews(PODUploadResponse podResponseToViews) {
        if (podResponseToViews.getStatus() == 200) {
            Toasty.success(PodScanActivity.this, podResponseToViews.getMessage(), Toast.LENGTH_SHORT, true).show();
            finish();
        } else {
            Toasty.warning(PodScanActivity.this, podResponseToViews.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }
}
