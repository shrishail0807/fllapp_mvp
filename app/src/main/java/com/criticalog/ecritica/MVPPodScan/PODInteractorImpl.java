package com.criticalog.ecritica.MVPPodScan;

import com.criticalog.ecritica.MVPPodScan.Model.PODUploadRequest;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PODInteractorImpl implements PodUploadContract.GetPODUploadIntractor {
    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void podUploadRequest(OnFinishedListener onFinishedListener, PODUploadRequest podUploadRequest) {

        Call<PODUploadResponse> podUploadResponseCall = service.podUpload(podUploadRequest);
        podUploadResponseCall.enqueue(new Callback<PODUploadResponse>() {
            @Override
            public void onResponse(Call<PODUploadResponse> call, Response<PODUploadResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PODUploadResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
