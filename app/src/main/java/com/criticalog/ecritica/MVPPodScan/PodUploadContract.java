package com.criticalog.ecritica.MVPPodScan;

import com.criticalog.ecritica.MVPEdpSdcCopy.EDPUploadContract;
import com.criticalog.ecritica.MVPEdpSdcCopy.model.ConnoteShortInfoResponse;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadRequest;
import com.criticalog.ecritica.MVPPodScan.Model.PODUploadResponse;

public interface PodUploadContract {
    interface presenter {

        void onDestroy();

        void podUpload(PODUploadRequest podUploadRequest);


    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setPODResponseToViews(PODUploadResponse podResponseToViews);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetPODUploadIntractor {

        interface OnFinishedListener {
            void onFinished(PODUploadResponse podUploadResponse);

            void onFailure(Throwable t);
        }

        void podUploadRequest(PodUploadContract.GetPODUploadIntractor.OnFinishedListener onFinishedListener, PODUploadRequest podUploadRequest);
    }
}
