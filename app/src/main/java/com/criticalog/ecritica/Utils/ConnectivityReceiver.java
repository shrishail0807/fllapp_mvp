package com.criticalog.ecritica.Utils;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.criticalog.ecritica.services.ProductDeliveredService;

/**
 * This class ensure the network connection changes and notify to the respective callbacks.
 * Created by jiteshmohite on 07/02/18.
 */

public class ConnectivityReceiver extends BroadcastReceiver {

    private ConnectivityReceiverListener mConnectivityReceiverListener;
    public static String TAG ="com.criticalog.ecritica.Utils.ConnectivityReciever";

    ConnectivityReceiver(ConnectivityReceiverListener listener) {
        mConnectivityReceiverListener = listener;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("inside connectivity ","reciever");
        mConnectivityReceiverListener.onNetworkConnectionChanged(isConnected(context));

    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);


        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        Log.d(TAG,"inside isconnected");
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
        if(isConnected){
            Log.d(TAG,"network connected" + "goto productdeliverservice");


          /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, ProductDeliveredService.class));
            } else {
                Intent serviceintent = new Intent(context, ProductDeliveredService.class);
                context.startService(serviceintent);
            }*/
          /*  Intent serviceintent = new Intent(context, ProductDeliveredService.class);
            context.startService(serviceintent);*/

        }else{
           // Toast.makeText(context, "Connection Disabled", Toast.LENGTH_SHORT).show();
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }
}
