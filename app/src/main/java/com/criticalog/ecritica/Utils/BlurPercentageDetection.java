package com.criticalog.ecritica.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

public class BlurPercentageDetection {

    public static double calculateBlurPercentage(Context context, Bitmap imageBitmap) {
        // Load the input image bitmap
        Bitmap bitmap = imageBitmap.copy(Bitmap.Config.ARGB_8888, true);

        // Convert the bitmap to mutable to allow modification
        bitmap = bitmap.copy(bitmap.getConfig(), true);

        // Apply blur using RenderScript
        RenderScript rs = RenderScript.create(context);
        Allocation input = Allocation.createFromBitmap(rs, bitmap);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // Set the blur radius (adjust as needed)
        float blurRadius = 10f;

        // Apply the blur
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setInput(input);
        script.setRadius(blurRadius);
        script.forEach(output);
        output.copyTo(bitmap);

        // Calculate the blur percentage
        double blurPercentage = calculateBitmapVariance(bitmap);

        // Release resources
        input.destroy();
        output.destroy();
        script.destroy();
        rs.destroy();

        return blurPercentage;
    }

    private static double calculateBitmapVariance(Bitmap bitmap) {
        long sum = 0;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        // Calculate the sum of squared pixel intensities
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixel = bitmap.getPixel(x, y);
                int intensity = (pixel & 0xFF);
                sum += intensity * intensity;
            }
        }

        // Calculate the variance
        double mean = (double) sum / (width * height);
        double variance = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixel = bitmap.getPixel(x, y);
                int intensity = (pixel & 0xFF);
                variance += (intensity - mean) * (intensity - mean);
            }
        }

        // Normalize the variance to a percentage
        double normalizedVariance = (variance / (width * height)) / (255 * 255);
        double blurPercentage = normalizedVariance * 100;

        return blurPercentage/10000;
    }
}

