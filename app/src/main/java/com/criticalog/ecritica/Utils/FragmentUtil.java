package com.criticalog.ecritica.Utils;

import android.content.Context;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.criticalog.ecritica.R;


/**
 * Created by ubuntu on 13/2/18.
 */

public class FragmentUtil {

    public static void addFragment(Fragment fragment, Context context, String TAG) {
        FragmentManager fragmentManage = ((AppCompatActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManage.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        Fragment currFragment = fragmentManage.findFragmentByTag(TAG);

        if (currFragment == null) {
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.addToBackStack(TAG);
            fragmentTransaction.commit();
        } else if (!currFragment.isVisible()) {
            fragmentManage.popBackStackImmediate();
        }

    }

    public static boolean isFragmentVisible(Context context, String TAG) {
        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        Fragment currFrg = fragmentManager.findFragmentByTag(TAG);
        if (currFrg != null) {
            if (currFrg.isVisible()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
