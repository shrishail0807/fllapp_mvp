package com.criticalog.ecritica.Utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;



public class Constants {
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 12 * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 150;

    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();
    static {
        // San Francisco International Airport.
        LANDMARKS.put("Criticalog Domlur", new LatLng(12.9563034,77.6386336));

      //  LANDMARKS.put("Criticalog whitefield", new LatLng(start_lat,start_long));

        // Googleplex.
        LANDMARKS.put("Criticalog ", new LatLng(12.980622,77.751964));

        // Test
        LANDMARKS.put("SFO", new LatLng(37.621313,-122.378955));


        //My Home
        LANDMARKS.put("Myhome", new LatLng(13.0446251,77.5674066));


    }
}
