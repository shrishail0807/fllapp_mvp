package com.criticalog.ecritica.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient1;
import com.criticalog.ecritica.Rest.RestServices1;
import com.criticalog.ecritica.model.BillionDistanceResponse;
//import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.io.ByteStreams;
//import com.google.firebase.crashlytics.buildtools.reloc.org.apache.commons.io.IOUtils;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class CriticalogSharedPreferences {

    private static CriticalogSharedPreferences yourPreference;
    private SharedPreferences sharedPreferences;
    private Context mContext;
    public static final String CriticalogPreferences = "criticalogpreferences";
    SimpleArcDialog mProgressDialog;
    RestServices1 mRestServices1;
    RequestQueue requestQueue;

    public static CriticalogSharedPreferences getInstance(Context context) {

        if (yourPreference == null) {
            yourPreference = new CriticalogSharedPreferences(context);
        }
        return yourPreference;
    }

    private CriticalogSharedPreferences(Context context) {
        this.mContext = context;
        sharedPreferences = context.getSharedPreferences("YourCustomNamedPreference", Context.MODE_PRIVATE);
      //  mRestServices1 = RestClient1.getRetrofitInstance().create(RestServices1.class);
        requestQueue = Volley.newRequestQueue(mContext);
    }

    public void saveData(String key, String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public String isToString(InputStream is) {
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        try {
            Reader in = new InputStreamReader(is, "UTF-8");
            for (; ; ) {
                int rsz = in.read(buffer, 0, buffer.length);
                if (rsz < 0)
                    break;
                out.append(buffer, 0, rsz);
            }
        } catch (Exception e) {

        }
        return out.toString();
    }

    public String
    getDistanceFromGoogelBillionAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        final String[] parsedDistance1 = new String[1];
        final String[] response = new String[1];

        try {
            URL url = new URL(StaticUtils.distance_api_url + "origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&key=" + StaticUtils.billionDistanceAPIKey);
            // URL url = new URL("https://api.nextbillion.io/directions/json?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&key=" + StaticUtils.billionDistanceAPIKey);
            Log.e("SHREEEE", String.valueOf(url));
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());

            String responseString = isToString(in);

            // If u enable buildtool library below code you can use but app size increases 10MB
            //response[0] = IOUtils.toString(in, "UTF-8");  //commented
            //JSONObject jsonObject = new JSONObject(response[0]); // commented

            // String total = new String(ByteStreams.toByteArray(in));

            JSONObject jsonObject = new JSONObject(responseString);
            JSONArray array = jsonObject.getJSONArray("routes");
            JSONObject routes = array.getJSONObject(0);
            JSONArray legs = routes.getJSONArray("legs");
            JSONObject steps = legs.getJSONObject(0);
            JSONObject distance = steps.getJSONObject("distance");
            String parsedDistance = distance.getString("value");

            parsedDistance1[0] = parsedDistance;
        } catch (ProtocolException e) {
            e.printStackTrace();
            Log.e("GOOGLE_EXCEPTION", e.toString());
            parsedDistance1[0] = "0";
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e("GOOGLE_EXCEPTION", e.toString());
            parsedDistance1[0] = "0";
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("GOOGLE_EXCEPTION", e.toString());
            parsedDistance1[0] = "0";
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("GOOGLE_EXCEPTION", e.toString());
            parsedDistance1[0] = "0";
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("GOOGLE_EXCEPTION", e.toString());
            parsedDistance1[0] = "0";
        }
        Log.e("GOOGLE_EXCEPTION", parsedDistance1[0]);
        return parsedDistance1[0];
    }


    public void showProgressBar() {
        mProgressDialog = new SimpleArcDialog(mContext);
        mProgressDialog.setConfiguration(new ArcConfiguration(mContext));
        mProgressDialog.setCancelable(false);
    }

    public void saveDoubleData(String key, double value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putLong(key, Double.doubleToRawLongBits(value));
        prefsEditor.commit();
    }

    public void saveIntData(String key, int value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }

    public void saveBooleanData(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }

    public boolean getBooleanData(String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(key, true);
        }
        return true;
    }

    public String getData(String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, "");
        }
        return "";
    }

    public void showLocationDialog(String close) {
       Dialog dialogProductDelivered;
        TextView mDialogText;
        dialogProductDelivered = new Dialog(mContext);
        dialogProductDelivered.setContentView(R.layout.location_dialog);
        // mOkDialog = dialogProductDelivered.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered.findViewById(R.id.mDialogText);
        mDialogText.setText("Location Permission!!");
        dialogProductDelivered.setCancelable(false);
        if(close.equalsIgnoreCase("close"))
        {
            dialogProductDelivered.dismiss();
        }else {
            dialogProductDelivered.show();
        }

       /* androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finish();
                        // redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica");


                    }
                })
                .setNegativeButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                        //finish();


                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        if(close.equalsIgnoreCase("close"))
        {
            if(alert.isShowing())
            {
                alert.dismiss();
            }

        }else {

            alert.show();
        }*/

    }
    public double getDoubleData(String key) {
        if (sharedPreferences != null) {
            return Double.doubleToLongBits(sharedPreferences.getLong(key, Double.doubleToLongBits(0)));
        }
        return 0;
    }

    public int getIntData(String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getInt(key, 0);
        }
        return 0;
    }

    public void cleardata() {

        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
        }
    }

    public void removekey(String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(CriticalogPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }


    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}




