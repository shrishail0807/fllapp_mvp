package com.criticalog.ecritica.Utils;

public class LatLongPojo implements Comparable<LatLongPojo> {
    private double latitude;
    private double longitude;

    public LatLongPojo() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    private String key;
    private double distance;


    public LatLongPojo(double latitude, double longitude,String key) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.key = key;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int compareTo(LatLongPojo o) {
        if(this.distance==o.distance)
            return 0;
        else if(this.distance>o.distance)
            return 1;
        else
            return -1;

    }


    @Override
    public String toString() {
        return "LatLongPojo{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", key='" + key + '\'' +
                ", distance=" + distance +
                '}';
    }
}
