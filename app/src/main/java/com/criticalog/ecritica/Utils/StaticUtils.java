package com.criticalog.ecritica.Utils;

/**
 * Created by DELL1 on 6/28/2018.
 */

public class StaticUtils {
    public static String NETWORK_BUTTON = "Settings";

    public static String BASE_URL = "http://test.criticalogistics.com/MobileApps/criticagit/criticalog-api/api/v1/user";
    public static String BASE_URL_OPERATION = "http://test.criticalogistics.com/MobileApps/criticagit/criticalog-api/api/v1/operation";
    public static String BASE_URL_WITHOUTFRAMEWORK = "http://test.criticalogistics.com/Test/api/";
    public static String BASE_URL_PRSDRS = "http://test.criticalogistics.com/MobileApps/criticagit/criticalog-api/api/v1/prsdrs";

    public static final String PICKUP_LIST = BASE_URL_PRSDRS + "/pickuplist";

    //public static final String PICKUP_LIST = BASE_URL_PRSDRS + "/pickuplist";
    public static String LOGIN_URL = BASE_URL + "/login";
    public static String STATUS_URL = BASE_URL + "/userstatus";

    public static String NOTIFICATION_URL = BASE_URL + "/task";

    public static String CANCELDROP_URL = BASE_URL + "/cancel";

    public static String TASK_INFO_URL = BASE_URL + "/dropinfo";

    public static String POD_URL = BASE_URL + "/productdelevered";

    //testting purpose
    //public static String POD_URL = BASE_URL_TEST + "/productdelevered";

    public static String TASK_ONGOING = BASE_URL + "/taskongoing";

    public static String HISTORY_URL = BASE_URL + "/histroy";

    public static String LOCATION_URL = BASE_URL + "/location";

    public static String CANCELREASONS_URL = BASE_URL + "/reasonscancel";

    public static String BULK_TASKLIST = BASE_URL + "/listtask";

    public static String CLIENT_DETAILS_LIST = BASE_URL + "/listclients";

    public static String LABELPRINT_URL = BASE_URL + "/labeldata";

    public static String CLOSE_CLIENT_URL = BASE_URL_PRSDRS + "/closeclient";

    public static String PICKUP_CANCEL = BASE_URL_PRSDRS + "/pickupcancel";

    public static String PICKUP_SUCCESS_URL = BASE_URL_PRSDRS + "/pickupsuccess";

    public static String CLOSE_DOCKET_URL = BASE_URL_PRSDRS + "/closedocket";

    public static String DOCKET_VALIDATE_URL = BASE_URL_PRSDRS + "/docketstatus";

    public static String CIT_MANUL_CHECK_URL = BASE_URL_PRSDRS + "/pickupstatus";

    public static String AIRWAY_LIMIT_URL = BASE_URL + "/airwaycheck";

    public static String INSCAN_DEST_PINCODE_VALIDATE = BASE_URL_OPERATION + "/inscandestpincode_validation";

    public static String INSCAN_BOX_NUMBER = BASE_URL_OPERATION + "/inscanbox_number";

    public static String BAGGING_SUBMIT = BASE_URL_OPERATION + "/bagging_submit";

    public static String PINCODE_VALIDATE = BASE_URL_PRSDRS + "/pincode_validation";

    public static String INSCAN_ORG_PINCODE_VALIDATE = BASE_URL_OPERATION + "/inscanorgpincode_validation";

    public static String INSCAN_ACTUALVAL_VALIDATE = BASE_URL_OPERATION + "/inscan_actualvalue";

    public static String REPRINT_LABEL = BASE_URL + "/reprint";

    public static String TAT_CHECK = BASE_URL + "/tatcheck";

    public static String OTP_VALIDATE = BASE_URL + "/otpvalidate";

    public static String AWB_CHECK = BASE_URL + "/awbcheck";

    public static String STOP_RIDE = BASE_URL + "/trip";

    public static String ROUND_CHECK = BASE_URL + "/roundcheck";

    public static String SECTOR_PICKUP = BASE_URL + "/sectorpickup";

    public static String HR_SUGGESTION = BASE_URL + "/hr";

    public static String SNIPPET_URL = BASE_URL + "/suggest";
    public static String SHOW_SNIPPET_URL = BASE_URL + "/showsnippet";

    public static String GET_FIREBASE_TOKEN = BASE_URL + "/gettoken";

    public static String HR_HISTORY = BASE_URL + "/ideaconcern";

    public static String FRRDBACK_SNIPPET = BASE_URL + "/feedbacksnippet";

    public static String FEEDBACK_IDEA = BASE_URL + "/feedbackidea";

    public static String CONNOTE_TRACK = BASE_URL + "/connoteenquiry";

    public static String IN_SCAN_DOCKET_CHECK = BASE_URL_OPERATION + "/inscandocketcheck";

    public static String IN_SCAN_SUBMIT = BASE_URL_OPERATION + "/inscan_submit";
    public static String BAGGING_ORG_HUB = BASE_URL_OPERATION + "/bagging_orghub";
    public static String BAGGING_BAG_VALIDITATION = BASE_URL_OPERATION + "/bagging_bag";
    public static String BAGGING_BOX_VALIDITATION = BASE_URL_OPERATION + "/bagging_boxnumber";
    public static String NEGATIVE_NEGATIVESTATUS = BASE_URL_OPERATION + "/negative_negstatus";
    public static String NEGATIVE_ORIHUB = BASE_URL_OPERATION + "/negative_orghub";
    public static String NEGATIVE_VALIDATE = BASE_URL_OPERATION + "/negative_validate";
    public static String NEGATIVE_SUBMIT = BASE_URL_OPERATION + "/negative_submit";
    public static String HYPERTRACKFLAG = BASE_URL_WITHOUTFRAMEWORK + "user.httracking.php";
    public static String HYPERTRACKSENDDETAILS = BASE_URL_WITHOUTFRAMEWORK + "user.htdevicelog.php";
    public static String HYPERTRACKSENDGEOTAG = BASE_URL_WITHOUTFRAMEWORK + "user.geotaglog.php";
    public static String USER_RIDELOG = BASE_URL_WITHOUTFRAMEWORK + "user.riderlog.php";

    public static String SRMESSAGE = BASE_URL + "/service";
    public static String SRUPDATE = BASE_URL + "/serviceupdate";

    public static String ERROR_TITLE = "Error";

    public static String ERROR_MESSAGE = "There was an error";

    public static String PARSE_ERROR_TITLE = "PARSE ERROR";

    public static String PARSE_ERROR_MESSAGE = "There was an error while reading the response";

    public static String SUCCESS_STATUS = "success";

    public static String FAILURE_STATUS = "Failure";

    public static String NETWORK_ERROR_TITLE = "Network Connectivity";

    public static String NETWORK_ERROR_MESSAGE = "Please Check Network Connection";

    public static String POSITIVE_BUTTON = "Ok";

    public static String PICKUP = "Pickup";
    public static String DELIVERY = "Delivery";
    public static String WEAK_NETWORK_MESSAGE = "Weak network, saving locally";

    public static String NETWORK_ERROR_SAVE_DETAILS = "Please Check Network Connection,Saving Details Locally";

    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public static final String CAMERA_FLASH_ON = "CAMERA_FLASH_ON";

    public static final String history = "pickup";

    public static final int LOCATION_REQUEST = 1000;
    public static final int GPS_REQUEST = 1001;
    public static final int ENABLE_BLUETOOTH = 1;

    public static String TOKEN = "";

    public static String APP_VERSION = "";

    public static String distance = "0";

    public static String billionDistanceAPIKey = "";

    public static String distance_api_url = "";

    public static String BASE_URL_DYNAMIC="";

    public static String URLToGetBaseURL="http://api.ecritica.co/settings.php?action=get_base_url";

}
