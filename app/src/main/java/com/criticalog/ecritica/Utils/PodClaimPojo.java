package com.criticalog.ecritica.Utils;

public class PodClaimPojo {
    private String distance;
    private String date,time,remark,taskid,tasktype;
    private double start_lat,start_long,edn_lat,end_long;
    private String dttime;
    private int responsecode;
    String distance_g;


    public PodClaimPojo() {
    }

    public PodClaimPojo(double start_lat, double start_long, double edn_lat, double end_long, String distance, String distance_g,String date, String time, String remark,String taskid,String tasktype,String dttime,int responsecode) {
        this.start_lat = start_lat;
        this.start_long = start_long;
        this.edn_lat = edn_lat;
        this.end_long = end_long;
        this.distance = distance;
        this.date = date;
        this.time = time;
        this.remark = remark;
        this.taskid = taskid;
        this.tasktype = tasktype;
        this.dttime = dttime ;
        this.responsecode = responsecode;
        this.distance_g=distance_g;

    }

    public String getDistance_g() {
        return distance_g;
    }

    public void setDistance_g(String distance_g) {
        this.distance_g = distance_g;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public double getStart_lat() {
        return start_lat;
    }

    public void setStart_lat(double start_lat) {
        this.start_lat = start_lat;
    }

    public double getStart_long() {
        return start_long;
    }

    public void setStart_long(double start_long) {
        this.start_long = start_long;
    }

    public double getEdn_lat() {
        return edn_lat;
    }

    public void setEdn_lat(double edn_lat) {
        this.edn_lat = edn_lat;
    }

    public double getEnd_long() {
        return end_long;
    }

    public void setEnd_long(double end_long) {
        this.end_long = end_long;
    }


    public String getTasktype() {
        return tasktype;
    }

    public void setTasktype(String tasktype) {
        this.tasktype = tasktype;
    }

    public String getDttime() {
        return dttime;
    }

    public void setDttime(String dttime) {
        this.dttime = dttime;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    @Override
    public String toString() {
        return "PodClaimPojo{" +
                "distance='" + distance + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", remark='" + remark + '\'' +
                ", taskid='" + taskid + '\'' +
                ", tasktype='" + tasktype + '\'' +
                ", start_lat=" + start_lat +
                ", start_long=" + start_long +
                ", edn_lat=" + edn_lat +
                ", end_long=" + end_long +
                ", dttime='" + dttime + '\'' +
                ", responsecode=" + responsecode +
                '}';
    }
}
