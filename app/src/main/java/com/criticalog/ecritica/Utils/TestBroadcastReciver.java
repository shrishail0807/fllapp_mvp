package com.criticalog.ecritica.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.google.android.gms.location.LocationResult;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TestBroadcastReciver extends BroadcastReceiver {

    public static final String ACTION_PROCESS_START_TEST ="com.criticalog.ecritica.Utils.TestBroadcastReciver";
    private DatabaseHelper db;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private double start_lat,start_long;
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("inside","TestBroadcastreciever");

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);

        if(!(criticalogSharedPreferences.getData("start_lat") == null || criticalogSharedPreferences.getData("start_lat").equalsIgnoreCase("") ))  {
            start_lat = Double.parseDouble((criticalogSharedPreferences.getData("start_lat")));
        }

        if(!(criticalogSharedPreferences.getData("start_long") == null || criticalogSharedPreferences.getData("start_long").equalsIgnoreCase("") ))  {
            start_long = Double.parseDouble((criticalogSharedPreferences.getData("start_long")));
        }

        if (intent != null) {
            String action = intent.getAction();

            if (ACTION_PROCESS_START_TEST.equalsIgnoreCase(action)) {
                Log.d("action is",ACTION_PROCESS_START_TEST);
                LocationResult locationResult = LocationResult.extractResult(intent);
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();
                    float latitude = (float) location.getLatitude();
                    float longitude = (float) location.getLongitude();
                    String builder = new StringBuilder(""+location.getLatitude()).append("/").append(location.getLongitude()).toString();
                    try {

                        // Toast.makeText(context,"cl "+builder,Toast.LENGTH_SHORT).show();
                        fn_update(latitude,longitude,context);

                    }catch (Exception e) {
                        // Toast.makeText(context,"Location not found",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    }

    private void fn_update(float latitude, float longitude,Context context) {

        Date c1 = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c1);

        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = df3.format(c1);


        SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
        String time1 = df4.format(c1);


        Log.d("Current time => " ,""+ c1.getTime());
        String timeStr=time1;
        SimpleDateFormat df=new SimpleDateFormat("hh:mm:ss a");
        try {
            Date dt=df.parse(timeStr);

            Log.d("time converted", ""+ dt.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        ArrayList<PodClaimPojo> latlonglist = db.getAllLatLongList();
        if (latlonglist.size() > 0) {
            int listsize = latlonglist.size();
            PodClaimPojo podClaimPojo;

            if (latlonglist.get(listsize - 1).getEdn_lat() == 0 || latlonglist.get(listsize - 1).getEnd_long() == 0 || latitude == 0 || longitude == 0) {
                podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "0","", date1, time1, "0", "0", "hour based",c1.toString(),10);
            } else {
                double distnce = distance(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "K");
                String convertedstring = converttorealnumber(distnce);
                podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring,"", date1, time1, "0", "0", "hour based",c1.toString(),10);
            }

            latlonglist.add(podClaimPojo);
            db.insertLatLOngDetails(podClaimPojo);
        } else {
            PodClaimPojo podClaimPojo;
            if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {
                podClaimPojo = new PodClaimPojo(start_lat, start_long, latitude, longitude, "0","0", date1, time1, "0", "0", "wherebout",c1.toString(),10);
            } else {
                double distnce = distance(start_lat, start_long, latitude, longitude, "K");
                String convertedstring = converttorealnumber(distnce);
                podClaimPojo = new PodClaimPojo(start_lat, start_long, latitude, longitude, convertedstring,"0", date1, time1, "1", "0", "wherebout",c1.toString(),10);
            }

            latlonglist.add(podClaimPojo);
            Log.d("obj", "prs" + podClaimPojo);
            db.insertLatLOngDetails(podClaimPojo);

        }
        Toast.makeText(context,"list size"+ latlonglist.size(),Toast.LENGTH_LONG).show();

    }

    public String converttorealnumber(double distance){
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    public double distance(double lat1, double lon1, double lat2, double lon2,String unit) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist/0.62137;

       /* String num = String.valueOf(dist);
        new BigDecimal(num).toPlainString();
        return new BigDecimal(num).toPlainString();*/

        return dist;

    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
