package com.criticalog.ecritica.Utils;

import android.content.Context;
import android.os.Build;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.RequiresApi;

public class SignalStrengthChecker {

    public int signalStrengthValue = 0;

    private Context mContext;

    public SignalStrengthChecker(Context mContext) {
        this.mContext = mContext;
        Log.d("signalstrengthchecker","productdeliverservice");
        myPhoneStateListener psListener = new myPhoneStateListener();
        TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

    }

    public class myPhoneStateListener extends PhoneStateListener {
        @RequiresApi(api = Build.VERSION_CODES.M)
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            try {

                Log.d("A_NETWORK_METRICS",
                        "Signal Strength (0-4 / dBm):" + signalStrength.getLevel() );

                signalStrengthValue = signalStrength.getLevel();
               /* if (signalStrength.isGsm()) {
                    if (signalStrength.getGsmSignalStrength() != 99)
                        signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
                    else
                        signalStrengthValue = signalStrength.getGsmSignalStrength();
                } else {
                    signalStrengthValue = signalStrength.getCdmaDbm();
                }
*/
                Log.e("signalStrengthValue", "" + signalStrengthValue);
               // Toast.makeText(mContext, "signalStrengthValue" + signalStrengthValue, Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                e.printStackTrace();
            }



        }
    }

    public int getSignalStrength(){

        return signalStrengthValue;
    }
}
