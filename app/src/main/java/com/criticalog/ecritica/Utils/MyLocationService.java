package com.criticalog.ecritica.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.model.LocationRequest;
import com.criticalog.ecritica.model.LocationResponse;
import com.google.android.gms.location.LocationResult;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyLocationService extends BroadcastReceiver {

    private double latitude = 0.0;
    private double longitude = 0.0;
    private String user_id;
    ;

    private Context context1;
    private DatabaseHelper db;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private double start_lat, start_long;
    private RestServices mServices;

    public static final String ACTION_PROCESS_UPDATE = "com.criticalog.ecritica.Utils.MyLocationService";

    @Override
    public void onReceive(Context context, Intent intent) {


        Log.d("inside", "mylocationservice" + "=" + intent.getAction());

        context1 = context;
        db = new DatabaseHelper(context);
        mServices = RestClient.getRetrofitInstance().create(RestServices.class);
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        if (!(criticalogSharedPreferences.getData("start_lat") == null || criticalogSharedPreferences.getData("start_lat").equalsIgnoreCase(""))) {
            start_lat = Double.parseDouble((criticalogSharedPreferences.getData("start_lat")));
        }

        if (!(criticalogSharedPreferences.getData("start_long") == null || criticalogSharedPreferences.getData("start_long").equalsIgnoreCase(""))) {
            start_long = Double.parseDouble((criticalogSharedPreferences.getData("start_long")));
        }

        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATE.equals(action)) {
                LocationResult locationResult = LocationResult.extractResult(intent);
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    String builder = new StringBuilder("" + location.getLatitude()).append("/").append(location.getLongitude()).toString();
                    try {
                        Log.d("location", builder);
                        // Toast.makeText(context,"cl "+builder,Toast.LENGTH_SHORT).show();
                        fn_update(latitude, longitude);

                    } catch (Exception e) {
                        Toast.makeText(context, "Location not found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void fn_update(double latitude, double longitude) {



       updateLocation();

        //   currentLocationVolley = new CurrentLocationVolley(user_id, latitude, longitude);


        Date c1 = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c1);

        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = df3.format(c1);


        SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
        String time1 = df4.format(c1);
        Log.d("time =>", time1);


        ArrayList<PodClaimPojo> latlonglist = db.getAllLatLongList();
        double inmeter = 0;
        if (latlonglist.size() > 0) {
            int listsize = latlonglist.size();
            PodClaimPojo podClaimPojo;

            if (latlonglist.get(listsize - 1).getEdn_lat() == 0 || latlonglist.get(listsize - 1).getEnd_long() == 0 || latitude == 0 || longitude == 0) {
                podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "0","", date1, time1, "0", "0", "hour based", c1.toString(), 10);
            } else {
                double distnce = distance(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "K");
                Log.d("inkm", "" + distnce);
                String convertedstring = converttorealnumber(distnce);
                inmeter = convertKmtoM(Double.parseDouble(convertedstring));
                Log.d("inmeter distance", "" + inmeter);
                String realnumber = converttorealnumber(inmeter);
                Log.d("imconversion", "after" + realnumber);
                podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring,"", date1, time1, "0", "0", "hour based", c1.toString(), 10);
            }
            PodClaimPojo previousobj = latlonglist.get(listsize - 1);
            Log.d("1", "1");
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            Log.d("11", "11");
            try {


                Date dtfetch = sdf.parse(previousobj.getDttime());
                long diff = c1.getTime() - dtfetch.getTime();
                long diffmin = diff / (60 * 1000);
                Log.d("diff ", "in time min  :" + diffmin);

                if (diffmin > 1) {
                    latlonglist.add(podClaimPojo);
                    db.insertLatLOngDetails(podClaimPojo);
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else {
            PodClaimPojo podClaimPojo;
            if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {
                podClaimPojo = new PodClaimPojo(start_lat, start_long, latitude, longitude, "0","0", date1, time1, "0", "0", "hour based", c1.toString(), 10);
            } else {
                double distnce = distance(start_lat, start_long, latitude, longitude, "K");
                String convertedstring = converttorealnumber(distnce);
                podClaimPojo = new PodClaimPojo(start_lat, start_long, latitude, longitude, convertedstring, date1, time1, "1", "0","0", "hour based", c1.toString(), 10);
            }

            latlonglist.add(podClaimPojo);
            db.insertLatLOngDetails(podClaimPojo);

        }

        Log.d("mylocation service", "list size" + latlonglist.size());
        Toast.makeText(context1, "list size" + latlonglist.size(), Toast.LENGTH_LONG).show();


    }

    public String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist / 0.62137;

       /* String num = String.valueOf(dist);
        new BigDecimal(num).toPlainString();
        return new BigDecimal(num).toPlainString();*/

        if (unit.equals("K")) {
            dist = dist * 1.609344;
        } else if (unit.equals("N")) {
            dist = dist * 0.8684;
        }

        return dist;

    }

    public void updateLocation()
    {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setAction("update_my_location");
        locationRequest.setUserId(user_id);
        locationRequest.setLatitude(String.valueOf(latitude));
        locationRequest.setLongitude(String.valueOf(longitude));
        Call<LocationResponse> locationResponseCall = mServices.locationUpdate(locationRequest);

        locationResponseCall.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {

            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {

            }
        });
    }
    private double convertKmtoM(double distancekn) {
        return (distancekn / 1000);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


}
