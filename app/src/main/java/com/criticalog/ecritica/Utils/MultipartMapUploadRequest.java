package com.criticalog.ecritica.Utils;

public class MultipartMapUploadRequest  {

   /* MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private final Response.Listener<String> mListener;

    private ArrayList<File> mFilePart;
    private String mstatus, mreciever_name, mtask_id, muser_id, drs_no,mradio_text,cheque,amount,modeofpay,cod_amount,cod_paytype,upiref;
    private String mDate,mTime;
    private File mcamera_File,mcamera_File1,mcamera_pod;
    private Context context;
    private double mlatitude ;
    private  double mlongitude ;

    private String bulkStatus,reverseTask;

    public MultipartMapUploadRequest(String url, Response.ErrorListener errorListener, Response.Listener<String> listener,
                                     File camera,File camer1,File camer2, String status, String reciever_name, String task_id, String user_id, String drsno, String bulk_Status, String date, String time, double latitude, double longitude,String radio_text,String mcheque,String mamount,String mmodeofpay,String reverseTaskId,String codamount,String codpaytype,String upirefno,Context ctx) {
        super(Method.POST, url, errorListener);

        mListener = listener;
        mcamera_File = camera;
        mcamera_File1 = camer1;
        mcamera_pod = camer2;
        mstatus = status;
        mreciever_name = reciever_name;
        mtask_id = task_id;
        muser_id = user_id;
        bulkStatus= bulk_Status;
        drs_no = drsno;
        mDate = date;
        mTime = time;
        mlatitude = latitude;
        mlongitude = longitude;
        mradio_text = radio_text;
        cheque = mcheque;
        amount = mamount;
        modeofpay = mmodeofpay;
        reverseTask = reverseTaskId;
        cod_amount = codamount;
        cod_paytype = codpaytype;
        upiref = upirefno;
        context = ctx;

        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        try {
            entity.setCharset(CharsetUtils.get("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        buildMultipartEntity();

        *//*setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*//*
        setRetryPolicy(MySingleton.getInstance(context).retryPolicy);
    }

    private void buildMultipartEntity() {

        if (mcamera_File == null  && mcamera_File1 == null) {
            entity.addTextBody("image1", "");
            entity.addTextBody("image2", "");
            entity.addTextBody("isimage", "0");
            Log.d("inside","isimage"+ 0);
        }else {
            entity.addPart("image1", new FileBody(mcamera_File));
            entity.addPart("image2", new FileBody(mcamera_File1));
            entity.addTextBody("isimage", "1");
            Log.d("inside","isimage"+ 1);
        }

        entity.addPart("file",new FileBody(mcamera_pod));
        entity.addTextBody("status", mstatus);
        entity.addTextBody("reciever_name", mreciever_name);
        entity.addTextBody("task_id", mtask_id);
        entity.addTextBody("user_id", muser_id);
        entity.addTextBody("refcount", drs_no);
        entity.addTextBody("bulk_flag", bulkStatus);
        entity.addTextBody("date", mDate);
        entity.addTextBody("time", mTime);
        entity.addTextBody("latitude", String.valueOf(mlatitude));
        entity.addTextBody("longitude", String.valueOf(mlongitude));
        entity.addTextBody("swap", mradio_text);
        entity.addTextBody("chequeno",cheque);
        entity.addTextBody("amount",amount);
        entity.addTextBody("modeofpay",modeofpay);
        entity.addTextBody("cod_taskid",reverseTask);
        entity.addTextBody("cod_amount",cod_amount);
        entity.addTextBody("cod_paytype",cod_paytype);
        entity.addTextBody("upirefno",upiref);

        Log.d("inside ","multipartreq"+ "upirefno = " + upiref);

    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity = entity.build();
            httpentity.writeTo(bos);

        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            System.out.println("Network Response " + new String(response.data, "UTF-8"));
            Log.d("response ","code"+ response.statusCode);
            return Response.success(new String(response.data, "UTF-8"),
                    getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.success(new String(response.data), getCacheEntry());
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError error) {


        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (error instanceof NetworkError) {
                    Toast.makeText(context,
                            "Oops. Network error!",
                            Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context,
                            "Oops. Server error!",
                            Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(context,
                            "Oops. AuthFail error!",
                            Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context,
                            "Oops. Parse error!",
                            Toast.LENGTH_LONG).show();
                } else if (error instanceof NoConnectionError) {
                    Toast.makeText(context,
                            "Oops. NoConnection error!",
                            Toast.LENGTH_LONG).show();
                } else if (error instanceof TimeoutError) {
                    Toast.makeText(context,
                            "Oops. Timeout error!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        return super.parseNetworkError(error);
    }
    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }*/
}

