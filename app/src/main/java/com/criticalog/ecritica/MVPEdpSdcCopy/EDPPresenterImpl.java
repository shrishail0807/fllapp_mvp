package com.criticalog.ecritica.MVPEdpSdcCopy;

import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSListContract;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPEdpCopy.model.ConnoteShortInfoRequest;

import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadResponse;
import com.criticalog.ecritica.MVPEdpSdcCopy.model.ConnoteShortInfoResponse;

public class EDPPresenterImpl implements EDPUploadContract.presenter, EDPUploadContract.GetEDPIntractor.OnFinishedListener, EDPUploadContract.GetConnoteShortInfoIntractor.OnFinishedListener {

    private EDPUploadContract.MainView mainView;
    private EDPUploadContract.GetEDPIntractor getEDPIntractor;
    private EDPUploadContract.GetConnoteShortInfoIntractor getConnoteShortInfoIntractor;


    public EDPPresenterImpl(EDPUploadContract.MainView mainView, EDPUploadContract.GetEDPIntractor getEDPIntractor,
                            EDPUploadContract.GetConnoteShortInfoIntractor getConnoteShortInfoIntractor) {
        this.mainView = mainView;
        this.getEDPIntractor = getEDPIntractor;
        this.getConnoteShortInfoIntractor=getConnoteShortInfoIntractor;

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void edpUpload(EDPUploadRequest edpUploadRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getEDPIntractor.edpUlpoadRequest(this, "", edpUploadRequest);
    }

    @Override
    public void connoteShortInfoRequest(ConnoteShortInfoRequest connoteShortInfoRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getConnoteShortInfoIntractor.connoteShortInfoRequest(this,"",connoteShortInfoRequest);
    }

    @Override
    public void onFinished(EDPUploadResponse edpUploadResponse) {
        if (mainView != null) {
            mainView.setEDPToViews(edpUploadResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(ConnoteShortInfoResponse connoteShortInfoResponse) {
        if(mainView!=null)
        {
            mainView.setConnoteShortInfoToViews(connoteShortInfoResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
