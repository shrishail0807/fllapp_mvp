package com.criticalog.ecritica.MVPEdpSdcCopy;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPEdpCopy.model.ConnoteShortInfoRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadResponse;
import com.criticalog.ecritica.MVPEdpSdcCopy.model.ConnoteShortInfoResponse;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class EDPActivity extends AppCompatActivity implements View.OnClickListener, EDPUploadContract.MainView {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private ImageView mEdpImage;
    private TextView mEdpUpload;
    File photoFile = null;
    private static final int PERMISSION_REQUEST_KM_IMAGE = 1888;
    private String base64KMImageEDP = "";
    EDPUploadContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    private String docketNumber = "";
    private SimpleArcDialog mProgressBar;
    private RadioButton mRadioEDP, mRadioSDC;
    private LinearLayout mEDPLayout;
    private RadioGroup radioGroup;
    private ImageView xtv_backbutton;
    private TextView mOrigin, mDestination, mClientName, mAddress;
    private LinearLayout mOriginLay, mDestLay, mClientLay, mStateLay;
    private EditText mEdpDocketNumber;
    private String edpsc = "EDP";
    private LinearLayout mCoordinatorLayout;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;
    RadioButton mSDCRB,mEDPRB;
    private Dialog dialogManualEntry;

    private TextView enter_connote,scan_qr_connote;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edp_upload);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        mCoordinatorLayout = findViewById(R.id.mLinearLayMain);
        mEdpImage = findViewById(R.id.mEdpImage);
        mEdpUpload = findViewById(R.id.mEdpUpload);
        mEdpDocketNumber = findViewById(R.id.mEdpDocketNumber);
        mEDPLayout = findViewById(R.id.mEDPLayout);
        radioGroup = findViewById(R.id.radioGroup);
        xtv_backbutton = findViewById(R.id.xtv_backbutton);
        mOrigin = findViewById(R.id.mOrigin);
        mDestination = findViewById(R.id.mDestination);
        mClientName = findViewById(R.id.mClientName);
        mAddress = findViewById(R.id.mAddress);
        mSDCRB=findViewById(R.id.mSDCRB);
        mEDPRB=findViewById(R.id.mEDPRB);
        mOriginLay = findViewById(R.id.mOriginLay);
        mDestLay = findViewById(R.id.mDestLay);
        mClientLay = findViewById(R.id.mClientLay);
        mStateLay = findViewById(R.id.mStateLay);
        enter_connote=findViewById(R.id.enter_connote);
        scan_qr_connote=findViewById(R.id.scan_qr_connote);

        mEdpDocketNumber.setOnClickListener(this);
        mEdpUpload.setOnClickListener(this);
        mEdpImage.setOnClickListener(this);
        xtv_backbutton.setOnClickListener(this);
        enter_connote.setOnClickListener(this);
        scan_qr_connote.setOnClickListener(this);

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");
        userId = mCriticalogSharedPreferences.getData("userId");

        if (checkPermission()) {
            mCoordinatorLayout.setVisibility(View.VISIBLE);
            //main logic or main code

            // . write your main code to execute, It will execute if the permission is already given.

        } else {
            requestPermission();
        }

        mEdpDocketNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 9) {

                    docketNumber=s.toString();
                    callConnoteShortInfAPI(s.toString());
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);

                // Add logic here

                switch (index) {
                    case 0: // first button
                        mEDPLayout.setVisibility(View.VISIBLE);
                        edpsc = "EDP";
                        mEdpUpload.setText("EDP upload");
                        mOriginLay.setVisibility(View.GONE);
                        mDestLay.setVisibility(View.GONE);
                        mClientLay.setVisibility(View.GONE);
                        mStateLay.setVisibility(View.GONE);
                        mEdpDocketNumber.setText("");
                        mEdpImage.setImageDrawable(null);
                        mEdpImage.setImageResource(R.drawable.outline_photo_camera_24);
                        // Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
                        break;
                    case 1: // secondbutton
                        // Toasty.success(EDPActivity.this, "SDC coming Soon!!", Toast.LENGTH_LONG, true).show();
                        mEDPLayout.setVisibility(View.VISIBLE);
                        edpsc = "SDC";
                        mEdpUpload.setText("SDC Upload");
                        mEDPRB.setChecked(false);
                        mOriginLay.setVisibility(View.GONE);
                        mDestLay.setVisibility(View.GONE);
                        mClientLay.setVisibility(View.GONE);
                        mStateLay.setVisibility(View.GONE);
                        mEdpDocketNumber.setText("");
                        mEdpImage.setImageDrawable(null);
                        mEdpImage.setImageResource(R.drawable.outline_photo_camera_24);

                        break;
                }
            }
        });

    }
    public void manualDialog() {
        EditText mDocketEntry;
        TextView mAddDocket;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(EDPActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_scan);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);

        mDocketEntry = dialogManualEntry.findViewById(R.id.mDocketEntry);
        mAddDocket = dialogManualEntry.findViewById(R.id.mAddDocket);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mDocketEntry.setHint("Enter Connote Number");
        mAddDocket.setText("Add Docket");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });


        mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketNumber = mDocketEntry.getText().toString();
                if (docketNumber.length() == 9) {
                    callConnoteShortInfAPI(docketNumber);
                   // dialogManualEntry.dismiss();
                } else {
                    Toasty.warning(EDPActivity.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });
    }
    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(EDPActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(EDPActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(EDPActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(EDPActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void callConnoteShortInfAPI(String connoteNumber) {
        ConnoteShortInfoRequest connoteShortInfoRequest = new ConnoteShortInfoRequest();
        connoteShortInfoRequest.setAction("connote_enquiry_short");
        connoteShortInfoRequest.setUserId(userId);
        connoteShortInfoRequest.setConnote(connoteNumber);
        connoteShortInfoRequest.setLatitude(String.valueOf(latitude));
        connoteShortInfoRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new EDPPresenterImpl(this, new EDPInteractorImpl(), new EDPInteractorImpl());
        mPresenter.connoteShortInfoRequest(connoteShortInfoRequest);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("Go to APP Settings and allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EDPActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                //   .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mEdpUpload:
                if (base64KMImageEDP.equals("")) {
                    if (edpsc.equalsIgnoreCase("EDP")) {
                        Toasty.warning(this, "Upload EDP Image!!", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.warning(this, "Upload SDC Image!!", Toast.LENGTH_LONG, true).show();
                    }

                } else {
                    if (edpsc.equalsIgnoreCase("EDP")) {
                        callEDPUpload("upload_senders_copy_edp");
                    } else {
                        callEDPUpload("upload_signed_dc_copy");
                    }
                }
                break;
            case R.id.mEdpImage:
                //docketNumber = mEdpDocketNumber.getText().toString();
                if (docketNumber.equals("")) {

                    Toasty.warning(this, "Please Enter the Docket Number", Toast.LENGTH_LONG, true).show();
                } else if (docketNumber.length() != 9) {
                    Toasty.warning(this, "Enter Valid Docket Number", Toast.LENGTH_LONG, true).show();
                } else {
                    openCameraIntent(PERMISSION_REQUEST_KM_IMAGE);
                }

                break;
            case R.id.mEdpDocketNumber:

                break;
            case R.id.xtv_backbutton:
                finish();
                break;
            case R.id.enter_connote:
                manualDialog();

                break;

            case R.id.scan_qr_connote:

                startBarcodeScan();
                break;
        }
    }

    public void callEDPUpload(String action) {
        EDPUploadRequest edpUploadRequest = new EDPUploadRequest();
        edpUploadRequest.setAction(action);
        edpUploadRequest.setUserId(userId);
        edpUploadRequest.setImage1(base64KMImageEDP);
        edpUploadRequest.setDocketNo(docketNumber);
        edpUploadRequest.setLatitude(String.valueOf(latitude));
        edpUploadRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new EDPPresenterImpl(EDPActivity.this, new EDPInteractorImpl(), new EDPInteractorImpl());
        mPresenter.edpUpload(edpUploadRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1888) {
            if (requestCode == PERMISSION_REQUEST_KM_IMAGE && resultCode == Activity.RESULT_OK) {
                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());



                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64KMImageEDP = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mEdpImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));


                }
            }
        }else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            String scanwhat = mCriticalogSharedPreferences.getData("scan_what");
            if (result != null) {
                try {
                    String MobilePattern = "[0-9]{9}";
                    docketNumber = result.getContents();
                    if (docketNumber.length() == 9) {
                        callConnoteShortInfAPI(docketNumber);
                    } else {
                        Toasty.warning(EDPActivity.this, "Scan Valid Docket!!", Toast.LENGTH_SHORT, true).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode

                }
            }
        }
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setEDPToViews(EDPUploadResponse edpUploadResponse) {

        if (edpUploadResponse.getStatus() == 200) {

            if (edpsc.equalsIgnoreCase("SDC")) {
                Toasty.success(this, edpUploadResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                mEdpImage.setImageResource(R.drawable.outline_photo_camera_24);
                base64KMImageEDP = "";
            } else {
                Toasty.success(this, edpUploadResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }

        } else {
            Toasty.warning(this, edpUploadResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }



    @Override
    public void setConnoteShortInfoToViews(ConnoteShortInfoResponse connoteShortResponse) {
        if (connoteShortResponse.getStatus() == 200) {
            if(dialogManualEntry!=null)
            {
                if(dialogManualEntry.isShowing())
                {
                    dialogManualEntry.dismiss();
                }
            }
            //dialogManualEntry.dismiss();
            mOriginLay.setVisibility(View.VISIBLE);
            mDestLay.setVisibility(View.VISIBLE);
            mClientLay.setVisibility(View.VISIBLE);
            mStateLay.setVisibility(View.VISIBLE);
            mOrigin.setText(connoteShortResponse.getData().getConsignorCity());
            mDestination.setText(connoteShortResponse.getData().getConsigneeCity());
            mClientName.setText(connoteShortResponse.getData().getClientName());
            mAddress.setText(connoteShortResponse.getData().getConsigneeState());
        } else {
            Toasty.warning(this, connoteShortResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }
    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.warning(this, R.string.re_try, Toast.LENGTH_LONG, true).show();
    }
}
