package com.criticalog.ecritica.MVPEdpSdcCopy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataShortInfo {
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("docket_id")
    @Expose
    private String docketId;
    @SerializedName("consignor_code")
    @Expose
    private String consignorCode;
    @SerializedName("consignor_name")
    @Expose
    private String consignorName;
    @SerializedName("consignor_state")
    @Expose
    private String consignorState;
    @SerializedName("consignor_city")
    @Expose
    private String consignorCity;
    @SerializedName("consignor_pin")
    @Expose
    private String consignorPin;
    @SerializedName("consignee_code")
    @Expose
    private String consigneeCode;
    @SerializedName("consignee_name")
    @Expose
    private String consigneeName;
    @SerializedName("consignee_state")
    @Expose
    private String consigneeState;
    @SerializedName("consignee_city")
    @Expose
    private String consigneeCity;
    @SerializedName("consignee_pin")
    @Expose
    private String consigneePin;

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getDocketId() {
        return docketId;
    }

    public void setDocketId(String docketId) {
        this.docketId = docketId;
    }

    public String getConsignorCode() {
        return consignorCode;
    }

    public void setConsignorCode(String consignorCode) {
        this.consignorCode = consignorCode;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorState() {
        return consignorState;
    }

    public void setConsignorState(String consignorState) {
        this.consignorState = consignorState;
    }

    public String getConsignorCity() {
        return consignorCity;
    }

    public void setConsignorCity(String consignorCity) {
        this.consignorCity = consignorCity;
    }

    public String getConsignorPin() {
        return consignorPin;
    }

    public void setConsignorPin(String consignorPin) {
        this.consignorPin = consignorPin;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeState() {
        return consigneeState;
    }

    public void setConsigneeState(String consigneeState) {
        this.consigneeState = consigneeState;
    }

    public String getConsigneeCity() {
        return consigneeCity;
    }

    public void setConsigneeCity(String consigneeCity) {
        this.consigneeCity = consigneeCity;
    }

    public String getConsigneePin() {
        return consigneePin;
    }

    public void setConsigneePin(String consigneePin) {
        this.consigneePin = consigneePin;
    }
}
