package com.criticalog.ecritica.MVPEdpSdcCopy;

import com.criticalog.ecritica.MVPEdpCopy.model.ConnoteShortInfoRequest;

import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadResponse;
import com.criticalog.ecritica.MVPEdpSdcCopy.model.ConnoteShortInfoResponse;

public interface EDPUploadContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void edpUpload(EDPUploadRequest edpUploadRequest);

        void connoteShortInfoRequest(ConnoteShortInfoRequest connoteShortInfoRequest);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setEDPToViews(EDPUploadResponse edpUploadResponse);

        void setConnoteShortInfoToViews(ConnoteShortInfoResponse connoteShortInfoToViews);


        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetEDPIntractor {

        interface OnFinishedListener {
            void onFinished(EDPUploadResponse edpUploadResponse);

            void onFailure(Throwable t);
        }

        void edpUlpoadRequest(EDPUploadContract.GetEDPIntractor.OnFinishedListener onFinishedListener, String token, EDPUploadRequest edpUploadRequest);
    }

    interface GetConnoteShortInfoIntractor {

        interface OnFinishedListener {
            void onFinished(ConnoteShortInfoResponse connoteShortInfoResponse);

            void onFailure(Throwable t);
        }

        void connoteShortInfoRequest(EDPUploadContract.GetConnoteShortInfoIntractor.OnFinishedListener onFinishedListener, String token, ConnoteShortInfoRequest connoteShortInfoRequest);
    }

}
