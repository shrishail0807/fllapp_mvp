package com.criticalog.ecritica.MVPEdpSdcCopy;

import com.criticalog.ecritica.MVPEdpCopy.model.ConnoteShortInfoRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadRequest;
import com.criticalog.ecritica.MVPEdpCopy.model.EDPUploadResponse;
import com.criticalog.ecritica.MVPEdpSdcCopy.model.ConnoteShortInfoResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EDPInteractorImpl implements EDPUploadContract.GetEDPIntractor, EDPUploadContract.GetConnoteShortInfoIntractor {
    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);


    @Override
    public void edpUlpoadRequest(EDPUploadContract.GetEDPIntractor.OnFinishedListener onFinishedListener, String token, EDPUploadRequest edpUploadRequest) {
        Call<EDPUploadResponse> edpUploadResponseCall = service.edpUpload(edpUploadRequest);
        edpUploadResponseCall.enqueue(new Callback<EDPUploadResponse>() {
            @Override
            public void onResponse(Call<EDPUploadResponse> call, Response<EDPUploadResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<EDPUploadResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void connoteShortInfoRequest(EDPUploadContract.GetConnoteShortInfoIntractor.OnFinishedListener onFinishedListener, String token, ConnoteShortInfoRequest connoteShortInfoRequest) {

        Call<ConnoteShortInfoResponse> connoteShortInfoResponseCall = service.connoteShortInfo(connoteShortInfoRequest);
        connoteShortInfoResponseCall.enqueue(new Callback<ConnoteShortInfoResponse>() {
            @Override
            public void onResponse(Call<ConnoteShortInfoResponse> call, Response<ConnoteShortInfoResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ConnoteShortInfoResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }
}
