package com.criticalog.ecritica.MVPDRS.DRSClose;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Adapters.RulesAdapterWithFlag;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Interface.OnclickOpenAdapter;
import com.criticalog.ecritica.Interface.OptionsAdapterClicked;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBarcodeScan.Adapter.OpenPickupAdpater;
import com.criticalog.ecritica.MVPBarcodeScan.BarcodeScanActivity;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketResponse;
import com.criticalog.ecritica.MVPBarcodeScan.DocketValidationContract;
import com.criticalog.ecritica.MVPBarcodeScan.DocketValidationPresenterImpl;
import com.criticalog.ecritica.MVPBarcodeScan.GetDocketValidationInteractorImpl;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ChecklistResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateChecklistYesNoResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateCheclistYesNoRequest;
import com.criticalog.ecritica.MVPDRS.Adapter.RulesDRSAdapter;
import com.criticalog.ecritica.MVPDRS.Adapter.RulesDRSHorizontalAdapter;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PayTypeListResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.ModelClasses.OpenImage;
import com.criticalog.ecritica.NewCODProcess.Adapter.OptionsAdapter;
import com.criticalog.ecritica.NewCODProcess.ChequeDetails;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionRequest;
import com.criticalog.ecritica.NewCODProcess.Model.CodChequeCollectionResponse;
import com.criticalog.ecritica.NewCODProcess.Model.DatumPayType;
import com.criticalog.ecritica.NewCODProcess.NewCodActivity;
import com.criticalog.ecritica.NewCODProcess.ShowQrCodeActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.DrsRulesQc;
import com.criticalog.ecritica.model.RulesRequest;
import com.criticalog.ecritica.model.RulesResponse;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.zxing.integration.android.IntentIntegrator;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import net.glxn.qrgen.android.QRCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DRSCloseActivity extends AppCompatActivity implements View.OnClickListener, DRSCloseContract.MainView, RulesClicked, OptionsAdapterClicked, DocketValidationContract.MainView, OnclickOpenAdapter {
    private PhotoView mPodCapture, mOpenImageCapture, mPaymentRefImage, mOpenImageCapture1, mSDCImage, mDcChecklistImageCapture,
            mDcChecklistImageCapture1, mCustomOption1Capture1, mCustomOption1Capture2;
    File photoFile = null;
    private static final int PERMISSION_REQUEST_CODE_POD = 1888;
    private static final int PERMISSION_REQUEST_CODE_OPEN_IMAGE = 1889;
    private static final int PERMISSION_REQUEST_CODE_OPEN_IMAGE1 = 1891;
    private static final int PERMISSION_REQUEST_PAYMENT_IMAGE = 1990;
    private static final int PERMISSION_REQUEST_SDC_IMAGE = 1992;
    private static final int PERMISSION_REQUEST_DCCHECKLIST_IMAGE = 1993;
    private static final int PERMISSION_REQUEST_DCCHECKLIST_IMAGE1 = 1994;
    private static final int PERMISSION_REQUEST_CUSTOMOPTION1_IMAGE = 1995;
    private static final int PERMISSION_REQUEST_CUSTOMOPTION2_IMAGE = 1996;
    private static int CAMERA_REQUEST_MULT = 1890;
    private ImageView mDeletePhoto, mDeleteOpenImage, mDeletePaymentRefImage, mDeletemCustomOption1Capture1, mDeletemCustomOption1Capture2;
    private String base64PodCapture = "";
    private String base64OpenImage = "", base64OpenImage1 = "", base64PaymentImage = "", base64SDCCapture = "",
            Base64DCChecklistCapture = "", Base64DCChecklistCapture1 = "", Base64PodCustomOption1 = "",
            Base64PodCustomOption2 = "", base64CapturedMultImage = "";
    private DRSCloseContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token, codamount;
    private TextView mTvPaymentStatus, mDone,mOpenDeliveryTextHeader;
    private ImageButton mRefreshUpi;
    private EditText mReceiverName, mUpiRefNumber;
    private LinearLayout mPayLayout, mPaymentReferenceLay,photolayout;
    public static String check_paytype = null;
    private String payType = "";
    private double latitude = 0.0, longitude = 0.0;
    String cheque = "";
    String amnt = "";
    SimpleArcDialog mProgressBar;
    private String Drs_no, task_id, reprint_con,
            customer_name, phone_num, address1, address2, city, payment_status,
            state, pincode, bulk_flag, isBulk, swap, paymentStatus, paymentAmount,
            clinetCode, payLink, qrPath, openDeliveryFlag,
            forceChecListOTPEnabled = "",gatePassImageURL="";
    private String modeOfpay = "cod";
    private RadioGroup codradiogroup;
    private AlertDialog DialogFinish;
    private TextView mPayAmount;
    EditText mChequeNoET, mAmountET, mReferenceNo;
    Button mSaveChequeBtn;
    ImageButton mChequeCloseBtn;
    private RadioButton online, cash, chequebtn;
    private int ftc_status_layout;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private DatabaseHelper dbHelper;
    private boolean statusfromdb;
    private double start_lat, start_long;
    private ImageView mTvBackbutton, mDeleteOpenImage1, mDeleteSDCImage, mDeleteDcChecklistImage, mDeleteDcChecklistImage1;
    private RestServices mRestServices;
    private Dialog dialogRules;
    private TextView mPaymentRefImageText;
    private String qrImageFlag, sdcMandatory, referenceNum = "";
    private int qrImageFlagInt = 0;
    private TextView mTopBarText;
    private Dialog dialogProductDelivered, dialogProductDelivered1;
    private TextView mOkDialog, mForceAllow;
    private LinearLayout payment_status_refresh_layout;
    private int prsRuleIndex = 0;
    private String forcePrsCheck, otpFlag, custom_option1;
    List<ChecklistResponse> drsRulesList = new ArrayList<>();
    List<ChecklistScan> prsRulesListPreview = new ArrayList<>();
    List<DrsRulesQc> prsRules = new ArrayList<>();
    private String remarks = "";
    private RelativeLayout mainLayout;
    private TextView mSDCImageText, mDCcumCheckListText, mCustomOption1Label;
    private LinearLayout mSDCLinLayout, mDcCumChecklistLay, mCustomOption1Lay1;
    private TextView mOkRule;
    List<String> stringList = new ArrayList<>();
    private ProgressBar mProgress;
    private RelativeLayout mMainLayoutRelative;
    RecyclerView mRvRules, mRvOpenDeliveryImages;
    Location gps_loc;
    Location network_loc;
    Location final_loc;
    private BarcodeDetector detector;
    TextView mDialogText, mSelectYesNoAlert;
    String useVision = "";
    private int imageCaptureRetryFlag = 0;
    private String forceImageCapture = "1";
    private Dialog dialogReasonForCitPrintMismatch;
    private String maxCheque, forcePrintFlag, reasonMessageCITPrint = "";
    RulesDRSHorizontalAdapter rulesHorizontalAdapter;
    private Dialog dialogCheckListWithFlag;
    LinearLayout mChecklistWithFalgLinLayout, mOtpLinLayout, mOpenDeliveryImagesLay;
    private String verifyOrSendOtp = "";
    DocketValidationContract.presenter mPresenterQC;
    List<OpenImage> openImageList = new ArrayList<>();
    OpenPickupAdpater openDeliveryImageAdpater;
    private int positionClicked;
    private String openPickupText;
    private int openDeliveryFlagForMultipleImages = 0;
   // private RenderScript renderScript;
    private LinearLayout mGatePassLay;
    private ImageView mGatePassImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drs_close_mvp);

        dialogProductDelivered1 = new Dialog(DRSCloseActivity.this);
        dialogProductDelivered1.setContentView(R.layout.dialog_pod_scan_not_done);

        // Initialize RenderScript
    //    renderScript = RenderScript.create(this);

        detector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.ALL_FORMATS | Barcode.ALL_FORMATS)
                .build();

        if (!detector.isOperational()) {
            Toast.makeText(this, "Detector initialisation failed", Toast.LENGTH_SHORT).show();
            //  return;
        }
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        checkGpsEnabledOrNot();

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };


        //  Intent intent = getIntent();

        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();
        mPresenter = new DRSClosePresenterImpl(this, new GetDRSCloseInteractorImpl(), new GetDRSCloseInteractorImpl(), new GetDRSCloseInteractorImpl());
        mPresenterQC = new DocketValidationPresenterImpl(DRSCloseActivity.this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl());
        getLocationRiderLog();
        getLocation();

        mPodCapture = findViewById(R.id.mPodCapture);
        mPaymentRefImage = findViewById(R.id.mPaymentRefImage);
        mDeletePhoto = findViewById(R.id.mDeletePhoto);
        mOpenImageCapture = findViewById(R.id.mOpenImageCapture);
        mDeleteOpenImage = findViewById(R.id.mDeleteOpenImage);
        mDeletePaymentRefImage = findViewById(R.id.mDeletePaymentRefImage);
        mTvPaymentStatus = findViewById(R.id.mTvPaymentStatus);
        mRefreshUpi = findViewById(R.id.mRefreshUpi);
        mPayLayout = findViewById(R.id.mPayLayout);
        codradiogroup = findViewById(R.id.codradiogrp);
        mPayAmount = findViewById(R.id.mPayAmount);
        mReferenceNo = findViewById(R.id.referenceno);
        online = findViewById(R.id.online);
        cash = findViewById(R.id.cash);
        chequebtn = findViewById(R.id.cheque);
        mReceiverName = findViewById(R.id.mReceiverName);
        mUpiRefNumber = findViewById(R.id.referenceno);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mProgress = findViewById(R.id.mProgress);
        mOpenDeliveryTextHeader=findViewById(R.id.mOpenDeliveryTextHeader);
        payment_status_refresh_layout = findViewById(R.id.payment_status_refresh_layout);

        mDone = findViewById(R.id.mDone);
        mPaymentRefImageText = findViewById(R.id.mPaymentRefImageText);
        mPaymentReferenceLay = findViewById(R.id.mPaymentReferenceLay);
        mTopBarText = findViewById(R.id.mTopBarText);

        mOpenImageCapture1 = findViewById(R.id.mOpenImageCapture1);
        mDeleteOpenImage1 = findViewById(R.id.mDeleteOpenImage1);
        mainLayout = findViewById(R.id.mainLayout);
        mSDCImageText = findViewById(R.id.mSDCImageText);
        mSDCLinLayout = findViewById(R.id.mSDCLinLayout);
        mSDCImage = findViewById(R.id.mSDCImage);
        mDeleteSDCImage = findViewById(R.id.mDeleteSDCImage);
        mDCcumCheckListText = findViewById(R.id.mDCcumCheckListText);
        mDcCumChecklistLay = findViewById(R.id.mDcCumChecklistLay);
        mDcChecklistImageCapture = findViewById(R.id.mDcChecklistImageCapture);
        mDcChecklistImageCapture1 = findViewById(R.id.mDcChecklistImageCapture1);
        mDeleteDcChecklistImage = findViewById(R.id.mDeleteDcChecklistImage);
        mDeleteDcChecklistImage1 = findViewById(R.id.mDeleteDcChecklistImage1);
        mDeleteDcChecklistImage = findViewById(R.id.mDeleteDcChecklistImage);
        mMainLayoutRelative = findViewById(R.id.mMainLayoutRelative);
        mCustomOption1Label = findViewById(R.id.mCustomOption1Label);
        mCustomOption1Lay1 = findViewById(R.id.mCustomOption1Lay1);
        mCustomOption1Capture1 = findViewById(R.id.mCustomOption1Capture1);
        mCustomOption1Capture2 = findViewById(R.id.mCustomOption1Capture2);
        mDeletemCustomOption1Capture1 = findViewById(R.id.mDeletemCustomOption1Capture1);
        mDeletemCustomOption1Capture2 = findViewById(R.id.mDeletemCustomOption1Capture2);
        mRvOpenDeliveryImages = findViewById(R.id.mRvOpenDeliveryImages);
        mOpenDeliveryImagesLay = findViewById(R.id.mOpenDeliveryImagesLay);
        photolayout=findViewById(R.id.photolayout);
        mGatePassLay=findViewById(R.id.mGatePassLay);
        mGatePassImage=findViewById(R.id.mGatePassImage);


        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        // mProgressBar.setCancelable(false);

        mCustomOption1Capture1.setOnClickListener(this);
        mCustomOption1Capture2.setOnClickListener(this);
        mDeletemCustomOption1Capture1.setOnClickListener(this);
        mDeletemCustomOption1Capture2.setOnClickListener(this);
        mDcChecklistImageCapture1.setOnClickListener(this);
        mDcChecklistImageCapture.setOnClickListener(this);
        mDeleteDcChecklistImage.setOnClickListener(this);
        mDeleteDcChecklistImage1.setOnClickListener(this);
        mPodCapture.setOnClickListener(this);
        mDeletePhoto.setOnClickListener(this);
        mOpenImageCapture.setOnClickListener(this);
        mDeleteOpenImage.setOnClickListener(this);
        mTvPaymentStatus.setOnClickListener(this);
        mRefreshUpi.setOnClickListener(this);
        mDone.setOnClickListener(this);
        mTvBackbutton.setOnClickListener(this);
        mPaymentRefImage.setOnClickListener(this);
        mDeletePaymentRefImage.setOnClickListener(this);
        mOpenImageCapture1.setOnClickListener(this);
        mDeleteOpenImage1.setOnClickListener(this);
        mSDCImage.setOnClickListener(this);
        mDeleteSDCImage.setOnClickListener(this);
        mGatePassImage.setOnClickListener(this);
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");

        Drs_no = mCriticalogSharedPreferences.getData("drs_number");
        task_id = mCriticalogSharedPreferences.getData("task_id");
        reprint_con = mCriticalogSharedPreferences.getData("reprint_con");
        swap = mCriticalogSharedPreferences.getData("swap");
        maxCheque = mCriticalogSharedPreferences.getData("max_cheque_num");
        forcePrintFlag = mCriticalogSharedPreferences.getData("force_reprint_flag");
        modeOfpay = mCriticalogSharedPreferences.getData("pay_type");
        customer_name = mCriticalogSharedPreferences.getData("receiver_name");
        isBulk = mCriticalogSharedPreferences.getData("bulk_flag");
        openDeliveryFlag = mCriticalogSharedPreferences.getData("open_delivery_flag");
        if (isBulk.equals("true")) {
            bulk_flag = "1";
        } else {
            bulk_flag = "0";
        }
        otpFlag = mCriticalogSharedPreferences.getData("otp_flag_drs");
        paymentStatus = mCriticalogSharedPreferences.getData("payment_status");
        paymentAmount = mCriticalogSharedPreferences.getData("payment_amount");
        clinetCode = mCriticalogSharedPreferences.getData("clientcode");
        ftc_status_layout = mCriticalogSharedPreferences.getIntData("ftc_layout");
        payLink = mCriticalogSharedPreferences.getData("pay_link");
        qrPath = mCriticalogSharedPreferences.getData("qr_path");
        qrImageFlag = mCriticalogSharedPreferences.getData("qrcode_upload_flag");
        sdcMandatory = mCriticalogSharedPreferences.getData("sdc_mandatory");
        forcePrsCheck = mCriticalogSharedPreferences.getData("force_drs_check");
        custom_option1 = mCriticalogSharedPreferences.getData("custom_option1");
        useVision = mCriticalogSharedPreferences.getData("image_vision");
        phone_num = mCriticalogSharedPreferences.getData("get_phone_otp");

        forceChecListOTPEnabled = mCriticalogSharedPreferences.getData("force_drs_checklist_otp");

        gatePassImageURL = mCriticalogSharedPreferences.getData("gate_pass_flag_drs");

        if (custom_option1.equalsIgnoreCase("")) {
            mCustomOption1Label.setVisibility(View.GONE);
            mCustomOption1Lay1.setVisibility(View.GONE);
        } else {
            mCustomOption1Label.setText(custom_option1);
            mCustomOption1Label.setVisibility(View.VISIBLE);
            mCustomOption1Lay1.setVisibility(View.VISIBLE);
        }

        if(!gatePassImageURL.equalsIgnoreCase(""))
        {
            mGatePassLay.setVisibility(View.VISIBLE);

            try {
                Picasso.with(this)
                        .load(gatePassImageURL)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .placeholder(R.drawable.brokenimage)
                        .into(mGatePassImage);

            }catch (Exception e)
            {
                Log.e("Image_Load",e.toString());
            }

        }else {
            mGatePassLay.setVisibility(View.GONE);
        }

        mCriticalogSharedPreferences.saveData("rule_yes_no", "");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        StaticUtils.distance_api_url = mCriticalogSharedPreferences.getData("distance_api_url");

        if (otpFlag.equalsIgnoreCase("1")) {
            mReceiverName.setText(customer_name);
        }
        String staticFrom = mCriticalogSharedPreferences.getData("from_qr_payment");

        if (modeOfpay.equalsIgnoreCase("cod")) {
            refreshUpiApiCall();
            String from = mCriticalogSharedPreferences.getData("new_cod_received");
            if (from.equalsIgnoreCase("0")) {
                if (!staticFrom.equalsIgnoreCase("true")) {
                    PaytypeListRequest paytypeListRequest = new PaytypeListRequest();
                    paytypeListRequest.setAction("client_paytype");
                    paytypeListRequest.setUserId(userId);
                    paytypeListRequest.setClientCode(clinetCode);
                    paytypeListRequest.setDocket_no(task_id);
                    mPresenter.getPayTypeList(paytypeListRequest);
                }
            }
        } else if (modeOfpay.equalsIgnoreCase("cheque")) {

            String from = mCriticalogSharedPreferences.getData("new_cod_received");

            if (from.equalsIgnoreCase("0")) {
                if (!staticFrom.equalsIgnoreCase("true")) {
                    PaytypeListRequest paytypeListRequest = new PaytypeListRequest();
                    paytypeListRequest.setAction("client_paytype");
                    paytypeListRequest.setUserId(userId);
                    paytypeListRequest.setClientCode(clinetCode);
                    paytypeListRequest.setDocket_no(task_id);
                    mPresenter.getPayTypeList(paytypeListRequest);
                }
            }
        } else if (modeOfpay.equalsIgnoreCase("FTC")) {
            refreshUpiApiCall();
            String from = mCriticalogSharedPreferences.getData("new_cod_received");

            if (from.equalsIgnoreCase("0")) {

                if (!staticFrom.equalsIgnoreCase("true")) {
                    PaytypeListRequest paytypeListRequest = new PaytypeListRequest();
                    paytypeListRequest.setAction("client_paytype");
                    paytypeListRequest.setUserId(userId);
                    paytypeListRequest.setClientCode(clinetCode);
                    paytypeListRequest.setDocket_no(task_id);
                    mPresenter.getPayTypeList(paytypeListRequest);
                }

            }
        } else {
            //  callCheckListApi();
        }

        if (!paymentStatus.equalsIgnoreCase("")) {
            payment_status_refresh_layout.setVisibility(View.VISIBLE);
        } else {
            payment_status_refresh_layout.setVisibility(View.GONE);

        }
        codradiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            int id;

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.online:
                        qrImageFlag = mCriticalogSharedPreferences.getData("qrcode_upload_flag");
                        if (qrImageFlag.equals("1")) {

                            if (!modeOfpay.equalsIgnoreCase("prepaid")) {
                                mPaymentRefImageText.setVisibility(View.VISIBLE);
                                mPaymentReferenceLay.setVisibility(View.VISIBLE);
                            }
                        }
                        if (online.isChecked() || !online.isChecked()) {
                            if (modeOfpay.equals("FTC")) {
                                if (ftc_status_layout == 1) {
                                    cheque = "";
                                    amnt = "";
                                    id = R.layout.ftc_qr_generatore_layout_dialog;
                                    showdialog(id);
                                    mReferenceNo.setVisibility(View.VISIBLE);
                                } else {
                                    cheque = "";
                                    amnt = "";
                                    id = R.layout.activity_qr_code_scan;
                                    showdialog(id);
                                    mReferenceNo.setVisibility(View.VISIBLE);
                                }
                            } else {
                                cheque = "";
                                amnt = "";
                                id = R.layout.activity_qr_code_scan;
                                showdialog(id);
                                mReferenceNo.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (modeOfpay.equals("FTC")) {
                                if (ftc_status_layout == 1) {
                                    cheque = "";
                                    amnt = "";
                                    id = R.layout.ftc_qr_generatore_layout_dialog;
                                    showdialog(id);
                                    mReferenceNo.setVisibility(View.VISIBLE);
                                }
                            } else {
                                cheque = "";
                                amnt = "";
                                id = R.layout.activity_qr_code_scan;
                                showdialog(id);
                                mReferenceNo.setVisibility(View.VISIBLE);
                            }
                        }
                        payType = "online";
                        break;
                    case R.id.cash:
                        mPaymentRefImageText.setVisibility(View.GONE);
                        mPaymentReferenceLay.setVisibility(View.GONE);
                        qrImageFlag = "";
                        cheque = "";
                        amnt = "";
                        id = R.layout.collectcodcash;
                        showdialog(id);
                        mReferenceNo.setVisibility(View.GONE);
                        payType = "cash";
                        break;
                    case R.id.cheque:
                        mPaymentRefImageText.setVisibility(View.GONE);
                        mPaymentReferenceLay.setVisibility(View.GONE);
                        qrImageFlag = "";
                        cheque = "";
                        amnt = "";
                        id = R.layout.cheque_layout_view;
                        Log.d("cheque", "cheque radio button" + id);
                        showdialog(id);
                        mReferenceNo.setVisibility(View.GONE);
                        payType = "cheque";
                        break;
                }
            }
        });
        mTopBarText.setText("Con No.: " + task_id);


        if (qrImageFlag.equals("1")) {

            if (!modeOfpay.equalsIgnoreCase("prepaid")) {
                mPaymentRefImageText.setVisibility(View.VISIBLE);
                mPaymentReferenceLay.setVisibility(View.VISIBLE);
            }

        } else {
            mPaymentRefImageText.setVisibility(View.GONE);
            mPaymentReferenceLay.setVisibility(View.GONE);
        }

        if (sdcMandatory.equalsIgnoreCase("1")) {
            mSDCImageText.setVisibility(View.VISIBLE);
            mSDCLinLayout.setVisibility(View.VISIBLE);
        } else {
            mSDCImageText.setVisibility(View.GONE);
            mSDCLinLayout.setVisibility(View.GONE);
        }

        callCheckListApi();
    }


    public void dialogCheckListWithFlag(List<ChecklistScan> prsRules, String checlistOtpFlag) {
        TextView mDone, xTvTaskId, mFailed;
        RecyclerView mRvRules;
        RulesAdapterWithFlag rulesAdapter;
        OtpTextView mOtpTextView;
        ImageView mOtpDialogBack;
        Button mVerifyOTPChecklist;

        CheckBox mCheckYes, mCheckNo;
        dialogCheckListWithFlag = new Dialog(DRSCloseActivity.this);
        dialogCheckListWithFlag.setContentView(R.layout.dialog_rule_with_flag);
        Window window = dialogCheckListWithFlag.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mDone = dialogCheckListWithFlag.findViewById(R.id.mDone);
        mFailed = dialogCheckListWithFlag.findViewById(R.id.mFailed);
        mRvRules = dialogCheckListWithFlag.findViewById(R.id.mRvRules);
        xTvTaskId = dialogCheckListWithFlag.findViewById(R.id.xTvTaskId);
        mChecklistWithFalgLinLayout = dialogCheckListWithFlag.findViewById(R.id.mChecklistWithFalgLinLayout);
        mOtpLinLayout = dialogCheckListWithFlag.findViewById(R.id.mOtpLinLayout);
        mOtpDialogBack = dialogCheckListWithFlag.findViewById(R.id.mOtpDialogBack);
        mVerifyOTPChecklist = dialogCheckListWithFlag.findViewById(R.id.mVerifyOTPChecklist);
        mOtpTextView = dialogCheckListWithFlag.findViewById(R.id.mOtpTextView);

        xTvTaskId.setText("Checklist");
        if (checlistOtpFlag.equalsIgnoreCase("1")) {
            mDone.setText("Generate OTP");
        } else {
            mDone.setText("DONE");
        }

        mFailed.setText("Modify");

        dialogCheckListWithFlag.setCancelable(false);
        rulesAdapter = new RulesAdapterWithFlag(prsRules);
        mRvRules.setLayoutManager(new LinearLayoutManager(this));
        mRvRules.setAdapter(rulesAdapter);

        mVerifyOTPChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = mOtpTextView.getOTP().toString();

                if (otp.equalsIgnoreCase("")) {
                    Toasty.error(DRSCloseActivity.this, "Enter OTP", Toast.LENGTH_SHORT, true).show();
                } else if (otp.length() < 4) {
                    Toasty.error(DRSCloseActivity.this, "Enter Valid OTP", Toast.LENGTH_SHORT, true).show();
                } else {
                    verifyOrSendOtp = "verifyotp";

                    OTPRequest otpVliadationChecklistRequest = new OTPRequest();
                    otpVliadationChecklistRequest.setAction("validate_otp");
                    otpVliadationChecklistRequest.setDocket_no(task_id);
                    otpVliadationChecklistRequest.setOtpMobile(phone_num);
                    otpVliadationChecklistRequest.setUserId(userId);
                    otpVliadationChecklistRequest.setOtp(otp);
                    otpVliadationChecklistRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                    otpVliadationChecklistRequest.setRound(mCriticalogSharedPreferences.getData("ROUND_NO"));
                    otpVliadationChecklistRequest.setFor_type("qc");
                    otpVliadationChecklistRequest.setIsResend("1");
                    otpVliadationChecklistRequest.setLatitude(String.valueOf(latitude));
                    otpVliadationChecklistRequest.setLongitude(String.valueOf(longitude));
                    mPresenterQC.otpValidationForChecklist(otpVliadationChecklistRequest);
                    mProgressBar.show();
                }
            }
        });

        mOtpDialogBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChecklistWithFalgLinLayout.setVisibility(View.VISIBLE);
                mOtpLinLayout.setVisibility(View.GONE);
            }
        });

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkBtnString = mDone.getText().toString();
                if (checkBtnString.equalsIgnoreCase("DONE")) {

                    ValidateCheclistYesNoRequest validateCheclistYesNoRequest = new ValidateCheclistYesNoRequest();
                    validateCheclistYesNoRequest.setAction("validate_response_checklist");
                    validateCheclistYesNoRequest.setChecklistResponse(drsRulesList);
                    validateCheclistYesNoRequest.setUserId(userId);
                    validateCheclistYesNoRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code_otp"));
                    validateCheclistYesNoRequest.setCheckType("drs");
                    validateCheclistYesNoRequest.setConnote_no(task_id);
                    validateCheclistYesNoRequest.setRemark(remarks);
                    mPresenterQC.validateChecklistYesNo(validateCheclistYesNoRequest);
                } else {
                    verifyOrSendOtp = "getotp";

                    ValidateCheclistYesNoRequest validateCheclistYesNoRequest = new ValidateCheclistYesNoRequest();
                    validateCheclistYesNoRequest.setAction("validate_response_checklist");
                    validateCheclistYesNoRequest.setChecklistResponse(drsRulesList);
                    validateCheclistYesNoRequest.setUserId(userId);
                    validateCheclistYesNoRequest.setCheckType("drs");
                    validateCheclistYesNoRequest.setConnote_no(task_id);
                    validateCheclistYesNoRequest.setRemark(remarks);
                    validateCheclistYesNoRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code_otp"));
                    mPresenterQC.validateChecklistYesNo(validateCheclistYesNoRequest);

                }

            }
        });

        mFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCheckListWithFlag.dismiss();

                dialogRules2(drsRulesList, 1);
                // startActivity(new Intent(BarcodeScanActivity.this,PickupActivity.class));
            }
        });
        dialogCheckListWithFlag.show();

    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Docket");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setBarcodeImageEnabled(true);
        integrator.setTimeout(20000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }

    private void getLocationRiderLog() {
        if (ActivityCompat.checkSelfPermission(DRSCloseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DRSCloseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DRSCloseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(DRSCloseActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (dialogProductDelivered != null) {
            if (dialogProductDelivered.isShowing()) {
                dialogProductDelivered.dismiss();
                startActivity(new Intent(this, DRSActivity.class));
                finish();
            }
        }

    }

    public String getDistanceFromGoogleAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        String distnace = "0";

        float distanceInKm = 0;
        String nextBillionHitStatus = mCriticalogSharedPreferences.getData("distance_api_status");
        if (nextBillionHitStatus.equalsIgnoreCase("1")) {
            distnace = mCriticalogSharedPreferences.getDistanceFromGoogelBillionAPI(lat1, lon1, lat2, lon2);

            distanceInKm = convertMeterToKilometer(Float.valueOf(distnace));
        } else {
            distanceInKm = 0;
        }
        return String.valueOf(distanceInKm);
    }

    public static float convertMeterToKilometer(float meter) {
        return (float) (meter * 0.001);
    }

    public void podScanAlertMesssage(String message, String photoFile) {
        //Dialog Manual Entry
        mForceAllow = dialogProductDelivered1.findViewById(R.id.mForceAllow);
        mOkDialog = dialogProductDelivered1.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered1.findViewById(R.id.mDialogText);
        mDialogText.setText(message);
        dialogProductDelivered1.setCancelable(false);
        dialogProductDelivered1.show();

        if (imageCaptureRetryFlag == 1) {
            mForceAllow.setVisibility(View.VISIBLE);
            mOkDialog.setText("Re-Scan");

        }

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogProductDelivered1.dismiss();
            }
        });

        mForceAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forceImageCapture = "1";
                Bitmap myBitmap = BitmapFactory.decodeFile(photoFile);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                base64PodCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                mPodCapture.setImageURI(Uri.fromFile(new File(photoFile)));

                dialogProductDelivered1.dismiss();
            }
        });
    }

    public void productDeliveredDialog(String message) {
        //Dialog Manual Entry
        TextView mDialogText;
        dialogProductDelivered = new Dialog(DRSCloseActivity.this);
        dialogProductDelivered.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = dialogProductDelivered.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered.findViewById(R.id.mDialogText);

        dialogProductDelivered.setCancelable(false);
        dialogProductDelivered.show();
        mDialogText.setText(message);
        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProductDelivered.dismiss();
                startActivity(new Intent(DRSCloseActivity.this, DRSActivity.class));
                finish();
            }
        });
    }

    public void codCashChequeDilaog(List<DatumPayType> results) {
        OptionsAdapter optionsAdapter;
        //List scannedresult = new ArrayList();
        RecyclerView mCodcheckcashRv;
        //Dialog Manual Entry
        TextView mCash, mCheque, mCashReal;
        ImageView mCloseDialog;

        dialogProductDelivered = new Dialog(DRSCloseActivity.this);
        dialogProductDelivered.setContentView(R.layout.cod_cash_cheque_dialoge);
        mCodcheckcashRv = dialogProductDelivered.findViewById(R.id.mCodcheckcashRv);
        mCloseDialog = dialogProductDelivered.findViewById(R.id.mCloseDialog);

        dialogProductDelivered.setCancelable(false);
        dialogProductDelivered.show();

        Window window = dialogProductDelivered.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        optionsAdapter = new OptionsAdapter(results, this, this);
        mCodcheckcashRv.setLayoutManager(new LinearLayoutManager(this));
        mCodcheckcashRv.setAdapter(optionsAdapter);

        mCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DRSCloseActivity.this, DRSActivity.class));
                finish();
            }
        });
    }

    public void callCheckListApi() {
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");
        prsRules.clear();
        RulesRequest rulesRequest = new RulesRequest();
        rulesRequest.setUserId(userId);
        rulesRequest.setAction("client_fll_rules");
        rulesRequest.setClientId(clinetCode);
        rulesRequest.setConnote_no(task_id);
        rulesRequest.setPrsDrs("DRS");
        //    mProgressDialog.show();
        Call<RulesResponse> rulesResponseCall = mRestServices.getClientRules(rulesRequest);

        rulesResponseCall.enqueue(new Callback<RulesResponse>() {
            @Override
            public void onResponse(Call<RulesResponse> call, Response<RulesResponse> response) {
                mMainLayoutRelative.setVisibility(View.VISIBLE);
                //    mProgressDialog.dismiss();

                if (response != null) {
                    if (response.body().getStatus() == 200) {
                        if (response.body().getData().getDrsRule().size() != 0 || response.body().getData().getDrsRule() != null) {
                            prsRules = response.body().getData().getDrsRulesQc();
                            // drsRulesList = new ArrayList(Arrays.asList(new Integer[prsRules.size()]));

                            if (response.body().getData().getOpenImages().size()!= 0) {
                                openDeliveryFlagForMultipleImages = 1;
                                openImageList = response.body().getData().getOpenImages();
                                photolayout.setVisibility(View.VISIBLE);
                                mOpenDeliveryTextHeader.setVisibility(View.VISIBLE);
                                mRvOpenDeliveryImages.setVisibility(View.VISIBLE);
                                mOpenDeliveryImagesLay.setVisibility(View.GONE);

                                openDeliveryImageAdpater = new OpenPickupAdpater(DRSCloseActivity.this, DRSCloseActivity.this, openImageList);
                                mRvOpenDeliveryImages.setLayoutManager(new GridLayoutManager(DRSCloseActivity.this, 3));
                                mRvOpenDeliveryImages.setAdapter(openDeliveryImageAdpater);

                            } else {
                                openDeliveryFlagForMultipleImages = 0;
                                mRvOpenDeliveryImages.setVisibility(View.GONE);
                              //  mOpenDeliveryImagesLay.setVisibility(View.VISIBLE);

                                if(openDeliveryFlag.equalsIgnoreCase("1"))
                                {
                                    photolayout.setVisibility(View.VISIBLE);
                                    mOpenDeliveryTextHeader.setVisibility(View.VISIBLE);
                                    mOpenDeliveryImagesLay.setVisibility(View.VISIBLE);
                                }else {
                                    photolayout.setVisibility(View.GONE);
                                    mOpenDeliveryTextHeader.setVisibility(View.GONE);
                                    mOpenDeliveryImagesLay.setVisibility(View.GONE);
                                }
                            }
                            for (int i = 0; i < prsRules.size(); i++) {
                                ChecklistResponse checklistResponse = new ChecklistResponse();
                                checklistResponse.setRule(prsRules.get(i).getRule());
                                checklistResponse.setFlag("");

                                ChecklistScan checklistResponse1 = new ChecklistScan();
                                checklistResponse1.setRule(prsRules.get(i).getRule());
                                checklistResponse1.setFlag("");

                                drsRulesList.add(i, checklistResponse);
                                prsRulesListPreview.add(i, checklistResponse1);
                            }

                            if (forcePrsCheck.equals("0") || forcePrsCheck.equals("")) {
                                mDCcumCheckListText.setVisibility(View.GONE);
                                mDcCumChecklistLay.setVisibility(View.GONE);
                                if (prsRules.size() > 0) {
                                    dialogRules1(prsRules, 0);
                                }

                            } else {
                                mainLayout.setVisibility(View.GONE);
                                mDCcumCheckListText.setVisibility(View.VISIBLE);
                                mDcCumChecklistLay.setVisibility(View.VISIBLE);

                                if (response.body().getData().getDrsResponseFlag().equalsIgnoreCase("0")) {
                                    dialogRules2(drsRulesList, 1);
                                } else {
                                    mainLayout.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    } else {

                        if(openDeliveryFlag.equalsIgnoreCase("1"))
                        {
                            photolayout.setVisibility(View.VISIBLE);
                            mOpenDeliveryTextHeader.setVisibility(View.VISIBLE);
                            mOpenDeliveryImagesLay.setVisibility(View.VISIBLE);
                        }else {
                            photolayout.setVisibility(View.GONE);
                            mOpenDeliveryTextHeader.setVisibility(View.GONE);
                            mOpenDeliveryImagesLay.setVisibility(View.GONE);
                        }
                        //Toast.makeText(DRSCloseActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }
            @Override
            public void onFailure(Call<RulesResponse> call, Throwable t) {
                mMainLayoutRelative.setVisibility(View.VISIBLE);
            }
        });
    }

    public void dialogRules2(List<ChecklistResponse> drsRules, int flag) {
        TextView mRuleText, mOkRemark;

        RulesDRSAdapter rulesAdapter;
        EditText mRemarkFLLRule;
        ImageView mCloseRulesDialog;
        dialogRules = new Dialog(DRSCloseActivity.this);
        dialogRules.setContentView(R.layout.dialog_rule_normal);
        Window window = dialogRules.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mOkRule = dialogRules.findViewById(R.id.mOkRule);
        mRvRules = dialogRules.findViewById(R.id.mRvRules);
        mRuleText = dialogRules.findViewById(R.id.mRuleText);
        mRemarkFLLRule = dialogRules.findViewById(R.id.mRemarkFLLRule);
        mOkRemark = dialogRules.findViewById(R.id.mOkRemark);
        mCloseRulesDialog = dialogRules.findViewById(R.id.mCloseRulesDialog);
        mSelectYesNoAlert = dialogRules.findViewById(R.id.mSelectYesNoAlert);

        if (flag == 1) {
            mOkRule.setVisibility(View.GONE);
            rulesHorizontalAdapter = new RulesDRSHorizontalAdapter(this, drsRulesList, DRSCloseActivity.this);
            mRvRules.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mRvRules.setAdapter(rulesHorizontalAdapter);
            mRvRules.suppressLayout(false);
        } else {
           /* rulesAdapter = new RulesDRSAdapter(drsRules);
            mRvRules.setLayoutManager(new LinearLayoutManager(this));
            mRvRules.setAdapter(rulesAdapter);*/

        }
        mCloseRulesDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DRSCloseActivity.this, DRSActivity.class));
                finish();
            }
        });
        mOkRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    dialogRules.dismiss();
                } else {
                    mRuleText.setVisibility(View.VISIBLE);
                    mRemarkFLLRule.setVisibility(View.VISIBLE);
                    mOkRemark.setVisibility(View.VISIBLE);
                    mRvRules.setVisibility(View.GONE);
                    mOkRule.setVisibility(View.GONE);
                }
                //dialogRules.dismiss();
            }
        });
        mOkRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                remarks = mRemarkFLLRule.getText().toString();
                dialogRules.dismiss();
                dialogCheckListWithFlag(prsRulesListPreview, forceChecListOTPEnabled);

            }
        });
        dialogRules.setCancelable(false);
        if (dialogRules != null) {
            try {
                dialogRules.show();
            } catch (Exception e) {

            }
        }
    }

    public void dialogRules1(List<DrsRulesQc> drsRules, int flag) {
        TextView mRuleText, mOkRemark;
        ImageView mCloseRulesDialog;
        RulesDRSAdapter rulesAdapter;
        EditText mRemarkFLLRule;
        RulesDRSHorizontalAdapter rulesHorizontalAdapter;
        dialogRules = new Dialog(DRSCloseActivity.this);
        dialogRules.setContentView(R.layout.dialog_rule_normal);
        Window window = dialogRules.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mOkRule = dialogRules.findViewById(R.id.mOkRule);
        mRvRules = dialogRules.findViewById(R.id.mRvRules);
        mRuleText = dialogRules.findViewById(R.id.mRuleText);
        mRemarkFLLRule = dialogRules.findViewById(R.id.mRemarkFLLRule);
        mOkRemark = dialogRules.findViewById(R.id.mOkRemark);
        mCloseRulesDialog = dialogRules.findViewById(R.id.mCloseRulesDialog);

        if (flag == 1) {
           /* mOkRule.setVisibility(View.GONE);
            rulesHorizontalAdapter = new RulesDRSHorizontalAdapter(this, prsRules, DRSCloseActivity.this);
            //   mRvRules.setLayoutManager(new LinearLayoutManager(this));
            mRvRules.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mRvRules.setAdapter(rulesHorizontalAdapter);*/
        } else {
            rulesAdapter = new RulesDRSAdapter(drsRules);
            mRvRules.setLayoutManager(new LinearLayoutManager(this));
            mRvRules.setAdapter(rulesAdapter);

        }
        mCloseRulesDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DRSCloseActivity.this, DRSActivity.class));
                finish();
            }
        });
        mOkRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    dialogRules.dismiss();
                } else {
                    mRuleText.setVisibility(View.VISIBLE);
                    mRemarkFLLRule.setVisibility(View.VISIBLE);
                    mOkRemark.setVisibility(View.VISIBLE);
                    mRvRules.setVisibility(View.GONE);
                    mOkRule.setVisibility(View.GONE);
                }

                //dialogRules.dismiss();
            }
        });
        mOkRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCriticalogSharedPreferences.getData("rule_yes_no").equalsIgnoreCase("false")) {
                    // Toast.makeText(DRSCloseActivity.this, "You cannot close DRS, because selected NO!!", Toast.LENGTH_SHORT).show();
                    dialogRules.dismiss();
                    Toasty.warning(DRSCloseActivity.this, "You cannot close DRS, because selected NO!!", Toast.LENGTH_LONG, true).show();
                    startActivity(new Intent(DRSCloseActivity.this, DRSActivity.class));
                    finish();
                } else {
                    remarks = mRemarkFLLRule.getText().toString();
                    dialogRules.dismiss();
                    mainLayout.setVisibility(View.VISIBLE);

                }
            }
        });
        dialogRules.setCancelable(false);
        if (dialogRules != null) {
            dialogRules.show();
        }

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(DRSCloseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DRSCloseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DRSCloseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(DRSCloseActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    private void showdialog(int id) {
        LayoutInflater finishDialog = LayoutInflater.from(DRSCloseActivity.this);
        final View finishDialogView = finishDialog.inflate(id, null);
        DialogFinish = new AlertDialog.Builder(DRSCloseActivity.this).create();
        DialogFinish.setView(finishDialogView);
        DialogFinish.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DialogFinish.show();
        DialogFinish.setCanceledOnTouchOutside(false);

        if (id == R.layout.activity_qr_code_scan) {
            ImageView qrcode = finishDialogView.findViewById(R.id.qrcode);

            if (qrPath.equalsIgnoreCase("")) {
                Glide.with(this).load("https://ecritica.co/eFreightLive/qr/codaccount.PNG").error(R.drawable.criticalog_qr1).into(qrcode);
            } else {
                Glide.with(this).load(qrPath).error(R.drawable.criticalog_qr1).into(qrcode);
            }
        }
        if (id == R.layout.ftc_qr_generatore_layout_dialog) {
            // Log.e("PAY_LINK", pay_link);
            DialogFinish.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            Bitmap myBitmap = QRCode.from(payLink).bitmap();
            ImageView myImage = finishDialogView.findViewById(R.id.mQRImage);
            TextView OK = finishDialogView.findViewById(R.id.mOkFtcRefernce);
            EditText mFtcRefNumber = finishDialogView.findViewById(R.id.mFtcRefNumber);
            myImage.setImageBitmap(myBitmap);
            mFtcRefNumber.setVisibility(View.GONE);

            OK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String ftcRefNum = mFtcRefNumber.getText().toString();
                    DialogFinish.dismiss();
                }
            });
        }
        if (id == R.layout.collectcodcash) {
            EditText editText = finishDialogView.findViewById(R.id.amount);
            editText.setEnabled(false);
            editText.setText(paymentAmount);
            finishDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFinish.dismiss();

                }
            });
            finishDialogView.findViewById(R.id.collectcash).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText editText = finishDialogView.findViewById(R.id.amount);
                    codamount = editText.getText().toString().trim();
                    Toast.makeText(DRSCloseActivity.this, codamount, Toast.LENGTH_SHORT).show();
                    int payTypeValue = mCriticalogSharedPreferences.getIntData("paytype_value");
                    CodChequeCollectionRequest codChequeCollectionRequest = new CodChequeCollectionRequest();
                    codChequeCollectionRequest.setAction("cod_collection_received");
                    codChequeCollectionRequest.setPaymentRecType(String.valueOf(payTypeValue));
                    codChequeCollectionRequest.setDocketNo(task_id);
                    codChequeCollectionRequest.setUserId(userId);
                    codChequeCollectionRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
                    codChequeCollectionRequest.setLongitude(mCriticalogSharedPreferences.getData("call_long"));
                    Call<CodChequeCollectionResponse> codChequeCollectionResponseCall = mRestServices.CodChequeCollection(codChequeCollectionRequest);

                    mProgressBar.show();
                    codChequeCollectionResponseCall.enqueue(new Callback<CodChequeCollectionResponse>() {
                        @Override
                        public void onResponse(Call<CodChequeCollectionResponse> call, Response<CodChequeCollectionResponse> response) {
                            mProgressBar.dismiss();
                            if (response.body().getStatus() == 200) {
                                Toast.makeText(DRSCloseActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                DialogFinish.dismiss();
                                if (dialogProductDelivered != null) {
                                    dialogProductDelivered.dismiss();
                                }
                                refreshUpiApiCall();
                            } else {
                                Toast.makeText(DRSCloseActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CodChequeCollectionResponse> call, Throwable t) {
                            mProgressBar.dismiss();
                        }
                    });

                }
            });

        }

        if (id == R.layout.cheque_layout_view) {
            mChequeNoET = finishDialogView.findViewById(R.id.chequeno_et);
            mAmountET = finishDialogView.findViewById(R.id.amount_et);
            mSaveChequeBtn = finishDialogView.findViewById(R.id.save_cheque_btn);
            mChequeCloseBtn = finishDialogView.findViewById(R.id.cheque_close_btn);

            mChequeCloseBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFinish.dismiss();

                }
            });
            mSaveChequeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cheque = mChequeNoET.getText().toString().trim();
                    amnt = mAmountET.getText().toString().trim();
                    DialogFinish.dismiss();

                }
            });
        }
    }

    public void refreshUpiApiCall() {
        RefreshUPIRequest refreshUPIRequest = new RefreshUPIRequest();
        refreshUPIRequest.setAction("refreshupi");
        refreshUPIRequest.setUserId(userId);
        refreshUPIRequest.setTaskId(task_id);

        mPresenter.onRefreshUPI(token, refreshUPIRequest);
    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);


        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            try {
                photoFile = createImageFile();

                // photoFile = createScopedStorage();

            } catch (IOException ex) {
            }
            if (photoFile != null) {
                //Uri photoURI = FileProvider.getUriForFile(this,".provider",photoFile);
                // Set the output file URI
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createScopedStorage() throws IOException {
        File image = null;
        ContentResolver resolver = this.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "my_file.txt");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "text/plain");
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOCUMENTS + "/my_folder");

        Uri uri = resolver.insert(MediaStore.Files.getContentUri("external"), contentValues);

        OutputStream outputStream = resolver.openOutputStream(uri);
        outputStream.write("Hello, world!".getBytes());
        outputStream.close();

        image = new File(uri.toString());
// Return the file
        return image;

    }

    private File createImageFile() throws IOException {
        //String imagePath;
      /* String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";*/

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );
        //   imagePath = image.getAbsolutePath();
        return image;
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE_POD);
        }
    }

    private void launchMediaScanIntent(Uri uri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(uri);
        this.sendBroadcast(mediaScanIntent);
    }

    private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
        int targetW = 600;
        int targetH = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeStream(ctx.getContentResolver()
                .openInputStream(uri), null, bmOptions);
    }


    private boolean isImageBlurred(Bitmap bitmap) {
        // Create a frame from the bitmap
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();

        // Create a TextRecognizer to detect blur
        TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();

        // Detect if the image is blurred
        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
        for (int i = 0; i < textBlocks.size(); i++) {
            TextBlock textBlock = textBlocks.valueAt(i);
            if (textBlock != null && textBlock.getComponents().size() > 0) {
                return false; // The image is not blurred
            }
        }
        return true; // The image is blurred
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //useVision="0";
        Bitmap photo = null;
        if (requestCode == 1888) {
            if (requestCode == PERMISSION_REQUEST_CODE_POD && resultCode == Activity.RESULT_OK) {

                if (photoFile != null) {

                    if (useVision.equalsIgnoreCase("0")) {
                        File absoluteFile = photoFile.getAbsoluteFile();
                        Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64PodCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        mPodCapture.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                        mDeletePhoto.setVisibility(View.VISIBLE);
                    } else {
                        CropImage.activity(Uri.fromFile(photoFile))
                                .start(this);
                    }

                    File absoluteFile = photoFile.getAbsoluteFile();
                } else {
                    requestPermission();

                    Toast.makeText(DRSCloseActivity.this, "Go To Settings and Allow Camera and Storage Permission", Toast.LENGTH_SHORT).show();
                }

            }
        } else if (requestCode == 1889) {
            if (requestCode == PERMISSION_REQUEST_CODE_OPEN_IMAGE && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64OpenImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mOpenImageCapture.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeleteOpenImage.setVisibility(View.VISIBLE);
                }
            }
        } else if (requestCode == 1891) {
            if (requestCode == PERMISSION_REQUEST_CODE_OPEN_IMAGE1 && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();
                if (absoluteFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64OpenImage1 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mOpenImageCapture1.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeleteOpenImage1.setVisibility(View.VISIBLE);
                }
            }
        } else if (requestCode == 1992) {
            if (requestCode == PERMISSION_REQUEST_SDC_IMAGE && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64SDCCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mSDCImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeleteSDCImage.setVisibility(View.VISIBLE);
                }
            }
        }
        if (requestCode == 1993) {
            if (requestCode == PERMISSION_REQUEST_DCCHECKLIST_IMAGE && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();


                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Base64DCChecklistCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mDcChecklistImageCapture.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeleteDcChecklistImage.setVisibility(View.VISIBLE);

                }
            }
        } else if (requestCode == 1994) {
            if (requestCode == PERMISSION_REQUEST_DCCHECKLIST_IMAGE1 && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Base64DCChecklistCapture1 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mDcChecklistImageCapture1.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeleteDcChecklistImage1.setVisibility(View.VISIBLE);

                }
            }
        } else if (requestCode == 1995) {
            if (requestCode == PERMISSION_REQUEST_CUSTOMOPTION1_IMAGE && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();


                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Base64PodCustomOption1 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mCustomOption1Capture1.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeletemCustomOption1Capture1.setVisibility(View.VISIBLE);

                }
            }
        } else if (requestCode == 1890) {
            File absoluteFile = photoFile.getAbsoluteFile();
            if (resultCode == 0) {
                Toast.makeText(this, "You cannot Press Back!!", Toast.LENGTH_SHORT).show();
            } else {
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CapturedMultImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                    OpenImage openImage = new OpenImage();
                    openImage.setImage(base64CapturedMultImage);
                    openImage.setLabel(openPickupText);

                    openImageList.set(positionClicked, openImage);

                    openDeliveryImageAdpater.notifyDataSetChanged();

                }
            }
        } else if (requestCode == 1996) {
            if (requestCode == PERMISSION_REQUEST_CUSTOMOPTION2_IMAGE && resultCode == Activity.RESULT_OK) {


                File absoluteFile = photoFile.getAbsoluteFile();


                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Base64PodCustomOption2 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mCustomOption1Capture2.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeletemCustomOption1Capture2.setVisibility(View.VISIBLE);

                }
            }
        } else if (requestCode == 1990) {
            if (requestCode == PERMISSION_REQUEST_PAYMENT_IMAGE && resultCode == Activity.RESULT_OK) {

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64PaymentImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    mPaymentRefImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    mDeletePaymentRefImage.setVisibility(View.VISIBLE);

                }
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            File absoluteFile = null;
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                absoluteFile = new File(resultUri.getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
            try {
                Bitmap bitmap = decodeBitmapUri(this, Uri.fromFile(absoluteFile.getAbsoluteFile()));

                boolean isBlurr = isImageBlurred(bitmap);





               /*if(isBlurr==true)
               {
                   Toast.makeText(this, "BLUR IMAGE", Toast.LENGTH_SHORT).show();
               }else {
                   Toast.makeText(this, "NOT A BLUR IMAGE", Toast.LENGTH_SHORT).show();
               }*/

                if (detector.isOperational() && bitmap != null) {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<Barcode> barcodes = detector.detect(frame);
                    for (int index = 0; index < barcodes.size(); index++) {
                        Barcode code = barcodes.valueAt(index);

                        if (bulk_flag.equalsIgnoreCase("1")) {
                            forceImageCapture = "0";
                            Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            myBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            base64PodCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                            mPodCapture.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                            int type = barcodes.valueAt(index).valueFormat;
                        } else {
                            if (task_id.equalsIgnoreCase(reprint_con)) {
                                if (code.displayValue.equalsIgnoreCase(task_id)) {
                                    forceImageCapture = "0";
                                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                                    base64PodCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                    mPodCapture.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                                    int type = barcodes.valueAt(index).valueFormat;
                                } else {
                                    podScanAlertMesssage("Wrong Connote Scan Re-Scan Again", absoluteFile.getAbsolutePath());
                                    imageCaptureRetryFlag++;
                                }
                            } else if (code.displayValue.equalsIgnoreCase(reprint_con)) {
                                forceImageCapture = "0";
                                Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                myBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                base64PodCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                mPodCapture.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                                int type = barcodes.valueAt(index).valueFormat;
                            } else {
                                Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                myBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                base64PodCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                int type = barcodes.valueAt(index).valueFormat;
                                manualDialogBookingIdMandatory(forcePrintFlag, Uri.fromFile(absoluteFile.getAbsoluteFile()));
                            }

                        }

                    }
                    if (barcodes.size() == 0) {
                        mPodCapture.setImageURI(null);
                        mPodCapture.setImageResource(R.drawable.outline_photo_camera_24);
                        podScanAlertMesssage("Wrong Image Captured Scan Again", absoluteFile.getAbsolutePath());
                        imageCaptureRetryFlag++;
                    }
                } else {
                    Toast.makeText(this, "Detector initialization failed", Toast.LENGTH_SHORT).show();
                    // txtResultBody.setText("Detector initialisation failed");
                    podScanAlertMesssage("Detector initialization failed", absoluteFile.getAbsolutePath());
                    imageCaptureRetryFlag++;
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Failed to load Image", Toast.LENGTH_SHORT)
                        .show();
                Log.e("TAG", e.toString());
            }
        }

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public void manualDialogBookingIdMandatory(String madndatoryFlag, Uri absolutePath) {
        EditText mEnterReason;
        TextView mOk;
        //Dialog Manual Entry
        dialogReasonForCitPrintMismatch = new Dialog(DRSCloseActivity.this);
        dialogReasonForCitPrintMismatch.setContentView(R.layout.dialog_cit_reprint_reason);
        dialogReasonForCitPrintMismatch.setCanceledOnTouchOutside(false);
        dialogReasonForCitPrintMismatch.setCancelable(false);

        mEnterReason = dialogReasonForCitPrintMismatch.findViewById(R.id.mEnterReason);
        mOk = dialogReasonForCitPrintMismatch.findViewById(R.id.mOk);

        Window window = dialogReasonForCitPrintMismatch.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        dialogReasonForCitPrintMismatch.show();

        if (madndatoryFlag.equalsIgnoreCase("1")) {
            mEnterReason.setVisibility(View.GONE);
        }

        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reasonMessageCITPrint = mEnterReason.getText().toString();
                if (madndatoryFlag.equalsIgnoreCase("1")) {
                    dialogReasonForCitPrintMismatch.dismiss();
                    startActivity(new Intent(DRSCloseActivity.this, DRSActivity.class));
                    finish();
                } else {
                    if (!reasonMessageCITPrint.equalsIgnoreCase("")) {
                        mPodCapture.setImageURI(absolutePath);
                        dialogReasonForCitPrintMismatch.dismiss();
                    } else {
                        Toasty.warning(DRSCloseActivity.this, "Enter Valid Reason!!", Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        });


    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_POD:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults.length > 0 && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[3] == PackageManager.PERMISSION_GRANTED
                        && grantResults.length > 0 && grantResults[4] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[5] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    showLocationDialog("");

                }
                break;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mPodCapture:
                //  startBarcodeScan();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_POD);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_CODE_POD);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_CODE_POD);
                }
                break;
            case R.id.mOpenImageCapture:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_OPEN_IMAGE);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_CODE_OPEN_IMAGE);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_CODE_OPEN_IMAGE);
                }
                break;
            case R.id.mOpenImageCapture1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_OPEN_IMAGE1);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_CODE_OPEN_IMAGE1);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_CODE_OPEN_IMAGE1);
                }
                break;
            case R.id.mSDCImage:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_SDC_IMAGE);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_SDC_IMAGE);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_SDC_IMAGE);
                }
                break;

            case R.id.mDcChecklistImageCapture:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_DCCHECKLIST_IMAGE);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_DCCHECKLIST_IMAGE);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_DCCHECKLIST_IMAGE);
                }
                break;
            case R.id.mDcChecklistImageCapture1:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_DCCHECKLIST_IMAGE1);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_DCCHECKLIST_IMAGE1);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_DCCHECKLIST_IMAGE1);
                }
                break;

            case R.id.mCustomOption1Capture1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CUSTOMOPTION1_IMAGE);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_CUSTOMOPTION1_IMAGE);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_CUSTOMOPTION1_IMAGE);
                }
                break;
            case R.id.mCustomOption1Capture2:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CUSTOMOPTION2_IMAGE);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_CUSTOMOPTION2_IMAGE);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_CUSTOMOPTION2_IMAGE);
                }
                break;
            case R.id.mDeletePhoto:
                mPodCapture.setImageDrawable(null);
                base64PodCapture = "";
                mPodCapture.setImageResource(R.drawable.outline_photo_camera_24);
                mDeletePhoto.setVisibility(View.GONE);
                break;
            case R.id.mDeleteOpenImage:
                mOpenImageCapture.setImageDrawable(null);
                base64OpenImage = "";
                mOpenImageCapture.setImageResource(R.drawable.outline_photo_camera_24);
                mDeleteOpenImage.setVisibility(View.GONE);
                break;
            case R.id.mDeleteOpenImage1:
                mOpenImageCapture1.setImageDrawable(null);
                base64OpenImage1 = "";
                mOpenImageCapture1.setImageResource(R.drawable.outline_photo_camera_24);
                mDeleteOpenImage1.setVisibility(View.GONE);
                break;
            case R.id.mDeleteSDCImage:
                mSDCImage.setImageDrawable(null);
                base64SDCCapture = "";
                mSDCImage.setImageResource(R.drawable.outline_photo_camera_24);
                mDeleteSDCImage.setVisibility(View.GONE);
                break;
            case R.id.mDeleteDcChecklistImage:
                mDcChecklistImageCapture.setImageDrawable(null);
                Base64DCChecklistCapture = "";
                mDcChecklistImageCapture.setImageResource(R.drawable.outline_photo_camera_24);
                mDeleteDcChecklistImage.setVisibility(View.GONE);
                break;
            case R.id.mDeleteDcChecklistImage1:
                mDcChecklistImageCapture1.setImageDrawable(null);
                Base64DCChecklistCapture1 = "";
                mDcChecklistImageCapture1.setImageResource(R.drawable.outline_photo_camera_24);
                mDeleteDcChecklistImage1.setVisibility(View.GONE);
                break;

            case R.id.mDeletemCustomOption1Capture1:
                mCustomOption1Capture1.setImageDrawable(null);
                Base64PodCustomOption1 = "";
                mCustomOption1Capture1.setImageResource(R.drawable.outline_photo_camera_24);
                mDeletemCustomOption1Capture1.setVisibility(View.GONE);
                break;
            case R.id.mDeletemCustomOption1Capture2:
                mCustomOption1Capture2.setImageDrawable(null);
                Base64PodCustomOption2 = "";
                mCustomOption1Capture2.setImageResource(R.drawable.outline_photo_camera_24);
                mDeletemCustomOption1Capture2.setVisibility(View.GONE);
                break;
            case R.id.mRefreshUpi:
                refreshUpiApiCall();
                break;

            case R.id.mDone:
                referenceNum = mUpiRefNumber.getText().toString();
                // getLocationRiderLog();
                getLocation();

                if (payType.equalsIgnoreCase("online")) {
                    if (referenceNum.equals("")) {
                        Toast.makeText(DRSCloseActivity.this, "Enter Reference Number", Toast.LENGTH_SHORT).show();
                    }
                }
                if (mReceiverName.getText().toString().equals("")) {
                    Toast.makeText(DRSCloseActivity.this, R.string.enter_rceiver_name, Toast.LENGTH_SHORT).show();
                } else if (base64PodCapture.equals("")) {
                    Toast.makeText(DRSCloseActivity.this, R.string.capture_pod_image, Toast.LENGTH_SHORT).show();
                } else if (qrImageFlag.equals("1")) {

                    if (!modeOfpay.equalsIgnoreCase("prepaid")) {
                        if (base64PaymentImage.equals("")) {
                            Toast.makeText(DRSCloseActivity.this, R.string.capture_payment_ref_image, Toast.LENGTH_SHORT).show();
                        } else {
                            qrImageFlagInt = 1;

                            if (openDeliveryFlag.equals("1")) {
                                if (openDeliveryFlagForMultipleImages == 1) {
                                    for (int i = 0; i < openImageList.size(); i++) {
                                        if (openImageList.get(i).getImage().equalsIgnoreCase("")) {
                                            Toast.makeText(this, "Capture All Open Delivery Images", Toast.LENGTH_SHORT).show();
                                            break;
                                        }
                                        if (i == openImageList.size() - 1) {
                                            if (sdcMandatory.equalsIgnoreCase("1")) {
                                                if (base64SDCCapture.equals("")) {
                                                    Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                                } else {

                                                    if (forcePrsCheck.equals("1")) {
                                                        if (Base64DCChecklistCapture.equals("")) {
                                                            Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                        } else {

                                                            callProductDelivered();
                                                        }
                                                    } else {
                                                        callProductDelivered();
                                                    }

                                                }
                                            } else {
                                                if (forcePrsCheck.equals("1")) {
                                                    if (Base64DCChecklistCapture.equals("")) {
                                                        Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                    } else {

                                                        callProductDelivered();
                                                    }
                                                } else {
                                                    callProductDelivered();
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    if (base64OpenImage.equals("")) {
                                        Toast.makeText(DRSCloseActivity.this, "Upload Open Delivery Image!!", Toast.LENGTH_SHORT).show();
                                    } else {

                                        if (sdcMandatory.equalsIgnoreCase("1")) {
                                            if (base64SDCCapture.equals("")) {
                                                Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                            } else {

                                                if (forcePrsCheck.equals("1")) {
                                                    if (Base64DCChecklistCapture.equals("")) {
                                                        Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                    } else {

                                                        callProductDelivered();
                                                    }
                                                } else {
                                                    callProductDelivered();
                                                }

                                            }
                                        } else {
                                            if (forcePrsCheck.equals("1")) {
                                                if (Base64DCChecklistCapture.equals("")) {
                                                    Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                } else {

                                                    callProductDelivered();
                                                }
                                            } else {
                                                callProductDelivered();
                                            }
                                        }
                                    }
                                }

                            } else {
                                if (sdcMandatory.equalsIgnoreCase("1")) {
                                    if (base64SDCCapture.equals("")) {
                                        Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (forcePrsCheck.equals("1")) {
                                            if (Base64DCChecklistCapture.equals("")) {
                                                Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                callProductDelivered();
                                            }
                                        } else {
                                            callProductDelivered();
                                        }
                                    }
                                } else {
                                    if (forcePrsCheck.equals("1")) {
                                        if (Base64DCChecklistCapture.equals("")) {
                                            Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            callProductDelivered();
                                        }
                                    } else {
                                        callProductDelivered();
                                    }
                                }
                            }
                        }
                    } else {
                        qrImageFlagInt = 0;

                        if (openDeliveryFlag.equals("1")) {
                            if (openDeliveryFlagForMultipleImages == 1) {

                                if (openImageList.size() > 0) {
                                    for (int i = 0; i < openImageList.size(); i++) {
                                        if (openImageList.get(i).getImage().equalsIgnoreCase("")) {
                                            Toast.makeText(this, "Capture All Open Delivery Images", Toast.LENGTH_SHORT).show();
                                            break;
                                        }

                                        if (i == openImageList.size() - 1) {

                                            if (sdcMandatory.equalsIgnoreCase("1")) {
                                                if (base64SDCCapture.equals("")) {
                                                    Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    if (forcePrsCheck.equals("1")) {
                                                        if (Base64DCChecklistCapture.equals("")) {
                                                            Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            callProductDelivered();
                                                        }
                                                    } else {
                                                        callProductDelivered();
                                                    }
                                                }
                                            } else {
                                                if (forcePrsCheck.equals("1")) {
                                                    if (Base64DCChecklistCapture.equals("")) {
                                                        Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        callProductDelivered();
                                                    }
                                                } else {
                                                    callProductDelivered();
                                                }
                                            }
                                        }
                                    }
                                }
                                Toast.makeText(this, "Call PRODUCT DELIVERED", Toast.LENGTH_SHORT).show();
                            } else {
                                if (base64OpenImage.equals("")) {
                                    Toast.makeText(DRSCloseActivity.this, "Upload Open Delivery Image!!", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (sdcMandatory.equalsIgnoreCase("1")) {
                                        if (base64SDCCapture.equals("")) {
                                            Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            if (forcePrsCheck.equals("1")) {
                                                if (Base64DCChecklistCapture.equals("")) {
                                                    Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    callProductDelivered();
                                                }
                                            } else {
                                                callProductDelivered();
                                            }
                                        }
                                    } else {
                                        if (forcePrsCheck.equals("1")) {
                                            if (Base64DCChecklistCapture.equals("")) {
                                                Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                callProductDelivered();
                                            }
                                        } else {
                                            callProductDelivered();
                                        }
                                    }
                                }
                            }

                        } else {
                            if (sdcMandatory.equalsIgnoreCase("1")) {
                                if (base64SDCCapture.equals("")) {
                                    Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (forcePrsCheck.equals("1")) {
                                        if (Base64DCChecklistCapture.equals("")) {
                                            Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            callProductDelivered();
                                        }
                                    } else {
                                        callProductDelivered();
                                    }
                                }
                            } else {
                                if (forcePrsCheck.equals("1")) {
                                    if (Base64DCChecklistCapture.equals("")) {
                                        Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        callProductDelivered();
                                    }
                                } else {
                                    callProductDelivered();
                                }
                            }
                        }
                    }
                } else {
                    qrImageFlagInt = 0;
                    if (openDeliveryFlag.equals("1")) {

                        if (openDeliveryFlagForMultipleImages == 1) {
                            if (openImageList.size() > 0) {
                                for (int i = 0; i < openImageList.size(); i++) {
                                    if (openImageList.get(i).getImage().equalsIgnoreCase("")) {
                                        Toast.makeText(this, "Capture All Open Delivery Images", Toast.LENGTH_SHORT).show();
                                        break;
                                    }

                                    if (i == openImageList.size() - 1) {
                                        if (sdcMandatory.equalsIgnoreCase("1")) {
                                            if (base64SDCCapture.equals("")) {
                                                Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                if (forcePrsCheck.equals("1")) {
                                                    if (Base64DCChecklistCapture.equals("")) {
                                                        Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        callProductDelivered();
                                                    }
                                                } else {
                                                    callProductDelivered();
                                                }
                                            }
                                        } else {
                                            if (forcePrsCheck.equals("1")) {
                                                if (Base64DCChecklistCapture.equals("")) {
                                                    Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    callProductDelivered();
                                                }
                                            } else {
                                                callProductDelivered();
                                            }
                                        }
                                    }
                                }
                            }

                        } else {
                            if (base64OpenImage.equals("")) {
                                Toast.makeText(DRSCloseActivity.this, "Upload Open Delivery Image!!", Toast.LENGTH_SHORT).show();
                            } else {
                                if (sdcMandatory.equalsIgnoreCase("1")) {
                                    if (base64SDCCapture.equals("")) {
                                        Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (forcePrsCheck.equals("1")) {
                                            if (Base64DCChecklistCapture.equals("")) {
                                                Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                callProductDelivered();
                                            }
                                        } else {
                                            callProductDelivered();
                                        }
                                    }
                                } else {
                                    if (forcePrsCheck.equals("1")) {
                                        if (Base64DCChecklistCapture.equals("")) {
                                            Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            callProductDelivered();
                                        }
                                    } else {
                                        callProductDelivered();
                                    }
                                }
                            }
                        }

                    } else {
                        if (sdcMandatory.equalsIgnoreCase("1")) {
                            if (base64SDCCapture.equals("")) {
                                Toast.makeText(DRSCloseActivity.this, "Upload SDC Image!!", Toast.LENGTH_SHORT).show();
                            } else {
                                if (forcePrsCheck.equals("1")) {
                                    if (Base64DCChecklistCapture.equals("")) {
                                        Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        callProductDelivered();
                                    }
                                } else {
                                    callProductDelivered();
                                }
                            }
                        } else {
                            if (forcePrsCheck.equals("1")) {
                                if (Base64DCChecklistCapture.equals("")) {
                                    Toast.makeText(DRSCloseActivity.this, "Upload DC Cum Checklist Image!!", Toast.LENGTH_SHORT).show();
                                } else {
                                    callProductDelivered();
                                }
                            } else {
                                callProductDelivered();
                            }
                        }
                    }
                }
                break;

            case R.id.mTvBackbutton:
                Intent intent = new Intent(DRSCloseActivity.this, DRSActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            case R.id.mPaymentRefImage:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_OPEN_IMAGE);
                    } else {
                        openCameraIntent(PERMISSION_REQUEST_PAYMENT_IMAGE);
                    }
                }
                break;
            case R.id.mDeletePaymentRefImage:
                mPaymentRefImage.setImageDrawable(null);
                base64PaymentImage = "";
                mPaymentRefImage.setImageResource(R.drawable.outline_photo_camera_24);
                mDeletePaymentRefImage.setVisibility(View.GONE);
                break;

            case R.id.mGatePassImage:
                showGatePassDialog(gatePassImageURL);
                break;



        }
    }

    public void showGatePassDialog(String bannerURL) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_gate_pass);
        // Set dialog title
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // set values for custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.mBannerText);
        ImageView image = (ImageView) dialog.findViewById(R.id.mImageBanner);
        ImageView cancelDialog = dialog.findViewById(R.id.mDialogCancel);
        TextView moreUpdates = dialog.findViewById(R.id.mMoreUpdates);

        moreUpdates.setText("OK");
        moreUpdates.setVisibility(View.GONE);
        cancelDialog.setVisibility(View.VISIBLE);

        // bannerURL = "https://source.unsplash.com/user/c_v_r/1900x800";
        Picasso.with(this).load(bannerURL).into(image);

        dialog.show();

        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        moreUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
        //   mProgressBar.show();
    }

    public void callProductDelivered() {
        if (custom_option1.equalsIgnoreCase("")) {
            callProductDeliveredFinal();
        } else {
            if (Base64PodCustomOption1.equalsIgnoreCase("")) {
                Toast.makeText(DRSCloseActivity.this, "Upload " + custom_option1 + " Details Image!!", Toast.LENGTH_SHORT).show();
            } else {
                callProductDeliveredFinal();
            }
        }
    }

    void callProductDeliveredFinal() {
        Log.e("LAT_LONG_SHREE_DELIVERY", String.valueOf(latitude) + "  " + String.valueOf(longitude));
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c);

        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        String time = df1.format(c);
        ProductDeliveredRequest productDeliveredRequest = new ProductDeliveredRequest();
        productDeliveredRequest.setAction("product_delivered");
        productDeliveredRequest.setBulkFlag(bulk_flag);
        productDeliveredRequest.setDate(date);
        productDeliveredRequest.setRefcount(Drs_no);
        productDeliveredRequest.setUserId(userId);
        productDeliveredRequest.setAmount(amnt);
        productDeliveredRequest.setChequeno(cheque);
        productDeliveredRequest.setCodAmount(codamount);
        productDeliveredRequest.setUpirefno(referenceNum);
        productDeliveredRequest.setTime(time);
        productDeliveredRequest.setTaskId(task_id);
        productDeliveredRequest.setSwap(swap);
        productDeliveredRequest.setStatus("1");
        productDeliveredRequest.setRecieverName(mReceiverName.getText().toString());
        productDeliveredRequest.setModeofpay(modeOfpay);
        productDeliveredRequest.setLongitude(String.valueOf(longitude));
        productDeliveredRequest.setLatitude(String.valueOf(latitude));
        productDeliveredRequest.setImagePod(base64PodCapture);
        productDeliveredRequest.setImage2(base64OpenImage1);
        productDeliveredRequest.setImage1(base64OpenImage);
        productDeliveredRequest.setCodTaskid(task_id);
        productDeliveredRequest.setCodPaytype(payType);
        productDeliveredRequest.setQrimage(base64PaymentImage);
        productDeliveredRequest.setQrFlag(qrImageFlagInt);
        productDeliveredRequest.setMedia("1");// As subhash suggested always "1"
        productDeliveredRequest.setRemark(remarks);
        productDeliveredRequest.setSigneddc_1(base64SDCCapture);
        productDeliveredRequest.setSigneddc_2("");
        productDeliveredRequest.setChecklist_1(Base64DCChecklistCapture);
        productDeliveredRequest.setChecklist_2(Base64DCChecklistCapture1);
        productDeliveredRequest.setCustom_option1_1(Base64PodCustomOption1);
        productDeliveredRequest.setCustom_option1_2(Base64PodCustomOption2);
        productDeliveredRequest.setForce_image_capture(forceImageCapture);
        productDeliveredRequest.setOpenImageList(openImageList);
        productDeliveredRequest.setReason_cit_reprint(reasonMessageCITPrint);


        mPresenter.productDeliveredCall(token, productDeliveredRequest);
    }

    @Override
    public void hideProgress() {
        if (mProgress != null) {
            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void setDataToViews(DocketValidateResponse docketValidateResponse) {

    }

    @Override
    public void pickUpSuccessViews(PickupSuccessResponse pickupSuccessResponse) {

    }

    @Override
    public void pincodeValidateSuccessViews(PincodeValidateResponse pincodeValidateResponse) {

    }

    @Override
    public void closeDocketSuccessViews(CloseDocketResponse closeDocketResponse) {

    }

    @Override
    public void closeClientSuccessViews(CloseClientResponse closeClientResponse) {

    }

    @Override
    public void qcItems(ItemListResponse itemListResponse) {

    }

    @Override
    public void qcRules(QCRulesResponse qcRulesResponse) {

    }

    @Override
    public void qcCheckResponse(QcRulesCheckResponse qcRulesCheckResponse) {

    }

    @Override
    public void qcUpdateResponse(QcRulesUpdateResponse qcRulesUpdateResponse) {

    }

    @Override
    public void qcNDCUpdateResponse(QCPassFailResponse qcPassFailResponse) {

    }

    @Override
    public void getDocketListWithOriginDestResponse(DocketListWithOriginDestResponse docketListWithOriginDestResponse) {

    }

    @Override
    public void actualValueResponse(ActualValueCheckResponse actualValueCheckResponse) {

    }

    @Override
    public void otpValidationChecklistResponse(OTPResponse otpVliadationChecklistResponse) {
        mProgressBar.dismiss();
        if (otpVliadationChecklistResponse.getStatus() == 200) {
            Toasty.success(this, otpVliadationChecklistResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            if (verifyOrSendOtp.equalsIgnoreCase("verifyotp")) {
                dialogCheckListWithFlag.dismiss();
                mainLayout.setVisibility(View.VISIBLE);

                // getQCItemList();
            } else if (verifyOrSendOtp.equalsIgnoreCase("getOtp")) {
                mChecklistWithFalgLinLayout.setVisibility(View.GONE);
                mOtpLinLayout.setVisibility(View.VISIBLE);
            }
        } else {
            Toasty.error(this, otpVliadationChecklistResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }

    }

    @Override
    public void validateChecklistYesNoResponse(ValidateChecklistYesNoResponse validateChecklistYesNoResponse) {
        if (validateChecklistYesNoResponse.getStatus() == 200) {

            if (verifyOrSendOtp.equalsIgnoreCase("getotp")) {
                OTPRequest otpVliadationChecklistRequest = new OTPRequest();
                otpVliadationChecklistRequest.setAction("validate_otp");
                otpVliadationChecklistRequest.setDocket_no(task_id);
                otpVliadationChecklistRequest.setOtpMobile(phone_num);
                otpVliadationChecklistRequest.setUserId(userId);
                otpVliadationChecklistRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code_otp"));
                otpVliadationChecklistRequest.setRound(mCriticalogSharedPreferences.getData("ROUND_NO"));
                otpVliadationChecklistRequest.setFor_type("qc");
                otpVliadationChecklistRequest.setIsResend("2");
                otpVliadationChecklistRequest.setLatitude(String.valueOf(latitude));
                otpVliadationChecklistRequest.setLongitude(String.valueOf(longitude));
                mPresenterQC.otpValidationForChecklist(otpVliadationChecklistRequest);
                mProgressBar.show();
            } else {
                mChecklistWithFalgLinLayout.setVisibility(View.GONE);
                dialogCheckListWithFlag.dismiss();
                mainLayout.setVisibility(View.VISIBLE);
            }
            Toasty.success(DRSCloseActivity.this, validateChecklistYesNoResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        } else {
            Toasty.error(DRSCloseActivity.this, validateChecklistYesNoResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }

    }

    @Override
    public void setDataUpiViews(RefreshUPIResponse refreshUPIResponse) {
        Toast.makeText(this, refreshUPIResponse.getMessage(), Toast.LENGTH_SHORT).show();
        if (refreshUPIResponse.getStatus() == 200) {
            payment_status_refresh_layout.setVisibility(View.VISIBLE);
            mTvPaymentStatus.setText("Payment Status: " + refreshUPIResponse.getMessage());
            mTvPaymentStatus.setTextColor(Color.GREEN);
        } else {
            payment_status_refresh_layout.setVisibility(View.VISIBLE);
            mTvPaymentStatus.setText("Payment Status: " + refreshUPIResponse.getMessage());
            mTvPaymentStatus.setTextColor(Color.RED);
        }
    }

    @Override
    public void productDeliveredResponseToViews(ProductDeliveredResponse productDeliveredResponse) {
        String convertedstring = "";
        getLocationRiderLog();
        if (productDeliveredResponse.getStatus() == 200) {
            Toast.makeText(DRSCloseActivity.this, productDeliveredResponse.getMessage(), Toast.LENGTH_SHORT).show();
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(c);

            SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
            String time = df1.format(c);

            getLocation();
            getLocationRiderLog();

            ArrayList<PodClaimPojo> latlonglist = dbHelper.getAllLatLongList();


            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    String convertedstring = "";
                    ArrayList<PodClaimPojo> latlonglist = dbHelper.getAllLatLongList();

                    if (latlonglist.size() > 0) {
                        int listsize = latlonglist.size();
                        PodClaimPojo podClaimPojo;
                        if (latlonglist.get(listsize - 1).getEdn_lat() == 0 || latlonglist.get(listsize - 1).getEnd_long() == 0 || latitude == 0 || longitude == 0) {
                            podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, "0", "0", date, time, "1", task_id, "drs", c.toString(), 200);
                        } else {
                            double distnce = distanceCalc(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                            convertedstring = converttorealnumber(distnce);
                            String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);

                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                convertedstring_g = convertedstring;
                            }
                            podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", task_id, "drs", c.toString(), 200);
                        }

                        latlonglist.add(podClaimPojo);
                        dbHelper.insertLatLOngDetails(podClaimPojo);

                    } else {
                        PodClaimPojo podClaimPojo = null;
                        if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {

                            String lastLatLongList = dbHelper.getLastLatLongDetails();
                            if (!lastLatLongList.equals("-")) {
                                String[] bits = lastLatLongList.split("-");
                                String lastOne = bits[bits.length - 1];

                                double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                convertedstring = converttorealnumber(distnce);
                                String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                    convertedstring_g = convertedstring;
                                }
                                podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", task_id, "drs", c.toString(), 200);

                            }
                        } else {
                            String lastLatLongList = dbHelper.getLastLatLongDetails();
                            if (!lastLatLongList.equals("-")) {
                                String[] bits = lastLatLongList.split("-");
                                String lastOne = bits[bits.length - 1];
                                double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                convertedstring = converttorealnumber(distnce);
                                String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                    convertedstring_g = convertedstring;
                                }
                                podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, convertedstring, convertedstring_g, date, time, "1", task_id, "drs", c.toString(), 200);
                            }
                        }

                        latlonglist.add(podClaimPojo);
                        dbHelper.insertLatLOngDetails(podClaimPojo);

                        mCriticalogSharedPreferences.saveData("start_date", date);
                        mCriticalogSharedPreferences.saveData("start_time", time);
                    }
                    mCriticalogSharedPreferences.saveDoubleData("last_activity_lat", latitude);
                    mCriticalogSharedPreferences.saveDoubleData("last_activity_long", longitude);
                }
            });
            userRiderLog("");
            thread.start();
            // userRiderLog("");
            productDeliveredDialog(productDeliveredResponse.getMessage());
        } else {
            Toast.makeText(DRSCloseActivity.this, productDeliveredResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void payTypeListResponse(PayTypeListResponse payTypeListResponse) {
        List<DatumPayType> results = new ArrayList<>();
        if (payTypeListResponse.getStatus() == 200) {
            results = payTypeListResponse.getData();

        }
        if (results.size() > 0) {
            codCashChequeDilaog(results);
        } else {
            //callCheckListApi();
        }
    }

    public void userRiderLog(String lastKM) {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;

        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo("");
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId("");
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(task_id);
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("5");
        riderLogRequest.setmLastKm(lastKM);
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("3");
        riderLogRequest.setActionNumber(5);
        riderLogRequest.setOsVersion(androidOS);

        dbHelper.insertLastLatLong(userId, String.valueOf(latitude), String.valueOf(longitude));

        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

        Toast.makeText(DRSCloseActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }

    private double distanceCalc(double lat1, double lon1, double lat2, double lon2) {
        double distance;
        double distance1;
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(lon1);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(lon2);

        distance = startPoint.distanceTo(endPoint);

        distance1 = distance / 1000;

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        System.out.println(numberFormat.format(distance1));

        return Double.parseDouble(numberFormat.format(distance1));
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist / 0.62137;

        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    private boolean checktaskalreadyadded(ArrayList<PodClaimPojo> latlonglist, String docket) {
        boolean status = false;
        for (int i = 0; i < latlonglist.size(); i++) {
            PodClaimPojo podClaimPojo = latlonglist.get(i);
            if (podClaimPojo.getTaskid().equalsIgnoreCase(docket))
                status = true;
            else {
                status = false;
            }
        }

        return status;
    }

    @Override
    public void onclickRulesList(String yesNo, int position) {
        if (yesNo.equalsIgnoreCase("scrollto")) {
            mRvRules.scrollToPosition(position + 1);
        }

        if (yesNo.equalsIgnoreCase("scrollback")) {
            mRvRules.scrollToPosition(position - 1);
        }
        if (position <= prsRules.size()) {
            if (yesNo.equalsIgnoreCase("yes")) {
                drsRulesList.set(position, new ChecklistResponse(prsRules.get(position).getRule(), "1", prsRules.get(position).getImage()));
                prsRulesListPreview.set(position, new ChecklistScan(prsRules.get(position).getRule().toString(), "1"));
                // rulesHorizontalAdapter = new RulesHorizontalAdapter(prsRulesList, BarcodeScanActivity.this);

                rulesHorizontalAdapter.notifyDataSetChanged();

            } else if (yesNo.equalsIgnoreCase("no")) {

                drsRulesList.set(position, new ChecklistResponse(prsRules.get(position).getRule(), "0", prsRules.get(position).getImage()));
                prsRulesListPreview.set(position, new ChecklistScan(prsRules.get(position).getRule().toString(), "0"));
                rulesHorizontalAdapter.notifyDataSetChanged();

            }
        }

        for (int i = 0; i < drsRulesList.size(); i++) {
            if (drsRulesList.get(i).getFlag().equalsIgnoreCase("")) {
                mOkRule.setVisibility(View.GONE);
                mSelectYesNoAlert.setVisibility(View.VISIBLE);
                break;
            } else {
                mSelectYesNoAlert.setVisibility(View.GONE);
                mOkRule.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onclickAdapter(int value, String payType) {
        mCriticalogSharedPreferences.saveData("part_payment", "");
        if (value == 1) {
            mCriticalogSharedPreferences.saveIntData("paytype_value", value);
            showdialog(R.layout.collectcodcash);
        } else if (value == 2) {
            mCriticalogSharedPreferences.saveIntData("paytype_value", value);
            startActivity(new Intent(DRSCloseActivity.this, NewCodActivity.class));
        } else if (value == 3) {
            mCriticalogSharedPreferences.saveIntData("paytype_value", value);
            startActivity(new Intent(DRSCloseActivity.this, ChequeDetails.class));
        } else if (value == 4) {
            mCriticalogSharedPreferences.saveIntData("paytype_value", value);
            startActivity(new Intent(DRSCloseActivity.this, ShowQrCodeActivity.class));
        } else if (value == 5) {
            mCriticalogSharedPreferences.saveIntData("paytype_value", value);
            mCriticalogSharedPreferences.saveData("part_payment", "true");
            startActivity(new Intent(DRSCloseActivity.this, ShowQrCodeActivity.class));
        }
    }

    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void openPickupAdapter(int position, String message) {
        positionClicked = position;
        openPickupText = message;
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        openCameraIntent(CAMERA_REQUEST_MULT);
    }
}
