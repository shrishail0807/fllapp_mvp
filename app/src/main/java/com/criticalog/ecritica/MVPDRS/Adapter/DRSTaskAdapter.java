package com.criticalog.ecritica.MVPDRS.Adapter;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Activities.ReadOtpActivity;
import com.criticalog.ecritica.Interface.CallMaskInterface;
import com.criticalog.ecritica.Interface.IAdapterClickListener;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSModel.DataDRS;
import com.criticalog.ecritica.MVPDRS.DRSModel.TaskDRS;
import com.criticalog.ecritica.MVPDRS.Interfaces.ICancelOnClick;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestClient1;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Rest.RestServices1;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.model.CallRequest;
import com.criticalog.ecritica.model.CallResponse;
import com.criticalog.ecritica.model.RulesRequest;
import com.criticalog.ecritica.model.RulesResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DRSTaskAdapter extends RecyclerView.Adapter<DRSTaskAdapter.TaskAdapterViewHolder> {
    List<TaskDRS> taskDRSList = new ArrayList<>();
    Context context;
    DataDRS dataDRS;
    ICancelOnClick iCancelOnClick;
    IAdapterClickListener iAdapterClickListener;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private RestServices mRestServices;
    private ProgressDialog mProgressDialog;
    private RestServices1 mRestServices1;
    private CallMaskInterface callMaskInterface;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private Dialog manualMobileNumberDialog;


    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;

    @RequiresApi(api = Build.VERSION_CODES.P)
    public DRSTaskAdapter(Context context, List<TaskDRS> taskDRSList, DataDRS dataDRS, ICancelOnClick iCancelOnClick,
                          IAdapterClickListener iAdapterClickListener) {
        this.context = context;
        this.taskDRSList = taskDRSList;
        this.dataDRS = dataDRS;
        this.iCancelOnClick = iCancelOnClick;
        this.iAdapterClickListener = iAdapterClickListener;
        this.callMaskInterface = callMaskInterface;
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        mRestServices1 = RestClient1.getRetrofitInstance().create(RestServices1.class);
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle("Please Wait!");
    }

    @NonNull
    @Override
    public DRSTaskAdapter.TaskAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.teasksitems, parent, false);
        return new DRSTaskAdapter.TaskAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DRSTaskAdapter.TaskAdapterViewHolder taskAdapterViewHolder, int i) {

        TaskDRS details = taskDRSList.get(i);

        taskAdapterViewHolder.textView.setText(details.getTaskId() + " (" + details.getPaytype().toString() + ")");

        String isTrial = details.getIs_trial();

        if (isTrial.equalsIgnoreCase("1")) {
            taskAdapterViewHolder.mTrialShipment.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load("https://www.ecritica.co/efreightlive/v2/images/trail-ship.png")
                    .resize(2200, 400)
                    .placeholder(R.drawable.brokenimage)
                    .into(taskAdapterViewHolder.mTrialShipment);
        } else {
            taskAdapterViewHolder.mTrialShipment.setImageBitmap(null);
        }

        taskAdapterViewHolder.mNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String completeAddress = taskDRSList.get(i).getNavigationAddress();
                try {
                    String uri = "http://maps.google.co.in/maps?daddr=" + completeAddress;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    context.startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(context, "Google Map Not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void callInitiate(String mobileNumber, String taskId) {
        mProgressDialog.setTitle("Please wait we are connecting the Call...Receive the call it will connect to end user");
        mProgressDialog.show();

        CallRequest callRequest = new CallRequest();
        callRequest.setAction("call_masking");
        callRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
        callRequest.setTo_number(mobileNumber);
        callRequest.setDrs(dataDRS.getDRSNo());
        callRequest.setDrs_no(dataDRS.getDRSNo());
        callRequest.setConnote_num(taskId);
        callRequest.setLatitude(mCriticalogSharedPreferences.getData("call_lat"));
        callRequest.setLongitute(mCriticalogSharedPreferences.getData("call_long"));

        //  progressDialog.show();
        Call<CallResponse> callResponseCall = mRestServices1.callInitiate(callRequest);
        callResponseCall.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if (response.body().getStatus() == 200) {
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mProgressDialog.dismiss();
                            manualMobileNumberDialog.dismiss();
                            Log.e("FAILURE_SUCCESS", response.body().getMessage());
                            //Do something after 100ms
                        }
                    }, 2000);
                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("FAILURE_CALL", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.e("FAILURE_CALL", t.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return taskDRSList.size();
    }

    public void codProofDilaog(String taskId, String mobileNum) {
        //Dialog Manual Entry

        EditText mMobileNumber;
        TextView mCall, mDirectCall;
        ImageView mCanceDialog;

        manualMobileNumberDialog = new Dialog(context);
        manualMobileNumberDialog.setContentView(R.layout.manual_entry_mobiel_dialoge);
        mMobileNumber = manualMobileNumberDialog.findViewById(R.id.mMobileNumber);
        mCall = manualMobileNumberDialog.findViewById(R.id.mCall);
        mDirectCall = manualMobileNumberDialog.findViewById(R.id.mDirectCall);
        mCanceDialog = manualMobileNumberDialog.findViewById(R.id.mCanceDialog);
        manualMobileNumberDialog.setCanceledOnTouchOutside(false);
        manualMobileNumberDialog.show();

        Window window = manualMobileNumberDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        mCanceDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualMobileNumberDialog.dismiss();
            }
        });
        mDirectCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobileNum.equalsIgnoreCase("")) {
                    Toasty.warning(context, "Enter Mobile number and call manually!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    callInitiate(mobileNum, taskId);
                }
            }
        });
        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNumber = mMobileNumber.getText().toString();
                if (mobileNumber.equalsIgnoreCase("")) {
                    Toasty.warning(context, R.string.enter_mobile_number, Toast.LENGTH_SHORT, true).show();
                } else if (mobileNumber.length() != 10) {
                    Toasty.warning(context, R.string.enter_mobile_number, Toast.LENGTH_SHORT, true).show();
                } else {
                    callInitiate(mobileNumber, taskId);
                }
            }
        });
    }

    public class TaskAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView micon_done, mDrsCancel, mTrialShipment;
        ImageButton mCall, mNavigation;

        public TaskAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.taskid);
            micon_done = (ImageView) itemView.findViewById(R.id.xicon_done);
            mDrsCancel = (ImageView) itemView.findViewById(R.id.mDrsCancel);
            mCall = itemView.findViewById(R.id.mCall);
            mNavigation = itemView.findViewById(R.id.mNavigation);
            mTrialShipment = itemView.findViewById(R.id.mTrialShipment);
            mCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mobileNumber = dataDRS.getPhone();
                    if (mobileNumber.equalsIgnoreCase("")) {
                        codProofDilaog(taskDRSList.get(getAdapterPosition()).getTaskId(), "");
                    } else {

                        codProofDilaog(taskDRSList.get(getAdapterPosition()).getTaskId(), mobileNumber);
                    }
                }
            });

            micon_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String otpFlag = mCriticalogSharedPreferences.getData("otp_flag_drs");
                    String otpVerified = mCriticalogSharedPreferences.getData("otp_flag_verified_drs");
                    mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                    if (otpFlag.equalsIgnoreCase("1")) {

                        if (otpVerified.equalsIgnoreCase("1")) {
                            RulesRequest rulesRequest = new RulesRequest();
                            rulesRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
                            rulesRequest.setAction("client_fll_rules");
                            rulesRequest.setClientId(dataDRS.getClientCode());
                            mProgressDialog.show();
                            Call<RulesResponse> rulesResponseCall = mRestServices.getClientRules(rulesRequest);

                            rulesResponseCall.enqueue(new Callback<RulesResponse>() {
                                @Override
                                public void onResponse(Call<RulesResponse> call, Response<RulesResponse> response) {
                                    mProgressDialog.dismiss();
                                    if (response.body().getStatus() == 200) {
                                        //Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                                        mCriticalogSharedPreferences.saveData("qrcode_upload_flag", response.body().getData().getQrCodImg());
                                        Intent done_intent = new Intent(context, DRSCloseActivity.class);

                                        Log.e("qrcode_upload_flag", response.body().getData().getQrCodImg());

                                        mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                                        mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                                        mCriticalogSharedPreferences.saveData("swap", dataDRS.getSwapped());
                                        mCriticalogSharedPreferences.saveData("pay_type", taskDRSList.get(getAdapterPosition()).getPaytype());
                                        mCriticalogSharedPreferences.saveData("receiver_name", dataDRS.getReciverNAME());
                                        mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());
                                        mCriticalogSharedPreferences.saveData("payment_status", taskDRSList.get(getAdapterPosition()).getPaymentStatus());
                                        mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                                        mCriticalogSharedPreferences.saveData("clientcode", dataDRS.getClientCode());
                                        mCriticalogSharedPreferences.saveIntData("ftc_layout", taskDRSList.get(getAdapterPosition()).getFtcLayout());
                                        mCriticalogSharedPreferences.saveData("pay_link", taskDRSList.get(getAdapterPosition()).getPayLink());
                                        mCriticalogSharedPreferences.saveData("qr_path", taskDRSList.get(getAdapterPosition()).getQrPath());
                                        mCriticalogSharedPreferences.saveData("new_cod_received", taskDRSList.get(getAdapterPosition()).getNew_cod_received());
                                        mCriticalogSharedPreferences.saveData("reprint_con", taskDRSList.get(getAdapterPosition()).getReprint_status());
                                        mCriticalogSharedPreferences.saveData("max_cheque_num", taskDRSList.get(getAdapterPosition()).getCheque_cod_allowed());
                                        mCriticalogSharedPreferences.saveData("force_reprint_flag", taskDRSList.get(getAdapterPosition()).getForce_reprint_upload());

                                        mCriticalogSharedPreferences.saveData("gate_pass_flag_drs", taskDRSList.get(getAdapterPosition()).getGatepass());

                                        mCriticalogSharedPreferences.saveData("force_drs_checklist_otp", dataDRS.getForce_drs_checklist_otp());



                                        context.startActivity(done_intent);
                                    } else {
                                        mCriticalogSharedPreferences.saveData("qrcode_upload_flag", "");
                                        Intent done_intent = new Intent(context, DRSCloseActivity.class);

                                        mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                                        mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                                        mCriticalogSharedPreferences.saveData("swap", dataDRS.getSwapped());
                                        mCriticalogSharedPreferences.saveData("pay_type", taskDRSList.get(getAdapterPosition()).getPaytype());
                                        mCriticalogSharedPreferences.saveData("receiver_name", dataDRS.getReciverNAME());
                                        mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());
                                        mCriticalogSharedPreferences.saveData("payment_status", taskDRSList.get(getAdapterPosition()).getPaymentStatus());
                                        mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                                        mCriticalogSharedPreferences.saveData("clientcode", dataDRS.getClientCode());
                                        mCriticalogSharedPreferences.saveIntData("ftc_layout", taskDRSList.get(getAdapterPosition()).getFtcLayout());
                                        mCriticalogSharedPreferences.saveData("pay_link", taskDRSList.get(getAdapterPosition()).getPayLink());

                                        mCriticalogSharedPreferences.saveData("qr_path", taskDRSList.get(getAdapterPosition()).getQrPath());
                                        mCriticalogSharedPreferences.saveData("new_cod_received", taskDRSList.get(getAdapterPosition()).getNew_cod_received());
                                        mCriticalogSharedPreferences.saveData("reprint_con", taskDRSList.get(getAdapterPosition()).getReprint_status());
                                        mCriticalogSharedPreferences.saveData("max_cheque_num", taskDRSList.get(getAdapterPosition()).getCheque_cod_allowed());
                                        mCriticalogSharedPreferences.saveData("force_reprint_flag", taskDRSList.get(getAdapterPosition()).getForce_reprint_upload());

                                        mCriticalogSharedPreferences.saveData("gate_pass_flag_drs", taskDRSList.get(getAdapterPosition()).getGatepass());
                                        mCriticalogSharedPreferences.saveData("force_drs_checklist_otp", dataDRS.getForce_drs_checklist_otp());
                                        context.startActivity(done_intent);
                                    }
                                }

                                @Override
                                public void onFailure(Call<RulesResponse> call, Throwable t) {
                                    mProgressDialog.dismiss();
                                    mCriticalogSharedPreferences.saveData("qrcode_upload_flag", "");

                                    Intent done_intent = new Intent(context, DRSCloseActivity.class);

                                    mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                                    mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                                    mCriticalogSharedPreferences.saveData("swap", dataDRS.getSwapped());
                                    mCriticalogSharedPreferences.saveData("pay_type", taskDRSList.get(getAdapterPosition()).getPaytype());
                                    mCriticalogSharedPreferences.saveData("receiver_name", dataDRS.getReciverNAME());
                                    mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());
                                    mCriticalogSharedPreferences.saveData("payment_status", taskDRSList.get(getAdapterPosition()).getPaymentStatus());
                                    mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                                    mCriticalogSharedPreferences.saveData("clientcode", dataDRS.getClientCode());
                                    mCriticalogSharedPreferences.saveIntData("ftc_layout", taskDRSList.get(getAdapterPosition()).getFtcLayout());
                                    mCriticalogSharedPreferences.saveData("pay_link", taskDRSList.get(getAdapterPosition()).getPayLink());

                                    mCriticalogSharedPreferences.saveData("qr_path", taskDRSList.get(getAdapterPosition()).getQrPath());
                                    mCriticalogSharedPreferences.saveData("new_cod_received", taskDRSList.get(getAdapterPosition()).getNew_cod_received());
                                    mCriticalogSharedPreferences.saveData("reprint_con", taskDRSList.get(getAdapterPosition()).getReprint_status());
                                    mCriticalogSharedPreferences.saveData("max_cheque_num", taskDRSList.get(getAdapterPosition()).getCheque_cod_allowed());
                                    mCriticalogSharedPreferences.saveData("force_reprint_flag", taskDRSList.get(getAdapterPosition()).getForce_reprint_upload());

                                    mCriticalogSharedPreferences.saveData("gate_pass_flag_drs", taskDRSList.get(getAdapterPosition()).getGatepass());
                                    mCriticalogSharedPreferences.saveData("force_drs_checklist_otp", dataDRS.getForce_drs_checklist_otp());

                                    context.startActivity(done_intent);
                                }
                            });
                        } else {
                            OTPRequest otpRequest = new OTPRequest();
                            otpRequest.setOtp("");
                            otpRequest.setAction("validate_otp");
                            otpRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code_otp"));
                            otpRequest.setRound(mCriticalogSharedPreferences.getData("round_otp"));
                            otpRequest.setOtpMobile(mCriticalogSharedPreferences.getData("get_phone_otp"));
                            otpRequest.setConsignee(mCriticalogSharedPreferences.getData("get_consinee_otp"));
                            otpRequest.setIsResend("2");
                            otpRequest.setRefcount(mCriticalogSharedPreferences.getData("drs_no_otp"));
                            otpRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
                            otpRequest.setDocket_no(mCriticalogSharedPreferences.getData("task_id"));
                            mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());

                            otpRequest.setFor_type("drs");
                            mProgressDialog.show();
                            Call<OTPResponse> otpResponseCall = mRestServices.otpRequestVerify(otpRequest);
                            otpResponseCall.enqueue(new Callback<OTPResponse>() {
                                @Override
                                public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                                    mProgressDialog.dismiss();
                                    if (response.body().getStatus() == 200) {
                                        Intent intent = new Intent(context, ReadOtpActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("userid", mCriticalogSharedPreferences.getData("userId"));
                                        intent.putExtra("refcount", mCriticalogSharedPreferences.getData("drs_no_otp"));
                                        intent.putExtra("clientcode", mCriticalogSharedPreferences.getData("client_code_otp"));
                                        intent.putExtra("round", mCriticalogSharedPreferences.getData("round_otp"));
                                        intent.putExtra("consignee", mCriticalogSharedPreferences.getData("get_consinee_otp"));
                                        intent.putExtra("phone", mCriticalogSharedPreferences.getData("get_phone_otp"));

                                        context.startActivity(intent);
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<OTPResponse> call, Throwable t) {
                                    Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                    } else {
                        RulesRequest rulesRequest = new RulesRequest();
                        rulesRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
                        rulesRequest.setAction("client_fll_rules");
                        rulesRequest.setClientId(dataDRS.getClientCode());
                        mProgressDialog.show();
                        Call<RulesResponse> rulesResponseCall = mRestServices.getClientRules(rulesRequest);

                        rulesResponseCall.enqueue(new Callback<RulesResponse>() {
                            @Override
                            public void onResponse(Call<RulesResponse> call, Response<RulesResponse> response) {
                                mProgressDialog.dismiss();
                                if (response.body().getStatus() == 200) {
                                    mCriticalogSharedPreferences.saveData("qrcode_upload_flag", response.body().getData().getQrCodImg());
                                    Intent done_intent = new Intent(context, DRSCloseActivity.class);

                                    mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                                    mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                                    mCriticalogSharedPreferences.saveData("swap", dataDRS.getSwapped());
                                    mCriticalogSharedPreferences.saveData("pay_type", taskDRSList.get(getAdapterPosition()).getPaytype());
                                    mCriticalogSharedPreferences.saveData("receiver_name", dataDRS.getReciverNAME());
                                    mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());
                                    mCriticalogSharedPreferences.saveData("payment_status", taskDRSList.get(getAdapterPosition()).getPaymentStatus());
                                    mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                                    mCriticalogSharedPreferences.saveData("clientcode", dataDRS.getClientCode());
                                    mCriticalogSharedPreferences.saveIntData("ftc_layout", taskDRSList.get(getAdapterPosition()).getFtcLayout());
                                    mCriticalogSharedPreferences.saveData("pay_link", taskDRSList.get(getAdapterPosition()).getPayLink());
                                    mCriticalogSharedPreferences.saveData("new_cod_received", taskDRSList.get(getAdapterPosition()).getNew_cod_received());
                                    mCriticalogSharedPreferences.saveData("qr_path", taskDRSList.get(getAdapterPosition()).getQrPath());
                                    mCriticalogSharedPreferences.saveData("reprint_con", taskDRSList.get(getAdapterPosition()).getReprint_status());
                                    mCriticalogSharedPreferences.saveData("max_cheque_num", taskDRSList.get(getAdapterPosition()).getCheque_cod_allowed());
                                    mCriticalogSharedPreferences.saveData("force_reprint_flag", taskDRSList.get(getAdapterPosition()).getForce_reprint_upload());
                                    mCriticalogSharedPreferences.saveData("gate_pass_flag_drs", taskDRSList.get(getAdapterPosition()).getGatepass());
                                    mCriticalogSharedPreferences.saveData("force_drs_checklist_otp", dataDRS.getForce_drs_checklist_otp());
                                    context.startActivity(done_intent);
                                } else {
                                    mCriticalogSharedPreferences.saveData("qrcode_upload_flag", "");
                                    Intent done_intent = new Intent(context, DRSCloseActivity.class);

                                    mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                                    mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                                    mCriticalogSharedPreferences.saveData("swap", dataDRS.getSwapped());
                                    mCriticalogSharedPreferences.saveData("pay_type", taskDRSList.get(getAdapterPosition()).getPaytype());
                                    mCriticalogSharedPreferences.saveData("receiver_name", dataDRS.getReciverNAME());
                                    mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());
                                    mCriticalogSharedPreferences.saveData("payment_status", taskDRSList.get(getAdapterPosition()).getPaymentStatus());
                                    mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                                    mCriticalogSharedPreferences.saveData("clientcode", dataDRS.getClientCode());
                                    mCriticalogSharedPreferences.saveIntData("ftc_layout", taskDRSList.get(getAdapterPosition()).getFtcLayout());
                                    mCriticalogSharedPreferences.saveData("pay_link", taskDRSList.get(getAdapterPosition()).getPayLink());
                                    mCriticalogSharedPreferences.saveData("new_cod_received", taskDRSList.get(getAdapterPosition()).getNew_cod_received());
                                    mCriticalogSharedPreferences.saveData("qr_path", taskDRSList.get(getAdapterPosition()).getQrPath());
                                    mCriticalogSharedPreferences.saveData("reprint_con", taskDRSList.get(getAdapterPosition()).getReprint_status());
                                    mCriticalogSharedPreferences.saveData("max_cheque_num", taskDRSList.get(getAdapterPosition()).getCheque_cod_allowed());
                                    mCriticalogSharedPreferences.saveData("force_reprint_flag", taskDRSList.get(getAdapterPosition()).getForce_reprint_upload());
                                    mCriticalogSharedPreferences.saveData("gate_pass_flag_drs", taskDRSList.get(getAdapterPosition()).getGatepass());
                                    mCriticalogSharedPreferences.saveData("force_drs_checklist_otp", dataDRS.getForce_drs_checklist_otp());
                                    context.startActivity(done_intent);
                                }
                            }

                            @Override
                            public void onFailure(Call<RulesResponse> call, Throwable t) {
                                mProgressDialog.dismiss();
                                mCriticalogSharedPreferences.saveData("qrcode_upload_flag", "");

                                Intent done_intent = new Intent(context, DRSCloseActivity.class);

                                mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                                mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                                mCriticalogSharedPreferences.saveData("swap", dataDRS.getSwapped());
                                mCriticalogSharedPreferences.saveData("pay_type", taskDRSList.get(getAdapterPosition()).getPaytype());
                                mCriticalogSharedPreferences.saveData("receiver_name", dataDRS.getReciverNAME());
                                mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());
                                mCriticalogSharedPreferences.saveData("payment_status", taskDRSList.get(getAdapterPosition()).getPaymentStatus());
                                mCriticalogSharedPreferences.saveData("payment_amount", taskDRSList.get(getAdapterPosition()).getPayValue());
                                mCriticalogSharedPreferences.saveData("clientcode", dataDRS.getClientCode());
                                mCriticalogSharedPreferences.saveIntData("ftc_layout", taskDRSList.get(getAdapterPosition()).getFtcLayout());
                                mCriticalogSharedPreferences.saveData("pay_link", taskDRSList.get(getAdapterPosition()).getPayLink());
                                mCriticalogSharedPreferences.saveData("new_cod_received", taskDRSList.get(getAdapterPosition()).getNew_cod_received());
                                mCriticalogSharedPreferences.saveData("qr_path", taskDRSList.get(getAdapterPosition()).getQrPath());
                                mCriticalogSharedPreferences.saveData("reprint_con", taskDRSList.get(getAdapterPosition()).getReprint_status());
                                mCriticalogSharedPreferences.saveData("max_cheque_num", taskDRSList.get(getAdapterPosition()).getCheque_cod_allowed());
                                mCriticalogSharedPreferences.saveData("force_reprint_flag", taskDRSList.get(getAdapterPosition()).getForce_reprint_upload());
                                mCriticalogSharedPreferences.saveData("gate_pass_flag_drs", taskDRSList.get(getAdapterPosition()).getGatepass());
                                mCriticalogSharedPreferences.saveData("force_drs_checklist_otp", dataDRS.getForce_drs_checklist_otp());
                                context.startActivity(done_intent);
                            }
                        });
                    }

                }
            });

            mDrsCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mCriticalogSharedPreferences.saveData("drs_number", dataDRS.getDRSNo());
                    mCriticalogSharedPreferences.saveData("task_id", taskDRSList.get(getAdapterPosition()).getTaskId());
                    mCriticalogSharedPreferences.saveData("bulk_flag", dataDRS.getBulk());

                    iCancelOnClick.drsCancelClick(dataDRS.getClientName(), dataDRS.getPhone(), dataDRS.getAddress1(), dataDRS.getAddress2(), dataDRS.getCityId(),
                            dataDRS.getState(), dataDRS.getPincode(), dataDRS.getDRSNo(), dataDRS.getBulk(), dataDRS.getSwapped(),
                            taskDRSList.get(getAdapterPosition()).getTaskId(), taskDRSList.get(getAdapterPosition()).getPaytype());
                }
            });

        }
    }
}
