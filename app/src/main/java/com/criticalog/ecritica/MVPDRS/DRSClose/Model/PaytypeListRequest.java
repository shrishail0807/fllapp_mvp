package com.criticalog.ecritica.MVPDRS.DRSClose.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaytypeListRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("client_code")
    @Expose
    private String clientCode;

    public String getDocket_no() {
        return docket_no;
    }

    public void setDocket_no(String docket_no) {
        this.docket_no = docket_no;
    }

    @SerializedName("docket_no")
    @Expose
    private String docket_no;



    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

}
