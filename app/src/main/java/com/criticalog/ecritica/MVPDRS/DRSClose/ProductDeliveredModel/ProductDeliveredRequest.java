package com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel;

import com.criticalog.ecritica.MVPBarcodeScan.model.ChecklistResponse;
import com.criticalog.ecritica.ModelClasses.OpenImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductDeliveredRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("task_id")
    @Expose
    private String taskId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("bulk_flag")
    @Expose
    private String bulkFlag;
    @SerializedName("refcount")
    @Expose
    private String refcount;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("swap")
    @Expose
    private String swap;
    @SerializedName("cod_taskid")
    @Expose
    private String codTaskid;
    @SerializedName("chequeno")
    @Expose
    private String chequeno;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("cod_amount")
    @Expose
    private String codAmount;
    @SerializedName("cod_paytype")
    @Expose
    private String codPaytype;
    @SerializedName("modeofpay")
    @Expose
    private String modeofpay;
    @SerializedName("upirefno")
    @Expose
    private String upirefno;
    @SerializedName("image_1")
    @Expose
    private String image1;
    @SerializedName("image_2")
    @Expose
    private String image2;
    @SerializedName("image_pod")
    @Expose
    private String imagePod;
    @SerializedName("reciever_name")
    @Expose
    private String recieverName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("qrimage")
    @Expose
    private String qrimage;

    @SerializedName("signeddc_1")
    @Expose
    private String signeddc_1;

    @SerializedName("signeddc_2")
    @Expose
    private String signeddc_2;

    @SerializedName("custom_option1_1")
    @Expose
    private String custom_option1_1;

    @SerializedName("custom_option1_2")
    @Expose
    private String custom_option1_2;

    @SerializedName("force_image_capture")
    @Expose
    private String force_image_capture;

    @SerializedName("reason_cit_reprint")
    @Expose
    private String reason_cit_reprint;

    @SerializedName("open_images")
    @Expose
    private List<OpenImage> openImageList = null;

    public List<OpenImage> getOpenImageList() {
        return openImageList;
    }

    public void setOpenImageList(List<OpenImage> openImageList) {
        this.openImageList = openImageList;
    }

    public String getReason_cit_reprint() {
        return reason_cit_reprint;
    }

    public void setReason_cit_reprint(String reason_cit_reprint) {
        this.reason_cit_reprint = reason_cit_reprint;
    }

    public String getForce_image_capture() {
        return force_image_capture;
    }

    public void setForce_image_capture(String force_image_capture) {
        this.force_image_capture = force_image_capture;
    }

    public String getCustom_option1_1() {
        return custom_option1_1;
    }

    public void setCustom_option1_1(String custom_option1_1) {
        this.custom_option1_1 = custom_option1_1;
    }

    public String getCustom_option1_2() {
        return custom_option1_2;
    }

    public void setCustom_option1_2(String custom_option1_2) {
        this.custom_option1_2 = custom_option1_2;
    }

    public String getSigneddc_1() {
        return signeddc_1;
    }

    public void setSigneddc_1(String signeddc_1) {
        this.signeddc_1 = signeddc_1;
    }

    public String getSigneddc_2() {
        return signeddc_2;
    }

    public void setSigneddc_2(String signeddc_2) {
        this.signeddc_2 = signeddc_2;
    }

    public String getChecklist_1() {
        return checklist_1;
    }

    public void setChecklist_1(String checklist_1) {
        this.checklist_1 = checklist_1;
    }

    public String getChecklist_2() {
        return checklist_2;
    }

    public void setChecklist_2(String checklist_2) {
        this.checklist_2 = checklist_2;
    }

    @SerializedName("checklist_1")
    @Expose
    private String checklist_1;

    @SerializedName("checklist_2")
    @Expose
    private String checklist_2;

    @SerializedName("qrFlag")
    @Expose
    private Integer qrFlag;

    @SerializedName("media")
    @Expose
    private String media;

    @SerializedName("checklist_response")
    @Expose
    private List<ChecklistResponse> checklistResponse = null;
    @SerializedName("remark")
    @Expose
    private String remark;

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public List<ChecklistResponse> getChecklistResponse() {
        return checklistResponse;
    }

    public void setChecklistResponse(List<ChecklistResponse> checklistResponse) {
        this.checklistResponse = checklistResponse;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getQrimage() {
        return qrimage;
    }

    public void setQrimage(String qrimage) {
        this.qrimage = qrimage;
    }

    public Integer getQrFlag() {
        return qrFlag;
    }

    public void setQrFlag(Integer qrFlag) {
        this.qrFlag = qrFlag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBulkFlag() {
        return bulkFlag;
    }

    public void setBulkFlag(String bulkFlag) {
        this.bulkFlag = bulkFlag;
    }

    public String getRefcount() {
        return refcount;
    }

    public void setRefcount(String refcount) {
        this.refcount = refcount;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSwap() {
        return swap;
    }

    public void setSwap(String swap) {
        this.swap = swap;
    }

    public String getCodTaskid() {
        return codTaskid;
    }

    public void setCodTaskid(String codTaskid) {
        this.codTaskid = codTaskid;
    }

    public String getChequeno() {
        return chequeno;
    }

    public void setChequeno(String chequeno) {
        this.chequeno = chequeno;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCodAmount() {
        return codAmount;
    }

    public void setCodAmount(String codAmount) {
        this.codAmount = codAmount;
    }

    public String getCodPaytype() {
        return codPaytype;
    }

    public void setCodPaytype(String codPaytype) {
        this.codPaytype = codPaytype;
    }

    public String getModeofpay() {
        return modeofpay;
    }

    public void setModeofpay(String modeofpay) {
        this.modeofpay = modeofpay;
    }

    public String getUpirefno() {
        return upirefno;
    }

    public void setUpirefno(String upirefno) {
        this.upirefno = upirefno;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImagePod() {
        return imagePod;
    }

    public void setImagePod(String imagePod) {
        this.imagePod = imagePod;
    }

    public String getRecieverName() {
        return recieverName;
    }

    public void setRecieverName(String recieverName) {
        this.recieverName = recieverName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
