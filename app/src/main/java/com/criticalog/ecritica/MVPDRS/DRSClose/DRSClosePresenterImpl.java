package com.criticalog.ecritica.MVPDRS.DRSClose;

import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PayTypeListResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredResponse;

public class DRSClosePresenterImpl implements DRSCloseContract.presenter, DRSCloseContract.GetUPIRefreshIntractor.OnFinishedListener, DRSCloseContract.GetProductDeliveredIntractor.OnFinishedListener, DRSCloseContract.GetPayTypeListIntractor.OnFinishedListener {

    private DRSCloseContract.MainView mainView;
    private DRSCloseContract.GetUPIRefreshIntractor getUPIRefreshIntractor;
    private DRSCloseContract.GetProductDeliveredIntractor getProductDeliveredIntractor;
    private DRSCloseContract.GetPayTypeListIntractor getPayTypeListIntractor;

    public DRSClosePresenterImpl(DRSCloseContract.MainView mainView, DRSCloseContract.GetUPIRefreshIntractor getUPIRefreshIntractor,
                                 DRSCloseContract.GetProductDeliveredIntractor getProductDeliveredIntractor,
                                 DRSCloseContract.GetPayTypeListIntractor getPayTypeListIntractor) {
        this.mainView = mainView;
        this.getUPIRefreshIntractor = getUPIRefreshIntractor;
        this.getProductDeliveredIntractor = getProductDeliveredIntractor;
        this.getPayTypeListIntractor=getPayTypeListIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void onRefreshUPI(String token, RefreshUPIRequest refreshUPIRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getUPIRefreshIntractor.refreshUpiSuccessful(this, token, refreshUPIRequest);
    }

    @Override
    public void productDeliveredCall(String token, ProductDeliveredRequest productDeliveredRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getProductDeliveredIntractor.productDeliveredRequest(this, token, productDeliveredRequest);
    }

    @Override
    public void getPayTypeList(PaytypeListRequest paytypeListRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getPayTypeListIntractor.payTypeListSuccessful(this,paytypeListRequest);
    }

    @Override
    public void onFinished(RefreshUPIResponse refreshUPIResponse) {
        if (mainView != null) {
            mainView.setDataUpiViews(refreshUPIResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(ProductDeliveredResponse productDeliveredResponse) {
        if (mainView != null) {
            mainView.productDeliveredResponseToViews(productDeliveredResponse);
            mainView.hideProgress();
        }
    }


    @Override
    public void onFinished(PayTypeListResponse payTypeListResponse) {
        if(mainView!=null)
        {
            mainView.payTypeListResponse(payTypeListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
