package com.criticalog.ecritica.MVPDRS.DRSModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaskDRS {
    @SerializedName("task_id")
    @Expose
    private String taskId;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("codtask_id")
    @Expose
    private Integer codtaskId;
    @SerializedName("paytype")
    @Expose
    private String paytype;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("pay_value")
    @Expose
    private String payValue;
    @SerializedName("ftc_layout")
    @Expose
    private Integer ftcLayout;
    @SerializedName("pay_link")
    @Expose
    private String payLink;
    @SerializedName("qr_path")
    @Expose
    private String qrPath;

    @SerializedName("is_trial")
    @Expose
    private String is_trial;

    @SerializedName("new_cod_received")
    @Expose
    private String new_cod_received;

    @SerializedName("reprint_status")
    @Expose
    private String reprint_status;
    @SerializedName("cheque_cod_allowed")
    @Expose
    private String cheque_cod_allowed;
    @SerializedName("force_reprint_upload")
    @Expose
    private String force_reprint_upload;

    @SerializedName("navigationAddress")
    @Expose
    private String navigationAddress;

    public String getGatepass() {
        return gatepass;
    }

    public void setGatepass(String gatepass) {
        this.gatepass = gatepass;
    }

    @SerializedName("gatepass")
    @Expose
    private String gatepass;


    public String getNavigationAddress() {
        return navigationAddress;
    }

    public void setNavigationAddress(String navigationAddress) {
        this.navigationAddress = navigationAddress;
    }

    public String getReprint_status() {
        return reprint_status;
    }

    public void setReprint_status(String reprint_status) {
        this.reprint_status = reprint_status;
    }

    public String getCheque_cod_allowed() {
        return cheque_cod_allowed;
    }

    public void setCheque_cod_allowed(String cheque_cod_allowed) {
        this.cheque_cod_allowed = cheque_cod_allowed;
    }

    public String getForce_reprint_upload() {
        return force_reprint_upload;
    }

    public void setForce_reprint_upload(String force_reprint_upload) {
        this.force_reprint_upload = force_reprint_upload;
    }

    public String getNew_cod_received() {
        return new_cod_received;
    }

    public void setNew_cod_received(String new_cod_received) {
        this.new_cod_received = new_cod_received;
    }

    public String getIs_trial() {
        return is_trial;
    }

    public void setIs_trial(String is_trial) {
        this.is_trial = is_trial;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getCodtaskId() {
        return codtaskId;
    }

    public void setCodtaskId(Integer codtaskId) {
        this.codtaskId = codtaskId;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPayValue() {
        return payValue;
    }

    public void setPayValue(String payValue) {
        this.payValue = payValue;
    }

    public Integer getFtcLayout() {
        return ftcLayout;
    }

    public void setFtcLayout(Integer ftcLayout) {
        this.ftcLayout = ftcLayout;
    }

    public String getPayLink() {
        return payLink;
    }

    public void setPayLink(String payLink) {
        this.payLink = payLink;
    }

    public String getQrPath() {
        return qrPath;
    }

    public void setQrPath(String qrPath) {
        this.qrPath = qrPath;
    }
}
