package com.criticalog.ecritica.MVPDRS.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.PrsRule;
import com.criticalog.ecritica.model.PrsRulesQc;

import java.util.List;

public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.ViewHolder>{

    private List<PrsRulesQc> userDetailsBulks;

    public RulesAdapter(List<PrsRulesQc> userDetailsBulks) {
        this.userDetailsBulks = userDetailsBulks;
    }

    @Override
    public RulesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row, parent, false);
        return new RulesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RulesAdapter.ViewHolder holder, int position) {

        PrsRulesQc tasklist= userDetailsBulks.get(position);

        holder.xTvTaskId.setText(String.valueOf(position+1)+") "+tasklist.getRule());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView xTvTaskId;

        public ViewHolder(View itemView) {
            super(itemView);

            xTvTaskId = itemView.findViewById(R.id.xTvTaskId);
        }
    }

    @Override
    public int getItemCount() {
        return userDetailsBulks.size();
    }
}
/*

package com.criticalog.ecritica.MVPDRS.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.PrsRule;

import java.util.List;

public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.ViewHolder>{

    private List<PrsRule> userDetailsBulks;

    public RulesAdapter(List<PrsRule> userDetailsBulks) {
        this.userDetailsBulks = userDetailsBulks;
    }

    @Override
    public RulesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row, parent, false);
        return new RulesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RulesAdapter.ViewHolder holder, int position) {

        PrsRule tasklist= userDetailsBulks.get(position);

        holder.xTvTaskId.setText(String.valueOf(position+1)+") "+tasklist.getRule());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView xTvTaskId;

        public ViewHolder(View itemView) {
            super(itemView);

            xTvTaskId = itemView.findViewById(R.id.xTvTaskId);
        }
    }

    @Override
    public int getItemCount() {
        return userDetailsBulks.size();
    }
}*/
