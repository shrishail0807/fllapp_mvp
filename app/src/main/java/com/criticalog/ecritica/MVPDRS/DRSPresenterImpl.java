package com.criticalog.ecritica.MVPDRS;

import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseContract;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPPickup.PickupContract;

public class DRSPresenterImpl implements DRSListContract.presenter, DRSListContract.GetDRSIntractor.OnFinishedListener, DRSListContract.GetDRSCancelIntractor.OnFinishedListener, DRSCloseContract.presenter {
    private DRSListContract.MainView mainView;
    private DRSListContract.GetDRSIntractor getDRSIntractor;
    private DRSListContract.GetDRSCancelIntractor getDRSCancelIntractor;

    public DRSPresenterImpl(DRSListContract.MainView mainView, DRSListContract.GetDRSIntractor getDRSIntractor, DRSListContract.GetDRSCancelIntractor getDRSCancelIntractor) {
        this.mainView = mainView;
        this.getDRSIntractor = getDRSIntractor;
        this.getDRSCancelIntractor=getDRSCancelIntractor;
    }
    @Override
    public void onDestroy() {

    }

    @Override
    public void onRefreshUPI(String token, RefreshUPIRequest refreshUPIRequest) {

    }

    @Override
    public void productDeliveredCall(String token, ProductDeliveredRequest productDeliveredRequest) {

    }

    @Override
    public void getPayTypeList(PaytypeListRequest paytypeListRequest) {

    }

    @Override
    public void onLoadDRSList(String token, DRSListRequest drsListRequest) {
        if(mainView != null){
            mainView.showProgress();
        }
        getDRSIntractor.drsListSuccessful(this,token,drsListRequest);
    }

    @Override
    public void drsCancel(String token, DrsCancelRequest drsCancelRequest) {
        if(mainView != null){
            mainView.showProgress();
        }
        getDRSCancelIntractor.drsCancelSuccess(this,token,drsCancelRequest);
    }

    @Override
    public void onFinished(DRSListResponse drsListResponse) {
        if(mainView != null){
            mainView.setDataToViews(drsListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(DrsCancelResponse drsCancelResponse) {
        if(mainView != null){
            mainView.drsCancelCallSuceess(drsCancelResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
