package com.criticalog.ecritica.MVPDRS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Interface.CallMaskInterface;
import com.criticalog.ecritica.MVPDRS.Adapter.DRSAdapter;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.MVPDRS.DRSModel.DataDRS;
import com.criticalog.ecritica.MVPDRS.Interfaces.ICancelOnClick;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.MVPPickup.PickupCancel.DatumReasons;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupReasonsResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.CallMaskingStatusRequest;
import com.criticalog.ecritica.model.CallMaskingStatusResponse;
import com.criticalog.ecritica.model.NDCOtpRequest;
import com.criticalog.ecritica.model.NDCOtpResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DRSActivity extends AppCompatActivity implements DRSListContract.MainView, SearchView.OnQueryTextListener, ICancelOnClick, CallMaskInterface {
    DRSListContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String token, userId;
    private SimpleArcDialog mProgressBar;
    private RecyclerView mRvDrs;
    private ImageView mTvBackbutton;
    DRSAdapter drsAdapter;
    List<DataDRS> listOnGoingList = new ArrayList<>();
    private RestServices mRestServices;
    private List<DatumReasons> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    private int year, month, day;
    int mHour;
    int mMinute;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double latitude = 0.0, longitude = 0;
    private String reasonCode = "", refno, bulkFlag;
    private DatabaseHelper dbHelper;
    private boolean statusfromdb;
    private double start_lat, start_long;
    private double wayLatitude = 0, wayLongitude = 0;
    private SearchView searchView;
    ArrayList<String> filterStringList = new ArrayList<>();
    private static final int PERMISSION_REQUEST_CODE = 200;
    private String convertedstring = "";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView mUserid;
    String androidOS;
    String versionName;
    private Dialog dialogCallAlert, manualMobileNumberDialog;
    Location gps_loc;
    Location network_loc;
    Location final_loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drs_mvp);

        androidOS = Build.VERSION.RELEASE;
        versionName = BuildConfig.VERSION_NAME;

        StaticUtils.APP_VERSION = BuildConfig.VERSION_NAME;

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        boolean per = checkPermission();

        checkGpsEnabledOrNot();

        mRvDrs = findViewById(R.id.mRvDrs);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        searchView = findViewById(R.id.searchView);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mUserid = findViewById(R.id.mUserid);
        searchView.setOnQueryTextListener(this);


        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);

        token = mCriticalogSharedPreferences.getData("token");
        userId = mCriticalogSharedPreferences.getData("userId");

        mUserid.setText(userId);
        mCriticalogSharedPreferences.saveData("R_DATE", "");
        mCriticalogSharedPreferences.saveData("R_TIME", "");
        mCriticalogSharedPreferences.saveData("from_qr_payment", "");
        mCriticalogSharedPreferences.saveData("checklist_done", "");
        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        StaticUtils.distance_api_url = mCriticalogSharedPreferences.getData("distance_api_url");

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        getLocation();

        DRSListRequest drsListRequest = new DRSListRequest();
        drsListRequest.setUserId(userId);
        drsListRequest.setAction("drslist");
        drsListRequest.setLatitude(String.valueOf(latitude));
        drsListRequest.setLongitude(String.valueOf(longitude));
        drsListRequest.setApp_ver_name(versionName);

        mPresenter = new DRSPresenterImpl(this, new GetDrsInteractorImpl(), new GetDrsInteractorImpl());
        mPresenter.onLoadDRSList(token, drsListRequest);


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DRSListRequest drsListRequest = new DRSListRequest();
                drsListRequest.setUserId(userId);
                drsListRequest.setAction("drslist");
                drsListRequest.setLatitude(String.valueOf(latitude));
                drsListRequest.setLongitude(String.valueOf(longitude));

                mPresenter = new DRSPresenterImpl(DRSActivity.this, new GetDrsInteractorImpl(), new GetDrsInteractorImpl());
                mPresenter.onLoadDRSList(token, drsListRequest);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {

            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }

    public void getLoactionFromGps() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            gps_loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            network_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (gps_loc != null) {
            final_loc = gps_loc;
            latitude = final_loc.getLatitude();
            longitude = final_loc.getLongitude();
            mCriticalogSharedPreferences.saveData("call_lat", String.valueOf(latitude));
            mCriticalogSharedPreferences.saveData("call_long", String.valueOf(longitude));
        } else if (network_loc != null) {
            final_loc = network_loc;
            latitude = final_loc.getLatitude();
            longitude = final_loc.getLongitude();
            mCriticalogSharedPreferences.saveData("call_lat", String.valueOf(latitude));
            mCriticalogSharedPreferences.saveData("call_long", String.valueOf(longitude));
        } else {
            latitude = 0.0;
            longitude = 0.0;
        }
    }

    private void getLocationRiderLog() {
        if (ActivityCompat.checkSelfPermission(DRSActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DRSActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DRSActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(DRSActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getLocation();
        getLocationRiderLog();

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }


    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults.length > 0 && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    showLocationDialog("");

                }

                break;
        }
    }

    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finish();
                        // redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica");


                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        requestPermission();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();

    }

    @Override
    public void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.show();
        }

    }

    @Override
    public void hideProgress() {
        try {
            if (mProgressBar != null) {
                mProgressBar.dismiss();
            }
        } catch (Exception e) {

        }


    }

    @Override
    public void setDataToViews(DRSListResponse drsListResponse) {
        Toasty.warning(this, drsListResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        if (drsListResponse.getStatus() == 200) {
            listOnGoingList = drsListResponse.getData();

            for (int i = 0; i < drsListResponse.getData().size(); i++) {
                filterStringList.add(drsListResponse.getData().get(i).getClientCode());
            }

            drsAdapter = new DRSAdapter(this, drsListResponse.getData(), filterStringList, DRSActivity.this, DRSActivity.this);
            mRvDrs.setLayoutManager(new LinearLayoutManager(this));
            mRvDrs.setAdapter(drsAdapter);
        }
    }

    public String getDistanceFromGoogleAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        String distnace = "0";

        float distanceInKm = 0;
        String nextBillionHitStatus = mCriticalogSharedPreferences.getData("distance_api_status");

        if (nextBillionHitStatus.equalsIgnoreCase("1")) {

            distnace = mCriticalogSharedPreferences.getDistanceFromGoogelBillionAPI(lat1, lon1, lat2, lon2);

            distanceInKm = convertMeterToKilometer(Float.valueOf(distnace));
        } else {
            distanceInKm = 0;
        }

        return String.valueOf(distanceInKm);

    }

    public static float convertMeterToKilometer(float meter) {
        return (float) (meter * 0.001);
    }

    @Override
    public void drsCancelCallSuceess(DrsCancelResponse drsCancelResponse) {
        getLocationRiderLog();

        if (drsCancelResponse.getStatus() == 200) {

            Toasty.success(this, drsCancelResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            ArrayList<PodClaimPojo> latlonglist;


            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(c);

            SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
            String time = df1.format(c);

            latlonglist = dbHelper.getAllLatLongList();


            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (latlonglist.size() > 0) {
                        PodClaimPojo podClaimPojo;
                        int listsize = latlonglist.size();
                        if (latlonglist.get(listsize - 1).getEdn_lat() == 0 || latlonglist.get(listsize - 1).getEnd_long() == 0 || wayLatitude == 0 || wayLongitude == 0) {
                            double distnce = distanceCalc(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);

                            String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                convertedstring_g = String.valueOf(distnce);
                            }
                            podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, String.valueOf(distnce), convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("task_id"), "drs cancel", c.toString(), 20);

                        } else {
                            double distnce = distanceCalc(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                            convertedstring = converttorealnumber(distnce);

                            String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                            if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                convertedstring_g = String.valueOf(distnce);
                            }
                            podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, String.valueOf(distnce), convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("task_id"), "drs cancel", c.toString(), 20);

                        }
                        statusfromdb = checktaskalreadyadded(latlonglist, mCriticalogSharedPreferences.getData("task_id"));
                        Log.d("status from ", "db" + statusfromdb);
                        if (statusfromdb == false) {
                            latlonglist.add(podClaimPojo);
                            dbHelper.insertLatLOngDetails(podClaimPojo);
                        }
                    } else {

                        PodClaimPojo podClaimPojo = null;
                        if (start_lat == 0 || start_long == 0 || wayLongitude == 0 || wayLatitude == 0) {

                            String lastLatLongList = dbHelper.getLastLatLongDetails();
                            if (!lastLatLongList.equals("-")) {
                                String[] bits = lastLatLongList.split("-");
                                String lastOne = bits[bits.length - 1];

                                double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);

                                String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                    convertedstring_g = String.valueOf(distnce);
                                }
                                podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, String.valueOf(distnce), convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("task_id"), "drs cancel", c.toString(), 20);
                            }

                        } else {

                            String lastLatLongList = dbHelper.getLastLatLongDetails();

                            if (!lastLatLongList.equals("-")) {
                                String[] bits = lastLatLongList.split("-");
                                String lastOne = bits[bits.length - 1];

                                double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                convertedstring = converttorealnumber(distnce);

                                String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                    convertedstring_g = String.valueOf(distnce);
                                }
                                podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, String.valueOf(distnce), convertedstring_g, date, time, "1", mCriticalogSharedPreferences.getData("task_id"), "drs cancel", c.toString(), 20);
                            }

                            mCriticalogSharedPreferences.saveData("hub_to_firstactivity", convertedstring);
                        }

                        latlonglist.add(podClaimPojo);
                        dbHelper.insertLatLOngDetails(podClaimPojo);
                        mCriticalogSharedPreferences.saveData("start_date", date);
                        mCriticalogSharedPreferences.saveData("start_time", time);
                        mCriticalogSharedPreferences.saveData("firstactivitydone", "yes");
                    }
                }
            });
            thread.start();
            userRiderLog(convertedstring);
            startActivity(new Intent(this, DRSActivity.class));
        } else {

            Toasty.error(this, drsCancelResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    public void userRiderLog(String lastKM) {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;

        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo("");
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId("");
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(mCriticalogSharedPreferences.getData("task_id"));
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("4");
        riderLogRequest.setmLastKm(lastKM);
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("3");
        riderLogRequest.setActionNumber(4);
        riderLogRequest.setOsVersion(androidOS);

        dbHelper.insertLastLatLong(userId, String.valueOf(latitude), String.valueOf(longitude));

        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    private double distanceCalc(double lat1, double lon1, double lat2, double lon2) {
        double distance;
        double distance1;
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(lon1);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(lon2);

        distance = startPoint.distanceTo(endPoint);

        distance1 = distance / 1000;

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        System.out.println(numberFormat.format(distance1));

        return Double.parseDouble(numberFormat.format(distance1));
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist / 0.62137;

        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private boolean checktaskalreadyadded(ArrayList<PodClaimPojo> latlonglist, String docket) {
        boolean status = false;
        for (int i = 0; i < latlonglist.size(); i++) {
            PodClaimPojo podClaimPojo = latlonglist.get(i);
            if (podClaimPojo.getTaskid().equalsIgnoreCase(docket))
                status = true;
        }

        return status;
    }

    public String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        if (mProgressBar != null) {
            mProgressBar.dismiss();
        }


        Toast.makeText(DRSActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (listOnGoingList != null) {
            drsAdapter.getFilter().filter(newText);
        }

        return true;
    }

    @Override
    public void drsCancelClick(String clientName, String phone, String address1, String address2, String city,
                               String state, String pincode, String drsNo, String bulkFlag, String swap,
                               String taskNo, String modeOfPay) {
        getLocation();

        PickupReasonsRequest pickupReasonsRequest = new PickupReasonsRequest();
        pickupReasonsRequest.setUserId(userId);
        pickupReasonsRequest.setProductStatus("delivery");
        pickupReasonsRequest.setAction("reasonscancel");
        pickupReasonsRequest.setLatitude(String.valueOf(latitude));
        pickupReasonsRequest.setLongitude(String.valueOf(longitude));
        mProgressBar.show();

        reasons.clear();
        reasonList.clear();
        Call<PickupReasonsResponse> pickupReasonsResponseCall = mRestServices.pickupCancelReasons(pickupReasonsRequest);

        pickupReasonsResponseCall.enqueue(new Callback<PickupReasonsResponse>() {
            @Override
            public void onResponse(Call<PickupReasonsResponse> call, Response<PickupReasonsResponse> response) {
                mProgressBar.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        ;
                        reasons = response.body().getData();

                        for (int i = 0; i < reasons.size(); i++) {

                            reasonList.add(reasons.get(i).getReasondesc());
                        }

                        showCancelDialog(clientName, phone, address1, address2, city, state, pincode, drsNo, bulkFlag, swap, taskNo, modeOfPay);
                    } else {
                        mProgressBar.dismiss();
                    }
                } else {
                    mProgressBar.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PickupReasonsResponse> call, Throwable t) {

            }
        });

    }

    public void dialogCallAlert(String message) {
        //Dialog Manual Entry
        TextView mDialogText, mOkDialog;
        ImageView mRightMark;
        dialogCallAlert = new Dialog(this);
        dialogCallAlert.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = dialogCallAlert.findViewById(R.id.mOk);
        mDialogText = dialogCallAlert.findViewById(R.id.mDialogText);
        mRightMark = dialogCallAlert.findViewById(R.id.mRightMark);
        mRightMark.setVisibility(View.GONE);
        mDialogText.setText(message);
        dialogCallAlert.setCancelable(false);
        dialogCallAlert.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCallAlert.dismiss();
            }
        });
    }

    public void otpDilaog(String bookingId, String uniqueId) {
        //Dialog Manual Entry

        EditText mMobileNumber;
        TextView mCall, mDialogText, mDirectCall;
        ImageView mCanceDialog;


        manualMobileNumberDialog = new Dialog(this);
        manualMobileNumberDialog.setContentView(R.layout.manual_entry_mobiel_dialoge);
        mMobileNumber = manualMobileNumberDialog.findViewById(R.id.mMobileNumber);
        mCall = manualMobileNumberDialog.findViewById(R.id.mCall);
        mDialogText = manualMobileNumberDialog.findViewById(R.id.mDialogText);
        mDirectCall = manualMobileNumberDialog.findViewById(R.id.mDirectCall);
        mCanceDialog = manualMobileNumberDialog.findViewById(R.id.mCanceDialog);
        manualMobileNumberDialog.setCanceledOnTouchOutside(false);
        mMobileNumber.setHint("Enter OTP");
        mDialogText.setText("Enter OTP");
        mCall.setText("Verify OTP");
        mDirectCall.setVisibility(View.GONE);
        manualMobileNumberDialog.show();

        Window window = manualMobileNumberDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        mCanceDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualMobileNumberDialog.dismiss();
            }
        });
        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = mMobileNumber.getText().toString();
                if (otp.equalsIgnoreCase("")) {
                    Toast.makeText(DRSActivity.this, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
                } else if (otp.length() != 4) {
                    Toast.makeText(DRSActivity.this, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
                } else {
                    callGenerateOTP(otp, "no");
                }
            }
        });


    }

    public void checkCallMaskStatus(String onlyCallMask) {
        CallMaskingStatusRequest callMaskingStatusRequest = new CallMaskingStatusRequest();
        callMaskingStatusRequest.setAction("is_call_masking_done");
        callMaskingStatusRequest.setUserId(userId);
        callMaskingStatusRequest.setDrsNo(mCriticalogSharedPreferences.getData("drs_number"));
        callMaskingStatusRequest.setConnoteNo(mCriticalogSharedPreferences.getData("task_id"));


        Call<CallMaskingStatusResponse> callMaskingStatusResponseCall = mRestServices.getCallMaskStatus(callMaskingStatusRequest);

        mProgressBar.show();
        callMaskingStatusResponseCall.enqueue(new Callback<CallMaskingStatusResponse>() {
            @Override
            public void onResponse(Call<CallMaskingStatusResponse> call, Response<CallMaskingStatusResponse> response) {
                mProgressBar.dismiss();
                if (response.body().getStatus() == 200) {
                    if (onlyCallMask.equalsIgnoreCase("1")) {
                        callDRSCancel();
                    } else {
                        callGenerateOTP("", "Yes");
                    }

                } else {
                    dialogCallAlert(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CallMaskingStatusResponse> call, Throwable t) {
                mProgressBar.dismiss();
            }
        });
    }

    public void callGenerateOTP(String otp, String otpSendFlag) {
        NDCOtpRequest ndcOtpRequest = new NDCOtpRequest();
        ndcOtpRequest.setAction("validate_otp_on_ndc");
        ndcOtpRequest.setOtp(otp);
        ndcOtpRequest.setBookingNo(mCriticalogSharedPreferences.getData("task_id"));
        ndcOtpRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code_otp"));
        ndcOtpRequest.setSendOtp(otpSendFlag);
        ndcOtpRequest.setConsignee(mCriticalogSharedPreferences.getData("consignee"));
        ndcOtpRequest.setUserId(userId);
        /*criticalogSharedPreferences.getData("mobile_number_for_otp")*/
        ndcOtpRequest.setMobile(mCriticalogSharedPreferences.getData("get_phone_otp"));
        ndcOtpRequest.setPrsDrsNo(mCriticalogSharedPreferences.getData("drs_number"));
        ndcOtpRequest.setRoundNo(mCriticalogSharedPreferences.getData("ROUND_NO"));
        ndcOtpRequest.setFor_type("drs");

        mProgressBar.show();
        Call<NDCOtpResponse> ndcOtpResponseCall = mRestServices.getNDCOtp(ndcOtpRequest);
        ndcOtpResponseCall.enqueue(new Callback<NDCOtpResponse>() {
            @Override
            public void onResponse(Call<NDCOtpResponse> call, Response<NDCOtpResponse> response) {
                mProgressBar.dismiss();
                if (response.body().getStatus() == 200) {
                    if (otpSendFlag.equalsIgnoreCase("Yes")) {
                        otpDilaog("", "");
                    } else {
                        if (manualMobileNumberDialog != null) {
                            if (manualMobileNumberDialog.isShowing()) {
                                manualMobileNumberDialog.dismiss();
                            }
                        }
                        Toast.makeText(DRSActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        callDRSCancel();
                    }
                } else {
                    Toast.makeText(DRSActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NDCOtpResponse> call, Throwable t) {
                mProgressBar.dismiss();
            }
        });
    }

    private void showCancelDialog(String clientName, String phone, String address1, String address2, String city, String state, String pincode, String drsNo, String bulkFlag, String swap, String taskNo, String modeOfPay) {
        MaterialDialog dialog = new MaterialDialog.Builder(DRSActivity.this)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                        reasonCode = reasons.get(which).getReasoncode();
                        String otpFlag = reasons.get(which).getOtpFlag();
                        String callFlag = reasons.get(which).getCallmaskFlag();

                        if (callFlag.equalsIgnoreCase("1") && otpFlag.equalsIgnoreCase("1")) {
                            checkCallMaskStatus("0");
                        } else if (callFlag.equalsIgnoreCase("1") && otpFlag.equalsIgnoreCase("0")) {

                            checkCallMaskStatus("1");

                        } else if (callFlag.equalsIgnoreCase("0") && otpFlag.equalsIgnoreCase("1")) {

                            callGenerateOTP("", "Yes");
                        } else {
                            if (reasonCode.equalsIgnoreCase("CI")) {
                                datePicker(taskNo, which);
                            } else if (reasonCode.equalsIgnoreCase("LD")) {
                                datePicker(taskNo, which);
                            } else {
                                mCriticalogSharedPreferences.saveData("R_DATE", "");
                                mCriticalogSharedPreferences.saveData("R_TIME", "");
                                if (which == 0 || which == 3) {
                                    getReferenceNo(which, taskNo);
                                } else {
                                    callDRSCancel();
                                }
                            }
                        }

                        return true; // allow selection
                    }
                })
                .show();
    }

    private void datePicker(String task_id, int which) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        mCriticalogSharedPreferences.saveData("R_DATE", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        Log.e("DATE_FORMAT", year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                        tiemPicker((year + "-" + (monthOfYear + 1) + "-" + dayOfMonth), task_id, which);
                    }
                }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private void tiemPicker(String date_time, String task_id, int which) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mHour = hourOfDay;
                        mMinute = minute;
                        //  Toast.makeText(Drop_IDdetailsActivity.this, date_time + " " + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
                        mCriticalogSharedPreferences.saveData("R_TIME", hourOfDay + ":" + minute);

                        Log.e("TIME_FORMAT", hourOfDay + ":" + minute);

                        if (which == 0 || which == 3) {
                            getReferenceNo(which, task_id);
                        } else {
                            callDRSCancel();
                        }
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void getReferenceNo(int which, String task_id) {
        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.input_reference, null);

        final EditText editText = (EditText) dialogView.findViewById(R.id.refno);


        Button submit = (Button) dialogView.findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refno = editText.getText().toString();
                if (refno.length() == 9) {
                    // datePicker();
                    //  checknetworkconnection(task_id);
                    dialogBuilder.dismiss();
                    callDRSCancel();
                } else {
                    editText.setError("Please enter 9 digit Ref.no");
                }
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    public void callDRSCancel() {
        getLocation();
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c);

        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        String time = df1.format(c);

        if (mCriticalogSharedPreferences.getData("bulk_flag").equalsIgnoreCase("true")) {
            bulkFlag = "1";
        } else {
            bulkFlag = "0";
        }
        DrsCancelRequest drsCancelRequest = new DrsCancelRequest();
        drsCancelRequest.setAction("drs_cancel");
        drsCancelRequest.setBulkFlag(bulkFlag);
        drsCancelRequest.setDate(date);
        drsCancelRequest.setTime(time);
        drsCancelRequest.setrDate(mCriticalogSharedPreferences.getData("R_DATE"));
        drsCancelRequest.setrTime(mCriticalogSharedPreferences.getData("R_TIME"));
        drsCancelRequest.setLatitude(String.valueOf(latitude));
        drsCancelRequest.setLongitude(String.valueOf(longitude));
        drsCancelRequest.setReasons(reasonCode);
        drsCancelRequest.setRefcount(mCriticalogSharedPreferences.getData("drs_number"));
        drsCancelRequest.setReferencenumber(refno);
        drsCancelRequest.setTaskId(mCriticalogSharedPreferences.getData("task_id"));
        drsCancelRequest.setUserId(userId);

        mPresenter = new DRSPresenterImpl(this, new GetDrsInteractorImpl(), new GetDrsInteractorImpl());
        mPresenter.drsCancel(token, drsCancelRequest);

    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(DRSActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DRSActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DRSActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(DRSActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    mCriticalogSharedPreferences.saveData("call_lat", String.valueOf(latitude));
                    mCriticalogSharedPreferences.saveData("call_long", String.valueOf(longitude));
                } else {

                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });
        }
    }

    @Override
    public void callMaskClick(int pos, String mobileNum, String drsNum, String taskId) {
        getLocation();
    }

}


