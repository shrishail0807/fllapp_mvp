package com.criticalog.ecritica.MVPDRS;

import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;

public interface DRSListContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onLoadDRSList(String token, DRSListRequest drsListRequest);

        void drsCancel(String token, DrsCancelRequest drsCancelRequest);


    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToViews(DRSListResponse drsListResponse);

        void drsCancelCallSuceess(DrsCancelResponse drsCancelResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetDRSIntractor {

        interface OnFinishedListener {
            void onFinished(DRSListResponse drsListResponse);
            void onFailure(Throwable t);
        }
        void drsListSuccessful(DRSListContract.GetDRSIntractor.OnFinishedListener onFinishedListener, String token, DRSListRequest drsListRequest);
    }

    interface GetDRSCancelIntractor {

        interface OnFinishedListener {
            void onFinished(DrsCancelResponse drsCancelResponse);
            void onFailure(Throwable t);
        }
        void drsCancelSuccess(DRSListContract.GetDRSCancelIntractor.OnFinishedListener onFinishedListener, String token, DrsCancelRequest drsCancelRequest);
    }
}
