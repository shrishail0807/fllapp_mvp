package com.criticalog.ecritica.MVPDRS;

import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelRequest;
import com.criticalog.ecritica.MVPDRS.DRSCancelModel.DrsCancelResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseContract;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListRequest;
import com.criticalog.ecritica.MVPDRS.DRSModel.DRSListResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetDrsInteractorImpl implements DRSListContract.GetDRSIntractor, DRSListContract.GetDRSCancelIntractor {
    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void drsListSuccessful(DRSListContract.GetDRSIntractor.OnFinishedListener onFinishedListener, String token, DRSListRequest drsListRequest) {

        Call<DRSListResponse> drsListResponseCall = service.getDRSList(drsListRequest);
        drsListResponseCall.enqueue(new Callback<DRSListResponse>() {
            @Override
            public void onResponse(Call<DRSListResponse> call, Response<DRSListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DRSListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });

    }


    @Override
    public void drsCancelSuccess(DRSListContract.GetDRSCancelIntractor.OnFinishedListener onFinishedListener, String token, DrsCancelRequest drsCancelRequest) {
        Call<DrsCancelResponse> drsCancelResponseCall = service.drsCancel(drsCancelRequest);
        drsCancelResponseCall.enqueue(new Callback<DrsCancelResponse>() {
            @Override
            public void onResponse(Call<DrsCancelResponse> call, Response<DrsCancelResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DrsCancelResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

}
