package com.criticalog.ecritica.MVPDRS.Interfaces;

public interface ICancelOnClick {

    public void drsCancelClick(String clientName, String phone, String address1, String address2, String city, String state, String pincode,
                               String drsNo, String bulkFlag , String swap, String taskNo, String modeOfPay);
}
