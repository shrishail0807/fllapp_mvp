package com.criticalog.ecritica.MVPDRS.DRSClose;

import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PayTypeListResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetDRSCloseInteractorImpl implements DRSCloseContract.GetUPIRefreshIntractor, DRSCloseContract.GetProductDeliveredIntractor, DRSCloseContract.GetPayTypeListIntractor {
    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void refreshUpiSuccessful(DRSCloseContract.GetUPIRefreshIntractor.OnFinishedListener onFinishedListener, String token, RefreshUPIRequest refreshUPIRequest) {
        Call<RefreshUPIResponse> refreshUPIResponseCall = service.refreshUPI(refreshUPIRequest);
        refreshUPIResponseCall.enqueue(new Callback<RefreshUPIResponse>() {
            @Override
            public void onResponse(Call<RefreshUPIResponse> call, Response<RefreshUPIResponse> response) {
                onFinishedListener.onFinished(response.body());

            }

            @Override
            public void onFailure(Call<RefreshUPIResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }




    @Override
    public void productDeliveredRequest(DRSCloseContract.GetProductDeliveredIntractor.OnFinishedListener onFinishedListener, String token, ProductDeliveredRequest productDeliveredRequest) {

        Call<ProductDeliveredResponse> productDeliveredResponseCall=service.productDelivered(productDeliveredRequest);

        productDeliveredResponseCall.enqueue(new Callback<ProductDeliveredResponse>() {
            @Override
            public void onResponse(Call<ProductDeliveredResponse> call, Response<ProductDeliveredResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ProductDeliveredResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }


    @Override
    public void payTypeListSuccessful(DRSCloseContract.GetPayTypeListIntractor.OnFinishedListener onFinishedListener, PaytypeListRequest paytypeListRequest) {
        Call<PayTypeListResponse> payTypeListResponseCall=service.getPayTypeList(paytypeListRequest);

        payTypeListResponseCall.enqueue(new Callback<PayTypeListResponse>() {
            @Override
            public void onResponse(Call<PayTypeListResponse> call, Response<PayTypeListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PayTypeListResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }
}
