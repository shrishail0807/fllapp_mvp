package com.criticalog.ecritica.MVPDRS.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.DrsRule;
import com.criticalog.ecritica.model.DrsRulesQc;

import java.util.List;

public class RulesDRSAdapter extends RecyclerView.Adapter<RulesDRSAdapter.ViewHolder>{

    private List<DrsRulesQc> userDetailsBulks;

    public RulesDRSAdapter(List<DrsRulesQc> userDetailsBulks) {
        this.userDetailsBulks = userDetailsBulks;
    }

    @Override
    public RulesDRSAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row, parent, false);
        return new RulesDRSAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RulesDRSAdapter.ViewHolder holder, int position) {

        DrsRulesQc tasklist= userDetailsBulks.get(position);

        holder.xTvTaskId.setText(String.valueOf(position+1)+") "+tasklist.getRule());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView xTvTaskId;

        public ViewHolder(View itemView) {
            super(itemView);

            xTvTaskId = itemView.findViewById(R.id.xTvTaskId);
        }
    }

    @Override
    public int getItemCount() {
        return userDetailsBulks.size();
    }
}
/*
package com.criticalog.ecritica.MVPDRS.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.DrsRule;

import java.util.List;

public class RulesDRSAdapter extends RecyclerView.Adapter<RulesDRSAdapter.ViewHolder>{

    private List<DrsRule> userDetailsBulks;

    public RulesDRSAdapter(List<DrsRule> userDetailsBulks) {
        this.userDetailsBulks = userDetailsBulks;
    }

    @Override
    public RulesDRSAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row, parent, false);
        return new RulesDRSAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RulesDRSAdapter.ViewHolder holder, int position) {

        DrsRule tasklist= userDetailsBulks.get(position);

        holder.xTvTaskId.setText(String.valueOf(position+1)+") "+tasklist.getRule());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView xTvTaskId;

        public ViewHolder(View itemView) {
            super(itemView);

            xTvTaskId = itemView.findViewById(R.id.xTvTaskId);
        }
    }

    @Override
    public int getItemCount() {
        return userDetailsBulks.size();
    }
}*/
