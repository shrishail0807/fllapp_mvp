package com.criticalog.ecritica.MVPDRS.DRSModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataDRS {
    @SerializedName("ROUND")
    @Expose
    private String round;
    @SerializedName("Client_code")
    @Expose
    private String clientCode;
    @SerializedName("Client_name")
    @Expose
    private String clientName;
    @SerializedName("swapped")
    @Expose
    private String swapped;
    @SerializedName("Driver_id")
    @Expose
    private String driverId;
    @SerializedName("consignee")
    @Expose
    private String consignee;
    @SerializedName("Reciver_NAME")
    @Expose
    private String reciverNAME;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("Address3")
    @Expose
    private String address3;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("DRS_No")
    @Expose
    private String dRSNo;
    @SerializedName("bulk")
    @Expose
    private String bulk;
    @SerializedName("otp_flag")
    @Expose
    private String otpFlag;
    @SerializedName("otp_verified")
    @Expose
    private Integer otpVerified;
    @SerializedName("tasks")
    @Expose
    private List<TaskDRS> tasks = null;
    @SerializedName("open_delivery")
    @Expose
    private String openDelivery;
    @SerializedName("force_drs_check")
    @Expose
    private String forceDrsCheck;
    @SerializedName("sdc_mandatory")
    @Expose
    private String sdcMandatory;

    @SerializedName("custom_option1")
    @Expose
    private String custom_option1;

    public String getForce_drs_checklist_otp() {
        return force_drs_checklist_otp;
    }

    public void setForce_drs_checklist_otp(String force_drs_checklist_otp) {
        this.force_drs_checklist_otp = force_drs_checklist_otp;
    }

    @SerializedName("force_drs_checklist_otp")
    @Expose
    private String force_drs_checklist_otp;



    public String getdRSNo() {
        return dRSNo;
    }

    public void setdRSNo(String dRSNo) {
        this.dRSNo = dRSNo;
    }

    public String getCustom_option1() {
        return custom_option1;
    }

    public void setCustom_option1(String custom_option1) {
        this.custom_option1 = custom_option1;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getSwapped() {
        return swapped;
    }

    public void setSwapped(String swapped) {
        this.swapped = swapped;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getReciverNAME() {
        return reciverNAME;
    }

    public void setReciverNAME(String reciverNAME) {
        this.reciverNAME = reciverNAME;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDRSNo() {
        return dRSNo;
    }

    public void setDRSNo(String dRSNo) {
        this.dRSNo = dRSNo;
    }

    public String getBulk() {
        return bulk;
    }

    public void setBulk(String bulk) {
        this.bulk = bulk;
    }

    public String getOtpFlag() {
        return otpFlag;
    }

    public void setOtpFlag(String otpFlag) {
        this.otpFlag = otpFlag;
    }

    public Integer getOtpVerified() {
        return otpVerified;
    }

    public void setOtpVerified(Integer otpVerified) {
        this.otpVerified = otpVerified;
    }

    public List<TaskDRS> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskDRS> tasks) {
        this.tasks = tasks;
    }

    public String getOpenDelivery() {
        return openDelivery;
    }

    public void setOpenDelivery(String openDelivery) {
        this.openDelivery = openDelivery;
    }

    public String getForceDrsCheck() {
        return forceDrsCheck;
    }

    public void setForceDrsCheck(String forceDrsCheck) {
        this.forceDrsCheck = forceDrsCheck;
    }

    public String getSdcMandatory() {
        return sdcMandatory;
    }

    public void setSdcMandatory(String sdcMandatory) {
        this.sdcMandatory = sdcMandatory;
    }
   /* @SerializedName("ROUND")
    @Expose
    private String round;
    @SerializedName("Client_code")
    @Expose
    private String clientCode;
    @SerializedName("Client_name")
    @Expose
    private String clientName;
    @SerializedName("swapped")
    @Expose
    private String swapped;
    @SerializedName("Driver_id")
    @Expose
    private String driverId;
    @SerializedName("consignee")
    @Expose
    private String consignee;
    @SerializedName("Reciver_NAME")
    @Expose
    private String reciverNAME;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("Address3")
    @Expose
    private String address3;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("DRS_No")
    @Expose
    private String dRSNo;
    @SerializedName("bulk")
    @Expose
    private String bulk;
    @SerializedName("otp_flag")
    @Expose
    private String otpFlag;
    @SerializedName("otp_verified")
    @Expose
    private Integer otpVerified;
    @SerializedName("tasks")
    @Expose
    private List<TaskDRS> tasks = null;
    @SerializedName("open_delivery")
    @Expose
    private String openDelivery;

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getSwapped() {
        return swapped;
    }

    public void setSwapped(String swapped) {
        this.swapped = swapped;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getReciverNAME() {
        return reciverNAME;
    }

    public void setReciverNAME(String reciverNAME) {
        this.reciverNAME = reciverNAME;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDRSNo() {
        return dRSNo;
    }

    public void setDRSNo(String dRSNo) {
        this.dRSNo = dRSNo;
    }

    public String getBulk() {
        return bulk;
    }

    public void setBulk(String bulk) {
        this.bulk = bulk;
    }

    public String getOtpFlag() {
        return otpFlag;
    }

    public void setOtpFlag(String otpFlag) {
        this.otpFlag = otpFlag;
    }

    public Integer getOtpVerified() {
        return otpVerified;
    }

    public void setOtpVerified(Integer otpVerified) {
        this.otpVerified = otpVerified;
    }

    public List<TaskDRS> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskDRS> tasks) {
        this.tasks = tasks;
    }

    public String getOpenDelivery() {
        return openDelivery;
    }

    public void setOpenDelivery(String openDelivery) {
        this.openDelivery = openDelivery;
    }*/


}
