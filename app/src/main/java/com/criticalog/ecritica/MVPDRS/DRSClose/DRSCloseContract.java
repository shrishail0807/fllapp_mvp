package com.criticalog.ecritica.MVPDRS.DRSClose;

import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PayTypeListResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.PaytypeListRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.Model.RefreshUPIResponse;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredRequest;
import com.criticalog.ecritica.MVPDRS.DRSClose.ProductDeliveredModel.ProductDeliveredResponse;

public interface DRSCloseContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void onRefreshUPI(String token, RefreshUPIRequest refreshUPIRequest);

        void productDeliveredCall(String token, ProductDeliveredRequest productDeliveredRequest);

        void getPayTypeList(PaytypeListRequest paytypeListRequest);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataUpiViews(RefreshUPIResponse refreshUPIResponse);

        void productDeliveredResponseToViews(ProductDeliveredResponse productDeliveredResponse);

        void payTypeListResponse(PayTypeListResponse payTypeListResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetUPIRefreshIntractor {

        interface OnFinishedListener {
            void onFinished(RefreshUPIResponse refreshUPIResponse);

            void onFailure(Throwable t);
        }

        void refreshUpiSuccessful(DRSCloseContract.GetUPIRefreshIntractor.OnFinishedListener onFinishedListener, String token, RefreshUPIRequest refreshUPIRequest);
    }

    interface GetProductDeliveredIntractor {

        interface OnFinishedListener {
            void onFinished(ProductDeliveredResponse productDeliveredResponse);

            void onFailure(Throwable t);
        }

        void productDeliveredRequest(DRSCloseContract.GetProductDeliveredIntractor.OnFinishedListener onFinishedListener, String token, ProductDeliveredRequest productDeliveredRequest);
    }

    interface GetPayTypeListIntractor {
        interface OnFinishedListener {
            void onFinished(PayTypeListResponse payTypeListResponse);

            void onFailure(Throwable t);
        }

        void payTypeListSuccessful(DRSCloseContract.GetPayTypeListIntractor.OnFinishedListener onFinishedListener, PaytypeListRequest paytypeListRequest);
    }
}
