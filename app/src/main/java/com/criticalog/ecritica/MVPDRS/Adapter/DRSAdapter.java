package com.criticalog.ecritica.MVPDRS.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Activities.CallMasking;
import com.criticalog.ecritica.Activities.ReadOtpActivity;
import com.criticalog.ecritica.Interface.CallMaskInterface;
import com.criticalog.ecritica.Interface.IAdapterClickListener;
import com.criticalog.ecritica.MVPDRS.DRSModel.DataDRS;
import com.criticalog.ecritica.MVPDRS.DRSModel.TaskDRS;
import com.criticalog.ecritica.MVPDRS.Interfaces.ICancelOnClick;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.ModelClasses.TaskOngoingResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestClient1;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Rest.RestServices1;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.model.CallRequest;
import com.criticalog.ecritica.model.CallResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DRSAdapter extends RecyclerView.Adapter<DRSAdapter.DRSViewHolder> implements Filterable, IAdapterClickListener {
    Context context;
    List<DataDRS> dataDRSArrayListFull;
    List<DataDRS> mfilteredlist;
    ICancelOnClick iCancelOnClick;
    IAdapterClickListener iAdapterClickListener;
    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    ArrayList<String> filterStringList;
    private RestServices mRestServices;
    private ProgressDialog mProgressDialog;
    DRSViewHolder viewHolder;
    RestServices1 mRestServices1;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude = 0.0, longitude = 0.0;
    private CallMaskInterface callMaskInterface;

    public DRSAdapter(Context context, List<DataDRS> dataDRSArrayList, ArrayList<String> filterStringList, ICancelOnClick iCancelOnClick, CallMaskInterface callMaskInterface) {
        this.context = context;
        this.dataDRSArrayListFull = dataDRSArrayList;
        mfilteredlist = dataDRSArrayListFull;
        this.iCancelOnClick = iCancelOnClick;
        this.filterStringList = filterStringList;
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle("Please Wait!");
        mProgressDialog.setCancelable(false);
        mRestServices1 = RestClient1.getRetrofitInstance().create(RestServices1.class);
        this.callMaskInterface = callMaskInterface;


    }

    @NonNull
    @Override
    public DRSAdapter.DRSViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_drs_mvp, parent, false);
        return new DRSViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onBindViewHolder(@NonNull DRSAdapter.DRSViewHolder holder, @SuppressLint("RecyclerView") int position) {

        viewHolder = holder;
        holder.clientname.setText("Client Name: " + mfilteredlist.get(position).getClientName());

        holder.mClientCode.setText("Client Code: " + mfilteredlist.get(position).getClientCode());
        holder.drsno.setText("DRS No : " + mfilteredlist.get(position).getDRSNo());
        holder.consignee.setText("Consignee :" + mfilteredlist.get(position).getConsignee());

        final boolean isExpanded = position == mExpandedPosition;
        holder.mTaskList.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

        holder.mdropdown_icon.setActivated(isExpanded);

        holder.mdropdown_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMaskInterface.callMaskClick(1, "", "", "");
            }
        });

        if (isExpanded) {
            previousExpandedPosition = position;
            holder.mdropdown_icon.setImageResource(R.drawable.expand_less_black_24dp);
        } else {
            holder.mdropdown_icon.setImageResource(R.drawable.expand_more_black_24dp);
        }

        holder.mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobileNumber = mfilteredlist.get(position).getPhone();
                if (mobileNumber.equalsIgnoreCase("")) {
                    Toast.makeText(context, "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
                } else {
                    mProgressDialog.setTitle("Please wait we are connecting the Call...Receive the call it will connect to end user");
                    mProgressDialog.show();

                    CallRequest callRequest = new CallRequest();
                    callRequest.setAction("call_masking");
                    callRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
                    callRequest.setTo_number(mobileNumber);
                    callRequest.setDrs_no(mfilteredlist.get(position).getDRSNo());
                    callRequest.setDrs(mfilteredlist.get(position).getDRSNo());
                    Call<CallResponse> callResponseCall = mRestServices1.callInitiate(callRequest);
                    callResponseCall.enqueue(new Callback<CallResponse>() {
                        @Override
                        public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                            if (response.body().getStatus() == 200) {
                                final Handler handler = new Handler(Looper.getMainLooper());
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        mProgressDialog.dismiss();
                                        Log.e("FAILURE_SUCCESS", response.body().getMessage());
                                        //Do something after 100ms
                                    }
                                }, 2000);
                            } else {
                                mProgressDialog.dismiss();
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("FAILURE_CALL", response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<CallResponse> call, Throwable t) {
                            mProgressDialog.dismiss();
                            Log.e("FAILURE_CALL", t.toString());
                        }
                    });
                }
            }
        });

        holder.mdropdown_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);
                mCriticalogSharedPreferences.saveData("open_delivery_flag", mfilteredlist.get(position).getOpenDelivery());
                mCriticalogSharedPreferences.saveData("force_drs_check", mfilteredlist.get(position).getForceDrsCheck());
                mCriticalogSharedPreferences.saveData("sdc_mandatory", mfilteredlist.get(position).getSdcMandatory());
                mCriticalogSharedPreferences.saveData("custom_option1", mfilteredlist.get(position).getCustom_option1());

                mCriticalogSharedPreferences.saveData("client_code_otp", mfilteredlist.get(position).getClientCode());
                mCriticalogSharedPreferences.saveData("get_phone_otp", mfilteredlist.get(position).getPhone());
                mCriticalogSharedPreferences.saveData("get_consinee_otp", mfilteredlist.get(position).getConsignee());
                mCriticalogSharedPreferences.saveData("drs_no_otp", mfilteredlist.get(position).getDRSNo());
                mCriticalogSharedPreferences.saveData("round_otp", mfilteredlist.get(position).getRound());

                mCriticalogSharedPreferences.saveData("otp_flag_drs", mfilteredlist.get(position).getOtpFlag());
                mCriticalogSharedPreferences.saveData("otp_flag_verified_drs", String.valueOf(mfilteredlist.get(position).getOtpVerified()));
                mCriticalogSharedPreferences.saveData("address_consignee1", mfilteredlist.get(position).getAddress1());
                mCriticalogSharedPreferences.saveData("address_consignee2", mfilteredlist.get(position).getAddress2());
                mCriticalogSharedPreferences.saveData("address_consignee3", mfilteredlist.get(position).getAddress3());
                mCriticalogSharedPreferences.saveData("pincode_consignee", mfilteredlist.get(position).getPincode());
                mCriticalogSharedPreferences.saveData("consignee", mfilteredlist.get(position).getConsignee());
                mCriticalogSharedPreferences.saveData("force_drs_checklist_otp",mfilteredlist.get(position).getForce_drs_checklist_otp());
            }
        });

        holder.xdroprow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);
                mCriticalogSharedPreferences.saveData("open_delivery_flag", mfilteredlist.get(position).getOpenDelivery());
                mCriticalogSharedPreferences.saveData("force_drs_check", mfilteredlist.get(position).getForceDrsCheck());
                mCriticalogSharedPreferences.saveData("sdc_mandatory", mfilteredlist.get(position).getSdcMandatory());
                mCriticalogSharedPreferences.saveData("custom_option1", mfilteredlist.get(position).getCustom_option1());

                mCriticalogSharedPreferences.saveData("client_code_otp", mfilteredlist.get(position).getClientCode());
                mCriticalogSharedPreferences.saveData("get_phone_otp", mfilteredlist.get(position).getPhone());
                mCriticalogSharedPreferences.saveData("get_consinee_otp", mfilteredlist.get(position).getConsignee());
                mCriticalogSharedPreferences.saveData("drs_no_otp", mfilteredlist.get(position).getDRSNo());
                mCriticalogSharedPreferences.saveData("round_otp", mfilteredlist.get(position).getRound());
                mCriticalogSharedPreferences.saveData("otp_flag_drs", mfilteredlist.get(position).getOtpFlag());
                mCriticalogSharedPreferences.saveData("otp_flag_verified_drs", String.valueOf(mfilteredlist.get(position).getOtpVerified()));
                mCriticalogSharedPreferences.saveData("address_consignee1", mfilteredlist.get(position).getAddress1());
                mCriticalogSharedPreferences.saveData("address_consignee2", mfilteredlist.get(position).getAddress2());
                mCriticalogSharedPreferences.saveData("address_consignee3", mfilteredlist.get(position).getAddress3());
                mCriticalogSharedPreferences.saveData("pincode_consignee", mfilteredlist.get(position).getPincode());
                mCriticalogSharedPreferences.saveData("consignee", mfilteredlist.get(position).getConsignee());
                mCriticalogSharedPreferences.saveData("force_drs_checklist_otp",mfilteredlist.get(position).getForce_drs_checklist_otp());
            }
        });


        if (mfilteredlist.get(position).getOtpFlag().equalsIgnoreCase("1")) {

            if (mfilteredlist.get(position).getOtpVerified() == 0) {
                holder.otptv.setVisibility(View.VISIBLE);
            } else if (mfilteredlist.get(position).getOtpVerified() == 1) {
                holder.otptv.setVisibility(View.VISIBLE);
                holder.otptv.setText("Verified");
                holder.otptv.setOnClickListener(null);
            }

        }

        DataDRS dataDRS = mfilteredlist.get(position);

        List<TaskDRS> taskDRSList = mfilteredlist.get(position).getTasks();

        DRSTaskAdapter drsTaskAdapter = new DRSTaskAdapter(context, taskDRSList, dataDRS, iCancelOnClick, DRSAdapter.this);

        holder.mTaskList.setLayoutManager(new LinearLayoutManager(context));
        holder.mTaskList.setAdapter(drsTaskAdapter);

    }

    @Override
    public int getItemCount() {
        return mfilteredlist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<DataDRS> filteredList = new ArrayList<>();
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = dataDRSArrayListFull;

                    mfilteredlist = filteredList;
                } else {

                    for (DataDRS row : dataDRSArrayListFull) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getDRSNo().toLowerCase().contains(charString.toLowerCase()) || row.getClientCode().toLowerCase().contains(charString.toLowerCase()) || row.getClientName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                        //Search Based on Connote
                        for (TaskDRS taskDRS : row.getTasks()) {
                            if (taskDRS.getTaskId().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }

                    }
                    mfilteredlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mfilteredlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //mfilteredlist.clear();
                mfilteredlist = (ArrayList<DataDRS>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onClickAdapter(int position) {


        Toast.makeText(context, "OTP Screen Clicked", Toast.LENGTH_SHORT).show();
        Toast.makeText(context, viewHolder.getAdapterPosition(), Toast.LENGTH_SHORT).show();
    }


    class DRSViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mtime, mClientCode, mcontact_num;
        private TextView mdropid, mcustomer_name, mdrs_num;
        private TextView mdrop_address1, mdrop_address2, drsno, mdrop_address3, mdroopcity_id, mdrop_state, mdrop_pincode, consignee, otptv, clientname;
        private ImageView mdropdown_icon, mdropID_details, micon_done, mlocation, mcall;
        private LinearLayout mdropdown_linearlayout;
        private RelativeLayout mdroprow, xdroprow;
        private RecyclerView mTaskList;
        private ImageView mCall;

        public DRSViewHolder(@NonNull View itemView) {
            super(itemView);
            mClientCode = (TextView) itemView.findViewById(R.id.clientcode);
            mdropdown_icon = (ImageView) itemView.findViewById(R.id.x_dropdown_icon);

            consignee = itemView.findViewById(R.id.xconsignee);
            otptv = itemView.findViewById(R.id.xOTPTV);
            clientname = itemView.findViewById(R.id.xclientname);
            mTaskList = itemView.findViewById(R.id.tasksrv);
            drsno = itemView.findViewById(R.id.xdrsno);
            xdroprow = itemView.findViewById(R.id.xdroprow);
            mCall = itemView.findViewById(R.id.mCall);

            mdropdown_icon.setOnClickListener(this);
            otptv.setOnClickListener(this);
            xdroprow.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.x_dropdown_icon:
                    break;


                case R.id.xOTPTV:
                    OTPRequest otpRequest = new OTPRequest();
                    otpRequest.setOtp("");
                    otpRequest.setAction("validate_otp");
                    otpRequest.setClientCode(mfilteredlist.get(getAdapterPosition()).getClientCode());
                    otpRequest.setOtpMobile(mfilteredlist.get(getAdapterPosition()).getPhone());
                    otpRequest.setConsignee(mfilteredlist.get(getAdapterPosition()).getConsignee());
                    otpRequest.setIsResend("2");
                    otpRequest.setRefcount(mfilteredlist.get(getAdapterPosition()).getDRSNo());
                    otpRequest.setRound(mfilteredlist.get(getAdapterPosition()).getRound());
                    otpRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
                    otpRequest.setDocket_no(mCriticalogSharedPreferences.getData("task_id"));
                    otpRequest.setFor_type("drs");

                    mCriticalogSharedPreferences.saveData("client_code_otp", mfilteredlist.get(getAdapterPosition()).getClientCode());
                    mCriticalogSharedPreferences.saveData("get_phone_otp", mfilteredlist.get(getAdapterPosition()).getPhone());
                    mCriticalogSharedPreferences.saveData("get_consinee_otp", mfilteredlist.get(getAdapterPosition()).getConsignee());
                    mCriticalogSharedPreferences.saveData("drs_no_otp", mfilteredlist.get(getAdapterPosition()).getDRSNo());
                    mCriticalogSharedPreferences.saveData("round_otp", mfilteredlist.get(getAdapterPosition()).getRound());
                    mProgressDialog.show();
                    Call<OTPResponse> otpResponseCall = mRestServices.otpRequestVerify(otpRequest);
                    otpResponseCall.enqueue(new Callback<OTPResponse>() {
                        @Override
                        public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                            mProgressDialog.dismiss();
                            if (response.body().getStatus() == 200) {
                                Intent intent = new Intent(context, ReadOtpActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("userid", mCriticalogSharedPreferences.getData("userId"));
                                intent.putExtra("refcount", mfilteredlist.get(getAdapterPosition()).getDRSNo());
                                intent.putExtra("clientcode", mfilteredlist.get(getAdapterPosition()).getClientCode());
                                intent.putExtra("round", mfilteredlist.get(getAdapterPosition()).getRound());
                                intent.putExtra("consignee", mfilteredlist.get(getAdapterPosition()).getConsignee());
                                intent.putExtra("phone", mfilteredlist.get(getAdapterPosition()).getPhone());
                                context.startActivity(intent);
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<OTPResponse> call, Throwable t) {
                            Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
            }
        }
    }
}
