package com.criticalog.ecritica.MVPDRS.DRSModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DRSListRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("app_ver_name")
    @Expose
    private String app_ver_name;

    public String getApp_ver_name() {
        return app_ver_name;
    }

    public void setApp_ver_name(String app_ver_name) {
        this.app_ver_name = app_ver_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
