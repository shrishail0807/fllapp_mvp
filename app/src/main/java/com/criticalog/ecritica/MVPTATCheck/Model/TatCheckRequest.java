package com.criticalog.ecritica.MVPTATCheck.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TatCheckRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("origin_pin")
    @Expose
    private String originPin;
    @SerializedName("destination_pin")
    @Expose
    private String destinationPin;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOriginPin() {
        return originPin;
    }

    public void setOriginPin(String originPin) {
        this.originPin = originPin;
    }

    public String getDestinationPin() {
        return destinationPin;
    }

    public void setDestinationPin(String destinationPin) {
        this.destinationPin = destinationPin;
    }
}
