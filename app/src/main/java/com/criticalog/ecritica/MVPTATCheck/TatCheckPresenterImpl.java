package com.criticalog.ecritica.MVPTATCheck;

import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckRequest;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckResponse;

public class TatCheckPresenterImpl implements TatCheckContract.presenter, TatCheckContract.GetTatCheckIntractor.OnFinishedListener {
    private TatCheckContract.MainView mainView;
    private TatCheckContract.GetTatCheckIntractor getTatCheckIntractor;

    public TatCheckPresenterImpl(TatCheckContract.MainView mainView, TatCheckContract.GetTatCheckIntractor getTatCheckIntractor) {
        this.mainView = mainView;
        this.getTatCheckIntractor = getTatCheckIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void onTatCheckClick(String token, TatCheckRequest tatCheckRequest) {

        if (mainView != null) {
            mainView.showProgress();
        }
        getTatCheckIntractor.tatcheckSuccessful(this, token, tatCheckRequest);
    }

    @Override
    public void onFinished(TatCheckResponse tatCheckResponse) {
        if (mainView != null) {
            mainView.setTatCheckDataToViews(tatCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
