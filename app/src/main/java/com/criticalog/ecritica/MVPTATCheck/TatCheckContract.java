package com.criticalog.ecritica.MVPTATCheck;

import com.criticalog.ecritica.MVPPickup.PickupContract;
import com.criticalog.ecritica.MVPPickup.PickupPostData;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckRequest;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckResponse;

public interface TatCheckContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onTatCheckClick(String token, TatCheckRequest tatCheckRequest);


    }

    interface MainView {

        void showProgress();

        void hideProgress();

        void setTatCheckDataToViews(TatCheckResponse tatCheckResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetTatCheckIntractor {

        interface OnFinishedListener {
            void onFinished(TatCheckResponse tatCheckResponse);
            void onFailure(Throwable t);
        }
        void tatcheckSuccessful(TatCheckContract.GetTatCheckIntractor.OnFinishedListener onFinishedListener, String token, TatCheckRequest tatCheckRequest);
    }
}
