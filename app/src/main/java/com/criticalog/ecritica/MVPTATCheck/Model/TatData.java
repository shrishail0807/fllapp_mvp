package com.criticalog.ecritica.MVPTATCheck.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TatData {
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("origin_control")
    @Expose
    private String originControl;
    @SerializedName("destination_control")
    @Expose
    private String destinationControl;
    @SerializedName("air_tat")
    @Expose
    private String airTat;
    @SerializedName("surface_tat")
    @Expose
    private String surfaceTat;
    @SerializedName("nfo_tat")
    @Expose
    private String nfoTat;
    @SerializedName("service_type")
    @Expose
    private String serviceType;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginControl() {
        return originControl;
    }

    public void setOriginControl(String originControl) {
        this.originControl = originControl;
    }

    public String getDestinationControl() {
        return destinationControl;
    }

    public void setDestinationControl(String destinationControl) {
        this.destinationControl = destinationControl;
    }

    public String getAirTat() {
        return airTat;
    }

    public void setAirTat(String airTat) {
        this.airTat = airTat;
    }

    public String getSurfaceTat() {
        return surfaceTat;
    }

    public void setSurfaceTat(String surfaceTat) {
        this.surfaceTat = surfaceTat;
    }

    public String getNfoTat() {
        return nfoTat;
    }

    public void setNfoTat(String nfoTat) {
        this.nfoTat = nfoTat;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
