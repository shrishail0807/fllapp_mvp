package com.criticalog.ecritica.MVPTATCheck;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckRequest;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

public class TATCheckActivity extends AppCompatActivity implements TatCheckContract.MainView, View.OnClickListener {
    private TextView mTatCheck;
    private EditText mOrigin, mDestination;
    private TatCheckContract.presenter mPresenter;
    private SimpleArcDialog mProgressBar;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token;
    private ImageView mBack;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tatcheck_mvp);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        StaticUtils.TOKEN=mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=mCriticalogSharedPreferences.getData("distance_api_key");
        mTatCheck = findViewById(R.id.mTatCheck);
        mOrigin = findViewById(R.id.mOrigin);
        mDestination = findViewById(R.id.mDestination);
        mBack = findViewById(R.id.xtv_backbutton);

        mBack.setOnClickListener(this);

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TATCheckActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mTatCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String origin = mOrigin.getText().toString();
                if (origin.trim().length() != 6) {
                    mOrigin.setError("please enter 6 digit pincode");
                } else if (mDestination.getText().toString().trim().length() != 6) {
                    mDestination.setError("please enter 6 digit pincode");
                } else {
                    TatCheckRequest tatCheckRequest = new TatCheckRequest();
                    tatCheckRequest.setAction("tat_check");
                    tatCheckRequest.setOriginPin(origin);
                    tatCheckRequest.setDestinationPin(mDestination.getText().toString());
                    tatCheckRequest.setUserId(userId);
                    tatCheckRequest.setLatitude(String.valueOf(latitude));
                    tatCheckRequest.setLongitude(String.valueOf(longitude));

                    mPresenter = new TatCheckPresenterImpl(TATCheckActivity.this, new TatCheckInteractorImpl());
                    mPresenter.onTatCheckClick(token, tatCheckRequest);
                }
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(TATCheckActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(TATCheckActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(TATCheckActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(TATCheckActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setTatCheckDataToViews(TatCheckResponse tatCheckResponse) {

        if (tatCheckResponse != null) {
            if (tatCheckResponse.getStatus() == 200) {
                Toast.makeText(TATCheckActivity.this, tatCheckResponse.getMessage(), Toast.LENGTH_SHORT).show();
                tatResultDialog(tatCheckResponse.getData().getOrigin(), tatCheckResponse.getData().getDestination(), tatCheckResponse.getData().getOriginControl(),
                        tatCheckResponse.getData().getDestinationControl(), String.valueOf(tatCheckResponse.getData().getAirTat()), String.valueOf(tatCheckResponse.getData().getSurfaceTat()),
                        tatCheckResponse.getData().getServiceType(),tatCheckResponse.getData().getNfoTat());
            } else {
                Toast.makeText(TATCheckActivity.this, tatCheckResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(TATCheckActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(TATCheckActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }


    public void tatResultDialog(String origin, String dest, String originControl, String destinationControl,
                                String airTat, String surfaceTat, String serviceType, String nfoTat) {

        TextView mOriginPlace, mDestinationPlace, mOriginControl,
                mDestinationControl, mAirTat, mSurfaceTat, mServiceType,mNFOTat;
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);

        // Include dialog.xml file
        dialog.setContentView(R.layout.tat_resut_dialog);

        Button ok = dialog.findViewById(R.id.mOk);
        mOriginPlace = dialog.findViewById(R.id.mOriginPlace);
        mDestinationPlace = dialog.findViewById(R.id.mDestinationPlace);
        mOriginControl = dialog.findViewById(R.id.mOriginControl);
        mDestinationControl = dialog.findViewById(R.id.mDestinationControl);
        mAirTat = dialog.findViewById(R.id.mAirTat);
        mSurfaceTat = dialog.findViewById(R.id.mSurfaceTat);
        mServiceType = dialog.findViewById(R.id.mServiceType);
        mNFOTat=dialog.findViewById(R.id.mNFOTat);
        TextView textBagsScanned = dialog.findViewById(R.id.mBagAnalyzeText);


        dialog.setCancelable(false);
        dialog.show();

        Window window = dialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


        mOriginPlace.setText(origin);
        mDestinationPlace.setText( dest);
        mOriginControl.setText(originControl);
        mDestinationControl.setText(destinationControl);
        if(airTat.equals("1"))
        {
            mAirTat.setText(airTat+" DAY");
        }else {
            mAirTat.setText(airTat+" DAYS");
        }
        if(surfaceTat.equals("1"))
        {
            mSurfaceTat.setText(surfaceTat+" DAY");
        }else {
            mSurfaceTat.setText(surfaceTat+" DAYS");
        }


        mServiceType.setText(serviceType);
        mNFOTat.setText(nfoTat+" HOURS");


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.mBack:
                Intent intent = new Intent(TATCheckActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }
}
