package com.criticalog.ecritica.MVPTATCheck;

import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckRequest;
import com.criticalog.ecritica.MVPTATCheck.Model.TatCheckResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TatCheckInteractorImpl implements TatCheckContract.GetTatCheckIntractor{
    RestServices services= RestClient.getRetrofitInstance().create(RestServices.class);
    @Override
    public void tatcheckSuccessful(OnFinishedListener onFinishedListener, String token, TatCheckRequest tatCheckRequest) {
        Call<TatCheckResponse> tatCheckResponseCall=services.tatCheck(tatCheckRequest);
        tatCheckResponseCall.enqueue(new Callback<TatCheckResponse>() {
            @Override
            public void onResponse(Call<TatCheckResponse> call, Response<TatCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<TatCheckResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }
}
