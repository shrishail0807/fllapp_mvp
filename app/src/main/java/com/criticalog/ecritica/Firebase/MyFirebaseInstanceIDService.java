package com.criticalog.ecritica.Firebase;


import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.VolleyClasses.GetFirebaseToken;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.e("inside","onrefreshtoken"+refreshedToken);

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);

        CriticalogSharedPreferences criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        Log.e(TAG,"user id"+criticalogSharedPreferences.getData("userId"));

        if (criticalogSharedPreferences.getData("userId") != null) {
            new GetFirebaseToken(this,criticalogSharedPreferences.getData("userId"),token);
        }


    }

    private void storeRegIdInPref(String token) {
        CriticalogSharedPreferences.getInstance(this).saveData("regid",token);
    }

}
