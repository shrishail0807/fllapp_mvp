package com.criticalog.ecritica.WorkManager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.MVPHomeScreen.Model.Detail;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyWorker extends Worker {
    private static final String DEFAULT_START_TIME = "08:00";
    private static final String DEFAULT_END_TIME = "19:00";

    private static final String TAG = "MyWorker";
    ArrayList<Detail> latlonglist = new ArrayList<>();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    /**
     * The current location.
     */
    private Location mLocation;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    private Context mContext;
    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;
    private DatabaseHelper dbHelper;

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;

        dbHelper = new DatabaseHelper(mContext);
        dbHelper.getWritableDatabase();
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.d(TAG, "doWork: Done");

        Log.d(TAG, "onStartJob: STARTING JOB..");

        DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String formattedDate = dateFormat.format(date);

        try {
            Date currentDate = dateFormat.parse(formattedDate);
            Date startDate = dateFormat.parse(DEFAULT_START_TIME);
            Date endDate = dateFormat.parse(DEFAULT_END_TIME);

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                }
            };

            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
         //   mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            try {
                mFusedLocationClient
                        .getLastLocation()
                        .addOnCompleteListener(new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                if (task.isSuccessful() && task.getResult() != null) {
                                    Date c1 = Calendar.getInstance().getTime();
                                    SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
                                    String date1 = df3.format(c1);

                                    SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
                                    String time1 = df4.format(c1);

                                    mLocation = task.getResult();
                                    Log.d(TAG, "Location : " + mLocation);
                                    PodClaimPojo podClaimPojo;

                                    latlonglist = dbHelper.getAllLatLongListNew();
                                    int listsize = latlonglist.size();
                                    if (latlonglist.size() > 0) {

                                        double distnce = distanceCalc(latlonglist.get(listsize - 1).getEndLat(), latlonglist.get(listsize - 1).getEndLong(), mLocation.getLatitude(), mLocation.getLongitude());
                                        String convertedstring = converttorealnumber(distnce);

                                       // podClaimPojo = new PodClaimPojo( latlonglist.get(listsize - 1).getEndLat(), latlonglist.get(listsize - 1).getEndLong(), mLocation.getLatitude(), mLocation.getLongitude(), convertedstring,"0.0", date1, time1, "bacground", "0", "", date1+" "+time1, 10);

                                        Log.e("double_value", String.valueOf(distnce));


                                      //  dbHelper.insertLatLOngDetails(podClaimPojo);

                                        //dbHelper.insertLatLOngDetails(podClaimPojo);

                                    }else {
                                        podClaimPojo = new PodClaimPojo( 0.0, 0.0, mLocation.getLatitude(), mLocation.getLongitude(), "0","0", date1, time1, "background", "0", "", date1+" "+time1, 10);
                                        dbHelper.insertLatLOngDetails(podClaimPojo);

                                    }

                                    // Create the NotificationChannel, but only on API 26+ because
                                    // the NotificationChannel class is new and not in the support library
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        CharSequence name = mContext.getString(R.string.app_name);
                                        String description = mContext.getString(R.string.app_name);
                                        int importance = NotificationManager.IMPORTANCE_DEFAULT;
                                        NotificationChannel channel = new NotificationChannel(mContext.getString(R.string.app_name), name, importance);
                                        channel.setDescription(description);
                                        // Register the channel with the system; you can't change the importance
                                        // or other notification behaviors after this
                                        NotificationManager notificationManager = mContext.getSystemService(NotificationManager.class);
                                        notificationManager.createNotificationChannel(channel);
                                    }

                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, mContext.getString(R.string.app_name))
                                            .setSmallIcon(android.R.drawable.ic_menu_mylocation)
                                            .setContentTitle("New Location Update")
                                            .setContentText("You are at " + getCompleteAddressString(mLocation.getLatitude(), mLocation.getLongitude()))
                                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                            .setStyle(new NotificationCompat.BigTextStyle().bigText("You are at " + getCompleteAddressString(mLocation.getLatitude(), mLocation.getLongitude())));

                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);

                                    // notificationId is a unique int for each notification that you must define
                                    notificationManager.notify(1001, builder.build());

                                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                                } else {
                                    Log.w(TAG, "Failed to get location.");
                                }
                            }
                        });
            } catch (SecurityException unlikely) {
                Log.e(TAG, "Lost location permission." + unlikely);
            }

            try {
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, null);
            } catch (SecurityException unlikely) {
                //Utils.setRequestingLocationUpdates(this, false);
                Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
            }

        } catch (ParseException ignored) {

        }

        return Result.success();
    }

    public double distance(double lat1, double lon1, double lat2, double lon2,String unit) {

        Log.e("start",String.valueOf(lat1)+","+String.valueOf(lon1)+String.valueOf(lat2)+","+String.valueOf(lon2));



        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist/0.62137;

        return dist;
    }

    private double distanceCalc(double lat1, double lon1, double lat2, double lon2) {
        double distance;
        double distance1;
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(lon1);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(lon2);

        distance = startPoint.distanceTo(endPoint);

        distance1 = distance / 1000;

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        System.out.println(numberFormat.format(distance1));

        return Double.parseDouble(numberFormat.format(distance1));
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public String converttorealnumber(double distance){
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }
}

