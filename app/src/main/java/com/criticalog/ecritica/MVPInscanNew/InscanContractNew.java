package com.criticalog.ecritica.MVPInscanNew;

import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InscanNegativeStatusResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegativeStatusRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateResponseNew;

public interface InscanContractNew {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void inScanDocketCheck(String token, DocketCheckRequestNew docketCheckRequestNew);

        void validateOriginPicode(String token, OriginPincodeValidateRequestNew originPincodeValidateRequestNew);

        void actualValueInscanCheck(String token, ActualValueInscanRequestNew actualValueInscanRequest);

        void inscanBoxNumberValidate(String token, InScanBoxRequestNew inScanBoxRequestNew);

        void inScanSubmit(String token, InScanSubmitRequestNew inScanSubmitRequestNew);

        void ewayBillValidate(EwayBillValidateRequestNew ewayBillValidateRequestNew);

        void inspectionDoneRequest(InspectionDoneRequestNew inspectionDoneRequestNew);

        void inspectionNegativeStatuse(InspectionNegativeStatusRequestNew inspectionNegativeStatusRequestNew);

        void inspectionNegativeSubmit(InspectionNegSubmitRequestNew inspectionNegSubmitRequestNew);
    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDocketCheckDataToViews(DocketCheckResponseNew docketCheckResponseNew);

        void setOriginPincodeResponse(OriginPincodeValidateResponseNew originPincodeResponse);

        void setActualValueInscanResponse(ActualValueInscanResponseNew actualValueInscanResponseNew);

        void setInscanBoxDataToViews(InScanBoxResponseNew inScanBoxResponseNew);

        void setInScanSubmitDataToViews(InScanSubmitResponseNew inScanSubmitResponseNew);

        void setEwayBillValidateResponseToViews(EwayBillValidateResponseNew ewayBillValidateResponseNew);

        void inspectionDoneResponseToData(InspectionDoneResponseNew inspectionDoneResponseNew);

        void inspectionNegativeStatusData(InscanNegativeStatusResponseNew inspectionNegativeStatusResponse);

        void inspectionNegSubmitDataToViews(InspectionNegSubmitResponseNew inspectionNegSubmitResponseNew);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface DocketCheckIntractor {

        interface OnFinishedListener {
            void onFinished(DocketCheckResponseNew docketCheckResponseNew);

            void onFailure(Throwable t);
        }

        void getDocketCheckSuccess(OnFinishedListener onFinishedListener, String token, DocketCheckRequestNew docketCheckRequestNew);
    }

    interface OriginPincodeValidateIntractor {

        interface OnFinishedListener {
            void onFinished(OriginPincodeValidateResponseNew originPincodeResponse);

            void onFailure(Throwable t);
        }

        void getOriginPincodeValidateSuccess(OnFinishedListener onFinishedListener, String token, OriginPincodeValidateRequestNew originPincodeValidateRequestNew);
    }

    interface ActualValueInscanIntractor {

        interface OnFinishedListener {
            void onFinished(ActualValueInscanResponseNew actualValueInscanResponseNew);

            void onFailure(Throwable t);
        }

        void getActualValueInscanRequest(OnFinishedListener onFinishedListener, String token, ActualValueInscanRequestNew actualValueInscanRequest);
    }

    interface InScanBoxIntractor {

        interface OnFinishedListener {
            void onFinished(InScanBoxResponseNew inScanBoxResponseNew);

            void onFailure(Throwable t);
        }

        void getActualValueInscanRequest(OnFinishedListener onFinishedListener, String token, InScanBoxRequestNew inScanBoxRequestNew);
    }

    interface InScanSubmitIntractor {

        interface OnFinishedListener {
            void onFinished(InScanSubmitResponseNew inScanSubmitResponseNew);

            void onFailure(Throwable t);
        }

        void getInScanSubmitRequest(OnFinishedListener onFinishedListener, String token, InScanSubmitRequestNew inScanSubmitRequestNew);
    }

    interface EwayBillValidateIntractor {

        interface OnFinishedListener {
            void onFinished(EwayBillValidateResponseNew ewayBillValidateResponseNew);

            void onFailure(Throwable t);
        }

        void getEwayBillRequest(OnFinishedListener onFinishedListener, String token, EwayBillValidateRequestNew ewayBillValidateRequestNew);
    }

    interface  InspectionDoneInteractor{
        interface OnFinishedListener {
            void onFinished(InspectionDoneResponseNew inspectionDoneResponseNew);

            void onFailure(Throwable t);
        }

        void inspectionDoneRequest(OnFinishedListener onFinishedListener, String token, InspectionDoneRequestNew inspectionDoneRequestNew);
    }

    interface  InspectionNegativeStatusInteractor{
        interface OnFinishedListener {
            void onFinished(InscanNegativeStatusResponseNew inscanNegativeStatusResponseNew);

            void onFailure(Throwable t);
        }

        void inspectionNegativeStatusRequest(OnFinishedListener onFinishedListener, String token, InspectionNegativeStatusRequestNew inspectionNegativeStatusRequestNew);
    }

    interface  InspectionNegativeSubmitInteractor{
        interface OnFinishedListener {
            void onFinished(InspectionNegSubmitResponseNew inspectionNegSubmitResponseNew);

            void onFailure(Throwable t);
        }

        void inspectionNegativeStatusRequest(OnFinishedListener onFinishedListener, String token, InspectionNegSubmitRequestNew inspectionNegSubmitRequestNew);
    }
}
