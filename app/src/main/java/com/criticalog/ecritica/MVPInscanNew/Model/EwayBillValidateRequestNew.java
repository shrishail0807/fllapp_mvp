package com.criticalog.ecritica.MVPInscanNew.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EwayBillValidateRequestNew implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("docket_no")
    @Expose
    private String docket;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("scanned_ewaybill")
    @Expose
    private List<Long> scannedEwaybill = null;

    public String getDocket() {
        return docket;
    }

    public void setDocket(String docket) {
        this.docket = docket;
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Long> getScannedEwaybill() {
        return scannedEwaybill;
    }

    public void setScannedEwaybill(List<Long> scannedEwaybill) {
        this.scannedEwaybill = scannedEwaybill;
    }
   /* @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("scanned_ewaybill")
    @Expose
    private List<Long> scannedEwaybill = null;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Long> getScannedEwaybill() {
        return scannedEwaybill;
    }

    public void setScannedEwaybill(List<Long> scannedEwaybill) {
        this.scannedEwaybill = scannedEwaybill;
    }*/
}
