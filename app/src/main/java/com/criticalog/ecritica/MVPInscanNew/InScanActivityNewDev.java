package com.criticalog.ecritica.MVPInscanNew;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Adapters.InscanBoxAdapter;
import com.criticalog.ecritica.Adapters.RulesAdapterWithFlag;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDRS.DRSClose.DRSCloseActivity;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPInscan.InScanActivity;
import com.criticalog.ecritica.MVPInscan.InscanContract;
import com.criticalog.ecritica.MVPInscan.InscanInteractorImpl;
import com.criticalog.ecritica.MVPInscan.InscanPresenterImpl;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.ItemSwatch;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DatumInspectionNegStatusesNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwaybillScanNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InscanNegativeStatusResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegativeStatusRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ScannedBoxNew;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InScanActivityNewDev extends AppCompatActivity implements InscanContractNew.MainView, InscanContract.MainView {
    private Spinner spinner, noofpcs_spinner, spinnerSwatch;
    private EditText noofbox, destcity, destcontrolling, pudlocation, actualvalue, noofewaybill, connoteno, mL, mB, mH, mNetWt;
    private Button scanewaybill, scanbox, submit;
    private ImageView backbutton;
    private RecyclerView ewaybillrv, docketrv;
    private String pickupby = "", docketno, sourcepin, destinitionpin, box_no, ewaybill_no = "", docket, noofpcs;
    private OtpTextView originpin, destpin;
    private ArrayList scannedboxlist = new ArrayList();
    private ArrayList<ScannedBoxNew> scannedBoxesLBHListNew = new ArrayList<ScannedBoxNew>();
    private ArrayList scannedeWaybilllist = new ArrayList();
    private int no_of_box, no_of_ewaybill;
    private LinearLayout ewaybilllayout, resultlayout, rvlayout, ewaybilldetails, docketdetailslayout, inputlayou, progressbarlayout;
    InscanBoxAdapter adapter;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    ArrayList<EditText> myArray;
    private Dialog dialogManualEntry;
    private Dialog dialogReasonForNoBookingId;
    String autoBoxFlag = "";

    private LinearLayout conlayout;

    private TextView tv_scanconnote, tv_noofbox, singleconn, mControllingHub;
    private EditText et_box;

    private ImageButton scan_connote;
    InscanContractNew.presenter mPresenter;
    InscanContract.presenter mOldInscanPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token;
    private SimpleArcDialog mProgressBar;
    private EditText mETEwayBill;
    private TextView mTextEwayBillDetails, mBoxDetailText;
    private RelativeLayout mQrScan, mRLManualConnote;
    private Button mBoxQRScan, mEwayBillQRScan;
    private RestServices mRestServices;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude = 0, longitude = 0;
    private ImageButton scan_connote1;
    List<ChecklistScan> checklistScansListNew;
    private Dialog dialogCheckListWithFlag;

    List<DatumInspectionNegStatusesNew> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    private String reasonCode = "";
    private LinearLayout mBoxDetailsLinLay;
    String length;
    String breadth;
    String height;
    String netWeight;
    private LinearLayout mBeforeAfterCutOffLay;
    private String funMessage = "", autobox = "", partCode = "", partDescr = "";
    private String reasonMessage = "";
    private String clientCode = "";
    RadioGroup radioGroup;
    String cutOff = "";
    Dialog bookingIdMandateDialog;
    private LinearLayout mSwatchLay;
    private ArrayList<String> partDescriptionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscan_new_dev);
        ButterKnife.bind(this);
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();
        inintviews();
        mOldInscanPresenter = new InscanPresenterImpl(InScanActivityNewDev.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            int id;

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.mBefore:
                        cutOff = "0";
                        Toast.makeText(InScanActivityNewDev.this, "Before", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.mAfter:
                        cutOff = "1";
                        Toast.makeText(InScanActivityNewDev.this, "After", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });


    }

    public void bookingIdMandateDialog(String message) {

        //Dialog Manual Entry
        TextView mDialogText;
        TextView mOkDialog;
        bookingIdMandateDialog = new Dialog(InScanActivityNewDev.this);
        bookingIdMandateDialog.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = bookingIdMandateDialog.findViewById(R.id.mOk);
        mDialogText = bookingIdMandateDialog.findViewById(R.id.mDialogText);
        mDialogText.setText(message);
        bookingIdMandateDialog.setCancelable(false);
        bookingIdMandateDialog.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingIdMandateDialog.dismiss();
                recreate();
            }
        });
    }

    public boolean validateNetWeight(String num) {
        if (num.matches("(^([0-9]{0,5})?)(\\.[0-9]{0,3})?$")) {
            Toast.makeText(this, "Valid", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(InScanActivityNewDev.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(InScanActivityNewDev.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(InScanActivityNewDev.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(InScanActivityNewDev.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.e("LAT LONG_INSCAN", String.valueOf(latitude) + "" + String.valueOf(longitude));
                } else {
                    Log.e("LAT LONG_INSCAN", String.valueOf(latitude) + "" + String.valueOf(longitude));

                }
            });

        }
    }

    public void callEwayBillVlaidate(String ewayBillNo) {
        ArrayList ewayBillNum = new ArrayList();
        ewayBillNum.add(ewayBillNo);

        EwayBillValidateRequestNew ewayBillValidateRequestNew = new EwayBillValidateRequestNew();
        ewayBillValidateRequestNew.setAction("validate_ewaybill_with_doc_portal");
        ewayBillValidateRequestNew.setUserId(userId);
        ewayBillValidateRequestNew.setDocket(connoteno.getText().toString());
        ewayBillValidateRequestNew.setScannedEwaybill(ewayBillNum);
        mPresenter.ewayBillValidate(ewayBillValidateRequestNew);
    }

    public void callInscanBoxCheck(String DOCKET) {
        InScanBoxRequestNew inScanBoxRequestNew = new InScanBoxRequestNew();
        inScanBoxRequestNew.setAction("inscanbox_number");
        inScanBoxRequestNew.setUserId(userId);
        inScanBoxRequestNew.setBoxNumber(DOCKET);
        inScanBoxRequestNew.setLatitude(String.valueOf(latitude));
        inScanBoxRequestNew.setLongitude(String.valueOf(longitude));

        mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
        mPresenter.inscanBoxNumberValidate(token, inScanBoxRequestNew);
    }

    public void callInscanDocketCheck(String connote) {

        DocketCheckRequestNew docketCheckRequestNew = new DocketCheckRequestNew();
        docketCheckRequestNew.setAction("inscan_docket_check_new");
        docketCheckRequestNew.setPickupby(pickupby);
        docketCheckRequestNew.setConnote(connote);
        docketCheckRequestNew.setUserId(userId);
        docketCheckRequestNew.setLatitude(String.valueOf(latitude));
        docketCheckRequestNew.setLongitude(String.valueOf(longitude));
        mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
        mPresenter.inScanDocketCheck(token, docketCheckRequestNew);
    }


    public void manualDialogBookingIdMandatory() {
        EditText mEnterReason;
        TextView mOk;
        //Dialog Manual Entry
        dialogReasonForNoBookingId = new Dialog(InScanActivityNewDev.this);
        dialogReasonForNoBookingId.setContentView(R.layout.dialog_manula_scan_new);
        dialogReasonForNoBookingId.setCanceledOnTouchOutside(false);
        dialogReasonForNoBookingId.setCancelable(false);

        mEnterReason = dialogReasonForNoBookingId.findViewById(R.id.mEnterReason);
        mOk = dialogReasonForNoBookingId.findViewById(R.id.mOk);


        dialogReasonForNoBookingId.show();


        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = mEnterReason.getText().toString();
                if (!reason.equalsIgnoreCase("")) {
                    reasonMessage = reason;
                    dialogReasonForNoBookingId.dismiss();
                    //  callInscanDocketCheck(docketno);
                } else {
                    Toasty.warning(InScanActivityNewDev.this, "Enter Valid Reason!!", Toast.LENGTH_LONG, true).show();
                }
            }
        });


    }

    public void manualDialog() {
        EditText mDocketEntry;
        TextView mAddDocket;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(InScanActivityNewDev.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_scan);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);

        mDocketEntry = dialogManualEntry.findViewById(R.id.mDocketEntry);
        mAddDocket = dialogManualEntry.findViewById(R.id.mAddDocket);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mDocketEntry.setHint("Enter Connote Number");
        mAddDocket.setText("Check");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });


        mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketno = mDocketEntry.getText().toString();

                if (docketno.length() == 9) {
                    callInscanDocketCheck(docketno);
                } else {
                    Toasty.warning(InScanActivityNewDev.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });


    }


    private void inintviews() {
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        spinner = findViewById(R.id.pickupbyspinner);
        noofpcs_spinner = findViewById(R.id.spinner);
        connoteno = findViewById(R.id.connoteno);
        noofbox = findViewById(R.id.boxno);
        originpin = findViewById(R.id.originpin);
        destpin = findViewById(R.id.destpin);
        destcity = findViewById(R.id.destcity);
        destcontrolling = findViewById(R.id.destcontrolling);
        pudlocation = findViewById(R.id.pudlocation);
        actualvalue = findViewById(R.id.actualvalue);
        noofewaybill = findViewById(R.id.ewaybill);
        scanewaybill = findViewById(R.id.ewaybillscan);
        scanbox = findViewById(R.id.boxscan);
        ewaybilllayout = findViewById(R.id.ewaybilllayout);
        resultlayout = findViewById(R.id.resultlayout);
        rvlayout = findViewById(R.id.rvlayout);
        ewaybilldetails = findViewById(R.id.ewaybilldetails);
        submit = findViewById(R.id.submit);
        docketdetailslayout = findViewById(R.id.docketdetailslayout);
        backbutton = findViewById(R.id.xtv_backbutton);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        //   inputlayou = findViewById(R.id.inputlayou);
        scan_connote = findViewById(R.id.scan_connote);
        //  progressbarlayout = findViewById(R.id.llProgressBar);

        conlayout = findViewById(R.id.conlayout);
        tv_scanconnote = findViewById(R.id.tv_scancon);

        tv_noofbox = findViewById(R.id.textview10);
        et_box = findViewById(R.id.et_box);

        mBeforeAfterCutOffLay = findViewById(R.id.mBeforeAfterCutOffLay);


        ewaybillrv = findViewById(R.id.ewaybilllist);
        docketrv = findViewById(R.id.boxlist);
        singleconn = findViewById(R.id.singleconn);
        mETEwayBill = findViewById(R.id.mETEwayBill);
        mTextEwayBillDetails = findViewById(R.id.mTextEwayBillDetails);
        mQrScan = findViewById(R.id.mQrScan);
        mBoxDetailText = findViewById(R.id.mBoxDetailText);
        mBoxQRScan = findViewById(R.id.mBoxQRScan);
        mEwayBillQRScan = findViewById(R.id.mEwayBillQRScan);
        mRLManualConnote = findViewById(R.id.mRLManualConnote);
        scan_connote1 = findViewById(R.id.scan_connote1);
        mBoxDetailsLinLay = findViewById(R.id.mBoxDetailsLinLay);
        mControllingHub = findViewById(R.id.mControllingHub);
        mL = findViewById(R.id.mL);
        mB = findViewById(R.id.mB);
        mH = findViewById(R.id.mH);
        mNetWt = findViewById(R.id.mNetWeight);
        mSwatchLay = findViewById(R.id.mSwatchLay);
        spinnerSwatch = findViewById(R.id.spinnerSwatch);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        docketrv.addItemDecoration(decoration);
        docketrv.setLayoutManager(layoutManager);


        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        DividerItemDecoration decoration1 = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        ewaybillrv.addItemDecoration(decoration1);
        ewaybillrv.setLayoutManager(layoutManager1);

        mEwayBillQRScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("scan_what", "ewaybill");
                ewaybill_no = noofewaybill.getText().toString().trim();
                criticalogSharedPreferences.saveData("scanwhat", "ewaybill");
                if (ewaybill_no.equalsIgnoreCase("") || ewaybill_no.equalsIgnoreCase("0")) {
                    Toasty.warning(InScanActivityNewDev.this, "Please enter valid  no of eWaybill", Toast.LENGTH_LONG, true).show();
                } else {
                    no_of_ewaybill = Integer.parseInt(ewaybill_no);
                    myArray = new ArrayList();

                    startBarcodeScan();
                }

            }
        });
        scan_connote1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannedboxlist.clear();
                scannedeWaybilllist.clear();
                mCriticalogSharedPreferences.saveData("scan_what", "connote");
                startBarcodeScan();
            }
        });

        mBoxQRScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("scan_what", "box");
                et_box.setVisibility(View.VISIBLE);
                mBoxDetailsLinLay.setVisibility(View.VISIBLE);
                requestFocus(mL);
                startBarcodeScan();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pickupby = parent.getItemAtPosition(position).toString();
                scannedboxlist.clear();
                scannedeWaybilllist.clear();
                resultlayout.setVisibility(View.GONE);
                scanbox.setVisibility(View.GONE);
                //  mBoxQRScan.setVisibility(View.GONE);
                connoteno.setText("");
                originpin.setOTP("");
                destpin.setOTP("");
                destcontrolling.setText("");
                pudlocation.setText("");
                actualvalue.setText("");
                noofbox.setText("");
                noofewaybill.setText("");
                destcity.setText("");
                scanewaybill.setVisibility(View.GONE);
                rvlayout.setVisibility(View.GONE);
                et_box.setVisibility(View.GONE);
                mBoxQRScan.setVisibility(View.GONE);
                mEwayBillQRScan.setVisibility(View.GONE);
                submit.setVisibility(View.GONE);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        scan_connote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criticalogSharedPreferences.saveData("scanwhat", "connote");
                scannedboxlist.clear();
                scannedeWaybilllist.clear();

                manualDialog();
                // startBarcodeScan();
                //code for TVS scanner
                //scanThroughTvscanner();

            }
        });
        scanbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criticalogSharedPreferences.saveData("scanwhat", "box");
                //  startBarcodeScan();
                //code changes for TVS scanner
                scan_BoxThroughTvsScanner();


            }
        });

        scanewaybill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ewaybill_no = noofewaybill.getText().toString().trim();
                criticalogSharedPreferences.saveData("scanwhat", "ewaybill");
                if (ewaybill_no.equalsIgnoreCase("") || ewaybill_no.equalsIgnoreCase("0")) {
                    Toasty.warning(InScanActivityNewDev.this, "Please enter valid  no of eWaybill", Toast.LENGTH_LONG, true).show();
                } else {
                    no_of_ewaybill = Integer.parseInt(ewaybill_no);
                    myArray = new ArrayList();
                }

                if (ewaybill_no.equalsIgnoreCase("") || ewaybill_no.matches("") || ewaybill_no.length() == 0) {

                } else if (no_of_ewaybill == 0) {

                } else {

                    mETEwayBill.setVisibility(View.VISIBLE);

                    if (scannedboxlist.size() == 0) {
                        Toasty.warning(InScanActivityNewDev.this, "First Enter Box Details!!", Toast.LENGTH_SHORT, true).show();
                    }

                    mETEwayBill.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            String ewayBillNo = s.toString();

                            if (ewayBillNo.length() == 12) {
                                // progressbarlayout.setVisibility(View.VISIBLE);
                                if (scannedeWaybilllist.size() == Integer.parseInt(ewaybill_no)) {
                                    Toasty.success(InScanActivityNewDev.this, "All Scanned", Toast.LENGTH_LONG, true).show();
                                } else {
                                    if (scannedeWaybilllist.contains(ewayBillNo)) {
                                        //  Toasty.warning(InScanActivity.this,"Eway Bill already present!..enter new one", Toast.LENGTH_LONG, true).show();
                                    } else {
                                        callEwayBillVlaidate(ewayBillNo);
                                        //     Toasty.success(InScanActivity.this,"Eway Bill " + String.valueOf(scannedeWaybilllist.size()) + " of " + ewaybill_no + " Scanned", Toast.LENGTH_LONG, true).show();
                                        mETEwayBill.setText("");
                                        mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                        ewaybilldetails.setVisibility(View.VISIBLE);
                                        adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivityNewDev.this);
                                        ewaybillrv.setAdapter(adapter);
                                    }
                                }
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    // scanewaybill.setVisibility(View.GONE);

                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scannedboxlist.size() != no_of_box) {
                    Toasty.warning(InScanActivityNewDev.this, "please scan box no", Toast.LENGTH_LONG, true).show();

                } else if (ewaybilllayout.getVisibility() == View.VISIBLE) {


                    if (scannedeWaybilllist.size() == no_of_ewaybill) {
                        // progressbarlayout.setVisibility(View.VISIBLE);

                        if (no_of_ewaybill == 0) {
                            Toasty.warning(InScanActivityNewDev.this, "Enter Eway Bill Details!!", Toast.LENGTH_LONG, true).show();
                        } else {
                            if (autoBoxFlag.equalsIgnoreCase("1")) {
                                if (cutOff.equalsIgnoreCase("")) {
                                    Toasty.warning(InScanActivityNewDev.this, "Select Before and After Cut Off!!", Toast.LENGTH_LONG, true).show();
                                } else {
                                    callInScanSubmit();
                                }
                            } else {
                                callInScanSubmit();
                            }
                        }

                    } else {
                        Toasty.warning(InScanActivityNewDev.this, "Enter all eway bill number!!", Toast.LENGTH_LONG, true).show();
                    }

                } else {
                    if (autoBoxFlag.equalsIgnoreCase("1")) {
                        if (cutOff.equalsIgnoreCase("")) {
                            Toasty.warning(InScanActivityNewDev.this, "Select Before and After Cut Off!!", Toast.LENGTH_LONG, true).show();
                        } else {
                            callInScanSubmit();
                        }
                    } else {
                        callInScanSubmit();
                    }
                }
            }
        });


        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(new Intent(InScanActivity.this, HomeActivity.class));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                finish();
            }
        });

        et_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 9) {

                    if (!scannedboxlist.contains(s.toString())) {
                        Log.d("inside if", scannedboxlist.toString());
                        docket = s.toString();

                        length = mL.getText().toString();
                        breadth = mB.getText().toString();
                        height = mH.getText().toString();
                        netWeight = mNetWt.getText().toString();

                        if (length.equalsIgnoreCase("") || breadth.equalsIgnoreCase("") || height.equalsIgnoreCase("") || netWeight.equalsIgnoreCase("")) {
                            Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H and Net Weight", Toast.LENGTH_SHORT).show();
                        } else if (length.equalsIgnoreCase("0") || breadth.equalsIgnoreCase("0") || height.equalsIgnoreCase("0") || netWeight.equalsIgnoreCase("0")) {
                            Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                        } else if (length.equalsIgnoreCase("00") || breadth.equalsIgnoreCase("00") || height.equalsIgnoreCase("00") || netWeight.equalsIgnoreCase("00")) {
                            Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                        } else if (length.equalsIgnoreCase("000") || breadth.equalsIgnoreCase("000") || height.equalsIgnoreCase("000") || netWeight.equalsIgnoreCase("000")) {
                            Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                        } else {

                            if (autobox.equalsIgnoreCase("2")) {
                                callInscanBoxCheck(docket);
                            } else {
                                boolean status = validateNetWeight(mNetWt.getText().toString());

                                if (status == true) {
                                    callInscanBoxCheck(docket);
                                } else {
                                    Toast.makeText(InScanActivityNewDev.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                    mNetWt.getText().clear();
                                }
                            }
                           /* boolean status = validateNetWeight(mNetWt.getText().toString());

                            if (status == true) {
                                callInscanBoxCheck(docket);
                            } else {
                                Toast.makeText(InScanActivityNewDev.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                mNetWt.getText().clear();
                            }*/

                        }
                        //   progressbarlayout.setVisibility(View.VISIBLE);


                    } else {
                        Toasty.warning(InScanActivityNewDev.this, "Box already scanned", Toast.LENGTH_LONG, true).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startBarcodeScan();
                            }
                        }, 2000);

                        //changes for TVSscanner

                        scan_BoxThroughTvsScanner();
                    }

                }
            }
        });
    }

    private void scanThroughTvscanner() {

        conlayout.setVisibility(View.VISIBLE);
        requestFocus(connoteno);

    }

    private void scan_BoxThroughTvsScanner() {

        if (autobox.equalsIgnoreCase("2")) {

            mSwatchLay.setVisibility(View.VISIBLE);
            mL.setFocusable(false);
            mB.setFocusable(false);
            mH.setFocusable(false);
            mNetWt.setFocusable(false);


            spinnerSwatch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    partDescr = parent.getItemAtPosition(position).toString();
                    partCode = partDescriptionList.get(position).toString();
                    // Toast.makeText(InScanActivityNewDev.this, partCode + "  " + partDescr, Toast.LENGTH_SHORT).show();

                    if (!partCode.equalsIgnoreCase("Select part code")) {
                        InscanGetItemDetailsRequest inscanGetItemDetailsRequest = new InscanGetItemDetailsRequest();
                        inscanGetItemDetailsRequest.setAction("inscan_get_item_details");
                        inscanGetItemDetailsRequest.setUserId(userId);
                        inscanGetItemDetailsRequest.setPartCode(partCode);
                        mOldInscanPresenter.inscanItemsDetaislRequest(inscanGetItemDetailsRequest);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        et_box.setVisibility(View.VISIBLE);
        mBoxDetailsLinLay.setVisibility(View.VISIBLE);
        requestFocus(mL);
        scanbox.setVisibility(View.GONE);

    }

    private void scan_BoxThroughTvsScannerEway() {
        // et_box.setVisibility(View.VISIBLE);
        requestFocus(et_box);
        //scanbox.setVisibility(View.GONE);
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void callInScanSubmit() {
        if (noofpcs.equalsIgnoreCase("Select Box Type")) {
            Toast.makeText(this, "Select Box Type", Toast.LENGTH_SHORT).show();
        } else {
            //  Toast.makeText(this, "Call Inscan Submit", Toast.LENGTH_SHORT).show();
            ArrayList<ScannedBoxNew> scannedBoxesLBHList1New = scannedBoxesLBHListNew;
            InScanSubmitRequestNew inScanSubmitRequestNew = new InScanSubmitRequestNew();
            inScanSubmitRequestNew.setAction("inscan_submit_with_doc_new");
            inScanSubmitRequestNew.setActualValue(actualvalue.getText().toString());
            inScanSubmitRequestNew.setConnoteno(connoteno.getText().toString());
            inScanSubmitRequestNew.setDestPin(destpin.getOTP());
            inScanSubmitRequestNew.setNoofpcs(noofpcs);
            inScanSubmitRequestNew.setScannedBox(scannedBoxesLBHList1New);
            inScanSubmitRequestNew.setOriPin(originpin.getOTP());
            inScanSubmitRequestNew.setScannedEwaybill(scannedeWaybilllist);
            inScanSubmitRequestNew.setUserId(userId);
            inScanSubmitRequestNew.setPickupby(pickupby);
            inScanSubmitRequestNew.setLatitude(String.valueOf(latitude));
            inScanSubmitRequestNew.setLongitude(String.valueOf(longitude));
            inScanSubmitRequestNew.setBooking_msg(funMessage);
            inScanSubmitRequestNew.setPick_up_reason_temp(reasonMessage);
            inScanSubmitRequestNew.setCut_off(cutOff);
            inScanSubmitRequestNew.setAuto_box(autobox);


            mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
            mPresenter.inScanSubmit(token, inScanSubmitRequestNew);
        }
    }

    public void userRiderLog() {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;

        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo("");
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId("");
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(connoteno.getText().toString());
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("10");
        riderLogRequest.setmLastKm("");
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("3");
        riderLogRequest.setActionNumber(10);
        riderLogRequest.setOsVersion(androidOS);
        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String scanwhat = mCriticalogSharedPreferences.getData("scan_what");
        if (result != null) {
            try {
                String MobilePattern = "[0-9]{9}";
                docketno = result.getContents();
                docket = result.getContents();

                if (scanwhat.equalsIgnoreCase("connote")) {
                    if (docketno.length() == 9) {
                        callInscanDocketCheck(docketno);

                    } else {
                        resultlayout.setVisibility(View.GONE);
                        scanbox.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        Toasty.warning(InScanActivityNewDev.this, "Scan Valid Connote", Toast.LENGTH_LONG, true).show();
                    }
                } else if (scanwhat.equalsIgnoreCase("box")) {
                    if (docketno.length() == 9) {

                        if (!scannedboxlist.contains(docketno)) {

                            length = mL.getText().toString();
                            breadth = mB.getText().toString();
                            height = mH.getText().toString();
                            netWeight = mNetWt.getText().toString();


                            if (length.equalsIgnoreCase("") || breadth.equalsIgnoreCase("") || height.equalsIgnoreCase("") || netWeight.equalsIgnoreCase("")) {
                                Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H and Net Weight", Toast.LENGTH_SHORT).show();
                            } else if (length.equalsIgnoreCase("0") || breadth.equalsIgnoreCase("0") || height.equalsIgnoreCase("0") || netWeight.equalsIgnoreCase("0")) {
                                Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                            } else if (length.equalsIgnoreCase("00") || breadth.equalsIgnoreCase("00") || height.equalsIgnoreCase("00") || netWeight.equalsIgnoreCase("00")) {
                                Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                            } else if (length.equalsIgnoreCase("000") || breadth.equalsIgnoreCase("000") || height.equalsIgnoreCase("000") || netWeight.equalsIgnoreCase("000")) {
                                Toast.makeText(InScanActivityNewDev.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                            } else {
                                if (autobox.equalsIgnoreCase("2")) {
                                    callInscanBoxCheck(docket);
                                } else {
                                    boolean status = validateNetWeight(mNetWt.getText().toString());

                                    if (status == true) {
                                        callInscanBoxCheck(docket);
                                    } else {
                                        Toast.makeText(InScanActivityNewDev.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                        mNetWt.getText().clear();
                                    }
                                }

                                /*boolean status = validateNetWeight(mNetWt.getText().toString());

                                if (status == true) {
                                    callInscanBoxCheck(docket);
                                } else {
                                    Toast.makeText(InScanActivityNewDev.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                    mNetWt.getText().clear();
                                }*/

                                // callInscanBoxCheck(docket);
                            }
                            //   progressbarlayout.setVisibility(View.VISIBLE);
                            //  callInscanBoxCheck(docketno);

                        } else {

                            Toasty.warning(InScanActivityNewDev.this, "Box already scanned!!", Toast.LENGTH_LONG, true).show();
                            // startBarcodeScan();
                        }

                    } else {
                        Toasty.warning(InScanActivityNewDev.this, "Scan Valid Box Number!!", Toast.LENGTH_LONG, true).show();

                    }
                } else {
                    if (docketno.length() == 12) {

                        if (!scannedboxlist.contains(docketno)) {

                            //   progressbarlayout.setVisibility(View.VISIBLE);
                            callEwayBillVlaidate(docketno);

                        } else {
                            Toasty.warning(InScanActivityNewDev.this, "Eway bill already scanned", Toast.LENGTH_LONG, true).show();
                            // startBarcodeScan();
                        }

                    } else {
                        Toasty.warning(InScanActivityNewDev.this, "Scan Valid Ewaybill Number!!", Toast.LENGTH_LONG, true).show();

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                //that means the encoded format not matches
                //in this case you can display whatever data is available on the qrcode

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setDocketCheckDataToViews(DocketCheckResponse docketCheckResponse) {

    }

    @Override
    public void setOriginPincodeResponse(OriginPincodeValidateResponse originPincodeResponse) {

    }

    @Override
    public void setActualValueInscanResponse(ActualValueInscanResponse actualValueInscanResponse) {

    }

    @Override
    public void setInscanBoxDataToViews(InScanBoxResponse inScanBoxResponse) {

    }

    @Override
    public void setInScanSubmitDataToViews(InScanSubmitResponse inScanSubmitResponse) {

    }

    @Override
    public void setEwayBillValidateResponseToViews(EwayBillValidateResponse ewayBillValidateResponse) {

    }

    @Override
    public void inspectionDoneResponseToData(InspectionDoneResponse inspectionDoneResponse) {

    }

    @Override
    public void inspectionNegativeStatusData(InscanNegativeStatusResponse inspectionNegativeStatusResponse) {

    }

    @Override
    public void inspectionNegSubmitDataToViews(InspectionNegSubmitResponse inspectionNegSubmitResponse) {

    }

    @Override
    public void inscanItemDetailsResponseDataToVews(InscanGetItemDetailsResponse inscanGetItemDetailsResponse) {
        Toast.makeText(this, inscanGetItemDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
        if (inscanGetItemDetailsResponse.getStatus() == 200) {
            mL.setText(inscanGetItemDetailsResponse.getData().getMdimlength());
            mB.setText(inscanGetItemDetailsResponse.getData().getMdimbreadth());
            mH.setText(inscanGetItemDetailsResponse.getData().getMdimheight());
            mNetWt.setText(inscanGetItemDetailsResponse.getData().getXnetwt());
        } else {
            mL.setText("");
            mB.setText("");
            mH.setText("");
            mNetWt.setText("");
        }
    }

    public void dialogCheckListWithFlag(List<ChecklistScan> prsRules) {
        TextView mDone, mFailed;
        RecyclerView mRvRules;
        RulesAdapterWithFlag rulesAdapter;
        CheckBox mCheckYes, mCheckNo;
        dialogCheckListWithFlag = new Dialog(InScanActivityNewDev.this);
        dialogCheckListWithFlag.setContentView(R.layout.dialog_rule_with_flag);
        Window window = dialogCheckListWithFlag.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mDone = dialogCheckListWithFlag.findViewById(R.id.mDone);
        mFailed = dialogCheckListWithFlag.findViewById(R.id.mFailed);
        mRvRules = dialogCheckListWithFlag.findViewById(R.id.mRvRules);

        dialogCheckListWithFlag.setCancelable(false);
        rulesAdapter = new RulesAdapterWithFlag(prsRules);
        mRvRules.setLayoutManager(new LinearLayoutManager(this));
        mRvRules.setAdapter(rulesAdapter);

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InspectionDoneRequestNew inspectionDoneRequestNew = new InspectionDoneRequestNew();
                inspectionDoneRequestNew.setAction("inspection_done");
                inspectionDoneRequestNew.setUserId(userId);
                inspectionDoneRequestNew.setDocketNo(connoteno.getText().toString());
                inspectionDoneRequestNew.setLatitude(String.valueOf(latitude));
                inspectionDoneRequestNew.setLatitude(String.valueOf(longitude));

                mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                mPresenter.inspectionDoneRequest(inspectionDoneRequestNew);
                dialogCheckListWithFlag.dismiss();
            }
        });

        mFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reasonList.clear();
                reasons.clear();
                InspectionNegativeStatusRequestNew inspectionNegativeStatusRequestNew = new InspectionNegativeStatusRequestNew();
                inspectionNegativeStatusRequestNew.setAction("negative_status_inspection");
                inspectionNegativeStatusRequestNew.setUserId(userId);
                inspectionNegativeStatusRequestNew.setLatitude(String.valueOf(latitude));
                inspectionNegativeStatusRequestNew.setLongitude(String.valueOf(longitude));

                mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                mPresenter.inspectionNegativeStatuse(inspectionNegativeStatusRequestNew);

            }
        });
        dialogCheckListWithFlag.show();

    }

    @Override
    public void setDocketCheckDataToViews(DocketCheckResponseNew docketCheckResponseNew) {
        if (docketCheckResponseNew.getStatus() == 200) {
            autobox = docketCheckResponseNew.getData().get(0).getAutobox();
            if (autobox.equalsIgnoreCase("2")) {
                ArrayList list = new ArrayList();
                List<ItemSwatch> itemSwatchList = docketCheckResponseNew.getData().get(0).getItems();
                for (int i = 0; i < itemSwatchList.size(); i++) {
                    list.add(itemSwatchList.get(i).getPartDesc());
                    partDescriptionList.add(itemSwatchList.get(i).getPartCode());
                }
                ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerSwatch.setAdapter(aa);
            }
            resultlayout.setVisibility(View.VISIBLE);
            pickupby = docketCheckResponseNew.getData().get(0).getPickupby();
            autoBoxFlag = docketCheckResponseNew.getData().get(0).getAutobox_flag();
            funMessage = docketCheckResponseNew.getData().get(0).getFun_msg();
            clientCode = docketCheckResponseNew.getData().get(0).getClient_code();
           /* if (funMessage.equalsIgnoreCase("Boooking Id Mandatory")) {
                Toast.makeText(this, funMessage, Toast.LENGTH_SHORT).show();
                manualDialogBookingIdMandatory();
            }*/

            if (pickupby.equalsIgnoreCase("")) {
                if (docketCheckResponseNew.getMessage().equalsIgnoreCase("ValidManual")) {

                    resultlayout.setVisibility(View.VISIBLE);
                    ewaybilllayout.setVisibility(View.GONE);
                    tv_noofbox.setVisibility(View.GONE);
                    noofbox.setVisibility(View.GONE);

                    connoteno.setText(docketno);

                    originpin.setOTP("");
                    destpin.setOTP("");
                    destcity.setText("");
                    destcontrolling.setText("");
                    pudlocation.setText("");
                    actualvalue.setText("");
                    noofbox.setText("");
                    noofbox.setEnabled(true);
                    scanbox.setVisibility(View.GONE);
                    // actualvalue.setEnabled(true);
                    //  mBoxQRScan.setVisibility(View.GONE);
                    //   mBoxDetailText.setVisibility(View.GONE);
                    docketrv.setVisibility(View.GONE);
                    //submit.setVisibility(View.GONE);
                    scanewaybill.setVisibility(View.GONE);
                    mTextEwayBillDetails.setVisibility(View.GONE);
                    mEwayBillQRScan.setVisibility(View.GONE);


                    originpin.setSaveEnabled(true);
                    destpin.setSaveEnabled(true);
                    pudlocation.setSaveEnabled(true);
                    destcontrolling.setSaveEnabled(true);
                    destcity.setSaveEnabled(true);
                    actualvalue.setSaveEnabled(true);
                    originpin.setFocusable(true);
                    destpin.setFocusable(true);
                    pudlocation.setFocusable(true);
                    destcontrolling.setFocusable(true);
                    destcity.setFocusable(true);


                    originpin.setOtpListener(new OTPListener() {
                        @Override
                        public void onInteractionListener() {

                        }

                        @Override
                        public void onOTPComplete(String otp) {
                            sourcepin = originpin.getOTP();
                            //    progressbarlayout.setVisibility(View.VISIBLE);
                            mCriticalogSharedPreferences.saveData("origin_destination", "origin");


                            OriginPincodeValidateRequestNew originPincodeValidateRequestNew = new OriginPincodeValidateRequestNew();

                            originPincodeValidateRequestNew.setPincode(sourcepin);
                            originPincodeValidateRequestNew.setAction("inscan_pincode_validation_new");
                            originPincodeValidateRequestNew.setUserId(userId);
                            originPincodeValidateRequestNew.setLatitude(String.valueOf(latitude));
                            originPincodeValidateRequestNew.setLongitude(String.valueOf(longitude));


                            mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                            mPresenter.validateOriginPicode(token, originPincodeValidateRequestNew);
                            //  new InscanOriginpincodevalidationvolley(InscanActivity.this, sourcepin, InscanActivity.this);

                        }
                    });


                    destpin.setOtpListener(new OTPListener() {
                        @Override
                        public void onInteractionListener() {
                            destcity.setText("");
                            destcontrolling.setText("");
                            pudlocation.setText("");

                        }

                        @Override
                        public void onOTPComplete(String otp) {
                            destinitionpin = destpin.getOTP();
                            //   progressbarlayout.setVisibility(View.VISIBLE);
                            mCriticalogSharedPreferences.saveData("origin_destination", "destination");
                            OriginPincodeValidateRequestNew originPincodeValidateRequestNew = new OriginPincodeValidateRequestNew();

                            originPincodeValidateRequestNew.setPincode(destinitionpin);
                            originPincodeValidateRequestNew.setAction("inscan_pincode_validation_new");
                            originPincodeValidateRequestNew.setUserId(userId);
                            originPincodeValidateRequestNew.setLatitude(String.valueOf(latitude));
                            originPincodeValidateRequestNew.setLongitude(String.valueOf(longitude));

                            mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                            mPresenter.validateOriginPicode(token, originPincodeValidateRequestNew);
                            //   new IInscandestpincodeValidationvolley(InscanActivity.this, destinitionpin, InscanActivity.this);

                        }
                    });

                    actualvalue.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            Log.d("inside", "ontextchane" + s);
                            String amount = s.toString();
                            if (amount.length() > 0) {
                                double amounttoint = Double.parseDouble(amount);


                                if (amounttoint == 0) {
                                    Toasty.warning(InScanActivityNewDev.this, "Actual value should be greater than 0", Toast.LENGTH_LONG, true).show();
                                } else if (amounttoint >= 50000) {
                                   /* ewaybilllayout.setVisibility(View.VISIBLE);
                                    ewaybilldetails.setVisibility(View.VISIBLE);
                                    tv_noofbox.setVisibility(View.VISIBLE);*/
                                    // noofbox.setVisibility(View.VISIBLE);

                                    //   progressbarlayout.setVisibility(View.VISIBLE);


                                    ActualValueInscanRequestNew actualValueInscanRequest = new ActualValueInscanRequestNew();
                                    actualValueInscanRequest.setAction("inscan_actualvalue_new");
                                    actualValueInscanRequest.setClientCode(docketCheckResponseNew.getData().get(0).getClient_code());
                                    actualValueInscanRequest.setActualVal(String.valueOf(amount));
                                    actualValueInscanRequest.setUserId(userId);
                                    actualValueInscanRequest.setOrigin(sourcepin);
                                    actualValueInscanRequest.setDest(destinitionpin);
                                    actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                                    actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                                    mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                                    mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);

                                    // new InScanActualValueVolley(InscanActivity.this, InscanActivity.this, sourcepin, destinitionpin, amount);
                                    noofbox.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            box_no = noofbox.getText().toString().trim();
                                            criticalogSharedPreferences.saveData("scanwhat", "box");
                                            if (!(box_no.matches("")))
                                                no_of_box = Integer.parseInt(box_no);

                                            if (box_no.equalsIgnoreCase("") || box_no.matches("") || box_no.length() == 0) {
                                                Toasty.warning(InScanActivityNewDev.this, "Please enter valid  box no", Toast.LENGTH_LONG, true).show();

                                            } else if (no_of_box == 0) {
                                                Toasty.warning(InScanActivityNewDev.this, "Box no should be greater than 0", Toast.LENGTH_LONG, true).show();

                                            } else {

                                                scanbox.setVisibility(View.VISIBLE);
                                                noofbox.setVisibility(View.VISIBLE);
                                                // mBoxQRScan.setVisibility(View.VISIBLE);
                                                //mBoxQRScan.setVisibility(View.VISIBLE);
                                                noofpcs = box_no;


                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {

                                        }
                                    });
                                } else {
                                    ewaybilllayout.setVisibility(View.GONE);
                                    scanewaybill.setVisibility(View.GONE);
                                    mEwayBillQRScan.setVisibility(View.GONE);
                                    ewaybilldetails.setVisibility(View.GONE);
                                    tv_noofbox.setVisibility(View.VISIBLE);
                                    noofbox.setVisibility(View.VISIBLE);
                                    //  mBoxQRScan.setVisibility(View.VISIBLE);

                                    noofbox.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            box_no = noofbox.getText().toString().trim();

                                            criticalogSharedPreferences.saveData("scanwhat", "box");
                                            if (!(box_no.matches("")))
                                                no_of_box = Integer.parseInt(box_no);
                                            if (box_no.equalsIgnoreCase("") || box_no.matches("") || box_no.length() == 0) {
                                                Toasty.warning(InScanActivityNewDev.this, "Please enter valid  box no", Toast.LENGTH_LONG, true).show();

                                            } else if (no_of_box == 0) {
                                                Toasty.warning(InScanActivityNewDev.this, "Box no should be greater than 0", Toast.LENGTH_LONG, true).show();

                                            } else {
                                                mBoxDetailText.setVisibility(View.VISIBLE);
                                                scanbox.setVisibility(View.VISIBLE);
                                                mBoxQRScan.setVisibility(View.VISIBLE);
                                                noofpcs = String.valueOf(box_no);
                                                Log.d("inside", "third party valid manual actual value <50000" + noofpcs);
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {

                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                }
            }
            if (pickupby.equalsIgnoreCase("RGSA")) {
                pickupby = "RGSA";
            }
            if (dialogManualEntry != null) {
                dialogManualEntry.dismiss();
            }

            connoteno.setEnabled(false);
            connoteno.setText(docketno);
            if (autoBoxFlag.equalsIgnoreCase("1")) {
                mBeforeAfterCutOffLay.setVisibility(View.VISIBLE);
            } else {
                mBeforeAfterCutOffLay.setVisibility(View.GONE);
            }

            if (pickupby.equalsIgnoreCase("OWN")) {


                if (docketCheckResponseNew.getStatus() == 200) {
                    resultlayout.setVisibility(View.VISIBLE);
                    tv_noofbox.setVisibility(View.VISIBLE);
                    //   mBoxDetailText.setVisibility(View.GONE);
                    docketrv.setVisibility(View.GONE);
                    submit.setVisibility(View.GONE);

                    noofbox.setText(String.valueOf(docketCheckResponseNew.getData().get(0).getNoOfPcs()));
                    no_of_box = docketCheckResponseNew.getData().get(0).getNoOfPcs();
                    int noOfPcsFlag = docketCheckResponseNew.getData().get(0).getFlagNoOfPcs();

                    //  tv_noofbox.setVisibility(View.VISIBLE);
                    noofbox.setVisibility(View.VISIBLE);

                    checklistScansListNew = docketCheckResponseNew.getData().get(0).getChecklistScan();
                    if (checklistScansListNew != null) {
                        if (checklistScansListNew.size() > 0) {
                            dialogCheckListWithFlag(checklistScansListNew);
                            // Toast.makeText(InScanActivity.this, docketCheckResponse.getData().get(0).getChecklistScan().get(0).getRule(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (noOfPcsFlag == 1) {
                        ArrayList list = new ArrayList();
                        List jsonArray = docketCheckResponseNew.getData().get(0).getTabnoOfPcs();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            list.add(jsonArray.get(i));
                        }
                        noofpcs_spinner.setVisibility(View.VISIBLE);
                        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        noofpcs_spinner.setAdapter(aa);

                        noofpcs_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                noofpcs = parent.getItemAtPosition(position).toString();
                                Log.d("inside", " own  if flag 1" + noofpcs);
                                Toast.makeText(InScanActivityNewDev.this, noofpcs, Toast.LENGTH_SHORT).show();
                                rvlayout.setVisibility(View.VISIBLE);
                                docketdetailslayout.setVisibility(View.VISIBLE);

                                try {
                                    singleconn.setText(docketCheckResponseNew.getData().get(0).getDocketno());

                                    if (!scannedboxlist.contains(docketCheckResponseNew.getData().get(0).getDocketno()))
                                        scannedboxlist.add(docketCheckResponseNew.getData().get(0).getDocketno());
                                    scannedBoxesLBHListNew.add(new ScannedBoxNew(docketCheckResponseNew.getData().get(0).getDocketno(), "", "", "", "", partCode, partDescr));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                singleconn.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                rvlayout.setVisibility(View.VISIBLE);

                                docketdetailslayout.setVisibility(View.VISIBLE);

                                try {
                                    singleconn.setText(docketCheckResponseNew.getData().get(0).getDocketno());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                singleconn.setVisibility(View.VISIBLE);
                            }
                        });

                        submit.setVisibility(View.VISIBLE);
                    } else if (noOfPcsFlag == 0) {
                        noofbox.setText(String.valueOf(docketCheckResponseNew.getData().get(0).getNoOfPcs()));
                        scanbox.setVisibility(View.VISIBLE);
                        mBoxQRScan.setVisibility(View.VISIBLE);
                        noofbox.setVisibility(View.VISIBLE);
                        noofbox.requestFocus();
                        noofpcs = noofbox.getText().toString();

                        Log.d("inside", " own else if flag 0" + noofpcs);

                    }


                    //replacing done here
                    originpin.setOTP(docketCheckResponseNew.getData().get(0).getOrgPin());
                    destpin.setOTP(docketCheckResponseNew.getData().get(0).getDestPin());
                    actualvalue.setText(String.valueOf(docketCheckResponseNew.getData().get(0).getActualVal()));

                    String amount = docketCheckResponseNew.getData().get(0).getActualVal().toString();

                    if (amount.length() > 0) {
                        double amounttoint = Double.parseDouble(amount);


                        if (amounttoint == 0) {
                            Toasty.warning(InScanActivityNewDev.this, "Actual value should be greater than 0", Toast.LENGTH_LONG, true).show();
                        } else if (amounttoint >= 50000) {
                            ewaybilllayout.setVisibility(View.VISIBLE);
                            ewaybilldetails.setVisibility(View.VISIBLE);
                            tv_noofbox.setVisibility(View.VISIBLE);
                            // noofbox.setVisibility(View.VISIBLE);

                            //   progressbarlayout.setVisibility(View.VISIBLE);


                            ActualValueInscanRequestNew actualValueInscanRequest = new ActualValueInscanRequestNew();
                            actualValueInscanRequest.setAction("inscan_actualvalue_new");
                            actualValueInscanRequest.setClientCode(clientCode);
                            actualValueInscanRequest.setActualVal(String.valueOf(amount));
                            actualValueInscanRequest.setUserId(userId);
                            actualValueInscanRequest.setOrigin(docketCheckResponseNew.getData().get(0).getOrgPin());
                            actualValueInscanRequest.setDest(docketCheckResponseNew.getData().get(0).getDestPin());
                            actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                            actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                            mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                            mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);
                        }
                    }

                    pudlocation.setText(docketCheckResponseNew.getData().get(0).getPudLoc());
                    destcontrolling.setText(docketCheckResponseNew.getData().get(0).getHubLoc());
                    destcity.setText(docketCheckResponseNew.getData().get(0).getCityName());

                    originpin.setEnabled(false);
                    destpin.setEnabled(false);
                    actualvalue.setEnabled(true);

                    noofbox.setEnabled(false);
                    if (docketCheckResponseNew.getData().get(0).getEwaybill().equalsIgnoreCase("WBDLMH")) {
                        //display input for eway bill no and ewaybillscan
                        ewaybilllayout.setVisibility(View.VISIBLE);
                        scanewaybill.setVisibility(View.VISIBLE);
                        mEwayBillQRScan.setVisibility(View.VISIBLE);
                        ewaybilldetails.setVisibility(View.VISIBLE);

                        List<EwaybillScanNew> ewaybillScanNewList = docketCheckResponseNew.getData().get(0).getEwaybillScan();

                        if (ewaybillScanNewList != null) {
                            if (ewaybillScanNewList.size() > 0) {

                                for (int k = 0; k < ewaybillScanNewList.size(); k++) {
                                    scannedeWaybilllist.add(ewaybillScanNewList.get(k).getXwaybillno());
                                }
                                no_of_ewaybill = ewaybillScanNewList.size();
                                noofewaybill.setText(String.valueOf(ewaybillScanNewList.size()));
                                noofewaybill.setEnabled(false);

                                mETEwayBill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);

                                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                ewaybillrv.setVisibility(View.VISIBLE);
                                rvlayout.setVisibility(View.VISIBLE);

                                scan_BoxThroughTvsScannerEway();
                                docketdetailslayout.setVisibility(View.VISIBLE);
                                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivityNewDev.this);
                                ewaybillrv.setAdapter(adapter);

                                //Toast.makeText(this, "ENABLE EWAY", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        ewaybilllayout.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        ewaybilldetails.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);

                        List<EwaybillScanNew> ewaybillScanNewList = docketCheckResponseNew.getData().get(0).getEwaybillScan();

                        if (ewaybillScanNewList != null) {

                            if (ewaybillScanNewList.size() > 0) {
                                ewaybilllayout.setVisibility(View.VISIBLE);
                                scanewaybill.setVisibility(View.VISIBLE);
                                mEwayBillQRScan.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                for (int k = 0; k < ewaybillScanNewList.size(); k++) {
                                    scannedeWaybilllist.add(ewaybillScanNewList.get(k).getXwaybillno());
                                }
                                no_of_ewaybill = ewaybillScanNewList.size();
                                noofewaybill.setText(String.valueOf(ewaybillScanNewList.size()));
                                noofewaybill.setEnabled(false);

                                mETEwayBill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);

                                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                ewaybillrv.setVisibility(View.VISIBLE);
                                rvlayout.setVisibility(View.VISIBLE);

                                scan_BoxThroughTvsScannerEway();
                                docketdetailslayout.setVisibility(View.VISIBLE);
                                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivityNewDev.this);
                                ewaybillrv.setAdapter(adapter);

                                //Toast.makeText(this, "ENABLE EWAY", Toast.LENGTH_SHORT).show();
                            } else {
                                ewaybilllayout.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                ewaybilldetails.setVisibility(View.GONE);
                            }
                        }

                    }


                } else {
                    Toasty.warning(InScanActivityNewDev.this, docketCheckResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();

                    scan_connote.setVisibility(View.VISIBLE);
                    tv_scanconnote.setVisibility(View.VISIBLE);
                    resultlayout.setVisibility(View.GONE);
                    scanbox.setVisibility(View.GONE);
                    //  mBoxQRScan.setVisibility(View.GONE);
                    scanewaybill.setVisibility(View.GONE);
                    mEwayBillQRScan.setVisibility(View.GONE);
                }

            } else if (pickupby.equalsIgnoreCase("THIRDPARTY") || pickupby.equalsIgnoreCase("RGSA")) {
                if (docketCheckResponseNew.getStatus() == 200) {

                    if (docketCheckResponseNew.getMessage().equalsIgnoreCase("validCitAll")) {
                        List<EwaybillScanNew> ewaybillScanNewList1 = docketCheckResponseNew.getData().get(0).getEwaybillScan();

                        if (ewaybillScanNewList1 != null) {
                            if (ewaybillScanNewList1.size() > 0) {

                                ewaybilllayout.setVisibility(View.VISIBLE);
                                for (int k = 0; k < ewaybillScanNewList1.size(); k++) {
                                    scannedeWaybilllist.add(ewaybillScanNewList1.get(k).getXwaybillno());
                                }
                                no_of_ewaybill = ewaybillScanNewList1.size();
                                noofewaybill.setText(String.valueOf(ewaybillScanNewList1.size()));
                                noofewaybill.setEnabled(false);

                                mETEwayBill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);

                                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                ewaybillrv.setVisibility(View.VISIBLE);
                                rvlayout.setVisibility(View.VISIBLE);

                                scan_BoxThroughTvsScannerEway();
                                docketdetailslayout.setVisibility(View.VISIBLE);
                                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivityNewDev.this);
                                ewaybillrv.setAdapter(adapter);

                                //Toast.makeText(this, "ENABLE EWAY", Toast.LENGTH_SHORT).show();
                            }
                        }
                        resultlayout.setVisibility(View.VISIBLE);
                        //  mBoxDetailText.setVisibility(View.GONE);
                        docketrv.setVisibility(View.GONE);
                        //    submit.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);

                        originpin.setOTP(docketCheckResponseNew.getData().get(0).getOrgPin());
                        destpin.setOTP(docketCheckResponseNew.getData().get(0).getDestPin());
                        actualvalue.setText(String.valueOf(docketCheckResponseNew.getData().get(0).getActualVal()));
                        pudlocation.setText(docketCheckResponseNew.getData().get(0).getPudLoc());
                        destcontrolling.setText(docketCheckResponseNew.getData().get(0).getHubLoc());
                        destcity.setText(docketCheckResponseNew.getData().get(0).getCityName());

                        tv_noofbox.setVisibility(View.VISIBLE);
                        noofbox.setVisibility(View.VISIBLE);

                        //code changes tart here
                        int noOfPcsFlag = docketCheckResponseNew.getData().get(0).getFlagNoOfPcs();

                        no_of_box = docketCheckResponseNew.getData().get(0).getNoOfPcs();
                        noofbox.setText(String.valueOf(no_of_box));

                        if (noOfPcsFlag == 1) {
                            ArrayList list = new ArrayList();
                            List jsonArray = docketCheckResponseNew.getData().get(0).getTabnoOfPcs();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                list.add(jsonArray.get(i));
                            }
                            noofpcs_spinner.setVisibility(View.VISIBLE);

                            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                            noofpcs_spinner.setAdapter(aa);

                            if (!docketCheckResponseNew.getData().get(0).getDocketno().equalsIgnoreCase("")) {
                                scannedboxlist.add(docketCheckResponseNew.getData().get(0).getDocketno());
                                scannedBoxesLBHListNew.add(new ScannedBoxNew(docketCheckResponseNew.getData().get(0).getDocketno(), "", "", "", "", partCode, partDescr));
                            }

                            noofpcs_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    noofpcs = (String) parent.getItemAtPosition(position);
                                    Log.d("inside", "third party valid cit flag 1" + noofpcs);

                                    rvlayout.setVisibility(View.VISIBLE);

                                    docketdetailslayout.setVisibility(View.VISIBLE);

                                    try {
                                        singleconn.setText(docketCheckResponseNew.getData().get(0).getDocketno());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    singleconn.setVisibility(View.VISIBLE);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {


                                    rvlayout.setVisibility(View.VISIBLE);

                                    docketdetailslayout.setVisibility(View.VISIBLE);

                                    try {
                                        singleconn.setText(docketCheckResponseNew.getData().get(0).getDocketno());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    singleconn.setVisibility(View.VISIBLE);
                                }
                            });
                            submit.setVisibility(View.VISIBLE);

                        } else if (noOfPcsFlag == 0) {

                            noofbox.setVisibility(View.VISIBLE);
                            noofbox.setText(docketCheckResponseNew.getData().get(0).getNoOfPcs().toString());
                            scanbox.setVisibility(View.VISIBLE);
                            mBoxQRScan.setVisibility(View.VISIBLE);
                            noofbox.requestFocus();

                            noofpcs = docketCheckResponseNew.getData().get(0).getNoOfPcs().toString();

                            Log.d("inside", "third party valid cit flag 0" + noofpcs);
                        }


                        if (docketCheckResponseNew.getData().get(0).getEwaybill().equalsIgnoreCase("WBDLMH")) {
                            //display input for eway bill no and ewaybillscan
                            ewaybilllayout.setVisibility(View.VISIBLE);
                            scanewaybill.setVisibility(View.VISIBLE);
                            mEwayBillQRScan.setVisibility(View.VISIBLE);
                            ewaybilldetails.setVisibility(View.VISIBLE);
                        } else {
                            ewaybilllayout.setVisibility(View.GONE);
                            scanewaybill.setVisibility(View.GONE);
                            ewaybilldetails.setVisibility(View.GONE);
                            mEwayBillQRScan.setVisibility(View.GONE);
                        }

                    }

                    if (docketCheckResponseNew.getMessage().equalsIgnoreCase("ValidManual")) {

                        resultlayout.setVisibility(View.VISIBLE);
                        ewaybilllayout.setVisibility(View.GONE);
                        tv_noofbox.setVisibility(View.GONE);
                        noofbox.setVisibility(View.GONE);


                        connoteno.setText(docketno);

                        originpin.setOTP("");
                        destpin.setOTP("");
                        destcity.setText("");
                        destcontrolling.setText("");
                        pudlocation.setText("");
                        actualvalue.setText("");
                        noofbox.setText("");
                        noofbox.setEnabled(true);
                        scanbox.setVisibility(View.GONE);
                        // actualvalue.setEnabled(true);
                        //  mBoxQRScan.setVisibility(View.GONE);
                        //   mBoxDetailText.setVisibility(View.GONE);
                        docketrv.setVisibility(View.GONE);
                        //submit.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        mTextEwayBillDetails.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);


                        originpin.setSaveEnabled(true);
                        destpin.setSaveEnabled(true);
                        pudlocation.setSaveEnabled(true);
                        destcontrolling.setSaveEnabled(true);
                        destcity.setSaveEnabled(true);
                        actualvalue.setSaveEnabled(true);
                        originpin.setFocusable(true);
                        destpin.setFocusable(true);
                        pudlocation.setFocusable(true);
                        destcontrolling.setFocusable(true);
                        destcity.setFocusable(true);


                        originpin.setOtpListener(new OTPListener() {
                            @Override
                            public void onInteractionListener() {

                            }

                            @Override
                            public void onOTPComplete(String otp) {
                                sourcepin = originpin.getOTP();
                                //    progressbarlayout.setVisibility(View.VISIBLE);
                                mCriticalogSharedPreferences.saveData("origin_destination", "origin");


                                OriginPincodeValidateRequestNew originPincodeValidateRequestNew = new OriginPincodeValidateRequestNew();

                                originPincodeValidateRequestNew.setPincode(sourcepin);
                                originPincodeValidateRequestNew.setAction("inscan_pincode_validation_new");
                                originPincodeValidateRequestNew.setUserId(userId);
                                originPincodeValidateRequestNew.setLatitude(String.valueOf(latitude));
                                originPincodeValidateRequestNew.setLongitude(String.valueOf(longitude));


                                mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                                mPresenter.validateOriginPicode(token, originPincodeValidateRequestNew);
                                //  new InscanOriginpincodevalidationvolley(InscanActivity.this, sourcepin, InscanActivity.this);

                            }
                        });


                        destpin.setOtpListener(new OTPListener() {
                            @Override
                            public void onInteractionListener() {
                                destcity.setText("");
                                destcontrolling.setText("");
                                pudlocation.setText("");

                            }

                            @Override
                            public void onOTPComplete(String otp) {
                                destinitionpin = destpin.getOTP();
                                //   progressbarlayout.setVisibility(View.VISIBLE);
                                mCriticalogSharedPreferences.saveData("origin_destination", "destination");
                                OriginPincodeValidateRequestNew originPincodeValidateRequestNew = new OriginPincodeValidateRequestNew();

                                originPincodeValidateRequestNew.setPincode(destinitionpin);
                                originPincodeValidateRequestNew.setAction("inscan_pincode_validation_new");
                                originPincodeValidateRequestNew.setUserId(userId);
                                originPincodeValidateRequestNew.setLatitude(String.valueOf(latitude));
                                originPincodeValidateRequestNew.setLongitude(String.valueOf(longitude));

                                mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                                mPresenter.validateOriginPicode(token, originPincodeValidateRequestNew);
                                //   new IInscandestpincodeValidationvolley(InscanActivity.this, destinitionpin, InscanActivity.this);

                            }
                        });

                        actualvalue.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                                Log.d("inside", "ontextchane" + s);
                                String amount = s.toString();
                                if (amount.length() > 0) {
                                    double amounttoint = Double.parseDouble(amount);


                                    if (amounttoint == 0) {
                                        Toasty.warning(InScanActivityNewDev.this, "Actual value should be greater than 0", Toast.LENGTH_LONG, true).show();
                                    } else if (amounttoint >= 50000) {
                                        ewaybilllayout.setVisibility(View.VISIBLE);
                                        ewaybilldetails.setVisibility(View.VISIBLE);
                                        tv_noofbox.setVisibility(View.VISIBLE);
                                        // noofbox.setVisibility(View.VISIBLE);

                                        //   progressbarlayout.setVisibility(View.VISIBLE);


                                        ActualValueInscanRequestNew actualValueInscanRequest = new ActualValueInscanRequestNew();
                                        actualValueInscanRequest.setAction("inscan_actualvalue_new");
                                        actualValueInscanRequest.setClientCode(clientCode);
                                        actualValueInscanRequest.setActualVal(String.valueOf(amount));
                                        actualValueInscanRequest.setUserId(userId);
                                        actualValueInscanRequest.setOrigin(sourcepin);
                                        actualValueInscanRequest.setDest(destinitionpin);
                                        actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                                        actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                                        mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                                        mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);

                                        // new InScanActualValueVolley(InscanActivity.this, InscanActivity.this, sourcepin, destinitionpin, amount);
                                        noofbox.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                box_no = noofbox.getText().toString().trim();
                                                criticalogSharedPreferences.saveData("scanwhat", "box");
                                                if (!(box_no.matches("")))
                                                    no_of_box = Integer.parseInt(box_no);

                                                if (box_no.equalsIgnoreCase("") || box_no.matches("") || box_no.length() == 0) {
                                                    Toasty.warning(InScanActivityNewDev.this, "Please enter valid  box no", Toast.LENGTH_LONG, true).show();

                                                } else if (no_of_box == 0) {
                                                    Toasty.warning(InScanActivityNewDev.this, "Box no should be greater than 0", Toast.LENGTH_LONG, true).show();

                                                } else {

                                                    scanbox.setVisibility(View.VISIBLE);
                                                    noofbox.setVisibility(View.VISIBLE);
                                                    // mBoxQRScan.setVisibility(View.VISIBLE);
                                                    //mBoxQRScan.setVisibility(View.VISIBLE);
                                                    noofpcs = box_no;


                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                    } else {
                                        ewaybilllayout.setVisibility(View.GONE);
                                        scanewaybill.setVisibility(View.GONE);
                                        mEwayBillQRScan.setVisibility(View.GONE);
                                        ewaybilldetails.setVisibility(View.GONE);
                                        tv_noofbox.setVisibility(View.VISIBLE);
                                        noofbox.setVisibility(View.VISIBLE);
                                        //  mBoxQRScan.setVisibility(View.VISIBLE);

                                        noofbox.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                box_no = noofbox.getText().toString().trim();

                                                criticalogSharedPreferences.saveData("scanwhat", "box");
                                                if (!(box_no.matches("")))
                                                    no_of_box = Integer.parseInt(box_no);
                                                if (box_no.equalsIgnoreCase("") || box_no.matches("") || box_no.length() == 0) {
                                                    Toasty.warning(InScanActivityNewDev.this, "Please enter valid  box no", Toast.LENGTH_LONG, true).show();

                                                } else if (no_of_box == 0) {
                                                    Toasty.warning(InScanActivityNewDev.this, "Box no should be greater than 0", Toast.LENGTH_LONG, true).show();

                                                } else {
                                                    mBoxDetailText.setVisibility(View.VISIBLE);
                                                    scanbox.setVisibility(View.VISIBLE);
                                                    mBoxQRScan.setVisibility(View.VISIBLE);
                                                    noofpcs = String.valueOf(box_no);
                                                    Log.d("inside", "third party valid manual actual value <50000" + noofpcs);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                    }
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });

                    } else if (docketCheckResponseNew.getMessage().equalsIgnoreCase("validCitAll")) {
                        List<EwaybillScanNew> ewaybillScanNewList1 = docketCheckResponseNew.getData().get(0).getEwaybillScan();

                        if (ewaybillScanNewList1 != null) {
                            if (ewaybillScanNewList1.size() > 0) {

                                ewaybilllayout.setVisibility(View.VISIBLE);
                                for (int k = 0; k < ewaybillScanNewList1.size(); k++) {
                                    scannedeWaybilllist.add(ewaybillScanNewList1.get(k).getXwaybillno());
                                }
                                no_of_ewaybill = ewaybillScanNewList1.size();
                                noofewaybill.setText(String.valueOf(ewaybillScanNewList1.size()));
                                noofewaybill.setEnabled(false);

                                mETEwayBill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);

                                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                ewaybillrv.setVisibility(View.VISIBLE);
                                rvlayout.setVisibility(View.VISIBLE);

                                scan_BoxThroughTvsScannerEway();
                                docketdetailslayout.setVisibility(View.VISIBLE);
                                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivityNewDev.this);
                                ewaybillrv.setAdapter(adapter);

                                //Toast.makeText(this, "ENABLE EWAY", Toast.LENGTH_SHORT).show();
                            }
                        }
                        resultlayout.setVisibility(View.VISIBLE);
                        //  mBoxDetailText.setVisibility(View.GONE);
                        docketrv.setVisibility(View.GONE);
                        //    submit.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);

                        originpin.setOTP(docketCheckResponseNew.getData().get(0).getOrgPin());
                        destpin.setOTP(docketCheckResponseNew.getData().get(0).getDestPin());
                        actualvalue.setText(String.valueOf(docketCheckResponseNew.getData().get(0).getActualVal()));
                        pudlocation.setText(docketCheckResponseNew.getData().get(0).getPudLoc());
                        destcontrolling.setText(docketCheckResponseNew.getData().get(0).getHubLoc());
                        destcity.setText(docketCheckResponseNew.getData().get(0).getCityName());

                        tv_noofbox.setVisibility(View.VISIBLE);
                        noofbox.setVisibility(View.VISIBLE);

                        //code changes tart here
                        int noOfPcsFlag = docketCheckResponseNew.getData().get(0).getFlagNoOfPcs();

                        no_of_box = docketCheckResponseNew.getData().get(0).getNoOfPcs();
                        noofbox.setText(String.valueOf(no_of_box));

                        if (noOfPcsFlag == 1) {
                            ArrayList list = new ArrayList();
                            List jsonArray = docketCheckResponseNew.getData().get(0).getTabnoOfPcs();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                list.add(jsonArray.get(i));
                            }
                            noofpcs_spinner.setVisibility(View.VISIBLE);

                            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                            noofpcs_spinner.setAdapter(aa);

                            if (!docketCheckResponseNew.getData().get(0).getDocketno().equalsIgnoreCase("")) {
                                scannedboxlist.add(docketCheckResponseNew.getData().get(0).getDocketno());
                                scannedBoxesLBHListNew.add(new ScannedBoxNew(docketCheckResponseNew.getData().get(0).getDocketno(), "", "", "", "", partCode, partDescr));
                            }


                            noofpcs_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    noofpcs = (String) parent.getItemAtPosition(position);
                                    Log.d("inside", "third party valid cit flag 1" + noofpcs);

                                    rvlayout.setVisibility(View.VISIBLE);

                                    docketdetailslayout.setVisibility(View.VISIBLE);

                                    try {
                                        singleconn.setText(docketCheckResponseNew.getData().get(0).getDocketno());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    singleconn.setVisibility(View.VISIBLE);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {


                                    rvlayout.setVisibility(View.VISIBLE);

                                    docketdetailslayout.setVisibility(View.VISIBLE);

                                    try {
                                        singleconn.setText(docketCheckResponseNew.getData().get(0).getDocketno());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    singleconn.setVisibility(View.VISIBLE);
                                }
                            });
                            submit.setVisibility(View.VISIBLE);

                        } else if (noOfPcsFlag == 0) {

                            noofbox.setVisibility(View.VISIBLE);
                            noofbox.setText(docketCheckResponseNew.getData().get(0).getNoOfPcs().toString());
                            scanbox.setVisibility(View.VISIBLE);
                            mBoxQRScan.setVisibility(View.VISIBLE);
                            noofbox.requestFocus();

                            noofpcs = docketCheckResponseNew.getData().get(0).getNoOfPcs().toString();

                            Log.d("inside", "third party valid cit flag 0" + noofpcs);
                        }


                        if (docketCheckResponseNew.getData().get(0).getEwaybill().equalsIgnoreCase("WBDLMH")) {
                            //display input for eway bill no and ewaybillscan
                            ewaybilllayout.setVisibility(View.VISIBLE);
                            scanewaybill.setVisibility(View.VISIBLE);
                            mEwayBillQRScan.setVisibility(View.VISIBLE);
                            ewaybilldetails.setVisibility(View.VISIBLE);
                        } else {
                            ewaybilllayout.setVisibility(View.GONE);
                            scanewaybill.setVisibility(View.GONE);
                            ewaybilldetails.setVisibility(View.GONE);
                            mEwayBillQRScan.setVisibility(View.GONE);
                        }

                    }

                } else {
                    Toasty.warning(InScanActivityNewDev.this, docketCheckResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();

                    scan_connote.setVisibility(View.VISIBLE);
                    tv_scanconnote.setVisibility(View.VISIBLE);
                    // conlayout.setVisibility(View.GONE);
                    resultlayout.setVisibility(View.GONE);

                }
            }

        } else {
            scan_connote.setVisibility(View.VISIBLE);
            tv_scanconnote.setVisibility(View.VISIBLE);
            // conlayout.setVisibility(View.GONE);
            resultlayout.setVisibility(View.GONE);
            docketdetailslayout.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            scanbox.setVisibility(View.GONE);
            // mBoxQRScan.setVisibility(View.GONE);
            scanewaybill.setVisibility(View.GONE);
            mEwayBillQRScan.setVisibility(View.GONE);
            resultlayout.setVisibility(View.GONE);
            Toasty.warning(InScanActivityNewDev.this, docketCheckResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();

        }
    }

    @Override
    public void setOriginPincodeResponse(OriginPincodeValidateResponseNew originPincodeResponse) {

        String originDestination = mCriticalogSharedPreferences.getData("origin_destination");


        if (originDestination.equalsIgnoreCase("origin")) {
            if (originPincodeResponse.getStatus() != 200) {

                Toasty.warning(InScanActivityNewDev.this, originPincodeResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                originpin.setOTP("");
            } else {
                pickupby = originPincodeResponse.getData().get(0).getPickupby();
                Toast.makeText(this, pickupby, Toast.LENGTH_SHORT).show();
                if (!pickupby.equalsIgnoreCase("OWN")) {
                    if (funMessage.equalsIgnoreCase("Boooking Id Mandatory")) {
                        Toast.makeText(this, funMessage, Toast.LENGTH_SHORT).show();
                        manualDialogBookingIdMandatory();
                    }
                } else {
                    bookingIdMandateDialog("Booking ID is Mandatory for OWN Pickup Type");
                    Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            if (originPincodeResponse.getStatus() == 200) {
                pudlocation.setText(originPincodeResponse.getData().get(0).getPudLoc());
                destcontrolling.setText(originPincodeResponse.getData().get(0).getHubLoc());
                destcity.setText(originPincodeResponse.getData().get(0).getCityname());
                mControllingHub.setVisibility(View.VISIBLE);
                String controllingHubText = "Hub Code: " + originPincodeResponse.getData().get(0).getControlling_hub().toString() + " - " + "Dest City: " + originPincodeResponse.getData().get(0).getCity_name().toString();
                mControllingHub.setText(Html.fromHtml(controllingHubText));
            } else {
                Toasty.warning(InScanActivityNewDev.this, originPincodeResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                destpin.setOTP("");
                mControllingHub.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setActualValueInscanResponse(ActualValueInscanResponseNew actualValueInscanResponseNew) {
       /* if (actualValueInscanResponseNew.getStatus() == 200) {
            //display input for eway bill no and ewaybillscan
            ewaybilllayout.setVisibility(View.VISIBLE);
            scanewaybill.setVisibility(View.VISIBLE);
            mEwayBillQRScan.setVisibility(View.VISIBLE);
        }*/
        if(actualValueInscanResponseNew.getMessage().equalsIgnoreCase("Eway bill required"))
        {
            ewaybilllayout.setVisibility(View.VISIBLE);
            scanewaybill.setVisibility(View.VISIBLE);
            mEwayBillQRScan.setVisibility(View.VISIBLE);
        }else if(actualValueInscanResponseNew.getMessage().equalsIgnoreCase("Eway bill not required")){
            ewaybilllayout.setVisibility(View.GONE);
            scanewaybill.setVisibility(View.GONE);
            mEwayBillQRScan.setVisibility(View.GONE);

        }else if(actualValueInscanResponseNew.getMessage().equalsIgnoreCase("Actual value cannot exceed")){
             actualvalue.setText("");
             actualvalue.setHint("Enter Actual Value");
        }
        Toast.makeText(this, actualValueInscanResponseNew.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setInscanBoxDataToViews(InScanBoxResponseNew inScanBoxResponseNew) {

        if (inScanBoxResponseNew.getStatus() == 200) {
            scanbox.setVisibility(View.GONE);
            //  mBoxQRScan.setVisibility(View.GONE);
            et_box.setVisibility(View.GONE);
            mBoxDetailsLinLay.setVisibility(View.GONE);
            et_box.setText("");


            if (!scannedboxlist.contains(docket)) {
                scannedboxlist.add(docket);
                scannedBoxesLBHListNew.add(new ScannedBoxNew(docket, netWeight, length, breadth, height, partCode, partDescr));
                mL.getText().clear();
                mB.getText().clear();
                mH.getText().clear();
                mNetWt.getText().clear();
            }


            rvlayout.setVisibility(View.VISIBLE);

            docketdetailslayout.setVisibility(View.VISIBLE);
            docketrv.setVisibility(View.VISIBLE);
            adapter = new InscanBoxAdapter(scannedboxlist, InScanActivityNewDev.this);
            docketrv.setAdapter(adapter);


            if (no_of_box == scannedboxlist.size()) {
                Toasty.success(InScanActivityNewDev.this, scannedboxlist.size() + "/" + no_of_box + "scanned", Toast.LENGTH_LONG, true).show();
                submit.setVisibility(View.VISIBLE);
                mBoxQRScan.setVisibility(View.GONE);
                et_box.setVisibility(View.GONE);
                mBoxDetailsLinLay.setVisibility(View.GONE);
            } else {
                Toasty.success(InScanActivityNewDev.this, scannedboxlist.size() + "/" + no_of_box + "scanned", Toast.LENGTH_LONG, true).show();

                scan_BoxThroughTvsScanner();


            }
        } else {
            scanbox.setVisibility(View.VISIBLE);
            //  mBoxQRScan.setVisibility(View.VISIBLE);
            Toasty.warning(InScanActivityNewDev.this, inScanBoxResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();
        }

    }

    @Override
    public void setInScanSubmitDataToViews(InScanSubmitResponseNew inScanSubmitResponseNew) {
        if (inScanSubmitResponseNew.getStatus() == 200) {
            Toasty.success(InScanActivityNewDev.this, inScanSubmitResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();

            startActivity(new Intent(InScanActivityNewDev.this, HomeActivity.class));

            userRiderLog();
        } else {
            Toasty.warning(InScanActivityNewDev.this, inScanSubmitResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void setEwayBillValidateResponseToViews(EwayBillValidateResponseNew ewayBillValidateResponseNew) {

        if (ewayBillValidateResponseNew.getStatus() == 200) {

            if (scannedeWaybilllist.contains(ewayBillValidateResponseNew.getData().get(0).getScannedEwaybill())) {
                Toasty.warning(InScanActivityNewDev.this, "Eway Bill already present!..enter new one", Toast.LENGTH_LONG, true).show();
            } else {
                scannedeWaybilllist.add(ewayBillValidateResponseNew.getData().get(0).getScannedEwaybill());
                Toasty.success(InScanActivityNewDev.this, "Eway Bill " + String.valueOf(scannedeWaybilllist.size()) + " of " + ewaybill_no + " Scanned", Toast.LENGTH_LONG, true).show();
                mETEwayBill.setText("");
                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                ewaybilldetails.setVisibility(View.VISIBLE);
                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivityNewDev.this);
                ewaybillrv.setAdapter(adapter);
                if (scannedeWaybilllist.size() == Integer.parseInt(ewaybill_no)) {
                    mETEwayBill.setVisibility(View.GONE);
                    mEwayBillQRScan.setVisibility(View.GONE);
                    scanewaybill.setVisibility(View.GONE);
                }
            }

        } else {
            Toasty.warning(InScanActivityNewDev.this, ewayBillValidateResponseNew.getMessage(), Toast.LENGTH_LONG, true).show();

        }

    }

    @Override
    public void inspectionDoneResponseToData(InspectionDoneResponseNew inspectionDoneResponseNew) {

        if (inspectionDoneResponseNew.getStatus() == 200) {
            Toasty.success(InScanActivityNewDev.this, inspectionDoneResponseNew.getMessage(), Toast.LENGTH_SHORT, true).show();
            dialogCheckListWithFlag.dismiss();
        } else {
            Toasty.warning(InScanActivityNewDev.this, inspectionDoneResponseNew.getMessage(), Toast.LENGTH_SHORT, true).show();
        }

    }

    @Override
    public void inspectionNegativeStatusData(InscanNegativeStatusResponseNew inspectionNegativeStatusResponse) {
        if (inspectionNegativeStatusResponse.getStatus() == 200) {

            reasons = inspectionNegativeStatusResponse.getData();

            for (int i = 0; i < reasons.size(); i++) {

                reasonList.add(reasons.get(i).getNegativeDesc());
            }

            showCancelDialog();
        } else {
            // mProgressBar.dismiss();
        }
    }

    @Override
    public void inspectionNegSubmitDataToViews(InspectionNegSubmitResponseNew inspectionNegSubmitResponseNew) {
        if (inspectionNegSubmitResponseNew.getStatus() == 200) {
            Toasty.success(InScanActivityNewDev.this, inspectionNegSubmitResponseNew.getMessage(), Toast.LENGTH_SHORT, true).show();
            dialogCheckListWithFlag.dismiss();
            //  mProgressBar.dismiss();
        } else {
            Toasty.success(InScanActivityNewDev.this, inspectionNegSubmitResponseNew.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    public void showCancelDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(InScanActivityNewDev.this)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                        reasonCode = reasons.get(which).getNegativeCode();

                        Toast.makeText(InScanActivityNewDev.this, reasonCode, Toast.LENGTH_SHORT).show();

                        InspectionNegSubmitRequestNew inspectionNegSubmitRequestNew = new InspectionNegSubmitRequestNew();
                        inspectionNegSubmitRequestNew.setAction("inspection_failed");
                        inspectionNegSubmitRequestNew.setDocketNo(connoteno.getText().toString());
                        inspectionNegSubmitRequestNew.setUserId(userId);
                        inspectionNegSubmitRequestNew.setNegStatusCode(reasonCode);
                        inspectionNegSubmitRequestNew.setLatitude(String.valueOf(latitude));
                        inspectionNegSubmitRequestNew.setLongitude(String.valueOf(longitude));

                        mPresenter = new InscanPresenterImplNew(InScanActivityNewDev.this, new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew(), new InscanInteractorImplNew());
                        mPresenter.inspectionNegativeSubmit(inspectionNegSubmitRequestNew);


                        return true; // allow selection
                    }
                })
                .show();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.warning(InScanActivityNewDev.this, R.string.re_try, Toast.LENGTH_LONG, true).show();
    }
}
