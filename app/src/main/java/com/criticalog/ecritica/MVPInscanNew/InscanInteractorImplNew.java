package com.criticalog.ecritica.MVPInscanNew;

import android.util.Log;

import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InscanNegativeStatusResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegativeStatusRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateResponseNew;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InscanInteractorImplNew implements InscanContractNew.DocketCheckIntractor, InscanContractNew.OriginPincodeValidateIntractor, InscanContractNew.ActualValueInscanIntractor, InscanContractNew.InScanBoxIntractor,
        InscanContractNew.InScanSubmitIntractor, InscanContractNew.EwayBillValidateIntractor, InscanContractNew.InspectionDoneInteractor, InscanContractNew.InspectionNegativeStatusInteractor, InscanContractNew.InspectionNegativeSubmitInteractor {
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void getDocketCheckSuccess(InscanContractNew.DocketCheckIntractor.OnFinishedListener onFinishedListener, String token, DocketCheckRequestNew docketCheckRequestNew) {
        Call<DocketCheckResponseNew> docketCheckResponseCall = services.inscanDocketCheckNew(docketCheckRequestNew);
        docketCheckResponseCall.enqueue(new Callback<DocketCheckResponseNew>() {
            @Override
            public void onResponse(Call<DocketCheckResponseNew> call, Response<DocketCheckResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DocketCheckResponseNew> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }


    @Override
    public void getOriginPincodeValidateSuccess(InscanContractNew.OriginPincodeValidateIntractor.OnFinishedListener onFinishedListener, String token, OriginPincodeValidateRequestNew originPincodeValidateRequestNew) {
        Call<OriginPincodeValidateResponseNew> originPincodeValidateResponseCall = services.originPincodeValidateNew(originPincodeValidateRequestNew);

        originPincodeValidateResponseCall.enqueue(new Callback<OriginPincodeValidateResponseNew>() {
            @Override
            public void onResponse(Call<OriginPincodeValidateResponseNew> call, Response<OriginPincodeValidateResponseNew> response) {
                 Log.e("ORIGIN",response.body().getCode().toString());
                onFinishedListener.onFinished(response.body());

            }

            @Override
            public void onFailure(Call<OriginPincodeValidateResponseNew> call, Throwable t) {
                Log.e("ORIGIN",t.toString());
                onFinishedListener.onFailure(t);

            }
        });
    }

    @Override
    public void getActualValueInscanRequest(InscanContractNew.ActualValueInscanIntractor.OnFinishedListener onFinishedListener, String token, ActualValueInscanRequestNew actualValueInscanRequest) {

        Call<ActualValueInscanResponseNew> actualValueInscanResponseCall=services.actualValueInscanNew(actualValueInscanRequest);
        actualValueInscanResponseCall.enqueue(new Callback<ActualValueInscanResponseNew>() {
            @Override
            public void onResponse(Call<ActualValueInscanResponseNew> call, Response<ActualValueInscanResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ActualValueInscanResponseNew> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getActualValueInscanRequest(InscanContractNew.InScanBoxIntractor.OnFinishedListener onFinishedListener, String token, InScanBoxRequestNew inScanBoxRequestNew) {

        Call<InScanBoxResponseNew> inScanBoxResponseCall=services.inScanBoxNew(inScanBoxRequestNew);
        inScanBoxResponseCall.enqueue(new Callback<InScanBoxResponseNew>() {
            @Override
            public void onResponse(Call<InScanBoxResponseNew> call, Response<InScanBoxResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InScanBoxResponseNew> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }


    @Override
    public void getInScanSubmitRequest(InscanContractNew.InScanSubmitIntractor.OnFinishedListener onFinishedListener, String token, InScanSubmitRequestNew inScanSubmitRequestNew) {

        Call<InScanSubmitResponseNew> inScanSubmitResponseCall=services.inScanSubmitNew(inScanSubmitRequestNew);
        inScanSubmitResponseCall.enqueue(new Callback<InScanSubmitResponseNew>() {
            @Override
            public void onResponse(Call<InScanSubmitResponseNew> call, Response<InScanSubmitResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InScanSubmitResponseNew> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getEwayBillRequest(InscanContractNew.EwayBillValidateIntractor.OnFinishedListener onFinishedListener, String token, EwayBillValidateRequestNew ewayBillValidateRequestNew) {
        Call<EwayBillValidateResponseNew> ewayBillValidateResponseCall=services.ewayBillValidate(ewayBillValidateRequestNew);
        ewayBillValidateResponseCall.enqueue(new Callback<EwayBillValidateResponseNew>() {
            @Override
            public void onResponse(Call<EwayBillValidateResponseNew> call, Response<EwayBillValidateResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<EwayBillValidateResponseNew> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void inspectionDoneRequest(InscanContractNew.InspectionDoneInteractor.OnFinishedListener onFinishedListener, String token, InspectionDoneRequestNew inspectionDoneRequestNew) {

        Call<InspectionDoneResponseNew> inspectionDoneResponseCall=services.inspectionDoneNew(inspectionDoneRequestNew);
        inspectionDoneResponseCall.enqueue(new Callback<InspectionDoneResponseNew>() {
            @Override
            public void onResponse(Call<InspectionDoneResponseNew> call, Response<InspectionDoneResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InspectionDoneResponseNew> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });


    }

    @Override
    public void inspectionNegativeStatusRequest(InscanContractNew.InspectionNegativeStatusInteractor.OnFinishedListener onFinishedListener, String token, InspectionNegativeStatusRequestNew inspectionNegativeStatusRequestNew) {

        Call<InscanNegativeStatusResponseNew> inscanNegativeStatusResponseCall=services.inspectionNegativeStatusNew(inspectionNegativeStatusRequestNew);

        inscanNegativeStatusResponseCall.enqueue(new Callback<InscanNegativeStatusResponseNew>() {
            @Override
            public void onResponse(Call<InscanNegativeStatusResponseNew> call, Response<InscanNegativeStatusResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InscanNegativeStatusResponseNew> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void inspectionNegativeStatusRequest(InscanContractNew.InspectionNegativeSubmitInteractor.OnFinishedListener onFinishedListener, String token, InspectionNegSubmitRequestNew inspectionNegSubmitRequestNew) {

        Call<InspectionNegSubmitResponseNew> inspectionNegSubmitResponseCall=services.inspectionNegativeSubmitNew(inspectionNegSubmitRequestNew);

        inspectionNegSubmitResponseCall.enqueue(new Callback<InspectionNegSubmitResponseNew>() {
            @Override
            public void onResponse(Call<InspectionNegSubmitResponseNew> call, Response<InspectionNegSubmitResponseNew> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InspectionNegSubmitResponseNew> call, Throwable t) {
             onFinishedListener.onFailure(t);
            }
        });
    }
}
