package com.criticalog.ecritica.MVPInscanNew.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class InScanSubmitRequestNew implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("pickupby")
    @Expose
    private String pickupby;
    @SerializedName("connoteno")
    @Expose
    private String connoteno;
    @SerializedName("ori_pin")
    @Expose
    private String oriPin;
    @SerializedName("dest_pin")
    @Expose
    private String destPin;
    @SerializedName("actual_value")
    @Expose
    private String actualValue;
    @SerializedName("noofpcs")
    @Expose
    private String noofpcs;

    @SerializedName("booking_msg")
    @Expose
    private String booking_msg;

    @SerializedName("pick_up_reason_temp")
    @Expose
    private String pick_up_reason_temp;

    @SerializedName("cut_off")
    @Expose
    private String cut_off;

    @SerializedName("auto_box")
    @Expose
    private String auto_box;

    public String getAuto_box() {
        return auto_box;
    }

    public void setAuto_box(String auto_box) {
        this.auto_box = auto_box;
    }

    public List<ScannedBoxNew> getScannedBoxNews() {
        return scannedBoxNews;
    }

    public void setScannedBoxNews(List<ScannedBoxNew> scannedBoxNews) {
        this.scannedBoxNews = scannedBoxNews;
    }

    public String getCut_off() {
        return cut_off;
    }

    public void setCut_off(String cut_off) {
        this.cut_off = cut_off;
    }

    public String getBooking_msg() {
        return booking_msg;
    }

    public void setBooking_msg(String booking_msg) {
        this.booking_msg = booking_msg;
    }

    public String getPick_up_reason_temp() {
        return pick_up_reason_temp;
    }

    public void setPick_up_reason_temp(String pick_up_reason_temp) {
        this.pick_up_reason_temp = pick_up_reason_temp;
    }

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("scanned_box")
    @Expose
    private List<ScannedBoxNew> scannedBoxNews = null;
    @SerializedName("scanned_ewaybill")
    @Expose
    private List<Integer> scannedEwaybill = null;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPickupby() {
        return pickupby;
    }

    public void setPickupby(String pickupby) {
        this.pickupby = pickupby;
    }

    public String getConnoteno() {
        return connoteno;
    }

    public void setConnoteno(String connoteno) {
        this.connoteno = connoteno;
    }

    public String getOriPin() {
        return oriPin;
    }

    public void setOriPin(String oriPin) {
        this.oriPin = oriPin;
    }

    public String getDestPin() {
        return destPin;
    }

    public void setDestPin(String destPin) {
        this.destPin = destPin;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }

    public String getNoofpcs() {
        return noofpcs;
    }

    public void setNoofpcs(String noofpcs) {
        this.noofpcs = noofpcs;
    }

    public List<ScannedBoxNew> getScannedBox() {
        return scannedBoxNews;
    }

    public void setScannedBox(List<ScannedBoxNew> scannedBoxNews) {
        this.scannedBoxNews = scannedBoxNews;
    }

  /*  public List<Integer> getScannedBox() {
        return scannedBox;
    }

    public void setScannedBox(List<Integer> scannedBox) {
        this.scannedBox = scannedBox;
    }*/

    public List<Integer> getScannedEwaybill() {
        return scannedEwaybill;
    }

    public void setScannedEwaybill(List<Integer> scannedEwaybill) {
        this.scannedEwaybill = scannedEwaybill;
    }
}
