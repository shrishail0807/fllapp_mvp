package com.criticalog.ecritica.MVPInscanNew.Model;

import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.MVPInscan.Model.ItemSwatch;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DatumDocketCheckNew implements Serializable {
    @SerializedName("org_pin")
    @Expose
    private String orgPin;
    @SerializedName("dest_pin")
    @Expose
    private String destPin;
    @SerializedName("actual_val")
    @Expose
    private Integer actualVal;
    @SerializedName("hub_loc")
    @Expose
    private String hubLoc;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("pud_loc")
    @Expose
    private String pudLoc;
    @SerializedName("ewaybill")
    @Expose
    private String ewaybill;
    @SerializedName("pickupby")
    @Expose
    private String pickupby;

    public String getClient_code() {
        return client_code;
    }

    public void setClient_code(String client_code) {
        this.client_code = client_code;
    }

    @SerializedName("client_code")
    @Expose
    private String client_code;

    @SerializedName("fun_msg")
    @Expose
    private String fun_msg;

    @SerializedName("autobox")
    @Expose
    private String autobox;
    @SerializedName("items")
    @Expose
    private List<ItemSwatch> items = null;

    public String getAutobox() {
        return autobox;
    }

    public void setAutobox(String autobox) {
        this.autobox = autobox;
    }

    public List<ItemSwatch> getItems() {
        return items;
    }

    public void setItems(List<ItemSwatch> items) {
        this.items = items;
    }

    public String getFun_msg() {
        return fun_msg;
    }

    public void setFun_msg(String fun_msg) {
        this.fun_msg = fun_msg;
    }

    public String getAutobox_flag() {
        return autobox_flag;
    }

    public void setAutobox_flag(String autobox_flag) {
        this.autobox_flag = autobox_flag;
    }

    public List<EwaybillScanNew> getEwaybillScanNew() {
        return ewaybillScanNew;
    }

    public void setEwaybillScanNew(List<EwaybillScanNew> ewaybillScanNew) {
        this.ewaybillScanNew = ewaybillScanNew;
    }

    @SerializedName("autobox_flag")
    @Expose
    private String autobox_flag;

    @SerializedName("flag_noOfPcs")
    @Expose
    private Integer flagNoOfPcs;
    @SerializedName("no_of_pcs")
    @Expose
    private Integer noOfPcs;

    @SerializedName("Tabno_of_pcs")
    @Expose
    private List<Object> tabnoOfPcs = null;
    @SerializedName("docketno")
    @Expose
    private String docketno;
    @SerializedName("ewaybill_scan")
    @Expose
    private List<EwaybillScanNew> ewaybillScanNew = null;
    @SerializedName("checklist_scan")
    @Expose
    private List<ChecklistScan> checklistScan = null;

    public String getOrgPin() {
        return orgPin;
    }

    public void setOrgPin(String orgPin) {
        this.orgPin = orgPin;
    }

    public String getDestPin() {
        return destPin;
    }

    public void setDestPin(String destPin) {
        this.destPin = destPin;
    }

    public Integer getActualVal() {
        return actualVal;
    }

    public void setActualVal(Integer actualVal) {
        this.actualVal = actualVal;
    }

    public String getHubLoc() {
        return hubLoc;
    }

    public void setHubLoc(String hubLoc) {
        this.hubLoc = hubLoc;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPudLoc() {
        return pudLoc;
    }

    public void setPudLoc(String pudLoc) {
        this.pudLoc = pudLoc;
    }

    public String getEwaybill() {
        return ewaybill;
    }

    public void setEwaybill(String ewaybill) {
        this.ewaybill = ewaybill;
    }

    public String getPickupby() {
        return pickupby;
    }

    public void setPickupby(String pickupby) {
        this.pickupby = pickupby;
    }

    public Integer getFlagNoOfPcs() {
        return flagNoOfPcs;
    }

    public void setFlagNoOfPcs(Integer flagNoOfPcs) {
        this.flagNoOfPcs = flagNoOfPcs;
    }

    public Integer getNoOfPcs() {
        return noOfPcs;
    }

    public void setNoOfPcs(Integer noOfPcs) {
        this.noOfPcs = noOfPcs;
    }

    public List<Object> getTabnoOfPcs() {
        return tabnoOfPcs;
    }

    public void setTabnoOfPcs(List<Object> tabnoOfPcs) {
        this.tabnoOfPcs = tabnoOfPcs;
    }

    public String getDocketno() {
        return docketno;
    }

    public void setDocketno(String docketno) {
        this.docketno = docketno;
    }

    public List<EwaybillScanNew> getEwaybillScan() {
        return ewaybillScanNew;
    }

    public void setEwaybillScan(List<EwaybillScanNew> ewaybillScanNew) {
        this.ewaybillScanNew = ewaybillScanNew;
    }

    public List<ChecklistScan> getChecklistScan() {
        return checklistScan;
    }

    public void setChecklistScan(List<ChecklistScan> checklistScan) {
        this.checklistScan = checklistScan;
    }
   /* @SerializedName("org_pin")
    @Expose
    private String orgPin;
    @SerializedName("dest_pin")
    @Expose
    private String destPin;
    @SerializedName("actual_val")
    @Expose
    private Integer actualVal;
    @SerializedName("hub_loc")
    @Expose
    private String hubLoc;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("pud_loc")
    @Expose
    private String pudLoc;
    @SerializedName("ewaybill")
    @Expose
    private String ewaybill;
    @SerializedName("pickupby")
    @Expose
    private String pickupby;
    @SerializedName("flag_noOfPcs")
    @Expose
    private Integer flagNoOfPcs;
    @SerializedName("no_of_pcs")
    @Expose
    private Integer noOfPcs;
    @SerializedName("Tabno_of_pcs")
    @Expose
    private List<Object> tabnoOfPcs = null;
    @SerializedName("docketno")
    @Expose
    private String docketno;
    @SerializedName("ewaybill_scan")
    @Expose
    private List<EwaybillScan> ewaybillScan = null;

    public String getOrgPin() {
        return orgPin;
    }

    public void setOrgPin(String orgPin) {
        this.orgPin = orgPin;
    }

    public String getDestPin() {
        return destPin;
    }

    public void setDestPin(String destPin) {
        this.destPin = destPin;
    }

    public Integer getActualVal() {
        return actualVal;
    }

    public void setActualVal(Integer actualVal) {
        this.actualVal = actualVal;
    }

    public String getHubLoc() {
        return hubLoc;
    }

    public void setHubLoc(String hubLoc) {
        this.hubLoc = hubLoc;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPudLoc() {
        return pudLoc;
    }

    public void setPudLoc(String pudLoc) {
        this.pudLoc = pudLoc;
    }

    public String getEwaybill() {
        return ewaybill;
    }

    public void setEwaybill(String ewaybill) {
        this.ewaybill = ewaybill;
    }

    public String getPickupby() {
        return pickupby;
    }

    public void setPickupby(String pickupby) {
        this.pickupby = pickupby;
    }

    public Integer getFlagNoOfPcs() {
        return flagNoOfPcs;
    }

    public void setFlagNoOfPcs(Integer flagNoOfPcs) {
        this.flagNoOfPcs = flagNoOfPcs;
    }

    public Integer getNoOfPcs() {
        return noOfPcs;
    }

    public void setNoOfPcs(Integer noOfPcs) {
        this.noOfPcs = noOfPcs;
    }

    public List<Object> getTabnoOfPcs() {
        return tabnoOfPcs;
    }

    public void setTabnoOfPcs(List<Object> tabnoOfPcs) {
        this.tabnoOfPcs = tabnoOfPcs;
    }

    public String getDocketno() {
        return docketno;
    }

    public void setDocketno(String docketno) {
        this.docketno = docketno;
    }

    public List<EwaybillScan> getEwaybillScan() {
        return ewaybillScan;
    }

    public void setEwaybillScan(List<EwaybillScan> ewaybillScan) {
        this.ewaybillScan = ewaybillScan;
    }*/


}
