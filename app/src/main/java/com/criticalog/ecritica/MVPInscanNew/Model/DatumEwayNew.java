package com.criticalog.ecritica.MVPInscanNew.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumEwayNew {
    @SerializedName("scanned_ewaybill")
    @Expose
    private Long scannedEwaybill;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("actual_value")
    @Expose
    private String actualValue;

    public Long getScannedEwaybill() {
        return scannedEwaybill;
    }

    public void setScannedEwaybill(Long scannedEwaybill) {
        this.scannedEwaybill = scannedEwaybill;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }
}
