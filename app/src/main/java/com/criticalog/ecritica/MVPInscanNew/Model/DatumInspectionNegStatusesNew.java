package com.criticalog.ecritica.MVPInscanNew.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumInspectionNegStatusesNew {
    @SerializedName("negative_code")
    @Expose
    private String negativeCode;
    @SerializedName("negative_desc")
    @Expose
    private String negativeDesc;

    public String getNegativeCode() {
        return negativeCode;
    }

    public void setNegativeCode(String negativeCode) {
        this.negativeCode = negativeCode;
    }

    public String getNegativeDesc() {
        return negativeDesc;
    }

    public void setNegativeDesc(String negativeDesc) {
        this.negativeDesc = negativeDesc;
    }

}
