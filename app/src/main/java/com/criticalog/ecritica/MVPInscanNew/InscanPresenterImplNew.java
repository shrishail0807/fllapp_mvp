package com.criticalog.ecritica.MVPInscanNew;

import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.DocketCheckResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.EwayBillValidateResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanBoxResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InScanSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InscanNegativeStatusResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionDoneResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegSubmitResponseNew;
import com.criticalog.ecritica.MVPInscanNew.Model.InspectionNegativeStatusRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateRequestNew;
import com.criticalog.ecritica.MVPInscanNew.Model.OriginPincodeValidateResponseNew;

public class InscanPresenterImplNew implements InscanContractNew.presenter, InscanContractNew.DocketCheckIntractor.OnFinishedListener,
        InscanContractNew.OriginPincodeValidateIntractor.OnFinishedListener, InscanContractNew.ActualValueInscanIntractor.OnFinishedListener, InscanContractNew.InScanBoxIntractor.OnFinishedListener, InscanContractNew.InScanSubmitIntractor.OnFinishedListener, InscanContractNew.EwayBillValidateIntractor.OnFinishedListener ,
        InscanContractNew.InspectionDoneInteractor.OnFinishedListener, InscanContractNew.InspectionNegativeStatusInteractor.OnFinishedListener,
InscanContractNew.InspectionNegativeSubmitInteractor.OnFinishedListener{
    private InscanContractNew.MainView mainView;
    private InscanContractNew.DocketCheckIntractor docketCheckIntractor;
    private InscanContractNew.OriginPincodeValidateIntractor originPincodeValidateIntractor;
    private InscanContractNew.ActualValueInscanIntractor actualValueInscanIntractor;
    private InscanContractNew.InScanBoxIntractor inScanBoxIntractor;
    private InscanContractNew.InScanSubmitIntractor inScanSubmitIntractor;
    private InscanContractNew.EwayBillValidateIntractor ewayBillValidateIntractor;
    private InscanContractNew.InspectionDoneInteractor inspectionDoneInteractor;
    private InscanContractNew.InspectionNegativeStatusInteractor inspectionNegativeStatusInteractor;
    private InscanContractNew.InspectionNegativeSubmitInteractor inspectionNegativeSubmitInteractor;

    public InscanPresenterImplNew(InscanContractNew.MainView mainView, InscanContractNew.DocketCheckIntractor docketCheckIntractor,
                                  InscanContractNew.OriginPincodeValidateIntractor originPincodeValidateIntractor,
                                  InscanContractNew.ActualValueInscanIntractor actualValueInscanIntractor,
                                  InscanContractNew.InScanBoxIntractor inScanBoxIntractor,
                                  InscanContractNew.InScanSubmitIntractor inScanSubmitIntractor,
                                  InscanContractNew.EwayBillValidateIntractor ewayBillValidateIntractor,
                                  InscanContractNew.InspectionDoneInteractor inspectionDoneInteractor,
                                  InscanContractNew.InspectionNegativeStatusInteractor inspectionNegativeStatusInteractor,
                                  InscanContractNew.InspectionNegativeSubmitInteractor inspectionNegativeSubmitInteractor) {
        this.mainView = mainView;
        this.docketCheckIntractor = docketCheckIntractor;
        this.originPincodeValidateIntractor=originPincodeValidateIntractor;
        this.actualValueInscanIntractor=actualValueInscanIntractor;
        this.inScanBoxIntractor=inScanBoxIntractor;
        this.inScanSubmitIntractor=inScanSubmitIntractor;
        this.ewayBillValidateIntractor=ewayBillValidateIntractor;
        this.inspectionDoneInteractor=inspectionDoneInteractor;
        this.inspectionNegativeStatusInteractor=inspectionNegativeStatusInteractor;
        this.inspectionNegativeSubmitInteractor=inspectionNegativeSubmitInteractor;

    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void inScanDocketCheck(String token, DocketCheckRequestNew docketCheckRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        docketCheckIntractor.getDocketCheckSuccess(this, token, docketCheckRequestNew);
    }

    @Override
    public void validateOriginPicode(String token, OriginPincodeValidateRequestNew originPincodeValidateRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        originPincodeValidateIntractor.getOriginPincodeValidateSuccess(this,token, originPincodeValidateRequestNew);
    }

    @Override
    public void actualValueInscanCheck(String token, ActualValueInscanRequestNew actualValueInscanRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }
        actualValueInscanIntractor.getActualValueInscanRequest(this,token,actualValueInscanRequest);
    }

    @Override
    public void inscanBoxNumberValidate(String token, InScanBoxRequestNew inScanBoxRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        inScanBoxIntractor.getActualValueInscanRequest(this,token, inScanBoxRequestNew);
    }

    @Override
    public void inScanSubmit(String token, InScanSubmitRequestNew inScanSubmitRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        inScanSubmitIntractor.getInScanSubmitRequest(this,token, inScanSubmitRequestNew);
    }

    @Override
    public void ewayBillValidate(EwayBillValidateRequestNew ewayBillValidateRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        ewayBillValidateIntractor.getEwayBillRequest(this,"", ewayBillValidateRequestNew);

    }

    @Override
    public void inspectionDoneRequest(InspectionDoneRequestNew inspectionDoneRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        inspectionDoneInteractor.inspectionDoneRequest((InscanContractNew.InspectionDoneInteractor.OnFinishedListener) this,"", inspectionDoneRequestNew);
    }

    @Override
    public void inspectionNegativeStatuse(InspectionNegativeStatusRequestNew inspectionNegativeStatusRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        inspectionNegativeStatusInteractor.inspectionNegativeStatusRequest(this,"", inspectionNegativeStatusRequestNew);
    }

    @Override
    public void inspectionNegativeSubmit(InspectionNegSubmitRequestNew inspectionNegSubmitRequestNew) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        inspectionNegativeSubmitInteractor.inspectionNegativeStatusRequest(this,"", inspectionNegSubmitRequestNew);

    }

    @Override
    public void onFinished(DocketCheckResponseNew docketCheckResponseNew) {
        if(mainView!=null)
        {
            mainView.setDocketCheckDataToViews(docketCheckResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(OriginPincodeValidateResponseNew originPincodeResponse) {
        if(mainView!=null)
        {
            mainView.setOriginPincodeResponse(originPincodeResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(ActualValueInscanResponseNew actualValueInscanResponseNew) {
        if(mainView!=null)
        {
            mainView.setActualValueInscanResponse(actualValueInscanResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InScanBoxResponseNew inScanBoxResponseNew) {
        if(mainView!=null)
        {
            mainView.setInscanBoxDataToViews(inScanBoxResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InScanSubmitResponseNew inScanSubmitResponseNew) {
        if(mainView!=null)
        {
            mainView.setInScanSubmitDataToViews(inScanSubmitResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(EwayBillValidateResponseNew ewayBillValidateResponseNew) {
        if(mainView!=null)
        {
            mainView.setEwayBillValidateResponseToViews(ewayBillValidateResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InspectionDoneResponseNew inspectionDoneResponseNew) {
        if(mainView!=null)
        {
            mainView.inspectionDoneResponseToData(inspectionDoneResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InscanNegativeStatusResponseNew inscanNegativeStatusResponseNew) {
        if(mainView!=null)
        {
            mainView.inspectionNegativeStatusData(inscanNegativeStatusResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InspectionNegSubmitResponseNew inspectionNegSubmitResponseNew) {
        if(mainView!=null)
        {
            mainView.inspectionNegSubmitDataToViews(inspectionNegSubmitResponseNew);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
