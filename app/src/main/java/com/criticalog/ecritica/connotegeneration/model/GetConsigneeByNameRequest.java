package com.criticalog.ecritica.connotegeneration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetConsigneeByNameRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("dest_pin")
    @Expose
    private String destPin;
    @SerializedName("consignee_name")
    @Expose
    private String consigneeName;

    @SerializedName("consignee_code")
    @Expose
    private String consignee_code;

    public String getConsignee_code() {
        return consignee_code;
    }

    public void setConsignee_code(String consignee_code) {
        this.consignee_code = consignee_code;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getDestPin() {
        return destPin;
    }

    public void setDestPin(String destPin) {
        this.destPin = destPin;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }
}
