package com.criticalog.ecritica.connotegeneration;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.criticalog.ecritica.MVPBarcodeScan.BarcodeScanActivity;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.additionclaim.viewmodel.AdditionalClaimViewModel;
import com.criticalog.ecritica.connotegeneration.adapter.CustomAutoBoxAdapter;
import com.criticalog.ecritica.connotegeneration.adapter.CustomAutoSuggestAdapter;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationRequest;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationResponse;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesRequest;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesResponse;
import com.criticalog.ecritica.connotegeneration.model.DatumConsigneeName;
import com.criticalog.ecritica.connotegeneration.model.DatumLBHWithBoxCode;
import com.criticalog.ecritica.connotegeneration.model.DatumService;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHRequest;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHResponse;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeRequest;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeResponse;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameRequest;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameResponse;
import com.criticalog.ecritica.connotegeneration.model.LBHModel;
import com.criticalog.ecritica.connotegeneration.viewmodel.ConnoteGenerationViewModel;
import com.criticalog.ecritica.databinding.ConnoteGenerationLayoutBinding;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class ConnoteGenerationActivity extends AppCompatActivity {

    private ConnoteGenerationLayoutBinding connoteGenerationLayoutBinding;
    private ConnoteGenerationViewModel connoteGenerationViewModel;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, bookingId;
    private AdditionalClaimViewModel additionalClaimViewModel;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double latitude = 0.0, longitude = 0.0;
    private SimpleArcDialog mProgressBar;
    private Dialog dialogSuccess;
    private ArrayList<SampleSearchModel> searchDataProductMode = new ArrayList<>();
    private ArrayList<String> searchDataProductModeString = new ArrayList<>();
    private List<DatumService> datumServiceList = new ArrayList<>();
    private String productCode = "";
    private String showDialogFlag = "";
    private String clientCode, cnorCode, autoBox;
    String pincode = "";
    private CustomAutoSuggestAdapter adapter;
    private CustomAutoBoxAdapter customAutoBoxAdapter;
    private List<DatumConsigneeName> datumConsigneeNameList = new ArrayList<>();
    private String pincodeFlag = "flase";
    private List<String> payModeList = new ArrayList<>();
    private List<LBHModel> lbhModelList = new ArrayList<>();

    String consigneeName = "";
    String address1 = "";
    String address2 = "";
    String address3 = "";
    String destPincode = "";
    String consigneeMobile = "";
    String noOfPieces = "";
    String approximateWeight = "";
    String invoiceValue = "";
    String codAmount = "";
    String emailId = "";
    String orderNumber = "";
    String gstNumber = "";
    String paymentMode = "";
    String splitPieces = "";
    private int totalPieces = 0;

    private String getConsigneeBuCode = "";
    private List<DatumLBHWithBoxCode> datumLBHWithBoxCodeListCustom = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        connoteGenerationLayoutBinding = DataBindingUtil.setContentView(this, R.layout.connote_generation_layout);
        connoteGenerationViewModel = ViewModelProviders.of(this).get(ConnoteGenerationViewModel.class);
        connoteGenerationViewModel.init();

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mCriticalogSharedPreferences.saveData("navigationStatus", "");
        mCriticalogSharedPreferences.saveData("generated_con", "");
        clientCode = mCriticalogSharedPreferences.getData("client_code");
        cnorCode = mCriticalogSharedPreferences.getData("cnor_code");
        autoBox = mCriticalogSharedPreferences.getData("auto_box");
        userId = mCriticalogSharedPreferences.getData("userId");
        bookingId = mCriticalogSharedPreferences.getData("bookingno");
        //autoBox="1";
        if (autoBox.equalsIgnoreCase("1")) {
            callGetAutoBoxType();

            connoteGenerationLayoutBinding.mSelectDimType.setFocusable(false);
            connoteGenerationLayoutBinding.mAddLBH.setVisibility(View.INVISIBLE);
            // connoteGenerationLayoutBinding.mSelectDimType.setClickable(true);
            // connoteGenerationLayoutBinding.mSelectDimType.setEnabled(false);
        }
        Window window = getWindow();
        // Set the dialog size and position
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        // Set the soft input mode to adjust the dialog size when the keyboard is shown
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);
        //Get Fused Location
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        connoteGenerationLayoutBinding.mProductMode.setFocusable(false);


        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        getLocation();

        getConsineeByConsigneeCode();
        getPaytypeMode();

        connoteGenerationLayoutBinding.mConsigneePincode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    // cursor is out of the EditText
                    // do something here
                    pincode = connoteGenerationLayoutBinding.mConsigneePincode.getText().toString();

                    if (pincode.length() == 6) {
                        if (pincodeFlag.equalsIgnoreCase("false")) {
                            PincodeValidateRequest pincodeValidateRequest = new PincodeValidateRequest();
                            pincodeValidateRequest.setAction("pincode_validation");
                            pincodeValidateRequest.setUserId(userId);
                            pincodeValidateRequest.setPincode(pincode);
                            pincodeValidateRequest.setLatitude(String.valueOf(latitude));
                            pincodeValidateRequest.setLongitude(String.valueOf(longitude));
                            connoteGenerationViewModel.pincodeValidationRequest(pincodeValidateRequest);
                            mProgressBar.show();
                        }

                    } else {
                        pincodeFlag = "false";
                        //connoteGenerationLayoutBinding.mConsineeName.clearFocus();
                        //requestFocus(connoteGenerationLayoutBinding.mConsigneePincode);
                        //  connoteGenerationLayoutBinding.mConsigneePincode.setActivated(true);
                        //  connoteGenerationLayoutBinding.mConsigneePincode.setPressed(true);
                        Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Pincode", Toasty.LENGTH_SHORT).show();
                    }
                }
            }
        });
        connoteGenerationLayoutBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        connoteGenerationLayoutBinding.mConsineeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                connoteGenerationLayoutBinding.mConsineeName.showDropDown();
            }
        });

        connoteGenerationLayoutBinding.mSelectDimType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connoteGenerationLayoutBinding.mSelectDimType.showDropDown();
            }
        });
        // connoteGenerationLayoutBinding.mConsineeName.setDropDownVerticalOffset(-10);

        connoteGenerationLayoutBinding.mSelectDimType.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {

                if (datumLBHWithBoxCodeListCustom.size() > 0) {
                    String selection = datumLBHWithBoxCodeListCustom.get(position).getBoxCode();
                    int pos = -1;

                    for (int i = 0; i < datumLBHWithBoxCodeListCustom.size(); i++) {
                        if (datumLBHWithBoxCodeListCustom.get(i).getBoxCode().equals(selection)) {
                            pos = i;
                            break;
                        }
                    }
                    lbhModelList = new ArrayList<>();
                    connoteGenerationLayoutBinding.mLength.setText(datumLBHWithBoxCodeListCustom.get(pos).getLength());
                    connoteGenerationLayoutBinding.mBreadth.setText(datumLBHWithBoxCodeListCustom.get(pos).getBreadth());
                    connoteGenerationLayoutBinding.mHeight.setText(datumLBHWithBoxCodeListCustom.get(pos).getHeight());
                    connoteGenerationLayoutBinding.mSpitPieces.setText(noOfPieces);

                    connoteGenerationLayoutBinding.mLength.setEnabled(false);
                    connoteGenerationLayoutBinding.mBreadth.setEnabled(false);
                    connoteGenerationLayoutBinding.mHeight.setEnabled(false);
                    connoteGenerationLayoutBinding.mSpitPieces.setEnabled(false);

                    LBHModel lbhModel = new LBHModel();
                    lbhModel.setAutobox(selection);
                    lbhModel.setLength(datumLBHWithBoxCodeListCustom.get(pos).getLength());
                    lbhModel.setBreadth(datumLBHWithBoxCodeListCustom.get(pos).getBreadth());
                    lbhModel.setHeight(datumLBHWithBoxCodeListCustom.get(pos).getHeight());
                    lbhModel.setSplit_pieces(noOfPieces);
                    lbhModelList.add(lbhModel);


                }

            }
        });
        connoteGenerationLayoutBinding.mConsineeName.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {

                if (datumConsigneeNameList.size() > 0) {
                    String selection = datumConsigneeNameList.get(position).getConsName();
                    int pos = -1;

                    for (int i = 0; i < datumConsigneeNameList.size(); i++) {
                        if (datumConsigneeNameList.get(i).getConsName().equals(selection)) {
                            pos = i;
                            break;
                        }
                    }

                    connoteGenerationLayoutBinding.mAddress1.setText(datumConsigneeNameList.get(pos).getAddress1());
                    connoteGenerationLayoutBinding.mAddress2.setText(datumConsigneeNameList.get(pos).getAddress2());
                    connoteGenerationLayoutBinding.mAddress3.setText(datumConsigneeNameList.get(pos).getAddress3());
                    connoteGenerationLayoutBinding.mConsigneeMobile.setText(datumConsigneeNameList.get(pos).getConsMobile());
                    connoteGenerationLayoutBinding.mEmail.setText(datumConsigneeNameList.get(pos).getConsEmail());

                    payModeList = datumConsigneeNameList.get(pos).getPayMode();

                   /* if (payModeList.size() > 0) {

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ConnoteGenerationActivity.this, android.R.layout.simple_spinner_dropdown_item, payModeList);
                        //set the spinners adapter to the previously created one.
                        connoteGenerationLayoutBinding.mPaymentMode.setAdapter(adapter);

                    }*/
                }

            }
        });
        connoteGenerationLayoutBinding.mPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.WHITE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        connoteGenerationViewModel.getAutoBoxTypeResponseLiveData().observe(this, new Observer<GetAutoLBHResponse>() {
            @Override
            public void onChanged(GetAutoLBHResponse getAutoLBHResponse) {
                // mProgressBar.dismiss();
                if (getAutoLBHResponse.getStatus() == 200) {


                    connoteGenerationLayoutBinding.mTextSelectDimType.setVisibility(View.VISIBLE);
                    connoteGenerationLayoutBinding.mSelectDimType.setVisibility(View.VISIBLE);

                    connoteGenerationLayoutBinding.mNoOfPcs.setText("1");
                    connoteGenerationLayoutBinding.mSpitPieces.setText("1");

                    connoteGenerationLayoutBinding.mNoOfPcs.setEnabled(false);
                    connoteGenerationLayoutBinding.mSpitPieces.setEnabled(false);

                    datumLBHWithBoxCodeListCustom = getAutoLBHResponse.getData();
                    Toast.makeText(ConnoteGenerationActivity.this, getAutoLBHResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    customAutoBoxAdapter = new CustomAutoBoxAdapter(ConnoteGenerationActivity.this, getAutoLBHResponse.getData());
                    connoteGenerationLayoutBinding.mSelectDimType.setAdapter(customAutoBoxAdapter);

                } else {
                    Toast.makeText(ConnoteGenerationActivity.this, getAutoLBHResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        connoteGenerationViewModel.getPaymentTypeResponseLiveData().observe(this, new Observer<GetClientPaytypeResponse>() {
            @Override
            public void onChanged(GetClientPaytypeResponse getClientPaytypeResponse) {
                mProgressBar.dismiss();

                if (getClientPaytypeResponse.getStatus() == 200) {
                    payModeList = getClientPaytypeResponse.getData().getPayMode();
                    if (payModeList.size() > 0) {
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ConnoteGenerationActivity.this, android.R.layout.simple_spinner_dropdown_item, payModeList);
                        //set the spinners adapter to the previously created one.
                        connoteGenerationLayoutBinding.mPaymentMode.setAdapter(adapter);
                    }
                } else {
                    Toast.makeText(ConnoteGenerationActivity.this, getClientPaytypeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        connoteGenerationViewModel.getConsigneeByNameResponseLiveData().observe(this, new Observer<GetConsigneeByNameResponse>() {
            @Override
            public void onChanged(GetConsigneeByNameResponse getConsigneeByNameResponse) {
                mProgressBar.dismiss();
                if (getConsigneeByNameResponse.getStatus() == 200) {

                    if (getConsigneeBuCode.equalsIgnoreCase("flase")) {
                        datumConsigneeNameList = getConsigneeByNameResponse.getData();
                        adapter = new CustomAutoSuggestAdapter(ConnoteGenerationActivity.this,
                                getConsigneeByNameResponse.getData());

                        connoteGenerationLayoutBinding.mConsineeName.setAdapter(adapter);
                        Toast.makeText(ConnoteGenerationActivity.this, getConsigneeByNameResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        connoteGenerationLayoutBinding.mAddress1.setText(getConsigneeByNameResponse.getData().get(0).getAddress1());
                        connoteGenerationLayoutBinding.mAddress2.setText(getConsigneeByNameResponse.getData().get(0).getAddress2());
                        connoteGenerationLayoutBinding.mAddress3.setText(getConsigneeByNameResponse.getData().get(0).getAddress3());
                        connoteGenerationLayoutBinding.mConsigneeMobile.setText(getConsigneeByNameResponse.getData().get(0).getConsMobile());
                        connoteGenerationLayoutBinding.mEmail.setText(getConsigneeByNameResponse.getData().get(0).getConsEmail());
                        connoteGenerationLayoutBinding.mConsigneePincode.setText(getConsigneeByNameResponse.getData().get(0).getDestPincode());
                        connoteGenerationLayoutBinding.mConsineeName.setText(getConsigneeByNameResponse.getData().get(0).getConsName());
                    }

                } else {
                    Toast.makeText(ConnoteGenerationActivity.this, getConsigneeByNameResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        connoteGenerationViewModel.criticalogServicesResponseLiveData().observe(this, new Observer<CriticalogServicesResponse>() {
            @Override
            public void onChanged(CriticalogServicesResponse criticalogServicesResponse) {
                mProgressBar.dismiss();
                if (criticalogServicesResponse.getStatus() == 200) {
                    if (criticalogServicesResponse.getData().size() > 0) {
                        connoteGenerationLayoutBinding.mProductMode.setText(criticalogServicesResponse.getData().get(0).getCodedescription());
                        productCode = criticalogServicesResponse.getData().get(0).getCodedescription();
                        datumServiceList = criticalogServicesResponse.getData();
                        for (int i = 0; i < criticalogServicesResponse.getData().size(); i++) {
                            searchDataProductMode.add(new SampleSearchModel(criticalogServicesResponse.getData().get(i).getCodedescription()));
                            searchDataProductModeString.add(criticalogServicesResponse.getData().get(i).getCodedescription());
                        }
                        if (showDialogFlag.equalsIgnoreCase("true")) {
                            showProductModeSearchDialog(searchDataProductMode, "Select Product Mode");
                        }

                    }
                }
            }
        });

        connoteGenerationViewModel.connoteGenerationResponseLiveData().observe(this, new Observer<ConnoteGenerationResponse>() {
            @Override
            public void onChanged(ConnoteGenerationResponse connoteGenerationResponse) {
                mProgressBar.dismiss();
                if (connoteGenerationResponse != null) {
                    if (connoteGenerationResponse.getStatus() == 200) {
                        Toasty.success(ConnoteGenerationActivity.this, connoteGenerationResponse.getMessage(), Toasty.LENGTH_SHORT).show();
                        connoteGenerationDialog(connoteGenerationResponse.getMessage());
                    } else {
                        Toasty.error(ConnoteGenerationActivity.this, connoteGenerationResponse.getMessage(), Toasty.LENGTH_SHORT).show();
                    }
                } else {
                    Toasty.error(ConnoteGenerationActivity.this, "Response in null", Toasty.LENGTH_SHORT).show();
                }
            }
        });

        connoteGenerationViewModel.pincodeValidateResponseLiveData().observe(this, new Observer<PincodeValidateResponse>() {
            @Override
            public void onChanged(PincodeValidateResponse pincodeValidateResponse) {
                mProgressBar.dismiss();
                if (pincodeValidateResponse.getStatus() == 200) {
                    Toasty.success(ConnoteGenerationActivity.this, pincodeValidateResponse.getMessage(), Toasty.LENGTH_SHORT).show();
                    connoteGenerationLayoutBinding.mGenerateConnote.setVisibility(View.VISIBLE);
                    pincodeFlag = "true";
                    getConsineeByName();
                } else {
                    pincodeFlag = "false";
                    connoteGenerationLayoutBinding.mConsigneePincode.setText("");
                    connoteGenerationLayoutBinding.mConsigneePincode.setHint("Enter Destination Pincode");
                    Toasty.error(ConnoteGenerationActivity.this, pincodeValidateResponse.getMessage(), Toasty.LENGTH_SHORT).show();
                    connoteGenerationLayoutBinding.mGenerateConnote.setVisibility(View.GONE);
                }
            }
        });

        connoteGenerationLayoutBinding.mAddLBH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noOfPieces = connoteGenerationLayoutBinding.mNoOfPcs.getText().toString();
                String length = connoteGenerationLayoutBinding.mLength.getText().toString();
                String breadth = connoteGenerationLayoutBinding.mBreadth.getText().toString();
                String height = connoteGenerationLayoutBinding.mHeight.getText().toString();
                splitPieces = connoteGenerationLayoutBinding.mSpitPieces.getText().toString();

                if (noOfPieces.equalsIgnoreCase("")) {
                    Toasty.error(ConnoteGenerationActivity.this, "No. of pieces cannot be empty!!", Toasty.LENGTH_SHORT).show();
                } else if (length.equalsIgnoreCase("")) {
                    Toasty.error(ConnoteGenerationActivity.this, "Length Cannot be Empty!!", Toasty.LENGTH_SHORT).show();
                } else if (breadth.equalsIgnoreCase("")) {
                    Toasty.error(ConnoteGenerationActivity.this, "Breadth Cannot be Empty!!", Toasty.LENGTH_SHORT).show();
                } else if (height.equalsIgnoreCase("")) {
                    Toasty.error(ConnoteGenerationActivity.this, "Height Cannot be Empty!!", Toasty.LENGTH_SHORT).show();
                } else if (splitPieces.equalsIgnoreCase("")) {
                    Toasty.error(ConnoteGenerationActivity.this, "Split Piece Cannot be Empty!!", Toasty.LENGTH_SHORT).show();
                } else {
                    int sumofPieces = 0;
                    if (lbhModelList.size() > 0) {

                        for (int i = 0; i < lbhModelList.size(); i++) {
                            totalPieces = Integer.parseInt(lbhModelList.get(i).getSplit_pieces());
                            sumofPieces = sumofPieces + totalPieces;
                        }
                        sumofPieces = sumofPieces + Integer.parseInt(splitPieces);

                        if (sumofPieces <= Integer.parseInt(noOfPieces)) {
                            LBHModel lbhModel = new LBHModel();
                            lbhModel.setAutobox("");
                            lbhModel.setLength(length);
                            lbhModel.setBreadth(breadth);
                            lbhModel.setHeight(height);
                            if (autoBox.equalsIgnoreCase("1")) {
                                lbhModel.setSplit_pieces("1");
                            } else {
                                lbhModel.setSplit_pieces(splitPieces);
                            }
                            lbhModelList.add(lbhModel);
                            connoteGenerationLayoutBinding.mLength.setText("");
                            connoteGenerationLayoutBinding.mLength.setHint("L");

                            connoteGenerationLayoutBinding.mBreadth.setText("");
                            connoteGenerationLayoutBinding.mBreadth.setHint("B");

                            connoteGenerationLayoutBinding.mHeight.setText("");
                            connoteGenerationLayoutBinding.mHeight.setHint("H");

                            connoteGenerationLayoutBinding.mSpitPieces.setText("");
                            connoteGenerationLayoutBinding.mSpitPieces.setHint("Split Pieces");
                        } else {
                            Toasty.error(ConnoteGenerationActivity.this, "Total Number of pieces exeeds!!", Toasty.LENGTH_SHORT).show();
                        }
                    } else {

                        if (Integer.parseInt(splitPieces) <= Integer.parseInt(noOfPieces)) {
                            LBHModel lbhModel = new LBHModel();
                            lbhModel.setAutobox("");
                            lbhModel.setLength(length);
                            lbhModel.setBreadth(breadth);
                            lbhModel.setHeight(height);
                            lbhModel.setSplit_pieces(splitPieces);
                            lbhModelList.add(lbhModel);
                            connoteGenerationLayoutBinding.mLength.setText("");
                            connoteGenerationLayoutBinding.mLength.setHint("L");

                            connoteGenerationLayoutBinding.mBreadth.setText("");
                            connoteGenerationLayoutBinding.mBreadth.setHint("B");

                            connoteGenerationLayoutBinding.mHeight.setText("");
                            connoteGenerationLayoutBinding.mHeight.setHint("H");

                            connoteGenerationLayoutBinding.mSpitPieces.setText("");
                            connoteGenerationLayoutBinding.mSpitPieces.setHint("Split Pieces");
                        } else {
                            Toasty.error(ConnoteGenerationActivity.this, "Total Number of pieces exeeds!!", Toasty.LENGTH_SHORT).show();
                        }

                    }

                }
            }
        });
        connoteGenerationLayoutBinding.mProductMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callCriticalogServicesApi("true");
            }
        });
        connoteGenerationLayoutBinding.mGenerateConnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consigneeName = connoteGenerationLayoutBinding.mConsineeName.getText().toString();
                address1 = connoteGenerationLayoutBinding.mAddress1.getText().toString();
                address2 = connoteGenerationLayoutBinding.mAddress2.getText().toString();
                address3 = connoteGenerationLayoutBinding.mAddress3.getText().toString();
                destPincode = connoteGenerationLayoutBinding.mConsigneePincode.getText().toString();
                consigneeMobile = connoteGenerationLayoutBinding.mConsigneeMobile.getText().toString();
                noOfPieces = connoteGenerationLayoutBinding.mNoOfPcs.getText().toString();
                approximateWeight = connoteGenerationLayoutBinding.mApproxWeight.getText().toString();
                invoiceValue = connoteGenerationLayoutBinding.mInvoiceValue.getText().toString();
                codAmount = connoteGenerationLayoutBinding.mCodAmount.getText().toString();
                emailId = connoteGenerationLayoutBinding.mEmail.getText().toString();
                orderNumber = connoteGenerationLayoutBinding.mOrderNumber.getText().toString();
                gstNumber = connoteGenerationLayoutBinding.mGst.getText().toString();
                try {
                    paymentMode = connoteGenerationLayoutBinding.mPaymentMode.getSelectedItem().toString();
                } catch (Exception e) {
                }

                if (TextUtils.isEmpty(consigneeName)) {

                    requestFocus(connoteGenerationLayoutBinding.mConsineeName);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Consignee Name", Toast.LENGTH_SHORT, true).show();
                } else if (TextUtils.isEmpty(address1)) {
                    requestFocus(connoteGenerationLayoutBinding.mAddress1);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Address", Toast.LENGTH_SHORT, true).show();

                } else if (TextUtils.isEmpty(address2)) {
                    requestFocus(connoteGenerationLayoutBinding.mAddress2);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Address", Toast.LENGTH_SHORT, true).show();

                } else if (TextUtils.isEmpty(destPincode)) {
                    requestFocus(connoteGenerationLayoutBinding.mConsigneePincode);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Pincode", Toast.LENGTH_SHORT, true).show();

                } else if (destPincode.length() != 6) {
                    requestFocus(connoteGenerationLayoutBinding.mConsigneePincode);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Pincode", Toast.LENGTH_SHORT, true).show();

                } else if (TextUtils.isEmpty(consigneeMobile)) {
                    requestFocus(connoteGenerationLayoutBinding.mConsigneeMobile);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT, true).show();
                } else if (isValidIndianMobileNumber(consigneeMobile) == false) {
                    requestFocus(connoteGenerationLayoutBinding.mConsigneeMobile);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Mobile Number", Toast.LENGTH_SHORT, true).show();

                } else if (TextUtils.isEmpty(noOfPieces)) {
                    requestFocus(connoteGenerationLayoutBinding.mNoOfPcs);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Number of Pieces", Toast.LENGTH_SHORT, true).show();

                } else if (lbhModelList.size() == 0) {
                    requestFocus(connoteGenerationLayoutBinding.mLength);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter L B H for All Pieces!!", Toast.LENGTH_SHORT, true).show();
                } else if (lbhModelList.size() > 0) {

                    int sumofPieces = 0;
                    if (lbhModelList.size() > 0) {

                        for (int i = 0; i < lbhModelList.size(); i++) {
                            totalPieces = Integer.parseInt(lbhModelList.get(i).getSplit_pieces());
                            sumofPieces = sumofPieces + totalPieces;
                        }

                        if (sumofPieces != Integer.parseInt(noOfPieces)) {
                            requestFocus(connoteGenerationLayoutBinding.mLength);
                            Toasty.error(ConnoteGenerationActivity.this, "Enter the L B H for All Pieces", Toast.LENGTH_SHORT, true).show();
                        } else {

                            if (noOfPieces.equalsIgnoreCase("0") || noOfPieces.equalsIgnoreCase("00")) {
                                requestFocus(connoteGenerationLayoutBinding.mNoOfPcs);
                                Toasty.error(ConnoteGenerationActivity.this, "Number of Pieces Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                            } else if (TextUtils.isEmpty(approximateWeight)) {
                                requestFocus(connoteGenerationLayoutBinding.mApproxWeight);
                                Toasty.error(ConnoteGenerationActivity.this, "Enter Weight", Toast.LENGTH_SHORT, true).show();
                            } else if (TextUtils.isEmpty(invoiceValue)) {
                                requestFocus(connoteGenerationLayoutBinding.mInvoiceValue);
                                Toasty.error(ConnoteGenerationActivity.this, "Enter Invoice Value", Toast.LENGTH_SHORT, true).show();
                            } else if (approximateWeight.equalsIgnoreCase("0")) {
                                requestFocus(connoteGenerationLayoutBinding.mApproxWeight);
                                Toasty.error(ConnoteGenerationActivity.this, "Approximate Weight Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                            } else if (TextUtils.isEmpty(productCode)) {
                                requestFocus(connoteGenerationLayoutBinding.mProductMode);
                                Toasty.error(ConnoteGenerationActivity.this, "Select Product Mode", Toast.LENGTH_SHORT, true).show();
                            } else if (paymentMode.equalsIgnoreCase("FTC")) {
                                if (TextUtils.isEmpty(gstNumber)) {

                                    requestFocus(connoteGenerationLayoutBinding.mGst);
                                    Toasty.error(ConnoteGenerationActivity.this, "Enter GST Number", Toast.LENGTH_SHORT, true).show();
                                } else {
                                    if (codAmount.equalsIgnoreCase("0")) {
                                        requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                                        Toasty.error(ConnoteGenerationActivity.this, "Amount Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                                    } else if (TextUtils.isEmpty(emailId)) {
                                        requestFocus(connoteGenerationLayoutBinding.mEmail);
                                        Toasty.error(ConnoteGenerationActivity.this, "Enter Email Id", Toast.LENGTH_SHORT, true).show();
                                    } else if (isValidEmail(emailId) == false) {
                                        requestFocus(connoteGenerationLayoutBinding.mEmail);
                                        Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT, true).show();
                                    } else if (paymentMode.equalsIgnoreCase("")) {
                                        Toasty.error(ConnoteGenerationActivity.this, "Select Payment Mode", Toast.LENGTH_SHORT, true).show();
                                    } else {
                                        callCitGenerate();
                                    }
                                }

                            } else if (paymentMode.equalsIgnoreCase("COD") || paymentMode.equalsIgnoreCase("Cheque")) {
                                if (TextUtils.isEmpty(codAmount)) {

                                    requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                                    Toasty.error(ConnoteGenerationActivity.this, "Enter the Amount Value", Toast.LENGTH_SHORT, true).show();
                                } else {
                                    if (codAmount.equalsIgnoreCase("0")) {
                                        requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                                        Toasty.error(ConnoteGenerationActivity.this, "Amount Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                                    } else if (TextUtils.isEmpty(emailId)) {
                                        requestFocus(connoteGenerationLayoutBinding.mEmail);
                                        Toasty.error(ConnoteGenerationActivity.this, "Enter Email Id", Toast.LENGTH_SHORT, true).show();
                                    } else if (isValidEmail(emailId) == false) {
                                        requestFocus(connoteGenerationLayoutBinding.mEmail);
                                        Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT, true).show();
                                    } else if (paymentMode.equalsIgnoreCase("")) {
                                        Toasty.error(ConnoteGenerationActivity.this, "Select Payment Mode", Toast.LENGTH_SHORT, true).show();
                                    } else {
                                        callCitGenerate();
                                    }
                                }
                            } else if (TextUtils.isEmpty(emailId)) {
                                requestFocus(connoteGenerationLayoutBinding.mEmail);
                                Toasty.error(ConnoteGenerationActivity.this, "Enter Email Id", Toast.LENGTH_SHORT, true).show();
                            } else if (isValidEmail(emailId) == false) {
                                requestFocus(connoteGenerationLayoutBinding.mEmail);
                                Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT, true).show();
                            } else if (paymentMode.equalsIgnoreCase("")) {
                                Toasty.error(ConnoteGenerationActivity.this, "Select Payment Mode", Toast.LENGTH_SHORT, true).show();
                            } else {
                                callCitGenerate();

                            }
                        }
                    }
                } else if (TextUtils.isEmpty(approximateWeight)) {
                    requestFocus(connoteGenerationLayoutBinding.mApproxWeight);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Weight", Toast.LENGTH_SHORT, true).show();
                } else if (TextUtils.isEmpty(invoiceValue)) {
                    requestFocus(connoteGenerationLayoutBinding.mInvoiceValue);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Invoice Value", Toast.LENGTH_SHORT, true).show();
                } else if (approximateWeight.equalsIgnoreCase("0")) {
                    requestFocus(connoteGenerationLayoutBinding.mApproxWeight);
                    Toasty.error(ConnoteGenerationActivity.this, "Approximate Weight Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                } else if (TextUtils.isEmpty(productCode)) {
                    requestFocus(connoteGenerationLayoutBinding.mProductMode);
                    Toasty.error(ConnoteGenerationActivity.this, "Select Product Mode", Toast.LENGTH_SHORT, true).show();
                } else if (paymentMode.equalsIgnoreCase("COD") || paymentMode.equalsIgnoreCase("Cheque")) {
                    if (TextUtils.isEmpty(codAmount)) {
                        requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                        Toasty.error(ConnoteGenerationActivity.this, "Enter the Amount Value", Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (codAmount.equalsIgnoreCase("0")) {
                            requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                            Toasty.error(ConnoteGenerationActivity.this, "Amount Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                        } else if (TextUtils.isEmpty(emailId)) {
                            requestFocus(connoteGenerationLayoutBinding.mEmail);
                            Toasty.error(ConnoteGenerationActivity.this, "Enter Email Id", Toast.LENGTH_SHORT, true).show();
                        } else if (isValidEmail(emailId) == false) {
                            requestFocus(connoteGenerationLayoutBinding.mEmail);
                            Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT, true).show();
                        } else if (paymentMode.equalsIgnoreCase("")) {
                            Toasty.error(ConnoteGenerationActivity.this, "Select Payment Mode", Toast.LENGTH_SHORT, true).show();
                        } else {
                            callCitGenerate();
                        }
                    }
                } else if (paymentMode.equalsIgnoreCase("FTC")) {
                    if (TextUtils.isEmpty(gstNumber)) {
                        requestFocus(connoteGenerationLayoutBinding.mGst);
                        Toasty.error(ConnoteGenerationActivity.this, "Enter GST Number", Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (codAmount.equalsIgnoreCase("0")) {
                            requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                            Toasty.error(ConnoteGenerationActivity.this, "Amount Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                        } else if (TextUtils.isEmpty(emailId)) {
                            requestFocus(connoteGenerationLayoutBinding.mEmail);
                            Toasty.error(ConnoteGenerationActivity.this, "Enter Email Id", Toast.LENGTH_SHORT, true).show();
                        } else if (isValidEmail(emailId) == false) {
                            requestFocus(connoteGenerationLayoutBinding.mEmail);
                            Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT, true).show();
                        } else if (paymentMode.equalsIgnoreCase("")) {
                            Toasty.error(ConnoteGenerationActivity.this, "Select Payment Mode", Toast.LENGTH_SHORT, true).show();
                        } else {
                            callCitGenerate();
                        }
                    }
                } else if (codAmount.equalsIgnoreCase("0")) {
                    requestFocus(connoteGenerationLayoutBinding.mCodAmount);
                    Toasty.error(ConnoteGenerationActivity.this, "Amount Cannot be Zero!!", Toast.LENGTH_SHORT, true).show();
                } else if (TextUtils.isEmpty(emailId)) {
                    requestFocus(connoteGenerationLayoutBinding.mEmail);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Email Id", Toast.LENGTH_SHORT, true).show();
                } else if (isValidEmail(emailId) == false) {
                    requestFocus(connoteGenerationLayoutBinding.mEmail);
                    Toasty.error(ConnoteGenerationActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT, true).show();
                } else if (paymentMode.equalsIgnoreCase("")) {
                    Toasty.error(ConnoteGenerationActivity.this, "Select Payment Mode", Toast.LENGTH_SHORT, true).show();
                } else {
                    callCitGenerate();
                }
            }
        });
        connoteGenerationLayoutBinding.mLength.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String apprValue = s.toString();
                noOfPieces = connoteGenerationLayoutBinding.mNoOfPcs.getText().toString();
                if (!apprValue.equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(apprValue) == 0) {
                            connoteGenerationLayoutBinding.mLength.setText("");
                            connoteGenerationLayoutBinding.mLength.setHint("H");
                            Toasty.error(ConnoteGenerationActivity.this, "Length Cannot be Zero", Toast.LENGTH_SHORT, true).show();
                        }
                    } catch (Exception e) {

                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        connoteGenerationLayoutBinding.mSpitPieces.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String apprValue = s.toString();
                noOfPieces = connoteGenerationLayoutBinding.mNoOfPcs.getText().toString();
                if (!apprValue.equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(apprValue) == 0) {
                            connoteGenerationLayoutBinding.mSpitPieces.setText("");
                            connoteGenerationLayoutBinding.mSpitPieces.setHint("Split Pieces");

                            Toasty.error(ConnoteGenerationActivity.this, "Split Piece Cannot be Zero", Toast.LENGTH_SHORT, true).show();
                        } else if (Integer.parseInt(apprValue) > Integer.parseInt(noOfPieces)) {
                            connoteGenerationLayoutBinding.mSpitPieces.setText("");
                            connoteGenerationLayoutBinding.mSpitPieces.setHint("Split Pieces");

                            Toasty.error(ConnoteGenerationActivity.this, "Split Piece Cannot be Greater than number of Pieces", Toast.LENGTH_SHORT, true).show();
                        }
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        connoteGenerationLayoutBinding.mNoOfPcs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String apprValue = s.toString();
                if (!apprValue.equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(apprValue) == 0) {
                            connoteGenerationLayoutBinding.mNoOfPcs.setText("");
                            connoteGenerationLayoutBinding.mNoOfPcs.setHint("No. of Pieces");

                            Toasty.error(ConnoteGenerationActivity.this, "Invoice Value Cannot be Zero", Toast.LENGTH_SHORT, true).show();
                        }
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        connoteGenerationLayoutBinding.mInvoiceValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String apprValue = s.toString();
                if (!apprValue.equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(apprValue) == 0) {
                            connoteGenerationLayoutBinding.mInvoiceValue.setText("");
                            connoteGenerationLayoutBinding.mInvoiceValue.setHint("Invoice Value");

                            Toasty.error(ConnoteGenerationActivity.this, "Invoice Value Cannot be Zero", Toast.LENGTH_SHORT, true).show();
                        }
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        connoteGenerationLayoutBinding.mCodAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String apprValue = s.toString();
                if (!apprValue.equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(apprValue) == 0) {
                            connoteGenerationLayoutBinding.mCodAmount.setText("");
                            connoteGenerationLayoutBinding.mCodAmount.setHint("Amount");

                            Toasty.error(ConnoteGenerationActivity.this, "Amount Cannot be Zero", Toast.LENGTH_SHORT, true).show();
                        }
                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        connoteGenerationLayoutBinding.mApproxWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String apprValue = s.toString();
                if (!apprValue.equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(apprValue) == 0) {
                            connoteGenerationLayoutBinding.mApproxWeight.setText("");
                            connoteGenerationLayoutBinding.mApproxWeight.setHint("Approximate Weight");

                            Toasty.error(ConnoteGenerationActivity.this, "Approximate Weight Cannot be Zero", Toast.LENGTH_SHORT, true).show();
                        }
                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        connoteGenerationLayoutBinding.mConsigneePincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pincode = s.toString();
                if (pincode.length() == 6) {
                    PincodeValidateRequest pincodeValidateRequest = new PincodeValidateRequest();
                    pincodeValidateRequest.setAction("pincode_validation");
                    pincodeValidateRequest.setUserId(userId);
                    pincodeValidateRequest.setPincode(pincode);
                    pincodeValidateRequest.setLatitude(String.valueOf(latitude));
                    pincodeValidateRequest.setLongitude(String.valueOf(longitude));
                    connoteGenerationViewModel.pincodeValidationRequest(pincodeValidateRequest);
                    mProgressBar.show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        callCriticalogServicesApi("false");
    }

    public void callGetAutoBoxType() {
        GetAutoLBHRequest getAutoLBHRequest = new GetAutoLBHRequest();
        getAutoLBHRequest.setAction("get_auto_boxes");
        getAutoLBHRequest.setUserId(userId);
        getAutoLBHRequest.setClientCode(clientCode);
        connoteGenerationViewModel.getAutoBoxTypeRequest(getAutoLBHRequest);
    }

    public void callCitGenerate() {
        ConnoteGenerationRequest connoteGenerationRequest = new ConnoteGenerationRequest();
        connoteGenerationRequest.setAction("generate_cit_against_brn");
        connoteGenerationRequest.setUserId(userId);
        connoteGenerationRequest.setBookingId(bookingId);
        connoteGenerationRequest.setReceiverName(consigneeName);
        connoteGenerationRequest.setAddress1(address1);
        connoteGenerationRequest.setAddress2(address2);
        connoteGenerationRequest.setAddress3(address3);
        connoteGenerationRequest.setReceiverPin(destPincode);
        connoteGenerationRequest.setReceiverMobile(consigneeMobile);
        connoteGenerationRequest.setOrderNumber(orderNumber);
        connoteGenerationRequest.setNoPcs(noOfPieces);
        connoteGenerationRequest.setWeight(approximateWeight);
        connoteGenerationRequest.setInvoiceValue(invoiceValue);
        connoteGenerationRequest.setProductMode(productCode);
        connoteGenerationRequest.setCodAmount(codAmount);
        connoteGenerationRequest.setEmail(emailId);
        connoteGenerationRequest.setReceiverGst(gstNumber);
        connoteGenerationRequest.setPayment_mode(paymentMode);
        connoteGenerationRequest.setlBHDetails(lbhModelList);
        connoteGenerationViewModel.connoteGenerationRequest(connoteGenerationRequest);
        mProgressBar.show();
    }

    public void getPaytypeMode() {
        GetClientPaytypeRequest getClientPaytypeRequest = new GetClientPaytypeRequest();
        getClientPaytypeRequest.setAction("get_client_paytype");
        getClientPaytypeRequest.setUserId(userId);
        getClientPaytypeRequest.setClientCode(clientCode);
        connoteGenerationViewModel.getPaymentTypeRequest(getClientPaytypeRequest);
        mProgressBar.show();
    }

    public void getConsineeByName() {
        getConsigneeBuCode = "flase";
        GetConsigneeByNameRequest getConsigneeByNameRequest = new GetConsigneeByNameRequest();
        getConsigneeByNameRequest.setAction("get_consignee_by_name");
        getConsigneeByNameRequest.setConsigneeName("");
        getConsigneeByNameRequest.setUserId(userId);
        getConsigneeByNameRequest.setClientCode(clientCode);
        getConsigneeByNameRequest.setDestPin(pincode);
        connoteGenerationViewModel.getConsigneeByNameRequest(getConsigneeByNameRequest);
        mProgressBar.show();
    }

    public void getConsineeByConsigneeCode() {
        getConsigneeBuCode = "true";
        GetConsigneeByNameRequest getConsigneeByNameRequest = new GetConsigneeByNameRequest();
        getConsigneeByNameRequest.setAction("get_consignee_by_name");
        getConsigneeByNameRequest.setConsigneeName("");
        getConsigneeByNameRequest.setUserId(userId);
        getConsigneeByNameRequest.setClientCode("");
        getConsigneeByNameRequest.setDestPin("");
        getConsigneeByNameRequest.setClientCode("");
        getConsigneeByNameRequest.setConsignee_code(cnorCode);

        connoteGenerationViewModel.getConsigneeByNameRequest(getConsigneeByNameRequest);
        mProgressBar.show();
    }

    public void requestFocus(View view) {

        view.requestFocus();
        // Scroll view to make the EditText field visible
        int[] location = new int[2];
        view.getLocationInWindow(location);
        int y = location[1];
        connoteGenerationLayoutBinding.mScrollView.scrollTo(0, view.getBottom());
        Window window = getWindow();
        // Set the soft input mode to adjust the dialog size when the keyboard is shown
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    public boolean isValidIndianMobileNumber(String number) {
        String regex = "^[6-9]\\d{9}$";
        // 6, 7, 8, or 9 followed by 9 digits
        return number.matches(regex);
    }

    public void callCriticalogServicesApi(String showDilog) {
        searchDataProductMode.clear();
        searchDataProductModeString.clear();
        datumServiceList.clear();
        CriticalogServicesRequest criticalogServicesRequest = new CriticalogServicesRequest();
        criticalogServicesRequest.setAction("criticalog_services_type");
        criticalogServicesRequest.setUserId(userId);
        criticalogServicesRequest.setClientCode(clientCode);
        connoteGenerationViewModel.criticalogServicesRequest(criticalogServicesRequest);
        mProgressBar.show();
        showDialogFlag = showDilog;
    }

    public void showProductModeSearchDialog(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(ConnoteGenerationActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        connoteGenerationLayoutBinding.mProductMode.setText(item.getTitle());
                        int index = searchDataProductModeString.indexOf(item.getTitle());
                        productCode = datumServiceList.get(index).getCode();
                        connoteGenerationLayoutBinding.mProductMode.setText(item.getTitle());
                        dialog.dismiss();
                    }
                }).show();
    }

    public static boolean isValidGSTIN(String gstin) {
        // GSTIN pattern: 2 uppercase letters followed by 10 digits and 1 checksum
        String gstinPattern = "^[A-Z]{2}\\d{2}[A-Z]{1}\\d{4}[A-Z]{1}\\d{5}$";
        return gstin.matches(gstinPattern);
    }

    public void connoteGenerationDialog(String message) {
        //Dialog Manual Entry
        TextView mDialogText, mOkDialog;
        ImageView mRightMark;
        dialogSuccess = new Dialog(ConnoteGenerationActivity.this);
        dialogSuccess.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = dialogSuccess.findViewById(R.id.mOk);
        mDialogText = dialogSuccess.findViewById(R.id.mDialogText);
        mRightMark = dialogSuccess.findViewById(R.id.mRightMark);
        mDialogText.setText(message);
        dialogSuccess.setCancelable(false);
        dialogSuccess.show();

        mRightMark.setVisibility(View.GONE);
        mDialogText.setText(message);

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSuccess.dismiss();
                mCriticalogSharedPreferences.saveData("navigationStatus", "con_generation");
                mCriticalogSharedPreferences.saveData("generated_con", message);
                Intent intent = new Intent(ConnoteGenerationActivity.this, BarcodeScanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    public static boolean isValidEmail(String email) {
        String regex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        return email.matches(regex);
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ConnoteGenerationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ConnoteGenerationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ConnoteGenerationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(ConnoteGenerationActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }
}
