package com.criticalog.ecritica.connotegeneration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DatumConsigneeName extends GetConsigneeByNameResponse {
    @SerializedName("cons_code")
    @Expose
    private String consCode;
    @SerializedName("cons_name")
    @Expose
    private String consName;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("address3")
    @Expose
    private String address3;
    @SerializedName("city_code")
    @Expose
    private String cityCode;
    @SerializedName("dest_pincode")
    @Expose
    private String destPincode;
    @SerializedName("gst_number")
    @Expose
    private String gstNumber;
    @SerializedName("cons_mobile")
    @Expose
    private String consMobile;
    @SerializedName("cons_email")
    @Expose
    private String consEmail;
    @SerializedName("pay_mode")
    @Expose
    private List<String> payMode;

    public String getConsCode() {
        return consCode;
    }

    public void setConsCode(String consCode) {
        this.consCode = consCode;
    }

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDestPincode() {
        return destPincode;
    }

    public void setDestPincode(String destPincode) {
        this.destPincode = destPincode;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getConsMobile() {
        return consMobile;
    }

    public void setConsMobile(String consMobile) {
        this.consMobile = consMobile;
    }

    public String getConsEmail() {
        return consEmail;
    }

    public void setConsEmail(String consEmail) {
        this.consEmail = consEmail;
    }

    public List<String> getPayMode() {
        return payMode;
    }

    public void setPayMode(List<String> payMode) {
        this.payMode = payMode;
    }

}
