package com.criticalog.ecritica.connotegeneration.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationRequest;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationResponse;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesRequest;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesResponse;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHRequest;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHResponse;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeRequest;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeResponse;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameRequest;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameResponse;
import com.criticalog.ecritica.connotegeneration.repository.ConnoteGenerationRespo;

public class ConnoteGenerationViewModel extends AndroidViewModel {
    private ConnoteGenerationRespo connoteGenerationRespo;
    private LiveData<PincodeValidateResponse> pincodeValidateResponseLiveData;
    private LiveData<ConnoteGenerationResponse> connoteGenerationResponseLiveData;
    private LiveData<CriticalogServicesResponse> criticalogServicesResponseLiveData;
    private LiveData<GetConsigneeByNameResponse> getConsigneeByNameResponseLiveData;
    private LiveData<GetClientPaytypeResponse> getClientPaytypeResponseLiveData;
    private LiveData<GetAutoLBHResponse> getAutoLBHResponseLiveData;

    public ConnoteGenerationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        connoteGenerationRespo = new ConnoteGenerationRespo();
        pincodeValidateResponseLiveData = connoteGenerationRespo.getPincodeValidationLiveData();
        connoteGenerationResponseLiveData = connoteGenerationRespo.connoteGenerationResponseLiveData();
        criticalogServicesResponseLiveData = connoteGenerationRespo.criticalogServicesResponseLiveData();
        getConsigneeByNameResponseLiveData = connoteGenerationRespo.getConsigneeByNameResponseLiveData();
        getClientPaytypeResponseLiveData = connoteGenerationRespo.getClientPaytypeResponseLiveData();
        getAutoLBHResponseLiveData=connoteGenerationRespo.getAutoLBHResponseLiveData();
    }

    //PINCODE VALIDATION
    public void pincodeValidationRequest(PincodeValidateRequest pincodeValidateRequest) {
        connoteGenerationRespo.pincodeValidationRequest(pincodeValidateRequest);
    }

    public LiveData<PincodeValidateResponse> pincodeValidateResponseLiveData() {
        return pincodeValidateResponseLiveData;
    }

    //CONNOTE GENERATION
    public void connoteGenerationRequest(ConnoteGenerationRequest connoteGenerationRequest) {
        connoteGenerationRespo.connoteGenerationRequest(connoteGenerationRequest);
    }

    public LiveData<ConnoteGenerationResponse> connoteGenerationResponseLiveData() {
        return connoteGenerationResponseLiveData;
    }

    //CRITICALOG SERVICES API CALL
    public void criticalogServicesRequest(CriticalogServicesRequest criticalogServicesRequest) {
        connoteGenerationRespo.criticalogServicesRequest(criticalogServicesRequest);
    }

    public LiveData<CriticalogServicesResponse> criticalogServicesResponseLiveData() {
        return criticalogServicesResponseLiveData;
    }

    //Get Consignee By Name API call

    public void getConsigneeByNameRequest(GetConsigneeByNameRequest getConsigneeByNameRequest) {
        connoteGenerationRespo.getConsigneeeByName(getConsigneeByNameRequest);
    }

    public LiveData<GetConsigneeByNameResponse> getConsigneeByNameResponseLiveData() {
        return getConsigneeByNameResponseLiveData;
    }

    //Get Payment Type Mode
    public void getPaymentTypeRequest(GetClientPaytypeRequest getClientPaytypeRequest) {
        connoteGenerationRespo.getPayMentType(getClientPaytypeRequest);
    }

    public LiveData<GetClientPaytypeResponse> getPaymentTypeResponseLiveData() {
        return getClientPaytypeResponseLiveData;
    }

    //Get Auto BoxType

    public void getAutoBoxTypeRequest(GetAutoLBHRequest getAutoLBHRequest) {
        connoteGenerationRespo.getAutoBoxType(getAutoLBHRequest);
    }

    public LiveData<GetAutoLBHResponse> getAutoBoxTypeResponseLiveData() {
        return getAutoLBHResponseLiveData;
    }
}
