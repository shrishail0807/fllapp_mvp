package com.criticalog.ecritica.connotegeneration.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationRequest;
import com.criticalog.ecritica.connotegeneration.model.ConnoteGenerationResponse;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesRequest;
import com.criticalog.ecritica.connotegeneration.model.CriticalogServicesResponse;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHRequest;
import com.criticalog.ecritica.connotegeneration.model.GetAutoLBHResponse;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeRequest;
import com.criticalog.ecritica.connotegeneration.model.GetClientPaytypeResponse;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameRequest;
import com.criticalog.ecritica.connotegeneration.model.GetConsigneeByNameResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnoteGenerationRespo {
    private RestServices services;
    private MutableLiveData<PincodeValidateResponse> pincodeValidateResponseMutableLiveData;
    private MutableLiveData<ConnoteGenerationResponse> connoteGenerationResponseMutableLiveData;
    private MutableLiveData<CriticalogServicesResponse> criticalogServicesResponseMutableLiveData;
    private MutableLiveData<GetConsigneeByNameResponse> consigneeByNameResponseMutableLiveData;
    private MutableLiveData<GetClientPaytypeResponse> clientPaytypeResponseMutableLiveData;
    private MutableLiveData<GetAutoLBHResponse> autoLBHResponseMutableLiveData;

    public ConnoteGenerationRespo() {
        services = RestClient.getRetrofitInstance().create(RestServices.class);
        pincodeValidateResponseMutableLiveData = new MutableLiveData<>();
        connoteGenerationResponseMutableLiveData = new MutableLiveData<>();
        criticalogServicesResponseMutableLiveData = new MutableLiveData<>();
        consigneeByNameResponseMutableLiveData=new MutableLiveData<>();
        clientPaytypeResponseMutableLiveData=new MutableLiveData<>();
        autoLBHResponseMutableLiveData=new MutableLiveData<>();
    }

    //PINCODE VALIDATION API CALL
    public void pincodeValidationRequest(PincodeValidateRequest pincodeValidateRequest) {
        Call<PincodeValidateResponse> pincodeValidateResponseCall = services.pincodeValidation(pincodeValidateRequest);
        pincodeValidateResponseCall.enqueue(new Callback<PincodeValidateResponse>() {
            @Override
            public void onResponse(Call<PincodeValidateResponse> call, Response<PincodeValidateResponse> response) {
                pincodeValidateResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<PincodeValidateResponse> call, Throwable t) {
                pincodeValidateResponseMutableLiveData.setValue(null);
            }
        });
    }

    //CONNOTE GENERATION API CALL
    public void connoteGenerationRequest(ConnoteGenerationRequest connoteGenerationRequest) {
        Call<ConnoteGenerationResponse> connoteGenerationResponseCall = services.connoteGeneration(connoteGenerationRequest);
        connoteGenerationResponseCall.enqueue(new Callback<ConnoteGenerationResponse>() {
            @Override
            public void onResponse(Call<ConnoteGenerationResponse> call, Response<ConnoteGenerationResponse> response) {
                connoteGenerationResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ConnoteGenerationResponse> call, Throwable t) {
                connoteGenerationResponseMutableLiveData.setValue(null);
            }
        });
    }

    // CRITICAL SERVICES API CALL
    public void criticalogServicesRequest(CriticalogServicesRequest criticalogServicesRequest) {

        Call<CriticalogServicesResponse> criticalogServicesResponseCall = services.criticalogServices(criticalogServicesRequest);
        criticalogServicesResponseCall.enqueue(new Callback<CriticalogServicesResponse>() {
            @Override
            public void onResponse(Call<CriticalogServicesResponse> call, Response<CriticalogServicesResponse> response) {
                criticalogServicesResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<CriticalogServicesResponse> call, Throwable t) {
                criticalogServicesResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getConsigneeeByName(GetConsigneeByNameRequest getConsigneeByNameRequest){
        Call<GetConsigneeByNameResponse> getConsigneeByNameResponseCall=services.getConsigneeByName(getConsigneeByNameRequest);
        getConsigneeByNameResponseCall.enqueue(new Callback<GetConsigneeByNameResponse>() {
            @Override
            public void onResponse(Call<GetConsigneeByNameResponse> call, Response<GetConsigneeByNameResponse> response) {
                consigneeByNameResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetConsigneeByNameResponse> call, Throwable t) {
                consigneeByNameResponseMutableLiveData.setValue(null);
            }
        });
    }
// Get Payment Mode API Call
    public void getPayMentType(GetClientPaytypeRequest getClientPaytypeRequest)
    {
        Call<GetClientPaytypeResponse> getClientPaytypeResponseCall=services.getPayType(getClientPaytypeRequest);
        getClientPaytypeResponseCall.enqueue(new Callback<GetClientPaytypeResponse>() {
            @Override
            public void onResponse(Call<GetClientPaytypeResponse> call, Response<GetClientPaytypeResponse> response) {
                clientPaytypeResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetClientPaytypeResponse> call, Throwable t) {

                clientPaytypeResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getAutoBoxType(GetAutoLBHRequest getAutoLBHRequest)
    {

        Call<GetAutoLBHResponse> getAutoLBHResponseCall= services.getAutoBoxType(getAutoLBHRequest);
        getAutoLBHResponseCall.enqueue(new Callback<GetAutoLBHResponse>() {
            @Override
            public void onResponse(Call<GetAutoLBHResponse> call, Response<GetAutoLBHResponse> response) {
                autoLBHResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetAutoLBHResponse> call, Throwable t) {
                autoLBHResponseMutableLiveData.setValue(null);
            }
        });
    }
    //CONNITE GENERATION
    public LiveData<ConnoteGenerationResponse> connoteGenerationResponseLiveData() {
        return connoteGenerationResponseMutableLiveData;
    }

    //PINCODE VALIDATION
    public LiveData<PincodeValidateResponse> getPincodeValidationLiveData() {
        return pincodeValidateResponseMutableLiveData;
    }

    //CRITICALOG SERVICES
    public LiveData<CriticalogServicesResponse> criticalogServicesResponseLiveData() {
        return criticalogServicesResponseMutableLiveData;
    }

    public LiveData<GetConsigneeByNameResponse> getConsigneeByNameResponseLiveData()
    {
        return consigneeByNameResponseMutableLiveData;
    }

    //PAYMET TYPE
    public LiveData<GetClientPaytypeResponse> getClientPaytypeResponseLiveData()
    {
        return clientPaytypeResponseMutableLiveData;
    }

    //Get Auto Box Type
    public LiveData<GetAutoLBHResponse> getAutoLBHResponseLiveData()
    {
        return autoLBHResponseMutableLiveData;
    }
}
