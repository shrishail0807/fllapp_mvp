package com.criticalog.ecritica.connotegeneration.model;

import com.criticalog.ecritica.MVPPickup.HeaderData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetConsigneeByNameResponse {
    @SerializedName("header")
    @Expose
    private HeaderData header;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DatumConsigneeName> data;

    public HeaderData getHeader() {
        return header;
    }

    public void setHeader(HeaderData header) {
        this.header = header;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumConsigneeName> getData() {
        return data;
    }

    public void setData(List<DatumConsigneeName> data) {
        this.data = data;
    }
}
