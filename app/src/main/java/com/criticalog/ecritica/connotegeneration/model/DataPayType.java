package com.criticalog.ecritica.connotegeneration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPayType {
    @SerializedName("pay_mode")
    @Expose
    private List<String> payMode;

    public List<String> getPayMode() {
        return payMode;
    }

    public void setPayMode(List<String> payMode) {
        this.payMode = payMode;
    }
}
