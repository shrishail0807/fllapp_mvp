package com.criticalog.ecritica.connotegeneration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LBHModel {

    @SerializedName("auto_box")
    @Expose
    private String autobox;
    @SerializedName("Length")
    @Expose
    private String length;

    @SerializedName("Breadth")
    @Expose
    private String breadth;

    @SerializedName("Height")
    @Expose
    private String height;

    @SerializedName("SplitPcs")
    @Expose
    private String split_pieces;

    public String getAutobox() {
        return autobox;
    }

    public void setAutobox(String autobox) {
        this.autobox = autobox;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getSplit_pieces() {
        return split_pieces;
    }

    public void setSplit_pieces(String split_pieces) {
        this.split_pieces = split_pieces;
    }
}
