package com.criticalog.ecritica.connotegeneration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.connotegeneration.model.DatumConsigneeName;
import com.criticalog.ecritica.connotegeneration.model.DatumLBHWithBoxCode;

import java.util.ArrayList;
import java.util.List;


public class CustomAutoBoxAdapter extends ArrayAdapter<DatumLBHWithBoxCode> {

    List<DatumLBHWithBoxCode> userDetails, tempUserDetails, suggestions;

    public CustomAutoBoxAdapter(Context context, List<DatumLBHWithBoxCode> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.userDetails = objects;
        this.tempUserDetails = new ArrayList<DatumLBHWithBoxCode>(objects);
        this.suggestions = new ArrayList<DatumLBHWithBoxCode>(objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DatumLBHWithBoxCode currentUserDetails = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_details_consignee_details, parent, false);
        }
        TextView consigneeName = convertView.findViewById(R.id.mTvConsigneeName);
        //TextView consigneeeAdrees = convertView.findViewById(R.id.mTvConsigneeAddress);

        if (currentUserDetails != null) {
            if (consigneeName != null) {
                consigneeName.setText(currentUserDetails.getBoxCode());
            }

           /* if (consigneeeAdrees != null) {
                consigneeeAdrees.setText(currentUserDetails.getAddress1() + "," + currentUserDetails.getAddress2() + ", " + currentUserDetails.getAddress3() + ", " + currentUserDetails.getDestPincode());
            }*/
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }


    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            DatumLBHWithBoxCode userDetails = (DatumLBHWithBoxCode) resultValue;
            return userDetails.getBoxCode();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (DatumLBHWithBoxCode userDetails : tempUserDetails) {
                    if (userDetails.getBoxCode().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(userDetails);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<DatumLBHWithBoxCode> c = (ArrayList<DatumLBHWithBoxCode>) results.values;
            if (results != null && results.count > 0) {
                clear();
                try {
                    for (DatumLBHWithBoxCode cust : c) {
                        add(cust);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {

                }

            }
        }
    };
}
