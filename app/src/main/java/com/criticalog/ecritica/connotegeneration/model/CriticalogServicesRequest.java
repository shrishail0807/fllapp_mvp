package com.criticalog.ecritica.connotegeneration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CriticalogServicesRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("client_code")
    @Expose
    private String clientCode;

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
