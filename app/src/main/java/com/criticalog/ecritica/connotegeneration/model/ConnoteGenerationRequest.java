package com.criticalog.ecritica.connotegeneration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConnoteGenerationRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("receiver_name")
    @Expose
    private String receiverName;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("address3")
    @Expose
    private String address3;
    @SerializedName("receiver_pin")
    @Expose
    private String receiverPin;
    @SerializedName("receiver_mobile")
    @Expose
    private String receiverMobile;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("no_pcs")
    @Expose
    private String noPcs;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("invoice_value")
    @Expose
    private String invoiceValue;
    @SerializedName("product_mode")
    @Expose
    private String productMode;
    @SerializedName("cod_amount")
    @Expose
    private String codAmount;
    @SerializedName("cit_on_email")
    @Expose
    private String email;

    @SerializedName("ReceiverGst")
    @Expose
    private String ReceiverGst;

    @SerializedName("payment_mode")
    @Expose
    private String payment_mode;

    @SerializedName("LBHDetails")
    @Expose
    private List<LBHModel> lBHDetails;

    public List<LBHModel> getlBHDetails() {
        return lBHDetails;
    }

    public void setlBHDetails(List<LBHModel> lBHDetails) {
        this.lBHDetails = lBHDetails;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getReceiverGst() {
        return ReceiverGst;
    }

    public void setReceiverGst(String receiverGst) {
        ReceiverGst = receiverGst;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getReceiverPin() {
        return receiverPin;
    }

    public void setReceiverPin(String receiverPin) {
        this.receiverPin = receiverPin;
    }

    public String getReceiverMobile() {
        return receiverMobile;
    }

    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getNoPcs() {
        return noPcs;
    }

    public void setNoPcs(String noPcs) {
        this.noPcs = noPcs;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(String invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getProductMode() {
        return productMode;
    }

    public void setProductMode(String productMode) {
        this.productMode = productMode;
    }

    public String getCodAmount() {
        return codAmount;
    }

    public void setCodAmount(String codAmount) {
        this.codAmount = codAmount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
