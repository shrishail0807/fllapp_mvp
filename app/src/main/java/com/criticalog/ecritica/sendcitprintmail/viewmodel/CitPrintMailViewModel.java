package com.criticalog.ecritica.sendcitprintmail.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentResponse;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentsRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberResponse;
import com.criticalog.ecritica.sendcitprintmail.repository.CitPrintSendMailRepo;

public class CitPrintMailViewModel extends AndroidViewModel {
    private CitPrintSendMailRepo citPrintSendMailRepo;
    private LiveData<GetDocketByBookingNumberResponse> docketByBookingNumberResponseLiveData;
    private LiveData<GetCITAttachmentResponse> citAttachmentResponseLiveData;

    public CitPrintMailViewModel(@NonNull Application application) {
        super(application);
    }
    public void init() {
        citPrintSendMailRepo = new CitPrintSendMailRepo();
        docketByBookingNumberResponseLiveData = citPrintSendMailRepo.getDocketDetailsByBookingId();
        citAttachmentResponseLiveData=citPrintSendMailRepo.getSendCitAttchments();
    }
    //Get Docket By Booking Id API Call
    public void getDocketsByBookingId(GetDocketByBookingNumberRequest getDocketByBookingNumberRequest) {
        citPrintSendMailRepo.getDocketListByBookingId(getDocketByBookingNumberRequest);
    }

    public LiveData<GetDocketByBookingNumberResponse> getDocketsByBookingIdLiveData() {
        return docketByBookingNumberResponseLiveData;
    }

    //Send CIT Attachments API Call
    public void getSendCitAttachments(GetCITAttachmentsRequest getCITAttachmentsRequest) {
        citPrintSendMailRepo.getCitAttachmentsRequest(getCITAttachmentsRequest);
    }

    public LiveData<GetCITAttachmentResponse> getSendCitAttachemetsLiveData() {
        return citAttachmentResponseLiveData;
    }
}
