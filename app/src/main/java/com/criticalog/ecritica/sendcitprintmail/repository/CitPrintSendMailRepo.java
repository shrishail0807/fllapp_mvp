package com.criticalog.ecritica.sendcitprintmail.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.mvpdrspreparation.model.AssignDRSResponse;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentResponse;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentsRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitPrintSendMailRepo {
    private RestServices mRestServices;
    private MutableLiveData<GetDocketByBookingNumberResponse> docketByBookingNumberResponseMutableLiveData;
    private MutableLiveData<GetCITAttachmentResponse> citAttachmentResponseMutableLiveData;


    public CitPrintSendMailRepo() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        docketByBookingNumberResponseMutableLiveData = new MutableLiveData<>();
        citAttachmentResponseMutableLiveData = new MutableLiveData<>();
    }

    public void getDocketListByBookingId(GetDocketByBookingNumberRequest getDocketByBookingNumberRequest) {
        Call<GetDocketByBookingNumberResponse> getDocketByBookingNumberResponseCall = mRestServices.getDoccketListByBokkingId(getDocketByBookingNumberRequest);
        getDocketByBookingNumberResponseCall.enqueue(new Callback<GetDocketByBookingNumberResponse>() {
            @Override
            public void onResponse(Call<GetDocketByBookingNumberResponse> call, Response<GetDocketByBookingNumberResponse> response) {
                docketByBookingNumberResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetDocketByBookingNumberResponse> call, Throwable t) {
                docketByBookingNumberResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getCitAttachmentsRequest(GetCITAttachmentsRequest getCITAttachmentsRequest) {
        Call<GetCITAttachmentResponse> citAttachmentResponseCall = mRestServices.sendCitAttachments(getCITAttachmentsRequest);
        citAttachmentResponseCall.enqueue(new Callback<GetCITAttachmentResponse>() {
            @Override
            public void onResponse(Call<GetCITAttachmentResponse> call, Response<GetCITAttachmentResponse> response) {
                citAttachmentResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetCITAttachmentResponse> call, Throwable t) {
                citAttachmentResponseMutableLiveData.setValue(null);
            }
        });
    }

    public LiveData<GetDocketByBookingNumberResponse> getDocketDetailsByBookingId() {
        return docketByBookingNumberResponseMutableLiveData;
    }

    public LiveData<GetCITAttachmentResponse> getSendCitAttchments() {
        return citAttachmentResponseMutableLiveData;
    }
}
