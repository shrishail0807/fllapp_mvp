package com.criticalog.ecritica.sendcitprintmail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCITAttachmentsRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("dockets")
    @Expose
    private List<String> dockets;
    @SerializedName("emailid")
    @Expose
    private String emailid;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<String> getDockets() {
        return dockets;
    }

    public void setDockets(List<String> dockets) {
        this.dockets = dockets;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
