package com.criticalog.ecritica.sendcitprintmail.adapter;

import android.content.Context;
import android.view.ContentInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.sendcitprintmail.iConnoteCheckOrUncheck;

import java.util.List;

public class ConnoteSelectionAdapter extends RecyclerView.Adapter<ConnoteSelectionAdapter.ConnoteSelectionViewHolder> {

    private Context context;
    List<String> stringListDockets;
    private iConnoteCheckOrUncheck connoteCheckOrUncheck;
    private String selectAll;
    public ConnoteSelectionAdapter(Context context,List<String> stringListDockets,String selectAll,iConnoteCheckOrUncheck connoteCheckOrUncheck) {

        this.context=context;
        this.stringListDockets=stringListDockets;
        this.connoteCheckOrUncheck=connoteCheckOrUncheck;
        this.selectAll=selectAll;
    }

    @NonNull
    @Override
    public ConnoteSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.con_selection_cit_item, parent, false);
        return new ConnoteSelectionAdapter.ConnoteSelectionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ConnoteSelectionViewHolder holder, int position) {

        if(selectAll.equalsIgnoreCase("true"))
        {
            holder.mDocketCheckbox.setChecked(true);
        }else {
            holder.mDocketCheckbox.setChecked(false);
        }
        holder.mConnote.setText(stringListDockets.get(position).toString());

        holder.mDocketCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if(holder.mDocketCheckbox.isChecked())
                {
                    connoteCheckOrUncheck.iConnoteCheckOrUncheck(position,stringListDockets.get(position).toString(),"true");
                }else {
                    connoteCheckOrUncheck.iConnoteCheckOrUncheck(position,stringListDockets.get(position).toString(),"false");
                }*/
                if(position>=stringListDockets.size())
                {
                    Toast.makeText(context, "Please Wait!!", Toast.LENGTH_SHORT).show();
                }else {
                    if(holder.mDocketCheckbox.isChecked())
                    {
                        connoteCheckOrUncheck.iConnoteCheckOrUncheck(position,stringListDockets.get(position).toString(),"true");
                    }else {
                        connoteCheckOrUncheck.iConnoteCheckOrUncheck(position,stringListDockets.get(position).toString(),"false");
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringListDockets.size();
    }

    public class ConnoteSelectionViewHolder extends RecyclerView.ViewHolder {
        private CheckBox mDocketCheckbox;
        private TextView mConnote;
        public ConnoteSelectionViewHolder(@NonNull View itemView) {
            super(itemView);

            mDocketCheckbox=itemView.findViewById(R.id.mDockekCheckbox);
            mConnote=itemView.findViewById(R.id.mConnote);
        }
    }
}
