package com.criticalog.ecritica.sendcitprintmail;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.databinding.CitPrintMailGenerateBinding;
import com.criticalog.ecritica.sendcitprintmail.adapter.ConnoteSelectionAdapter;
import com.criticalog.ecritica.sendcitprintmail.adapter.CustomAutoSuggestBookingIdAdapter;
import com.criticalog.ecritica.sendcitprintmail.model.DatumBookingId;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentResponse;
import com.criticalog.ecritica.sendcitprintmail.model.GetCITAttachmentsRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberRequest;
import com.criticalog.ecritica.sendcitprintmail.model.GetDocketByBookingNumberResponse;
import com.criticalog.ecritica.sendcitprintmail.viewmodel.CitPrintMailViewModel;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class CitPrintMailGenerationActivity extends AppCompatActivity implements iConnoteCheckOrUncheck {
    private CitPrintMailGenerateBinding citPrintMailGenerateBinding;
    private ConnoteSelectionAdapter connoteSelectionAdapter;
    private CitPrintMailViewModel citPrintMailViewModel;
    private List<DatumBookingId> datumBookingIdsList = new ArrayList<>();
    private CustomAutoSuggestBookingIdAdapter customAutoSuggestBookingIdAdapter;
    private String selectedBookingId = "", userId = "";
    private List<String> stringListDockets = new ArrayList<>();
    private List<String> selectedConnotes = new ArrayList<>();
    private SimpleArcDialog mProgressBar;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        citPrintMailGenerateBinding = DataBindingUtil.setContentView(this, R.layout.cit_print_mail_generate);

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);
        citPrintMailViewModel = ViewModelProviders.of(this).get(CitPrintMailViewModel.class);
        citPrintMailViewModel.init();

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        userId = mCriticalogSharedPreferences.getData("userId");

        GetDocketByBookingNumberRequest getDocketByBookingNumberRequest = new GetDocketByBookingNumberRequest();
        getDocketByBookingNumberRequest.setAction("get_dockets_by_bookingid");
        getDocketByBookingNumberRequest.setUserId(userId);
        citPrintMailViewModel.getDocketsByBookingId(getDocketByBookingNumberRequest);
        mProgressBar.show();
        citPrintMailGenerateBinding.mBookingId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                citPrintMailGenerateBinding.mBookingId.showDropDown();
            }
        });

        citPrintMailGenerateBinding.mSubmitGetMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = citPrintMailGenerateBinding.mEmail.getText().toString();
                String bookingId = citPrintMailGenerateBinding.mBookingId.getText().toString();
                if (bookingId.equalsIgnoreCase("")) {
                    requestFocus(citPrintMailGenerateBinding.mBookingId);
                    Toasty.error(CitPrintMailGenerationActivity.this, "Select Booking Id", Toasty.LENGTH_SHORT, true).show();
                } else if (selectedBookingId.equalsIgnoreCase("")) {
                    requestFocus(citPrintMailGenerateBinding.mBookingId);
                    Toasty.error(CitPrintMailGenerationActivity.this, "Select Booking Id", Toasty.LENGTH_SHORT, true).show();
                } else if (email.equalsIgnoreCase("")) {
                    requestFocus(citPrintMailGenerateBinding.mEmail);
                    Toasty.error(CitPrintMailGenerationActivity.this, "Enter Email Id", Toasty.LENGTH_SHORT, true).show();
                } else if (isValidEmail(email) == false) {
                    requestFocus(citPrintMailGenerateBinding.mEmail);
                    Toasty.error(CitPrintMailGenerationActivity.this, "Enter Valid Email Id", Toasty.LENGTH_SHORT, true).show();
                } else if (selectedConnotes.size() == 0) {
                    Toasty.error(CitPrintMailGenerationActivity.this, "Select Minimum one connote to Proceed!!", Toasty.LENGTH_SHORT, true).show();
                } else {
                    GetCITAttachmentsRequest getCITAttachmentsRequest = new GetCITAttachmentsRequest();
                    getCITAttachmentsRequest.setAction("send_cit_attachments");
                    getCITAttachmentsRequest.setEmailid(email);
                    getCITAttachmentsRequest.setUserId(userId);
                    getCITAttachmentsRequest.setDockets(selectedConnotes);
                    citPrintMailViewModel.getSendCitAttachments(getCITAttachmentsRequest);
                    mProgressBar.show();
                }
            }
        });
        citPrintMailGenerateBinding.mSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stringListDockets.size() > 0) {

                    if (citPrintMailGenerateBinding.mSelectAll.isChecked()) {
                        selectedConnotes.addAll(stringListDockets);
                        connoteSelectionAdapter = new ConnoteSelectionAdapter(CitPrintMailGenerationActivity.this, stringListDockets, "true", CitPrintMailGenerationActivity.this);
                    } else {
                        selectedConnotes = new ArrayList<>();
                        connoteSelectionAdapter = new ConnoteSelectionAdapter(CitPrintMailGenerationActivity.this, stringListDockets, "false", CitPrintMailGenerationActivity.this);
                    }
                    citPrintMailGenerateBinding.mRvConnote.setLayoutManager(new GridLayoutManager(CitPrintMailGenerationActivity.this, 2));
                    citPrintMailGenerateBinding.mRvConnote.setAdapter(connoteSelectionAdapter);
                    //connoteSelectionAdapter.notifyDataSetChanged();
                }
            }
        });
        citPrintMailGenerateBinding.mBookingId.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                String selection = datumBookingIdsList.get(position).getBookingId();
                int pos = -1;

                for (int i = 0; i < datumBookingIdsList.size(); i++) {
                    if (datumBookingIdsList.get(i).getBookingId().equals(selection)) {
                        pos = i;
                        break;
                    }
                }
                citPrintMailGenerateBinding.mEmailText.setVisibility(View.VISIBLE);
                citPrintMailGenerateBinding.mEmail.setVisibility(View.VISIBLE);
                citPrintMailGenerateBinding.mRvDocketText.setVisibility(View.VISIBLE);
                citPrintMailGenerateBinding.mRvConnote.setVisibility(View.VISIBLE);
                citPrintMailGenerateBinding.mSelectAll.setVisibility(View.VISIBLE);
                stringListDockets = datumBookingIdsList.get(pos).getDockets();
                selectedBookingId = selection;
                selectedConnotes = new ArrayList<>();
                if (stringListDockets.size() > 0) {
                    connoteSelectionAdapter = new ConnoteSelectionAdapter(CitPrintMailGenerationActivity.this, stringListDockets, "false", CitPrintMailGenerationActivity.this);
                    citPrintMailGenerateBinding.mRvConnote.setLayoutManager(new GridLayoutManager(CitPrintMailGenerationActivity.this, 2));
                    citPrintMailGenerateBinding.mRvConnote.setAdapter(connoteSelectionAdapter);
                }
            }
        });

        citPrintMailViewModel.getSendCitAttachemetsLiveData().observe(this, new Observer<GetCITAttachmentResponse>() {
            @Override
            public void onChanged(GetCITAttachmentResponse getCITAttachmentResponse) {
                mProgressBar.dismiss();
                if (getCITAttachmentResponse.getStatus() == 200) {
                    Toasty.success(CitPrintMailGenerationActivity.this, getCITAttachmentResponse.getMessage(), Toasty.LENGTH_SHORT, true).show();
                    finish();
                } else {
                    Toasty.error(CitPrintMailGenerationActivity.this, getCITAttachmentResponse.getMessage(), Toasty.LENGTH_SHORT, true).show();
                }
            }
        });
        citPrintMailViewModel.getDocketsByBookingIdLiveData().observe(this, new Observer<GetDocketByBookingNumberResponse>() {
            @Override
            public void onChanged(GetDocketByBookingNumberResponse getDocketByBookingNumberResponse) {
                mProgressBar.dismiss();
                if (getDocketByBookingNumberResponse.getStatus() == 200) {

                    datumBookingIdsList = getDocketByBookingNumberResponse.getData();
                    customAutoSuggestBookingIdAdapter = new CustomAutoSuggestBookingIdAdapter(CitPrintMailGenerationActivity.this,
                            getDocketByBookingNumberResponse.getData());


                    citPrintMailGenerateBinding.mBookingId.setAdapter(customAutoSuggestBookingIdAdapter);
                    Toasty.warning(CitPrintMailGenerationActivity.this, getDocketByBookingNumberResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                } else {
                    Toasty.warning(CitPrintMailGenerationActivity.this, getDocketByBookingNumberResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
            }
        });

        citPrintMailGenerateBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void requestFocus(View view) {

        view.requestFocus();
        // Scroll view to make the EditText field visible
        int[] location = new int[2];
        view.getLocationInWindow(location);
        int y = location[1];
        citPrintMailGenerateBinding.mScrollView.scrollTo(0, view.getBottom());
        Window window = getWindow();
        // Set the soft input mode to adjust the dialog size when the keyboard is shown
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    public static boolean isValidEmail(String email) {
        String regex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        return email.matches(regex);
    }

    @Override
    public void iConnoteCheckOrUncheck(int position, String connote, String checkUnchek) {

        if (checkUnchek.equalsIgnoreCase("true")) {
            if (selectedConnotes.contains(connote)) {
                Toast.makeText(this, "Alrady Added", Toast.LENGTH_SHORT).show();
            } else {
                selectedConnotes.add(connote);
            }

        } else {
            if (selectedConnotes.size() > 0) {
                if (selectedConnotes.contains(connote)) {
                    selectedConnotes.remove(connote);
                }
            }
        }
    }
}
