package com.criticalog.ecritica.sendcitprintmail.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.sendcitprintmail.model.DatumBookingId;

import java.util.ArrayList;
import java.util.List;

public class CustomAutoSuggestBookingIdAdapter extends ArrayAdapter<DatumBookingId> {

    List<DatumBookingId> userDetails, tempUserDetails, suggestions;

    public CustomAutoSuggestBookingIdAdapter(Context context, List<DatumBookingId> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.userDetails = objects;
        this.tempUserDetails = new ArrayList<DatumBookingId>(objects);
        this.suggestions = new ArrayList<DatumBookingId>(objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DatumBookingId currentUserDetails = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_details_consignee_details, parent, false);
        }
        TextView consigneeName = convertView.findViewById(R.id.mTvConsigneeName);
        //TextView consigneeeAdrees = convertView.findViewById(R.id.mTvConsigneeAddress);

        if (currentUserDetails != null) {
            if (consigneeName != null) {
                consigneeName.setText(currentUserDetails.getBookingId() + "-" + currentUserDetails.getClientName());
            }

           /* if (consigneeeAdrees != null) {
                consigneeeAdrees.setText(currentUserDetails.getAddress1() + "," + currentUserDetails.getAddress2() + ", " + currentUserDetails.getAddress3() + ", " + currentUserDetails.getDestPincode());
            }*/
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }


    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            DatumBookingId userDetails = (DatumBookingId) resultValue;
            return userDetails.getBookingId();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (DatumBookingId userDetails : tempUserDetails) {
                    if (userDetails.getBookingId().toLowerCase().contains(constraint.toString().toLowerCase())
                            || userDetails.getClientName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(userDetails);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<DatumBookingId> c = (ArrayList<DatumBookingId>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (DatumBookingId cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
