package com.criticalog.ecritica.MVPOTP.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OTPRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("otp_mobile")
    @Expose
    private String otpMobile;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("is_resend")
    @Expose
    private String isResend;
    @SerializedName("round")
    @Expose
    private String round;
    @SerializedName("consignee")
    @Expose
    private String consignee;
    @SerializedName("refcount")
    @Expose
    private String refcount;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getDocket_no() {
        return docket_no;
    }

    public void setDocket_no(String docket_no) {
        this.docket_no = docket_no;
    }

    @SerializedName("docket_no")
    @Expose
    private String docket_no;



    public String getFor_type() {
        return for_type;
    }

    public void setFor_type(String for_type) {
        this.for_type = for_type;
    }

    @SerializedName("for_type")
    @Expose
    private String for_type;


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOtpMobile() {
        return otpMobile;
    }

    public void setOtpMobile(String otpMobile) {
        this.otpMobile = otpMobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getIsResend() {
        return isResend;
    }

    public void setIsResend(String isResend) {
        this.isResend = isResend;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getRefcount() {
        return refcount;
    }

    public void setRefcount(String refcount) {
        this.refcount = refcount;
    }
}
