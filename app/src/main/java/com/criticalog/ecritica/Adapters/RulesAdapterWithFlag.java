package com.criticalog.ecritica.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPDRS.Adapter.RulesAdapter;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.PrsRule;

import java.util.List;

public class RulesAdapterWithFlag extends RecyclerView.Adapter<RulesAdapterWithFlag.ViewHolder>{

    private List<ChecklistScan> userDetailsBulks;

    public RulesAdapterWithFlag(List<ChecklistScan> userDetailsBulks) {
        this.userDetailsBulks = userDetailsBulks;
    }

    @Override
    public RulesAdapterWithFlag.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row_with_flag, parent, false);
        return new RulesAdapterWithFlag.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RulesAdapterWithFlag.ViewHolder holder, int position) {

        ChecklistScan tasklist= userDetailsBulks.get(position);

        holder.xTvTaskId.setText(String.valueOf(position+1)+") "+tasklist.getRule());
        if(tasklist.getFlag().equalsIgnoreCase("1"))
        {
            holder.mCheckYes.setChecked(true);
            holder.mCheckYes.setEnabled(true);
            holder.mCheckNo.setEnabled(true);
        }else if(tasklist.getFlag().equalsIgnoreCase("0")){
            holder.mCheckNo.setChecked(true);
            holder.mCheckNo.setEnabled(true);
            holder.mCheckYes.setEnabled(true);
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView xTvTaskId;
        CheckBox mCheckYes,mCheckNo;

        public ViewHolder(View itemView) {
            super(itemView);

            xTvTaskId = itemView.findViewById(R.id.xTvTaskId);
            mCheckYes=itemView.findViewById(R.id.mCheckYes);
            mCheckNo=itemView.findViewById(R.id.mCheckNo);
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getItemCount() {
        return userDetailsBulks.size();
    }
}
