package com.criticalog.ecritica.Adapters;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataEvent;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.EventSummary;

import java.util.List;


public class EventDetailsRVAdapetr extends RecyclerView.Adapter<EventDetailsRVAdapetr.EventDetailsRvViewHolder>  {

    List<DataEvent> eventDetails;
    private LayoutInflater layoutInflater;
    View view;




    public EventDetailsRVAdapetr( List<DataEvent> eventDetails) {
        this.eventDetails = eventDetails;
        Log.d("inside","eventdetails adapter"+eventDetails);
    }


    @Override
    public EventDetailsRvViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        Log.d("inside","eventdetailsrv"+ "oncreateviewholder");
        view = LayoutInflater.from(parent.getContext()).inflate( R.layout.event_details_rv_layout,parent,false);
        return new EventDetailsRvViewHolder(view);
    }

    @Override
    public void onBindViewHolder( EventDetailsRvViewHolder holder, int position) {

        holder.time.setText(eventDetails.get(position).getEventtime());
        holder.location.setText("("+eventDetails.get(position).getHubLocation()+")");
        holder.desc.setText(eventDetails.get(position).getEvent());
    }

    @Override
    public int getItemCount() {

        return eventDetails.size();
    }



    public class EventDetailsRvViewHolder extends RecyclerView.ViewHolder {

        TextView time ,location,desc;

        public EventDetailsRvViewHolder(View binding) {
            super(binding);
            time = view.findViewById(R.id.time);
            location = view.findViewById(R.id.location);
            desc = view.findViewById(R.id.desc);
        }
    }
}
