package com.criticalog.ecritica.Adapters;

import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataEvent;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.EventResponse;
import com.criticalog.ecritica.model.EventSummary;

import java.util.ArrayList;
import java.util.List;

public class ConnoteTrackDtAdpater extends RecyclerView.Adapter<ConnoteTrackDtAdpater.DtTrackViewHolder> {


    private LayoutInflater layoutInflater;
    Context context;
    ;

    LinearLayoutManager mLayoutManager;


    View view;
    List<DataEvent> totaleventResponseArrayList;


    public ConnoteTrackDtAdpater(List<DataEvent> totaleventResponseArrayList, Context context) {
        this.context = context;
        this.totaleventResponseArrayList = totaleventResponseArrayList;

    }


    @Override
    public DtTrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_rv_adapter, parent, false);

        mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        return new DtTrackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DtTrackViewHolder holder, int position) {

        holder.date.setText(totaleventResponseArrayList.get(position).getDate() + " " + totaleventResponseArrayList.get(position).getEventtime());
        //holder.time.setText(totaleventResponseArrayList.get(position).getEventtime());
        holder.location.setText(totaleventResponseArrayList.get(position).getHubLocation());
        holder.description.setText(totaleventResponseArrayList.get(position).getEvent());

        /*if(totaleventResponseArrayList.size()>0)
        {
            if(totaleventResponseArrayList.size()==position+1)
            {
                holder.mDownDoubleArrow.setVisibility(View.GONE);
            }
        }*/


      /*  List<DataEvent> clientList = totaleventResponseArrayList;
        holder.date_rv.setLayoutManager(new LinearLayoutManager(context));
        holder.date_rv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        holder.date_rv.setAdapter(new EventDetailsRVAdapetr(clientList));*/


    }


    @Override
    public int getItemCount() {
        return totaleventResponseArrayList.size();
    }

    public class DtTrackViewHolder extends RecyclerView.ViewHolder {
        TextView date, time, description, location;
        RecyclerView date_rv;
        ImageView mDownDoubleArrow;

        public DtTrackViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            // time=itemView.findViewById(R.id.time);
            description = itemView.findViewById(R.id.desc);
            location = itemView.findViewById(R.id.location);
            mDownDoubleArrow = itemView.findViewById(R.id.mDownDoubleArrow);
            // date_rv = itemView.findViewById(R.id.date_rv);
        }
    }

}

