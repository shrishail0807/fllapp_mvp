package com.criticalog.ecritica.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SRDatum;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.SrDetails;

import java.util.ArrayList;

public class SrMessageAdapter extends  RecyclerView.Adapter<SrMessageAdapter.SrmessageViewHolder> {

    public SrMessageAdapter(Context context, ArrayList<SRDatum> srDetailsArrayList) {
        this.context = context;
        this.srDetailsArrayList = srDetailsArrayList;
    }

    private Context context;
    private ArrayList<SRDatum> srDetailsArrayList;



    @NonNull
    @Override
    public SrmessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.srupdate_dialog, parent, false);

        return new SrmessageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SrmessageViewHolder holder, int position) {

        holder.srmessage.setText(srDetailsArrayList.get(position).getComments());
        holder.srraisedby.setText(srDetailsArrayList.get(position).getRaisedBy());
        holder.date.setText(srDetailsArrayList.get(position).getDatetime());

    }

    @Override
    public int getItemCount() {
        return srDetailsArrayList.size();
    }

    public class SrmessageViewHolder extends RecyclerView.ViewHolder {

        private TextView date,srraisedby,srmessage;
        public SrmessageViewHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            srraisedby = itemView.findViewById(R.id.srraisedby);
            srmessage = itemView.findViewById(R.id.srmessage);
        }
    }
}
