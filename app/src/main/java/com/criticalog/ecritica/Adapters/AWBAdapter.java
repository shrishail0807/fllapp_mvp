package com.criticalog.ecritica.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.ItemLongClickListener;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataAwb;
import com.criticalog.ecritica.ModelClasses.AWBDetails;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class AWBAdapter extends RecyclerView.Adapter<AWBAdapter.AWBAdapterViewHolder> {
   List<DataAwb> awbDetailsArrayList;
    LinearLayoutManager layoutInflater;
    private ClipboardManager myClipboard;
    private ClipData myClip;

    View view;
    public AWBAdapter(List<DataAwb> awbDetailsArrayList) {
        this.awbDetailsArrayList = awbDetailsArrayList;
    }





    @NonNull
    @Override
    public AWBAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        final View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.awb_adapter, viewGroup, false);
        return new AWBAdapterViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull AWBAdapterViewHolder awbAdapterViewHolder, int i) {

        awbAdapterViewHolder.awbno.setText(awbDetailsArrayList.get(i).getAirwayBill());
        awbAdapterViewHolder.vendor.setText(awbDetailsArrayList.get(i).getVendor());
        awbAdapterViewHolder.flight.setText(awbDetailsArrayList.get(i).getFlight());
        awbAdapterViewHolder.book_city.setText(awbDetailsArrayList.get(i).getBookcity());
        awbAdapterViewHolder.book_by.setText(awbDetailsArrayList.get(i).getBookby());
        awbAdapterViewHolder.book_dt.setText(awbDetailsArrayList.get(i).getBookdt() +" "+ awbDetailsArrayList.get(i).getBooktm());
        awbAdapterViewHolder.pick_dt.setText(awbDetailsArrayList.get(i).getPickdt() +" "+ awbDetailsArrayList.get(i).getPicktm());
        awbAdapterViewHolder.pick_by.setText(awbDetailsArrayList.get(i).getPickedby());
        awbAdapterViewHolder.pick_city.setText(awbDetailsArrayList.get(i).getPickcity());


        awbAdapterViewHolder.setItemLongClickListener(new ItemLongClickListener() {
            @Override
            public void onItemLongClick(View v, int pos) {

                myClipboard = (ClipboardManager) v.getContext().getSystemService(CLIPBOARD_SERVICE);
                myClip = ClipData.newPlainText("text",awbDetailsArrayList.get(pos).getAirwayBill());
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(v.getContext(),"Text Copied",Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return awbDetailsArrayList.size();
    }


    public class AWBAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        TextView awbno,vendor,flight,book_city,book_by,book_dt,pick_city,pick_by,pick_dt;
        ItemLongClickListener itemLongClickListener;

        public AWBAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            awbno = itemView.findViewById(R.id.awbno);
            vendor = itemView.findViewById(R.id.vendor);
            flight = itemView.findViewById(R.id.flight);
            book_city = itemView.findViewById(R.id.book_city);
            book_by = itemView.findViewById(R.id.book_by);
            book_dt = itemView.findViewById(R.id.book_dt_time);
            pick_by = itemView.findViewById(R.id.pick_by);
            pick_city = itemView.findViewById(R.id.pick_city);
            pick_dt = itemView.findViewById(R.id.pick_dt_time);

            itemView.setOnLongClickListener(this);

        }

        public void setItemLongClickListener(ItemLongClickListener ic)
        {
            this.itemLongClickListener=ic;
        }
        @Override
        public boolean onLongClick(View view) {
            this.itemLongClickListener.onItemLongClick(view,getLayoutPosition());
            return false;
        }
    }}
