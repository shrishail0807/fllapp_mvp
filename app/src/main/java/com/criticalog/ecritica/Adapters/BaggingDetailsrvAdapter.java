package com.criticalog.ecritica.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.Baggingdetails;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class BaggingDetailsrvAdapter extends RecyclerView.Adapter<BaggingDetailsrvAdapter.BaggingrvdetailvH> {
    ArrayList<Baggingdetails> scannedresult;
    Context context;

    public BaggingDetailsrvAdapter(ArrayList<Baggingdetails> scannedresult, Context context) {
        this.scannedresult = scannedresult;
        this.context = context;
    }

    @NonNull
    @Override
    public BaggingrvdetailvH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.bagging_rv_items,parent,false);
        return new BaggingrvdetailvH(itemview);

    }

    @Override
    public void onBindViewHolder(@NonNull BaggingrvdetailvH holder, int position) {

        holder.docketno.setText(scannedresult.get(position).getDocketno());
        holder.boxno.setText(scannedresult.get(position).getBoxno());
        holder.dest.setText(scannedresult.get(position).getDest());

    }

    @Override
    public int getItemCount() {
        return scannedresult.size();
    }

    public class BaggingrvdetailvH extends RecyclerView.ViewHolder {

        TextView docketno,boxno,dest;
        public BaggingrvdetailvH(@NonNull View itemView) {
            super(itemView);

            docketno = itemView.findViewById(R.id.docketno);
            boxno = itemView.findViewById(R.id.boxno);
            dest = itemView.findViewById(R.id.dest);


        }
    }
}
