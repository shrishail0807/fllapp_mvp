package com.criticalog.ecritica.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataPart;
import com.criticalog.ecritica.ModelClasses.PartDetails;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

public class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.PartsAdapterViewHolder> {

    List<DataPart> partDetailsArrayList;
    public PartsAdapter(List<DataPart> partDetailsArrayList) {
        this.partDetailsArrayList = partDetailsArrayList;
    }


    @Override
    public PartsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parts, parent, false);
        return new PartsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PartsAdapterViewHolder holder, int position) {

        holder.desc.setText(partDetailsArrayList.get(position).getXdddesc());
        holder.invoice.setText(partDetailsArrayList.get(position).getXinvoicenm());
        holder.l.setText(partDetailsArrayList.get(position).getXddimlength());
        holder.b.setText(partDetailsArrayList.get(position).getXddimbreadth());
        holder.h.setText(partDetailsArrayList.get(position).getXddimheight());
        holder.pcs.setText(partDetailsArrayList.get(position).getXddqty());
    }

    @Override
    public int getItemCount() {

        return partDetailsArrayList.size();

    }

    public class PartsAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView desc,pcs,l,b,h,invoice;
        public PartsAdapterViewHolder(View itemView)
        {
            super(itemView);
            desc = itemView.findViewById(R.id.awbno);
            pcs = itemView.findViewById(R.id.vendor);
            l = itemView.findViewById(R.id.flight);
            b = itemView.findViewById(R.id.book_by);
            h = itemView.findViewById(R.id.book_city);
            invoice = itemView.findViewById(R.id.book_dt_time);

        }

    }
}
