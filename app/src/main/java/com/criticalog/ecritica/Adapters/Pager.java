package com.criticalog.ecritica.Adapters;


import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.criticalog.ecritica.Activities.AirwayBillFragment;
import com.criticalog.ecritica.Activities.ConnoteTrackFragment;
import com.criticalog.ecritica.Activities.PartDetailsFragment;
import com.criticalog.ecritica.EDPimageFragment;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataAwb;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataEvent;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataPart;
import com.criticalog.ecritica.ModelClasses.ConsigneeDeatils;
import com.criticalog.ecritica.ModelClasses.ConsignerDetails;
import com.criticalog.ecritica.PODimageFragment;

import java.util.ArrayList;
import java.util.List;

public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;
    ConsignerDetails consignerDetails;
    ConsigneeDeatils consigneeDeatils;
    List<DataEvent> totaleventResponseArrayList;
    List<DataPart> partDetailsArrayList;
    List<DataAwb> awbDetailsArrayList;
    String podimage,edpimage;



    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount, ConsignerDetails consignerDetails,
                 ConsigneeDeatils consigneeDeatils,
                 List<DataEvent> totaleventResponseArrayList,
                 List<DataPart> partDetailsArrayList,
                 List<DataAwb> awbDetailsArrayList,
                 String podimage, String edpimage) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.consignerDetails = consignerDetails;
        this.consigneeDeatils = consigneeDeatils ;

        this.partDetailsArrayList = partDetailsArrayList;
        this.awbDetailsArrayList = awbDetailsArrayList;
        this.podimage = podimage;
        this.edpimage = edpimage;
        this.totaleventResponseArrayList =totaleventResponseArrayList ;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {

        //Returning the current tabs
        switch (position) {

            case 0:
                Log.e("PAGER",totaleventResponseArrayList.get(0).getDate());
                ConnoteTrackFragment tab1 = new ConnoteTrackFragment(consignerDetails,consigneeDeatils,totaleventResponseArrayList);
                return tab1;
            case 1:

               PartDetailsFragment tab2 = new PartDetailsFragment(partDetailsArrayList);
                return tab2;
            case 2:

                AirwayBillFragment tab3 = new AirwayBillFragment(awbDetailsArrayList);
                return tab3;
              /*  if(awbDetailsArrayList!=null)
                {
                    AirwayBillFragment tab3 = new AirwayBillFragment(awbDetailsArrayList);
                    return tab3;
                }else {

                    Log.e("PART_DETAILs","NUll");
                }*/

            case 3:
                PODimageFragment tab4 = new PODimageFragment(podimage);
                return tab4;
            case 4:
                EDPimageFragment tab5 = new EDPimageFragment(edpimage);
                return tab5;
            default:
                return null;

        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount()
    {
        return tabCount;
    }


}

