package com.criticalog.ecritica.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.IAdapterClickListener;
import com.criticalog.ecritica.R;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {
    private Context context;
    private ArrayList<String> homeOptionList;
    private IAdapterClickListener iAdapterClickListener;



    public HomeAdapter(Context context, ArrayList<String> homeOptionList,IAdapterClickListener iAdapterClickListener) {
        this.context = context;
        this.homeOptionList = homeOptionList;
        this.iAdapterClickListener = iAdapterClickListener;

    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_aptions, parent, false);
        return new HomeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        holder.mHomeOptionName.setText(homeOptionList.get(position));

        holder.mOptonLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                iAdapterClickListener.onClickAdapter(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeOptionList.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        private TextView mHomeOptionName;
        private LinearLayout mOptonLinearLayout;

        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            mHomeOptionName = itemView.findViewById(R.id.mHomeOptionName);
            mOptonLinearLayout = itemView.findViewById(R.id.mOptonLinearLayout);
        }
    }
}
