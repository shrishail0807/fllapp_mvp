package com.criticalog.ecritica.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.Icancelinteface;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.model.BoxOrConDetails;

import java.util.ArrayList;

public class Negative_Entry_adapter  extends RecyclerView.Adapter<Negative_Entry_adapter.Negative_Entry> {


    private   ArrayList<BoxOrConDetails> list;
    private Icancelinteface icancelinteface;
    Context context ;
    private String SelectedOption;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    public Negative_Entry_adapter(ArrayList<BoxOrConDetails> list, Context context, String SelectedOption, Icancelinteface icancelinteface) {
        this.list = list;
        this.context = context;
        this.SelectedOption = SelectedOption;
        this.icancelinteface = icancelinteface;
        mCriticalogSharedPreferences=CriticalogSharedPreferences.getInstance(context);

        Log.d("list","inadapter"+list);
        Log.d("SelectedOption","inadapter"+SelectedOption);
    }

    @NonNull
    @Override
    public Negative_Entry onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.negative_entry_adapter,parent,false);
        return new Negative_Entry(itemview);

    }

    @Override
    public void onBindViewHolder(@NonNull Negative_Entry holder, int position) {

        if (mCriticalogSharedPreferences.getData("neg_status_of").equalsIgnoreCase("Box Number") ||mCriticalogSharedPreferences.getData("neg_status_of").equalsIgnoreCase("Con Number")) {
        holder.tv5.setText("Customer Name: ");
        holder.tv6.setText("Consigne Address: ");
        holder.tv7.setText("Customer Code: ");
        holder.tv8.setText("Docket No: ");
        }
        else if (mCriticalogSharedPreferences.getData("neg_status_of").equalsIgnoreCase("MBag Number") ) {
            holder.tv5.setText("Destination: ");
            holder.tv6.setText("Connote: ");
            holder.tv7.setText("Origin: ");
            holder.tv8.setText("Mbag Number: ");
        }else if(mCriticalogSharedPreferences.getData("neg_status_of").equalsIgnoreCase("MAWB Number"))
        {
            holder.tv5.setText("Destination: ");
            holder.tv6.setText("Connote: ");
            holder.tv7.setText("Origin: ");
            holder.tv8.setText("MAWB Number: ");
        }
        holder.tv1.setText(list.get(position).getNumber());
        holder.tv2.setText(list.get(position).getCust_code());
        holder.tv3.setText(list.get(position).getCust_name());
        holder.tv4.setText(list.get(position).getConsignee_add());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("clicked position","="+holder.getAdapterPosition());
                delete(holder.getAdapterPosition());
            }
        });




    }


    public void delete(int position) { //removes the row
        icancelinteface.removerow(list.get(position).getNumber());
        list.remove(position);
        notifyItemRemoved(position);


    }
    @Override
    public int getItemCount() {

        if (list == null) {
            return 0;

        }else {
            return list.size();
        }

    }

    public class Negative_Entry extends RecyclerView.ViewHolder {

        TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8;
        ImageView imageView;
        public Negative_Entry(@NonNull View itemView) {
            super(itemView);

            tv1 = itemView.findViewById(R.id.textView9);
            tv2 = itemView.findViewById(R.id.textView10);
            tv3 = itemView.findViewById(R.id.textView11);
            tv4 = itemView.findViewById(R.id.textView12);
            imageView = itemView.findViewById(R.id.imageView);
            tv5 = itemView.findViewById(R.id.textView15);
            tv6 = itemView.findViewById(R.id.textView16);
            tv7 = itemView.findViewById(R.id.textView14);
            tv8=itemView.findViewById(R.id.textView13);

        }
    }
}
