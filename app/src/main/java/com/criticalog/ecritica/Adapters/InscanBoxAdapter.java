package com.criticalog.ecritica.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.R;

import java.util.ArrayList;

public class InscanBoxAdapter extends RecyclerView.Adapter<InscanBoxAdapter.InscanVh>  {
    ArrayList scannedresult;
    Context context;

    public InscanBoxAdapter(ArrayList scannedresult, Context context) {
        this.scannedresult = scannedresult;
        this.context = context;
    }

    @NonNull
    @Override
    public InscanVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.inscanboxitems,parent,false);
        return new InscanVh(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull InscanVh holder, int position) {

        holder.textView.setText(scannedresult.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return scannedresult.size();
    }

    public class InscanVh extends RecyclerView.ViewHolder {
        TextView textView;
        public InscanVh(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textView3);
        }
    }
}

