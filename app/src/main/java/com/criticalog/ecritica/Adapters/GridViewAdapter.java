package com.criticalog.ecritica.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.OperationMenuDetails;


import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {
    LayoutInflater layoutInflater;
    private Context mContext;
    ArrayList<OperationMenuDetails> btndetails  = new ArrayList<>();

    public GridViewAdapter(ArrayList<OperationMenuDetails> doctorDetailsArrayList, Context mContext) {
        this.mContext = mContext;
        this.btndetails =doctorDetailsArrayList;
    }

    @Override
    public int getCount() {
        return btndetails.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        Holder holder = new Holder();
        View rowView;

        rowView = layoutInflater.inflate(R.layout.operationgridview, null);
        holder.name =( TextView) rowView.findViewById(R.id.text);
        holder.name.setText(btndetails.get(position).getName());


        return rowView;
    }

    public class Holder
    {
        TextView name;

    }

}
