package com.criticalog.ecritica.Adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.ModelClasses.history_data;
import com.criticalog.ecritica.R;

import java.util.ArrayList;

/**
 * Created by DELL1 on 7/30/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    private ArrayList<history_data> historyDataArrayList = new ArrayList<>();

    public HistoryAdapter(ArrayList<history_data> history_data) {
        this.historyDataArrayList = history_data;
    }


    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_history, parent, false);
        return new HistoryViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {

        final history_data data = historyDataArrayList.get(position);
        holder.mtaskid.setText(data.getTask_id());
        holder.mtask_status.setText(data.getTask_status());
    }

    @Override
    public int getItemCount() {
        return historyDataArrayList.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        private TextView mtaskid, mtask_status;

        public HistoryViewHolder(View itemView) {
            super(itemView);
            mtaskid = (TextView) itemView.findViewById(R.id.xtv_taskid);
            mtask_status = (TextView) itemView.findViewById(R.id.xtv_task_status);

        }
    }
}
