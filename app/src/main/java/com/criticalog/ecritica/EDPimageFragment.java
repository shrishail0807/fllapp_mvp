package com.criticalog.ecritica;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.Activities.ZoomImageActivity;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class EDPimageFragment extends Fragment {


    String edpimage;
    View view;


    public EDPimageFragment(String edpimage) {
        this.edpimage = edpimage ;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_edpimage, container, false);
        ImageView edpimageview = view.findViewById(R.id.edpimage);
        TextView text = view.findViewById(R.id.text);

        String url = edpimage;

        Log.d("image url",url);

        if (edpimage != null) {

            Glide.with(getActivity()).load(url).centerCrop().placeholder(R.drawable.brokenimage).into(edpimageview);
            edpimageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ZoomImageActivity.class);
                    intent.putExtra("imageurl",url);
                    startActivity(intent);
                }
            });

        }

        else {
            text.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load("").centerCrop().placeholder(R.drawable.brokenimage).into(edpimageview);
        }

        return view;
    }

}
