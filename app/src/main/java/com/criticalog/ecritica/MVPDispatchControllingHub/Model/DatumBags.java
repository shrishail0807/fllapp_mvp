package com.criticalog.ecritica.MVPDispatchControllingHub.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DatumBags implements Serializable {
    @SerializedName("bags")
    @Expose
    private String bags;

    public String getBags() {
        return bags;
    }
    public void setBags(String bags) {
        this.bags = bags;
    }
}
