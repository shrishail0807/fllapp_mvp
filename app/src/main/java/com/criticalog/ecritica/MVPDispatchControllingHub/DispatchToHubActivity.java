package com.criticalog.ecritica.MVPDispatchControllingHub;
import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Adapters.InscanBoxAdapter;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPDispatchControllingHub.Adapter.BagsAdapter;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.DatumBags;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.DispatchHubConfirmContract;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.GetMyBagResponse;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.GetMyBagsRequest;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.RGSAConfirmHubRequest;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.RGSAPickupHubConfirmImpl;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.RGSAPickupHubInteractorImpl;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.MVPPickupConfirm.Model.DatumVendor;
import com.criticalog.ecritica.MVPPickupConfirm.Model.PickupRGSAResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListRequest;
import com.criticalog.ecritica.MVPPickupConfirm.Model.VendorListResponse;
import com.criticalog.ecritica.MVPPickupConfirm.PickupConfirmContract;
import com.criticalog.ecritica.MVPPickupConfirm.PickupConfirmPresenterImpl;
import com.criticalog.ecritica.MVPPickupConfirm.PickupContractContractInteractorImpl;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class DispatchToHubActivity extends AppCompatActivity implements View.OnClickListener, PickupConfirmContract.MainView, DispatchHubConfirmContract.MainView, RulesClicked {
    private RecyclerView mRVPickupListConfirm;
    private BagsAdapter mPickupConfirmAdapter;
    private CheckBox mCheckYes;
    private TextView mTopBarText;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId;
    EditText mSelectVendor;
    PickupConfirmContract.presenter mPresenter;
    ArrayList<DatumVendor> datumVendorList = new ArrayList<>();
    ArrayList<SampleSearchModel> vendorSearchList = new ArrayList();
    ArrayList<DatumBags> datumRgsaPickupArrayList = new ArrayList<>();
    List<DatumBags> bagsArrayList = new ArrayList<>();
    private String vehicleNumber = "", cdNumber = "";
    ArrayList<String> stringArrayListVendror = new ArrayList<>();
    private LinearLayout mDocketLabelLay;
    SimpleArcDialog mProgressDialog1;
    private Button submit;
    private List<Integer> docketList = new ArrayList<>();
    private List<Integer> docketListSelect = new ArrayList<>();
    private List<Integer> bagList = new ArrayList<>();
    private CheckBox mCheckYesAll;
    private EditText mVehicleNumber, mCDNumber;
    private ImageView mTvBackbutton;
    private Dialog dialogOtherEntry;
    private RecyclerView mRvBox;
    private InscanBoxAdapter mDocketsAdapter;
    private ArrayList scannedboxlist = new ArrayList();
    private Dialog dialogManualEntry;
    private RelativeLayout mRLManualBag, mQrScan;
    private String bagNumber = "";
    // private LinearLayout mManualScanLayBag;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude = 0, longitude = 0;
    private DispatchHubConfirmContract.presenter mPresenterHubConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mvp_dispatch_hub);

        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);

        Toasty.warning(DispatchToHubActivity.this, "Kindly Please Make Sure the Clients are Closed.", Toast.LENGTH_LONG, true).show();

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");

        userId = mCriticalogSharedPreferences.getData("userId");
        mRVPickupListConfirm = findViewById(R.id.mRVPickupListConfirm);
        mTopBarText = findViewById(R.id.mTopBarText);
        mSelectVendor = findViewById(R.id.mSelectVendor);
        mDocketLabelLay = findViewById(R.id.mDocketLabelLay);
        submit = findViewById(R.id.submit);
        mCheckYesAll = findViewById(R.id.mCheckYesAll);
        mVehicleNumber = findViewById(R.id.mVehicleNumber);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);
        mRvBox = findViewById(R.id.mRvBox);
        mCDNumber = findViewById(R.id.mCDNumber);
        //  mManualScanLayBag = findViewById(R.id.mManualScanLayBag);

        submit.setOnClickListener(this);
        mCheckYesAll.setOnClickListener(this);

        mSelectVendor.setOnClickListener(this);
        mTvBackbutton.setOnClickListener(this);

        mSelectVendor.setFocusable(false);

        mTopBarText.setText("Dispatch to Controlling Hub");

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getBagsList();

        getLocation();


        mCheckYesAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    if (bagsArrayList.size() != 0) {
                        mPickupConfirmAdapter = new BagsAdapter(DispatchToHubActivity.this, 1, bagsArrayList, DispatchToHubActivity.this);
                        mRVPickupListConfirm.setLayoutManager(new LinearLayoutManager(DispatchToHubActivity.this));
                        mRVPickupListConfirm.setAdapter(mPickupConfirmAdapter);
                        mPickupConfirmAdapter.notifyDataSetChanged();
                    }


                } else {
                    if (bagsArrayList.size() != 0) {
                        mPickupConfirmAdapter = new BagsAdapter(DispatchToHubActivity.this, 0, bagsArrayList, DispatchToHubActivity.this);
                        mRVPickupListConfirm.setLayoutManager(new LinearLayoutManager(DispatchToHubActivity.this));
                        mRVPickupListConfirm.setAdapter(mPickupConfirmAdapter);
                        mPickupConfirmAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(DispatchToHubActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DispatchToHubActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DispatchToHubActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(DispatchToHubActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("LAT LONG_BAG", String.valueOf(latitude) + "" + String.valueOf(longitude));

                } else {
                    // mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void getBagsList() {
        GetMyBagsRequest getMyBagsRequest = new GetMyBagsRequest();
        getMyBagsRequest.setAction("get_my_bags");
        getMyBagsRequest.setUserId(userId);

        mPresenterHubConfirm = new RGSAPickupHubConfirmImpl(this, new RGSAPickupHubInteractorImpl(), new RGSAPickupHubInteractorImpl());
        mPresenterHubConfirm.getMyBagsRequest(getMyBagsRequest);
    }

    private boolean isVehicleNumber1(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING = "^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{1}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }

    private boolean isVehicleNumber2(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING = "^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{2}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }

    private boolean isVehicleNumber3(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING = "^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{3}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }

    private boolean isVehicleNumber4(String vehicleNumber) {

        // String EMAIL_STRING = "^[A-Za-b]{2}[-][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}[0-9]{3}[0-9]{2}[0-9]{1}$";


        String EMAIL_STRING = "^[A-Za-z]{2}[0-9]{1,2}[A-Za-z]{1,2}[0-9]{4}$";
        return Pattern.compile(EMAIL_STRING).matcher(vehicleNumber).matches();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mSelectVendor:
                vendorSearchList.clear();
                datumVendorList.clear();
                VendorListRequest vendorListRequest = new VendorListRequest();
                vendorListRequest.setAction("get_vendor_list");
                vendorListRequest.setUserId(userId);
                vendorListRequest.setLatitude(String.valueOf(latitude));
                vendorListRequest.setLongitude(String.valueOf(longitude));

                mPresenter = new PickupConfirmPresenterImpl(DispatchToHubActivity.this, new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl(), new PickupContractContractInteractorImpl());
                mPresenter.getVendorList(vendorListRequest);
                break;
            case R.id.submit:
                getLocation();
                vehicleNumber = mVehicleNumber.getText().toString();
                cdNumber = mCDNumber.getText().toString();
                if (vehicleNumber.equalsIgnoreCase("")) {
                    Toasty.warning(this, "Enter Vehicle Number", Toast.LENGTH_LONG, true).show();
                } else if ((isVehicleNumber1(vehicleNumber) == false) && (isVehicleNumber2(vehicleNumber) == false) && (isVehicleNumber3(vehicleNumber) == false) && (isVehicleNumber4(vehicleNumber) == false)) {
                    Toasty.warning(this, "Enter Valid Vehicle Number", Toast.LENGTH_SHORT, true).show();
                } else if (cdNumber.equalsIgnoreCase("")) {
                    Toasty.warning(this, "Enter CD Number", Toast.LENGTH_SHORT, true).show();
                } else if (docketListSelect.size() == 0) {
                    Toasty.warning(this, "You have not selected any dockets!!", Toast.LENGTH_LONG, true).show();
                } else {

                    RGSAConfirmHubRequest rgsaConfirmHubRequest = new RGSAConfirmHubRequest();
                    rgsaConfirmHubRequest.setAction("rgsa_confirm_pickup");
                    rgsaConfirmHubRequest.setUserId(userId);
                    rgsaConfirmHubRequest.setVehicleNo(vehicleNumber);
                    rgsaConfirmHubRequest.setVendorName(mCriticalogSharedPreferences.getData("vendor_name"));
                    rgsaConfirmHubRequest.setVendorId(mCriticalogSharedPreferences.getData("vendor_id"));
                    rgsaConfirmHubRequest.setBags(docketListSelect);
                    rgsaConfirmHubRequest.setLat(String.valueOf(latitude));
                    rgsaConfirmHubRequest.setLng(String.valueOf(longitude));

                    mPresenterHubConfirm = new RGSAPickupHubConfirmImpl(this, new RGSAPickupHubInteractorImpl(), new RGSAPickupHubInteractorImpl());
                    mPresenterHubConfirm.rgsaPickupHubConfirmRequest(rgsaConfirmHubRequest);
                }
                break;

            case R.id.mTvBackbutton:
                finish();
                break;

            case R.id.mRLManualBag:
                // manualDialogBag();
                break;

            case R.id.mQrScan:
                mCriticalogSharedPreferences.saveData("scan_what", "bag");
                //startBarcodeScan();
                break;

        }
    }

    public void otherDialog() {
        ImageView mCancelDialog;
        EditText mVendorName;
        TextView mSubmit;
        //Dialog Other Vendor Entry
        dialogOtherEntry = new Dialog(DispatchToHubActivity.this);
        dialogOtherEntry.setContentView(R.layout.dialog_other_vendor);
        dialogOtherEntry.setCanceledOnTouchOutside(false);
        dialogOtherEntry.setCancelable(false);
        mCancelDialog = dialogOtherEntry.findViewById(R.id.mCancelDialog);
        mVendorName = dialogOtherEntry.findViewById(R.id.mVendorName);
        mSubmit = dialogOtherEntry.findViewById(R.id.mSubmit);

        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOtherEntry.dismiss();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vendorName = mVendorName.getText().toString();

                if (vendorName.equalsIgnoreCase("")) {
                    Toasty.warning(DispatchToHubActivity.this, "Enter Vendor Name", Toast.LENGTH_LONG, true).show();
                } else {
                    mCriticalogSharedPreferences.saveData("vendor_name", vendorName);
                    dialogOtherEntry.dismiss();
                    // callRGSAPickupListAPI();
                }

            }
        });
        dialogOtherEntry.show();
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(DispatchToHubActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        {
                            if (fromWhere.equalsIgnoreCase("vendor")) {
                                mSelectVendor.setText(item.getTitle());

                                mCriticalogSharedPreferences.saveData("vendor_id", datumVendorList.get(stringArrayListVendror.indexOf(item.getTitle())).getVendorId());

                                mCriticalogSharedPreferences.saveData("vendor_name", item.getTitle());

                                mDocketLabelLay.setVisibility(View.VISIBLE);
                                mRVPickupListConfirm.setVisibility(View.VISIBLE);
                                submit.setVisibility(View.VISIBLE);
                                if (item.getTitle().equalsIgnoreCase("Other")) {
                                    otherDialog();
                                } else {

                                }
                            }

                            dialog.dismiss();
                        }

                    }
                }).show();
    }

    @Override
    public void showProgress() {
        mProgressDialog1.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog1.dismiss();
    }

    @Override
    public void setRGSAPickupHubConfirmResponse(RGSAPickupConfirmResponse rgsaPickupConfirmResponse) {

        if (rgsaPickupConfirmResponse.getStatus() == 200) {
            Toasty.success(DispatchToHubActivity.this, rgsaPickupConfirmResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            finish();
        } else {
            Toasty.warning(DispatchToHubActivity.this, rgsaPickupConfirmResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }

    }

    @Override
    public void setBagsListResponse(GetMyBagResponse bagsListResponse) {

        if (bagsListResponse.getStatus() == 200) {
            bagsArrayList = bagsListResponse.getData();


            if (bagsArrayList.size() != 0) {
                mPickupConfirmAdapter = new BagsAdapter(DispatchToHubActivity.this, 0, bagsArrayList, DispatchToHubActivity.this);
                mRVPickupListConfirm.setLayoutManager(new LinearLayoutManager(DispatchToHubActivity.this));
                mRVPickupListConfirm.setAdapter(mPickupConfirmAdapter);
                mPickupConfirmAdapter.notifyDataSetChanged();

                for (int i = 0; i < bagsArrayList.size(); i++) {
                    docketList.add(Integer.parseInt(bagsArrayList.get(i).getBags()));
                }
            }
        } else {
            Toast.makeText(DispatchToHubActivity.this, bagsListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setVendorDataToViews(VendorListResponse vendorListResponse) {
        if (vendorListResponse.getStatus() == 200) {
            datumVendorList = (ArrayList<DatumVendor>) vendorListResponse.getData();
            for (int i = 0; i < datumVendorList.size(); i++) {
                vendorSearchList.add(new SampleSearchModel(datumVendorList.get(i).getVendorName()));
                stringArrayListVendror.add(datumVendorList.get(i).getVendorName());
            }
            showOriginDestination(vendorSearchList, "Vendor");
            Toast.makeText(this, vendorListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, vendorListResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setPickupRgsaListToViews(PickupRGSAResponse pickupRGSAResponse) {

    }

    @Override
    public void setRGSAPickupConfirmResponse(RGSAPickupConfirmResponse rgsaPickupConfirmResponse) {

    }

    @Override
    public void setRgsaBaggingValidateResponse(BagCheckResponse bagCheckResponse) {

    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    @Override
    public void onclickRulesList(String yesNo, int position) {
        if (yesNo.equalsIgnoreCase("yes")) {
            docketListSelect = docketList;
        } else if (yesNo.equalsIgnoreCase("no")) {

            docketListSelect = new ArrayList<>();
        } else if (yesNo.equalsIgnoreCase("remove")) {
            if (docketListSelect.size() > 0) {
                if (position < docketList.size()) {
                    if (docketListSelect.contains(docketList.get(position))) {
                        int index = docketListSelect.indexOf(docketList.get(position));
                        docketListSelect.remove(index);
                    }
                }
            }
        } else {
            docketListSelect.add(Integer.parseInt(String.valueOf(docketList.get(position))));
        }
    }
}
