package com.criticalog.ecritica.MVPDispatchControllingHub.Model;

import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RGSAPickupHubInteractorImpl implements DispatchHubConfirmContract.GetRGSAHubConfirmIntractor, DispatchHubConfirmContract.GetMyBagsInteractor {

    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);


    @Override
    public void rgsaPickupHubRequestToServer(DispatchHubConfirmContract.GetRGSAHubConfirmIntractor.OnFinishedListener onFinishedListener, RGSAConfirmHubRequest rgsaConfirmHubRequest) {
        Call<RGSAPickupConfirmResponse> rgsaPickupConfirmResponseCall = services.rgsaPickupHubConfirm(rgsaConfirmHubRequest);

        rgsaPickupConfirmResponseCall.enqueue(new Callback<RGSAPickupConfirmResponse>() {
            @Override
            public void onResponse(Call<RGSAPickupConfirmResponse> call, Response<RGSAPickupConfirmResponse> response) {

                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<RGSAPickupConfirmResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getMyBagsRequestToServer(DispatchHubConfirmContract.GetMyBagsInteractor.OnFinishedListener onFinishedListener, GetMyBagsRequest getMyBagsRequest) {

        Call<GetMyBagResponse> getMyBagResponseCall = services.getMyBagsList(getMyBagsRequest);
        getMyBagResponseCall.enqueue(new Callback<GetMyBagResponse>() {
            @Override
            public void onResponse(Call<GetMyBagResponse> call, Response<GetMyBagResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<GetMyBagResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }
}
