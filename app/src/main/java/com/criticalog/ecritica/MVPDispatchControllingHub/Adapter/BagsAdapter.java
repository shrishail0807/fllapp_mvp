package com.criticalog.ecritica.MVPDispatchControllingHub.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPDispatchControllingHub.Model.DatumBags;
import com.criticalog.ecritica.R;

import java.util.List;

public class BagsAdapter extends RecyclerView.Adapter<BagsAdapter.ViewHolder> {

    private Context context;
    int checkAll;
    List<DatumBags> datumRgsaPickupList;
    RulesClicked rulesClicked;

    public BagsAdapter(Context context, int checkAll,List<DatumBags> datumRgsaPickupList,RulesClicked rulesClicked) {
        this.context = context;
        this.checkAll = checkAll;
        this.datumRgsaPickupList=datumRgsaPickupList;
        this.rulesClicked=rulesClicked;
    }

    @NonNull
    @Override
    public BagsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bags_hub_view, parent, false);
        return new BagsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BagsAdapter.ViewHolder holder, int position) {

        holder.mDocketNo.setText(datumRgsaPickupList.get(position).getBags());
       /* holder.mOrigin.setText(datumRgsaPickupList.get(position).getOriginpincode());
        holder.mDestination.setText(datumRgsaPickupList.get(position).getDestpincode());*/
        if (checkAll == 1) {
            holder.mCheckYes.setChecked(true);
            rulesClicked.onclickRulesList("yes",position);
        } else if(checkAll ==0){
            holder.mCheckYes.setChecked(false);
            rulesClicked.onclickRulesList("no",position);
        }



        holder.mCheckYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {

                    holder.mCheckYes.setChecked(true);
                    rulesClicked.onclickRulesList("",position);

                } else {
                    holder.mCheckYes.setChecked(false);
                    rulesClicked.onclickRulesList("remove",position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datumRgsaPickupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mDocketNo,mOrigin,mDestination;
        CheckBox mCheckYes;

        public ViewHolder(View itemView) {
            super(itemView);
            mDocketNo = itemView.findViewById(R.id.mDocketNo);
            mCheckYes = itemView.findViewById(R.id.mCheckYes);
            mOrigin=itemView.findViewById(R.id.mOrigin);
            mDestination=itemView.findViewById(R.id.mDestination);
        }
    }
}
