package com.criticalog.ecritica.MVPDispatchControllingHub.Model;

import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;

public interface DispatchHubConfirmContract {
    interface presenter{

        void onDestroy();

        void rgsaPickupHubConfirmRequest(RGSAConfirmHubRequest rgsaConfirmHubRequest);

        void getMyBagsRequest(GetMyBagsRequest getMyBagsRequest);
    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setRGSAPickupHubConfirmResponse(RGSAPickupConfirmResponse rgsaPickupConfirmResponse);

        void setBagsListResponse(GetMyBagResponse bagsListResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/

    interface GetRGSAHubConfirmIntractor {

        interface OnFinishedListener {
            void onFinished(RGSAPickupConfirmResponse rgsaPickupConfirmResponse);
            void onFailure(Throwable t);
        }
        void rgsaPickupHubRequestToServer(DispatchHubConfirmContract.GetRGSAHubConfirmIntractor.OnFinishedListener onFinishedListener, RGSAConfirmHubRequest rgsaConfirmHubRequest);
    }

    interface GetMyBagsInteractor{
        interface OnFinishedListener {
            void onFinished(GetMyBagResponse getMyBagResponse);
            void onFailure(Throwable t);
        }
        void getMyBagsRequestToServer(DispatchHubConfirmContract.GetMyBagsInteractor.OnFinishedListener onFinishedListener,GetMyBagsRequest getMyBagsRequest);

    }

}
