package com.criticalog.ecritica.MVPDispatchControllingHub.Model;

import com.criticalog.ecritica.MVPPickupConfirm.Model.RGSAPickupConfirmResponse;

public class RGSAPickupHubConfirmImpl implements DispatchHubConfirmContract.presenter, DispatchHubConfirmContract.GetRGSAHubConfirmIntractor.OnFinishedListener, DispatchHubConfirmContract.GetMyBagsInteractor.OnFinishedListener {

    private DispatchHubConfirmContract.MainView mainView;
    private DispatchHubConfirmContract.GetRGSAHubConfirmIntractor getRGSAHubConfirmIntractor;
    private DispatchHubConfirmContract.GetMyBagsInteractor getMyBagsInteractor;

    public RGSAPickupHubConfirmImpl(DispatchHubConfirmContract.MainView mainView,DispatchHubConfirmContract.GetRGSAHubConfirmIntractor getRGSAHubConfirmIntractor,
                                    DispatchHubConfirmContract.GetMyBagsInteractor getMyBagsInteractor)
    {
        this.mainView=mainView;
        this.getRGSAHubConfirmIntractor=getRGSAHubConfirmIntractor;
        this.getMyBagsInteractor=getMyBagsInteractor;
    }
    @Override
    public void onDestroy() {

    }

    @Override
    public void rgsaPickupHubConfirmRequest(RGSAConfirmHubRequest rgsaConfirmHubRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }

        getRGSAHubConfirmIntractor.rgsaPickupHubRequestToServer(this,rgsaConfirmHubRequest);
    }

    @Override
    public void getMyBagsRequest(GetMyBagsRequest getMyBagsRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        getMyBagsInteractor.getMyBagsRequestToServer(this,getMyBagsRequest);
    }

    @Override
    public void onFinished(RGSAPickupConfirmResponse rgsaPickupConfirmResponse) {

        if(mainView!=null)
        {
            mainView.setRGSAPickupHubConfirmResponse(rgsaPickupConfirmResponse );
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(GetMyBagResponse getMyBagResponse) {
        if(mainView!=null)
        {
            mainView.setBagsListResponse(getMyBagResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {

        if(mainView!=null)
        {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
