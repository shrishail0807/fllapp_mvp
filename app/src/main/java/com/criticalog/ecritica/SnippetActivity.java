package com.criticalog.ecritica;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.Interface.SnippetWSCalled;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.VolleyClasses.SnippetVolley;

import org.json.JSONException;
import org.json.JSONObject;

public class SnippetActivity extends AppCompatActivity implements SnippetWSCalled {

    Button moreclarification,understood;
    ImageView imageView;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    int snippet_id;
    private SnippetVolley snippetVolley;
    private TextView textview1,textview2,textview3,textview4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snippet);

        findViews();
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(getApplicationContext());

        Intent intent = getIntent();

        String response = intent.getStringExtra("response");
        try {
            JSONObject jsonObject = new JSONObject(response);
            String text1 =jsonObject.getString("text1");
            String text2 =jsonObject.getString("text2");
            String text3 =jsonObject.getString("text3");
            String text4 =jsonObject.getString("text4");
            String url = jsonObject.getString("img");
            snippet_id = jsonObject.getInt("snippet_id");
            Log.e("snippetid","localdb"+snippet_id);


            textview1.setText(text1);
            textview2.setText(text2);
            textview3.setText(text3);
            textview4.setText(text4);


            Glide.with(getApplicationContext()).load(url).into(imageView);



        } catch (JSONException e) {
            e.printStackTrace();
        }




    }


    private void findViews(){
        moreclarification = findViewById(R.id.moresuggestion);
        understood = findViewById(R.id.understood);
        imageView = findViewById(R.id.image);
        textview1 = findViewById(R.id.text1);
        textview2 = findViewById(R.id.text2);
        textview3 = findViewById(R.id.text3);
        textview4 = findViewById(R.id.text4);
    }

    public void closepopup(View view) {

        finish();
        int id = view.getId();
        if (id == R.id.understood) {
            criticalogSharedPreferences.saveData("opt_typeId","1");
        }else {
            criticalogSharedPreferences.saveData("opt_typeId","2");
        }

        criticalogSharedPreferences.saveIntData("snippet_id",snippet_id);
        new SnippetVolley(SnippetActivity.this,SnippetActivity.this);

    }


    @Override
    public void snippetWsSuccessFailure(String result,int type) {
        if (type ==1) {
            Log.e("return",""+type);
            Intent intent = new Intent(SnippetActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }



    }
}
