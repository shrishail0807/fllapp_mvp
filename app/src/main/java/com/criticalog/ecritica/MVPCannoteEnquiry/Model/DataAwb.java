package com.criticalog.ecritica.MVPCannoteEnquiry.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataAwb {
    @SerializedName("AirwayBill")
    @Expose
    private String airwayBill;
    @SerializedName("Vendor")
    @Expose
    private String vendor;
    @SerializedName("flight")
    @Expose
    private String flight;
    @SerializedName("bookcity")
    @Expose
    private String bookcity;
    @SerializedName("bookby")
    @Expose
    private String bookby;
    @SerializedName("bookdt")
    @Expose
    private String bookdt;
    @SerializedName("booktm")
    @Expose
    private String booktm;
    @SerializedName("pickcity")
    @Expose
    private String pickcity;
    @SerializedName("pickedby")
    @Expose
    private String pickedby;
    @SerializedName("pickdt")
    @Expose
    private String pickdt;
    @SerializedName("picktm")
    @Expose
    private String picktm;

    public String getAirwayBill() {
        return airwayBill;
    }

    public void setAirwayBill(String airwayBill) {
        this.airwayBill = airwayBill;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getBookcity() {
        return bookcity;
    }

    public void setBookcity(String bookcity) {
        this.bookcity = bookcity;
    }

    public String getBookby() {
        return bookby;
    }

    public void setBookby(String bookby) {
        this.bookby = bookby;
    }

    public String getBookdt() {
        return bookdt;
    }

    public void setBookdt(String bookdt) {
        this.bookdt = bookdt;
    }

    public String getBooktm() {
        return booktm;
    }

    public void setBooktm(String booktm) {
        this.booktm = booktm;
    }

    public String getPickcity() {
        return pickcity;
    }

    public void setPickcity(String pickcity) {
        this.pickcity = pickcity;
    }

    public String getPickedby() {
        return pickedby;
    }

    public void setPickedby(String pickedby) {
        this.pickedby = pickedby;
    }

    public String getPickdt() {
        return pickdt;
    }

    public void setPickdt(String pickdt) {
        this.pickdt = pickdt;
    }

    public String getPicktm() {
        return picktm;
    }

    public void setPicktm(String picktm) {
        this.picktm = picktm;
    }
}
