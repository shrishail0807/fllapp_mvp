package com.criticalog.ecritica.MVPCannoteEnquiry;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateResponse;

public class ConnoteEnquiryPresenterImpl implements ConnoteEnquiryContract.presenter, ConnoteEnquiryContract.ConnoteEnquiryIntractor.OnFinishedListener, ConnoteEnquiryContract.SrServiceIntractor.OnFinishedListener, ConnoteEnquiryContract.SrUpdateIntractor.OnFinishedListener {
    private ConnoteEnquiryContract.MainView mainView;
    private ConnoteEnquiryContract.ConnoteEnquiryIntractor connoteEnquiryIntractor;
    private ConnoteEnquiryContract.SrServiceIntractor srServiceIntractor;
    private ConnoteEnquiryContract.SrUpdateIntractor srUpdateIntractor;

    public ConnoteEnquiryPresenterImpl(ConnoteEnquiryContract.MainView mainView,
                                       ConnoteEnquiryContract.ConnoteEnquiryIntractor connoteEnquiryIntractor,
                                       ConnoteEnquiryContract.SrServiceIntractor srServiceIntractor,
                                       ConnoteEnquiryContract.SrUpdateIntractor srUpdateIntractor) {
        this.mainView = mainView;
        this.connoteEnquiryIntractor = connoteEnquiryIntractor;
        this.srServiceIntractor=srServiceIntractor;
        this.srUpdateIntractor=srUpdateIntractor;
    }

    @Override
    public void onDestroy() {

        if(mainView!=null)
        {
            mainView=null;
        }
    }

    @Override
    public void connoteEnquiry(String token, ConnoteEnquiryRequest connoteEnquiryRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }
        connoteEnquiryIntractor.connoteEnquiryRequest(this,token,connoteEnquiryRequest);
    }

    @Override
    public void SrServiceRequest(String token, SrServiceRequest srServiceRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        srServiceIntractor.srServiceRequest(this,token,srServiceRequest);
    }

    @Override
    public void SrUpdateRequest(String token, SrUpdateRequest srUpdateRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        srUpdateIntractor.srUpdateRequest(this,token,srUpdateRequest);
    }

    @Override
    public void onFinished(ConnoteEnquiryResponse connoteEnquiryResponse) {

        if(mainView!=null)
        {
            mainView.connoteEnquirySetDataToViews(connoteEnquiryResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(SrServiceResponse srServiceResponse) {
        if(mainView!=null)
        {
            mainView.srServiceResponseDataToViews(srServiceResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(SrUpdateResponse srUpdateResponse) {
        if(mainView!=null)
        {
            mainView.SrUpdateResponseDataToViews(srUpdateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {

        if(mainView!=null)
        {
            mainView.onResponseFailure(t);
        }
    }
}
