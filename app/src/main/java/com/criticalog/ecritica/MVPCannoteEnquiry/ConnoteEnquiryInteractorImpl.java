package com.criticalog.ecritica.MVPCannoteEnquiry;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnoteEnquiryInteractorImpl implements ConnoteEnquiryContract.ConnoteEnquiryIntractor,ConnoteEnquiryContract.SrServiceIntractor,ConnoteEnquiryContract.SrUpdateIntractor {
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void connoteEnquiryRequest(ConnoteEnquiryContract.ConnoteEnquiryIntractor.OnFinishedListener onFinishedListener, String token, ConnoteEnquiryRequest connoteEnquiryRequest) {
        Call<ConnoteEnquiryResponse> connoteEnquiryResponseCall = services.connoteEnquiry(connoteEnquiryRequest);
        connoteEnquiryResponseCall.enqueue(new Callback<ConnoteEnquiryResponse>() {
            @Override
            public void onResponse(Call<ConnoteEnquiryResponse> call, Response<ConnoteEnquiryResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ConnoteEnquiryResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void srServiceRequest(ConnoteEnquiryContract.SrServiceIntractor.OnFinishedListener onFinishedListener, String token, SrServiceRequest srServiceRequest) {
        Call<SrServiceResponse> srServiceResponseCall=services.srSeriveRequest(srServiceRequest);
        srServiceResponseCall.enqueue(new Callback<SrServiceResponse>() {
            @Override
            public void onResponse(Call<SrServiceResponse> call, Response<SrServiceResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<SrServiceResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void srUpdateRequest(ConnoteEnquiryContract.SrUpdateIntractor.OnFinishedListener onFinishedListener, String token, SrUpdateRequest srUpdateRequest) {

        Call<SrUpdateResponse> srUpdateResponseCall=services.srUpdateRequest(srUpdateRequest);
        srUpdateResponseCall.enqueue(new Callback<SrUpdateResponse>() {
            @Override
            public void onResponse(Call<SrUpdateResponse> call, Response<SrUpdateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<SrUpdateResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }
}
