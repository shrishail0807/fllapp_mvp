package com.criticalog.ecritica.MVPCannoteEnquiry.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPart {
    @SerializedName("XDDQTY")
    @Expose
    private String xddqty;
    @SerializedName("XDDDESC")
    @Expose
    private String xdddesc;
    @SerializedName("XDDIMLENGTH")
    @Expose
    private String xddimlength;
    @SerializedName("XDDIMBREADTH")
    @Expose
    private String xddimbreadth;
    @SerializedName("XDDIMHEIGHT")
    @Expose
    private String xddimheight;
    @SerializedName("XINVOICENM")
    @Expose
    private String xinvoicenm;

    public String getXddqty() {
        return xddqty;
    }

    public void setXddqty(String xddqty) {
        this.xddqty = xddqty;
    }

    public String getXdddesc() {
        return xdddesc;
    }

    public void setXdddesc(String xdddesc) {
        this.xdddesc = xdddesc;
    }

    public String getXddimlength() {
        return xddimlength;
    }

    public void setXddimlength(String xddimlength) {
        this.xddimlength = xddimlength;
    }

    public String getXddimbreadth() {
        return xddimbreadth;
    }

    public void setXddimbreadth(String xddimbreadth) {
        this.xddimbreadth = xddimbreadth;
    }

    public String getXddimheight() {
        return xddimheight;
    }

    public void setXddimheight(String xddimheight) {
        this.xddimheight = xddimheight;
    }

    public String getXinvoicenm() {
        return xinvoicenm;
    }

    public void setXinvoicenm(String xinvoicenm) {
        this.xinvoicenm = xinvoicenm;
    }
}
