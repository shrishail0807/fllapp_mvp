package com.criticalog.ecritica.MVPCannoteEnquiry.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("docket_id")
    @Expose
    private String docketId;
    @SerializedName("consignor_code")
    @Expose
    private String consignorCode;
    @SerializedName("consignor_name")
    @Expose
    private String consignorName;
    @SerializedName("consignor_add")
    @Expose
    private String consignorAdd;
    @SerializedName("consignor_city_loc")
    @Expose
    private String consignorCityLoc;
    @SerializedName("consignor_city")
    @Expose
    private String consignorCity;
    @SerializedName("consignor_pin")
    @Expose
    private String consignorPin;
    @SerializedName("consignor_state")
    @Expose
    private String consignorState;
    @SerializedName("consignee_code")
    @Expose
    private String consigneeCode;
    @SerializedName("consignee_name")
    @Expose
    private String consigneeName;
    @SerializedName("consignee_add")
    @Expose
    private String consigneeAdd;
    @SerializedName("consignee_city_loc")
    @Expose
    private String consigneeCityLoc;
    @SerializedName("consignee_pin")
    @Expose
    private String consigneePin;
    @SerializedName("consignee_state")
    @Expose
    private String consigneeState;
    @SerializedName("Manifest")
    @Expose
    private Object manifest;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("XPAYVALUE")
    @Expose
    private String xpayvalue;
    @SerializedName("product_mode")
    @Expose
    private String productMode;
    @SerializedName("shipping_type")
    @Expose
    private String shippingType;
    @SerializedName("pick_date")
    @Expose
    private String pickDate;
    @SerializedName("pick_time")
    @Expose
    private String pickTime;
    @SerializedName("expected_dt")
    @Expose
    private String expectedDt;
    @SerializedName("expected_tm")
    @Expose
    private String expectedTm;
    @SerializedName("pieces")
    @Expose
    private String pieces;
    @SerializedName("actual_value")
    @Expose
    private String actualValue;
    @SerializedName("gross_wt")
    @Expose
    private String grossWt;
    @SerializedName("bill_wt")
    @Expose
    private String billWt;
    @SerializedName("dim_wt")
    @Expose
    private String dimWt;
    @SerializedName("XPRTCD")
    @Expose
    private String xprtcd;
    @SerializedName("l")
    @Expose
    private String l;
    @SerializedName("b")
    @Expose
    private String b;
    @SerializedName("h")
    @Expose
    private String h;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("pod")
    @Expose
    private String pod;
    @SerializedName("edp")
    @Expose
    private Object edp;
    @SerializedName("data_awb")
    @Expose
    private List<DataAwb> dataAwb = null;
    @SerializedName("data_event")
    @Expose
    private List<DataEvent> dataEvent = null;
    @SerializedName("data_part")
    @Expose
    private List<DataPart> dataPart = null;
    @SerializedName("pod_scan")
    @Expose
    private String podScan;
    @SerializedName("edp_attachment")
    @Expose
    private String edpAttachment;

    @SerializedName("is_trial")
    @Expose
    private String is_trial;

    public String getIs_trial() {
        return is_trial;
    }

    public void setIs_trial(String is_trial) {
        this.is_trial = is_trial;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getDocketId() {
        return docketId;
    }

    public void setDocketId(String docketId) {
        this.docketId = docketId;
    }

    public String getConsignorCode() {
        return consignorCode;
    }

    public void setConsignorCode(String consignorCode) {
        this.consignorCode = consignorCode;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorAdd() {
        return consignorAdd;
    }

    public void setConsignorAdd(String consignorAdd) {
        this.consignorAdd = consignorAdd;
    }

    public String getConsignorCityLoc() {
        return consignorCityLoc;
    }

    public void setConsignorCityLoc(String consignorCityLoc) {
        this.consignorCityLoc = consignorCityLoc;
    }

    public String getConsignorCity() {
        return consignorCity;
    }

    public void setConsignorCity(String consignorCity) {
        this.consignorCity = consignorCity;
    }

    public String getConsignorPin() {
        return consignorPin;
    }

    public void setConsignorPin(String consignorPin) {
        this.consignorPin = consignorPin;
    }

    public String getConsignorState() {
        return consignorState;
    }

    public void setConsignorState(String consignorState) {
        this.consignorState = consignorState;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeAdd() {
        return consigneeAdd;
    }

    public void setConsigneeAdd(String consigneeAdd) {
        this.consigneeAdd = consigneeAdd;
    }

    public String getConsigneeCityLoc() {
        return consigneeCityLoc;
    }

    public void setConsigneeCityLoc(String consigneeCityLoc) {
        this.consigneeCityLoc = consigneeCityLoc;
    }

    public String getConsigneePin() {
        return consigneePin;
    }

    public void setConsigneePin(String consigneePin) {
        this.consigneePin = consigneePin;
    }

    public String getConsigneeState() {
        return consigneeState;
    }

    public void setConsigneeState(String consigneeState) {
        this.consigneeState = consigneeState;
    }

    public Object getManifest() {
        return manifest;
    }

    public void setManifest(Object manifest) {
        this.manifest = manifest;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getXpayvalue() {
        return xpayvalue;
    }

    public void setXpayvalue(String xpayvalue) {
        this.xpayvalue = xpayvalue;
    }

    public String getProductMode() {
        return productMode;
    }

    public void setProductMode(String productMode) {
        this.productMode = productMode;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getPickDate() {
        return pickDate;
    }

    public void setPickDate(String pickDate) {
        this.pickDate = pickDate;
    }

    public String getPickTime() {
        return pickTime;
    }

    public void setPickTime(String pickTime) {
        this.pickTime = pickTime;
    }

    public String getExpectedDt() {
        return expectedDt;
    }

    public void setExpectedDt(String expectedDt) {
        this.expectedDt = expectedDt;
    }

    public String getExpectedTm() {
        return expectedTm;
    }

    public void setExpectedTm(String expectedTm) {
        this.expectedTm = expectedTm;
    }

    public String getPieces() {
        return pieces;
    }

    public void setPieces(String pieces) {
        this.pieces = pieces;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }

    public String getGrossWt() {
        return grossWt;
    }

    public void setGrossWt(String grossWt) {
        this.grossWt = grossWt;
    }

    public String getBillWt() {
        return billWt;
    }

    public void setBillWt(String billWt) {
        this.billWt = billWt;
    }

    public String getDimWt() {
        return dimWt;
    }

    public void setDimWt(String dimWt) {
        this.dimWt = dimWt;
    }

    public String getXprtcd() {
        return xprtcd;
    }

    public void setXprtcd(String xprtcd) {
        this.xprtcd = xprtcd;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public Object getEdp() {
        return edp;
    }

    public void setEdp(Object edp) {
        this.edp = edp;
    }

    public List<DataAwb> getDataAwb() {
        return dataAwb;
    }

    public void setDataAwb(List<DataAwb> dataAwb) {
        this.dataAwb = dataAwb;
    }

    public List<DataEvent> getDataEvent() {
        return dataEvent;
    }

    public void setDataEvent(List<DataEvent> dataEvent) {
        this.dataEvent = dataEvent;
    }

    public List<DataPart> getDataPart() {
        return dataPart;
    }

    public void setDataPart(List<DataPart> dataPart) {
        this.dataPart = dataPart;
    }

    public String getPodScan() {
        return podScan;
    }

    public void setPodScan(String podScan) {
        this.podScan = podScan;
    }

    public String getEdpAttachment() {
        return edpAttachment;
    }

    public void setEdpAttachment(String edpAttachment) {
        this.edpAttachment = edpAttachment;
    }
}
