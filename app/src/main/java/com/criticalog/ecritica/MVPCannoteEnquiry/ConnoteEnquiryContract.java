package com.criticalog.ecritica.MVPCannoteEnquiry;

import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateResponse;

public interface ConnoteEnquiryContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void connoteEnquiry(String token, ConnoteEnquiryRequest connoteEnquiryRequest);

        void SrServiceRequest(String token, SrServiceRequest srServiceRequest);

        void SrUpdateRequest(String token, SrUpdateRequest srUpdateRequest);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void connoteEnquirySetDataToViews(ConnoteEnquiryResponse connoteEnquiryResponse);

        void srServiceResponseDataToViews(SrServiceResponse srServiceResponse);

        void SrUpdateResponseDataToViews(SrUpdateResponse srUpdateResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface ConnoteEnquiryIntractor {

        interface OnFinishedListener {
            void onFinished(ConnoteEnquiryResponse connoteEnquiryResponse);

            void onFailure(Throwable t);
        }

        void connoteEnquiryRequest(ConnoteEnquiryContract.ConnoteEnquiryIntractor.OnFinishedListener onFinishedListener, String token, ConnoteEnquiryRequest connoteEnquiryRequest);
    }

    interface SrServiceIntractor {

        interface OnFinishedListener {
            void onFinished(SrServiceResponse srServiceResponse);

            void onFailure(Throwable t);
        }

        void srServiceRequest(ConnoteEnquiryContract.SrServiceIntractor.OnFinishedListener onFinishedListener, String token, SrServiceRequest srServiceRequest);
    }

    interface SrUpdateIntractor {

        interface OnFinishedListener {
            void onFinished(SrUpdateResponse srUpdateResponse);

            void onFailure(Throwable t);
        }

        void srUpdateRequest(ConnoteEnquiryContract.SrUpdateIntractor.OnFinishedListener onFinishedListener, String token, SrUpdateRequest srUpdateRequest);
    }


}
