package com.criticalog.ecritica.MVPCannoteEnquiry.Model;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SRDatum implements Serializable {
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("raised_by")
    @Expose
    private String raisedBy;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRaisedBy() {
        return raisedBy;
    }

    public void setRaisedBy(String raisedBy) {
        this.raisedBy = raisedBy;
    }

}
