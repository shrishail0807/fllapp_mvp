package com.criticalog.ecritica.MVPCannoteEnquiry;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.criticalog.ecritica.Activities.SRUpdateActivity;
import com.criticalog.ecritica.Adapters.Pager;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.Data;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataAwb;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataEvent;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataPart;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateResponse;
import com.criticalog.ecritica.MVPNegativeStatus.NegativeStatusActivity;
import com.criticalog.ecritica.ModelClasses.ConsigneeDeatils;
import com.criticalog.ecritica.ModelClasses.ConsignerDetails;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.EventResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class ConnoteEnquiry extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ConnoteEnquiryContract.MainView {
    private static String TAG = "TAG";
    ConsignerDetails consignerDetails;
    ConsigneeDeatils consigneeDeatils;
    List<DataPart> partDetailsArrayList = new ArrayList<>();
    List<DataAwb> awbDetailsArrayList = new ArrayList<>();
    String podimage, edpimage;

    ImageView back_button, mTrialShiment;
    Pager adapter;
    private LinearLayout progressbarlayout;

    EditText cononote_edittext;
    ViewPager pager;
    TabLayout tabs;
    LinearLayout mainlayout;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private TextView customer_code, customer_name, pay_value, pay_mode, service_type, pickup_date, ed_date,
            no_of_pcs, connote_value, actual_wt, dim_wt, product_mode, srupdate;

    private ConnoteEnquiryContract.presenter mPresenter;
    String userId, token;
    SimpleArcDialog mProgressDialog1;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connote_enquiry);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=criticalogSharedPreferences.getData("base_url_dynamic");

        userId = criticalogSharedPreferences.getData("userId");

        token = criticalogSharedPreferences.getData("token");

        StaticUtils.TOKEN = criticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = criticalogSharedPreferences.getData("distance_api_key");


        cononote_edittext = findViewById(R.id.connoteno);
        pager = findViewById(R.id.pager);
        tabs = findViewById(R.id.tabs);
        mainlayout = findViewById(R.id.mainlayout);
        customer_code = findViewById(R.id.cus_code);
        customer_name = findViewById(R.id.cus_name);
        pay_value = findViewById(R.id.pay_value);
        pay_mode = findViewById(R.id.pay_mode);
        service_type = findViewById(R.id.servicetype);
        pickup_date = findViewById(R.id.pu_date);
        ed_date = findViewById(R.id.edd_date);
        no_of_pcs = findViewById(R.id.no_of_pcs);
        connote_value = findViewById(R.id.con_value);
        actual_wt = findViewById(R.id.actual_wt);
        dim_wt = findViewById(R.id.dim_wt);
        back_button = findViewById(R.id.xtv_backbutton);
        product_mode = findViewById(R.id.product_mode);
        progressbarlayout = findViewById(R.id.llProgressBar);
        srupdate = findViewById(R.id.srupdate);
        mTrialShiment = findViewById(R.id.mTrialShiment);

        srupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String connoteno = cononote_edittext.getText().toString().trim();

                if (TextUtils.isEmpty(connoteno) || connoteno.matches("") || connoteno.length() < 9) {
                    cononote_edittext.setError("Please enter 9 digit Connote No");
                } else {

                    SrServiceRequest srServiceRequest = new SrServiceRequest();
                    srServiceRequest.setAction("service");
                    srServiceRequest.setTaskId(connoteno);
                    srServiceRequest.setUserId(userId);
                    srServiceRequest.setLatitude(String.valueOf(latitude));
                    srServiceRequest.setLongitude(String.valueOf(longitude));

                    mPresenter = new ConnoteEnquiryPresenterImpl(ConnoteEnquiry.this, new ConnoteEnquiryInteractorImpl(), new ConnoteEnquiryInteractorImpl(), new ConnoteEnquiryInteractorImpl());
                    mPresenter.SrServiceRequest(token, srServiceRequest);


                }

            }
        });


        pager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        tabs.addTab(tabs.newTab().setText("Track"));
        tabs.addTab(tabs.newTab().setText("Part Details"));
        tabs.addTab(tabs.newTab().setText("Airway Bill"));
        tabs.addTab(tabs.newTab().setText("POD Image"));
        tabs.addTab(tabs.newTab().setText("EDP Image"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        cononote_edittext.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (cononote_edittext.getRight() - cononote_edittext.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        mainlayout.setVisibility(View.GONE);
                        try {
                            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        getConnoteNo();

                        return true;
                    }
                }
                return false;
            }
        });


        tabs.addOnTabSelectedListener(this);


    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ConnoteEnquiry.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ConnoteEnquiry.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ConnoteEnquiry.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(ConnoteEnquiry.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        pager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }


    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void getConnoteNo() {

        String connoteno = cononote_edittext.getText().toString().trim();

        if (TextUtils.isEmpty(connoteno) || connoteno.matches("") || connoteno.length() < 9) {
            cononote_edittext.setError("Please enter 9 digit Connote No");
        } else {

            criticalogSharedPreferences.saveData("connote_no_sr_update", connoteno);
            ConnoteEnquiryRequest connoteEnquiryRequest = new ConnoteEnquiryRequest();
            connoteEnquiryRequest.setAction("connote_enquiry");
            connoteEnquiryRequest.setUserId(userId);
            connoteEnquiryRequest.setConnote(connoteno);
            connoteEnquiryRequest.setLatitude(String.valueOf(latitude));
            connoteEnquiryRequest.setLongitude(String.valueOf(longitude));

            mPresenter = new ConnoteEnquiryPresenterImpl(this, new ConnoteEnquiryInteractorImpl(), new ConnoteEnquiryInteractorImpl(), new ConnoteEnquiryInteractorImpl());
            mPresenter.connoteEnquiry(token, connoteEnquiryRequest);

        }
    }

    @Override
    public void showProgress() {
        mProgressDialog1.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog1.dismiss();
    }

    @Override
    public void connoteEnquirySetDataToViews(ConnoteEnquiryResponse connoteEnquiryResponse) {
        if (connoteEnquiryResponse != null) {
            if (connoteEnquiryResponse.getStatus() == 200) {
                Data connoteDetails = connoteEnquiryResponse.getData();
                List<DataEvent> totaleventResponseArrayList = new ArrayList<>();
                mainlayout.setVisibility(View.VISIBLE);

                List<DataPart> dataParts = new ArrayList<>();
                List<DataAwb> dataAwbs = new ArrayList<>();
                Data data_array = connoteEnquiryResponse.getData();

                dataParts = connoteEnquiryResponse.getData().getDataPart();

                dataAwbs = connoteEnquiryResponse.getData().getDataAwb();

                List<DataEvent> event_array = connoteEnquiryResponse.getData().getDataEvent();

                Type prslisttype = new TypeToken<ArrayList<EventResponse>>() {
                }.getType();
                Gson googleJson = new Gson();

                totaleventResponseArrayList = event_array;

                if (data_array != null) {
                    if (connoteEnquiryResponse != null) {

                        Log.d(TAG, connoteDetails.toString());
                        criticalogSharedPreferences.saveData("clientcodeforsrupdate", connoteDetails.getClientCode());

                        customer_code.setText(connoteDetails.getClientCode());
                        customer_name.setText(connoteDetails.getClientName());
                        pay_mode.setText(connoteDetails.getPaymentMode());
                        service_type.setText(connoteDetails.getServiceType());
                        pickup_date.setText(connoteDetails.getPickDate());
                        ed_date.setText(connoteDetails.getExpectedDt());
                        no_of_pcs.setText(connoteDetails.getPieces());
                        actual_wt.setText(connoteDetails.getGrossWt());
                        dim_wt.setText(connoteDetails.getDimWt());
                        pay_mode.setText(connoteDetails.getPaymentMode());
                        pay_value.setText(connoteDetails.getXpayvalue());
                        connote_value.setText(connoteDetails.getActualValue());
                        product_mode.setText(connoteDetails.getProductMode());

                        if (connoteDetails.getIs_trial() != null) {
                            if (connoteDetails.getIs_trial().equalsIgnoreCase("1")) {
                                mTrialShiment.setVisibility(View.VISIBLE);
                                Picasso.with(this)
                                        .load("https://www.ecritica.co/efreightlive/v2/images/trail-ship.png")
                                        .resize(200, 50)
                                        .placeholder(R.drawable.brokenimage)
                                        .into(mTrialShiment);
                            } else {
                                mTrialShiment.setImageBitmap(null);
                            }

                        } else {
                            mTrialShiment.setImageBitmap(null);
                        }


                        podimage = connoteDetails.getPod();
                        if (connoteDetails.getEdp() != null) {
                            edpimage = connoteDetails.getEdp().toString();
                        }

                        consignerDetails = new ConsignerDetails(connoteDetails.getConsignorName(), connoteDetails.getConsignorAdd(), connoteDetails.getConsignorCityLoc(), connoteDetails.getConsignorState(), connoteDetails.getConsignorPin());
                        consigneeDeatils = new ConsigneeDeatils(connoteDetails.getConsigneeName(), connoteDetails.getConsigneeAdd(), connoteDetails.getConsigneeCityLoc(), connoteDetails.getConsigneeState(), connoteDetails.getConsigneePin());

                        if (dataParts != null) {
                            partDetailsArrayList = dataParts;
                        }
                        if (dataAwbs != null) {
                            awbDetailsArrayList = dataAwbs;
                        }

                        if (partDetailsArrayList != null) {
                            adapter = new Pager(getSupportFragmentManager(), tabs.getTabCount(), consignerDetails, consigneeDeatils, totaleventResponseArrayList, partDetailsArrayList, awbDetailsArrayList, podimage, edpimage);
                            pager.setAdapter(adapter);
                            TabLayout.Tab tab = tabs.getTabAt(0);
                            tab.select();

                        } else {
                            adapter = new Pager(getSupportFragmentManager(), tabs.getTabCount(), consignerDetails, consigneeDeatils, totaleventResponseArrayList, partDetailsArrayList, awbDetailsArrayList, podimage, edpimage);
                            pager.setAdapter(adapter);
                            TabLayout.Tab tab = tabs.getTabAt(0);
                            tab.select();
                            Toast.makeText(ConnoteEnquiry.this, "No Part Details", Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {

                }
            } else {
                Toast.makeText(this, connoteEnquiryResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void srServiceResponseDataToViews(SrServiceResponse srServiceResponse) {
        if (srServiceResponse.getStatus() == 200) {
            //Log.d("sr display response",response.getData().toString());
            Intent intent = new Intent(this, SRUpdateActivity.class);
            intent.putExtra("details", (Serializable) srServiceResponse.getData());
            startActivity(intent);
        } else {
            Toasty.warning(ConnoteEnquiry.this, srServiceResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void SrUpdateResponseDataToViews(SrUpdateResponse srUpdateResponse) {

    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        mProgressDialog1.dismiss();
        Toast.makeText(ConnoteEnquiry.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }
}
