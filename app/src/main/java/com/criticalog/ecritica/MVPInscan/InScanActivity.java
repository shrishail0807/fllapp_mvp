package com.criticalog.ecritica.MVPInscan;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Adapters.InscanBoxAdapter;
import com.criticalog.ecritica.Adapters.RulesAdapterWithFlag;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPBarcodeScan.BarcodeScanActivity;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanRequest;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.MVPInscan.Model.DatumInspectionNegStatuses;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckRequest;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwaybillScan;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegativeStatusRequest;
import com.criticalog.ecritica.MVPInscan.Model.ItemSwatch;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.ScannedBox;
import com.criticalog.ecritica.MVPInscanNew.InScanActivityNewDev;
import com.criticalog.ecritica.MVPInscanNew.InscanInteractorImplNew;
import com.criticalog.ecritica.MVPInscanNew.InscanPresenterImplNew;
import com.criticalog.ecritica.MVPInscanNew.Model.ActualValueInscanRequestNew;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InScanActivity extends AppCompatActivity implements InscanContract.MainView {
    private Spinner spinner, noofpcs_spinner, spinnerSwatch;
    private EditText noofbox, destcity, destcontrolling, pudlocation, actualvalue, noofewaybill, connoteno, mL, mB, mH, mNetWt;
    private Button scanewaybill, scanbox, submit;
    private ImageView backbutton;
    private RecyclerView ewaybillrv, docketrv;
    private String pickupby, docketno, sourcepin, destinitionpin, box_no, ewaybill_no = "", docket, noofpcs;
    private OtpTextView originpin, destpin;
    private ArrayList scannedboxlist = new ArrayList();
    private ArrayList<ScannedBox> scannedBoxesLBHList = new ArrayList<ScannedBox>();
    private ArrayList scannedeWaybilllist = new ArrayList();
    private int no_of_box, no_of_ewaybill;
    private LinearLayout ewaybilllayout, resultlayout, rvlayout, ewaybilldetails, docketdetailslayout, inputlayou, progressbarlayout;
    InscanBoxAdapter adapter;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    ArrayList<EditText> myArray;
    private Dialog dialogManualEntry, dialohShowControllingHub;

    private LinearLayout conlayout;

    private TextView tv_scanconnote, tv_noofbox, singleconn, mControllingHub;
    private EditText et_box;

    private ImageButton scan_connote;
    InscanContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token;
    private SimpleArcDialog mProgressBar;
    private EditText mETEwayBill;
    private TextView mTextEwayBillDetails, mBoxDetailText;
    private RelativeLayout mQrScan, mRLManualConnote;
    private Button mBoxQRScan, mEwayBillQRScan;
    private RestServices mRestServices;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude = 0, longitude = 0;
    private ImageButton scan_connote1;
    List<ChecklistScan> checklistScansList;
    private Dialog dialogCheckListWithFlag;

    List<DatumInspectionNegStatuses> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    private String reasonCode = "", partBoxType = "", partCode = "", partDescr = "";

    private LinearLayout mBoxDetailsLinLay;
    String length;
    String breadth;
    String height;
    String netWeight;
    private LinearLayout mSwatchLay;
    private String autobox = "";
    private ArrayList<String> partDescriptionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscan);
        ButterKnife.bind(this);
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();
        inintviews();


    }

    public boolean validateNetWeight(String num) {
        if (num.matches("(^([0-9]{0,5})?)(\\.[0-9]{0,3})?$")) {
            Toast.makeText(this, "Valid", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(InScanActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(InScanActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(InScanActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(InScanActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("LAT LONG_INSCAN", String.valueOf(latitude) + "" + String.valueOf(longitude));

                } else {
                    Log.e("LAT LONG_INSCAN", String.valueOf(latitude) + "" + String.valueOf(longitude));
                    // mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void callEwayBillVlaidate(String ewayBillNo) {
        ArrayList ewayBillNum = new ArrayList();
        ewayBillNum.add(ewayBillNo);

        EwayBillValidateRequest ewayBillValidateRequest = new EwayBillValidateRequest();
        ewayBillValidateRequest.setAction("validate_ewaybill_with_doc_portal");
        ewayBillValidateRequest.setUserId(userId);
        ewayBillValidateRequest.setDocket(connoteno.getText().toString());
        ewayBillValidateRequest.setScannedEwaybill(ewayBillNum);
        mPresenter.ewayBillValidate(ewayBillValidateRequest);
    }

    public void callInscanBoxCheck(String DOCKET) {
        InScanBoxRequest inScanBoxRequest = new InScanBoxRequest();
        inScanBoxRequest.setAction("inscanbox_number");
        inScanBoxRequest.setUserId(userId);
        inScanBoxRequest.setBoxNumber(DOCKET);
        inScanBoxRequest.setLatitude(String.valueOf(latitude));
        inScanBoxRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
        mPresenter.inscanBoxNumberValidate(token, inScanBoxRequest);
    }

    public void callInscanDocketCheck(String connote) {

        DocketCheckRequest docketCheckRequest = new DocketCheckRequest();
        docketCheckRequest.setAction("inscan_docket_check");
        docketCheckRequest.setPickupby(pickupby);
        docketCheckRequest.setConnote(connote);
        docketCheckRequest.setUserId(userId);
        docketCheckRequest.setLatitude(String.valueOf(latitude));
        docketCheckRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
        mPresenter.inScanDocketCheck(token, docketCheckRequest);
    }


    public void manualDialog() {
        EditText mDocketEntry;
        TextView mAddDocket;
        ImageView mCancelDialog;
        //Dialog Manual Entry
        dialogManualEntry = new Dialog(InScanActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_scan);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);

        mDocketEntry = dialogManualEntry.findViewById(R.id.mDocketEntry);
        mAddDocket = dialogManualEntry.findViewById(R.id.mAddDocket);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);

        mDocketEntry.setHint("Enter Connote Number");
        mAddDocket.setText("Check");

        dialogManualEntry.show();
        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManualEntry.dismiss();
            }
        });


        mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docketno = mDocketEntry.getText().toString();

                if (docketno.length() == 9) {
                    callInscanDocketCheck(docketno);
                } else {
                    Toasty.warning(InScanActivity.this, "Enter Valid Connote", Toast.LENGTH_LONG, true).show();
                }
            }
        });


    }


    private void inintviews() {
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        spinner = findViewById(R.id.pickupbyspinner);
        noofpcs_spinner = findViewById(R.id.spinner);
        connoteno = findViewById(R.id.connoteno);
        noofbox = findViewById(R.id.boxno);
        originpin = findViewById(R.id.originpin);
        destpin = findViewById(R.id.destpin);
        destcity = findViewById(R.id.destcity);
        destcontrolling = findViewById(R.id.destcontrolling);
        pudlocation = findViewById(R.id.pudlocation);
        actualvalue = findViewById(R.id.actualvalue);
        noofewaybill = findViewById(R.id.ewaybill);
        scanewaybill = findViewById(R.id.ewaybillscan);
        scanbox = findViewById(R.id.boxscan);
        ewaybilllayout = findViewById(R.id.ewaybilllayout);
        resultlayout = findViewById(R.id.resultlayout);
        rvlayout = findViewById(R.id.rvlayout);
        ewaybilldetails = findViewById(R.id.ewaybilldetails);
        submit = findViewById(R.id.submit);
        docketdetailslayout = findViewById(R.id.docketdetailslayout);
        backbutton = findViewById(R.id.xtv_backbutton);
        scan_connote = findViewById(R.id.scan_connote);

        conlayout = findViewById(R.id.conlayout);
        tv_scanconnote = findViewById(R.id.tv_scancon);

        tv_noofbox = findViewById(R.id.textview10);
        et_box = findViewById(R.id.et_box);


        ewaybillrv = findViewById(R.id.ewaybilllist);
        docketrv = findViewById(R.id.boxlist);
        singleconn = findViewById(R.id.singleconn);
        mETEwayBill = findViewById(R.id.mETEwayBill);
        mTextEwayBillDetails = findViewById(R.id.mTextEwayBillDetails);
        mQrScan = findViewById(R.id.mQrScan);
        mBoxDetailText = findViewById(R.id.mBoxDetailText);
        mBoxQRScan = findViewById(R.id.mBoxQRScan);
        mEwayBillQRScan = findViewById(R.id.mEwayBillQRScan);
        mRLManualConnote = findViewById(R.id.mRLManualConnote);
        scan_connote1 = findViewById(R.id.scan_connote1);
        mBoxDetailsLinLay = findViewById(R.id.mBoxDetailsLinLay);
        mControllingHub = findViewById(R.id.mControllingHub);
        mL = findViewById(R.id.mL);
        mB = findViewById(R.id.mB);
        mH = findViewById(R.id.mH);
        mNetWt = findViewById(R.id.mNetWeight);
        mSwatchLay = findViewById(R.id.mSwatchLay);
        spinnerSwatch = findViewById(R.id.spinnerSwatch);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        docketrv.addItemDecoration(decoration);
        docketrv.setLayoutManager(layoutManager);


        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        DividerItemDecoration decoration1 = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        ewaybillrv.addItemDecoration(decoration1);
        ewaybillrv.setLayoutManager(layoutManager1);

        mEwayBillQRScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("scan_what", "ewaybill");
                ewaybill_no = noofewaybill.getText().toString().trim();
                criticalogSharedPreferences.saveData("scanwhat", "ewaybill");
                if (ewaybill_no.equalsIgnoreCase("") || ewaybill_no.equalsIgnoreCase("0")) {
                    Toasty.warning(InScanActivity.this, "Please enter valid  no of eWaybill", Toast.LENGTH_LONG, true).show();
                } else {
                    no_of_ewaybill = Integer.parseInt(ewaybill_no);
                    myArray = new ArrayList();

                    startBarcodeScan();
                }

            }
        });
        scan_connote1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannedboxlist.clear();
                scannedeWaybilllist.clear();
                mCriticalogSharedPreferences.saveData("scan_what", "connote");
                startBarcodeScan();
            }
        });

        mBoxQRScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("scan_what", "box");
                et_box.setVisibility(View.VISIBLE);
                mBoxDetailsLinLay.setVisibility(View.VISIBLE);
                requestFocus(mL);
                startBarcodeScan();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pickupby = parent.getItemAtPosition(position).toString();
                scannedboxlist.clear();
                scannedeWaybilllist.clear();
                resultlayout.setVisibility(View.GONE);
                scanbox.setVisibility(View.GONE);
                //  mBoxQRScan.setVisibility(View.GONE);
                connoteno.setText("");
                originpin.setOTP("");
                destpin.setOTP("");
                destcontrolling.setText("");
                pudlocation.setText("");
                actualvalue.setText("");
                noofbox.setText("");
                noofewaybill.setText("");
                destcity.setText("");
                scanewaybill.setVisibility(View.GONE);
                rvlayout.setVisibility(View.GONE);
                et_box.setVisibility(View.GONE);
                mBoxQRScan.setVisibility(View.GONE);
                mEwayBillQRScan.setVisibility(View.GONE);
                submit.setVisibility(View.GONE);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        scan_connote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criticalogSharedPreferences.saveData("scanwhat", "connote");
                scannedboxlist.clear();
                scannedeWaybilllist.clear();

                manualDialog();
                // startBarcodeScan();
                //code for TVS scanner
                //scanThroughTvscanner();

            }
        });
        scanbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criticalogSharedPreferences.saveData("scanwhat", "box");
                //  startBarcodeScan();
                //code changes for TVS scanner
                scan_BoxThroughTvsScanner();


            }
        });

        scanewaybill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ewaybill_no = noofewaybill.getText().toString().trim();
                criticalogSharedPreferences.saveData("scanwhat", "ewaybill");
                if (ewaybill_no.equalsIgnoreCase("") || ewaybill_no.equalsIgnoreCase("0")) {
                    Toasty.warning(InScanActivity.this, "Please enter valid  no of eWaybill", Toast.LENGTH_LONG, true).show();
                } else {
                    no_of_ewaybill = Integer.parseInt(ewaybill_no);
                    myArray = new ArrayList();
                }

                if (ewaybill_no.equalsIgnoreCase("") || ewaybill_no.matches("") || ewaybill_no.length() == 0) {

                } else if (no_of_ewaybill == 0) {

                } else {

                    ewaybilldetails.setVisibility(View.VISIBLE);
                    mETEwayBill.setVisibility(View.VISIBLE);

                    if (scannedboxlist.size() == 0) {
                        Toasty.warning(InScanActivity.this, "First Enter Box Details!!", Toast.LENGTH_SHORT, true).show();
                    }

                    mETEwayBill.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            String ewayBillNo = s.toString();

                            if (ewayBillNo.length() == 12) {
                                // progressbarlayout.setVisibility(View.VISIBLE);
                                if (scannedeWaybilllist.size() == Integer.parseInt(ewaybill_no)) {
                                    Toasty.success(InScanActivity.this, "All Scanned", Toast.LENGTH_LONG, true).show();
                                } else {
                                    if (scannedeWaybilllist.contains(ewayBillNo)) {
                                        //  Toasty.warning(InScanActivity.this,"Eway Bill already present!..enter new one", Toast.LENGTH_LONG, true).show();
                                    } else {
                                        callEwayBillVlaidate(ewayBillNo);
                                        //     Toasty.success(InScanActivity.this,"Eway Bill " + String.valueOf(scannedeWaybilllist.size()) + " of " + ewaybill_no + " Scanned", Toast.LENGTH_LONG, true).show();
                                        mETEwayBill.setText("");
                                        mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                        ewaybilldetails.setVisibility(View.VISIBLE);
                                        adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivity.this);
                                        ewaybillrv.setAdapter(adapter);
                                    }
                                }
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    // scanewaybill.setVisibility(View.GONE);

                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scannedboxlist.size() != no_of_box) {
                    Toasty.warning(InScanActivity.this, "please scan box no", Toast.LENGTH_LONG, true).show();

                } else if (ewaybilllayout.getVisibility() == View.VISIBLE) {


                    if (scannedeWaybilllist.size() == no_of_ewaybill) {
                        // progressbarlayout.setVisibility(View.VISIBLE);

                        if (no_of_ewaybill == 0) {
                            Toasty.warning(InScanActivity.this, "Enter Eway Bill Details!!", Toast.LENGTH_LONG, true).show();
                        } else {
                            callInScanSubmit();
                        }

                    } else {
                        Toasty.warning(InScanActivity.this, "Enter all eway bill number!!", Toast.LENGTH_LONG, true).show();
                    }

                } else {

                    callInScanSubmit();

                }
            }
        });


        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(new Intent(InScanActivity.this, HomeActivity.class));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                finish();
            }
        });

        et_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 9) {

                    if (!scannedboxlist.contains(s.toString())) {
                        Log.d("inside if", scannedboxlist.toString());
                        docket = s.toString();

                        length = mL.getText().toString();
                        breadth = mB.getText().toString();
                        height = mH.getText().toString();
                        netWeight = mNetWt.getText().toString();

                        if (length.equalsIgnoreCase("") || breadth.equalsIgnoreCase("") || height.equalsIgnoreCase("") || netWeight.equalsIgnoreCase("")) {
                            Toast.makeText(InScanActivity.this, "Enter L,B,H and Net Weight", Toast.LENGTH_SHORT).show();
                        } else if (length.equalsIgnoreCase("0") || breadth.equalsIgnoreCase("0") || height.equalsIgnoreCase("0") || netWeight.equalsIgnoreCase("0")) {
                            Toast.makeText(InScanActivity.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                        } else if (length.equalsIgnoreCase("00") || breadth.equalsIgnoreCase("00") || height.equalsIgnoreCase("00") || netWeight.equalsIgnoreCase("00")) {
                            Toast.makeText(InScanActivity.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                        } else if (length.equalsIgnoreCase("000") || breadth.equalsIgnoreCase("000") || height.equalsIgnoreCase("000") || netWeight.equalsIgnoreCase("000")) {
                            Toast.makeText(InScanActivity.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                        } else {

                            if (autobox.equalsIgnoreCase("2")) {
                                callInscanBoxCheck(docket);
                            } else {
                                boolean status = validateNetWeight(mNetWt.getText().toString());

                                if (status == true) {
                                    callInscanBoxCheck(docket);
                                } else {
                                    Toast.makeText(InScanActivity.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                    mNetWt.getText().clear();
                                }
                            }
                            /*boolean status = validateNetWeight(mNetWt.getText().toString());

                            if (status == true) {
                                callInscanBoxCheck(docket);
                            } else {
                                Toast.makeText(InScanActivity.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                mNetWt.getText().clear();
                            }*/

                        }
                        //   progressbarlayout.setVisibility(View.VISIBLE);


                    } else {
                        Toasty.warning(InScanActivity.this, "Box already scanned", Toast.LENGTH_LONG, true).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startBarcodeScan();
                            }
                        }, 2000);

                        //changes for TVSscanner

                        scan_BoxThroughTvsScanner();
                    }

                }
            }
        });
    }

    private void scanThroughTvscanner() {

        conlayout.setVisibility(View.VISIBLE);
        requestFocus(connoteno);

    }

    private void scan_BoxThroughTvsScanner() {


        if (autobox.equalsIgnoreCase("2")) {

            mSwatchLay.setVisibility(View.VISIBLE);
            mL.setFocusable(false);
            mB.setFocusable(false);
            mH.setFocusable(false);
            mNetWt.setFocusable(false);


            spinnerSwatch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    partDescr = parent.getItemAtPosition(position).toString();
                    partCode = partDescriptionList.get(position).toString();
                    Toast.makeText(InScanActivity.this, partCode + "  " + partDescr, Toast.LENGTH_SHORT).show();

                    if (!partCode.equalsIgnoreCase("Select part code")) {
                        InscanGetItemDetailsRequest inscanGetItemDetailsRequest = new InscanGetItemDetailsRequest();
                        inscanGetItemDetailsRequest.setAction("inscan_get_item_details");
                        inscanGetItemDetailsRequest.setUserId(userId);
                        inscanGetItemDetailsRequest.setPartCode(partCode);
                        mPresenter.inscanItemsDetaislRequest(inscanGetItemDetailsRequest);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        et_box.setVisibility(View.VISIBLE);
        mBoxDetailsLinLay.setVisibility(View.VISIBLE);
        requestFocus(mL);
        scanbox.setVisibility(View.GONE);

    }

    private void scan_BoxThroughTvsScannerEway() {
        // et_box.setVisibility(View.VISIBLE);
        requestFocus(et_box);
        //scanbox.setVisibility(View.GONE);
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void callInScanSubmit() {
        if (noofpcs.equalsIgnoreCase("Select Box Type")) {
            Toast.makeText(this, "Select Box Type", Toast.LENGTH_SHORT).show();
        } else {
            ArrayList<ScannedBox> scannedBoxesLBHList1 = scannedBoxesLBHList;
            InScanSubmitRequest inScanSubmitRequest = new InScanSubmitRequest();
            inScanSubmitRequest.setAction("inscan_submit_with_doc");
            inScanSubmitRequest.setActualValue(actualvalue.getText().toString());
            inScanSubmitRequest.setConnoteno(connoteno.getText().toString());
            inScanSubmitRequest.setDestPin(destpin.getOTP());
            inScanSubmitRequest.setNoofpcs(noofpcs);
            inScanSubmitRequest.setScannedBox(scannedBoxesLBHList1);
            inScanSubmitRequest.setOriPin(originpin.getOTP());
            inScanSubmitRequest.setScannedEwaybill(scannedeWaybilllist);
            inScanSubmitRequest.setUserId(userId);
            inScanSubmitRequest.setPickupby(pickupby);
            inScanSubmitRequest.setLatitude(String.valueOf(latitude));
            inScanSubmitRequest.setLongitude(String.valueOf(longitude));
            inScanSubmitRequest.setAuto_box(autobox);

            mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
            mPresenter.inScanSubmit(token, inScanSubmitRequest);
        }

    }

    public void userRiderLog() {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;

        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo("");
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId("");
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(connoteno.getText().toString());
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("10");
        riderLogRequest.setmLastKm("");
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("3");
        riderLogRequest.setActionNumber(10);
        riderLogRequest.setOsVersion(androidOS);
        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
      /*  if (criticalogSharedPreferences.getData("scanwhat").equalsIgnoreCase("connote")) {
            integrator.setPrompt("Scan Connote Number");
        } else if (criticalogSharedPreferences.getData("scanwhat").equalsIgnoreCase("box")) {
            integrator.setPrompt("Scan Box Number");
        }*/
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String scanwhat = mCriticalogSharedPreferences.getData("scan_what");
        if (result != null) {
            try {
                String MobilePattern = "[0-9]{9}";
                docketno = result.getContents();
                docket = result.getContents();

                if (scanwhat.equalsIgnoreCase("connote")) {
                    if (docketno.length() == 9) {
                        callInscanDocketCheck(docketno);

                    } else {
                        resultlayout.setVisibility(View.GONE);
                        scanbox.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        Toasty.warning(InScanActivity.this, "Scan Valid Connote", Toast.LENGTH_LONG, true).show();
                    }
                } else if (scanwhat.equalsIgnoreCase("box")) {
                    if (docketno.length() == 9) {

                        if (!scannedboxlist.contains(docketno)) {

                            length = mL.getText().toString();
                            breadth = mB.getText().toString();
                            height = mH.getText().toString();
                            netWeight = mNetWt.getText().toString();


                            if (length.equalsIgnoreCase("") || breadth.equalsIgnoreCase("") || height.equalsIgnoreCase("") || netWeight.equalsIgnoreCase("")) {
                                Toast.makeText(InScanActivity.this, "Enter L,B,H and Net Weight", Toast.LENGTH_SHORT).show();
                            } else if (length.equalsIgnoreCase("0") || breadth.equalsIgnoreCase("0") || height.equalsIgnoreCase("0") || netWeight.equalsIgnoreCase("0")) {
                                Toast.makeText(InScanActivity.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                            } else if (length.equalsIgnoreCase("00") || breadth.equalsIgnoreCase("00") || height.equalsIgnoreCase("00") || netWeight.equalsIgnoreCase("00")) {
                                Toast.makeText(InScanActivity.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                            } else if (length.equalsIgnoreCase("000") || breadth.equalsIgnoreCase("000") || height.equalsIgnoreCase("000") || netWeight.equalsIgnoreCase("000")) {
                                Toast.makeText(InScanActivity.this, "Enter L,B,H Cant be Zero", Toast.LENGTH_SHORT).show();
                            } else {

                                if (autobox.equalsIgnoreCase("2")) {
                                    callInscanBoxCheck(docket);
                                } else {
                                    boolean status = validateNetWeight(mNetWt.getText().toString());

                                    if (status == true) {
                                        callInscanBoxCheck(docket);
                                    } else {
                                        Toast.makeText(InScanActivity.this, "Enter Valid Net Weight!!", Toast.LENGTH_SHORT).show();
                                        mNetWt.getText().clear();
                                    }
                                }

                            }
                            //   progressbarlayout.setVisibility(View.VISIBLE);
                            //  callInscanBoxCheck(docketno);

                        } else {

                            Toasty.warning(InScanActivity.this, "Box already scanned!!", Toast.LENGTH_LONG, true).show();
                            // startBarcodeScan();
                        }

                    } else {
                        Toasty.warning(InScanActivity.this, "Scan Valid Box Number!!", Toast.LENGTH_LONG, true).show();

                    }
                } else {
                    if (docketno.length() == 12) {

                        if (!scannedboxlist.contains(docketno)) {

                            //   progressbarlayout.setVisibility(View.VISIBLE);
                            callEwayBillVlaidate(docketno);

                        } else {
                            Toasty.warning(InScanActivity.this, "Eway bill already scanned", Toast.LENGTH_LONG, true).show();
                            // startBarcodeScan();
                        }

                    } else {
                        Toasty.warning(InScanActivity.this, "Scan Valid Ewaybill Number!!", Toast.LENGTH_LONG, true).show();

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                //that means the encoded format not matches
                //in this case you can display whatever data is available on the qrcode

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    public void dialogCheckListWithFlag(List<ChecklistScan> prsRules) {
        TextView mDone, mFailed;
        RecyclerView mRvRules;
        RulesAdapterWithFlag rulesAdapter;
        CheckBox mCheckYes, mCheckNo;
        dialogCheckListWithFlag = new Dialog(InScanActivity.this);
        dialogCheckListWithFlag.setContentView(R.layout.dialog_rule_with_flag);
        Window window = dialogCheckListWithFlag.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mDone = dialogCheckListWithFlag.findViewById(R.id.mDone);
        mFailed = dialogCheckListWithFlag.findViewById(R.id.mFailed);
        mRvRules = dialogCheckListWithFlag.findViewById(R.id.mRvRules);

        dialogCheckListWithFlag.setCancelable(false);
        rulesAdapter = new RulesAdapterWithFlag(prsRules);
        mRvRules.setLayoutManager(new LinearLayoutManager(this));
        mRvRules.setAdapter(rulesAdapter);

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InspectionDoneRequest inspectionDoneRequest = new InspectionDoneRequest();
                inspectionDoneRequest.setAction("inspection_done");
                inspectionDoneRequest.setUserId(userId);
                inspectionDoneRequest.setDocketNo(connoteno.getText().toString());
                inspectionDoneRequest.setLatitude(String.valueOf(latitude));
                inspectionDoneRequest.setLatitude(String.valueOf(longitude));

                mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                mPresenter.inspectionDoneRequest(inspectionDoneRequest);
                dialogCheckListWithFlag.dismiss();
            }
        });

        mFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reasonList.clear();
                reasons.clear();
                InspectionNegativeStatusRequest inspectionNegativeStatusRequest = new InspectionNegativeStatusRequest();
                inspectionNegativeStatusRequest.setAction("negative_status_inspection");
                inspectionNegativeStatusRequest.setUserId(userId);
                inspectionNegativeStatusRequest.setLatitude(String.valueOf(latitude));
                inspectionNegativeStatusRequest.setLongitude(String.valueOf(longitude));

                mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                mPresenter.inspectionNegativeStatuse(inspectionNegativeStatusRequest);

            }
        });
        dialogCheckListWithFlag.show();

    }

    @Override
    public void setDocketCheckDataToViews(DocketCheckResponse docketCheckResponse) {
        if (docketCheckResponse.getStatus() == 200) {
            if (dialogManualEntry != null) {
                dialogManualEntry.dismiss();
            }

            autobox = docketCheckResponse.getData().get(0).getAutobox();

            if (autobox.equalsIgnoreCase("2")) {
                ArrayList list = new ArrayList();
                List<ItemSwatch> itemSwatchList = docketCheckResponse.getData().get(0).getItems();
                for (int i = 0; i < itemSwatchList.size(); i++) {
                    list.add(itemSwatchList.get(i).getPartDesc());
                    partDescriptionList.add(itemSwatchList.get(i).getPartCode());
                }
                //   spinnerSwatch.setVisibility(View.VISIBLE);
                ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spinnerSwatch.setAdapter(aa);
            }
            connoteno.setEnabled(false);
            connoteno.setText(docketno);

            if (pickupby.equalsIgnoreCase("OWN")) {
                if (docketCheckResponse.getStatus() == 200) {
                    resultlayout.setVisibility(View.VISIBLE);
                    tv_noofbox.setVisibility(View.VISIBLE);
                    //   mBoxDetailText.setVisibility(View.GONE);
                    docketrv.setVisibility(View.GONE);
                    submit.setVisibility(View.GONE);

                    noofbox.setText(String.valueOf(docketCheckResponse.getData().get(0).getNoOfPcs()));
                    no_of_box = docketCheckResponse.getData().get(0).getNoOfPcs();
                    int noOfPcsFlag = docketCheckResponse.getData().get(0).getFlagNoOfPcs();

                    //  tv_noofbox.setVisibility(View.VISIBLE);
                    noofbox.setVisibility(View.VISIBLE);

                    checklistScansList = docketCheckResponse.getData().get(0).getChecklistScan();
                    if (checklistScansList != null) {
                        if (checklistScansList.size() > 0) {
                            dialogCheckListWithFlag(checklistScansList);
                            // Toast.makeText(InScanActivity.this, docketCheckResponse.getData().get(0).getChecklistScan().get(0).getRule(), Toast.LENGTH_SHORT).show();
                        }
                    }


                    if (noOfPcsFlag == 1) {
                        ArrayList list = new ArrayList();
                        List jsonArray = docketCheckResponse.getData().get(0).getTabnoOfPcs();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            list.add(jsonArray.get(i));
                        }
                        noofpcs_spinner.setVisibility(View.VISIBLE);
                        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        noofpcs_spinner.setAdapter(aa);


                        noofpcs_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                noofpcs = parent.getItemAtPosition(position).toString();
                                Log.d("inside", " own  if flag 1" + noofpcs);
                                rvlayout.setVisibility(View.VISIBLE);

                                docketdetailslayout.setVisibility(View.VISIBLE);

                                try {
                                    singleconn.setText(docketCheckResponse.getData().get(0).getDocketno());

                                    if (!scannedboxlist.contains(docketCheckResponse.getData().get(0).getDocketno()))
                                        scannedboxlist.add(docketCheckResponse.getData().get(0).getDocketno());
                                    scannedBoxesLBHList.add(new ScannedBox(docketCheckResponse.getData().get(0).getDocketno(), "", "", "", "", "", ""));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                singleconn.setVisibility(View.VISIBLE);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                rvlayout.setVisibility(View.VISIBLE);

                                docketdetailslayout.setVisibility(View.VISIBLE);

                                try {
                                    singleconn.setText(docketCheckResponse.getData().get(0).getDocketno());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                singleconn.setVisibility(View.VISIBLE);
                            }
                        });

                        submit.setVisibility(View.VISIBLE);
                    } else if (noOfPcsFlag == 0) {
                        noofbox.setText(String.valueOf(docketCheckResponse.getData().get(0).getNoOfPcs()));
                        scanbox.setVisibility(View.VISIBLE);
                        mBoxQRScan.setVisibility(View.VISIBLE);
                        noofbox.setVisibility(View.VISIBLE);
                        noofbox.requestFocus();
                        noofpcs = noofbox.getText().toString();

                        Log.d("inside", " own else if flag 0" + noofpcs);

                    }


                    //replacing done here
                    originpin.setOTP(docketCheckResponse.getData().get(0).getOrgPin());
                    destpin.setOTP(docketCheckResponse.getData().get(0).getDestPin());
                    actualvalue.setText(String.valueOf(docketCheckResponse.getData().get(0).getActualVal()));
                    pudlocation.setText(docketCheckResponse.getData().get(0).getPudLoc());
                    destcontrolling.setText(docketCheckResponse.getData().get(0).getHubLoc());
                    destcity.setText(docketCheckResponse.getData().get(0).getCityName());

                    originpin.setEnabled(false);
                    destpin.setEnabled(false);
                    actualvalue.setEnabled(true);

                    noofbox.setEnabled(false);
                    if (docketCheckResponse.getData().get(0).getEwaybill().equalsIgnoreCase("WBDLMH")) {
                        //display input for eway bill no and ewaybillscan
                      /*  ewaybilllayout.setVisibility(View.VISIBLE);
                        scanewaybill.setVisibility(View.VISIBLE);
                        mEwayBillQRScan.setVisibility(View.VISIBLE);
                        ewaybilldetails.setVisibility(View.VISIBLE);*/

                        ActualValueInscanRequest actualValueInscanRequest = new ActualValueInscanRequest();
                        actualValueInscanRequest.setAction("inscan_actualvalue_new");
                        actualValueInscanRequest.setClientCode(docketCheckResponse.getData().get(0).getClient_code().toString());
                        actualValueInscanRequest.setActualVal(docketCheckResponse.getData().get(0).getActualVal().toString());
                        actualValueInscanRequest.setUserId(userId);
                        actualValueInscanRequest.setOrigin(docketCheckResponse.getData().get(0).getOrgPin());
                        actualValueInscanRequest.setDest(docketCheckResponse.getData().get(0).getDestPin());
                        actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                        actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                        mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                        mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);

                        List<EwaybillScan> ewaybillScanList = docketCheckResponse.getData().get(0).getEwaybillScan();

                        if (ewaybillScanList != null) {
                            if (ewaybillScanList.size() > 0) {

                                for (int k = 0; k < ewaybillScanList.size(); k++) {
                                    scannedeWaybilllist.add(ewaybillScanList.get(k).getXwaybillno());
                                }
                                no_of_ewaybill = ewaybillScanList.size();
                                noofewaybill.setText(String.valueOf(ewaybillScanList.size()));
                                noofewaybill.setEnabled(false);

                                mETEwayBill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);

                                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                ewaybillrv.setVisibility(View.VISIBLE);
                                rvlayout.setVisibility(View.VISIBLE);

                                scan_BoxThroughTvsScannerEway();
                                docketdetailslayout.setVisibility(View.VISIBLE);
                                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivity.this);
                                ewaybillrv.setAdapter(adapter);

                                //Toast.makeText(this, "ENABLE EWAY", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        ewaybilllayout.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        ewaybilldetails.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);

                        List<EwaybillScan> ewaybillScanList = docketCheckResponse.getData().get(0).getEwaybillScan();

                        if (ewaybillScanList != null) {

                            if (ewaybillScanList.size() > 0) {
                                ewaybilllayout.setVisibility(View.VISIBLE);
                                scanewaybill.setVisibility(View.VISIBLE);
                                mEwayBillQRScan.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                for (int k = 0; k < ewaybillScanList.size(); k++) {
                                    scannedeWaybilllist.add(ewaybillScanList.get(k).getXwaybillno());
                                }
                                no_of_ewaybill = ewaybillScanList.size();
                                noofewaybill.setText(String.valueOf(ewaybillScanList.size()));
                                noofewaybill.setEnabled(false);

                                mETEwayBill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);

                                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                ewaybillrv.setVisibility(View.VISIBLE);
                                rvlayout.setVisibility(View.VISIBLE);

                                scan_BoxThroughTvsScannerEway();
                                docketdetailslayout.setVisibility(View.VISIBLE);
                                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivity.this);
                                ewaybillrv.setAdapter(adapter);

                                //Toast.makeText(this, "ENABLE EWAY", Toast.LENGTH_SHORT).show();
                            } else {
                                ewaybilllayout.setVisibility(View.GONE);
                                scanewaybill.setVisibility(View.GONE);
                                mEwayBillQRScan.setVisibility(View.GONE);
                                ewaybilldetails.setVisibility(View.GONE);
                            }
                        }

                    }


                } else {
                    Toasty.warning(InScanActivity.this, docketCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();

                    scan_connote.setVisibility(View.VISIBLE);
                    tv_scanconnote.setVisibility(View.VISIBLE);
                    resultlayout.setVisibility(View.GONE);
                    scanbox.setVisibility(View.GONE);
                    //  mBoxQRScan.setVisibility(View.GONE);
                    scanewaybill.setVisibility(View.GONE);
                    mEwayBillQRScan.setVisibility(View.GONE);
                }

            } else if (pickupby.equalsIgnoreCase("THIRDPARTY")) {
                if (docketCheckResponse.getStatus() == 200) {

                    if (docketCheckResponse.getMessage().equalsIgnoreCase("ValidManual")) {

                        resultlayout.setVisibility(View.VISIBLE);
                        ewaybilllayout.setVisibility(View.GONE);
                        tv_noofbox.setVisibility(View.GONE);
                        noofbox.setVisibility(View.GONE);


                        connoteno.setText(docketno);

                        originpin.setOTP("");
                        destpin.setOTP("");
                        destcity.setText("");
                        destcontrolling.setText("");
                        pudlocation.setText("");
                        actualvalue.setText("");
                        noofbox.setText("");
                        noofbox.setEnabled(true);
                        scanbox.setVisibility(View.GONE);
                        // actualvalue.setEnabled(true);
                        //  mBoxQRScan.setVisibility(View.GONE);
                        //   mBoxDetailText.setVisibility(View.GONE);
                        docketrv.setVisibility(View.GONE);
                        //submit.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        mTextEwayBillDetails.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);


                        originpin.setSaveEnabled(true);
                        destpin.setSaveEnabled(true);
                        pudlocation.setSaveEnabled(true);
                        destcontrolling.setSaveEnabled(true);
                        destcity.setSaveEnabled(true);
                        actualvalue.setSaveEnabled(true);
                        originpin.setFocusable(true);
                        destpin.setFocusable(true);
                        pudlocation.setFocusable(true);
                        destcontrolling.setFocusable(true);
                        destcity.setFocusable(true);


                        originpin.setOtpListener(new OTPListener() {
                            @Override
                            public void onInteractionListener() {

                            }

                            @Override
                            public void onOTPComplete(String otp) {
                                sourcepin = originpin.getOTP();
                                //    progressbarlayout.setVisibility(View.VISIBLE);
                                mCriticalogSharedPreferences.saveData("origin_destination", "origin");


                                OriginPincodeValidateRequest originPincodeValidateRequest = new OriginPincodeValidateRequest();

                                originPincodeValidateRequest.setPincode(sourcepin);
                                originPincodeValidateRequest.setAction("inscan_pincode_validation");
                                originPincodeValidateRequest.setUserId(userId);
                                originPincodeValidateRequest.setLatitude(String.valueOf(latitude));
                                originPincodeValidateRequest.setLongitude(String.valueOf(longitude));


                                mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                                mPresenter.validateOriginPicode(token, originPincodeValidateRequest);
                                //  new InscanOriginpincodevalidationvolley(InscanActivity.this, sourcepin, InscanActivity.this);

                            }
                        });


                        destpin.setOtpListener(new OTPListener() {
                            @Override
                            public void onInteractionListener() {
                                destcity.setText("");
                                destcontrolling.setText("");
                                pudlocation.setText("");

                            }

                            @Override
                            public void onOTPComplete(String otp) {
                                destinitionpin = destpin.getOTP();
                                //   progressbarlayout.setVisibility(View.VISIBLE);
                                mCriticalogSharedPreferences.saveData("origin_destination", "destination");
                                OriginPincodeValidateRequest originPincodeValidateRequest = new OriginPincodeValidateRequest();

                                originPincodeValidateRequest.setPincode(destinitionpin);
                                originPincodeValidateRequest.setAction("inscan_pincode_validation");
                                originPincodeValidateRequest.setUserId(userId);
                                originPincodeValidateRequest.setLatitude(String.valueOf(latitude));
                                originPincodeValidateRequest.setLongitude(String.valueOf(longitude));

                                mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                                mPresenter.validateOriginPicode(token, originPincodeValidateRequest);
                                //   new IInscandestpincodeValidationvolley(InscanActivity.this, destinitionpin, InscanActivity.this);

                            }
                        });

                        actualvalue.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                                Log.d("inside", "ontextchane" + s);
                                String amount = s.toString();
                                if (amount.length() > 0) {
                                    double amounttoint = Double.parseDouble(amount);


                                    if (amounttoint == 0) {
                                        Toasty.warning(InScanActivity.this, "Actual value should be greater than 0", Toast.LENGTH_LONG, true).show();
                                    } else if (amounttoint >= 50000) {
                                       /* ewaybilllayout.setVisibility(View.VISIBLE);
                                        ewaybilldetails.setVisibility(View.VISIBLE);
                                        tv_noofbox.setVisibility(View.VISIBLE);*/
                                        // noofbox.setVisibility(View.VISIBLE);

                                        //   progressbarlayout.setVisibility(View.VISIBLE);


                                        ActualValueInscanRequest actualValueInscanRequest = new ActualValueInscanRequest();
                                        actualValueInscanRequest.setAction("inscan_actualvalue_new");
                                        actualValueInscanRequest.setClientCode(docketCheckResponse.getData().get(0).getClient_code().toString());
                                        actualValueInscanRequest.setActualVal(String.valueOf(amount));
                                        actualValueInscanRequest.setUserId(userId);
                                        actualValueInscanRequest.setOrigin(sourcepin);
                                        actualValueInscanRequest.setDest(destinitionpin);
                                        actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                                        actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                                        mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                                        mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);

                                        // new InScanActualValueVolley(InscanActivity.this, InscanActivity.this, sourcepin, destinitionpin, amount);
                                        noofbox.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                box_no = noofbox.getText().toString().trim();
                                                criticalogSharedPreferences.saveData("scanwhat", "box");
                                                if (!(box_no.matches("")))
                                                    no_of_box = Integer.parseInt(box_no);

                                                if (box_no.equalsIgnoreCase("") || box_no.matches("") || box_no.length() == 0) {
                                                    Toasty.warning(InScanActivity.this, "Please enter valid  box no", Toast.LENGTH_LONG, true).show();

                                                } else if (no_of_box == 0) {
                                                    Toasty.warning(InScanActivity.this, "Box no should be greater than 0", Toast.LENGTH_LONG, true).show();

                                                } else {

                                                    scanbox.setVisibility(View.VISIBLE);
                                                    noofbox.setVisibility(View.VISIBLE);
                                                    // mBoxQRScan.setVisibility(View.VISIBLE);
                                                    //mBoxQRScan.setVisibility(View.VISIBLE);
                                                    noofpcs = box_no;


                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                    } else {
                                        ewaybilllayout.setVisibility(View.GONE);
                                        scanewaybill.setVisibility(View.GONE);
                                        mEwayBillQRScan.setVisibility(View.GONE);
                                        ewaybilldetails.setVisibility(View.GONE);
                                        tv_noofbox.setVisibility(View.VISIBLE);
                                        noofbox.setVisibility(View.VISIBLE);
                                        //  mBoxQRScan.setVisibility(View.VISIBLE);

                                        noofbox.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                box_no = noofbox.getText().toString().trim();

                                                criticalogSharedPreferences.saveData("scanwhat", "box");
                                                if (!(box_no.matches("")))
                                                    no_of_box = Integer.parseInt(box_no);
                                                if (box_no.equalsIgnoreCase("") || box_no.matches("") || box_no.length() == 0) {
                                                    Toasty.warning(InScanActivity.this, "Please enter valid  box no", Toast.LENGTH_LONG, true).show();

                                                } else if (no_of_box == 0) {
                                                    Toasty.warning(InScanActivity.this, "Box no should be greater than 0", Toast.LENGTH_LONG, true).show();

                                                } else {
                                                    mBoxDetailText.setVisibility(View.VISIBLE);
                                                    scanbox.setVisibility(View.VISIBLE);
                                                    mBoxQRScan.setVisibility(View.VISIBLE);
                                                    noofpcs = String.valueOf(box_no);
                                                    Log.d("inside", "third party valid manual actual value <50000" + noofpcs);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                    }
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });

                    } else if (docketCheckResponse.getMessage().equalsIgnoreCase("validCitAll")) {
                        resultlayout.setVisibility(View.VISIBLE);
                        //  mBoxDetailText.setVisibility(View.GONE);
                        docketrv.setVisibility(View.GONE);
                        //    submit.setVisibility(View.GONE);
                        scanewaybill.setVisibility(View.GONE);
                        mEwayBillQRScan.setVisibility(View.GONE);

                        originpin.setOTP(docketCheckResponse.getData().get(0).getOrgPin());
                        destpin.setOTP(docketCheckResponse.getData().get(0).getDestPin());
                        actualvalue.setText(String.valueOf(docketCheckResponse.getData().get(0).getActualVal()));

                        String amount = docketCheckResponse.getData().get(0).getActualVal().toString();

                        if (amount.length() > 0) {
                            double amounttoint = Double.parseDouble(amount);


                            if (amounttoint == 0) {
                                Toasty.warning(InScanActivity.this, "Actual value should be greater than 0", Toast.LENGTH_LONG, true).show();
                            } else if (amounttoint >= 50000) {
                              /*  ewaybilllayout.setVisibility(View.VISIBLE);
                                ewaybilldetails.setVisibility(View.VISIBLE);
                                tv_noofbox.setVisibility(View.VISIBLE);*/
                                // noofbox.setVisibility(View.VISIBLE);

                                //   progressbarlayout.setVisibility(View.VISIBLE);

                                ActualValueInscanRequest actualValueInscanRequest = new ActualValueInscanRequest();
                                actualValueInscanRequest.setAction("inscan_actualvalue_new");
                                /*if(docketCheckResponse.getData().get(0).getClient_code()==null)
                                {
                                    actualValueInscanRequest.setClientCode("");
                                }else {
                                    actualValueInscanRequest.setClientCode(docketCheckResponse.getData().get(0).getClient_code().toString());
                                }*/
                                actualValueInscanRequest.setClientCode(docketCheckResponse.getData().get(0).getClient_code().toString());
                                actualValueInscanRequest.setActualVal(String.valueOf(amount));
                                actualValueInscanRequest.setUserId(userId);
                                actualValueInscanRequest.setOrigin(docketCheckResponse.getData().get(0).getOrgPin());
                                actualValueInscanRequest.setDest(docketCheckResponse.getData().get(0).getDestPin());
                                actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                                actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                                mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                                mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);
                            }
                        }
                        pudlocation.setText(docketCheckResponse.getData().get(0).getPudLoc());
                        destcontrolling.setText(docketCheckResponse.getData().get(0).getHubLoc());
                        destcity.setText(docketCheckResponse.getData().get(0).getCityName());

                        tv_noofbox.setVisibility(View.VISIBLE);
                        noofbox.setVisibility(View.VISIBLE);

                        //code changes tart here
                        int noOfPcsFlag = docketCheckResponse.getData().get(0).getFlagNoOfPcs();

                        no_of_box = docketCheckResponse.getData().get(0).getNoOfPcs();
                        noofbox.setText(String.valueOf(no_of_box));

                        if (noOfPcsFlag == 1) {
                            ArrayList list = new ArrayList();
                            List jsonArray = docketCheckResponse.getData().get(0).getTabnoOfPcs();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                list.add(jsonArray.get(i));
                            }
                            noofpcs_spinner.setVisibility(View.VISIBLE);

                            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                            noofpcs_spinner.setAdapter(aa);

                            if (!docketCheckResponse.getData().get(0).getDocketno().equalsIgnoreCase("")) {
                                scannedboxlist.add(docketCheckResponse.getData().get(0).getDocketno());
                                scannedBoxesLBHList.add(new ScannedBox(docketCheckResponse.getData().get(0).getDocketno(), "", "", "", "", "", ""));
                            }


                            noofpcs_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    noofpcs = (String) parent.getItemAtPosition(position);
                                    Log.d("inside", "third party valid cit flag 1" + noofpcs);

                                    rvlayout.setVisibility(View.VISIBLE);

                                    docketdetailslayout.setVisibility(View.VISIBLE);

                                    try {
                                        singleconn.setText(docketCheckResponse.getData().get(0).getDocketno());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    singleconn.setVisibility(View.VISIBLE);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {


                                    rvlayout.setVisibility(View.VISIBLE);

                                    docketdetailslayout.setVisibility(View.VISIBLE);

                                    try {
                                        singleconn.setText(docketCheckResponse.getData().get(0).getDocketno());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    singleconn.setVisibility(View.VISIBLE);
                                }
                            });
                            submit.setVisibility(View.VISIBLE);

                        } else if (noOfPcsFlag == 0) {

                            noofbox.setVisibility(View.VISIBLE);
                            noofbox.setText(docketCheckResponse.getData().get(0).getNoOfPcs().toString());
                            scanbox.setVisibility(View.VISIBLE);
                            mBoxQRScan.setVisibility(View.VISIBLE);
                            noofbox.requestFocus();

                            noofpcs = docketCheckResponse.getData().get(0).getNoOfPcs().toString();

                            Log.d("inside", "third party valid cit flag 0" + noofpcs);
                        }


                        if (docketCheckResponse.getData().get(0).getEwaybill().equalsIgnoreCase("WBDLMH")) {
                            //display input for eway bill no and ewaybillscan
                           /* ewaybilllayout.setVisibility(View.VISIBLE);
                            scanewaybill.setVisibility(View.VISIBLE);
                            mEwayBillQRScan.setVisibility(View.VISIBLE);
                            ewaybilldetails.setVisibility(View.VISIBLE);*/

                            ActualValueInscanRequest actualValueInscanRequest = new ActualValueInscanRequest();
                            actualValueInscanRequest.setAction("inscan_actualvalue_new");
                            actualValueInscanRequest.setClientCode(docketCheckResponse.getData().get(0).getClient_code().toString());
                            actualValueInscanRequest.setActualVal(String.valueOf(amount));
                            actualValueInscanRequest.setUserId(userId);
                            actualValueInscanRequest.setOrigin(docketCheckResponse.getData().get(0).getOrgPin());
                            actualValueInscanRequest.setDest(docketCheckResponse.getData().get(0).getDestPin());
                            actualValueInscanRequest.setLatitude(String.valueOf(latitude));
                            actualValueInscanRequest.setLongitude(String.valueOf(longitude));

                            mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                            mPresenter.actualValueInscanCheck(token, actualValueInscanRequest);
                        } else {
                            ewaybilllayout.setVisibility(View.GONE);
                            scanewaybill.setVisibility(View.GONE);
                            ewaybilldetails.setVisibility(View.GONE);
                            mEwayBillQRScan.setVisibility(View.GONE);
                        }

                    }

                } else {
                    Toasty.warning(InScanActivity.this, docketCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();

                    scan_connote.setVisibility(View.VISIBLE);
                    tv_scanconnote.setVisibility(View.VISIBLE);
                    // conlayout.setVisibility(View.GONE);
                    resultlayout.setVisibility(View.GONE);

                }
            }

        } else {
            scan_connote.setVisibility(View.VISIBLE);
            tv_scanconnote.setVisibility(View.VISIBLE);
            // conlayout.setVisibility(View.GONE);
            resultlayout.setVisibility(View.GONE);
            docketdetailslayout.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            scanbox.setVisibility(View.GONE);
            // mBoxQRScan.setVisibility(View.GONE);
            scanewaybill.setVisibility(View.GONE);
            mEwayBillQRScan.setVisibility(View.GONE);
            Toasty.warning(InScanActivity.this, docketCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();

        }
    }

    @Override
    public void setOriginPincodeResponse(OriginPincodeValidateResponse originPincodeResponse) {

        String originDestination = mCriticalogSharedPreferences.getData("origin_destination");

        if (originDestination.equalsIgnoreCase("origin")) {
            if (originPincodeResponse.getStatus() != 200) {
                Toasty.warning(InScanActivity.this, originPincodeResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                originpin.setOTP("");
            }
        } else {
            if (originPincodeResponse.getStatus() == 200) {
                pudlocation.setText(originPincodeResponse.getData().get(0).getPudLoc());
                destcontrolling.setText(originPincodeResponse.getData().get(0).getHubLoc());
                destcity.setText(originPincodeResponse.getData().get(0).getCityname());
                mControllingHub.setVisibility(View.VISIBLE);
                String controllingHubText = "Hub Code: " + originPincodeResponse.getData().get(0).getControlling_hub().toString() + " - " + "Dest City: " + originPincodeResponse.getData().get(0).getCity_name().toString();
                mControllingHub.setText(Html.fromHtml(controllingHubText));

            } else {
                Toasty.warning(InScanActivity.this, originPincodeResponse.getMessage(), Toast.LENGTH_LONG, true).show();
                destpin.setOTP("");
                mControllingHub.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setActualValueInscanResponse(ActualValueInscanResponse actualValueInscanResponse) {
       /* if (actualValueInscanResponse.getStatus() == 200) {
            //display input for eway bill no and ewaybillscan
            ewaybilllayout.setVisibility(View.VISIBLE);
            scanewaybill.setVisibility(View.VISIBLE);
            mEwayBillQRScan.setVisibility(View.VISIBLE);
        }*/


        if(actualValueInscanResponse.getMessage().equalsIgnoreCase("Eway bill required"))
        {
            ewaybilllayout.setVisibility(View.VISIBLE);
            scanewaybill.setVisibility(View.VISIBLE);
            mEwayBillQRScan.setVisibility(View.VISIBLE);
        }else if(actualValueInscanResponse.getMessage().equalsIgnoreCase("Eway bill not required")){
            ewaybilllayout.setVisibility(View.GONE);
            scanewaybill.setVisibility(View.GONE);
            mEwayBillQRScan.setVisibility(View.GONE);

        }else if(actualValueInscanResponse.getMessage().equalsIgnoreCase("Actual value cannot exceed")){
            actualvalue.setText("");
            actualvalue.setHint("Enter Actual Value");
        }
        Toast.makeText(this, actualValueInscanResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setInscanBoxDataToViews(InScanBoxResponse inScanBoxResponse) {

        if (inScanBoxResponse.getStatus() == 200) {
            scanbox.setVisibility(View.GONE);
            //  mBoxQRScan.setVisibility(View.GONE);
            et_box.setVisibility(View.GONE);
            mBoxDetailsLinLay.setVisibility(View.GONE);
            et_box.setText("");


            if (!scannedboxlist.contains(docket)) {
                scannedboxlist.add(docket);
                scannedBoxesLBHList.add(new ScannedBox(docket, netWeight, length, breadth, height, partCode, partDescr));
                mL.getText().clear();
                mB.getText().clear();
                mH.getText().clear();
                mNetWt.getText().clear();
            }

            rvlayout.setVisibility(View.VISIBLE);

            docketdetailslayout.setVisibility(View.VISIBLE);
            docketrv.setVisibility(View.VISIBLE);
            adapter = new InscanBoxAdapter(scannedboxlist, InScanActivity.this);
            docketrv.setAdapter(adapter);


            if (no_of_box == scannedboxlist.size()) {
                Toasty.success(InScanActivity.this, scannedboxlist.size() + "/" + no_of_box + "scanned", Toast.LENGTH_LONG, true).show();
                submit.setVisibility(View.VISIBLE);
                mBoxQRScan.setVisibility(View.GONE);
                et_box.setVisibility(View.GONE);
                mBoxDetailsLinLay.setVisibility(View.GONE);
            } else {
                Toasty.success(InScanActivity.this, scannedboxlist.size() + "/" + no_of_box + "scanned", Toast.LENGTH_LONG, true).show();

                scan_BoxThroughTvsScanner();


            }
        } else {
            scanbox.setVisibility(View.VISIBLE);
            //  mBoxQRScan.setVisibility(View.VISIBLE);
            Toasty.warning(InScanActivity.this, inScanBoxResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }

    }

    @Override
    public void setInScanSubmitDataToViews(InScanSubmitResponse inScanSubmitResponse) {
        if (inScanSubmitResponse.getStatus() == 200) {
            Toasty.success(InScanActivity.this, inScanSubmitResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            userRiderLog();
            finish();
        } else {
            Toasty.warning(InScanActivity.this, inScanSubmitResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void setEwayBillValidateResponseToViews(EwayBillValidateResponse ewayBillValidateResponse) {

        if (ewayBillValidateResponse.getStatus() == 200) {

            if (scannedeWaybilllist.contains(ewayBillValidateResponse.getData().get(0).getScannedEwaybill())) {
                Toasty.warning(InScanActivity.this, "Eway Bill already present!..enter new one", Toast.LENGTH_LONG, true).show();
            } else {
                scannedeWaybilllist.add(ewayBillValidateResponse.getData().get(0).getScannedEwaybill());
                Toasty.success(InScanActivity.this, "Eway Bill " + String.valueOf(scannedeWaybilllist.size()) + " of " + ewaybill_no + " Scanned", Toast.LENGTH_LONG, true).show();
                mETEwayBill.setText("");
                mTextEwayBillDetails.setVisibility(View.VISIBLE);
                ewaybilldetails.setVisibility(View.VISIBLE);
                adapter = new InscanBoxAdapter(scannedeWaybilllist, InScanActivity.this);
                ewaybillrv.setAdapter(adapter);
                if (scannedeWaybilllist.size() == Integer.parseInt(ewaybill_no)) {
                    mETEwayBill.setVisibility(View.GONE);
                    mEwayBillQRScan.setVisibility(View.GONE);
                    scanewaybill.setVisibility(View.GONE);
                }
            }

        } else {
            Toasty.warning(InScanActivity.this, ewayBillValidateResponse.getMessage(), Toast.LENGTH_LONG, true).show();

        }

    }

    @Override
    public void inspectionDoneResponseToData(InspectionDoneResponse inspectionDoneResponse) {

        if (inspectionDoneResponse.getStatus() == 200) {
            Toasty.success(InScanActivity.this, inspectionDoneResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            dialogCheckListWithFlag.dismiss();
        } else {
            Toasty.warning(InScanActivity.this, inspectionDoneResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }

    }

    @Override
    public void inspectionNegativeStatusData(InscanNegativeStatusResponse inspectionNegativeStatusResponse) {
        if (inspectionNegativeStatusResponse.getStatus() == 200) {

            reasons = inspectionNegativeStatusResponse.getData();

            for (int i = 0; i < reasons.size(); i++) {

                reasonList.add(reasons.get(i).getNegativeDesc());
            }
            showCancelDialog();
        } else {
            Toasty.error(this,inspectionNegativeStatusResponse.getMessage(),Toasty.LENGTH_SHORT,true).show();
            // mProgressBar.dismiss();
        }
    }

    @Override
    public void inspectionNegSubmitDataToViews(InspectionNegSubmitResponse inspectionNegSubmitResponse) {
        if (inspectionNegSubmitResponse.getStatus() == 200) {
            Toasty.success(InScanActivity.this, inspectionNegSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            dialogCheckListWithFlag.dismiss();
            //  mProgressBar.dismiss();
        } else {
            Toasty.success(InScanActivity.this, inspectionNegSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void inscanItemDetailsResponseDataToVews(InscanGetItemDetailsResponse inscanGetItemDetailsResponse) {

        Toast.makeText(this, inscanGetItemDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
        if (inscanGetItemDetailsResponse.getStatus() == 200) {
            mL.setText(inscanGetItemDetailsResponse.getData().getMdimlength());
            mB.setText(inscanGetItemDetailsResponse.getData().getMdimbreadth());
            mH.setText(inscanGetItemDetailsResponse.getData().getMdimheight());
            mNetWt.setText(inscanGetItemDetailsResponse.getData().getXnetwt());
        }else {
            mL.setText("");
            mB.setText("");
            mH.setText("");
            mNetWt.setText("");
        }
    }

    public void showCancelDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(InScanActivity.this)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                        reasonCode = reasons.get(which).getNegativeCode();

                       // Toast.makeText(InScanActivity.this, reasonCode, Toast.LENGTH_SHORT).show();

                        InspectionNegSubmitRequest inspectionNegSubmitRequest = new InspectionNegSubmitRequest();
                        inspectionNegSubmitRequest.setAction("inspection_failed");
                        inspectionNegSubmitRequest.setDocketNo(connoteno.getText().toString());
                        inspectionNegSubmitRequest.setUserId(userId);
                        inspectionNegSubmitRequest.setNegStatusCode(reasonCode);
                        inspectionNegSubmitRequest.setLatitude(String.valueOf(latitude));
                        inspectionNegSubmitRequest.setLongitude(String.valueOf(longitude));

                        mPresenter = new InscanPresenterImpl(InScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());
                        mPresenter.inspectionNegativeSubmit(inspectionNegSubmitRequest);


                        return true; // allow selection
                    }
                })
                .show();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.warning(InScanActivity.this, R.string.re_try, Toast.LENGTH_LONG, true).show();
    }

}

