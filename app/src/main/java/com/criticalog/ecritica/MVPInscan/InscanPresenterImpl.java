package com.criticalog.ecritica.MVPInscan;

import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanRequest;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckRequest;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegativeStatusRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;

public class InscanPresenterImpl implements InscanContract.presenter, InscanContract.DocketCheckIntractor.OnFinishedListener,
        InscanContract.OriginPincodeValidateIntractor.OnFinishedListener, InscanContract.ActualValueInscanIntractor.OnFinishedListener, InscanContract.InScanBoxIntractor.OnFinishedListener, InscanContract.InScanSubmitIntractor.OnFinishedListener, InscanContract.EwayBillValidateIntractor.OnFinishedListener,
        InscanContract.InspectionDoneInteractor.OnFinishedListener, InscanContract.InspectionNegativeStatusInteractor.OnFinishedListener,
        InscanContract.InspectionNegativeSubmitInteractor.OnFinishedListener, InscanContract.InscanItemDetailsIntractor.OnFinishedListener {
    private InscanContract.MainView mainView;
    private InscanContract.DocketCheckIntractor docketCheckIntractor;
    private InscanContract.OriginPincodeValidateIntractor originPincodeValidateIntractor;
    private InscanContract.ActualValueInscanIntractor actualValueInscanIntractor;
    private InscanContract.InScanBoxIntractor inScanBoxIntractor;
    private InscanContract.InScanSubmitIntractor inScanSubmitIntractor;
    private InscanContract.EwayBillValidateIntractor ewayBillValidateIntractor;
    private InscanContract.InspectionDoneInteractor inspectionDoneInteractor;
    private InscanContract.InspectionNegativeStatusInteractor inspectionNegativeStatusInteractor;
    private InscanContract.InspectionNegativeSubmitInteractor inspectionNegativeSubmitInteractor;
    private InscanContract.InscanItemDetailsIntractor inscanItemDetailsIntractor;

    public InscanPresenterImpl(InscanContract.MainView mainView, InscanContract.DocketCheckIntractor docketCheckIntractor,
                               InscanContract.OriginPincodeValidateIntractor originPincodeValidateIntractor,
                               InscanContract.ActualValueInscanIntractor actualValueInscanIntractor,
                               InscanContract.InScanBoxIntractor inScanBoxIntractor,
                               InscanContract.InScanSubmitIntractor inScanSubmitIntractor,
                               InscanContract.EwayBillValidateIntractor ewayBillValidateIntractor,
                               InscanContract.InspectionDoneInteractor inspectionDoneInteractor,
                               InscanContract.InspectionNegativeStatusInteractor inspectionNegativeStatusInteractor,
                               InscanContract.InspectionNegativeSubmitInteractor inspectionNegativeSubmitInteractor,
                               InscanContract.InscanItemDetailsIntractor inscanItemDetailsIntractor) {
        this.mainView = mainView;
        this.docketCheckIntractor = docketCheckIntractor;
        this.originPincodeValidateIntractor=originPincodeValidateIntractor;
        this.actualValueInscanIntractor=actualValueInscanIntractor;
        this.inScanBoxIntractor=inScanBoxIntractor;
        this.inScanSubmitIntractor=inScanSubmitIntractor;
        this.ewayBillValidateIntractor=ewayBillValidateIntractor;
        this.inspectionDoneInteractor=inspectionDoneInteractor;
        this.inspectionNegativeStatusInteractor=inspectionNegativeStatusInteractor;
        this.inspectionNegativeSubmitInteractor=inspectionNegativeSubmitInteractor;
        this.inscanItemDetailsIntractor=inscanItemDetailsIntractor;

    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void inScanDocketCheck(String token, DocketCheckRequest docketCheckRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        docketCheckIntractor.getDocketCheckSuccess(this, token, docketCheckRequest);
    }

    @Override
    public void validateOriginPicode(String token, OriginPincodeValidateRequest originPincodeValidateRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        originPincodeValidateIntractor.getOriginPincodeValidateSuccess(this,token,originPincodeValidateRequest);
    }

    @Override
    public void actualValueInscanCheck(String token, ActualValueInscanRequest actualValueInscanRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }
        actualValueInscanIntractor.getActualValueInscanRequest(this,token,actualValueInscanRequest);
    }

    @Override
    public void inscanBoxNumberValidate(String token, InScanBoxRequest inScanBoxRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        inScanBoxIntractor.getActualValueInscanRequest(this,token,inScanBoxRequest);
    }

    @Override
    public void inScanSubmit(String token, InScanSubmitRequest inScanSubmitRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        inScanSubmitIntractor.getInScanSubmitRequest(this,token,inScanSubmitRequest);
    }

    @Override
    public void ewayBillValidate(EwayBillValidateRequest ewayBillValidateRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        ewayBillValidateIntractor.getEwayBillRequest(this,"",ewayBillValidateRequest);

    }

    @Override
    public void inspectionDoneRequest(InspectionDoneRequest inspectionDoneRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        inspectionDoneInteractor.inspectionDoneRequest((InscanContract.InspectionDoneInteractor.OnFinishedListener) this,"",inspectionDoneRequest);
    }

    @Override
    public void inspectionNegativeStatuse(InspectionNegativeStatusRequest inspectionNegativeStatusRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        inspectionNegativeStatusInteractor.inspectionNegativeStatusRequest(this,"", inspectionNegativeStatusRequest);
    }

    @Override
    public void inspectionNegativeSubmit(InspectionNegSubmitRequest inspectionNegSubmitRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        inspectionNegativeSubmitInteractor.inspectionNegativeStatusRequest(this,"",inspectionNegSubmitRequest);

    }

    @Override
    public void inscanItemsDetaislRequest(InscanGetItemDetailsRequest inscanGetItemDetailsRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();

        }

        inscanItemDetailsIntractor.getDocketCheckSuccess(this,"",inscanGetItemDetailsRequest);

    }

    @Override
    public void onFinished(DocketCheckResponse docketCheckResponse) {
        if(mainView!=null)
        {
            mainView.setDocketCheckDataToViews(docketCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(OriginPincodeValidateResponse originPincodeResponse) {
        if(mainView!=null)
        {
            mainView.setOriginPincodeResponse(originPincodeResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(ActualValueInscanResponse actualValueInscanResponse) {
        if(mainView!=null)
        {
            mainView.setActualValueInscanResponse(actualValueInscanResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InScanBoxResponse inScanBoxResponse) {
        if(mainView!=null)
        {
            mainView.setInscanBoxDataToViews(inScanBoxResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InScanSubmitResponse inScanSubmitResponse) {
        if(mainView!=null)
        {
            mainView.setInScanSubmitDataToViews(inScanSubmitResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(EwayBillValidateResponse ewayBillValidateResponse) {
        if(mainView!=null)
        {
            mainView.setEwayBillValidateResponseToViews(ewayBillValidateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InspectionDoneResponse inspectionDoneResponse) {
        if(mainView!=null)
        {
            mainView.inspectionDoneResponseToData(inspectionDoneResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InscanNegativeStatusResponse inscanNegativeStatusResponse) {
        if(mainView!=null)
        {
            mainView.inspectionNegativeStatusData(inscanNegativeStatusResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InspectionNegSubmitResponse inspectionNegSubmitResponse) {
        if(mainView!=null)
        {
            mainView.inspectionNegSubmitDataToViews(inspectionNegSubmitResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(InscanGetItemDetailsResponse inscanGetItemDetailsResponse) {
        if(mainView!=null)
        {
            mainView.inscanItemDetailsResponseDataToVews(inscanGetItemDetailsResponse);
            mainView.hideProgress();
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
