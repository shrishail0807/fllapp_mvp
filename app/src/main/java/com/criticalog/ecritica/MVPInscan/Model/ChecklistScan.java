package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChecklistScan implements Serializable {
    @SerializedName("Flag")
    @Expose
    private String flag;
    @SerializedName("Rule")
    @Expose
    private String rule;
    public ChecklistScan() {
    }
    public ChecklistScan(String rule, String flag) {
        this.rule = rule;
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }
}
