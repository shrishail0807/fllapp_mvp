package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataPartInscan implements Serializable {
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("MDIMCODE")
    @Expose
    private String mdimcode;
    @SerializedName("MDIMLENGTH")
    @Expose
    private String mdimlength;
    @SerializedName("MDIMBREADTH")
    @Expose
    private String mdimbreadth;
    @SerializedName("MDIMHEIGHT")
    @Expose
    private String mdimheight;
    @SerializedName("MDIMDIVVAL")
    @Expose
    private String mdimdivval;
    @SerializedName("XNETWT")
    @Expose
    private String xnetwt;
    @SerializedName("XVOLWT")
    @Expose
    private String xvolwt;
    @SerializedName("XBOXTYPE")
    @Expose
    private String xboxtype;

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getMdimcode() {
        return mdimcode;
    }

    public void setMdimcode(String mdimcode) {
        this.mdimcode = mdimcode;
    }

    public String getMdimlength() {
        return mdimlength;
    }

    public void setMdimlength(String mdimlength) {
        this.mdimlength = mdimlength;
    }

    public String getMdimbreadth() {
        return mdimbreadth;
    }

    public void setMdimbreadth(String mdimbreadth) {
        this.mdimbreadth = mdimbreadth;
    }

    public String getMdimheight() {
        return mdimheight;
    }

    public void setMdimheight(String mdimheight) {
        this.mdimheight = mdimheight;
    }

    public String getMdimdivval() {
        return mdimdivval;
    }

    public void setMdimdivval(String mdimdivval) {
        this.mdimdivval = mdimdivval;
    }

    public String getXnetwt() {
        return xnetwt;
    }

    public void setXnetwt(String xnetwt) {
        this.xnetwt = xnetwt;
    }

    public String getXvolwt() {
        return xvolwt;
    }

    public void setXvolwt(String xvolwt) {
        this.xvolwt = xvolwt;
    }

    public String getXboxtype() {
        return xboxtype;
    }

    public void setXboxtype(String xboxtype) {
        this.xboxtype = xboxtype;
    }
}
