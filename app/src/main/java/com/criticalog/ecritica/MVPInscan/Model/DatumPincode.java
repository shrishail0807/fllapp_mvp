package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumPincode {
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("hub_loc")
    @Expose
    private String hubLoc;
    @SerializedName("pud_loc")
    @Expose
    private String pudLoc;

    @SerializedName("city_name")
    @Expose
    private String city_name;

    @SerializedName("controlling_hub")
    @Expose
    private String controlling_hub;

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getControlling_hub() {
        return controlling_hub;
    }

    public void setControlling_hub(String controlling_hub) {
        this.controlling_hub = controlling_hub;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getHubLoc() {
        return hubLoc;
    }

    public void setHubLoc(String hubLoc) {
        this.hubLoc = hubLoc;
    }

    public String getPudLoc() {
        return pudLoc;
    }

    public void setPudLoc(String pudLoc) {
        this.pudLoc = pudLoc;
    }
}
