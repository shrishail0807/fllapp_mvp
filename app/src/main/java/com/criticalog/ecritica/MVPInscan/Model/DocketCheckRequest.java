package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DocketCheckRequest implements Serializable {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("pickupby")
    @Expose
    private String pickupby;
    @SerializedName("connote")
    @Expose
    private String connote;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPickupby() {
        return pickupby;
    }

    public void setPickupby(String pickupby) {
        this.pickupby = pickupby;
    }

    public String getConnote() {
        return connote;
    }

    public void setConnote(String connote) {
        this.connote = connote;
    }

}
