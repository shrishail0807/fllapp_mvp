package com.criticalog.ecritica.MVPInscan;

import android.util.Log;

import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanRequest;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckRequest;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegativeStatusRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InscanInteractorImpl implements InscanContract.DocketCheckIntractor, InscanContract.OriginPincodeValidateIntractor, InscanContract.ActualValueInscanIntractor, InscanContract.InScanBoxIntractor,
        InscanContract.InScanSubmitIntractor, InscanContract.EwayBillValidateIntractor, InscanContract.InspectionDoneInteractor, InscanContract.InspectionNegativeStatusInteractor, InscanContract.InspectionNegativeSubmitInteractor,
        InscanContract.InscanItemDetailsIntractor{
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void getDocketCheckSuccess(InscanContract.DocketCheckIntractor.OnFinishedListener onFinishedListener, String token, DocketCheckRequest docketCheckRequest) {
        Call<DocketCheckResponse> docketCheckResponseCall = services.inscanDocketCheck(docketCheckRequest);
        docketCheckResponseCall.enqueue(new Callback<DocketCheckResponse>() {
            @Override
            public void onResponse(Call<DocketCheckResponse> call, Response<DocketCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DocketCheckResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }


    @Override
    public void getOriginPincodeValidateSuccess(InscanContract.OriginPincodeValidateIntractor.OnFinishedListener onFinishedListener, String token, OriginPincodeValidateRequest originPincodeValidateRequest) {
        Call<OriginPincodeValidateResponse> originPincodeValidateResponseCall = services.originPincodeValidate(originPincodeValidateRequest);

        originPincodeValidateResponseCall.enqueue(new Callback<OriginPincodeValidateResponse>() {
            @Override
            public void onResponse(Call<OriginPincodeValidateResponse> call, Response<OriginPincodeValidateResponse> response) {
                 Log.e("ORIGIN",response.body().getCode().toString());
                onFinishedListener.onFinished(response.body());

            }

            @Override
            public void onFailure(Call<OriginPincodeValidateResponse> call, Throwable t) {
                Log.e("ORIGIN",t.toString());
                onFinishedListener.onFailure(t);

            }
        });
    }

    @Override
    public void getActualValueInscanRequest(InscanContract.ActualValueInscanIntractor.OnFinishedListener onFinishedListener, String token, ActualValueInscanRequest actualValueInscanRequest) {

        Call<ActualValueInscanResponse> actualValueInscanResponseCall=services.actualValueInscan(actualValueInscanRequest);
        actualValueInscanResponseCall.enqueue(new Callback<ActualValueInscanResponse>() {
            @Override
            public void onResponse(Call<ActualValueInscanResponse> call, Response<ActualValueInscanResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ActualValueInscanResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getActualValueInscanRequest(InscanContract.InScanBoxIntractor.OnFinishedListener onFinishedListener, String token, InScanBoxRequest inScanBoxRequest) {

        Call<InScanBoxResponse> inScanBoxResponseCall=services.inScanBox(inScanBoxRequest);
        inScanBoxResponseCall.enqueue(new Callback<InScanBoxResponse>() {
            @Override
            public void onResponse(Call<InScanBoxResponse> call, Response<InScanBoxResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InScanBoxResponse> call, Throwable t) {
              onFinishedListener.onFailure(t);
            }
        });
    }


    @Override
    public void getInScanSubmitRequest(InscanContract.InScanSubmitIntractor.OnFinishedListener onFinishedListener, String token, InScanSubmitRequest inScanSubmitRequest) {

        Call<InScanSubmitResponse> inScanSubmitResponseCall=services.inScanSubmit(inScanSubmitRequest);
        inScanSubmitResponseCall.enqueue(new Callback<InScanSubmitResponse>() {
            @Override
            public void onResponse(Call<InScanSubmitResponse> call, Response<InScanSubmitResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InScanSubmitResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getEwayBillRequest(InscanContract.EwayBillValidateIntractor.OnFinishedListener onFinishedListener, String token, EwayBillValidateRequest ewayBillValidateRequest) {
        Call<EwayBillValidateResponse> ewayBillValidateResponseCall=services.ewayBillValidate(ewayBillValidateRequest);
        ewayBillValidateResponseCall.enqueue(new Callback<EwayBillValidateResponse>() {
            @Override
            public void onResponse(Call<EwayBillValidateResponse> call, Response<EwayBillValidateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<EwayBillValidateResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void inspectionDoneRequest(InscanContract.InspectionDoneInteractor.OnFinishedListener onFinishedListener, String token, InspectionDoneRequest inspectionDoneRequest) {

        Call<InspectionDoneResponse> inspectionDoneResponseCall=services.inspectionDone(inspectionDoneRequest);
        inspectionDoneResponseCall.enqueue(new Callback<InspectionDoneResponse>() {
            @Override
            public void onResponse(Call<InspectionDoneResponse> call, Response<InspectionDoneResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InspectionDoneResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });


    }

    @Override
    public void inspectionNegativeStatusRequest(InscanContract.InspectionNegativeStatusInteractor.OnFinishedListener onFinishedListener, String token, InspectionNegativeStatusRequest inspectionNegativeStatusRequest) {

        Call<InscanNegativeStatusResponse> inscanNegativeStatusResponseCall=services.inspectionNegativeStatus(inspectionNegativeStatusRequest);

        inscanNegativeStatusResponseCall.enqueue(new Callback<InscanNegativeStatusResponse>() {
            @Override
            public void onResponse(Call<InscanNegativeStatusResponse> call, Response<InscanNegativeStatusResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InscanNegativeStatusResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void inspectionNegativeStatusRequest(InscanContract.InspectionNegativeSubmitInteractor.OnFinishedListener onFinishedListener, String token, InspectionNegSubmitRequest inspectionNegSubmitRequest) {

        Call<InspectionNegSubmitResponse> inspectionNegSubmitResponseCall=services.inspectionNegativeSubmit(inspectionNegSubmitRequest);

        inspectionNegSubmitResponseCall.enqueue(new Callback<InspectionNegSubmitResponse>() {
            @Override
            public void onResponse(Call<InspectionNegSubmitResponse> call, Response<InspectionNegSubmitResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InspectionNegSubmitResponse> call, Throwable t) {
             onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getDocketCheckSuccess(InscanContract.InscanItemDetailsIntractor.OnFinishedListener onFinishedListener, String token, InscanGetItemDetailsRequest inscanGetItemDetailsRequest) {
        Call<InscanGetItemDetailsResponse> inscanGetItemDetailsResponseCall=services.inscanGetItemDetails(inscanGetItemDetailsRequest);

        inscanGetItemDetailsResponseCall.enqueue(new Callback<InscanGetItemDetailsResponse>() {
            @Override
            public void onResponse(Call<InscanGetItemDetailsResponse> call, Response<InscanGetItemDetailsResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<InscanGetItemDetailsResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
