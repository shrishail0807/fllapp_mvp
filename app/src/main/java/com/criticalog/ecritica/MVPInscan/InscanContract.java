package com.criticalog.ecritica.MVPInscan;

import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanRequest;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckRequest;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegativeStatusRequest;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneRequest;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;

public interface InscanContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void inScanDocketCheck(String token, DocketCheckRequest docketCheckRequest);

        void validateOriginPicode(String token, OriginPincodeValidateRequest originPincodeValidateRequest);

        void actualValueInscanCheck(String token, ActualValueInscanRequest actualValueInscanRequest);

        void inscanBoxNumberValidate(String token, InScanBoxRequest inScanBoxRequest);

        void inScanSubmit(String token, InScanSubmitRequest inScanSubmitRequest);

        void ewayBillValidate(EwayBillValidateRequest ewayBillValidateRequest);

        void inspectionDoneRequest(InspectionDoneRequest inspectionDoneRequest);

        void inspectionNegativeStatuse(InspectionNegativeStatusRequest inspectionNegativeStatusRequest);

        void inspectionNegativeSubmit(InspectionNegSubmitRequest inspectionNegSubmitRequest);

        void inscanItemsDetaislRequest(InscanGetItemDetailsRequest inscanGetItemDetailsRequest);
    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDocketCheckDataToViews(DocketCheckResponse docketCheckResponse);

        void setOriginPincodeResponse(OriginPincodeValidateResponse originPincodeResponse);

        void setActualValueInscanResponse(ActualValueInscanResponse actualValueInscanResponse);

        void setInscanBoxDataToViews(InScanBoxResponse inScanBoxResponse);

        void setInScanSubmitDataToViews(InScanSubmitResponse inScanSubmitResponse);

        void setEwayBillValidateResponseToViews(EwayBillValidateResponse ewayBillValidateResponse);

        void inspectionDoneResponseToData(InspectionDoneResponse inspectionDoneResponse);

        void inspectionNegativeStatusData(InscanNegativeStatusResponse inspectionNegativeStatusResponse);

        void inspectionNegSubmitDataToViews(InspectionNegSubmitResponse inspectionNegSubmitResponse);

        void inscanItemDetailsResponseDataToVews(InscanGetItemDetailsResponse inscanGetItemDetailsResponse);

        void onResponseFailure(Throwable throwable);
    }

    interface InscanItemDetailsIntractor {

        interface OnFinishedListener {
            void onFinished(InscanGetItemDetailsResponse inscanGetItemDetailsResponse);

            void onFailure(Throwable t);
        }

        void getDocketCheckSuccess(InscanContract.InscanItemDetailsIntractor.OnFinishedListener onFinishedListener, String token, InscanGetItemDetailsRequest inscanGetItemDetailsRequest);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface DocketCheckIntractor {

        interface OnFinishedListener {
            void onFinished(DocketCheckResponse docketCheckResponse);

            void onFailure(Throwable t);
        }

        void getDocketCheckSuccess(InscanContract.DocketCheckIntractor.OnFinishedListener onFinishedListener, String token, DocketCheckRequest docketCheckRequest);
    }

    interface OriginPincodeValidateIntractor {

        interface OnFinishedListener {
            void onFinished(OriginPincodeValidateResponse originPincodeResponse);

            void onFailure(Throwable t);
        }

        void getOriginPincodeValidateSuccess(InscanContract.OriginPincodeValidateIntractor.OnFinishedListener onFinishedListener, String token, OriginPincodeValidateRequest originPincodeValidateRequest);
    }

    interface ActualValueInscanIntractor {

        interface OnFinishedListener {
            void onFinished(ActualValueInscanResponse actualValueInscanResponse);

            void onFailure(Throwable t);
        }

        void getActualValueInscanRequest(InscanContract.ActualValueInscanIntractor.OnFinishedListener onFinishedListener, String token, ActualValueInscanRequest actualValueInscanRequest);
    }

    interface InScanBoxIntractor {

        interface OnFinishedListener {
            void onFinished(InScanBoxResponse inScanBoxResponse);

            void onFailure(Throwable t);
        }

        void getActualValueInscanRequest(InscanContract.InScanBoxIntractor.OnFinishedListener onFinishedListener, String token, InScanBoxRequest inScanBoxRequest);
    }

    interface InScanSubmitIntractor {

        interface OnFinishedListener {
            void onFinished(InScanSubmitResponse inScanSubmitResponse);

            void onFailure(Throwable t);
        }

        void getInScanSubmitRequest(InscanContract.InScanSubmitIntractor.OnFinishedListener onFinishedListener, String token, InScanSubmitRequest inScanSubmitRequest);
    }

    interface EwayBillValidateIntractor {

        interface OnFinishedListener {
            void onFinished(EwayBillValidateResponse ewayBillValidateResponse);

            void onFailure(Throwable t);
        }

        void getEwayBillRequest(InscanContract.EwayBillValidateIntractor.OnFinishedListener onFinishedListener, String token, EwayBillValidateRequest ewayBillValidateRequest);
    }

    interface  InspectionDoneInteractor{
        interface OnFinishedListener {
            void onFinished(InspectionDoneResponse inspectionDoneResponse);

            void onFailure(Throwable t);
        }

        void inspectionDoneRequest(InscanContract.InspectionDoneInteractor.OnFinishedListener onFinishedListener, String token, InspectionDoneRequest inspectionDoneRequest);
    }

    interface  InspectionNegativeStatusInteractor{
        interface OnFinishedListener {
            void onFinished(InscanNegativeStatusResponse inscanNegativeStatusResponse);

            void onFailure(Throwable t);
        }

        void inspectionNegativeStatusRequest(InscanContract.InspectionNegativeStatusInteractor.OnFinishedListener onFinishedListener, String token, InspectionNegativeStatusRequest inspectionNegativeStatusRequest);
    }

    interface  InspectionNegativeSubmitInteractor{
        interface OnFinishedListener {
            void onFinished(InspectionNegSubmitResponse inspectionNegSubmitResponse);

            void onFailure(Throwable t);
        }

        void inspectionNegativeStatusRequest(InscanContract.InspectionNegativeSubmitInteractor.OnFinishedListener onFinishedListener, String token, InspectionNegSubmitRequest inspectionNegSubmitRequest);
    }
}
