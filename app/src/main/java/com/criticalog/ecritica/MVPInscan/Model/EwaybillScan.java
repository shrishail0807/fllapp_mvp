package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EwaybillScan implements Serializable {
    @SerializedName("XWAYBILLNO")
    @Expose
    private String xwaybillno;

    public String getXwaybillno() {
        return xwaybillno;
    }

    public void setXwaybillno(String xwaybillno) {
        this.xwaybillno = xwaybillno;
    }
}
