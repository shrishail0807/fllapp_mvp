package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScannedBox {
    @SerializedName("box_num")
    @Expose
    private String boxNum;
    @SerializedName("box_weight")
    @Expose
    private String nWt;

    public ScannedBox(String boxNum, String nWt, String l, String b, String h, String part_code, String part_desc) {
        this.boxNum = boxNum;
        this.nWt = nWt;
        this.l = l;
        this.b = b;
        this.h = h;
        this.part_code = part_code;
        this.part_desc = part_desc;
    }

    @SerializedName("box_length")
    @Expose
    private String l;
    @SerializedName("box_breadth")
    @Expose
    private String b;
    @SerializedName("box_height")
    @Expose
    private String h;

    @SerializedName("part_code")
    @Expose
    private String part_code;

    @SerializedName("part_desc")
    @Expose
    private String part_desc;

    public String getPart_code() {
        return part_code;
    }

    public void setPart_code(String part_code) {
        this.part_code = part_code;
    }

    public String getPart_desc() {
        return part_desc;
    }

    public void setPart_desc(String part_desc) {
        this.part_desc = part_desc;
    }

    public String getnWt() {
        return nWt;
    }

    public void setnWt(String nWt) {
        this.nWt = nWt;
    }


    public String getBoxNum() {
        return boxNum;
    }

    public void setBoxNum(String boxNum) {
        this.boxNum = boxNum;
    }

    public String getNWt() {
        return nWt;
    }

    public void setNWt(String nWt) {
        this.nWt = nWt;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }
}
