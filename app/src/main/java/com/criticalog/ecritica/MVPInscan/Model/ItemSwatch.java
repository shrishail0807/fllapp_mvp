package com.criticalog.ecritica.MVPInscan.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemSwatch implements Serializable {
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("part_desc")
    @Expose
    private String partDesc;
    @SerializedName("part_code")
    @Expose
    private String partCode;

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPartDesc() {
        return partDesc;
    }

    public void setPartDesc(String partDesc) {
        this.partDesc = partDesc;
    }

    public String getPartCode() {
        return partCode;
    }

    public void setPartCode(String partCode) {
        this.partCode = partCode;
    }

}
