package com.criticalog.ecritica.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import com.criticalog.ecritica.services.Locationservice;

public class Restartservice extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Background Service.", Toast.LENGTH_LONG).show();

        context.startService(new Intent(context, Locationservice.class));

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, Locationservice.class));
        } else {
            context.startService(new Intent(context, Locationservice.class));
        }*/
    }
}
