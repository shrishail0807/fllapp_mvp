package com.criticalog.ecritica.recievers;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.criticalog.ecritica.Utils.HourBasedLatLongReciever;
import com.criticalog.ecritica.Utils.MyLocationService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import static com.criticalog.ecritica.Utils.HourBasedLatLongReciever.ACTION_PROCESS_START;

public class GeofenceBroadcastReceiver extends BroadcastReceiver {
    Context mcontext;
    LocationRequest locationRequest_hour;
    FusedLocationProviderClient fusedLocationProviderClient_hour;
    // ...
    public void onReceive(Context context, Intent intent) {
        mcontext = context;

        Log.d("inside","geofencesbroadcastreciever");

        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if (event.hasError()) {
            Log.e("Broadcastreciever", "GeofencingEvent Error: " + event.getErrorCode());
            return;
        }

        int geofenceTransition = event.getGeofenceTransition();
        String message = "" ;
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER){
            Log.d("Entered", "Entered the Location");
            message = "Entered the Location";
            updatelocation_Hour();
            Toast.makeText(context, "Entered the Location",Toast.LENGTH_LONG).show();
        }
        else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            message = "Exited the Location";
            Log.d("Exited", "Exited the Location");

        } else {
            Log.d("Error", "Error");
        }

        Log.d("inside","on recieve");







    }


    private void updatelocation_Hour() {
        buildlocationrequest_Hour();
        fusedLocationProviderClient_hour = LocationServices.getFusedLocationProviderClient(mcontext);
        if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient_hour.requestLocationUpdates(locationRequest_hour, getPendingIntentHour());




    }

    private void buildlocationrequest_Hour() {
        locationRequest_hour = new LocationRequest();
            locationRequest_hour.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
      /*  locationRequest_hour.setInterval(1800000);
        locationRequest_hour.setFastestInterval(3600000);*/
        locationRequest_hour.setInterval(60*1000);//10 min
        locationRequest_hour.setFastestInterval(60*1000); //5 min
        locationRequest_hour.setSmallestDisplacement(0f);
    }

    private PendingIntent getPendingIntentHour() {
       /* Intent intent = new Intent(mcontext, HourBasedLatLongReciever.class);
        intent.setAction(ACTION_PROCESS_START);
        return PendingIntent.getBroadcast(mcontext,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);*/

        Intent intent = new Intent(mcontext, MyLocationService.class);
        intent.setAction(MyLocationService.ACTION_PROCESS_UPDATE);

        return PendingIntent.getBroadcast(mcontext,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

    }



    private static String getGeofenceTransitionDetails(GeofencingEvent event) {
        String transitionString =
                GeofenceStatusCodes.getStatusCodeString(event.getGeofenceTransition());
        List triggeringIDs = new ArrayList();
        for (Geofence geofence : event.getTriggeringGeofences()) {
            triggeringIDs.add(geofence.getRequestId());
        }
        return String.format("%s: %s", transitionString, TextUtils.join(", ", triggeringIDs));
    }
}
