package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPunchStatus {
    @SerializedName("is_punched_in")
    @Expose
    private Boolean isPunchedIn;

    @SerializedName("data")
    @Expose
    private List<DatumPunchDetails> data = null;
    @SerializedName("data_count")
    @Expose
    private Integer dataCount;
    @SerializedName("fromMemcache")
    @Expose
    private Integer fromMemcache;

    public List<DatumPunchDetails> getData() {
        return data;
    }

    public void setData(List<DatumPunchDetails> data) {
        this.data = data;
    }

    public Integer getDataCount() {
        return dataCount;
    }

    public void setDataCount(Integer dataCount) {
        this.dataCount = dataCount;
    }

    public Integer getFromMemcache() {
        return fromMemcache;
    }

    public void setFromMemcache(Integer fromMemcache) {
        this.fromMemcache = fromMemcache;
    }

    public Boolean getIsPunchedIn() {
        return isPunchedIn;
    }

    public void setIsPunchedIn(Boolean isPunchedIn) {
        this.isPunchedIn = isPunchedIn;
    }
}
