package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("total_distance")
    @Expose
    private String totalDistance;

    public String getDistance_g() {
        return distance_g;
    }

    public void setDistance_g(String distance_g) {
        this.distance_g = distance_g;
    }

    @SerializedName("distance_g")
    @Expose
    private String distance_g;
    @SerializedName("round")
    @Expose
    private String round;
    @SerializedName("hub_id")
    @Expose
    private String hubId;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("end_time")
    @Expose
    private String endTime;

    @SerializedName("milomtr_start")
    @Expose
    private String milomtrstart;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("milomtr_stop")
    @Expose
    private String milomtrstop;

    public String getMilomtrstart() {
        return milomtrstart;
    }

    public void setMilomtrstart(String milomtrstart) {
        this.milomtrstart = milomtrstart;
    }

    public String getMilomtrstop() {
        return milomtrstop;
    }

    public void setMilomtrstop(String milomtrstop) {
        this.milomtrstop = milomtrstop;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
