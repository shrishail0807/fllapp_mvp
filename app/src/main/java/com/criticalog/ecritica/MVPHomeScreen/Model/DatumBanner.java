package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumBanner {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("for_page")
    @Expose
    private String forPage;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getForPage() {
        return forPage;
    }

    public void setForPage(String forPage) {
        this.forPage = forPage;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
