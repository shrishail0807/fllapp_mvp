package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {
    @SerializedName("start_lat")
    @Expose
    private Double startLat;
    @SerializedName("start_long")
    @Expose
    private Double startLong;
    @SerializedName("end_lat")
    @Expose
    private Double endLat;
    @SerializedName("end_long")
    @Expose
    private Double endLong;
    @SerializedName("distance")
    @Expose
    private String distance;

    public String getDistance_g() {
        return distance_g;
    }

    public void setDistance_g(String distance_g) {
        this.distance_g = distance_g;
    }

    @SerializedName("distance_g")
    @Expose
    private String distance_g;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    public Detail(Double endLat, Double endLong, Double start_lat, Double start_long, String s, String date, String time, String s1, String s2, String closetrip, String toString, int i)
    {
        this.startLat = startLat;
        this.startLong = startLong;
        this.endLat = endLat;
        this.endLong = endLong;
        this.distance = distance;
        this.date = date;
        this.time = time;
        this.remark = remark;
        this.taskid = taskid;
        this.tasktype = tasktype;
        this.statuscode = statuscode;
    }

    public Detail()
    {

    }

    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("taskid")
    @Expose
    private String taskid;
    @SerializedName("tasktype")
    @Expose
    private String tasktype;
    @SerializedName("statuscode")
    @Expose
    private Integer statuscode;

    public Double getStartLat() {
        return startLat;
    }

    public void setStartLat(Double startLat) {
        this.startLat = startLat;
    }

    public Double getStartLong() {
        return startLong;
    }

    public void setStartLong(Double startLong) {
        this.startLong = startLong;
    }

    public Double getEndLat() {
        return endLat;
    }

    public void setEndLat(Double endLat) {
        this.endLat = endLat;
    }

    public Double getEndLong() {
        return endLong;
    }

    public void setEndLong(Double endLong) {
        this.endLong = endLong;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getTasktype() {
        return tasktype;
    }

    public void setTasktype(String tasktype) {
        this.tasktype = tasktype;
    }

    public Integer getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(Integer statuscode) {
        this.statuscode = statuscode;
    }
}
