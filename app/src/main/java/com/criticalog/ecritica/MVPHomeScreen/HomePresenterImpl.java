package com.criticalog.ecritica.MVPHomeScreen;
import static com.criticalog.ecritica.MVPHomeScreen.HomeContract.*;

import com.criticalog.ecritica.MVPHomeScreen.Model.BannerRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripResponse;

import java.util.ArrayList;

public class HomePresenterImpl implements presenter, GetHomeIntractor.OnFinishedListener, RiderLogIntractor.OnFinishedListener, TripIntractor.OnFinishedListener,BannerCheckIntractor.OnFinishedListener,
        MilometerUdateIntractor.OnFinishedListener, PunchStatusInteractor.OnFinishedListener, StartStopRideIntractor.OnFinishedListener {

    private MainView mainView;
    private GetHomeIntractor HomeIntractor;
    private RiderLogIntractor riderLogIntractor;
    private TripIntractor tripIntractor;
    private StartStopRideIntractor startStopRideIntractor;
    private BannerCheckIntractor bannerCheckIntractor;
    private MilometerUdateIntractor milometerUdateIntractor;
    private PunchStatusInteractor punchStatusInteractor;
    public HomePresenterImpl(HomeActivity homeActivity, GetHomeInteractorImpl getHomeInteractor,
                             RiderLogIntractor riderLogIntractor,
                             TripIntractor tripIntractor,
                             StartStopRideIntractor startStopRideIntractor,
                             BannerCheckIntractor bannerCheckIntractor,
                             MilometerUdateIntractor milometerUdateIntractor,
                             PunchStatusInteractor punchStatusInteractor) {
        this.mainView=homeActivity;
        this.HomeIntractor=getHomeInteractor;
        this.riderLogIntractor=riderLogIntractor;
        this.tripIntractor=tripIntractor;
        this.startStopRideIntractor=startStopRideIntractor;
        this.bannerCheckIntractor=bannerCheckIntractor;
        this.milometerUdateIntractor=milometerUdateIntractor;
        this.punchStatusInteractor=punchStatusInteractor;
    }

    @Override
    public void onDestroy() {

        if(mainView!=null)
        {
            mainView=null;
        }
    }

    @Override
    public void onLoadOfHomeScreenOptions(ArrayList<String> homeOptions) {
        HomeIntractor.homeScreenLoaded(this,homeOptions);
    }

    @Override
    public void riderLogRequest(String token, RiderLogRequest riderLogRequest) {

        if(mainView!=null)
        {
            mainView.showProgress();
        }
        riderLogIntractor.riderLogRequest(this,token,riderLogRequest);
    }

    @Override
    public void tripReuqest(String token, TripRequest tripRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        tripIntractor.tripRequest(this,token,tripRequest);
    }

    @Override
    public void startAndStopRide(String token, StartStopRideRequest startStopRideRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        startStopRideIntractor.startStopRideRequest(this,token,startStopRideRequest);

    }

    @Override
    public void bannerCheck(String token, BannerRequest bannerRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        bannerCheckIntractor.bannerRequest(this,token,bannerRequest);
    }

    @Override
    public void milometerUpdateRequest(MilometerUpdateRequest milometerUpdateRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        milometerUdateIntractor.milometerUpdateRequest(this,"",milometerUpdateRequest);
    }

    @Override
    public void punchStatusRequest(PunchStatusRequest punchStatusRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        punchStatusInteractor.punchStatusRequest(this,"",punchStatusRequest);
    }

    @Override
    public void onFinished(ArrayList<String> homeOptions) {
        mainView.setDataToViews(homeOptions);
    }

    @Override
    public void onFinished(RiderLogResponse riderLogResponse) {
        if(mainView!=null)
        {
            mainView.setRiderLogDataToViews(riderLogResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(TripResponse tripResponse) {
        if(mainView!=null)
        {
            mainView.setTripResponseToViews(tripResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(StartStopRideResponse startStopRideResponse) {
        if(mainView!=null)
        {
            mainView.setStartStopRideDataToViews(startStopRideResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(BannerResponse bannerResponse) {
        if(mainView!=null)
        {
            mainView.bannerResponseData(bannerResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(MilometerUpdateResponse milometerUpdateResponse) {
        if(mainView!=null)
        {
            mainView.milometerDataSetToViews(milometerUpdateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(PunchStatusResponse punchStatusResponse) {
        if(mainView!=null)
        {
            mainView.punchStatusDataToViews(punchStatusResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView!=null)
        {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
