package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PunchStatusRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("app_ver_name")
    @Expose
    private String app_ver_name;

    @SerializedName("app_ver_testing")
    @Expose
    private String app_ver_testing;

    public String getApp_ver_testing() {
        return app_ver_testing;
    }

    public void setApp_ver_testing(String app_ver_testing) {
        this.app_ver_testing = app_ver_testing;
    }

    public String getApp_ver_name() {
        return app_ver_name;
    }

    public void setApp_ver_name(String app_ver_name) {
        this.app_ver_name = app_ver_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
