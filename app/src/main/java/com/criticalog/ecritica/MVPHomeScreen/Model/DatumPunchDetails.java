package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DatumPunchDetails implements Serializable {
    @SerializedName("details")
    @Expose
    private List<Details> details = null;
    @SerializedName("is_punched_in")
    @Expose
    private Boolean isPunchedIn;

    @SerializedName("latest_app_version")
    @Expose
    private String latest_app_version;

    @SerializedName("image_vision")
    @Expose
    private String image_vision;

    public Boolean getPunchedIn() {
        return isPunchedIn;
    }

    public void setPunchedIn(Boolean punchedIn) {
        isPunchedIn = punchedIn;
    }

    public String getLatest_app_version() {
        return latest_app_version;
    }

    public void setLatest_app_version(String latest_app_version) {
        this.latest_app_version = latest_app_version;
    }



    public String getImage_vision() {
        return image_vision;
    }

    public void setImage_vision(String image_vision) {
        this.image_vision = image_vision;
    }





    public List<Details> getDetails() {
        return details;
    }

    public void setDetails(List<Details> details) {
        this.details = details;
    }

    public Boolean getIsPunchedIn() {
        return isPunchedIn;
    }

    public void setIsPunchedIn(Boolean isPunchedIn) {
        this.isPunchedIn = isPunchedIn;
    }

  /*  @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("lat_in")
    @Expose
    private String latIn;
    @SerializedName("lng_in")
    @Expose
    private String lngIn;
    @SerializedName("address_in")
    @Expose
    private String addressIn;
    @SerializedName("date_punch_in")
    @Expose
    private String datePunchIn;
    @SerializedName("lat_out")
    @Expose
    private String latOut;
    @SerializedName("lng_out")
    @Expose
    private String lngOut;
    @SerializedName("address_out")
    @Expose
    private String addressOut;
    @SerializedName("date_punch_out")
    @Expose
    private String datePunchOut;
    @SerializedName("spent_duration")
    @Expose
    private String spentDuration;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatIn() {
        return latIn;
    }

    public void setLatIn(String latIn) {
        this.latIn = latIn;
    }

    public String getLngIn() {
        return lngIn;
    }

    public void setLngIn(String lngIn) {
        this.lngIn = lngIn;
    }

    public String getAddressIn() {
        return addressIn;
    }

    public void setAddressIn(String addressIn) {
        this.addressIn = addressIn;
    }

    public String getDatePunchIn() {
        return datePunchIn;
    }

    public void setDatePunchIn(String datePunchIn) {
        this.datePunchIn = datePunchIn;
    }

    public String getLatOut() {
        return latOut;
    }

    public void setLatOut(String latOut) {
        this.latOut = latOut;
    }

    public String getLngOut() {
        return lngOut;
    }

    public void setLngOut(String lngOut) {
        this.lngOut = lngOut;
    }

    public String getAddressOut() {
        return addressOut;
    }

    public void setAddressOut(String addressOut) {
        this.addressOut = addressOut;
    }

    public String getDatePunchOut() {
        return datePunchOut;
    }

    public void setDatePunchOut(String datePunchOut) {
        this.datePunchOut = datePunchOut;
    }

    public String getSpentDuration() {
        return spentDuration;
    }

    public void setSpentDuration(String spentDuration) {
        this.spentDuration = spentDuration;
    }
*/
}
