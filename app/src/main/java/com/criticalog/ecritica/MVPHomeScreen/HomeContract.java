package com.criticalog.ecritica.MVPHomeScreen;

import com.criticalog.ecritica.MVPHomeScreen.Model.BannerRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.LinehaulReceivePostData;

import java.util.ArrayList;

public interface HomeContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void onLoadOfHomeScreenOptions(ArrayList<String> homeOptions);

        void riderLogRequest(String token, RiderLogRequest riderLogRequest);

        void tripReuqest(String token, TripRequest tripRequest);

        void startAndStopRide(String token, StartStopRideRequest startStopRideRequest);

        void bannerCheck(String token, BannerRequest bannerRequest);

        void milometerUpdateRequest(MilometerUpdateRequest milometerUpdateRequest);

        void punchStatusRequest(PunchStatusRequest punchStatusRequest);
    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToViews(ArrayList<String> homeOptions);

        void setRiderLogDataToViews(RiderLogResponse riderLogResponse);

        void setTripResponseToViews(TripResponse tripResponse);

        void setStartStopRideDataToViews(StartStopRideResponse startStopRideResponse);

        void bannerResponseData(BannerResponse bannerResponse);

        void milometerDataSetToViews(MilometerUpdateResponse milometerUpdateResponse);

        void punchStatusDataToViews(PunchStatusResponse punchStatusResponse);

        void onResponseFailure(Throwable throwable);


    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetHomeIntractor {

        interface OnFinishedListener {
            void onFinished(ArrayList<String> homeOptions);

            void onFailure(Throwable t);
        }

        void homeScreenLoaded(HomeContract.GetHomeIntractor.OnFinishedListener onFinishedListener, ArrayList<String> homeOptions);
    }

    interface RiderLogIntractor {

        interface OnFinishedListener {
            void onFinished(RiderLogResponse riderLogResponse);

            void onFailure(Throwable t);
        }

        void riderLogRequest(HomeContract.RiderLogIntractor.OnFinishedListener onFinishedListener, String token, RiderLogRequest riderLogRequest);
    }

    interface TripIntractor {

        interface OnFinishedListener {
            void onFinished(TripResponse tripResponse);

            void onFailure(Throwable t);
        }

        void tripRequest(HomeContract.TripIntractor.OnFinishedListener onFinishedListener, String token, TripRequest tripRequest);
    }

    interface StartStopRideIntractor {

        interface OnFinishedListener {
            void onFinished(StartStopRideResponse startStopRideResponse);

            void onFailure(Throwable t);
        }

        void startStopRideRequest(HomeContract.StartStopRideIntractor.OnFinishedListener onFinishedListener, String token, StartStopRideRequest startStopRideRequest);
    }

    interface BannerCheckIntractor {

        interface OnFinishedListener {
            void onFinished(BannerResponse bannerResponse);

            void onFailure(Throwable t);
        }

        void bannerRequest(HomeContract.BannerCheckIntractor.OnFinishedListener onFinishedListener, String token, BannerRequest bannerRequest);
    }

    interface MilometerUdateIntractor {

        interface OnFinishedListener {
            void onFinished(MilometerUpdateResponse milometerUpdateResponse);

            void onFailure(Throwable t);
        }

        void milometerUpdateRequest(HomeContract.MilometerUdateIntractor.OnFinishedListener onFinishedListener, String token, MilometerUpdateRequest milometerUpdateRequest);
    }
    interface PunchStatusInteractor {

        interface OnFinishedListener {
            void onFinished(PunchStatusResponse punchStatusResponse);

            void onFailure(Throwable t);
        }

        void punchStatusRequest(HomeContract.PunchStatusInteractor.OnFinishedListener onFinishedListener, String token, PunchStatusRequest punchStatusRequest);
    }
    /*interface PunchStatusInteractor {

        interface OnFinishedListener {
            void onFinished(PunchStatusResponse punchStatusResponse);

            void onFailure(Throwable t);
        }

        void punchStatusRequest(HomeContract.PunchStatusInteractor.OnFinishedListener onFinishedListener, String token, PunchStatusRequest punchStatusRequest);
    }*/
}
