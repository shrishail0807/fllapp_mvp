package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataStartStop {
    @SerializedName("round")
    @Expose
    private String round;
    @SerializedName("START_STOP_IMG")
    @Expose
    private String startStopImg;
    @SerializedName("hub_id")
    @Expose
    private String hubId;

    @SerializedName("distance_api_status")
    @Expose
    private String distance_api_status;

    @SerializedName("distance_api_key")
    @Expose
    private String distance_api_key;

    @SerializedName("distance_api_url")
    @Expose
    private String distance_api_url;

    public String getDistance_api_url() {
        return distance_api_url;
    }

    public void setDistance_api_url(String distance_api_url) {
        this.distance_api_url = distance_api_url;
    }

    public String getDistance_api_status() {
        return distance_api_status;
    }

    public void setDistance_api_status(String distance_api_status) {
        this.distance_api_status = distance_api_status;
    }

    public String getDistance_api_key() {
        return distance_api_key;
    }

    public void setDistance_api_key(String distance_api_key) {
        this.distance_api_key = distance_api_key;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getStartStopImg() {
        return startStopImg;
    }

    public void setStartStopImg(String startStopImg) {
        this.startStopImg = startStopImg;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }
  /*  @SerializedName("round")
    @Expose
    private String round;
    @SerializedName("START_STOP_IMG")
    @Expose
    private String startStopImg;

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getStartStopImg() {
        return startStopImg;
    }

    public void setStartStopImg(String startStopImg) {
        this.startStopImg = startStopImg;
    }*/

}
