package com.criticalog.ecritica.MVPHomeScreen;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.ColorUtils;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.work.WorkRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.criticalog.ecritica.Activities.ContactUs;
import com.criticalog.ecritica.BaseActivities.BaseActivity;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Interface.IAdapterClickListener;
import com.criticalog.ecritica.MVPAttendance.AttendanceActivity;
import com.criticalog.ecritica.MVPCannoteEnquiry.ConnoteEnquiry;
import com.criticalog.ecritica.MVPCashHandover.CashHandoverActivity;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPDispatchControllingHub.DispatchToHubActivity;
import com.criticalog.ecritica.MVPEdpSdcCopy.EDPActivity;
import com.criticalog.ecritica.MVPHistory.HistoryActivity;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.Detail;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripResponse;
import com.criticalog.ecritica.MVPInscan.InScanActivity;
import com.criticalog.ecritica.MVPInscanNew.InScanActivityNewDev;
import com.criticalog.ecritica.MVPLinehaul.LinehaulActivity;
import com.criticalog.ecritica.MVPLogin.LoginActivity;
import com.criticalog.ecritica.MVPNegativeStatus.NegativeStatusActivity;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.MVPPickupConfirm.PickupConfirmActivity;
import com.criticalog.ecritica.MVPPodScan.PodScanActivity;
import com.criticalog.ecritica.MVPSectorPickup.SectorPickupActivity;
import com.criticalog.ecritica.MVPTATCheck.TATCheckActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient1;
import com.criticalog.ecritica.Rest.RestServices1;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.ForceUpdateChecker;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.additionclaim.AdditionalClaimActivity;
import com.criticalog.ecritica.mvpdrspreparation.DRSPreparationActivity;
import com.criticalog.ecritica.mvproundcreation.RoundCreationActivity;
import com.criticalog.ecritica.nfofragment.NfoFragmentActivity;
import com.criticalog.ecritica.sendcitprintmail.CitPrintMailGenerationActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class HomeActivity extends BaseActivity implements HomeContract.MainView, IAdapterClickListener,
        View.OnClickListener, ForceUpdateChecker.OnUpdateNeededListener {
    HomeContract.presenter presenterHome;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    private NavigationView navigationView;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String sdkKeyNoticeboard = "935eda2b-b066-44e0-b2b2-e8b8f9d722f5";
    private String identityPostFix = "@criticalog.nb";
    private String noticeboardIdentity, user_id, user_name, designation, location;
    private TextView mTatCheck;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private double start_lat, start_long;
    private double latitude = 0.0, longitude = 0.0;
    private double latitude1 = 0, longitude1 = 0;
    private HomeContract.presenter mPresenter;
    private String userId, token;
    private SimpleArcDialog mProgressBar;
    private Button mStartRide, mStopRide;
    private String start_date, start_time;
    private ImageView mLogOut;
    private TextView mConnoteEnquiry;
    private String roundNumber = "";
    private Dialog mDilogKiloMeterImage;
    File photoFile = null;
    private static final int PERMISSION_REQUEST_KM_IMAGE = 1888;
    TextView mDone, mAttendanceAlert;
    ImageView mKmImage;
    private String base64KMImageCapture = "", kilometerNumber = "";
    EditText mKimeterNumber;
    double totaldistance = 0.0, totaldistance_g = 0.0;
    private String buttonType = "start_ride";
    private static final int PERMISSION_REQUEST_CODE = 200;
    ArrayList<Detail> latlonglist = new ArrayList<>();
    String hub_id;
    private String kmImageUploadFlag;
    private LocationManager locationManager;
    private String lccsStatus, address;
    private String isRGSA;
    private String startRide = "";
    private Dialog appUpdateDelivered;
    private static WorkRequest periodicWork;

    int googleApiDistance;

    Location gps_loc;
    Location network_loc;
    Location final_loc;
    String userCountry, userAddress;

    private TextView mPrs, mDrs, mInScan, mBagging, mLCCS, mSectorPickup,
            mEdpSdc, mNegativeStatus, mConfirmPickup, mPunch, mDispatchControHub, mPodScan, mCashHandover, mNewInScan, mCreateRound,
            mDRSPreparation, mAdditionalClaim, mNFOPrsDrs, mSendMailCit;

    private String prsFlag, drsFlag, inScnaFlag, baggingFlag, linehaulFlag, sectorFlag,
            edpsdcFlag, negativeFlag, pickupConfirmFlag, punchFlag, tatcheckFlag, conenquiryFlag,
            punchin_restriction, podFlag, newInscan, rounCreationFlag, drsPreaparationFlag, additionalClaimFlag,
            sendMailFlag, urgentShipmentFlag;

    private AppUpdateManager appUpdateManager;
    private static final int IMMEDIATE_APP_UPDATE_REQ_CODE = 124;
    private RestServices1 mRestServices1;
    private static final String TAG = "periodic-work-request";
    String newToken = "";
    String androidOS;
    int versionCode;
    String versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_mvp);

        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());

        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();
        androidOS = Build.VERSION.RELEASE;
        versionCode = BuildConfig.VERSION_CODE;
        versionName = BuildConfig.VERSION_NAME;

        checkGpsEnabledOrNot();

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(HomeActivity.this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(100); // 10 seconds
        locationRequest.setFastestInterval(100); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };
        getLocation();

        //Loader
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mPrs = findViewById(R.id.mPrs);
        mDrs = findViewById(R.id.mDrs);
        mInScan = findViewById(R.id.mInScan);
        mBagging = findViewById(R.id.mBagging);
        mLCCS = findViewById(R.id.mLCCS);
        mSectorPickup = findViewById(R.id.mSectorPickup);
        mEdpSdc = findViewById(R.id.mEdpSdc);
        mNegativeStatus = findViewById(R.id.mNegativeStatus);
        mConfirmPickup = findViewById(R.id.mConfirmPickup);
        mPunch = findViewById(R.id.mPunch1);
        mDispatchControHub = findViewById(R.id.mDispatchControHub);
        mNFOPrsDrs = findViewById(R.id.mNFOPrsDrs);
        mPodScan = findViewById(R.id.mPodScan);
        mCashHandover = findViewById(R.id.mCashHandover);
        mNewInScan = findViewById(R.id.mNewInScan);
        mCreateRound = findViewById(R.id.mCreateRound);
        mDRSPreparation = findViewById(R.id.mDRSPreparation);
        mAdditionalClaim = findViewById(R.id.mAdditionalClaim);
        mSendMailCit = findViewById(R.id.mSendMailCit);

        mPrs.setOnClickListener(this);
        mDrs.setOnClickListener(this);
        mInScan.setOnClickListener(this);
        mBagging.setOnClickListener(this);
        mLCCS.setOnClickListener(this);
        mEdpSdc.setOnClickListener(this);
        mNegativeStatus.setOnClickListener(this);
        mConfirmPickup.setOnClickListener(this);
        mPunch.setOnClickListener(this);
        mSectorPickup.setOnClickListener(this);
        mDispatchControHub.setOnClickListener(this);
        mNFOPrsDrs.setOnClickListener(this);
        mSendMailCit.setOnClickListener(this);
        mPodScan.setOnClickListener(this);
        mCashHandover.setOnClickListener(this);
        mNewInScan.setOnClickListener(this);
        mCreateRound.setOnClickListener(this);
        mDRSPreparation.setOnClickListener(this);
        mAdditionalClaim.setOnClickListener(this);
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        if (mCriticalogSharedPreferences != null) {
            user_id = mCriticalogSharedPreferences.getData("userId");
            user_name = mCriticalogSharedPreferences.getData("username");
            designation = mCriticalogSharedPreferences.getData("designation");
            location = mCriticalogSharedPreferences.getData("location");

            prsFlag = mCriticalogSharedPreferences.getData("prs_flag");
            drsFlag = mCriticalogSharedPreferences.getData("drs_flag");
            inScnaFlag = mCriticalogSharedPreferences.getData("inscan_flag");
            baggingFlag = mCriticalogSharedPreferences.getData("bagging_flag");
            linehaulFlag = mCriticalogSharedPreferences.getData("linehaul_flag");
            sectorFlag = mCriticalogSharedPreferences.getData("sector_flag");
            edpsdcFlag = mCriticalogSharedPreferences.getData("edp_sdc_flag");
            negativeFlag = mCriticalogSharedPreferences.getData("negative_flag");
            pickupConfirmFlag = mCriticalogSharedPreferences.getData("pickupconfirm_flag");
            punchFlag = mCriticalogSharedPreferences.getData("punch_flag");
            tatcheckFlag = mCriticalogSharedPreferences.getData("tatcheck_flag");
            conenquiryFlag = mCriticalogSharedPreferences.getData("conenquiry_flag");
            podFlag = mCriticalogSharedPreferences.getData("pod");
            newInscan = mCriticalogSharedPreferences.getData("new_inscan");

            rounCreationFlag = mCriticalogSharedPreferences.getData("ROUND_CREATION");
            drsPreaparationFlag = mCriticalogSharedPreferences.getData("DRS_PREPARATION");

            additionalClaimFlag = mCriticalogSharedPreferences.getData("ADDITIONAL_CLAIM");

            sendMailFlag = mCriticalogSharedPreferences.getData("SEND_MAIL");
            urgentShipmentFlag = mCriticalogSharedPreferences.getData("URGENT_SHIPMENT");
        }

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        StaticUtils.distance_api_url = mCriticalogSharedPreferences.getData("distance_api_url");
        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        lccsStatus = mCriticalogSharedPreferences.getData("LCCS");
        isRGSA = mCriticalogSharedPreferences.getData("is_rgsa");

        // getPunchStatus();
        //Dialog Manual Entry
        mDilogKiloMeterImage = new Dialog(HomeActivity.this);
        mDilogKiloMeterImage.setContentView(R.layout.dialog_km_image);
        mDilogKiloMeterImage.setCanceledOnTouchOutside(false);
        mDilogKiloMeterImage.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mRestServices1 = RestClient1.getRetrofitInstance().create(RestServices1.class);


        if (mCriticalogSharedPreferences.haveNetworkConnection() == true) {
            // Toast.makeText(this, "Network Connected!", Toast.LENGTH_SHORT).show();
        } else {

            try {
                new AlertDialog.Builder(HomeActivity.this)
                        .setTitle("Error")
                        .setMessage("Internet not available, Cross check your internet connectivity and try again later...")
                        .setCancelable(false)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            } catch (Exception e) {
                //    Log.d(Constants.TAG, "Show Dialog: " + e.getMessage());
            }

        }

        mStartRide = findViewById(R.id.mStartRide);
        mStopRide = findViewById(R.id.mStopRide);
        mTatCheck = findViewById(R.id.mTatCheck);
        mLogOut = findViewById(R.id.mLogOut);
        mConnoteEnquiry = findViewById(R.id.mConnoteEnquiry);
        mAttendanceAlert = findViewById(R.id.mAttendanceAlert);

        mStopRide.setOnClickListener(this);
        mStartRide.setOnClickListener(this);
        mLogOut.setOnClickListener(this);
        mConnoteEnquiry.setOnClickListener(this);


        punchin_restriction = mCriticalogSharedPreferences.getData("punchin_restriction");

        if (mCriticalogSharedPreferences.getData("start_stop").equalsIgnoreCase("true")) {
            mStartRide.setVisibility(View.GONE);
            mStopRide.setVisibility(View.VISIBLE);
        } else {
            mStartRide.setVisibility(View.VISIBLE);
            mStopRide.setVisibility(View.GONE);
        }

        if (mCriticalogSharedPreferences.getData("show_dialog_nb").equals("")) {
            callBannerAPI();
        }
        navigationView = findViewById(R.id.nav_view);

        mTatCheck.setOnClickListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView userid = (TextView) headerView.findViewById(R.id.nav_header_textView1);
        TextView username = (TextView) headerView.findViewById(R.id.nav_header_textView);

        userid.setText(user_id);
        username.setText(user_name);

        noticeboardIdentity = user_id + identityPostFix;
        setUpDrawerLayout();

        getShowHideDetais();

        hideItem();

        setUpNavigationView(navigationView);


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
        });


    }

    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }

        } else {
            showSettingAlert();
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(HomeActivity.this, location -> {

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);

                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void getShowHideDetais() {
        if (prsFlag.equalsIgnoreCase("true")) {

            mPodScan.setVisibility(View.VISIBLE);
        } else {
            mPrs.setVisibility(View.GONE);

        }

        if (podFlag.equalsIgnoreCase("true")) {
            mPrs.setVisibility(View.VISIBLE);
        } else {
            mPodScan.setVisibility(View.GONE);
        }


        if (newInscan.equalsIgnoreCase("true")) {
            mNewInScan.setVisibility(View.VISIBLE);
        } else {
            mNewInScan.setVisibility(View.GONE);
        }

        if (drsPreaparationFlag.equalsIgnoreCase("true")) {
            mDRSPreparation.setVisibility(View.VISIBLE);
        } else {
            mDRSPreparation.setVisibility(View.GONE);
        }
        if (additionalClaimFlag.equalsIgnoreCase("true")) {
            mAdditionalClaim.setVisibility(View.VISIBLE);
        } else {
            mAdditionalClaim.setVisibility(View.GONE);
        }


        if (rounCreationFlag.equalsIgnoreCase("true")) {
            mCreateRound.setVisibility(View.VISIBLE);
        } else {
            mCreateRound.setVisibility(View.GONE);
        }

        if (drsFlag.equalsIgnoreCase("true")) {
            mDrs.setVisibility(View.VISIBLE);
        } else {
            mDrs.setVisibility(View.GONE);
        }
        if (inScnaFlag.equalsIgnoreCase("true")) {
            mInScan.setVisibility(View.VISIBLE);
        } else {
            mInScan.setVisibility(View.GONE);
        }
        if (baggingFlag.equalsIgnoreCase("true")) {
            mBagging.setVisibility(View.VISIBLE);
        } else {
            mBagging.setVisibility(View.GONE);
        }
        if (edpsdcFlag.equalsIgnoreCase("true")) {
            mEdpSdc.setVisibility(View.VISIBLE);
        } else {
            mEdpSdc.setVisibility(View.GONE);
        }
        if (negativeFlag.equalsIgnoreCase("true")) {
            mNegativeStatus.setVisibility(View.VISIBLE);
        } else {
            mNegativeStatus.setVisibility(View.GONE);
        }

        if (pickupConfirmFlag.equalsIgnoreCase("true")) {
            mConfirmPickup.setVisibility(View.VISIBLE);
            mDispatchControHub.setVisibility(View.VISIBLE);
        } else {
            mConfirmPickup.setVisibility(View.GONE);
            mDispatchControHub.setVisibility(View.GONE);
        }
        if (tatcheckFlag.equalsIgnoreCase("true")) {
            mTatCheck.setVisibility(View.VISIBLE);
        } else {
            mTatCheck.setVisibility(View.GONE);
        }

        if (conenquiryFlag.equalsIgnoreCase("true")) {
            mConnoteEnquiry.setVisibility(View.VISIBLE);
        } else {
            mConnoteEnquiry.setVisibility(View.GONE);
        }

        if (linehaulFlag.equalsIgnoreCase("true")) {
            mLCCS.setVisibility(View.VISIBLE);
        } else {
            mLCCS.setVisibility(View.GONE);
        }

        if (sectorFlag.equalsIgnoreCase("true")) {
            mSectorPickup.setVisibility(View.VISIBLE);
        } else {
            mSectorPickup.setVisibility(View.GONE);
        }

        if (punchFlag.equalsIgnoreCase("true")) {
            mPunch.setVisibility(View.VISIBLE);
        } else {
            mPunch.setVisibility(View.GONE);
        }

        if (sendMailFlag.equalsIgnoreCase("true")) {
            mSendMailCit.setVisibility(View.VISIBLE);
        } else {
            mSendMailCit.setVisibility(View.GONE);
        }

        if (urgentShipmentFlag.equalsIgnoreCase("true")) {
            mNFOPrsDrs.setVisibility(View.VISIBLE);
        } else {
            mNFOPrsDrs.setVisibility(View.GONE);
        }
    }

    private void hideItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        if (prsFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.prs).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.prs).setVisible(false);
        }

        if (podFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.pod).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.pod).setVisible(false);
        }
        if (newInscan.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.newInScan).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.newInScan).setVisible(false);
        }

        if (newInscan.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.pod).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.pod).setVisible(false);
        }

        if (drsPreaparationFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mDRSPreparation).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mDRSPreparation).setVisible(false);
        }

        if (additionalClaimFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mAdditionalClaim).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mAdditionalClaim).setVisible(false);
        }

        if (rounCreationFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mCreateRound).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mCreateRound).setVisible(false);
        }

        if (drsFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.drs).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.drs).setVisible(false);
        }
        if (inScnaFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.inscan).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.inscan).setVisible(false);
        }
        if (baggingFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.bagging).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.bagging).setVisible(false);
        }
        if (linehaulFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.linehaulReceived).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.linehaulReceived).setVisible(false);
        }
        if (sectorFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.sectorpickup).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.sectorpickup).setVisible(false);
        }
        if (prsFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.prs).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.prs).setVisible(false);
        }
        if (punchFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mAttendance).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mAttendance).setVisible(false);
        }

        if (conenquiryFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.connoteinquery).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.connoteinquery).setVisible(false);
        }

        if (tatcheckFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.tatcheck).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.tatcheck).setVisible(false);
        }
        if (pickupConfirmFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mPickupRGSA).setVisible(true);
            nav_Menu.findItem(R.id.mRGSACtrlHub).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mPickupRGSA).setVisible(false);
            nav_Menu.findItem(R.id.mRGSACtrlHub).setVisible(false);
        }
        if (negativeFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.negative_status).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.negative_status).setVisible(false);
        }

        if (sendMailFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mSendMailCit).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mSendMailCit).setVisible(false);
        }

        if (urgentShipmentFlag.equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.mUrgentShipment).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.mUrgentShipment).setVisible(false);
        }
    }

    public void textAnimation(String message) {
        mAttendanceAlert.setText(message);
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
        valueAnimator.setDuration(325);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                float fractionAnim = (float) valueAnimator.getAnimatedValue();

                mAttendanceAlert.setTextColor(ColorUtils.blendARGB(Color.parseColor("#FFFFFF")
                        , Color.parseColor("#000000")
                        , fractionAnim));
            }
        });
        valueAnimator.start();
    }

    public void getPunchStatus() {
        PunchStatusRequest punchStatusRequest = new PunchStatusRequest();
        punchStatusRequest.setAction("check_punch_in_status_new");
        punchStatusRequest.setUserId(userId);
        punchStatusRequest.setLatitude(String.valueOf(latitude));
        punchStatusRequest.setLongitude(String.valueOf(longitude));
        punchStatusRequest.setApp_ver_name(versionName);
        punchStatusRequest.setApp_ver_testing(versionName);
        mPresenter = new HomePresenterImpl(this, new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl());
        mPresenter.punchStatusRequest(punchStatusRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMMEDIATE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Update canceled by user! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Update success! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Update Failed! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
                checkUpdate();
            }
        }
        if (requestCode == 1888) {
            if (requestCode == PERMISSION_REQUEST_KM_IMAGE && resultCode == Activity.RESULT_OK) {
                //  Bitmap photo = (Bitmap) data.getExtras().get("data");

                if (photoFile != null) {
                    File absoluteFile = photoFile.getAbsoluteFile();
                    if (absoluteFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());
                        //   captureImage.setImageBitmap(myBitmap);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 5, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64KMImageCapture = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        //mPodCapture.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        mKmImage.setImageURI(Uri.fromFile(absoluteFile.getAbsoluteFile()));
                    }
                } else {
                    requestPermission();
                    Toast.makeText(HomeActivity.this, "Go To Settings and Allow Camera and Storeage Permission", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void callMilometerUpdateAPI() {
        if (buttonType.equalsIgnoreCase("start_ride")) {
            mCriticalogSharedPreferences.saveData("start_km_milometer", kilometerNumber);
        } else {
            mCriticalogSharedPreferences.saveData("stop_km_milometer", kilometerNumber);
        }
        MilometerUpdateRequest milometerUpdateRequest = new MilometerUpdateRequest();
        milometerUpdateRequest.setAction("milomtr_update");
        milometerUpdateRequest.setButtonType(buttonType);
        milometerUpdateRequest.setUserId(userId);
        milometerUpdateRequest.setRoundNo(roundNumber);
        milometerUpdateRequest.setImage1(base64KMImageCapture);
        milometerUpdateRequest.setMilomtr(kilometerNumber);
        milometerUpdateRequest.setLatitude(String.valueOf(latitude));
        milometerUpdateRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new HomePresenterImpl(this, new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl());
        mPresenter.milometerUpdateRequest(milometerUpdateRequest);
    }

    public void callBannerAPI() {
        BannerRequest bannerRequest = new BannerRequest();
        bannerRequest.setAction("banner");
        bannerRequest.setUserId(userId);
        bannerRequest.setForPage("1");
        bannerRequest.setLatitude(String.valueOf(latitude));
        bannerRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new HomePresenterImpl(this, new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl());
        mPresenter.bannerCheck(token, bannerRequest);

    }

    @Override
    public void onClickAdapter(int position) {
        startRide = mCriticalogSharedPreferences.getData("start_stop");
    }

    public void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("0k", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }


    private void getLocationRiderLog() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(HomeActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("LONG_START", String.valueOf(latitude) + " :    " + String.valueOf(longitude));

                } else {
                    Log.e("LONG_START", String.valueOf(latitude) + "" + String.valueOf(longitude));
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    private void getLocation(String kmImageUploadFlag) {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            try {
                if (mCriticalogSharedPreferences.getData("stopbtnclick").equals("yes")) {
                    double distance = distance(mCriticalogSharedPreferences.getDoubleData("last_activity_lat"), mCriticalogSharedPreferences.getDoubleData("last_activity_long"), latitude, longitude, "k");
                    Log.d("last activity", mCriticalogSharedPreferences.getDoubleData("last_activity_lat") + "/" + mCriticalogSharedPreferences.getDoubleData("last_activity_long"));
                    Log.d("current lat long", latitude + "/" + longitude);
                    String convertedString = converttorealnumber(distance);
                    mCriticalogSharedPreferences.saveData("punch_lat", String.valueOf(latitude));
                    mCriticalogSharedPreferences.saveData("punch_long", String.valueOf(longitude));


                    if (start_lat == 0.0 || start_long == 0.0) {

                        if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                            manualDialog();
                        } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                        } else {
                            callRiderLog();
                        }

                    } else {
                        double distance_lastactivity_hub = distance(mCriticalogSharedPreferences.getDoubleData("last_activity_lat"), mCriticalogSharedPreferences.getDoubleData("last_activity_long"), start_lat, start_long, "k");
                        String convertedstringlastactivitytohub = converttorealnumber(distance_lastactivity_hub);

                        if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                            manualDialog();
                        } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                        } else {
                            callRiderLog();
                        }

                    }

                } else {
                    mCriticalogSharedPreferences.saveDoubleData("last_activity_lat", latitude);
                    mCriticalogSharedPreferences.saveDoubleData("last_activity_long", longitude);

                    if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                        manualDialog();
                    } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                    } else {
                        callRiderLog();
                    }
                }
                mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(HomeActivity.this, location -> {

                    mCriticalogSharedPreferences.saveData("location_permission", "yes");
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        mCriticalogSharedPreferences.saveData("punch_lat", String.valueOf(latitude));
                        mCriticalogSharedPreferences.saveData("punch_long", String.valueOf(longitude));

                        if (mCriticalogSharedPreferences.getData("stopbtnclick").equals("yes")) {
                            double distance = distance(mCriticalogSharedPreferences.getDoubleData("last_activity_lat"), mCriticalogSharedPreferences.getDoubleData("last_activity_long"), latitude, longitude, "k");
                            Log.d("last activity", mCriticalogSharedPreferences.getDoubleData("last_activity_lat") + "/" + mCriticalogSharedPreferences.getDoubleData("last_activity_long"));
                            Log.d("current lat long", latitude + "/" + longitude);
                            String convertedString = converttorealnumber(distance);

                            if (start_lat == 0.0 || start_long == 0.0) {

                                if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                                    manualDialog();
                                } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                                } else {
                                    callRiderLog();
                                }

                            } else {
                                double distance_lastactivity_hub = distance(mCriticalogSharedPreferences.getDoubleData("last_activity_lat"), mCriticalogSharedPreferences.getDoubleData("last_activity_long"), start_lat, start_long, "k");
                                String convertedstringlastactivitytohub = converttorealnumber(distance_lastactivity_hub);
                                if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                                    manualDialog();
                                } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                                } else {
                                    callRiderLog();
                                }

                            }

                        } else {
                            mCriticalogSharedPreferences.saveDoubleData("last_activity_lat", latitude);
                            mCriticalogSharedPreferences.saveDoubleData("last_activity_long", longitude);
                            Log.d("last activity ", "lat long" + latitude + "/" + longitude);
                            if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                                manualDialog();
                            } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                            } else {
                                callRiderLog();
                            }

                        }
                    } else {

                        mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("FINE_LOCC", e.toString());

            }
        }
    }

    private void checkUpdate() {

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                startUpdateFlow(appUpdateInfo);
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                startUpdateFlow(appUpdateInfo);
            }
        });
    }

    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE, this, HomeActivity.IMMEDIATE_APP_UPDATE_REQ_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    public void setUpDrawerLayout() {

        toolbar = findViewById(R.id.toolbar_main);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.black));
        toggle.setHomeAsUpIndicator(R.drawable.outline_view_headline_24);
        toggle.setDrawerIndicatorEnabled(false);
        drawer.addDrawerListener(toggle);

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.show();
        }

    }

    @Override
    public void hideProgress() {

        if (mProgressBar != null && mProgressBar.isShowing()) {
            mProgressBar.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setDataToViews(ArrayList<String> homeOptions) {
      /*  RecyclerView mRvOptions;
        mRvOptions = findViewById(R.id.mRvOptionsOperations);
        HomeAdapter homeAdapter = new HomeAdapter(this, homeOptions, this);

        mRvOptions = findViewById(R.id.mRvOptionsOperations);
        mRvOptions.setLayoutManager(new GridLayoutManager(this, 2));
        mRvOptions.setAdapter(homeAdapter);*/
    }

    @Override
    public void setRiderLogDataToViews(RiderLogResponse riderLogResponse) {

        if (riderLogResponse.getStatus() == 200) {
            // Toast.makeText(HomeActivity.this, riderLogResponse.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            //  Toast.makeText(HomeActivity.this, riderLogResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setTripResponseToViews(TripResponse tripResponse) {

        if (tripResponse.getStatus() == 200) {
            Toasty.success(this, tripResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            mCriticalogSharedPreferences.saveData("start_stop", "false");
            dbHelper.clearTableData();
            //  recreate();
        } else {
            Toasty.error(this, tripResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void setStartStopRideDataToViews(StartStopRideResponse startStopRideResponse) {

        Log.e("START_RESPONSE", startStopRideResponse.getMessage());


        if (startStopRideResponse.getStatus() == 200) {


            mCriticalogSharedPreferences.saveData("XHUBID", startStopRideResponse.getData().getHubId());
            kmImageUploadFlag = startStopRideResponse.getData().getStartStopImg();

            StaticUtils.billionDistanceAPIKey = startStopRideResponse.getData().getDistance_api_key();
            StaticUtils.distance_api_url = startStopRideResponse.getData().getDistance_api_url();

            mCriticalogSharedPreferences.saveData("distance_api_status", startStopRideResponse.getData().getDistance_api_status());
            mCriticalogSharedPreferences.saveData("distance_api_key", startStopRideResponse.getData().getDistance_api_key());
            mCriticalogSharedPreferences.saveData("distance_api_url", startStopRideResponse.getData().getDistance_api_url());

            if (startStopRideResponse.getMessage().equalsIgnoreCase("Ride started successfully")) {
                mCriticalogSharedPreferences.saveData("start_stop_ride_message", startStopRideResponse.getMessage().toString());
                dbHelper.clearTableData();

                start_date = mCriticalogSharedPreferences.getData("start_date");
                start_time = mCriticalogSharedPreferences.getData("start_time");

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String date = df.format(c);

                SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
                String time = df1.format(c);


                PodClaimPojo podClaimPojo;

                podClaimPojo = new PodClaimPojo(0.0, 0.0, latitude, longitude, "0.0", "0.0", date, time, "startRide", "0", "", date + " " + time, 10);

                dbHelper.insertLatLOngDetails(podClaimPojo);


                String input = "message";


                mCriticalogSharedPreferences.saveData("round", startStopRideResponse.getData().getRound());

                roundNumber = startStopRideResponse.getData().getRound();
                mCriticalogSharedPreferences.saveData("ROUND_NO", roundNumber);


                if (mCriticalogSharedPreferences.getData("location_permission").equalsIgnoreCase("")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

                    alertDialogBuilder.setTitle("Location Permission");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage("This App Collects Location data to Calculate the Distance Travelled by FLL(Front Line Leader) for Claim Process even when the app is closed.");

                    alertDialogBuilder.setPositiveButton("Allow",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Dexter.withContext(HomeActivity.this)
                                            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                                            .withListener(new PermissionListener() {
                                                @Override
                                                public void onPermissionGranted(PermissionGrantedResponse response) {

                                                    mCriticalogSharedPreferences.saveData("startride", "started");

                                                    kmImageUploadFlag = startStopRideResponse.getData().getStartStopImg();

                                                    getLocation(startStopRideResponse.getData().getStartStopImg());
                                                }

                                                @Override
                                                public void onPermissionDenied(PermissionDeniedResponse response) {
                                                    mCriticalogSharedPreferences.saveData("location_permission", "");

                                                }

                                                @Override
                                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                                                }
                                            }).check();
                                }
                            });


                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    //    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));

                } else {

                    mCriticalogSharedPreferences.saveData("startride", "started");

                    getLocation(startStopRideResponse.getData().getStartStopImg());

                    if (kmImageUploadFlag.equalsIgnoreCase("1")) {
                        manualDialog();
                    } else if (kmImageUploadFlag.equalsIgnoreCase("2")) {

                    } else {
                        callRiderLog();
                    }
                }

            } else if (startStopRideResponse.getMessage().equalsIgnoreCase("Ride stopped successfully")) {

                mCriticalogSharedPreferences.saveData("start_stop_ride_message", startStopRideResponse.getMessage().toString());
                getLocation();
                //  workManager.cancelAllWorkByTag(TAG);

                //  WorkManager.cancelWorkById(periodicWork.getId());

                roundNumber = startStopRideResponse.getData().getRound();
                mCriticalogSharedPreferences.saveData("ROUND_NO", roundNumber);

                start_date = mCriticalogSharedPreferences.getData("start_date");
                start_time = mCriticalogSharedPreferences.getData("start_time");

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String date = df.format(c);

                SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
                String time = df1.format(c);

                latlonglist = dbHelper.getAllLatLongListNew();
                if (latlonglist.size() > 0) {
                    int listsize = latlonglist.size();


                    PodClaimPojo podClaimPojo;

                    double distnce = distance(latlonglist.get(listsize - 1).getEndLat(), latlonglist.get(listsize - 1).getEndLong(), latitude, longitude, "K");
                    String convertedstring = converttorealnumber(distnce);

                    String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEndLat(), latlonglist.get(listsize - 1).getEndLong(), latitude, longitude);

                    if (convertedstring_g.equalsIgnoreCase("0.0")) {
                        convertedstring_g = convertedstring;
                    }
                    podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEndLat(), latlonglist.get(listsize - 1).getEndLong(), latitude, longitude, convertedstring, convertedstring_g, date, time, "closetrip", "0", "", date + " " + time, 10);


                    Log.e("double_value", latitude + ":" + longitude);
                    // latlonglist.add(podClaimPojo);
                    dbHelper.insertLatLOngDetails(podClaimPojo);
                }

                hub_id = mCriticalogSharedPreferences.getData("hub_id");

                latlonglist = dbHelper.getAllLatLongListNew();

                for (int j = 0; j < latlonglist.size(); j++) {

                    if (latlonglist.get(j).getDistance() != null) {

                        if (Double.valueOf(latlonglist.get(j).getDistance()) > 400) {
                            totaldistance = totaldistance + 1.00;
                        } else {
                            totaldistance = totaldistance + Double.valueOf(latlonglist.get(j).getDistance());
                        }
                    }

                    if (latlonglist.get(j).getDistance_g() != null) {

                        if (Double.valueOf(latlonglist.get(j).getDistance_g()) > 400) {
                            totaldistance_g = totaldistance_g + 1.00;
                        } else {
                            totaldistance_g = totaldistance_g + Double.valueOf(latlonglist.get(j).getDistance_g());
                        }
                    }
                }

                manualDialog();

                //  callFllTrip();
            }

        } else {
            Log.e("START_RESPONSE", startStopRideResponse.getMessage());
            Toasty.warning(this, startStopRideResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            getLocationRiderLog();

            Log.e("double_value", latitude + ":" + longitude);
        }
    }

    public String getDistanceFromGoogleAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        String distnace = "0";

        float distanceInKm = 0;
        String nextBillionHitStatus = mCriticalogSharedPreferences.getData("distance_api_status");
        if (nextBillionHitStatus.equalsIgnoreCase("1")) {
            distnace = mCriticalogSharedPreferences.getDistanceFromGoogelBillionAPI(lat1, lon1, lat2, lon2);

            distanceInKm = convertMeterToKilometer(Float.valueOf(distnace));
        } else {
            distanceInKm = 0;
        }

        return String.valueOf(distanceInKm);
    }

    public static float convertMeterToKilometer(float meter) {
        return (float) (meter * 0.001);
    }

    @Override
    public void bannerResponseData(BannerResponse bannerResponse) {

        if (bannerResponse != null) {
            if (bannerResponse.getStatus() == 200) {

                if (bannerResponse.getData() != null) {
                    String bannerURL = bannerResponse.getData().get(0).getImgUrl();

                    if (!bannerURL.equals("")) {
                        showBannerDialog(bannerURL);
                    } else {
                        mCriticalogSharedPreferences.saveData("show_dialog_nb", "");
                    }
                }
            } else {
                mCriticalogSharedPreferences.saveData("show_dialog_nb", "");
            }
        }

    }

    @Override
    public void milometerDataSetToViews(MilometerUpdateResponse milometerUpdateResponse) {
        if (milometerUpdateResponse.getStatus() == 200) {
            mDilogKiloMeterImage.setCancelable(true);
            mDilogKiloMeterImage.dismiss();

            if (mStartRide.getVisibility() == View.VISIBLE) {
                mStartRide.setVisibility(View.GONE);
                mStopRide.setVisibility(View.VISIBLE);
                mCriticalogSharedPreferences.saveData("start_stop", "true");
                if (mDilogKiloMeterImage.isShowing()) {
                    mDilogKiloMeterImage.dismiss();
                }
                callRiderLog();

            } else {
                // callFllTrip();
                callRiderLog();
                mStartRide.setVisibility(View.VISIBLE);
                mStopRide.setVisibility(View.GONE);
                mCriticalogSharedPreferences.saveData("start_stop", "false");
                if (mDilogKiloMeterImage.isShowing()) {
                    mDilogKiloMeterImage.dismiss();
                }
            }

            String message = mCriticalogSharedPreferences.getData("start_stop_ride_message");

            if (message.equalsIgnoreCase("Ride stopped successfully")) {
                callFllTrip();
            }

            Toasty.success(HomeActivity.this, milometerUpdateResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        } else {
            Toasty.warning(HomeActivity.this, milometerUpdateResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void punchStatusDataToViews(PunchStatusResponse punchStatusResponse) {
        String versionName = BuildConfig.VERSION_NAME;
        // productDeliveredDialog();
        if (punchStatusResponse != null) {

            if (punchStatusResponse.getStatus() == 200) {
                mCriticalogSharedPreferences.saveData("image_vision", String.valueOf(punchStatusResponse.getData().get(0).getImage_vision()));
                if (punchStatusResponse.getData().get(0).getLatest_app_version().equalsIgnoreCase(versionName)) {
                    mAttendanceAlert.setVisibility(View.VISIBLE);
                    textAnimation(punchStatusResponse.getMessage());
                    mCriticalogSharedPreferences.saveData("punchin_restriction", String.valueOf(punchStatusResponse.getData().get(0).getIsPunchedIn()));
                    mCriticalogSharedPreferences.saveData("last_address", punchStatusResponse.getData().get(0).getDetails().get(0).getAddressIn());
                    mCriticalogSharedPreferences.saveData("punchin_time", punchStatusResponse.getData().get(0).getDetails().get(0).getDatePunchIn());
                    mCriticalogSharedPreferences.saveData("last_address1", punchStatusResponse.getData().get(0).getDetails().get(0).getAddressOut());
                    mCriticalogSharedPreferences.saveData("punchout_time", punchStatusResponse.getData().get(0).getDetails().get(0).getDatePunchOut());

                } else {
                    showUpdateDialog();
                    Toasty.error(HomeActivity.this, "Your app is not updated upadte it from Playstore or uinstall it", Toast.LENGTH_SHORT, true).show();
                }
            } else {

                if(punchStatusResponse.getData()!=null)
                {
                    mCriticalogSharedPreferences.saveData("image_vision", String.valueOf(punchStatusResponse.getData().get(0).getImage_vision()));

                    if (punchStatusResponse.getData().get(0).getLatest_app_version().equalsIgnoreCase(versionName)) {
                        mAttendanceAlert.setVisibility(View.VISIBLE);
                        textAnimation(punchStatusResponse.getMessage());
                        try {
                            if (punchStatusResponse.getData().get(0).getIsPunchedIn() != null) {
                                mCriticalogSharedPreferences.saveData("punchin_restriction", String.valueOf(punchStatusResponse.getData().get(0).getIsPunchedIn()));
                            }
                        } catch (Exception e) {

                        }
                    } else {
                        showUpdateDialog();
                        Toasty.error(HomeActivity.this, "Your app is not updated upadte it from Playstore or uinstall it", Toast.LENGTH_SHORT, true).show();
                    }
                }else {
                    Toast.makeText(this, punchStatusResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

                Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
            if (photoFile != null) {
                //Uri photoURI = FileProvider.getUriForFile(this,".provider",photoFile);

                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    public void manualDialog() {

        mDone = mDilogKiloMeterImage.findViewById(R.id.mDone);
        mKmImage = mDilogKiloMeterImage.findViewById(R.id.mKMImage);
        mKimeterNumber = mDilogKiloMeterImage.findViewById(R.id.mKimeterNumber);
        mKmImage.setImageResource(R.drawable.outline_photo_camera_24);
        mKimeterNumber.setText("");

        mDilogKiloMeterImage.show();

        mKmImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_KM_IMAGE);
                        Toasty.warning(HomeActivity.this, "GO TO SETTINGS AND ALLOW CAMEARA OPTION", Toast.LENGTH_LONG, true).show();

                    } else {
                        openCameraIntent(PERMISSION_REQUEST_KM_IMAGE);
                    }
                } else {
                    openCameraIntent(PERMISSION_REQUEST_KM_IMAGE);
                }
            }
        });

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                kilometerNumber = mKimeterNumber.getText().toString();
                if (base64KMImageCapture.equals("")) {
                    Toasty.warning(HomeActivity.this, "Please capture the image", Toast.LENGTH_LONG, true).show();
                } else if (kilometerNumber.equals("")) {
                    Toasty.warning(HomeActivity.this, "Enter Kilometres!!", Toast.LENGTH_LONG, true).show();
                } else {
                    //Here new code added
                    start_date = mCriticalogSharedPreferences.getData("start_date");
                    start_time = mCriticalogSharedPreferences.getData("start_time");

                    callMilometerUpdateAPI();
                }

            }
        });
    }

    private File createScopedStorage() throws IOException {
        // Get the context
        Context context = getApplicationContext();

// Get the file name and create the file
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File file = new File(context.getFilesDir(), timeStamp + ".jpg");
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write("Hello, World!".getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

// Return the file
        return file;

    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//-----------------------------------------------------------//
        File image = null;
        try {
            image = File.createTempFile("imagename",
                    ".jpg", getCacheDir());
            // File created successfully
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            // Error creating temp file
        }
        //-------------------------------------------------------------//
        /*File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );*/
        imagePath = image.getAbsolutePath();
        return image;
    }

    public void productDeliveredDialog() {
        TextView mOkDialog;
        appUpdateDelivered = new Dialog(HomeActivity.this);
        appUpdateDelivered.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = appUpdateDelivered.findViewById(R.id.mOk);

        appUpdateDelivered.setCancelable(false);
        appUpdateDelivered.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUpdateDelivered.dismiss();
                redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica&hl=en_IN&gl=US");
            }
        });
    }

    public void showBannerDialog(String bannerURL) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(HomeActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_banner);
        // Set dialog title
        // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        // set values for custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.mBannerText);
        ImageView image = (ImageView) dialog.findViewById(R.id.mImageBanner);
        TextView moreUpdates = dialog.findViewById(R.id.mMoreUpdates);

        Picasso.with(this).load(bannerURL).into(image);

        dialog.setCancelable(false);
        dialog.show();

        moreUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle metadata = new Bundle();
                if (designation.equals("")) {
                    metadata.putString("designation", "FLL");
                } else {
                    metadata.putString("designation", designation);
                }
                if (location.equals("")) {
                    metadata.putString("location", "Bengaluru");
                } else {
                    metadata.putString("location", location);
                }
                metadata.putString("employeeId", userId);
                metadata.putString("name", user_name);

                //  NbSdkConnector.openNoticeboardWithMetadata(sdkKeyNoticeboard, HomeActivity.this, noticeboardIdentity, metadata);
                mCriticalogSharedPreferences.saveData("show_dialog_nb", "true");
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        if (mProgressBar != null) {
            mProgressBar.dismiss();
        }

        Toasty.warning(this, R.string.re_try, Toast.LENGTH_LONG, true).show();
    }

    private void selectDrawerItemintentt(MenuItem menuItem) {

        int id = menuItem.getItemId();
        punchin_restriction = mCriticalogSharedPreferences.getData("punchin_restriction");

        switch (id) {

            case R.id.prs:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (prsFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, PickupActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;

            case R.id.drs:
                startActivity(new Intent(this, DRSActivity.class));
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (drsFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, DRSActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;

            case R.id.tatcheck:

                if (tatcheckFlag.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, TATCheckActivity.class));
                    setUpNavigationView(navigationView);
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    setUpNavigationView(navigationView);
                    drawer.closeDrawer(GravityCompat.START);
                    Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.connoteinquery:
                if (conenquiryFlag.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, ConnoteEnquiry.class));
                    setUpNavigationView(navigationView);
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    setUpNavigationView(navigationView);
                    drawer.closeDrawer(GravityCompat.START);
                    Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.sectorpickup:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (sectorFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, SectorPickupActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.linehaulReceived:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (linehaulFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, LinehaulActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.inscan:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (inScnaFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, InScanActivity.class));
                        // startActivity(new Intent(this, InScanActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;

            case R.id.bagging:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (baggingFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, com.criticalog.ecritica.MVPBagging.BaggingActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mSendMailCit:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (startRide.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, CitPrintMailGenerationActivity.class));
                    } else {
                        Toasty.warning(this, "Please Start the Ride!", Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mUrgentShipment:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (startRide.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, NfoFragmentActivity.class));
                    } else {
                        Toasty.warning(this, "Please Start the Ride!", Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;
            case R.id.prshistory:
                mCriticalogSharedPreferences.saveData("history", "prs");
                startActivity(new Intent(this, HistoryActivity.class));
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.drshistory:
                mCriticalogSharedPreferences.saveData("history", "drs");
                Intent history = new Intent(this, HistoryActivity.class);
                startActivity(history);
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.negative_status:

                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (negativeFlag.equalsIgnoreCase("true")) {
                        Intent negativeStatus = new Intent(this, NegativeStatusActivity.class);
                        startActivity(negativeStatus);
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;
            case R.id.mContactUs:
                Intent contactUs = new Intent(this, ContactUs.class);
                startActivity(contactUs);
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.pod:
                Intent pod = new Intent(this, PodScanActivity.class);
                startActivity(pod);
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.newInScan:
                Intent inscanNew = new Intent(this, InScanActivityNewDev.class);
                startActivity(inscanNew);
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.mAttendance:
                getLocation("");
                if (punchFlag.equalsIgnoreCase("true")) {
                    mCriticalogSharedPreferences.saveData("punch_lat", String.valueOf(latitude));
                    mCriticalogSharedPreferences.saveData("punch_long", String.valueOf(longitude));
                    Intent attendance = new Intent(this, AttendanceActivity.class);
                    startActivity(attendance);
                    setUpNavigationView(navigationView);
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    setUpNavigationView(navigationView);
                    drawer.closeDrawer(GravityCompat.START);
                    Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                }

                break;
            case R.id.logout:
                mCriticalogSharedPreferences.saveData("login_status", "");
                mCriticalogSharedPreferences.saveData("last_address", "");
                mCriticalogSharedPreferences.saveData("punchin_time", "");
                mCriticalogSharedPreferences.saveData("last_address1", "");
                mCriticalogSharedPreferences.saveData("punchout_time", "");
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.mPickupRGSA:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (pickupConfirmFlag.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, PickupConfirmActivity.class));
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        setUpNavigationView(navigationView);
                        drawer.closeDrawer(GravityCompat.START);
                        Toasty.warning(this, R.string.you_dont_have_perm, Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mDRSPreparation:
                startActivity(new Intent(this, DRSPreparationActivity.class));
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.mCreateRound:
                startActivity(new Intent(this, RoundCreationActivity.class));
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.mAdditionalClaim:
                startActivity(new Intent(this, AdditionalClaimActivity.class));
                setUpNavigationView(navigationView);
                drawer.closeDrawer(GravityCompat.START);
                break;
        }

    }

    private void setUpNavigationView(final NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                selectDrawerItemintentt(menuItem);
                return true;
            }
        });
    }

    public void callFllTrip() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c);

        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        String time = df1.format(c);

        TripRequest tripRequest = new TripRequest();
        tripRequest.setAction("fll_trip");
        tripRequest.setDetails(latlonglist);
        tripRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        tripRequest.setStartDate(start_date);
        tripRequest.setUserId(userId);
        tripRequest.setEndDate(date);
        tripRequest.setEndTime(c.toString());
        tripRequest.setRound(roundNumber);
        tripRequest.setStartTime(start_time);
        tripRequest.setTotalDistance(String.valueOf(totaldistance));
        tripRequest.setDistance_g(String.valueOf(totaldistance_g));
        tripRequest.setMilomtrstart(mCriticalogSharedPreferences.getData("start_km_milometer"));
        tripRequest.setMilomtrstop(mCriticalogSharedPreferences.getData("stop_km_milometer"));
        tripRequest.setUserName(mCriticalogSharedPreferences.getData("username"));
        tripRequest.setLatitude(String.valueOf(latitude));
        tripRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new HomePresenterImpl(this, new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl());
        mPresenter.tripReuqest(token, tripRequest);
    }

    public void callStartRide(String startStopRide) {
        buttonType = startStopRide;

        StartStopRideRequest startStopRideRequest1 = new StartStopRideRequest();
        startStopRideRequest1.setAction("start_and_stop_ride");
        startStopRideRequest1.setButtonType(startStopRide);
        startStopRideRequest1.setUserId(userId);
        startStopRideRequest1.setLatitude(String.valueOf(latitude));
        startStopRideRequest1.setLongitude(String.valueOf(longitude));
        startStopRideRequest1.setFirebase_token(newToken);
        mPresenter = new HomePresenterImpl(this, new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl());
        mPresenter.startAndStopRide(token, startStopRideRequest1);
    }

    @Override
    public void onClick(View v) {
        startRide = mCriticalogSharedPreferences.getData("start_stop");
        punchin_restriction = mCriticalogSharedPreferences.getData("punchin_restriction");
        switch (v.getId()) {

            case R.id.mTatCheck:
                startActivity(new Intent(this, TATCheckActivity.class));

                break;
            case R.id.mStopRide:
                getLocationRiderLog();
                buttonType = "stop_ride";
                callStartRide("stop_ride");

                break;
            case R.id.mStartRide:
                if (checkPermission()) {
                } else {
                    requestPermission();
                }
                punchin_restriction = mCriticalogSharedPreferences.getData("punchin_restriction");
                buttonType = "start_ride";
                getLocationRiderLog();

                if (checkPermission()) {
                    if (punchin_restriction.equalsIgnoreCase("true")) {
                        callStartRide("start_ride");
                    } else {
                        Toasty.warning(this, R.string.punch_in_message, Toast.LENGTH_LONG, true).show();
                    }

                } else {
                    requestPermission();
                }
                break;

            case R.id.mLogOut:

                mCriticalogSharedPreferences.saveData("login_status", "");
                mCriticalogSharedPreferences.saveData("last_address", "");
                mCriticalogSharedPreferences.saveData("punchin_time", "");
                mCriticalogSharedPreferences.saveData("last_address1", "");
                mCriticalogSharedPreferences.saveData("punchout_time", "");
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;

            case R.id.mConnoteEnquiry:
                startActivity(new Intent(this, ConnoteEnquiry.class));
                break;

            case R.id.mPrs:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (startRide.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, PickupActivity.class));
                    } else {
                        Toasty.warning(this, "Please Start the Ride!", Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;
            case R.id.mDrs:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (startRide.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, DRSActivity.class));
                    } else {
                        Toasty.warning(this, "Please Start the Ride!", Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;
            case R.id.mInScan:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, InScanActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;
            case R.id.mBagging:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, com.criticalog.ecritica.MVPBagging.BaggingActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;
            case R.id.mLCCS:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, LinehaulActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;
            case R.id.mSectorPickup:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, SectorPickupActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;

            case R.id.mEdpSdc:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, EDPActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;

            case R.id.mNegativeStatus:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, NegativeStatusActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }

                break;

            case R.id.mConfirmPickup:

                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, PickupConfirmActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mPunch1:
                getLocation("");
                mCriticalogSharedPreferences.saveData("punch_lat", String.valueOf(latitude));
                mCriticalogSharedPreferences.saveData("punch_long", String.valueOf(longitude));
                startActivity(new Intent(this, AttendanceActivity.class));

                break;

            case R.id.mDispatchControHub:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    startActivity(new Intent(this, DispatchToHubActivity.class));
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mNFOPrsDrs:

                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (startRide.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, NfoFragmentActivity.class));
                    } else {
                        Toasty.warning(this, "Please Start the Ride!", Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mSendMailCit:
                if (punchin_restriction.equalsIgnoreCase("true")) {
                    if (startRide.equalsIgnoreCase("true")) {
                        startActivity(new Intent(this, CitPrintMailGenerationActivity.class));
                    } else {
                        Toasty.warning(this, "Please Start the Ride!", Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toasty.warning(this, "Please Punch-In Before Any Activity!", Toast.LENGTH_LONG, true).show();
                }
                break;

            case R.id.mPodScan:
                startActivity(new Intent(this, PodScanActivity.class));
                break;

            case R.id.mCashHandover:
                startActivity(new Intent(this, CashHandoverActivity.class));
                break;

            case R.id.mNewInScan:
                startActivity(new Intent(this, InScanActivityNewDev.class));
                break;

            case R.id.mCreateRound:
                startActivity(new Intent(this, RoundCreationActivity.class));
                break;
            case R.id.mDRSPreparation:
                startActivity(new Intent(this, DRSPreparationActivity.class));
                break;

            case R.id.mAdditionalClaim:
                startActivity(new Intent(this, AdditionalClaimActivity.class));
                break;
        }
    }

    public void callRiderLog() {
        dbHelper.clearLastLatLongTableData();
        getLocationRiderLog();
        int actionNumber = 0;
        String androidOS = Build.VERSION.RELEASE;
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
        if (buttonType.equals("start_ride")) {
            actionNumber = 1;
        } else {
            actionNumber = 7;
        }
        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo(mCriticalogSharedPreferences.getData("bookingno"));
        riderLogRequest.setRoundId(roundNumber);
        riderLogRequest.setClientId(mCriticalogSharedPreferences.getData("client_code"));
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo("0");
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("1");
        riderLogRequest.setmLastKm("0");
        riderLogRequest.setmStartStopKm(kilometerNumber);
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("1");
        riderLogRequest.setOsVersion(androidOS);
        riderLogRequest.setActionNumber(actionNumber);

        dbHelper.insertLastLatLong(userId, String.valueOf(latitude), String.valueOf(longitude));
        mPresenter = new HomePresenterImpl(this, new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl(), new GetHomeInteractorImpl());
        mPresenter.riderLogRequest(token, riderLogRequest);

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Application")
                .setMessage("Are you sure you want to close this App!!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults.length > 0 && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    punchin_restriction = mCriticalogSharedPreferences.getData("punchin_restriction");
                    if (punchin_restriction.equalsIgnoreCase("true")) {

                    } else {

                        Toasty.warning(this, R.string.punch_in_message, Toast.LENGTH_LONG, true).show();
                    }

                    // main logic
                } else {
                    showLocationDialog("");

                }
                break;
        }
    }

    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finish();
                        // redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica");


                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                    /*    dialog.cancel();
                        finish();*/
                        requestPermission();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                //   .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onUpdateNeeded(String updateUrl) {
        android.app.AlertDialog dialogForceUpdate = new android.app.AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update the application.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).create();
        dialogForceUpdate.show();
        dialogForceUpdate.setCanceledOnTouchOutside(false);
        dialogForceUpdate.setCancelable(false);
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseUrl();
        checkUpdate();
        getPunchStatus();
        getFirebaseToken();
    }

    public void getBaseUrl() {
        RequestQueue queue = Volley.newRequestQueue(this);
        try {
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("action", "get_base_url");
            jsonBody.put("user_id", "11337");

            JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, StaticUtils.URLToGetBaseURL, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    //   jsonObject = new JSONObject(response);
                    JSONObject dataObje = null;
                    try {
                        dataObje = response.getJSONObject("data");

                        String URL = dataObje.getString("BASE_URL").toString();

                        StaticUtils.BASE_URL_DYNAMIC = URL;

                        mCriticalogSharedPreferences.saveData("base_url_dynamic", URL);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        StaticUtils.BASE_URL_DYNAMIC = "https://www.ecritica.co/efreightlive/ecritica_api/";
                        Log.e("WHERE_TAG", "JSON Exception");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    //onBackPressed();
                    StaticUtils.BASE_URL_DYNAMIC = "https://www.ecritica.co/efreightlive/ecritica_api/";
                    Log.e("WHERE_TAG", "Volley Error");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    headers.put("token", "HscXR3mh9E0QfKPFOW6DKhkJpT0EpF23N5LOK4ALszIVjDyw7sl7KabJLV8MD4");//put your token here
                    return headers;
                }
            };
            queue.add(jsonOblect);

        } catch (JSONException e) {
            e.printStackTrace();
            StaticUtils.BASE_URL_DYNAMIC = "https://www.ecritica.co/efreightlive/ecritica_api/";
            Log.e("WHERE_TAG", "EXCEPTION");
        }

    }

    public void showUpdateDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Update App or Uninstall and re-Install it from Playstore!!")
                .setCancelable(false)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finish();
                        redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica");
                    }
                })
                .setNegativeButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Application Update is Available");
        alert.show();
    }

    public void getFirebaseToken() {

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            newToken = instanceIdResult.getToken();
            // Log.e("newToken", newToken);
            //  getActivity().getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });
    }


}
