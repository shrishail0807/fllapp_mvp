package com.criticalog.ecritica.MVPHomeScreen;

import com.criticalog.ecritica.MVPHomeScreen.Model.BannerRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.BannerResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.MilometerUpdateResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.PunchStatusResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.StartStopRideResponse;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.TripResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import java.net.NoRouteToHostException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetHomeInteractorImpl implements HomeContract.GetHomeIntractor, HomeContract.RiderLogIntractor, HomeContract.TripIntractor, HomeContract.StartStopRideIntractor, HomeContract.BannerCheckIntractor, HomeContract.MilometerUdateIntractor, HomeContract.PunchStatusInteractor {

    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void homeScreenLoaded(HomeContract.GetHomeIntractor.OnFinishedListener onFinishedListener, ArrayList<String> homeOptions) {

        onFinishedListener.onFinished(homeOptions);
    }

    @Override
    public void riderLogRequest(HomeContract.RiderLogIntractor.OnFinishedListener onFinishedListener, String token, RiderLogRequest riderLogRequest) {

        Call<RiderLogResponse> riderLogResponseCall = services.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void tripRequest(HomeContract.TripIntractor.OnFinishedListener onFinishedListener, String token, TripRequest tripRequest) {
        Call<TripResponse> tripResponseCall = services.tripDataSubmit(tripRequest);
        tripResponseCall.enqueue(new Callback<TripResponse>() {
            @Override
            public void onResponse(Call<TripResponse> call, Response<TripResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<TripResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void startStopRideRequest(HomeContract.StartStopRideIntractor.OnFinishedListener onFinishedListener, String token, StartStopRideRequest startStopRideRequest) {

        Call<StartStopRideResponse> startStopRideResponseCall = services.startStopRide(startStopRideRequest);
        startStopRideResponseCall.enqueue(new Callback<StartStopRideResponse>() {
            @Override
            public void onResponse(Call<StartStopRideResponse> call, Response<StartStopRideResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<StartStopRideResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void bannerRequest(HomeContract.BannerCheckIntractor.OnFinishedListener onFinishedListener, String token, BannerRequest bannerRequest) {
        Call<BannerResponse> bannerResponseCall = services.bannerCheck(bannerRequest);
        bannerResponseCall.enqueue(new Callback<BannerResponse>() {
            @Override
            public void onResponse(Call<BannerResponse> call, Response<BannerResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<BannerResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void milometerUpdateRequest(HomeContract.MilometerUdateIntractor.OnFinishedListener onFinishedListener, String token, MilometerUpdateRequest milometerUpdateRequest) {

        Call<MilometerUpdateResponse> milometerUpdateResponseCall = services.milometerUpdate(milometerUpdateRequest);
        milometerUpdateResponseCall.enqueue(new Callback<MilometerUpdateResponse>() {
            @Override
            public void onResponse(Call<MilometerUpdateResponse> call, Response<MilometerUpdateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<MilometerUpdateResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void punchStatusRequest(HomeContract.PunchStatusInteractor.OnFinishedListener onFinishedListener, String token, PunchStatusRequest punchStatusRequest) {

        Call<PunchStatusResponse> punchStatusResponseCall=services.punchStatus(punchStatusRequest);
        punchStatusResponseCall.enqueue(new Callback<PunchStatusResponse>() {
            @Override
            public void onResponse(Call<PunchStatusResponse> call, Response<PunchStatusResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PunchStatusResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }
}
