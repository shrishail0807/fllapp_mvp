package com.criticalog.ecritica.MVPHomeScreen.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RiderLogRequest implements Serializable {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("booking_no")
    @Expose
    private String bookingNo;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("hub_id")
    @Expose
    private String hubId;
    @SerializedName("round_id")
    @Expose
    private String roundId;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("os_version")
    @Expose
    private String osVersion;
    @SerializedName("m_start_stop_km")
    @Expose
    private String mStartStopKm;
    @SerializedName("m_last_km")
    @Expose
    private String mLastKm;
    @SerializedName("marker_id")
    @Expose
    private String markerId;

    @SerializedName("action_number")
    @Expose
    private Integer actionNumber;

    public Integer getActionNumber() {
        return actionNumber;
    }

    public void setActionNumber(Integer actionNumber) {
        this.actionNumber = actionNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getRoundId() {
        return roundId;
    }

    public void setRoundId(String roundId) {
        this.roundId = roundId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getmStartStopKm() {
        return mStartStopKm;
    }

    public void setmStartStopKm(String mStartStopKm) {
        this.mStartStopKm = mStartStopKm;
    }

    public String getmLastKm() {
        return mLastKm;
    }

    public void setmLastKm(String mLastKm) {
        this.mLastKm = mLastKm;
    }

    public String getMarkerId() {
        return markerId;
    }

    public void setMarkerId(String markerId) {
        this.markerId = markerId;
    }

}
