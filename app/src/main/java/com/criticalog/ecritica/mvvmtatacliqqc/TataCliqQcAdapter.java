package com.criticalog.ecritica.mvvmtatacliqqc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRule;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TataCliqQcAdapter extends RecyclerView.Adapter<TataCliqQcAdapter.QCViewHolder> {

    Context context;
    iAdapterClickTataCliq iAdapterClickListener;
    List<QcRule> qcRuleList;
    DatabaseHelper mDatabaseHelper;
    CriticalogSharedPreferences mCriticalogSharedPreferences;

    public TataCliqQcAdapter(Context context, iAdapterClickTataCliq iAdapterClickListener, List<QcRule> qcRuleList) {
        this.context = context;
        this.iAdapterClickListener = iAdapterClickListener;
        this.qcRuleList = qcRuleList;
        mDatabaseHelper = new DatabaseHelper(context);
        mDatabaseHelper.getWritableDatabase();
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(context);
    }

    @NonNull
    @Override
    public TataCliqQcAdapter.QCViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.qc_tatacliq_itemview, parent, false);
        return new TataCliqQcAdapter.QCViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TataCliqQcAdapter.QCViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.mLabel.setText(qcRuleList.get(position).getQcLabel());

        if (qcRuleList.get(position).getQcType().equalsIgnoreCase("1")) {
            holder.mRadioLay.setVisibility(View.VISIBLE);
            if (qcRuleList.get(position).getQcUpload().equalsIgnoreCase("1")) {
                holder.mImageCapture.setVisibility(View.VISIBLE);
            }

            if (!qcRuleList.get(position).getCaptured_image().equalsIgnoreCase("")) {
                holder.mUploadQcLay.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(qcRuleList.get(position).getCaptured_image(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.mDisplayImage.setImageBitmap(decodedByte);

            } else {
                if (!qcRuleList.get(position).getQcUploadedImg().equalsIgnoreCase("")) {

                    if (qcRuleList.get(position).getQcUploadedImg().toString().contains("http")) {
                        holder.mUploadQcLay.setVisibility(View.VISIBLE);
                        holder.mImageCapture.setVisibility(View.VISIBLE);
                        Picasso.with(context).invalidate(qcRuleList.get(position).getQcUploadedImg());
                        Picasso.with(context).load(qcRuleList.get(position).getQcUploadedImg()).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);
                        Picasso.with(context)
                                .load(qcRuleList.get(position).getQcUploadedImg())
                                .resize(400, 400)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .placeholder(R.drawable.brokenimage)
                                .into(holder.mDisplayImage);
                    }
                }
            }

            holder.mImageCapture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String qcBoolean = "";
                    if (holder.radioMale.isChecked()) {
                        qcBoolean = "yes";

                    }
                    if (holder.radioFemale.isChecked()) {
                        qcBoolean = "no";
                    }
                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), qcBoolean, "", "", "true");
                }
            });

            if (qcRuleList.get(position).getQcBoolResponse().equalsIgnoreCase("1")) {
                holder.radioMale.setChecked(true);
            }
            if (qcRuleList.get(position).getQcBoolResponse().equalsIgnoreCase("0")) {
                holder.radioFemale.setChecked(true);
            }
            if (!qcRuleList.get(position).getQcValueToCheck().equalsIgnoreCase("")) {
                if (qcRuleList.get(position).getQcValueToCheck().toString().contains("http")) {
                    if (!qcRuleList.get(position).getQcValueToCheck().toString().equalsIgnoreCase("")) {
                        holder.mUploadQcLay.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(qcRuleList.get(position).getQcValueToCheck().toString())
                                .resize(400, 400)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .placeholder(R.drawable.brokenimage)
                                .into(holder.mDisplayImage);
                    }

                } else {
                    holder.mSerialNumberCheck.setVisibility(View.VISIBLE);
                    holder.mSerialNumberCheck.setEnabled(false);
                    holder.mSerialNumberCheck.setTextColor(Color.BLACK);
                    holder.mSerialNumberCheck.setText(qcRuleList.get(position).getQcValueToCheck().toString());
                }
            }

            holder.radioMale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), "yes", "", "", "false");
                }
            });

            holder.radioFemale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(),
                            qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(),
                            qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(),
                            qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), "no", "", holder.mSerialNumber.getText().toString(), "false");
                }
            });
        }
        if (qcRuleList.get(position).getQcType().equalsIgnoreCase("2")) {
            holder.mSerialNumber.setVisibility(View.VISIBLE);

            if (!qcRuleList.get(position).getQcInputResponse().equalsIgnoreCase("")) {
                holder.mSerialNumber.setText(qcRuleList.get(position).getQcInputResponse());

                if (!qcRuleList.get(position).getQcInputResponse().toString().equalsIgnoreCase("")) {

                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), "", "", qcRuleList.get(position).getQcInputResponse().toString(), "false");
                } else {
                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), "", "", "", "false");
                }

            }
            if (!qcRuleList.get(position).getQcValueToCheck().equalsIgnoreCase("")) {

                if (!qcRuleList.get(position).getQcValueToCheck().toString().contains("http")) {
                    holder.mSerialNumber.setText(qcRuleList.get(position).getQcValueToCheck());

                    if (!qcRuleList.get(position).getQcInputResponse().toString().equalsIgnoreCase("")) {

                        iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                                qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                                qcRuleList.get(position).getQcBoolResponse(), "", "", qcRuleList.get(position).getQcInputResponse().toString(), "false");
                    } else {
                        iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                                qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                                qcRuleList.get(position).getQcBoolResponse(), "", "", "", "false");
                    }
                }
            }
        }
        holder.mImageCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String qcBoolean = "";
                if (holder.radioMale.isChecked()) {
                    qcBoolean = "yes";
                    // is checked
                }
                if (holder.radioFemale.isChecked()) {
                    qcBoolean = "no";
                    // is checked
                }
                iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                        qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                        qcRuleList.get(position).getQcBoolResponse(), qcBoolean, "", "", "true");
            }
        });
        if (qcRuleList.get(position).getQcType().equalsIgnoreCase("3")) {
            if (!qcRuleList.get(position).getCaptured_image().equalsIgnoreCase("")) {
                holder.mUploadQcLay.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(qcRuleList.get(position).getCaptured_image(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.mDisplayImage.setImageBitmap(decodedByte);

            } else {
                if (!qcRuleList.get(position).getQcUploadedImg().equalsIgnoreCase("")) {

                    if (qcRuleList.get(position).getQcUploadedImg().toString().contains("http")) {
                        holder.mUploadQcLay.setVisibility(View.VISIBLE);
                        holder.mImageCapture.setVisibility(View.VISIBLE);
                        Picasso.with(context).invalidate(qcRuleList.get(position).getQcUploadedImg());
                        Picasso.with(context).load(qcRuleList.get(position).getQcUploadedImg()).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);
                        Picasso.with(context)
                                .load(qcRuleList.get(position).getQcUploadedImg())
                                .resize(400, 400)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .placeholder(R.drawable.brokenimage)
                                .into(holder.mDisplayImage);
                    }

                }
            }
            if (qcRuleList.get(position).getQcUpload().equalsIgnoreCase("1")) {
                holder.mImageCapture.setVisibility(View.VISIBLE);
            }
        }

        holder.mSerialNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {

                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), "", "", s.toString(), "false");
                } else {
                    iAdapterClickListener.qcAdpaterClickTataCliq(position, qcRuleList.get(position).getQcId(), qcRuleList.get(position).getQcLabel(), qcRuleList.get(position).getQcType(), qcRuleList.get(position).getQcValueToCheck(),
                            qcRuleList.get(position).getQcUploadedImg(), qcRuleList.get(position).getQcUpload(), qcRuleList.get(position).getQc_passFactor(), qcRuleList.get(position).getQcInputResponse(),
                            qcRuleList.get(position).getQcBoolResponse(), "", "", "", "false");
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return qcRuleList.size();
    }

    public class QCViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mRadioLay, mUploadQcLay;
        EditText mSerialNumber, mSerialNumberCheck;
        TextView mLabel, mImageCapture;
        ImageView mImageUpload, mDisplayImage;
        RadioButton radioMale, radioFemale;

        public QCViewHolder(@NonNull View itemView) {
            super(itemView);
            mRadioLay = itemView.findViewById(R.id.mRadioLay);
            mUploadQcLay = itemView.findViewById(R.id.mUploadQcLay);
            mSerialNumber = itemView.findViewById(R.id.mSerialNumber);
            mLabel = itemView.findViewById(R.id.mLabel);
            mImageUpload = itemView.findViewById(R.id.mImageUpload);
            mDisplayImage = itemView.findViewById(R.id.mDisplayImage);
            radioMale = itemView.findViewById(R.id.radioMale);
            radioFemale = itemView.findViewById(R.id.radioFemale);
            mImageCapture = itemView.findViewById(R.id.mImageCapture);
            mSerialNumberCheck = itemView.findViewById(R.id.mSerialNumberCheck);
        }
    }
}
