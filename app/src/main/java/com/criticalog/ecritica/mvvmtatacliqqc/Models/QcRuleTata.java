package com.criticalog.ecritica.mvvmtatacliqqc.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QcRuleTata {
    @SerializedName("qc_id")
    @Expose
    private String qcId;
    @SerializedName("qc_item_no")
    @Expose
    private String qcItemNo;
    @SerializedName("qc_label")
    @Expose
    private String qcLabel;
    @SerializedName("qc_type")
    @Expose
    private String qcType;
    @SerializedName("qc_value_to_check")
    @Expose
    private String qcValueToCheck;
    @SerializedName("qc_uploaded_img")
    @Expose
    private String qcUploadedImg;
    @SerializedName("qc_upload")
    @Expose
    private String qcUpload;
    @SerializedName("qc_custom_text")
    @Expose
    private String qcCustomText;
    @SerializedName("qc_input_response")
    @Expose
    private String qcInputResponse;
    @SerializedName("qc_bool_response")
    @Expose
    private String qcBoolResponse;

    @SerializedName("qc_passFactor")
    @Expose
    private String qc_passFactor;

    @SerializedName("selected_boolean")
    @Expose
    private String selected_boolean;

    @SerializedName("selected_text")
    @Expose
    private String selected_text;

    @SerializedName("captured_image")
    @Expose
    private String captured_image;

    public String getSelected_boolean() {
        return selected_boolean;
    }

    public void setSelected_boolean(String selected_boolean) {
        this.selected_boolean = selected_boolean;
    }

    public String getSelected_text() {
        return selected_text;
    }

    public void setSelected_text(String selected_text) {
        this.selected_text = selected_text;
    }

    public String getCaptured_image() {
        return captured_image;
    }

    public void setCaptured_image(String captured_image) {
        this.captured_image = captured_image;
    }

    public String getQc_passFactor() {
        return qc_passFactor;
    }

    public void setQc_passFactor(String qc_passFactor) {
        this.qc_passFactor = qc_passFactor;
    }

    public String getQcId() {
        return qcId;
    }

    public void setQcId(String qcId) {
        this.qcId = qcId;
    }

    public String getQcItemNo() {
        return qcItemNo;
    }

    public void setQcItemNo(String qcItemNo) {
        this.qcItemNo = qcItemNo;
    }

    public String getQcLabel() {
        return qcLabel;
    }

    public void setQcLabel(String qcLabel) {
        this.qcLabel = qcLabel;
    }

    public String getQcType() {
        return qcType;
    }

    public void setQcType(String qcType) {
        this.qcType = qcType;
    }

    public String getQcValueToCheck() {
        return qcValueToCheck;
    }

    public void setQcValueToCheck(String qcValueToCheck) {
        this.qcValueToCheck = qcValueToCheck;
    }

    public String getQcUploadedImg() {
        return qcUploadedImg;
    }

    public void setQcUploadedImg(String qcUploadedImg) {
        this.qcUploadedImg = qcUploadedImg;
    }

    public String getQcUpload() {
        return qcUpload;
    }

    public void setQcUpload(String qcUpload) {
        this.qcUpload = qcUpload;
    }

    public String getQcCustomText() {
        return qcCustomText;
    }

    public void setQcCustomText(String qcCustomText) {
        this.qcCustomText = qcCustomText;
    }

    public String getQcInputResponse() {
        return qcInputResponse;
    }

    public void setQcInputResponse(String qcInputResponse) {
        this.qcInputResponse = qcInputResponse;
    }

    public String getQcBoolResponse() {
        return qcBoolResponse;
    }

    public void setQcBoolResponse(String qcBoolResponse) {
        this.qcBoolResponse = qcBoolResponse;
    }
   /* @SerializedName("qc_id")
    @Expose
    private String qcId;
    @SerializedName("qc_label")
    @Expose
    private String qcLabel;
    @SerializedName("qc_type")
    @Expose
    private String qcType;
    @SerializedName("qc_value_to_check")
    @Expose
    private String qcValueToCheck;
    @SerializedName("qc_uploaded_img")
    @Expose
    private String qcUploadedImg;
    @SerializedName("qc_upload")
    @Expose
    private String qcUpload;
    @SerializedName("qc_passFactor")
    @Expose
    private String qcPassFactor;
    @SerializedName("qc_input_response")
    @Expose
    private String qcInputResponse;
    @SerializedName("qc_bool_response")
    @Expose
    private String qcBoolResponse;
    @SerializedName("selected_boolean")
    @Expose
    private String selectedBoolean;
    @SerializedName("selected_text")
    @Expose
    private String selectedText;
    @SerializedName("captured_image")
    @Expose
    private String capturedImage;

    public String getQcId() {
        return qcId;
    }

    public void setQcId(String qcId) {
        this.qcId = qcId;
    }

    public String getQcLabel() {
        return qcLabel;
    }

    public void setQcLabel(String qcLabel) {
        this.qcLabel = qcLabel;
    }

    public String getQcType() {
        return qcType;
    }

    public void setQcType(String qcType) {
        this.qcType = qcType;
    }

    public String getQcValueToCheck() {
        return qcValueToCheck;
    }

    public void setQcValueToCheck(String qcValueToCheck) {
        this.qcValueToCheck = qcValueToCheck;
    }

    public String getQcUploadedImg() {
        return qcUploadedImg;
    }

    public void setQcUploadedImg(String qcUploadedImg) {
        this.qcUploadedImg = qcUploadedImg;
    }

    public String getQcUpload() {
        return qcUpload;
    }

    public void setQcUpload(String qcUpload) {
        this.qcUpload = qcUpload;
    }

    public String getQcPassFactor() {
        return qcPassFactor;
    }

    public void setQcPassFactor(String qcPassFactor) {
        this.qcPassFactor = qcPassFactor;
    }

    public String getQcInputResponse() {
        return qcInputResponse;
    }

    public void setQcInputResponse(String qcInputResponse) {
        this.qcInputResponse = qcInputResponse;
    }

    public String getQcBoolResponse() {
        return qcBoolResponse;
    }

    public void setQcBoolResponse(String qcBoolResponse) {
        this.qcBoolResponse = qcBoolResponse;
    }

    public String getSelectedBoolean() {
        return selectedBoolean;
    }

    public void setSelectedBoolean(String selectedBoolean) {
        this.selectedBoolean = selectedBoolean;
    }

    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }

    public String getCapturedImage() {
        return capturedImage;
    }

    public void setCapturedImage(String capturedImage) {
        this.capturedImage = capturedImage;
    }*/

}

