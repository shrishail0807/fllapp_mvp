package com.criticalog.ecritica.mvvmtatacliqqc;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRule;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.databinding.ActivityTataCliqQcBinding;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataRequest;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataResponse;
import com.criticalog.ecritica.mvvmtatacliqqc.viewmodel.TataCliqViewModel;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class TataCliqQcActivity extends AppCompatActivity implements iAdapterClickTataCliq {
    TataCliqQcAdapter mQcAdapter;
    private List<QcRule> getQcRules;
    File photoFile = null;
    private static int CAMERA_REQUEST_QC = 1890;
    private String base64CapturedQCImage = "";
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String itemId, userId;
    private TataCliqViewModel tataCliqViewModel;
    private SimpleArcDialog mProgressBar;
    private Dialog qcNDCUpdateDialog;
    int position1;
    String qcId1, qcLabel1, qcType1, qc_value_to_check1, qc_uploaded_img1, qc_upload1, qc_passFactor1, qc_input_response1,
            qc_bool_response1, qcBolean1, image1, customText1, imageCaptureFlag1;
    ActivityTataCliqQcBinding activityTataCliqQcBinding;
    private String docket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityTataCliqQcBinding = DataBindingUtil.setContentView(this, R.layout.activity_tata_cliq_qc);
        tataCliqViewModel = ViewModelProviders.of(this).get(TataCliqViewModel.class);
        tataCliqViewModel.init();
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);
        itemId = mCriticalogSharedPreferences.getData("item_id");
        userId = mCriticalogSharedPreferences.getData("userId");
        docket = mCriticalogSharedPreferences.getData("docket_check_list");

        activityTataCliqQcBinding.mTopBarText.setText("QC Check");
        getQCRules(itemId, "");
        activityTataCliqQcBinding.mTvBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGoBackDialog("");
            }
        });

        activityTataCliqQcBinding.mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getQcRules != null) {
                    for (int i = 0; i < getQcRules.size(); i++) {
                        if (getQcRules.get(i).getQcType().equalsIgnoreCase("1")) {
                            if (getQcRules.get(i).getQcBoolResponse().equalsIgnoreCase("")) {
                                if (getQcRules.get(i).getSelected_boolean().equalsIgnoreCase("")) {

                                    Toasty.error(TataCliqQcActivity.this, "Select Yes or No Properly!!", Toast.LENGTH_SHORT, true).show();
                                    activityTataCliqQcBinding.mQCRV.scrollToPosition(i);
                                    mQcAdapter.notifyDataSetChanged();
                                    return;
                                }
                            }
                        } else if (getQcRules.get(i).getQcType().equalsIgnoreCase("2")) {

                            if (getQcRules.get(i).getQcValueToCheck().equalsIgnoreCase("")) {
                                if (getQcRules.get(i).getSelected_text().equalsIgnoreCase("")) {

                                    Toasty.error(TataCliqQcActivity.this, "Please Enter Valid Details!!", Toast.LENGTH_SHORT, true).show();

                                    activityTataCliqQcBinding.mQCRV.scrollToPosition(i);
                                    mQcAdapter.notifyDataSetChanged();
                                    return;
                                }
                            }

                        } else if (getQcRules.get(i).getQcType().equalsIgnoreCase("3")) {
                            if (getQcRules.get(i).getQcUploadedImg().toString().equalsIgnoreCase("")) {
                                if (getQcRules.get(i).getCaptured_image().equalsIgnoreCase("")) {
                                    //Toast.makeText(TataCliqQcActivity.this, "Please Capture The Image!!", Toast.LENGTH_SHORT).show();
                                    Toasty.error(TataCliqQcActivity.this, "Please Capture The Image!!", Toast.LENGTH_SHORT, true).show();

                                    activityTataCliqQcBinding.mQCRV.scrollToPosition(i);
                                    mQcAdapter.notifyDataSetChanged();
                                    return;
                                }
                            }
                        }
                    }
                    QCTataRequest qcTataRequest = new QCTataRequest();
                    qcTataRequest.setAction("qc_rule_update_new");
                    qcTataRequest.setUserId(userId);
                    qcTataRequest.setDocketNo(docket);
                    qcTataRequest.setQcRules(getQcRules);
                    tataCliqViewModel.tataQcUpdate(qcTataRequest);
                    mProgressBar.show();
                }
            }
        });

        tataCliqViewModel.qcPassFailLiveData().observe(this, new Observer<QCPassFailResponse>() {
            @Override
            public void onChanged(QCPassFailResponse qcPassFailResponse) {
                mProgressBar.dismiss();
                if (qcPassFailResponse.getStatus() == 200) {
                    if (qcNDCUpdateDialog != null) {
                        if (qcNDCUpdateDialog.isShowing()) {
                            qcNDCUpdateDialog.dismiss();
                        }
                    }
                    if (qcPassFailResponse.getMessage().equalsIgnoreCase("NDC updated successfully")) {
                        Intent newIntent = new Intent(TataCliqQcActivity.this, PickupActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);
                    } else if (qcPassFailResponse.getMessage().equalsIgnoreCase("QC passed updated successfully")) {
                        finish();
                    }

                    Toasty.success(TataCliqQcActivity.this, qcPassFailResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                } else {
                    Toasty.warning(TataCliqQcActivity.this, qcPassFailResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
            }
        });
        tataCliqViewModel.qcCheckSubLiveData().observe(this, new Observer<QcRulesCheckResponse>() {
            @Override
            public void onChanged(QcRulesCheckResponse qcRulesCheckResponse) {
                mProgressBar.dismiss();
                if (qcRulesCheckResponse.getStatus() == 200) {

                    mCriticalogSharedPreferences.saveData("coming_from_qc", "true");
                    Toasty.success(TataCliqQcActivity.this, qcRulesCheckResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    if (qcRulesCheckResponse.getData().getQualityCheck().equalsIgnoreCase("failed")) {
                        qcNDCDialog();
                    } else {
                        QCNdcUpdate("1");

                    }

                } else {
                    Toasty.warning(TataCliqQcActivity.this, qcRulesCheckResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
            }
        });
        tataCliqViewModel.tataQcUpdateLiveData().observe(this, new Observer<QCTataResponse>() {
            @Override
            public void onChanged(QCTataResponse qcTataResponse) {
                mProgressBar.dismiss();
                Toast.makeText(TataCliqQcActivity.this, qcTataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                if (qcTataResponse.getStatus() == 200) {
                    // startActivity(new Intent(TataCliqQcActivity.this, BarcodeScanActivity.class));
                    QcRulesCheckRequest qcRulesCheckRequest = new QcRulesCheckRequest();
                    qcRulesCheckRequest.setAction("qc_rule_check");
                    qcRulesCheckRequest.setUserId(userId);
                    qcRulesCheckRequest.setQcItemId(itemId);
                    // qcRulesCheckRequest.setLatitude(String.valueOf(latitude));
                    //qcRulesCheckRequest.setLongitude(String.valueOf(longitude));
                    tataCliqViewModel.qcRulesCheck(qcRulesCheckRequest);
                    mProgressBar.show();
                    //  finish();
                }
            }
        });

        tataCliqViewModel.qcRulesLiveData().observe(this, new Observer<QCRulesResponse>() {
            @Override
            public void onChanged(QCRulesResponse qcRulesResponse) {
                mProgressBar.dismiss();
                mQcAdapter = new TataCliqQcAdapter(TataCliqQcActivity.this, TataCliqQcActivity.this, qcRulesResponse.getData().getQcRules());
                activityTataCliqQcBinding.mQCRV.setLayoutManager(new LinearLayoutManager(TataCliqQcActivity.this));
                activityTataCliqQcBinding.mQCRV.setAdapter(mQcAdapter);
                mQcAdapter.notifyDataSetChanged();

                getQcRules = qcRulesResponse.getData().getQcRules();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

    }

    public void qcNDCDialog() {
        TextView mConfirm, mUpdate;
        //Dialog Manual Entry
        qcNDCUpdateDialog = new Dialog(TataCliqQcActivity.this);
        qcNDCUpdateDialog.setContentView(R.layout.qc_ndc_dialog);
        mConfirm = qcNDCUpdateDialog.findViewById(R.id.mConfirm);
        mUpdate = qcNDCUpdateDialog.findViewById(R.id.mUpdate);

        qcNDCUpdateDialog.setCancelable(false);
        qcNDCUpdateDialog.show();

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qcNDCUpdateDialog.dismiss();

            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QCNdcUpdate("0");
            }
        });
    }

    public void QCNdcUpdate(String passFail) {
        QCPassFailRequest qcPassFailRequest = new QCPassFailRequest();
        qcPassFailRequest.setAction("qc_pass_fail_update");
        qcPassFailRequest.setUserId(userId);
        qcPassFailRequest.setQcItemId(itemId);
        qcPassFailRequest.setQcPassFail(passFail);
        qcPassFailRequest.setUnique_id(mCriticalogSharedPreferences.getData("unique_id"));
        qcPassFailRequest.setDocket_no(docket);

        tataCliqViewModel.qcPassFail(qcPassFailRequest);
        mProgressBar.show();


    }

    @Override
    public void onBackPressed() {
        showGoBackDialog("");
    }

    public void showGoBackDialog(String close) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("All Selected Details will be deleted!! Are you Sure want to Go Back.");
        builder1.setTitle("QC Details");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //startActivity(new Intent(TataCliqQcActivity.this, PickupActivity.class));

                        Intent a = new Intent(TataCliqQcActivity.this, PickupActivity.class);
                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(a);
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    public void getQCRules(String itemId, String fromWhere) {

        QCRulesRequest qcRulesRequest = new QCRulesRequest();
        qcRulesRequest.setAction("get_qc_rules_new");
        qcRulesRequest.setUserId(userId);
        qcRulesRequest.setQcItemId(itemId);
        qcRulesRequest.setLatitude(String.valueOf(""));
        qcRulesRequest.setLongitude(String.valueOf(""));
        tataCliqViewModel.qcRules(qcRulesRequest);
        mProgressBar.show();
    }

    @Override
    public void qcAdpaterClickTataCliq(int position, String qcId, String qcLabel,
                                       String qcType, String qc_value_to_check, String qc_uploaded_img,
                                       String qc_upload, String qc_passFactor, String qc_input_response,
                                       String qc_bool_response, String qcBolean, String image,
                                       String customText, String imageCaptureFlag) {

        qcId1 = qcId;
        position1 = position;
        qcLabel1 = qcLabel;
        qcType1 = qcType;
        qc_value_to_check1 = qc_value_to_check;
        qc_uploaded_img1 = qc_uploaded_img;
        qc_upload1 = qc_upload;
        qc_passFactor1 = qc_passFactor;
        qc_input_response1 = qc_input_response;
        qc_bool_response1 = qc_bool_response;
        qcBolean1 = qcBolean;
        image1 = image;
        customText1 = customText;
        imageCaptureFlag1 = imageCaptureFlag;

        if (qcType.equalsIgnoreCase("3")) {


            if (imageCaptureFlag.equalsIgnoreCase("true")) {
                openCameraIntent(CAMERA_REQUEST_QC);
            }
        }

        if (qcType.equalsIgnoreCase("1")) {
            if (qc_upload.equalsIgnoreCase("1")) {

                if (imageCaptureFlag.equalsIgnoreCase("true")) {
                    openCameraIntent(CAMERA_REQUEST_QC);
                }

            }
        }

        QcRule qcRule = new QcRule();
        qcRule.setQcId(qcId);
        qcRule.setQcLabel(qcLabel);
        qcRule.setQcType(qcType);
        qcRule.setQcValueToCheck(qc_value_to_check);
        qcRule.setQcUploadedImg(qc_uploaded_img);
        qcRule.setQcUpload(qc_upload);
        qcRule.setQc_passFactor(qc_passFactor);
        qcRule.setQcBoolResponse(qcBolean);
        qcRule.setQcInputResponse(qc_input_response);
        qcRule.setSelected_boolean(qcBolean);
        qcRule.setSelected_text(customText);
        qcRule.setCaptured_image(image);

        getQcRules.set(position, qcRule);
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1890) {
            File absoluteFile = photoFile.getAbsoluteFile();
            if (resultCode == 0) {
                Toast.makeText(this, "You cannot Press Back!!", Toast.LENGTH_SHORT).show();
            } else {
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CapturedQCImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                    QcRule qcRule = new QcRule();
                    qcRule.setQcId(qcId1);
                    qcRule.setQcLabel(qcLabel1);
                    qcRule.setQcType(qcType1);
                    qcRule.setQcValueToCheck(qc_value_to_check1);
                    qcRule.setQcUploadedImg(qc_uploaded_img1);
                    qcRule.setQcUpload(qc_upload1);
                    qcRule.setQc_passFactor(qc_passFactor1);
                    qcRule.setQcBoolResponse(qcBolean1);
                    qcRule.setQcInputResponse(qc_input_response1);
                    qcRule.setSelected_boolean(qcBolean1);
                    qcRule.setSelected_text(customText1);
                    qcRule.setCaptured_image(base64CapturedQCImage);

                    getQcRules.set(position1, qcRule);
                    mQcAdapter = new TataCliqQcAdapter(this, TataCliqQcActivity.this, getQcRules);
                    activityTataCliqQcBinding.mQCRV.setLayoutManager(new LinearLayoutManager(this));
                    mQcAdapter.notifyDataSetChanged();

                }
            }
        }
    }
}
