package com.criticalog.ecritica.mvvmtatacliqqc.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataRequest;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TataCliqQcRepo {

    private RestServices mRestServices;
    private MutableLiveData<QCTataResponse> qcTataResponseMutableLiveData;
    private MutableLiveData<QCRulesResponse> qcRulesResponseMutableLiveData;
    private MutableLiveData<QcRulesCheckResponse> qcRulesCheckResponseMutableLiveData;
    private MutableLiveData<QCPassFailResponse> qcPassFailResponseMutableLiveData;


    public TataCliqQcRepo() {
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        qcTataResponseMutableLiveData = new MutableLiveData<>();
        qcRulesResponseMutableLiveData = new MutableLiveData<>();
        qcRulesCheckResponseMutableLiveData = new MutableLiveData<>();
        qcPassFailResponseMutableLiveData = new MutableLiveData<>();


    }

    public void getQcPassFail(QCPassFailRequest qcPassFailRequest) {
        Call<QCPassFailResponse> qcPassFailResponseCall = mRestServices.qcNDCUpdate(qcPassFailRequest);
        qcPassFailResponseCall.enqueue(new Callback<QCPassFailResponse>() {
            @Override
            public void onResponse(Call<QCPassFailResponse> call, Response<QCPassFailResponse> response) {
                qcPassFailResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<QCPassFailResponse> call, Throwable t) {
                qcPassFailResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getQcCheck(QcRulesCheckRequest qcRulesCheckRequest) {
        Call<QcRulesCheckResponse> qcRulesCheckResponseCall = mRestServices.qcCheckSubmit(qcRulesCheckRequest);
        qcRulesCheckResponseCall.enqueue(new Callback<QcRulesCheckResponse>() {
            @Override
            public void onResponse(Call<QcRulesCheckResponse> call, Response<QcRulesCheckResponse> response) {
                qcRulesCheckResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<QcRulesCheckResponse> call, Throwable t) {
                qcRulesCheckResponseMutableLiveData.setValue(null);
            }
        });
    }

    public void getSubmitTataQCAll(QCTataRequest qcTataRequest) {
        Call<QCTataResponse> qcTataResponseCall = mRestServices.qcTataUpate(qcTataRequest);

        qcTataResponseCall.enqueue(new Callback<QCTataResponse>() {
            @Override
            public void onResponse(Call<QCTataResponse> call, Response<QCTataResponse> response) {
                qcTataResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<QCTataResponse> call, Throwable t) {
                qcTataResponseMutableLiveData.setValue(null);
            }
        });

    }

    public void getQcRules(QCRulesRequest qcRulesRequest) {
        Call<QCRulesResponse> qcRulesResponseCall = mRestServices.getQcRules(qcRulesRequest);

        qcRulesResponseCall.enqueue(new Callback<QCRulesResponse>() {
            @Override
            public void onResponse(Call<QCRulesResponse> call, Response<QCRulesResponse> response) {
                qcRulesResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<QCRulesResponse> call, Throwable t) {
                qcRulesResponseMutableLiveData.setValue(null);
            }
        });

    }

    public LiveData<QCTataResponse> getQcUpdateLiveData() {
        return qcTataResponseMutableLiveData;
    }

    public LiveData<QCRulesResponse> getQcRulesLiveData() {
        return qcRulesResponseMutableLiveData;
    }

    public LiveData<QcRulesCheckResponse> getQcRulesCheckLiveData() {
        return qcRulesCheckResponseMutableLiveData;
    }

    public LiveData<QCPassFailResponse> getQcPassFailLiveData() {
        return qcPassFailResponseMutableLiveData;
    }
}
