package com.criticalog.ecritica.mvvmtatacliqqc.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataRequest;
import com.criticalog.ecritica.mvvmtatacliqqc.Models.QCTataResponse;
import com.criticalog.ecritica.mvvmtatacliqqc.repository.TataCliqQcRepo;

public class TataCliqViewModel extends AndroidViewModel {
    private TataCliqQcRepo tataCliqQcRepo;
    private LiveData<QCTataResponse> qcUpadetLiveLiveData;
    private LiveData<QCRulesResponse> qcRulesResponseLiveData;
    private LiveData<QcRulesCheckResponse> qcRulesCheckResponseLiveData;
    private LiveData<QCPassFailResponse> qcPassFailResponseLiveData;

    public TataCliqViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        tataCliqQcRepo = new TataCliqQcRepo();
        qcUpadetLiveLiveData = tataCliqQcRepo.getQcUpdateLiveData();
        qcRulesResponseLiveData = tataCliqQcRepo.getQcRulesLiveData();
        qcRulesCheckResponseLiveData = tataCliqQcRepo.getQcRulesCheckLiveData();
        qcPassFailResponseLiveData = tataCliqQcRepo.getQcPassFailLiveData();
    }

    public void tataQcUpdate(QCTataRequest qcTataRequest) {
        tataCliqQcRepo.getSubmitTataQCAll(qcTataRequest);
    }

    public LiveData<QCTataResponse> tataQcUpdateLiveData() {
        return qcUpadetLiveLiveData;
    }

    public void qcRules(QCRulesRequest qcRulesRequest) {
        tataCliqQcRepo.getQcRules(qcRulesRequest);
    }

    public LiveData<QCRulesResponse> qcRulesLiveData() {
        return qcRulesResponseLiveData;
    }

    public void qcRulesCheck(QcRulesCheckRequest qcRulesCheckRequest) {
        tataCliqQcRepo.getQcCheck(qcRulesCheckRequest);
    }

    public LiveData<QcRulesCheckResponse> qcCheckSubLiveData() {
        return qcRulesCheckResponseLiveData;
    }


    public void qcPassFail(QCPassFailRequest qcPassFailRequest) {
        tataCliqQcRepo.getQcPassFail(qcPassFailRequest);
    }

    public LiveData<QCPassFailResponse> qcPassFailLiveData() {
        return qcPassFailResponseLiveData;
    }

}
