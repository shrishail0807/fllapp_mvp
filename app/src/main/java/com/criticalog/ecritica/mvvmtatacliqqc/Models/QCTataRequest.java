package com.criticalog.ecritica.mvvmtatacliqqc.Models;

import com.criticalog.ecritica.MVPBarcodeScan.model.QcRule;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QCTataRequest {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("qc_rules")
    @Expose
    private List<QcRule> qcRules;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public List<QcRule> getQcRules() {
        return qcRules;
    }

    public void setQcRules(List<QcRule> qcRules) {
        this.qcRules = qcRules;
    }

}
