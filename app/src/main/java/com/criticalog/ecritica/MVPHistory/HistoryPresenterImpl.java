package com.criticalog.ecritica.MVPHistory;

import com.criticalog.ecritica.MVPHistory.Model.HistoryRequest;
import com.criticalog.ecritica.MVPHistory.Model.HistoryResponse;

public class HistoryPresenterImpl implements HistoryContract.presenter, HistoryContract.GetHistoryIntractor.OnFinishedListener {
    private HistoryContract.MainView mainView;
    private HistoryContract.GetHistoryIntractor getHistoryIntractor;

    public HistoryPresenterImpl(HistoryContract.MainView mainView, HistoryContract.GetHistoryIntractor getHistoryIntractor) {
        this.mainView = mainView;
        this.getHistoryIntractor = getHistoryIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void loadHistory(HistoryRequest historyRequest, String token) {
        if (mainView != null) {
            mainView.showProgress();
        }

        getHistoryIntractor.historySuccessful(this,token, historyRequest);
    }

    @Override
    public void onFinished(HistoryResponse historyResponse) {

        if (mainView != null) {
            mainView.setHistoryDataToViews(historyResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
