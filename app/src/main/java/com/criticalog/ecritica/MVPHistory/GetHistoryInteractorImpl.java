package com.criticalog.ecritica.MVPHistory;

import com.criticalog.ecritica.MVPHistory.Model.HistoryRequest;
import com.criticalog.ecritica.MVPHistory.Model.HistoryResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetHistoryInteractorImpl implements HistoryContract.GetHistoryIntractor {
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void historySuccessful(OnFinishedListener onFinishedListener, String token, HistoryRequest historyRequest) {
        Call<HistoryResponse> historyResponseCall = services.history(historyRequest);
        historyResponseCall.enqueue(new Callback<HistoryResponse>() {
            @Override
            public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<HistoryResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
