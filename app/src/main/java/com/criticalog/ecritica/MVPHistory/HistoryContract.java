package com.criticalog.ecritica.MVPHistory;

import com.criticalog.ecritica.MVPHistory.Model.HistoryRequest;
import com.criticalog.ecritica.MVPHistory.Model.HistoryResponse;

public interface HistoryContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void loadHistory(HistoryRequest historyRequest, String token);

        //void LoginRequestToServer(LoginPostData loginPostData);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setHistoryDataToViews(HistoryResponse historyResponse);

        void onResponseFailure(Throwable throwable);
    }
    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetHistoryIntractor {

        interface OnFinishedListener {
            void onFinished(HistoryResponse historyResponse);
            void onFailure(Throwable t);
        }
        void historySuccessful(HistoryContract.GetHistoryIntractor.OnFinishedListener onFinishedListener,String token, HistoryRequest historyRequest);
    }
}
