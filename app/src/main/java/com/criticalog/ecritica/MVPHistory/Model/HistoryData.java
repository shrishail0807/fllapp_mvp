package com.criticalog.ecritica.MVPHistory.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryData {
    @SerializedName("records")
    @Expose
    private Integer records;
    @SerializedName("list")
    @Expose
    private List<HistoryList> list = null;

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public List<HistoryList> getList() {
        return list;
    }

    public void setList(List<HistoryList> list) {
        this.list = list;
    }
}
