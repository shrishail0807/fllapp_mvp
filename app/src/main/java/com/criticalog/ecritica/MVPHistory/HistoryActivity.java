package com.criticalog.ecritica.MVPHistory;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPHistory.Model.HistoryList;
import com.criticalog.ecritica.MVPHistory.Model.HistoryRequest;
import com.criticalog.ecritica.MVPHistory.Model.HistoryResponse;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPTATCheck.TATCheckActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PaginationScrollListener;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity implements HistoryContract.MainView, View.OnClickListener {
    private HistoryContract.presenter mPresenter;
    RecyclerView mHistoryRV;
    private SimpleArcDialog mProgressBar;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token,prsdrs;
    String history_pickup_delivery;
    private TextView mTopBarText;
    private ImageView mTvBackbutton;
    private LinearLayoutManager linearLayoutManager;
    boolean isScrolling = false;
    private int records = 0;
    private HistoryAdapter historyAdapter;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;

    List<HistoryList> totalHistory = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_mvp);

        mHistoryRV = findViewById(R.id.mHistoryRV);
        mTopBarText = findViewById(R.id.mTopBarText);
        mTvBackbutton = findViewById(R.id.mTvBackbutton);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        mTvBackbutton.setOnClickListener(this);

        mTopBarText.setText("HISTORY");


        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC=mCriticalogSharedPreferences.getData("base_url_dynamic");

        StaticUtils.TOKEN=mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=mCriticalogSharedPreferences.getData("distance_api_key");
        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");
        prsdrs=mCriticalogSharedPreferences.getData("history");

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        historyAdapter = new HistoryAdapter(totalHistory,prsdrs);
        mHistoryRV.setLayoutManager(linearLayoutManager);
        mHistoryRV.setAdapter(historyAdapter);

        historyApiCall();

        pagination();

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(HistoryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HistoryActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HistoryActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(HistoryActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void pagination() {
        mHistoryRV.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isScrolling = true;
                historyApiCall();
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setHistoryDataToViews(HistoryResponse historyResponse) {
        if (historyResponse != null) {
            if (historyResponse.getStatus() == 200) {
                if (historyResponse != null) {
                    records = historyResponse.getData().getRecords();
                    List<HistoryList> historyList = historyResponse.getData().getList();

                    totalHistory.addAll(historyList);
                    historyAdapter.notifyDataSetChanged();

                }
            } else {
                Toast.makeText(HistoryActivity.this, historyResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onResponseFailure(Throwable throwable) {

        Toast.makeText(HistoryActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTvBackbutton:
                Intent home = new Intent(this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(home);
                break;
        }
    }

    public void historyApiCall() {
        HistoryRequest historyRequest = new HistoryRequest();
        historyRequest.setAction("prs_drs_history");
        historyRequest.setUserId(userId);
        historyRequest.setRecords(String.valueOf(records));
        historyRequest.setType(mCriticalogSharedPreferences.getData("history"));
        historyRequest.setLatitude(String.valueOf(latitude));
        historyRequest.setLongitude(String.valueOf(longitude));
        Log.e("HISTORY", mCriticalogSharedPreferences.getData("history"));

        mPresenter = new HistoryPresenterImpl(this, new GetHistoryInteractorImpl());
        mPresenter.loadHistory(historyRequest, token);
    }
}
