package com.criticalog.ecritica.MVPHistory.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryList {
    @SerializedName("task_id")
    @Expose
    private String taskId;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("text")
    @Expose
    private String text;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
