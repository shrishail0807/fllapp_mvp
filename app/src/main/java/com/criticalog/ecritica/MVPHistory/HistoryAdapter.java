package com.criticalog.ecritica.MVPHistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.MVPHistory.Model.HistoryList;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {
    private List<HistoryList> historyDataArrayList = new ArrayList<>();
    private String prsdrs;


    public HistoryAdapter(List<HistoryList> history_data,String prsdrs) {
        this.historyDataArrayList = history_data;
        this.prsdrs=prsdrs;
    }


    @Override
    public HistoryAdapter.HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_history, parent, false);
        return new HistoryAdapter.HistoryViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(HistoryAdapter.HistoryViewHolder holder, int position) {

        final HistoryList data = historyDataArrayList.get(position);

        holder.mtask_status.setText("Status: "+data.getText());
        holder.mClientCode.setText("Client Code: "+data.getClientCode());
        holder.mDate.setText("Date and Time :"+data.getDatetime());
        holder.mPinCode.setText("Pincode :"+data.getPincode());
        if(prsdrs.equalsIgnoreCase("prs"))
        {
            holder.mtaskid.setText("PRS : "+data.getTaskId());
        }else {
            holder.mtaskid.setText("DRS : "+data.getTaskId());
        }
        if(data.getMode()!=null)
        {
            holder.mPayType.setText("Pickup Type :"+data.getMode());
        }else {
            holder.mPayType.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return historyDataArrayList.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        private TextView mtaskid, mtask_status,mClientCode,mPinCode,mDate,mPayType;

        public HistoryViewHolder(View itemView) {
            super(itemView);
            mtaskid = (TextView) itemView.findViewById(R.id.xtv_taskid);
            mtask_status = (TextView) itemView.findViewById(R.id.xtv_task_status);
            mClientCode=itemView.findViewById(R.id.mClientCode);
            mPinCode=itemView.findViewById(R.id.mPinCode);
            mDate=itemView.findViewById(R.id.mDate);
            mPayType=itemView.findViewById(R.id.mPayType);

        }
    }
}
