package com.criticalog.ecritica.Activities;


import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Adapters.ConnoteTrackDtAdpater;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataEvent;
import com.criticalog.ecritica.ModelClasses.ConsigneeDeatils;
import com.criticalog.ecritica.ModelClasses.ConsignerDetails;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.EventResponse;

import java.util.ArrayList;
import java.util.List;


@SuppressLint("ValidFragment")
public class ConnoteTrackFragment extends Fragment {


    ConsignerDetails consignerDetails;
    ConsigneeDeatils consigneeDeatils;
    List<DataEvent> totaleventResponseArrayList;
    View view;
    TextView consignorName, consignorAddress, consignorCity, consignorState, consignorPin;
    TextView consigneeName, consigneeAddress, consigneeCity, consigneeState, consigneePin;
    RecyclerView event_rv;


    public ConnoteTrackFragment(ConsignerDetails consignerDetails, ConsigneeDeatils consigneeDeatils, List<DataEvent> totaleventResponseArrayList) {
        this.consignerDetails = consignerDetails;
        this.consigneeDeatils = consigneeDeatils;
        this.totaleventResponseArrayList = totaleventResponseArrayList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_connote_track, container, false);
        consignorName = view.findViewById(R.id.consignor_name);
        consignorAddress = view.findViewById(R.id.consignor_address);
        consignorCity = view.findViewById(R.id.consignor_city);
        consignorState = view.findViewById(R.id.consignor_state);
        consignorPin = view.findViewById(R.id.consignor_pin);

        consigneeName = view.findViewById(R.id.consignee_name);
        consigneeAddress = view.findViewById(R.id.consignee_address);
        consigneeCity = view.findViewById(R.id.consignee_city);
        consigneeState = view.findViewById(R.id.consignee_state);
        consigneePin = view.findViewById(R.id.consignee_pincode);

        event_rv = view.findViewById(R.id.event_rv);

        consignorName.setText("Name: "+consignerDetails.getName());
        consignorAddress.setText("Address: "+consignerDetails.getAddress());
        consignorCity.setText("City: "+consignerDetails.getCity());
        consignorState.setText("State: "+consignerDetails.getState());
        consignorPin.setText("Pincode: "+consignerDetails.getPincode());

        consigneeName.setText("Name: "+consigneeDeatils.getName());
        consigneeAddress.setText("Address: "+consigneeDeatils.getAddress());
        consigneeCity.setText("City: "+consigneeDeatils.getCity());
        consigneeState.setText("State: "+consigneeDeatils.getState());
        consigneePin.setText("Pincode: "+consigneeDeatils.getPincode());


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        event_rv.setLayoutManager(mLayoutManager);


        event_rv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        ConnoteTrackDtAdpater adpater = new ConnoteTrackDtAdpater(totaleventResponseArrayList, getActivity());
        event_rv.setAdapter(adpater);


        // Inflate the layout for this fragment
        return view;
    }

}
