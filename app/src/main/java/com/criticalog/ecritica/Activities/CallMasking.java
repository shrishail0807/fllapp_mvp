package com.criticalog.ecritica.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient1;
import com.criticalog.ecritica.Rest.RestServices1;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.model.CallRequest;
import com.criticalog.ecritica.model.CallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallMasking extends AppCompatActivity {
    private TextView mCall;
    private ImageView xtv_backbutton;
    private RestServices1 mRestServices1;
    ProgressDialog progressDialog;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String token;
    EditText mMobileNumber;
    ImageView mCallNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_masking);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait we are connecting the Call....");
        progressDialog.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        mRestServices1 = RestClient1.getRetrofitInstance().create(RestServices1.class);

        mMobileNumber = findViewById(R.id.mMobileNumber);
        mCallNumber = findViewById(R.id.mCallNumber);

        token = mCriticalogSharedPreferences.getData("token");

        xtv_backbutton=findViewById(R.id.xtv_backbutton);

        xtv_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

       /* mCall=findViewById(R.id.mCall);
        xtv_backbutton=findViewById(R.id.xtv_backbutton);

        xtv_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        mCallNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobileNumber = mMobileNumber.getText().toString();
                if (mobileNumber.equalsIgnoreCase("")) {

                    Toast.makeText(CallMasking.this, "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() != 11) {
                    Toast.makeText(CallMasking.this, "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
                } else {
                    CallRequest callRequest = new CallRequest();
                    callRequest.setAction("call_masking");
                    callRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
                    callRequest.setTo_number(mobileNumber);
                    progressDialog.show();
                    Call<CallResponse> callResponseCall = mRestServices1.callInitiate(callRequest);
                    callResponseCall.enqueue(new Callback<CallResponse>() {
                        @Override
                        public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                            if (response.body().getStatus() == 200) {
                                final Handler handler = new Handler(Looper.getMainLooper());
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                        //Do something after 100ms
                                    }
                                }, 2000);
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(CallMasking.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CallResponse> call, Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }

               /* Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9513555079"));
                startActivity(intent);*/
            }
        });
    }

}
