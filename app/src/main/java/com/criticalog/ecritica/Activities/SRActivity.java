package com.criticalog.ecritica.Activities;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.criticalog.ecritica.Adapters.Pager;
import com.criticalog.ecritica.BaseActivities.BaseActivity;
import com.criticalog.ecritica.Interface.IConnoteTrackSuccesssFailure;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataPart;
import com.criticalog.ecritica.ModelClasses.AWBDetails;
import com.criticalog.ecritica.ModelClasses.ConnoteDetails;
import com.criticalog.ecritica.ModelClasses.ConsigneeDeatils;
import com.criticalog.ecritica.ModelClasses.ConsignerDetails;
import com.criticalog.ecritica.ModelClasses.PartDetails;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.EventResponse;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SRActivity extends BaseActivity implements  IConnoteTrackSuccesssFailure {

    private static String TAG = SRActivity.class.getSimpleName();
    ConsignerDetails consignerDetails ;
    ConsigneeDeatils consigneeDeatils;
    ArrayList<DataPart> partDetailsArrayList;
    ArrayList<AWBDetails> awbDetailsArrayList;
    String podimage,edpimage;

    ImageView back_button;
    Pager adapter;
    private LinearLayout progressbarlayout ;
    private RecyclerView chatrv;

    EditText cononote_edittext;
    ViewPager pager;
    TabLayout tabs;
    LinearLayout mainlayout;
    private TextView customer_code,customer_name,pay_value,pay_mode,service_type,pickup_date,ed_date,no_of_pcs,connote_value,actual_wt,dim_wt,product_mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sr);

        cononote_edittext = findViewById(R.id.connoteno);
        mainlayout = findViewById(R.id.mainlayout);
        customer_code = findViewById(R.id.cus_code);
        customer_name = findViewById(R.id.cus_name);
        pay_value = findViewById(R.id.pay_value);
        pay_mode = findViewById(R.id.pay_mode);
        service_type = findViewById(R.id.servicetype);
        pickup_date = findViewById(R.id.pu_date);
        ed_date = findViewById(R.id.edd_date);
        no_of_pcs = findViewById(R.id.no_of_pcs);
        connote_value = findViewById(R.id.con_value);
        actual_wt = findViewById(R.id.actual_wt);
        dim_wt = findViewById(R.id.dim_wt);
        back_button = findViewById(R.id.xtv_backbutton);
        product_mode = findViewById(R.id.product_mode);
        progressbarlayout = findViewById(R.id.llProgressBar);
        chatrv = findViewById(R.id.chatrv);




        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        cononote_edittext.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= ( cononote_edittext.getRight() -  cononote_edittext.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        mainlayout.setVisibility(View.GONE);
                        try {
                            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        getConnoteNo();

                        return true;
                    }
                }
                return false;
            }
        });



    }
    public void getConnoteNo() {

        String connoteno = cononote_edittext.getText().toString().trim();

        if (TextUtils.isEmpty(connoteno) || connoteno.matches("") || connoteno.length() <9) {
            cononote_edittext.setError("Please enter 9 digit Connote No");
        }else {
            progressbarlayout.setVisibility(View.VISIBLE);
           // connoteTrackVolley = new ConnoteTrackVolley((IConnoteTrackSuccesssFailure) this,connoteno,this);

        }
    }



    @Override
    public void connoteTrackSuccessFailure(JSONObject response) {
        progressbarlayout.setVisibility(View.GONE);

        try {

            if (response != null) {
                if (response.getString("status").equalsIgnoreCase("success")) {
                    ArrayList<EventResponse> totaleventResponseArrayList  = new ArrayList<>();
                    mainlayout.setVisibility(View.VISIBLE);
                    JSONArray data_array = response.getJSONArray("data");
                    JSONArray awb_array = response.getJSONArray("data_awb");
                    JSONArray part_array = response.getJSONArray("data_part");
                    JSONArray event_array = response.getJSONArray("data_event");


                    Type prslisttype = new TypeToken<ArrayList<EventResponse>>() {
                    }.getType();
                    Gson googleJson = new Gson();

                    ArrayList<EventResponse> eventResponses ;
                    if (event_array != null){
                        eventResponses = googleJson.fromJson(String.valueOf(event_array), prslisttype);
                        Log.d("event response",eventResponses.toString());
                        if (eventResponses!= null){
                            for (int i = 0;i< eventResponses.size();i++) {
                                EventResponse drsDetails1 = eventResponses.get(i);
                                totaleventResponseArrayList.add(drsDetails1);
                            }
                        }

                    }


                    Type ideaconcern = new TypeToken<ArrayList<ConnoteDetails>>() {
                    }.getType();
                    Type part = new TypeToken<ArrayList<PartDetails>>() {
                    }.getType();
                    Type awb = new TypeToken<ArrayList<AWBDetails>>() {
                    }.getType();


                    if (data_array!=null) {
                        final ArrayList<ConnoteDetails> connoteDetailsArrayList = googleJson.fromJson(String.valueOf(data_array),ideaconcern);
                        if (connoteDetailsArrayList.size()>0) {
                            final ConnoteDetails connoteDetails = connoteDetailsArrayList.get(0);
                            Log.d(TAG,connoteDetails.toString());

                            customer_code.setText(connoteDetails.getClient_code());
                            customer_name.setText(connoteDetails.getClient_name());
                            pay_mode.setText(connoteDetails.getPayment_mode());
                            service_type.setText(connoteDetails.getService_type());
                            pickup_date.setText(connoteDetails.getPick_date() );
                            ed_date.setText(connoteDetails.getExpected_dt());
                            no_of_pcs.setText(connoteDetails.getPieces());
                            actual_wt.setText(connoteDetails.getGross_wt());
                            dim_wt.setText(connoteDetails.getDim_wt());
                            pay_mode.setText(connoteDetails.getPayment_mode());
                            pay_value.setText(connoteDetails.getXPAYVALUE());
                            connote_value.setText(connoteDetails.getActual_value());
                            product_mode.setText(connoteDetails.getProduct_mode());

                            podimage = connoteDetails.getPod();
                            edpimage = connoteDetails.getEdp();


                            consignerDetails = new ConsignerDetails(connoteDetails.getConsignor_name(),connoteDetails.getConsignor_add(),connoteDetails.getConsignor_city_loc(),connoteDetails.getConsignor_state(),connoteDetails.getConsignor_pin());
                            consigneeDeatils = new ConsigneeDeatils(connoteDetails.getConsignee_name(),connoteDetails.getConsignee_add(),connoteDetails.getConsignee_city_loc(),connoteDetails.getConsignee_state(),connoteDetails.getConsignee_pin());

                            if (part_array != null) {
                                partDetailsArrayList = googleJson.fromJson(String.valueOf(part_array),part);
                            }

                            if (awb_array != null) {
                                awbDetailsArrayList = googleJson.fromJson(String.valueOf(awb_array),awb);
                            }


                           // adapter = new Pager(getSupportFragmentManager(), tabs.getTabCount(),consignerDetails,consigneeDeatils,totaleventResponseArrayList,partDetailsArrayListawbDetailsArrayList,podimage,edpimage);
                            pager.setAdapter(adapter);
                            TabLayout.Tab tab = tabs.getTabAt(0);
                            tab.select();





                        }

                    }else {
                        Toast.makeText(this,response.getString("status") + "\n"+response.getString("message"),Toast.LENGTH_SHORT).show();

                    }
                }else {
                    Toast.makeText(this,response.getString("message"),Toast.LENGTH_SHORT).show();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Something went wrong... JSON parsing" , Toast.LENGTH_SHORT).show();
        }
    }
}