package com.criticalog.ecritica.Activities;


import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Adapters.PartsAdapter;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataPart;
import com.criticalog.ecritica.ModelClasses.PartDetails;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PartDetailsFragment extends Fragment {

    List<DataPart> partDetailsArrayList;
    RecyclerView recyclerView ;
    View view;


    public PartDetailsFragment(List<DataPart> partDetailsArrayList) {
        this.partDetailsArrayList = partDetailsArrayList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_part_details, container, false);
        recyclerView = view.findViewById(R.id.part_rv);
        // Inflate the layout for this fragment

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);


        recyclerView.setAdapter(new PartsAdapter(partDetailsArrayList));
      //  createTable();
        return view;
    }

/*
    private void createTable() {
        partTable.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        TableRow tableRow = new TableRow(getActivity());
        partTable.setStretchAllColumns(true);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setLayoutParams(lp);
        tableRow.setBackgroundResource(R.drawable.valuecellborder);

        TextView tv1 = new TextView(getActivity());
        tv1.setText("Description");
        tv1.setTextSize(15);
        tv1.setLayoutParams(lp);
        tv1.setBackgroundResource(R.drawable.valuecellborder);
        tableRow.addView(tv1);


        TextView tv2 = new TextView(getActivity());
        tv2.setText("Pcs");
        tv2.setTextSize(15);
        tv2.setLayoutParams(lp);
        tv2.setBackgroundResource(R.drawable.valuecellborder);
        tableRow.addView(tv2);

        TextView tv3 = new TextView(getActivity());
        tv3.setText("L");
        tv3.setLayoutParams(lp);
        tv3.setTextSize(15);
        tv3.setBackgroundResource(R.drawable.valuecellborder);
        tableRow.addView(tv3);

        TextView tv4 = new TextView(getActivity());
        tv4.setText("B");
        tv4.setLayoutParams(lp);
        tv4.setBackgroundResource(R.drawable.valuecellborder);
        tv4.setTextSize(15);
        tableRow.addView(tv4);

        TextView tv5 = new TextView(getActivity());
        tv5.setText("H");
        tv5.setLayoutParams(lp);
        tv5.setTextSize(15);
        tv5.setBackgroundResource(R.drawable.valuecellborder);
        tableRow.addView(tv5);

        TextView tv6 = new TextView(getActivity());
        tv6.setText("Invoice");
        tv6.setLayoutParams(lp);
        tv6.setTextSize(15);
        tv6.setBackgroundResource(R.drawable.valuecellborder);
        tableRow.addView(tv6);

        partTable.addView(tableRow);


        for (int i =0 ;i< partDetailsArrayList.size();i++) {
            PartDetails eventDetails = partDetailsArrayList.get(i);

            TableRow tbrow = new TableRow(getActivity());

            TextView t1v = new TextView(getActivity());
            t1v.setText(eventDetails.getDesc());
            t1v.setTextColor(Color.BLACK);
            t1v.setBackgroundResource(R.drawable.valuecellborder);
            tbrow.addView(t1v);
            tbrow.setBackgroundResource(R.drawable.valuecellborder);
            TextView t2v = new TextView(getActivity());
            t2v.setText(eventDetails.getPcs());
            t2v.setTextColor(Color.BLACK);
            t2v.setBackgroundResource(R.drawable.valuecellborder);
            tbrow.addView(t2v);
            TextView t3v = new TextView(getActivity());
            t3v.setText(eventDetails.getLength());
            t3v.setTextColor(Color.BLACK);
            t3v.setBackgroundResource(R.drawable.valuecellborder);
            tbrow.addView(t3v);

            TextView t4v = new TextView(getActivity());
            t4v.setText(eventDetails.getBreadth());
            t4v.setTextColor(Color.BLACK);
            t4v.setBackgroundResource(R.drawable.valuecellborder);
            tbrow.addView(t4v);

            TextView t5v = new TextView(getActivity());
            t5v.setText(eventDetails.getHeight());
            t5v.setTextColor(Color.BLACK);
            t5v.setBackgroundResource(R.drawable.valuecellborder);
            tbrow.addView(t5v);

            TextView t6v = new TextView(getActivity());
            t6v.setText(eventDetails.getInvoiceno());
            t6v.setTextColor(Color.BLACK);
            t6v.setBackgroundResource(R.drawable.valuecellborder);
            tbrow.addView(t6v);

            partTable.addView(tbrow);

        }
    }
*/

}
