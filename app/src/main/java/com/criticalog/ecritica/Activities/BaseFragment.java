package com.criticalog.ecritica.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Utils.NetworkClass;
import com.criticalog.ecritica.Utils.SignalStrengthChecker;
import com.criticalog.ecritica.Utils.StaticUtils;


import java.text.DecimalFormat;
import java.text.NumberFormat;

public class BaseFragment extends Fragment implements FragmentNavigation.View {

    // the root view
    protected View rootView;
    public NetworkClass mNetworkClass;
    public SignalStrengthChecker mSignalStrengthChecker;
    public DatabaseHelper dbHelper;

    /**
     * navigation presenter instance
     * declared in base for easier access
     */
    protected FragmentNavigation.Presenter navigationPresenter;
    public static final String PUBLISHABLE_KEY = "MtveXuzXNKsr2pKMGuwZ_j_PDZLqSnu9m9pzakokOGtDhJTf3Rq8qYsAUCO-S0sqcbv60i0q_la_PyEAsf9rBA";
    private static final String ACCOUNTID = "g_eLK6Xbl9_GJtzhT-yOsNCXHRY";
    private static final String SECRETKEY = "7VqmHDuwFp-EGeXUIfNfjxaqm4w4XsRYxKikwNb4EGjMpNRjd48v2Q";

    //  protected abstract int getLayout();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DatabaseHelper(getActivity());
        dbHelper.getWritableDatabase();



    }

    /**
     * set the navigation presenter instance
     * //@param presenter
     */


    public void mShowDialog(String title, String message, String positiveButton, final String negativeButton, final Boolean closeActivity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        //dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);

        mSignalStrengthChecker = new SignalStrengthChecker(getContext());
        dialogBuilder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (closeActivity) {
                    getActivity().finish();
                }
                dialogInterface.cancel();
            }
        });

        if (negativeButton != null) {
            dialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    if (negativeButton.equalsIgnoreCase(StaticUtils.NETWORK_BUTTON)) {
                        dialogInterface.cancel();
                        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    }
                }
            });
        }

        final AlertDialog dialog = dialogBuilder.create();
        if (!(this.isRemoving())) {
            //show dialog
            dialog.show();
        }
    }

    @Override
    public void atachPresenter(FragmentNavigation.Presenter presenter) {
        navigationPresenter = presenter;
    }

    public double distance(double lat1, double lon1, double lat2, double lon2,String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist/0.62137;

       /* String num = String.valueOf(dist);
        new BigDecimal(num).toPlainString();
        return new BigDecimal(num).toPlainString();*/

        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    public String converttorealnumber(double distance){
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }
}
