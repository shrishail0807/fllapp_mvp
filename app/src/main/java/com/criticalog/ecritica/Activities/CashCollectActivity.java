package com.criticalog.ecritica.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.criticalog.ecritica.R;

public class CashCollectActivity extends Activity {

    ImageButton button;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collectcodcash);

        button = findViewById(R.id.cancel);
        editText = findViewById(R.id.amount);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void collectCashFromText(View view) {

        String amount =  editText.getText().toString().trim();

        Toast.makeText(CashCollectActivity.this,amount,Toast.LENGTH_SHORT).show();
    }
}
