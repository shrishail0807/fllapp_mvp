package com.criticalog.ecritica.Activities;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.criticalog.ecritica.Interface.IOtpVerify;
import com.criticalog.ecritica.MVPDRS.DRSActivity;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.MVPTATCheck.TATCheckActivity;
import com.criticalog.ecritica.NewCODProcess.Model.PaymentLinkRequest;
import com.criticalog.ecritica.NewCODProcess.NewCodActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.OtpEditText;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.VolleyClasses.VerifyOtpVolley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReadOtpActivity extends AppCompatActivity implements View.OnClickListener, IOtpVerify {

    ImageButton back;
    OtpEditText otpentered;
    Button verify, generateotp;
    TextView resendotptv, tv2;
    EditText entermobilenoed;
    TextInputLayout textInputLayout;
    String refno, userid, tskid, clientcode, round, consignee, phone;
    String otp_mobile = "0";
    private ProgressDialog mProgressDialog;
    private RestServices mRestServices;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;
    private EditText mMobileNumber;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String dockenNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_otp);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Please Wait!");
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);

        mCriticalogSharedPreferences=CriticalogSharedPreferences.getInstance(this);

        dockenNo=mCriticalogSharedPreferences.getData("task_id");

        findViews();
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();
        mMobileNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;


                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (mMobileNumber.getRight() - mMobileNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        String mobile = mMobileNumber.getText().toString();

                        if (!mobile.matches(regexStr)) {
                            Toast.makeText(ReadOtpActivity.this, "Enter Valid Mobile Number!!", Toast.LENGTH_SHORT).show();
                        } else {
                          /*  PaymentLinkRequest paymentLinkRequest = new PaymentLinkRequest();
                            paymentLinkRequest.setAction("resend_payment_link");
                            paymentLinkRequest.setDocketNo(coonoteNum);
                            paymentLinkRequest.setUserId(userId);
                            paymentLinkRequest.setMobileNo(mobile);

                            mPresenter.paymentLinkRequest(paymentLinkRequest);*/

                            //   validateOTPCall(2,mobile);
                            validateOTPCallManual(2, mobile);
                            phone = mobile;
                        }
                        return true;
                    }
                }
                return false;
            }
        });

    }

    private void getIntentData() {
        Intent intent = getIntent();

        refno = intent.getStringExtra("refcount");
        tskid = intent.getStringExtra("taskid");
        userid = intent.getStringExtra("userid");
        clientcode = intent.getStringExtra("clientcode");
        round = intent.getStringExtra("round");
        consignee = intent.getStringExtra("consignee");
        phone = intent.getStringExtra("phone");

    }

    private void findViews() {

        back = findViewById(R.id.back);
        verify = findViewById(R.id.verfifyotp);
        otpentered = findViewById(R.id.et_otp);
        resendotptv = findViewById(R.id.resendotp);
        entermobilenoed = findViewById(R.id.entermobileno);
        textInputLayout = findViewById(R.id.textinput);
        tv2 = findViewById(R.id.enterotptv2);
        generateotp = findViewById(R.id.generateotp);
        mMobileNumber = findViewById(R.id.mMobileNumber);

        getIntentData();


        tv2.setText("Enter the otp sent to " + phone);

        setClickListener();

    }

    private void setClickListener() {

        back.setOnClickListener(this);
        verify.setOnClickListener(this);
        otpentered.setOnClickListener(this);
        resendotptv.setOnClickListener(this);
        entermobilenoed.setOnClickListener(this);
        generateotp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                //onBackPressed();

                startActivity(new Intent(this, DRSActivity.class));
                finish();
                break;
            case R.id.verfifyotp:
                String otp = otpentered.getText().toString().trim();

                Log.d("client code", clientcode);


                validateOTPCall(1, otp);

                //   new VerifyOtpVolley(this,userid,tskid,refno,otp,phone,this,"1",clientcode,round,consignee);


              /*  Intent intent=new Intent();
                intent.putExtra("otp",otp);
                intent.putExtra("otp_mobile",otp_mobile);
                setResult(2,intent);
                finish();//finishing activity*/


                break;
            case R.id.resendotp:
               /* entermobilenoed.setVisibility(View.VISIBLE);
                textInputLayout.setVisibility(View.VISIBLE);
                verify.setVisibility(View.GONE);
                generateotp.setVisibility(View.VISIBLE);
                otpentered.setVisibility(View.GONE);
                tv2.setVisibility(View.GONE);
*/
                validateOTPCall(2, "");

                // new VerifyOtpVolley(this, userid, tskid, refno, "0", phone, this, "2", clientcode,round,consignee);

                break;

          /*  case  R.id.generateotp:

                tv2.setVisibility(View.GONE);
                otp_mobile = entermobilenoed.getText().toString().trim();
                Log.d("client code",otp_mobile);

                break;*/

        }

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ReadOtpActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ReadOtpActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ReadOtpActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(ReadOtpActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void validateOTPCallManual(int resendFlag, String mobile) {
        OTPRequest otpRequest = new OTPRequest();
        otpRequest.setOtp("");
        otpRequest.setAction("validate_otp");
        otpRequest.setClientCode(clientcode);
        otpRequest.setOtpMobile(mobile);
        otpRequest.setConsignee(consignee);
        otpRequest.setIsResend(String.valueOf(resendFlag));
        otpRequest.setRefcount(refno);
        otpRequest.setRound(round);
        otpRequest.setUserId(userid);
        otpRequest.setLatitude(String.valueOf(latitude));
        otpRequest.setLongitude(String.valueOf(longitude));
        otpRequest.setDocket_no(dockenNo);
        otpRequest.setFor_type("drs");

        mProgressDialog.show();
        Call<OTPResponse> otpResponseCall = mRestServices.otpRequestVerify(otpRequest);
        otpResponseCall.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                mProgressDialog.dismiss();
                if (response.body().getStatus() == 200) {
                    if (response.body().getMessage().equalsIgnoreCase("OTP Varified")) {
                        Toast.makeText(ReadOtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ReadOtpActivity.this, DRSActivity.class));
                        finish();
                    } else {
                        Toast.makeText(ReadOtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(ReadOtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(ReadOtpActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void validateOTPCall(int resendFlag, String otp) {
        OTPRequest otpRequest = new OTPRequest();
        otpRequest.setOtp(otp);
        otpRequest.setAction("validate_otp");
        otpRequest.setClientCode(clientcode);
        otpRequest.setOtpMobile(phone);
        otpRequest.setConsignee(consignee);
        otpRequest.setIsResend(String.valueOf(resendFlag));
        otpRequest.setRefcount(refno);
        otpRequest.setRound(round);
        otpRequest.setUserId(userid);
        otpRequest.setLatitude(String.valueOf(latitude));
        otpRequest.setLongitude(String.valueOf(longitude));
        otpRequest.setDocket_no(dockenNo);
        otpRequest.setFor_type("drs");

        mProgressDialog.show();
        Call<OTPResponse> otpResponseCall = mRestServices.otpRequestVerify(otpRequest);
        otpResponseCall.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                mProgressDialog.dismiss();
                if (response.body().getStatus() == 200) {
                    if (response.body().getMessage().equalsIgnoreCase("OTP Varified")) {
                        Toast.makeText(ReadOtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ReadOtpActivity.this, DRSActivity.class));
                        finish();
                    } else {
                        Toast.makeText(ReadOtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(ReadOtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(ReadOtpActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void verifyOtp(JSONObject response) {

        if (response != null) {
            try {
                int status = response.getInt("status");
                String flag = "";

                if (status == 0) {
                    flag = "no";
                    Toast.makeText(ReadOtpActivity.this, response.getString("Message"), Toast.LENGTH_LONG).show();
                    finish();
                } else if (status == 1) {

                    if (response.getString("Message").equalsIgnoreCase("OTP Resent Successfully!!!")) {
                        Log.d("inside", "if");
                        Toast.makeText(ReadOtpActivity.this, response.getString("Message"), Toast.LENGTH_LONG).show();

                    } else if (response.getString("Message").equalsIgnoreCase("OTP Varified")) {

                        Toast.makeText(ReadOtpActivity.this, response.getString("Message"), Toast.LENGTH_LONG).show();
                        // finish();
                        Log.d("inside", "else");
                        startActivity(new Intent(ReadOtpActivity.this, DRSActivity.class));


                    }
                    flag = "yes";

                }


            } catch (JSONException e) {
                Toast.makeText(this, "Something went wrong... JSON parsing", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }


    }
}
