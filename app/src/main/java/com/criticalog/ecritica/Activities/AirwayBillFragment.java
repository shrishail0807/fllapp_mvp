package com.criticalog.ecritica.Activities;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Adapters.AWBAdapter;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.DataAwb;
import com.criticalog.ecritica.R;

import java.util.List;


@SuppressLint("ValidFragment")
public class AirwayBillFragment extends BaseFragment {

    List<DataAwb> awbDetailsArrayList1;
    RecyclerView awb_rv;
    private TextView mNodataText;

    public AirwayBillFragment(List<DataAwb> awbDetailsArrayList) {
        // Required empty public constructor

        this.awbDetailsArrayList1 = awbDetailsArrayList;
    }

    View view;
    TableLayout awb_table;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_airway_bill, container, false);

       /* awb_table = view.findViewById(R.id.awbtable);
        createTable();*/

        awb_rv = view.findViewById(R.id.awb_rv);
        mNodataText = view.findViewById(R.id.mNodataText);
        setData();
        return view;
    }

    private void setData() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        awb_rv.setLayoutManager(mLayoutManager);

        if (awbDetailsArrayList1 != null) {
            awb_rv.setAdapter(new AWBAdapter(awbDetailsArrayList1));
        }else {
            mNodataText.setVisibility(View.VISIBLE);
        }

    }
    
}
