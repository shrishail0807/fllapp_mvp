package com.criticalog.ecritica.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.criticalog.ecritica.R;

public class ContactUs extends AppCompatActivity {
    private TextView mCall;
    private ImageView xtv_backbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        mCall=findViewById(R.id.mCall);
        xtv_backbutton=findViewById(R.id.xtv_backbutton);

        xtv_backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9513555079"));
                startActivity(intent);
            }
        });
    }

}
