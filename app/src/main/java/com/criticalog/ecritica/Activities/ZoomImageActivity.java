package com.criticalog.ecritica.Activities;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.R;
import com.github.chrisbanes.photoview.PhotoView;

public class ZoomImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);

        PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);
        photoView.setImageResource(R.drawable.image);
        String url = getIntent().getStringExtra("imageurl");
        Glide.with(this).load(url).placeholder(R.drawable.brokenimage).into(photoView);

    }
}
