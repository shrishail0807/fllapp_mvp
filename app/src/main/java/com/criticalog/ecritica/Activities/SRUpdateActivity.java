package com.criticalog.ecritica.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.criticalog.ecritica.Adapters.AWBAdapter;
import com.criticalog.ecritica.Adapters.SrMessageAdapter;
import com.criticalog.ecritica.Interface.ISrMessagetoDisplay;
import com.criticalog.ecritica.Interface.IUpdateSr;
import com.criticalog.ecritica.MVPCannoteEnquiry.ConnoteEnquiryContract;
import com.criticalog.ecritica.MVPCannoteEnquiry.ConnoteEnquiryInteractorImpl;
import com.criticalog.ecritica.MVPCannoteEnquiry.ConnoteEnquiryPresenterImpl;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.ConnoteEnquiryResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SRDatum;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrServiceResponse;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateRequest;
import com.criticalog.ecritica.MVPCannoteEnquiry.Model.SrUpdateResponse;
import com.criticalog.ecritica.MVPSectorPickup.SectorPickupActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.VolleyClasses.SrMessageVolley;
import com.criticalog.ecritica.VolleyClasses.SrUpdateVolley;
import com.criticalog.ecritica.model.SrDetails;
import com.criticalog.ecritica.model.SrResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

import es.dmoral.toasty.Toasty;

public class SRUpdateActivity extends AppCompatActivity implements  IUpdateSr , ConnoteEnquiryContract.MainView {

    private RecyclerView srupdaterv;
    private ScrollView scrollView;
    private Button cancel,submit;
    private EditText message;
    private CriticalogSharedPreferences criticalogSharedPreferences ;
    private ConnoteEnquiryContract.presenter mPresenter;
    private String userId,token;
    private SimpleArcDialog mProgressDialog1;
    int width;
    int height;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.srupdate_activity);

        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        scrollView = findViewById(R.id.scrollView);
        cancel = findViewById(R.id.cancel);
        submit = findViewById(R.id.submit);
        message = findViewById(R.id.message);


        userId=criticalogSharedPreferences.getData("userId");
        token=criticalogSharedPreferences.getData("token");

        String sHeightWidth=getScreenResolution(this);

        if(sHeightWidth!=null)
        {
            String string = sHeightWidth;
            String[] parts = string.split(",");
            String width = parts[0]; // 004
            String height = parts[1]; // 034556


            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.x = -10;
            params.height = 900;
            params.width = Integer.parseInt(width);;
            params.y = -10;
            this.getWindow().setAttributes(params);
        }else {
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.x = -10;
            params.height = 900;
            params.width = 600;
            params.y = -10;
            this.getWindow().setAttributes(params);
        }

        srupdaterv = findViewById(R.id.srmessagerv);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        srupdaterv.addItemDecoration(decoration);
        srupdaterv.setLayoutManager(mLayoutManager);


        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        },500);

      //  new SrMessageVolley(this,this::displaySrmessage);

        if (getIntent().getSerializableExtra("details") != null ) {
            ArrayList<SRDatum> srDetailsArrayList = (ArrayList<SRDatum>) getIntent().getSerializableExtra("details");
            Log.d("sr details","from intent"+ srDetailsArrayList.toString());

            srupdaterv.setAdapter(new SrMessageAdapter(this,srDetailsArrayList));
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String comment = message.getText().toString().trim();

                if (comment.equals("")) {
                    message.setError("Please Enter the Comment!!");
                }else {
                    SrUpdateRequest srUpdateRequest=new SrUpdateRequest();
                    srUpdateRequest.setAction("service_update");
                    srUpdateRequest.setUserId(userId);
                    srUpdateRequest.setClientId("");
                    srUpdateRequest.setTaskId(criticalogSharedPreferences.getData("connote_no_sr_update"));
                    srUpdateRequest.setComment(comment);
                    srUpdateRequest.setLatitude(String.valueOf(latitude));
                    srUpdateRequest.setLongitude(String.valueOf(longitude));

                    mPresenter=new ConnoteEnquiryPresenterImpl(SRUpdateActivity.this,new ConnoteEnquiryInteractorImpl(),new ConnoteEnquiryInteractorImpl(),new ConnoteEnquiryInteractorImpl());
                    mPresenter.SrUpdateRequest(token,srUpdateRequest);
                  //  new SrUpdateVolley(SRUpdateActivity.this,SRUpdateActivity.this::updateSR,criticalogSharedPreferences.getData("connote_no_sr_update"),criticalogSharedPreferences.getData("userId"),comment);
                }
            }
        });

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(SRUpdateActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(SRUpdateActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SRUpdateActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(SRUpdateActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    private static String getScreenResolution(Context context)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        return "" + width + "," + height + "";
    }

    @Override
    public void updateSR(JSONObject response) {



        if (response != null){
            try {
                if (response.getString("status").equalsIgnoreCase("success")){
                    Toast.makeText(SRUpdateActivity.this,response.getString("message"),Toast.LENGTH_LONG).show();
                    finish();
                }else if (response.getString("status").equalsIgnoreCase("fail")) {
                    Toast.makeText(SRUpdateActivity.this,response.getString("message"),Toast.LENGTH_LONG).show();
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void showProgress() {
        mProgressDialog1.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog1.dismiss();
    }

    @Override
    public void connoteEnquirySetDataToViews(ConnoteEnquiryResponse connoteEnquiryResponse) {

    }

    @Override
    public void srServiceResponseDataToViews(SrServiceResponse srServiceResponse) {

    }

    @Override
    public void SrUpdateResponseDataToViews(SrUpdateResponse srUpdateResponse) {

        if(srUpdateResponse.getStatus()==200)
        {
            Toasty.success(SRUpdateActivity.this, srUpdateResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            finish();
        }else {
            Toasty.error(SRUpdateActivity.this, srUpdateResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            finish();
        }

    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.error(SRUpdateActivity.this, throwable.toString(), Toast.LENGTH_SHORT, true).show();
    }
}