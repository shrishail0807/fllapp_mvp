package com.criticalog.ecritica.Activities;

import android.content.Context;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.criticalog.ecritica.R;


public class Pickup_IDdetailsActivity extends AppCompatActivity {

    private TextView mtv_cancel;
    private Context mcontext;
    private ImageView mback_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup__iddetails);
        mtv_cancel = (TextView) findViewById(R.id.xtv_cancel);
        mback_button = (ImageView) findViewById(R.id.xback_icon);


        mtv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.xtv_cancel:
                        finish();
                        break;
                }
            }
        });

        mback_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.xback_icon:
                        finish();
                        break;
                }
            }
        });


    }
}
