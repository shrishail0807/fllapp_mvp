package com.criticalog.ecritica.Activities;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.criticalog.ecritica.BaseActivities.BaseActivity;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPLogin.LoginActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends BaseActivity {
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String androidOS,versionName;
    private ProgressBar mProgressBar;
  //  private String URLToGetBaseURL = "http://api.ecritica.co/settings.php?action=get_base_url";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mProgressBar=findViewById(R.id.mProgressBar);

        mCriticalogSharedPreferences=CriticalogSharedPreferences.getInstance(this);

        androidOS = Build.VERSION.RELEASE;
        versionName = BuildConfig.VERSION_NAME;

        StaticUtils.APP_VERSION=BuildConfig.VERSION_NAME;

        StaticUtils.TOKEN=mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=mCriticalogSharedPreferences.getData("distance_api_key");

        getBaseUrl();
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               String loginStatus=mCriticalogSharedPreferences.getData("login_status");
                if (loginStatus.equals("")) {
                    Intent loginWelcome = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(loginWelcome);
                    finish();
                } else {
                    Intent loginWelcome = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(loginWelcome);
                    finish();
                }
            }
        }, 3000);
    }
    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getBaseUrl() {
        RequestQueue queue = Volley.newRequestQueue(this);
        try {
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("action", "get_base_url");
            jsonBody.put("user_id", "11337");

            mProgressBar.setVisibility(View.VISIBLE);
            JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, StaticUtils.URLToGetBaseURL, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    mProgressBar.setVisibility(View.GONE);
                    //   jsonObject = new JSONObject(response);
                    JSONObject dataObje = null;
                    try {
                        dataObje = response.getJSONObject("data");

                        String URL = dataObje.getString("BASE_URL").toString();

                        StaticUtils.BASE_URL_DYNAMIC=URL;

                        mCriticalogSharedPreferences.saveData("base_url_dynamic",URL);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressBar.setVisibility(View.GONE);
                        StaticUtils.BASE_URL_DYNAMIC="https://www.ecritica.co/efreightlive/ecritica_api/";
                        Log.e("WHERE_TAG","JSON Exception");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    //onBackPressed();
                    mProgressBar.setVisibility(View.GONE);
                    StaticUtils.BASE_URL_DYNAMIC="https://www.ecritica.co/efreightlive/ecritica_api/";
                    Log.e("WHERE_TAG","Volley Error");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    headers.put("token","HscXR3mh9E0QfKPFOW6DKhkJpT0EpF23N5LOK4ALszIVjDyw7sl7KabJLV8MD4");//put your token here
                    return headers;
                }
            };
            queue.add(jsonOblect);

        } catch (JSONException e) {
            e.printStackTrace();
            mProgressBar.setVisibility(View.GONE);
            StaticUtils.BASE_URL_DYNAMIC="https://www.ecritica.co/efreightlive/ecritica_api/";
            Log.e("WHERE_TAG","EXCEPTION");
        }
    }
}
