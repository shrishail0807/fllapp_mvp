package com.criticalog.ecritica.directionhelper;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
