package com.criticalog.ecritica.MVPSectorPickup.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AWBDatum {
    @SerializedName("bags_array")
    @Expose
    private List<BagsArray> bagsArray = null;
    @SerializedName("no_of_bags")
    @Expose
    private Integer noOfBags;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("flight_no")
    @Expose
    private String flightNo;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("boxes_array")
    @Expose
    private List<String> boxesArray = null;

    public List<BagsArray> getBagsArray() {
        return bagsArray;
    }

    public void setBagsArray(List<BagsArray> bagsArray) {
        this.bagsArray = bagsArray;
    }

    public Integer getNoOfBags() {
        return noOfBags;
    }

    public void setNoOfBags(Integer noOfBags) {
        this.noOfBags = noOfBags;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<String> getBoxesArray() {
        return boxesArray;
    }

    public void setBoxesArray(List<String> boxesArray) {
        this.boxesArray = boxesArray;
    }
    /*@SerializedName("bags_array")
    @Expose
    private ArrayList<BagsArray> bagsArray = null;
    @SerializedName("no_of_bags")
    @Expose
    private Integer noOfBags;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("flight_no")
    @Expose
    private String flightNo;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("boxes_array")
    @Expose
    private List<String> boxesArray = null;

    public ArrayList<BagsArray> getBagsArray() {
        return bagsArray;
    }

    public void setBagsArray(ArrayList<BagsArray> bagsArray) {
        this.bagsArray = bagsArray;
    }

    public Integer getNoOfBags() {
        return noOfBags;
    }

    public void setNoOfBags(Integer noOfBags) {
        this.noOfBags = noOfBags;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<String> getBoxesArray() {
        return boxesArray;
    }

    public void setBoxesArray(List<String> boxesArray) {
        this.boxesArray = boxesArray;
    }*/
  /*  @SerializedName("bags_array")
    @Expose
    private List<String> bagsArray = null;
    @SerializedName("no_of_bags")
    @Expose
    private Integer noOfBags;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("flight_no")
    @Expose
    private String flightNo;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("boxes_array")
    @Expose
    private List<String> boxesArray = null;

    public List<String> getBagsArray() {
        return bagsArray;
    }

    public void setBagsArray(List<String> bagsArray) {
        this.bagsArray = bagsArray;
    }

    public Integer getNoOfBags() {
        return noOfBags;
    }

    public void setNoOfBags(Integer noOfBags) {
        this.noOfBags = noOfBags;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<String> getBoxesArray() {
        return boxesArray;
    }

    public void setBoxesArray(List<String> boxesArray) {
        this.boxesArray = boxesArray;
    }*/

  /*  @SerializedName("bags_array")
    @Expose
    private List<String> bagsArray = null;
    @SerializedName("no_of_bags")
    @Expose
    private Integer noOfBags;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("flight_no")
    @Expose
    private String flightNo;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;

    public List<String> getBagsArray() {
        return bagsArray;
    }

    public void setBagsArray(List<String> bagsArray) {
        this.bagsArray = bagsArray;
    }

    public Integer getNoOfBags() {
        return noOfBags;
    }

    public void setNoOfBags(Integer noOfBags) {
        this.noOfBags = noOfBags;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }*/

}
