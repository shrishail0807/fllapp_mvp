package com.criticalog.ecritica.MVPSectorPickup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.CriticallogApplication;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPInscan.InScanActivity;
import com.criticalog.ecritica.MVPInscan.InscanContract;
import com.criticalog.ecritica.MVPInscan.InscanInteractorImpl;
import com.criticalog.ecritica.MVPInscan.InscanPresenterImpl;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;
import com.criticalog.ecritica.MVPLinehaul.Adapter.DocketsAdapter;
import com.criticalog.ecritica.MVPLinehaul.LinehaulActivity;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.DocketModelWithFlag;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckResponse;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupResponse;
import com.criticalog.ecritica.MVPTATCheck.TATCheckActivity;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class SectorPickupActivity extends AppCompatActivity implements View.OnClickListener, TimePicker.OnTimeChangedListener, View.OnTouchListener, SectorPickupContract.MainView {
    private EditText Xawbno, Xorigin, Xdest, Xbags, XManualbox;
    private TextView scan, manual, scan_result, Finish_tv;
    private String bag, box;
    private LinearLayout root_layout, details_layout, button_layout, air_layout, radio_layout;
    private List baglist = new ArrayList();
    private List scan_baglist = new ArrayList();

    private List boxlist = new ArrayList();
    private List scan_boxlist = new ArrayList();
    private TextView scannext, finish, check, XFlight, Xatd, Xata, select_option, ata_text, atd_text;
    private String mode = "";
    private String flight, ata = "", atd = "";
    private Button ok_btn;
    Spinner atd_hour_spinner, atd_min_spinner, ata_hour_spinner, ata_min_spinner;
    private RadioGroup radioGroup;
    private RadioButton atd_radio_btn, ata_radio_btn;

    private ImageView backbutton;
    String scan_bag = "";
    String boxNumber = "";
    TimePicker tp;
    private Date atd_dt;
    CriticalogSharedPreferences criticalogSharedPreferences;
    SimpleDateFormat sdf;

    private String awbno;


    private SectorPickupContract.presenter mPresenter;

    private String userId, token;

    private SimpleArcDialog mProgressBar;

    private LinearLayout mBoxEnterLayout;
    private EditText mBoxNumEntry;
    private Button mAddBox;
    private ImageView mScanBox;

    private String scanBarcode = "";
    private TextView mSubmit;
    private RecyclerView mRvBox,mRvBag;
    ArrayList<DocketModelWithFlag> docketModelWithFlags = new ArrayList<>();
    ArrayList<DocketModelWithFlag> docketModelWithFlags1 = new ArrayList<>();
    TextView mListOfBagText,mListOfBoxText;
    LinearLayout mLayoutFlight;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sector_pickup);

        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        findView();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(SectorPickupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(SectorPickupActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SectorPickupActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(SectorPickupActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    private void findView() {

        Xawbno = findViewById(R.id.Xawbno);
        Xorigin = findViewById(R.id.Xorigin);
        Xorigin.setEnabled(false);
        Xdest = findViewById(R.id.Xdest);
        Xdest.setEnabled(false);
        Xbags = findViewById(R.id.Xbags);
        Xbags.setEnabled(false);
        scan = findViewById(R.id.scan);
        XFlight = findViewById(R.id.XFlight);
        ok_btn = findViewById(R.id.ok);
        atd_radio_btn = findViewById(R.id.atd_radio);
        ata_radio_btn = findViewById(R.id.ata_radio);
        ata_text = findViewById(R.id.ata_time);
        atd_text = findViewById(R.id.atd_time);
        manual = findViewById(R.id.manual);
        select_option = findViewById(R.id.select_option);
        XManualbox = findViewById(R.id.manual_ed);
        radio_layout = findViewById(R.id.radion_layout);
        Finish_tv = findViewById(R.id.finish_tv);


        backbutton = findViewById(R.id.xtv_backbutton);
        root_layout = findViewById(R.id.mainLayout);
        details_layout = findViewById(R.id.details_layout);

        air_layout = findViewById(R.id.air_layout);
        radio_layout.setVisibility(View.GONE);
        radioGroup = findViewById(R.id.radiogroup);

        mBoxEnterLayout = findViewById(R.id.mBoxEnterLayout);
        mBoxNumEntry = findViewById(R.id.mBoxNumEntry);
        mAddBox = findViewById(R.id.mAddBox);
        mScanBox = findViewById(R.id.mScanBox);
        mSubmit = findViewById(R.id.mSubmit);
        mRvBox = findViewById(R.id.mRvBox);
        mRvBag=findViewById(R.id.mRvBag);
        mListOfBagText=findViewById(R.id.mListOfBagText);
        mListOfBoxText=findViewById(R.id.mListOfBoxText);
        mLayoutFlight=findViewById(R.id.mLayoutFlight);



        tp = findViewById(R.id.tp);
        tp.setIs24HourView(true);
        tp.setOnTimeChangedListener(SectorPickupActivity.this);

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = criticalogSharedPreferences.getData("base_url_dynamic");

        userId = criticalogSharedPreferences.getData("userId");
        token = criticalogSharedPreferences.getData("token");

        StaticUtils.TOKEN = criticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=criticalogSharedPreferences.getData("distance_api_key");
        scan.setOnClickListener(this);
        backbutton.setOnClickListener(this);
        Xawbno.setOnTouchListener(this);
        manual.setOnClickListener(this);
        XManualbox.setOnTouchListener(this);
        Finish_tv.setOnClickListener(this);
        mAddBox.setOnClickListener(this);
        mScanBox.setOnClickListener(this);


        mAddBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxNumber = mBoxNumEntry.getText().toString();

                if (boxNumber.equals("")) {
                    Toasty.success(SectorPickupActivity.this, "Enter BOX Number!!", Toast.LENGTH_SHORT, true).show();
                } else if (boxNumber.length() != 9) {
                    Toasty.success(SectorPickupActivity.this, "Enter Valid BOX Number!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    if (boxlist.contains(boxNumber)) {
                        Log.e("bag array", baglist.toString());
                        if (!(scan_boxlist.contains(boxNumber))) {
                            scan_boxlist.add(boxNumber);

                            mBoxNumEntry.setText("");

                            int rightMarkIndex = boxlist.indexOf(boxNumber);

                            docketModelWithFlags.remove(rightMarkIndex);

                            docketModelWithFlags.add(rightMarkIndex, new DocketModelWithFlag(boxNumber, "true"));

                            DocketsAdapter docketsAdapter = new DocketsAdapter(SectorPickupActivity.this, docketModelWithFlags,2);
                            mRvBox.setLayoutManager(new GridLayoutManager(SectorPickupActivity.this, 2));
                            //mRvDockets.setAdapter(docketsAdapter);
                            docketsAdapter.notifyDataSetChanged();

                            if (boxlist.size() == scan_boxlist.size()) {
                                mSubmit.setVisibility(View.VISIBLE);
                                // Toast.makeText(SectorPickupActivity.this, "SUBMIT ", Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(SectorPickupActivity.this, "Box Scanned " + scan_boxlist.size() + " / " + boxlist.size(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SectorPickupActivity.this, "Box already Scanned ", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SectorPickupActivity.this, "you entered wrong Box Number", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mScanBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanBarcode = "BOX";
                startBarcodeScan();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mode.equalsIgnoreCase("A")) {
                    flight = XFlight.getText().toString().trim();
                    if (flight.matches("") || TextUtils.isEmpty(flight)) {
                        XFlight.setError("Please provide Flight no");
                    } else {
                        ata = ata_text.getText().toString().trim();
                        Log.d("ata", ata);
                        atd = atd_text.getText().toString().trim();
                        Log.d("atd", atd);

                        callSectorPickup(flight, ata, atd);
                        //   new SectorPickupVolley(SectorPickup.this, SectorPickup.this, scan_baglist, flight, ata, atd);
                    }

                } else {
                    callSectorPickup("", "", "");
                    // new SectorPickupVolley(SectorPickup.this, SectorPickup.this, scan_baglist, "", "", "");
                }

            }
        });

        ok_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (atd_radio_btn.isChecked()) {
                    atd_text.setText(atd);
                    sdf = new SimpleDateFormat("hh:mm aa");
                    try {

                        Date mToday = new Date();
                        String curTime = sdf.format(mToday);
                        Date curr;
                        Log.d("atd string", atd);
                        atd_dt = sdf.parse(atd);
                        curr = sdf.parse(curTime);

                        if (atd_dt.after(curr)) {
                            Toast.makeText(SectorPickupActivity.this, "ATD cannot be grater than current time", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("right", "atd");
                            ata_radio_btn.setChecked(true);

                        }

                    } catch (ParseException e) {
                        // Invalid date was entered
                    }

                } else if (ata_radio_btn.isChecked()) {

                    ata_text.setText(ata);
                    sdf = new SimpleDateFormat("hh:mm aa");
                    try {
                        Date mToday = new Date();
                        String curTime = sdf.format(mToday);
                        Date curr = sdf.parse(curTime);
                        Date ata_dt = null;
                        if (!ata.equals("")) {
                            ata_dt = sdf.parse(ata);
/*
                            if (ata_dt.after(atd_dt) && ata_dt.before(curr)) {
                                Log.d("right", "ata");
                            } else {
                              //  Toast.makeText(SectorPickupActivity.this, "Please select right ATA", Toast.LENGTH_SHORT).show();
                            }*/
                        } else {
                            Toast.makeText(SectorPickupActivity.this, "Please Select ATA", Toast.LENGTH_SHORT).show();
                        }


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.scan) {
            if (CriticallogApplication.scanCount == baglist.size()) {
                Toast.makeText(SectorPickupActivity.this, "All bag received click on finish", Toast.LENGTH_SHORT).show();
            } else
                scanBarcode = "BAG";
            startBarcodeScan();
        } else if (v.getId() == R.id.xtv_backbutton) {
            onBackPressed();
        } else if (v.getId() == R.id.manual) {


            if (CriticallogApplication.scanCount == baglist.size()) {
                Toast.makeText(SectorPickupActivity.this, "All bag recieved click on finish", Toast.LENGTH_SHORT).show();
            } else {
                XManualbox.setText("");
                XManualbox.setVisibility(View.VISIBLE);

                radio_layout.setVisibility(View.GONE);
            }


        } else if (v.getId() == R.id.finish_tv) {
            if (mode.equalsIgnoreCase("A")) {
                flight = XFlight.getText().toString().trim();
                if (flight.matches("") || TextUtils.isEmpty(flight)) {
                    XFlight.setError("Please provide Flight no");
                } else {
                    ata = ata_text.getText().toString().trim();
                    Log.d("ata", ata);
                    atd = atd_text.getText().toString().trim();
                    Log.d("atd", atd);

                    callSectorPickup(flight, ata, atd);
                    //   new SectorPickupVolley(SectorPickup.this, SectorPickup.this, scan_baglist, flight, ata, atd);
                }

            } else {
                callSectorPickup("", "", "");
                // new SectorPickupVolley(SectorPickup.this, SectorPickup.this, scan_baglist, "", "", "");
            }
            CriticallogApplication.scanCount = 0;
        }
    }


    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Number");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (scanBarcode.equalsIgnoreCase("BAG")) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                try {
                    String MobilePattern = "[0-9]{9}";
                    scan_bag = result.getContents().toString().trim();
                    Log.d("scan bag", scan_bag);
                    if (scan_bag.equals("")) {
                        Toast.makeText(this, "Scan 9 digit number", Toast.LENGTH_SHORT).show();
                        //  startBarcodeScan();
                    } else if (scan_bag.length() == 9 || scan_bag.length() == 8 || scan_bag.length() == 7) {

                        if (baglist.contains(scan_bag)) {
                            if (!(scan_baglist.contains(scan_bag))) {

                                scan_baglist.add(scan_bag);
                                select_option.setVisibility(View.VISIBLE);
                                // Finish_tv.setVisibility(View.VISIBLE);
                                CriticallogApplication.scanCount++;
                                Toast.makeText(this, "Bag Scanned " + CriticallogApplication.scanCount + " / " + baglist.size(), Toast.LENGTH_SHORT).show();
                              /*  if (baglist.size() == scan_baglist.size()) {
                                    Finish_tv.setVisibility(View.GONE);
                                    scan.setVisibility(View.GONE);
                                    manual.setVisibility(View.GONE);
                                    select_option.setVisibility(View.GONE);
                                    mBoxEnterLayout.setVisibility(View.VISIBLE);

                                }*/
                                if(baglist.size() == scan_baglist.size() && boxlist.size()==0)
                                {
                                    mSubmit.setVisibility(View.VISIBLE);
                                    Finish_tv.setVisibility(View.GONE);
                                    scan.setVisibility(View.GONE);
                                    manual.setVisibility(View.GONE);
                                    select_option.setVisibility(View.GONE);
                                }else if (baglist.size() == scan_baglist.size()) {
                                    Finish_tv.setVisibility(View.GONE);
                                    scan.setVisibility(View.GONE);
                                    manual.setVisibility(View.GONE);
                                    select_option.setVisibility(View.GONE);
                                    mBoxEnterLayout.setVisibility(View.VISIBLE);
                                    mRvBox.setVisibility(View.VISIBLE);

                                    if(boxlist.size()>0)
                                    {
                                        mListOfBoxText.setVisibility(View.VISIBLE);
                                    }
                                }

                            } else {
                                Toast.makeText(SectorPickupActivity.this, "Bag already exist", Toast.LENGTH_SHORT).show();
                                //   startBarcodeScan();
                            }


                        } else {
                            Toast.makeText(SectorPickupActivity.this, "Wrong bag scanned", Toast.LENGTH_SHORT).show();
                            //    startBarcodeScan();
                        }

                    } else {
                        Toast.makeText(this, "Scan 9 digit number", Toast.LENGTH_SHORT).show();
                        // startBarcodeScan();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    Toast.makeText(this, R.string.scan_barcode, Toast.LENGTH_LONG).show();

                    // startBarcodeScan();
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);

            }
        } else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                boxNumber = result.getContents();
                if(boxNumber!=null)
                {

                    if (boxNumber.equals("")) {
                        Toasty.success(SectorPickupActivity.this, "Scan BOX Number!!", Toast.LENGTH_SHORT, true).show();
                    } else if (boxNumber.length() != 9) {
                        Toasty.success(SectorPickupActivity.this, "Scan Valid BOX Number!!", Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (boxlist.contains(boxNumber)) {
                            Log.e("bag array", baglist.toString());
                            if (!(scan_boxlist.contains(boxNumber))) {
                                scan_boxlist.add(boxNumber);
                                mBoxNumEntry.setText("");

                                int rightMarkIndex = boxlist.indexOf(boxNumber);

                                docketModelWithFlags.remove(rightMarkIndex);

                                docketModelWithFlags.add(rightMarkIndex, new DocketModelWithFlag(boxNumber, "true"));

                                DocketsAdapter docketsAdapter = new DocketsAdapter(SectorPickupActivity.this, docketModelWithFlags,2);
                                mRvBox.setLayoutManager(new GridLayoutManager(SectorPickupActivity.this, 2));
                                //mRvDockets.setAdapter(docketsAdapter);
                                docketsAdapter.notifyDataSetChanged();
                                if (boxlist.size() == scan_boxlist.size()) {
                                    mSubmit.setVisibility(View.VISIBLE);
                                }
                                Toast.makeText(SectorPickupActivity.this, "Box Scanned " + scan_boxlist.size() + " / " + boxlist.size(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SectorPickupActivity.this, "Box already Scanned ", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(SectorPickupActivity.this, "You Scanned wrong Box Number", Toast.LENGTH_SHORT).show();
                        }
                    }
                }


            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CriticallogApplication.scanCount = 0;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
        final int DRAWABLE_BOTTOM = 3;
        if (view.getId() == R.id.Xawbno) {
            if (event.getAction() == MotionEvent.ACTION_UP) {

                if (event.getRawX() >= (Xawbno.getRight() - Xawbno.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    getawbNo();

                    return true;
                }

            }
        } else if (view.getId() == R.id.manual_ed) {
            if (event.getAction() == MotionEvent.ACTION_UP) {

                if (event.getRawX() >= (XManualbox.getRight() - XManualbox.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    String bagNo = XManualbox.getText().toString();
                    Log.d("box no", bagNo);

                    if (baglist.contains(bagNo)) {
                        Log.e("bag array", baglist.toString());
                        if (!(scan_baglist.contains(bagNo))) {
                            scan_baglist.add(bagNo);
                          /*  scan_result.setVisibility(View.VISIBLE);
                            scan_result.setText(boxno);*/
                            CriticallogApplication.scanCount++;
                            XManualbox.setVisibility(View.GONE);

                            // Finish_tv.setVisibility(View.VISIBLE);
                            select_option.setVisibility(View.VISIBLE);
                            manual.setVisibility(View.VISIBLE);
                            radio_layout.setVisibility(View.VISIBLE);
                             if(baglist.size()==scan_baglist.size() && boxlist.size()==0)
                            {
                                mSubmit.setVisibility(View.VISIBLE);
                                Finish_tv.setVisibility(View.GONE);
                                scan.setVisibility(View.GONE);
                                manual.setVisibility(View.GONE);
                                select_option.setVisibility(View.GONE);
                            }else  if (baglist.size() == scan_baglist.size()) {
                                Finish_tv.setVisibility(View.GONE);
                                scan.setVisibility(View.GONE);
                                manual.setVisibility(View.GONE);
                                select_option.setVisibility(View.GONE);
                                mBoxEnterLayout.setVisibility(View.VISIBLE);
                                mRvBox.setVisibility(View.VISIBLE);

                                if(boxlist.size()>0)
                                {
                                    mListOfBoxText.setVisibility(View.VISIBLE);
                                }
                            }
                            Toast.makeText(this, "Bag Scanned " + scan_baglist.size() + " / " + baglist.size(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SectorPickupActivity.this, "Bag already received ", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SectorPickupActivity.this, "you entered wrong bag no", Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }

            }
        }
        return false;

    }

    private void getawbNo() {
        awbno = Xawbno.getText().toString().trim();

        if (TextUtils.isEmpty(awbno) || awbno.matches("")) {
            Xawbno.setError("Please enter MAWB no");
        } else {
            baglist.clear();
            boxlist.clear();
            docketModelWithFlags.clear();
            docketModelWithFlags.clear();
            docketModelWithFlags1.clear();
            AWBCheckRequest awbCheckRequest = new AWBCheckRequest();
            awbCheckRequest.setAction("awb_check");
            awbCheckRequest.setUserId(userId);
            awbCheckRequest.setAirwaybill(awbno);
            awbCheckRequest.setLatitude(String.valueOf(latitude));
            awbCheckRequest.setLongitude(String.valueOf(longitude));
            mPresenter = new SectorPickupPresenterImpl(this, new SectorPickupInteractorImpl(), new SectorPickupInteractorImpl());
            mPresenter.awbCheck(token, awbCheckRequest);
            criticalogSharedPreferences.saveData("awbno", awbno);
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }

    }

    @Override
    public void onTimeChanged(TimePicker timePicker, int hourOfDay, int minute) {

        String AMPM = "AM";

        if (hourOfDay > 11) {
            hourOfDay = hourOfDay - 12;
            AMPM = "PM";
        }
        int selectedradiobuttonid = radioGroup.getCheckedRadioButtonId();
        RadioButton selectedradiobutton = findViewById(selectedradiobuttonid);
        String radiobutton_text = selectedradiobutton.getText().toString();

        if (radiobutton_text.equalsIgnoreCase("ata")) {
            ata = hourOfDay + ":" + minute + " " + AMPM;


        } else if (radiobutton_text.equalsIgnoreCase("atd")) {
            atd = hourOfDay + ":" + minute + " " + AMPM;

        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

    }


    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }



    @Override
    public void setAWBCheckResponseToViews(AWBCheckResponse awbCheckResponse) {
        if (awbCheckResponse.getStatus() == 200) {
            mode = awbCheckResponse.getData().get(0).getMode();
            String atd=awbCheckResponse.getData().get(0).getDepartureTime();
            String ata=awbCheckResponse.getData().get(0).getArrivalTime();

            ata_text.setText(ata);
            atd_text.setText(atd);
            for (int i = 0; i < awbCheckResponse.getData().get(0).getBagsArray().size(); i++) {
                bag = awbCheckResponse.getData().get(0).getBagsArray().get(i).getBag();
                baglist.add(bag);
                DocketModelWithFlag docketModelWithFlag = new DocketModelWithFlag();
                docketModelWithFlag.setDocketNumber(String.valueOf(awbCheckResponse.getData().get(0).getBagsArray().get(i).getBag()));
                docketModelWithFlag.setDocketFalg(String.valueOf(awbCheckResponse.getData().get(0).getBagsArray().get(i).getTransit()));

                docketModelWithFlags1.add(docketModelWithFlag);
            }
            if(awbCheckResponse.getData().get(0).getBoxesArray()!=null)
            {

                for (int j = 0; j < awbCheckResponse.getData().get(0).getBoxesArray().size(); j++) {
                    box = awbCheckResponse.getData().get(0).getBoxesArray().get(j);
                    boxlist.add(box);

                    DocketModelWithFlag docketModelWithFlag = new DocketModelWithFlag();
                    docketModelWithFlag.setDocketNumber(String.valueOf(awbCheckResponse.getData().get(0).getBoxesArray().get(j)));
                    docketModelWithFlag.setDocketFalg("");

                    docketModelWithFlags.add(docketModelWithFlag);
                }
            }


            if(baglist.size()>0)
            {
                radio_layout.setVisibility(View.VISIBLE);
                mListOfBagText.setVisibility(View.VISIBLE);
            }

            DocketsAdapter docketsAdapter1 = new DocketsAdapter(SectorPickupActivity.this, docketModelWithFlags1,1);
            mRvBag.setLayoutManager(new GridLayoutManager(SectorPickupActivity.this, 2));
            mRvBag.setAdapter(docketsAdapter1);

            DocketsAdapter docketsAdapter = new DocketsAdapter(SectorPickupActivity.this, docketModelWithFlags,2);
            mRvBox.setLayoutManager(new GridLayoutManager(SectorPickupActivity.this, 2));
            mRvBox.setAdapter(docketsAdapter);

            details_layout.setVisibility(View.VISIBLE);
            radio_layout.setVisibility(View.VISIBLE);
            Finish_tv.setVisibility(View.GONE);
            mRvBox.setVisibility(View.GONE);
            mBoxEnterLayout.setVisibility(View.GONE);
            mListOfBoxText.setVisibility(View.GONE);


            radioGroup.setVisibility(View.VISIBLE);
            tp.setVisibility(View.VISIBLE);
            ok_btn.setVisibility(View.VISIBLE);
            mLayoutFlight.setVisibility(View.VISIBLE);

            Xorigin.setText(awbCheckResponse.getData().get(0).getOrigin());
            Xdest.setText(awbCheckResponse.getData().get(0).getDestination());
            Xbags.setText(awbCheckResponse.getData().get(0).getNoOfBags().toString());
            XFlight.setText(awbCheckResponse.getData().get(0).getFlightNo());


            if ((mode.equalsIgnoreCase("A"))) {
                air_layout.setVisibility(View.VISIBLE);
            }

        } else {
            details_layout.setVisibility(View.GONE);
            radio_layout.setVisibility(View.GONE);
            radioGroup.setVisibility(View.GONE);
            tp.setVisibility(View.GONE);
            ok_btn.setVisibility(View.GONE);
            mListOfBagText.setVisibility(View.GONE);
            mLayoutFlight.setVisibility(View.GONE);
            mSubmit.setVisibility(View.GONE);
            mBoxEnterLayout.setVisibility(View.GONE);
            mListOfBoxText.setVisibility(View.GONE);
          //mBoxEnterLayout.setVisibility(View.GONE);
            Xawbno.setVisibility(View.VISIBLE);
            Toast.makeText(this, awbCheckResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setSectorPickupDataToViews(SectorPickupResponse sectorPickupResponse) {

        if (sectorPickupResponse.getStatus() == 200) {
            Toasty.success(this, sectorPickupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            Intent intent = new Intent(SectorPickupActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toasty.error(this, sectorPickupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

        mProgressBar.dismiss();
        Toast.makeText(SectorPickupActivity.this,throwable.toString(), Toast.LENGTH_SHORT).show();
    }

    public void callSectorPickup(String flight1, String ata1, String atd1) {
        SectorPickupRequest sectorPickupRequest = new SectorPickupRequest();
        sectorPickupRequest.setAction("sector_pickup");
        sectorPickupRequest.setAta(ata1);
        sectorPickupRequest.setAtd(atd1);
        sectorPickupRequest.setAwbno(awbno);
        sectorPickupRequest.setBag(scan_baglist);
        sectorPickupRequest.setBoxes(scan_boxlist);
        sectorPickupRequest.setFlight(flight1);
        sectorPickupRequest.setUserId(userId);
        sectorPickupRequest.setMode(mode);
        sectorPickupRequest.setLatitude(String.valueOf(latitude));
        sectorPickupRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new SectorPickupPresenterImpl(this, new SectorPickupInteractorImpl(), new SectorPickupInteractorImpl());
        mPresenter.sectorPickup(token, sectorPickupRequest);
    }
}
