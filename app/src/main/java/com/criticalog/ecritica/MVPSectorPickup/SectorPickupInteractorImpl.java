package com.criticalog.ecritica.MVPSectorPickup;

import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckResponse;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SectorPickupInteractorImpl implements SectorPickupContract.AWBCheckIntractor,SectorPickupContract.SectorPickupIntractor {

    private RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void awbCheckRequest(SectorPickupContract.AWBCheckIntractor.OnFinishedListener onFinishedListener, String token, AWBCheckRequest awbCheckRequest) {

        Call<AWBCheckResponse> awbCheckResponseCall = services.awbCheck(awbCheckRequest);

        awbCheckResponseCall.enqueue(new Callback<AWBCheckResponse>() {
            @Override
            public void onResponse(Call<AWBCheckResponse> call, Response<AWBCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<AWBCheckResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void sectorPickupRequest(SectorPickupContract.SectorPickupIntractor.OnFinishedListener onFinishedListener, String token, SectorPickupRequest sectorPickupRequest) {

        Call<SectorPickupResponse> sectorPickupResponseCall=services.sectorPickup(sectorPickupRequest);
        sectorPickupResponseCall.enqueue(new Callback<SectorPickupResponse>() {
            @Override
            public void onResponse(Call<SectorPickupResponse> call, Response<SectorPickupResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<SectorPickupResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }
}
