package com.criticalog.ecritica.MVPSectorPickup;

import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckResponse;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupResponse;

public interface SectorPickupContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void awbCheck(String token, AWBCheckRequest awbCheckRequest);

        void sectorPickup(String token, SectorPickupRequest sectorPickupRequest);
    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setAWBCheckResponseToViews(AWBCheckResponse awbCheckResponse);

        void setSectorPickupDataToViews(SectorPickupResponse sectorPickupResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface AWBCheckIntractor {

        interface OnFinishedListener {
            void onFinished(AWBCheckResponse awbCheckResponse);

            void onFailure(Throwable t);
        }

        void awbCheckRequest(SectorPickupContract.AWBCheckIntractor.OnFinishedListener onFinishedListener, String token, AWBCheckRequest awbCheckRequest);
    }

    interface SectorPickupIntractor {

        interface OnFinishedListener {
            void onFinished(SectorPickupResponse sectorPickupResponse);

            void onFailure(Throwable t);
        }

        void sectorPickupRequest(SectorPickupContract.SectorPickupIntractor.OnFinishedListener onFinishedListener, String token, SectorPickupRequest sectorPickupRequest);
    }
}
