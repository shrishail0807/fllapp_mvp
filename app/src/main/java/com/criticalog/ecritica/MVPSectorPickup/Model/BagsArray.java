package com.criticalog.ecritica.MVPSectorPickup.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BagsArray implements Serializable {
    @SerializedName("bag")
    @Expose
    private String bag;
    @SerializedName("transit")
    @Expose
    private String transit;

    public String getBag() {
        return bag;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public String getTransit() {
        return transit;
    }

    public void setTransit(String transit) {
        this.transit = transit;
    }
}
