package com.criticalog.ecritica.MVPSectorPickup;

import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.AWBCheckResponse;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupRequest;
import com.criticalog.ecritica.MVPSectorPickup.Model.SectorPickupResponse;

public class SectorPickupPresenterImpl implements SectorPickupContract.presenter, SectorPickupContract.AWBCheckIntractor.OnFinishedListener, SectorPickupContract.SectorPickupIntractor.OnFinishedListener {
    private SectorPickupContract.MainView mainView;
    private SectorPickupContract.AWBCheckIntractor awbCheckIntractor;
    private SectorPickupContract.SectorPickupIntractor sectorPickupIntractor;

    public SectorPickupPresenterImpl(SectorPickupContract.MainView mainView, SectorPickupContract.AWBCheckIntractor awbCheckIntractor,
                                     SectorPickupContract.SectorPickupIntractor sectorPickupIntractor) {
        this.mainView = mainView;
        this.awbCheckIntractor = awbCheckIntractor;
        this.sectorPickupIntractor=sectorPickupIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
         mainView=null;
        }
    }

    @Override
    public void awbCheck(String token, AWBCheckRequest awbCheckRequest) {

        if (mainView != null) {
            mainView.showProgress();
        }
        awbCheckIntractor.awbCheckRequest(this, token, awbCheckRequest);
    }

    @Override
    public void sectorPickup(String token, SectorPickupRequest sectorPickupRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        sectorPickupIntractor.sectorPickupRequest(this,token,sectorPickupRequest);

    }

    @Override
    public void onFinished(AWBCheckResponse awbCheckResponse) {
        if (mainView != null) {
            mainView.setAWBCheckResponseToViews(awbCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(SectorPickupResponse sectorPickupResponse) {
        if(mainView!=null)
        {
            mainView.setSectorPickupDataToViews(sectorPickupResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {

        if(mainView!=null)
        {
            mainView.onResponseFailure(t);
        }
    }
}
