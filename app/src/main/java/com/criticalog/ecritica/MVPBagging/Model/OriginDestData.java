package com.criticalog.ecritica.MVPBagging.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OriginDestData {
    @SerializedName("hub_details")
    @Expose
    private List<OriginHubDetail> hubDetails = null;
    @SerializedName("dest_hub_details")
    @Expose
    private List<DestHubDetail> destHubDetails = null;

    public List<OriginHubDetail> getHubDetails() {
        return hubDetails;
    }

    public void setHubDetails(List<OriginHubDetail> hubDetails) {
        this.hubDetails = hubDetails;
    }

    public List<DestHubDetail> getDestHubDetails() {
        return destHubDetails;
    }

    public void setDestHubDetails(List<DestHubDetail> destHubDetails) {
        this.destHubDetails = destHubDetails;
    }
}
