package com.criticalog.ecritica.MVPBagging;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Adapters.BaggingDetailsrvAdapter;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitResponse;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationRequest;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationResponse;
import com.criticalog.ecritica.MVPBagging.Model.DestHubDetail;
import com.criticalog.ecritica.MVPBagging.Model.OriginHubDetail;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.ModelClasses.Hubdetails;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.Baggingdetails;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaggingActivity extends AppCompatActivity implements BaggingContract.MainView, View.OnClickListener {

    private BaggingContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token;
    private SimpleArcDialog mProgressBar;
    private Spinner mOriginDepoCode, mDestDepoCode;
    private EditText origindeponame, destdeponame, et_mbag, bagno;
    private Button scanMbagno, scanboxdetailsByBarcode, scanboxdetails;
    private String originHubCode = "", destHubCode = "", originHubCode1, destHubCode1;
    private TextView mbagtv;
    private ArrayList<Baggingdetails> list = new ArrayList<>();
    private ArrayList boxscannedlist = new ArrayList<String>();

    private EditText destdeponme, boxno, boxnonew;
    private String ori_hub_id, dest_hub_id, mbagresponse;
    private static String hub;
    private CriticalogSharedPreferences criticalogSharedPreferences;
    private LinearLayout layout, sublayout, progressbarlayout;
    private ImageView backbutton;
    private RecyclerView docketdetailsrv;
    private BaggingDetailsrvAdapter adapter;

    private Button submit, scanmbagno, mBarcodeScanMBag;
    private static String scanstatus;
    private int flag = 0;
    private AlertDialog DialogFinish;
    private String mBagNo;
    private Dialog dialogProductDelivered;
    ArrayAdapter<Hubdetails> adapter1;
    private RestServices mRestServices;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude = 0, longitude = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bagging);

        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);

        mOriginDepoCode = findViewById(R.id.oridepocode);
        mDestDepoCode = findViewById(R.id.destdepocode);
        origindeponame = findViewById(R.id.origindeponame);
        scanMbagno = findViewById(R.id.scanMbagno);
        scanboxdetails = findViewById(R.id.scanboxdetails);
        scanboxdetailsByBarcode = findViewById(R.id.scanboxdetailsByBarcode);
        et_mbag = findViewById(R.id.et_mbag);
        bagno = findViewById(R.id.bagno);
        mbagtv = findViewById(R.id.mbagtv);
        destdeponame = findViewById(R.id.destdeponame);
        boxno = findViewById(R.id.boxno);

        origindeponame = findViewById(R.id.origindeponame);
        destdeponme = findViewById(R.id.destdeponame);
        boxno = findViewById(R.id.boxno);
        bagno = findViewById(R.id.bagno);
        layout = findViewById(R.id.mainlayout);
        backbutton = findViewById(R.id.xtv_backbutton);
        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        docketdetailsrv = findViewById(R.id.docketdetailsrv);
        submit = findViewById(R.id.submit);
        scanmbagno = findViewById(R.id.scanMbagno);
        scanboxdetails = findViewById(R.id.scanboxdetails);
        sublayout = findViewById(R.id.sublayout);
        mbagtv = findViewById(R.id.mbagtv);
        et_mbag = findViewById(R.id.et_mbag);
        mBarcodeScanMBag = findViewById(R.id.mBarcodeScanMBag);

        scanMbagno.setOnClickListener(this);
        scanboxdetailsByBarcode.setOnClickListener(this);
        scanboxdetails.setOnClickListener(this);
        scanboxdetailsByBarcode.setOnClickListener(this);
        backbutton.setOnClickListener(this);
        //Loader
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        mCriticalogSharedPreferences.saveData("bagging_select_org_dest", "");

        BaggingOriginDestRequest baggingOriginDestRequest = new BaggingOriginDestRequest();
        baggingOriginDestRequest.setAction("bagging_orghub");
        baggingOriginDestRequest.setUserId(userId);
        baggingOriginDestRequest.setLatitude(String.valueOf(latitude));
        baggingOriginDestRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new BaggingPresenterImpl(this, new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl());
        mPresenter.loadOriginDestHubs(token, baggingOriginDestRequest);

        mBarcodeScanMBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criticalogSharedPreferences.saveData("scanstatus", "scanbag");
                startBarcodeScan();
            }
        });
        et_mbag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBagNo = s.toString();
                if (mBagNo.length() == 7) {
                    callMBagValidateAPI();
                } else {
                    scanboxdetails.setVisibility(View.GONE);
                    scanboxdetailsByBarcode.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        boxno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 9) {
                    if (boxscannedlist.contains(s.toString())) {
                        Toasty.warning(BaggingActivity.this, "Box already scanned", Toast.LENGTH_LONG, true).show();
                        opendialog();

                    } else {
                        String boxNumber = boxno.getText().toString();
                        BoxNumberValidationRequest boxNumberValidationRequest = new BoxNumberValidationRequest();
                        boxNumberValidationRequest.setAction("bagging_boxnumber");
                        boxNumberValidationRequest.setBoxNo(boxNumber);
                        boxNumberValidationRequest.setOrgHub(originHubCode);
                        boxNumberValidationRequest.setDestHub(destHubCode);
                        boxNumberValidationRequest.setUserId(userId);
                        boxNumberValidationRequest.setLatitude(String.valueOf(latitude));
                        boxNumberValidationRequest.setLongitude(String.valueOf(longitude));

                        mPresenter = new BaggingPresenterImpl(BaggingActivity.this, new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl());
                        mPresenter.boxValidationCheck(token, boxNumberValidationRequest);

                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
      /*Intent intent = new Intent(new Intent(BaggingActivity.this, HomeActivity.class));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
        finish();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(BaggingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(BaggingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BaggingActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(BaggingActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("LAT LONG_BAG", String.valueOf(latitude) + "" + String.valueOf(longitude));

                } else {
                    // mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    public void callMBagValidateAPI() {
        BagCheckRequest bagCheckRequest = new BagCheckRequest();
        bagCheckRequest.setAction("bagging_bag");
        bagCheckRequest.setBagNo(mBagNo);
        bagCheckRequest.setDestHub(destHubCode);
        bagCheckRequest.setUserId(userId);
        bagCheckRequest.setOrgHub(originHubCode);
        bagCheckRequest.setLatitude(String.valueOf(latitude));
        bagCheckRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new BaggingPresenterImpl(BaggingActivity.this, new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl());
        mPresenter.bagBaggingCheck(token, bagCheckRequest);
    }

    @Override
    public void showProgress() {

        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setOriginDestHubsToViews(BaggingOriginDestResponse baggingOriginDestResponse) {

        if (baggingOriginDestResponse != null) {
            if (baggingOriginDestResponse.getStatus() == 200) {
                ArrayList hublist = new ArrayList();

                List<OriginHubDetail> originHubDetails = baggingOriginDestResponse.getData().getHubDetails();

                Log.e("ORIGIN_HUB_SIZE", String.valueOf(originHubDetails.size()));
                for (int i = 0; i < originHubDetails.size(); i++) {

                    hublist.add(originHubDetails.get(i).getHubCode());
                }
                ArrayAdapter<Hubdetails> adapter = new ArrayAdapter<Hubdetails>(this,
                        android.R.layout.simple_spinner_item, hublist);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOriginDepoCode.setAdapter(adapter);
                mOriginDepoCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        originHubCode = originHubDetails.get(position).getHubId();
                        originHubCode1 = originHubDetails.get(position).getHubCode();

                        origindeponame.setText(originHubDetails.get(position).getHubName());
                        if (bagno.getVisibility() == View.VISIBLE && scanboxdetails.getVisibility() == View.VISIBLE &&
                                scanboxdetailsByBarcode.getVisibility() == View.VISIBLE) {
                            scanMbagno.setVisibility(View.VISIBLE);
                            mBarcodeScanMBag.setVisibility(View.VISIBLE);
                        }
                        if (mCriticalogSharedPreferences.getData("bagging_select_org_dest").equalsIgnoreCase("")) {
                            if (originHubCode.equalsIgnoreCase(destHubCode)) {

                                productDeliveredDialog();
                            } else {
                                origindeponame.setText(originHubDetails.get(position).getHubName());
                            }


                        } else {
                            scanboxdetailsByBarcode.setVisibility(View.VISIBLE);
                            scanboxdetails.setVisibility(View.VISIBLE);
                            scanboxdetails.setVisibility(View.GONE);
                            scanboxdetailsByBarcode.setVisibility(View.GONE);
                            mbagtv.setVisibility(View.GONE);
                            bagno.setVisibility(View.GONE);
                        }

                        //OriginHubDetail originHubDetail= (OriginHubDetail) parent.getSelectedItem();
                        //origindeponame.setText(originHubDetail.getHubName());
                        // Hubdetails user = (Hubdetails) parent.getSelectedItem();
                      /*  displayUserData(user);
                        flag = 1;*/
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    /*    flag = 0;
                        displayUserData((Hubdetails) originhublist.get(0));*/

                    }
                });

                //  JSONArray desthubdetails = object.getJSONArray("dest_hub_details");
                ArrayList desthub = new ArrayList();
                List<DestHubDetail> destHubDetails = baggingOriginDestResponse.getData().getDestHubDetails();
                Log.e("DEST_HUB_SIZE", String.valueOf(destHubDetails.size()));
                for (int i = 0; i < destHubDetails.size(); i++) {

                    desthub.add(destHubDetails.get(i).getHubCode());

                }

                /*ArrayAdapter<Hubdetails>*/
                adapter1 = new ArrayAdapter<Hubdetails>(this,
                        android.R.layout.simple_spinner_item, desthub);
                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mDestDepoCode.setAdapter(adapter1);
                mDestDepoCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        destHubCode = destHubDetails.get(position).getHubId();
                        destHubCode1 = destHubDetails.get(position).getHubCode();

                        if (mCriticalogSharedPreferences.getData("bagging_select_org_dest").equalsIgnoreCase("")) {
                            if (destHubCode.equalsIgnoreCase(originHubCode)) {

                                productDeliveredDialog();
                            } else {
                                destdeponame.setText(destHubDetails.get(position).getHubName());
                                //
                            }
                        } else {
                            scanboxdetailsByBarcode.setVisibility(View.VISIBLE);
                            scanboxdetails.setVisibility(View.VISIBLE);
                            scanboxdetailsByBarcode.setVisibility(View.GONE);
                            mbagtv.setVisibility(View.GONE);
                            bagno.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


            }
        }
    }


    public void productDeliveredDialog() {
        TextView mOkDialog, mDialogText;
        ImageView mRightMark;
        //Dialog Manual Entry
        dialogProductDelivered = new Dialog(BaggingActivity.this);
        dialogProductDelivered.setContentView(R.layout.dialog_product_delivered);
        mOkDialog = dialogProductDelivered.findViewById(R.id.mOk);
        mDialogText = dialogProductDelivered.findViewById(R.id.mDialogText);
        mRightMark = dialogProductDelivered.findViewById(R.id.mRightMark);
        mRightMark.setVisibility(View.GONE);

        mDialogText.setText("Origin and Destination Hub can't be same!!");
        dialogProductDelivered.setCancelable(false);
        dialogProductDelivered.show();

        mOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogProductDelivered.dismiss();
                mDestDepoCode.setAdapter(adapter1);
              /*  startActivity(new Intent(BaggingActivity.this, DRSActivity.class));
                finish();*/
            }
        });
    }

    @Override
    public void setBagCheckDataToViews(BagCheckResponse bagCheckResponse) {

        Log.e("RESPONSE_BAGG", bagCheckResponse.getMessage());
        if (bagCheckResponse.getStatus() == 200) {

            scanMbagno.setVisibility(View.GONE);
            bagno.setText(bagCheckResponse.getData().get(0).getBagNo());
            scanboxdetails.setVisibility(View.VISIBLE);
            scanboxdetailsByBarcode.setVisibility(View.VISIBLE);
            scanMbagno.setVisibility(View.GONE);
            mBarcodeScanMBag.setVisibility(View.GONE);
            mbagtv.setVisibility(View.VISIBLE);
            bagno.setVisibility(View.VISIBLE);
            et_mbag.setVisibility(View.GONE);
            mOriginDepoCode.setEnabled(false);
            mDestDepoCode.setEnabled(false);

            Toasty.success(this, bagCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();
            mCriticalogSharedPreferences.saveData("bagging_select_org_dest", "true");
        } else {
            scanboxdetailsByBarcode.setVisibility(View.VISIBLE);
            scanboxdetails.setVisibility(View.VISIBLE);
            scanboxdetails.setVisibility(View.GONE);
            scanboxdetailsByBarcode.setVisibility(View.GONE);
            mbagtv.setVisibility(View.GONE);
            bagno.setVisibility(View.GONE);
            //   scanMbagno.setVisibility(View.VISIBLE);
            mBarcodeScanMBag.setVisibility(View.VISIBLE);
            et_mbag.setText("");
            Toasty.warning(this, bagCheckResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void setBoxValidationResponseToViews(BoxNumberValidationResponse boxNumberValidationResponse) {
        Baggingdetails baggingdetails;
        if (boxNumberValidationResponse.getStatus() == 200) {
            String docketno = boxNumberValidationResponse.getData().getDocketNo().toString();
            String destcity = boxNumberValidationResponse.getData().getCityName();
            String box = boxNumberValidationResponse.getData().getBoxNo();

            boxno.setVisibility(View.GONE);
            baggingdetails = new Baggingdetails(box, docketno, destcity);

            list.add(0, baggingdetails);
            boxscannedlist.add(baggingdetails.getBoxno());


            if (!boxscannedlist.contains(box)) {
                boxscannedlist.add(baggingdetails.getBoxno());

            }

            sublayout.setVisibility(View.VISIBLE);

            adapter = new BaggingDetailsrvAdapter(list, BaggingActivity.this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
            docketdetailsrv.addItemDecoration(decoration);
            docketdetailsrv.setLayoutManager(layoutManager);
            docketdetailsrv.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            opendialog();


        } else {

            if (list.size() > 0) {
                sublayout.setVisibility(View.VISIBLE);
                opendialog();

            } else {
                scanboxdetails.setVisibility(View.VISIBLE);
                scanboxdetailsByBarcode.setVisibility(View.VISIBLE);
                sublayout.setVisibility(View.GONE);
            }
            boxno.setVisibility(View.GONE);
            Toasty.warning(this, boxNumberValidationResponse.getMessage(), Toast.LENGTH_LONG, true).show();

        }

    }

    @Override
    public void setBaggingSubmitDataToViews(BaggingSubmitResponse baggingSubmitResponse) {

        if (baggingSubmitResponse.getStatus() == 200) {
            Toasty.success(this, baggingSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            userRiderLog();
            Intent intent = new Intent(BaggingActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Toasty.warning(this, baggingSubmitResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }

    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Barcode");
      /*  if (mCriticalogSharedPreferences.getData("scanstatus").equalsIgnoreCase("scanbox")) {
            integrator.setPrompt("Scan Box Number");
        }*/
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(10000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toasty.warning(this, R.string.re_try, Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scanMbagno:
                et_mbag.setVisibility(View.VISIBLE);
                scanMbagno.setVisibility(View.GONE);
                break;

            case R.id.et_mbag:
                break;

            case R.id.scanboxdetailsByBarcode:
                criticalogSharedPreferences.saveData("scanstatus", "scanbox");
                startBarcodeScan();
                break;

            case R.id.scanboxdetails:
                criticalogSharedPreferences.saveData("scanstatus", "scanbox");
                // startBarcodeScan();
                scanboxdetails.setVisibility(View.GONE);

                //below code for tvs scanner
                boxno.setVisibility(View.VISIBLE);
                requestFocus(boxno);
                break;

            case R.id.xtv_backbutton:
                finish();
                break;
        }
    }

    void opendialog() {
        LayoutInflater finishDialog = LayoutInflater.from(BaggingActivity.this);
        final View finishDialogView = finishDialog.inflate(R.layout.scannextbox_dialog, null);
        DialogFinish = new AlertDialog.Builder(BaggingActivity.this).create();
        DialogFinish.setView(finishDialogView);
        DialogFinish.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DialogFinish.show();
        DialogFinish.setCanceledOnTouchOutside(false);

        Window window = DialogFinish.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        if (DialogFinish.isShowing()) {
            finishDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boxno.getText().clear();
                    // boxno.setVisibility(View.GONE);
                    DialogFinish.dismiss();
                    //startBarcodeScan();

                    //below code for tvs scanner
                    boxno.setVisibility(View.VISIBLE);
                    requestFocus(boxno);

                }
            });
            finishDialogView.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("requset ", "before submit" + ori_hub_id + dest_hub_id + bagno.getText().toString() + list);

                    BaggingSubmitRequest baggingSubmitRequest = new BaggingSubmitRequest();
                    baggingSubmitRequest.setAction("bagging_submit");
                    baggingSubmitRequest.setDestHub(destHubCode);
                    baggingSubmitRequest.setUserId(userId);
                    baggingSubmitRequest.setOriginHub(originHubCode);
                    baggingSubmitRequest.setMbagno(mBagNo);
                    baggingSubmitRequest.setBoxlist(boxscannedlist);
                    baggingSubmitRequest.setLatitude(String.valueOf(latitude));
                    baggingSubmitRequest.setLongitude(String.valueOf(longitude));

                    mPresenter = new BaggingPresenterImpl(BaggingActivity.this, new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl());
                    mPresenter.baggingSubmit(token, baggingSubmitRequest);
                    //  new BaggingBoxSubmitVolley(BaggingActivity.this, BaggingActivity.this,ori_hub_id,dest_hub_id,bagno.getText().toString(),boxscannedlist);

                }
            });
        }


    }

    public void userRiderLog() {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;

        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo("");
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId("");
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(mBagNo);
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("6");
        riderLogRequest.setmLastKm("");
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("3");
        riderLogRequest.setActionNumber(6);
        riderLogRequest.setOsVersion(androidOS);
        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        Log.d("result", result.toString());
        if (result != null) {
            try {
                String MobilePattern = "[0-9]{9}";
                String scannedresult = result.getContents();
                if (mCriticalogSharedPreferences.getData("scanstatus").equalsIgnoreCase("scanbox")) {
                    Log.d("result", mCriticalogSharedPreferences.getData("scanstatus"));

                    if (scannedresult == null) {
                        Log.d("inside", "scan box" + "else");
                        boxno.setVisibility(View.VISIBLE);
                        Log.d("inside else", "");


                    } else {

                        if (scannedresult.length() == 9) {
                            if (boxscannedlist.contains(scannedresult)) {
                                Toasty.warning(this, "Box already scanned", Toast.LENGTH_LONG, true).show();
                                opendialog();


                            } else {

                                BoxNumberValidationRequest boxNumberValidationRequest = new BoxNumberValidationRequest();
                                boxNumberValidationRequest.setAction("bagging_boxnumber");
                                boxNumberValidationRequest.setBoxNo(scannedresult);
                                boxNumberValidationRequest.setOrgHub(originHubCode);
                                boxNumberValidationRequest.setDestHub(destHubCode);
                                boxNumberValidationRequest.setUserId(userId);
                                boxNumberValidationRequest.setLatitude(String.valueOf(latitude));
                                boxNumberValidationRequest.setLongitude(String.valueOf(longitude));

                                mPresenter = new BaggingPresenterImpl(BaggingActivity.this, new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl(), new BaggingInteractorImpl());
                                mPresenter.boxValidationCheck(token, boxNumberValidationRequest);

                            }


                        } else {
                            Toasty.warning(this, "Scan 9 digit number", Toast.LENGTH_LONG, true).show();
                            //startBarcodeScan();
                        }
                    }


                } else {
                    if (scannedresult.length() == 7) {

                        mBagNo = scannedresult.toString();
                        callMBagValidateAPI();


                    } else {
                        scanboxdetails.setVisibility(View.GONE);
                        scanboxdetailsByBarcode.setVisibility(View.GONE);
                        Toasty.warning(this, "Scan Valid mBag Number!!", Toast.LENGTH_LONG, true).show();

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toasty.warning(this, R.string.scan_barcode, Toast.LENGTH_LONG, true).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
