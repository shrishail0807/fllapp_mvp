package com.criticalog.ecritica.MVPBagging.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BagCheckRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("org_hub")
    @Expose
    private String orgHub;
    @SerializedName("dest_hub")
    @Expose
    private String destHub;
    @SerializedName("bag_no")
    @Expose
    private String bagNo;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgHub() {
        return orgHub;
    }

    public void setOrgHub(String orgHub) {
        this.orgHub = orgHub;
    }

    public String getDestHub() {
        return destHub;
    }

    public void setDestHub(String destHub) {
        this.destHub = destHub;
    }

    public String getBagNo() {
        return bagNo;
    }

    public void setBagNo(String bagNo) {
        this.bagNo = bagNo;
    }

}
