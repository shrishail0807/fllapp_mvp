package com.criticalog.ecritica.MVPBagging;

import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitResponse;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationRequest;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationResponse;
import com.criticalog.ecritica.MVPInscan.InscanContract;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;

public interface BaggingContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void loadOriginDestHubs(String token, BaggingOriginDestRequest baggingOriginDestRequest);

        void bagBaggingCheck(String token, BagCheckRequest bagCheckRequest);

        void boxValidationCheck(String token, BoxNumberValidationRequest boxNumberValidationRequest);

        void baggingSubmit(String token, BaggingSubmitRequest baggingSubmitRequest);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setOriginDestHubsToViews(BaggingOriginDestResponse baggingOriginDestResponse);

        void setBagCheckDataToViews(BagCheckResponse bagCheckResponse);

        void setBoxValidationResponseToViews(BoxNumberValidationResponse boxNumberValidationResponse);

        void setBaggingSubmitDataToViews(BaggingSubmitResponse baggingSubmitResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface OriginDestHubIntractor {

        interface OnFinishedListener {
            void onFinished(BaggingOriginDestResponse baggingOriginDestResponse);

            void onFailure(Throwable t);
        }

        void getORiginDestHubRequest(BaggingContract.OriginDestHubIntractor.OnFinishedListener onFinishedListener, String token, BaggingOriginDestRequest baggingOriginDestRequest);
    }

    interface BagCheckIntractor {

        interface OnFinishedListener {
            void onFinished(BagCheckResponse bagCheckResponse);

            void onFailure(Throwable t);
        }

        void bagCheckRequest(BaggingContract.BagCheckIntractor.OnFinishedListener onFinishedListener, String token, BagCheckRequest bagCheckRequest);
    }

    interface BoxValodationIntractor {

        interface OnFinishedListener {
            void onFinished(BoxNumberValidationResponse boxNumberValidationResponse);

            void onFailure(Throwable t);
        }

        void boxValidationRequest(BaggingContract.BoxValodationIntractor.OnFinishedListener onFinishedListener, String token, BoxNumberValidationRequest boxNumberValidationRequest);
    }

    interface BaggingSubmitIntractor {

        interface OnFinishedListener {
            void onFinished(BaggingSubmitResponse baggingSubmitResponse);

            void onFailure(Throwable t);
        }

        void baggingSubmitRequest(BaggingContract.BaggingSubmitIntractor.OnFinishedListener onFinishedListener, String token, BaggingSubmitRequest baggingSubmitRequest);
    }
}
