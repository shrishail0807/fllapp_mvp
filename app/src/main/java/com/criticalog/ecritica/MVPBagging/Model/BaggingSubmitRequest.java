package com.criticalog.ecritica.MVPBagging.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BaggingSubmitRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("origin_hub")
    @Expose
    private String originHub;
    @SerializedName("dest_hub")
    @Expose
    private String destHub;
    @SerializedName("mbagno")
    @Expose
    private String mbagno;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("boxlist")
    @Expose
    private List<Integer> boxlist = null;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOriginHub() {
        return originHub;
    }

    public void setOriginHub(String originHub) {
        this.originHub = originHub;
    }

    public String getDestHub() {
        return destHub;
    }

    public void setDestHub(String destHub) {
        this.destHub = destHub;
    }

    public String getMbagno() {
        return mbagno;
    }

    public void setMbagno(String mbagno) {
        this.mbagno = mbagno;
    }

    public List<Integer> getBoxlist() {
        return boxlist;
    }

    public void setBoxlist(List<Integer> boxlist) {
        this.boxlist = boxlist;
    }
}
