package com.criticalog.ecritica.MVPBagging.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OriginHubDetail implements Serializable {
    @SerializedName("hub_id")
    @Expose
    private String hubId;
    @SerializedName("hub_code")
    @Expose
    private String hubCode;
    @SerializedName("hub_name")
    @Expose
    private String hubName;
    @SerializedName("hub_actual")
    @Expose
    private String hubActual;
    @SerializedName("hub_default")
    @Expose
    private String hubDefault;

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getHubCode() {
        return hubCode;
    }

    public void setHubCode(String hubCode) {
        this.hubCode = hubCode;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getHubActual() {
        return hubActual;
    }

    public void setHubActual(String hubActual) {
        this.hubActual = hubActual;
    }

    public String getHubDefault() {
        return hubDefault;
    }

    public void setHubDefault(String hubDefault) {
        this.hubDefault = hubDefault;
    }
}
