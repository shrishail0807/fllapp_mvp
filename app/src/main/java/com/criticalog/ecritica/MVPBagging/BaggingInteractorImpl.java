package com.criticalog.ecritica.MVPBagging;

import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitResponse;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationRequest;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaggingInteractorImpl implements BaggingContract.OriginDestHubIntractor,BaggingContract.BagCheckIntractor,BaggingContract.BoxValodationIntractor, BaggingContract.BaggingSubmitIntractor {
    RestServices services= RestClient.getRetrofitInstance().create(RestServices.class);
    @Override
    public void getORiginDestHubRequest(BaggingContract.OriginDestHubIntractor.OnFinishedListener onFinishedListener, String token, BaggingOriginDestRequest baggingOriginDestRequest) {
        Call<BaggingOriginDestResponse> baggingOriginDestResponseCall=services.baggingOriginDestinationHub(baggingOriginDestRequest);
        baggingOriginDestResponseCall.enqueue(new Callback<BaggingOriginDestResponse>() {
            @Override
            public void onResponse(Call<BaggingOriginDestResponse> call, Response<BaggingOriginDestResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<BaggingOriginDestResponse> call, Throwable t) {
             onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void bagCheckRequest(BaggingContract.BagCheckIntractor.OnFinishedListener onFinishedListener, String token, BagCheckRequest bagCheckRequest) {

        Call<BagCheckResponse> bagCheckResponseCall=services.baggingBag(bagCheckRequest);

        bagCheckResponseCall.enqueue(new Callback<BagCheckResponse>() {
            @Override
            public void onResponse(Call<BagCheckResponse> call, Response<BagCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<BagCheckResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void boxValidationRequest(BaggingContract.BoxValodationIntractor.OnFinishedListener onFinishedListener, String token, BoxNumberValidationRequest boxNumberValidationRequest) {
        Call<BoxNumberValidationResponse> boxNumberValidationResponseCall=services.boxValidationBagging(boxNumberValidationRequest);
        boxNumberValidationResponseCall.enqueue(new Callback<BoxNumberValidationResponse>() {
            @Override
            public void onResponse(Call<BoxNumberValidationResponse> call, Response<BoxNumberValidationResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<BoxNumberValidationResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void baggingSubmitRequest(BaggingContract.BaggingSubmitIntractor.OnFinishedListener onFinishedListener, String token, BaggingSubmitRequest baggingSubmitRequest) {
        Call<BaggingSubmitResponse> baggingSubmitResponseCall=services.baggingSubmit(baggingSubmitRequest);
        baggingSubmitResponseCall.enqueue(new Callback<BaggingSubmitResponse>() {
            @Override
            public void onResponse(Call<BaggingSubmitResponse> call, Response<BaggingSubmitResponse> response) {
                onFinishedListener.onFinished(response.body());
            }
            @Override
            public void onFailure(Call<BaggingSubmitResponse> call, Throwable t) {
               onFinishedListener.onFailure(t);
            }
        });
    }
}
