package com.criticalog.ecritica.MVPBagging.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BagCheckDatum {
    @SerializedName("hub")
    @Expose
    private String hub;
    @SerializedName("bag_no")
    @Expose
    private String bagNo;

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getBagNo() {
        return bagNo;
    }

    public void setBagNo(String bagNo) {
        this.bagNo = bagNo;
    }
}
