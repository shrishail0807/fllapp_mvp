package com.criticalog.ecritica.MVPBagging;

import com.criticalog.ecritica.MVPBagging.Model.BagCheckRequest;
import com.criticalog.ecritica.MVPBagging.Model.BagCheckResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingOriginDestResponse;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitRequest;
import com.criticalog.ecritica.MVPBagging.Model.BaggingSubmitResponse;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationRequest;
import com.criticalog.ecritica.MVPBagging.Model.BoxNumberValidationResponse;

public class BaggingPresenterImpl implements BaggingContract.presenter, BaggingContract.OriginDestHubIntractor.OnFinishedListener, BaggingContract.BagCheckIntractor.OnFinishedListener, BaggingContract.BoxValodationIntractor.OnFinishedListener, BaggingContract.BaggingSubmitIntractor.OnFinishedListener {

    private BaggingContract.MainView mainView;
    private BaggingContract.OriginDestHubIntractor originDestHubIntractor;
    private BaggingContract.BagCheckIntractor bagCheckIntractor;
    private BaggingContract.BoxValodationIntractor boxValodationIntractor;
    private BaggingContract.BaggingSubmitIntractor baggingSubmitIntractor;

    public BaggingPresenterImpl(BaggingContract.MainView mainView, BaggingContract.OriginDestHubIntractor originDestHubIntractor,
                                BaggingContract.BagCheckIntractor bagCheckIntractor,
                                BaggingContract.BoxValodationIntractor boxValodationIntractor,
                                BaggingContract.BaggingSubmitIntractor baggingSubmitIntractor) {
        this.mainView = mainView;
        this.originDestHubIntractor = originDestHubIntractor;
        this.bagCheckIntractor=bagCheckIntractor;
        this.boxValodationIntractor=boxValodationIntractor;
        this.baggingSubmitIntractor=baggingSubmitIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void loadOriginDestHubs(String token, BaggingOriginDestRequest baggingOriginDestRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        originDestHubIntractor.getORiginDestHubRequest(this, token, baggingOriginDestRequest);
    }

    @Override
    public void bagBaggingCheck(String token, BagCheckRequest bagCheckRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        bagCheckIntractor.bagCheckRequest(this,token,bagCheckRequest);
    }

    @Override
    public void boxValidationCheck(String token, BoxNumberValidationRequest boxNumberValidationRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        boxValodationIntractor.boxValidationRequest(this,token,boxNumberValidationRequest);
    }

    @Override
    public void baggingSubmit(String token, BaggingSubmitRequest baggingSubmitRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        baggingSubmitIntractor.baggingSubmitRequest(this,token,baggingSubmitRequest);

    }

    @Override
    public void onFinished(BaggingOriginDestResponse baggingOriginDestResponse) {

        if (mainView != null) {
            mainView.setOriginDestHubsToViews(baggingOriginDestResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(BagCheckResponse bagCheckResponse) {
        if(mainView!=null)
        {
            mainView.setBagCheckDataToViews(bagCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(BoxNumberValidationResponse boxNumberValidationResponse) {
        if(mainView!=null)
        {
            mainView.setBoxValidationResponseToViews(boxNumberValidationResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(BaggingSubmitResponse baggingSubmitResponse) {
        if(mainView!=null)
        {
            mainView.setBaggingSubmitDataToViews(baggingSubmitResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {

        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
