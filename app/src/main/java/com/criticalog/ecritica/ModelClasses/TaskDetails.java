package com.criticalog.ecritica.ModelClasses;

import java.io.Serializable;



public class TaskDetails  implements Serializable {



    String task_id;
    String delivery_date;
    String delivery_time;
    String codtask_id;
    String paytype;


    public TaskDetails(String task_id, String delivery_date, String delivery_time, String codtask_id, String paytype) {
        this.task_id = task_id;
        this.delivery_date = delivery_date;
        this.delivery_time = delivery_time;
        this.codtask_id = codtask_id;
        this.paytype = paytype;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getCodtask_id() {
        return codtask_id;
    }

    public void setCodtask_id(String codtask_id) {
        this.codtask_id = codtask_id;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    @Override
    public String toString() {
        return "TaskDetails{" +
                "task_id='" + task_id + '\'' +
                ", delivery_date='" + delivery_date + '\'' +
                ", delivery_time='" + delivery_time + '\'' +
                ", codtask_id='" + codtask_id + '\'' +
                ", paytype='" + paytype + '\'' +
                '}';
    }
}
