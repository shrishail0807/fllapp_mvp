package com.criticalog.ecritica.ModelClasses;

public class ReasonModel {

    private String reason;
    private String reasonCode;

    public ReasonModel(String reason, String reasonCode) {
        this.reason=reason;
        this.reasonCode=reasonCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @Override
    public String toString() {
        return reason;
    }
}
