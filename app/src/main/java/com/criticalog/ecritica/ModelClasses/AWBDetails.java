package com.criticalog.ecritica.ModelClasses;

import java.io.Serializable;

public class AWBDetails implements Serializable {


    private String AirwayBill;
    private String Vendor;
    private String flight;
    private String bookcity;
    private String bookby;
    private String bookdt;
    private String booktm;
    private String pickcity;
    private String pickedby;
    private String pickdt;
    private String picktm;


    public AWBDetails(String airwayBill, String vendor, String flight, String bookcity, String bookby, String bookdt, String booktm, String pickcity, String pickedby, String pickdt, String picktm) {
        AirwayBill = airwayBill;
        Vendor = vendor;
        this.flight = flight;
        this.bookcity = bookcity;
        this.bookby = bookby;
        this.bookdt = bookdt;
        this.booktm = booktm;
        this.pickcity = pickcity;
        this.pickedby = pickedby;
        this.pickdt = pickdt;
        this.picktm = picktm;
    }

    public String getAirwayBill() {
        return AirwayBill;
    }

    public void setAirwayBill(String airwayBill) {
        AirwayBill = airwayBill;
    }

    public String getVendor() {
        return Vendor;
    }

    public void setVendor(String vendor) {
        Vendor = vendor;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getBookcity() {
        return bookcity;
    }

    public void setBookcity(String bookcity) {
        this.bookcity = bookcity;
    }

    public String getBookby() {
        return bookby;
    }

    public void setBookby(String bookby) {
        this.bookby = bookby;
    }

    public String getBookdt() {
        return bookdt;
    }

    public void setBookdt(String bookdt) {
        this.bookdt = bookdt;
    }

    public String getBooktm() {
        return booktm;
    }

    public void setBooktm(String booktm) {
        this.booktm = booktm;
    }

    public String getPickcity() {
        return pickcity;
    }

    public void setPickcity(String pickcity) {
        this.pickcity = pickcity;
    }

    public String getPickedby() {
        return pickedby;
    }

    public void setPickedby(String pickedby) {
        this.pickedby = pickedby;
    }

    public String getPickdt() {
        return pickdt;
    }

    public void setPickdt(String pickdt) {
        this.pickdt = pickdt;
    }

    public String getPicktm() {
        return picktm;
    }

    public void setPicktm(String picktm) {
        this.picktm = picktm;
    }
}
