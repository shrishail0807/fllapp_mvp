package com.criticalog.ecritica.ModelClasses;

import java.util.ArrayList;
import java.util.Arrays;

public class TaskOngoingResponse {

    public String taskid;
    public String customername, phone_num, droptime, address1, address2, address3,driver_id,city_id,state,pincode,drs_no,bulkStatus,swap,modeofpay,reversecon,otp_flag,Client_code,consignee,round;
    public int flag,otp_verified;
    public double latitude, longitude;
    ArrayList<TaskDetails>[] clientsExtract1 = new ArrayList[100];

    public ArrayList<TaskDetails>[] getClientsExtract1() {
        return clientsExtract1;
    }

    public void setClientsExtract1(ArrayList<TaskDetails>[] clientsExtract1) {
        this.clientsExtract1 = clientsExtract1;
    }


    public int getOtp_verified() {
        return otp_verified;
    }

    public void setOtp_verified(int otp_verified) {
        this.otp_verified = otp_verified;
    }

    public TaskOngoingResponse(String customername, String phone_num, String droptime, String address1, String address2, String address3, String driver_id, String city_id, String state, String pincode, String drs_no, String bulkStatus, String swap, String modeofpay, String reversecon, String otp_flag, String client_code, String consignee, String round, ArrayList<TaskDetails>[] clientsExtract1,int otp_verified) {
        this.customername = customername;
        this.phone_num = phone_num;
        this.droptime = droptime;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.driver_id = driver_id;
        this.city_id = city_id;
        this.state = state;
        this.pincode = pincode;
        this.drs_no = drs_no;
        this.bulkStatus = bulkStatus;
        this.swap = swap;
        this.modeofpay = modeofpay;
        this.reversecon = reversecon;
        this.otp_flag = otp_flag;
        Client_code = client_code;
        this.consignee = consignee;
        this.round = round;

        this.clientsExtract1 = clientsExtract1;
        this.otp_verified = otp_verified;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getDroptime() {
        return droptime;
    }

    public void setDroptime(String droptime) {
        this.droptime = droptime;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDrs_no() {
        return drs_no;
    }

    public void setDrs_no(String drs_no) {
        this.drs_no = drs_no;
    }

    public String getBulkStatus() {
        return bulkStatus;
    }

    public void setBulkStatus(String bulkStatus) {
        this.bulkStatus = bulkStatus;
    }

    public String getSwap() {
        return swap;
    }

    public void setSwap(String swap) {
        this.swap = swap;
    }

    public String getModeofpay() {
        return modeofpay;
    }

    public void setModeofpay(String modeofpay) {
        this.modeofpay = modeofpay;
    }

    public String getReversecon() {
        return reversecon;
    }

    public void setReversecon(String reversecon) {
        this.reversecon = reversecon;
    }

    public String getOtp_flag() {
        return otp_flag;
    }

    public void setOtp_flag(String otp_flag) {
        this.otp_flag = otp_flag;
    }

    public String getClient_code() {
        return Client_code;
    }

    public void setClient_code(String client_code) {
        Client_code = client_code;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }





    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    @Override
    public String toString() {
        return "TaskOngoingResponse{" +
                "taskid='" + taskid + '\'' +
                ", customername='" + customername + '\'' +
                ", phone_num='" + phone_num + '\'' +
                ", droptime='" + droptime + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", driver_id='" + driver_id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", state='" + state + '\'' +
                ", pincode='" + pincode + '\'' +
                ", drs_no='" + drs_no + '\'' +
                ", bulkStatus='" + bulkStatus + '\'' +
                ", swap='" + swap + '\'' +
                ", modeofpay='" + modeofpay + '\'' +
                ", reversecon='" + reversecon + '\'' +
                ", otp_flag='" + otp_flag + '\'' +
                ", Client_code='" + Client_code + '\'' +
                ", consignee='" + consignee + '\'' +
                ", round='" + round + '\'' +
                ", flag=" + flag +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", clientsExtract1=" + Arrays.toString(clientsExtract1) +
                '}';
    }
}
