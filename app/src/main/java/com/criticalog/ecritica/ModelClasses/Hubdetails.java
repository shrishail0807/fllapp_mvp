package com.criticalog.ecritica.ModelClasses;

public class Hubdetails {
    private String hubcode;
    private String hubname;
    private String hubid;
    private String hubdefault;
    private String hubactual;

    public Hubdetails(String hubcode, String hubname, String hubid, String hubdefault, String hubactual) {
        this.hubcode = hubcode;
        this.hubname = hubname;
        this.hubid = hubid;
        this.hubdefault = hubdefault;
        this.hubactual = hubactual;
    }

    public String getHubcode() {
        return hubcode;
    }

    public void setHubcode(String hubcode) {
        this.hubcode = hubcode;
    }

    public String getHubname() {
        return hubname;
    }

    public void setHubname(String hubname) {
        this.hubname = hubname;
    }

    public String getHubid() {
        return hubid;
    }

    public void setHubid(String hubid) {
        this.hubid = hubid;
    }

    public String getHubdefault() {
        return hubdefault;
    }

    public void setHubdefault(String hubdefault) {
        this.hubdefault = hubdefault;
    }

    public String getHubactual() {
        return hubactual;
    }

    public void setHubactual(String hubactual) {
        this.hubactual = hubactual;
    }

    @Override
    public String toString() {
        return hubcode;
    }
}
