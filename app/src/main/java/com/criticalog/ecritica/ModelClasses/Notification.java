package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 7/9/2018.
 */

public class Notification {

    String taskId;
    String bulkStatus;

    public String getBulkStatus() {
        return bulkStatus;
    }

    public void setBulkStatus(String bulkStatus) {
        this.bulkStatus = bulkStatus;
    }

    public Notification(String taskId,String bulkStatus) {
        this.taskId = taskId;
        this.bulkStatus=bulkStatus;

    }

    public String getTaskId() {
        return taskId;
    }
}
