package com.criticalog.ecritica.ModelClasses;

import java.io.Serializable;

public class PickupSuccess implements Serializable {

    private String docketNo;
    private String userId;
    private String clientCode;
    private String boxNo;
   /* private String date;
    private String time;*/
    private String prs;

    public PickupSuccess(String docketNo, String userId, String clientCode, String boxNo,String prs) {
        this.docketNo = docketNo;
        this.userId = userId;
        this.clientCode = clientCode;
        this.boxNo = boxNo;
     /*   this.date = date;
        this.time = time;*/
        this.prs = prs;
    }
    public PickupSuccess() {

    }
    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getBoxNo() {
        return boxNo;
    }

    public void setBoxNo(String boxNo) {
        this.boxNo = boxNo;
    }

   /* public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }*/

    public String getPrs() {
        return prs;
    }

    public void setPrs(String prs) {
        this.prs = prs;
    }
}
