package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 6/22/2018.
 */

public class Drop_row {
    String time, drop_ID;

    public Drop_row(String time, String drop_ID) {
        this.time = time;
        this.drop_ID = drop_ID;
    }

    public String getTime() {
        return time;
    }

    public String getDrop_ID() {
        return drop_ID;
    }

}
