package com.criticalog.ecritica.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpenImage {
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("label")
    @Expose
    private String label;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
