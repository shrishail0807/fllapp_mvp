package com.criticalog.ecritica.ModelClasses;

import java.io.Serializable;

public class DeliveryDetails implements Serializable{

    private String taskId;
    private String userId;
    private String receiverName;
    private String status;
    private String refCount;
    private String bulkFlag;
    private String date;
    private String time;
    private byte[] fileBytes;
    private byte[] fileBytes1;
   // private byte[] signatureBytes;
    private byte[] fileBytesPod;
    private String swap;
    private double latitude;
    private double longitude;
    private String cheque;
    private String amount;
    private String modeofpay;
    private String codtaskId;

    private String codamount;
    private String codpaytype;
    private String upirefno;
    private String dttime;

    public DeliveryDetails() {
    }


    public DeliveryDetails(String taskId, String userId, String receiverName, String status, String refCount, String bulkFlag, String date, String time, byte[] fileBytes, byte[] fileBytes1, byte[] fileBytesPod, String swap, double latitude, double longitude, String cheque, String amount, String modeofpay, String codtaskId,String codamount,String codpaytype,String upirefno,String dttime) {
        this.taskId = taskId;
        this.userId = userId;
        this.receiverName = receiverName;
        this.status = status;
        this.refCount = refCount;
        this.bulkFlag = bulkFlag;
        this.date = date;
        this.time = time;
        this.fileBytes = fileBytes;
        this.fileBytes1 = fileBytes1;
        this.fileBytesPod = fileBytesPod;
        this.swap = swap;
        this.latitude = latitude;
        this.longitude = longitude;
        this.cheque = cheque;
        this.amount = amount;
        this.modeofpay = modeofpay;
        this.codtaskId = codtaskId;
        this.codamount = codamount;
        this.codpaytype = codpaytype;
        this.upirefno = upirefno ;
        this.dttime = dttime ;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefCount() {
        return refCount;
    }

    public void setRefCount(String refCount) {
        this.refCount = refCount;
    }

    public String getBulkFlag() {
        return bulkFlag;
    }

    public void setBulkFlag(String bulkFlag) {
        this.bulkFlag = bulkFlag;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public String getSwap() {
        return swap;
    }

    public void setSwap(String swap) {
        this.swap = swap;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCheque() {
        return cheque;
    }

    public void setCheque(String cheque) {
        this.cheque = cheque;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getModeofpay() {
        return modeofpay;
    }

    public void setModeofpay(String modeofpay) {
        this.modeofpay = modeofpay;
    }

    public String getCodtaskId() {
        return codtaskId;
    }

    public void setCodtaskId(String codtaskId) {
        this.codtaskId = codtaskId;
    }

    public byte[] getFileBytes1() {
        return fileBytes1;
    }

    public void setFileBytes1(byte[] fileBytes1) {
        this.fileBytes1 = fileBytes1;
    }

    public byte[] getFileBytesPod() {
        return fileBytesPod;
    }

    public void setFileBytesPod(byte[] fileBytesPod) {
        this.fileBytesPod = fileBytesPod;
    }

    public String getCodamount() {
        return codamount;
    }

    public void setCodamount(String codamount) {
        this.codamount = codamount;
    }

    public String getCodpaytype() {
        return codpaytype;
    }

    public void setCodpaytype(String codpaytype) {
        this.codpaytype = codpaytype;
    }

    public String getUpirefno() {
        return upirefno;
    }

    public void setUpirefno(String upirefno) {
        this.upirefno = upirefno;
    }

    public String getDttime() {
        return dttime;
    }

    public void setDttime(String dttime) {
        this.dttime = dttime;
    }
}
