package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 8/17/2018.
 */

public class DRS_data {

    public DRS_data(String drs_id) {
        this.drs_id = drs_id;
    }

    public String getDrs_id() {
        return drs_id;
    }

    public void setDrs_id(String drs_id) {
        this.drs_id = drs_id;
    }

    private String drs_id;
}
