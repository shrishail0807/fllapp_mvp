package com.criticalog.ecritica.ModelClasses;

public class CancelTask  {

    private String taskId;
    private String userId;
    private String refCount;


    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    private String refNumber;
    private String bulkFlag;
    private String date;
    private String time;
    private String reason;
    private double latitude;
    private double longitude;



    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }



    public CancelTask(String taskId, String userId, String refCount, String bulkFlag,String refNumber, String date, String time, String reason,double latitude,double longitude) {
        this.taskId = taskId;
        this.userId = userId;
        this.refCount = refCount;
        this.bulkFlag = bulkFlag;
        this.refNumber = refNumber;
        this.date = date;
        this.time = time;
        this.reason = reason;
        this.latitude = latitude;
        this.longitude = longitude;


    }

    public CancelTask() {
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRefCount() {
        return refCount;
    }

    public void setRefCount(String refCount) {
        this.refCount = refCount;
    }

    public String getBulkFlag() {
        return bulkFlag;
    }

    public void setBulkFlag(String bulkFlag) {
        this.bulkFlag = bulkFlag;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
