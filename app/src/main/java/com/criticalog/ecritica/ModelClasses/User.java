package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 7/2/2018.
 */

public class User {
    private String id, username;

    public User(String id, String username) {
        this.id = id;
        this.username = username;

    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
