package com.criticalog.ecritica.ModelClasses;

public class UserDetailsBulk {
    private String DRS_NO;
    private String name;
    private String addr1;
    private int pinCode;
    private String phone;
    private String state;
    private String driverId;
    private String time;
    private String lat;
    private String longt;
    private String cityId;
    private String mcmpcd;

    public UserDetailsBulk(String name, String addr1, int pinCode, String phone, String state, String drs_no, String driverId, String time, String lat, String longt, String cityId, String mcmpcd) {
        this.name = name;
        this.addr1 = addr1;
        this.pinCode = pinCode;
        this.phone = phone;
        this.state = state;
        this.DRS_NO = drs_no;
        this.driverId = driverId;
        this.time = time;
        this.lat = lat;
        this.longt = longt;
        this.cityId = cityId;
        this.mcmpcd = mcmpcd;


    }

    public String getDRS_NO() {
        return DRS_NO;
    }


    public int getPinCode() {
        return pinCode;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getTime() {
        return time;
    }

    public String getLat() {
        return lat;
    }

    public String getLongt() {
        return longt;
    }

    public String getCityId() {
        return cityId;
    }

    public String getMcmpcd() {
        return mcmpcd;
    }

    public String getName() {
        return name;
    }


    public String getAddr1() {
        return addr1;
    }


    public String getPhone() {
        return phone;
    }


    public String getState() {
        return state;
    }

}
