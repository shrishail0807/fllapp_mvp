package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 6/20/2018.
 */

public class Pickup_row {
    String time, pickup_ID;

    public Pickup_row(String time, String pickup_ID) {
        this.time = time;
        this.pickup_ID = pickup_ID;
    }

    public String getTime() {
        return time;
    }

    public String getPickup_ID() {
        return pickup_ID;
    }

}
