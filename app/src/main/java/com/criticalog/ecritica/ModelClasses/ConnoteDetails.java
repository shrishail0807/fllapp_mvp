package com.criticalog.ecritica.ModelClasses;


import java.io.Serializable;

public class ConnoteDetails implements Serializable {
    private String client_code;
    private String client_name;
    private String consignor_code;
    private String consignor_name;
    private String consignor_add;
    private String consignor_city_loc;
    private String consignor_city;
    private String consignor_pin;
    private String consignor_state;
    private String consignee_code;
    private String consignee_name;
    private String consignee_add;
    private String consignee_city_loc;
    private String consignee_pin;
    private String consignee_state;
    private String pod;
    private String edp;

    private String BagNumber;
    private String payment_mode;
    private String XPAYVALUE;
    private String product_mode;
    private String shipping_type;
    private String pick_date;
    private String pick_time;
    private String expected_dt;
    private String pickedby;
    private String expected_tm;
    private String pieces;
    private String actual_value;
    private String gross_wt;

    private String bill_wt;
    private String dim_wt;
    private String XPRTCD;
    private String l;
    private String b;
    private String h;
    private String service_type;

    public ConnoteDetails(String client_code, String client_name, String consignor_code, String consignor_name, String consignor_add, String consignor_city_loc, String consignor_city, String consignor_pin,String consignor_state, String consignee_code, String consignee_name, String consignee_add, String consignee_city_loc, String consignee_pin,String consignee_state, String bagNumber, String payment_mode, String XPAYVALUE, String product_mode, String shipping_type, String pick_date, String pick_time, String expected_dt, String pickedby, String expected_tm, String pieces, String actual_value, String gross_wt, String bill_wt, String dim_wt, String XPRTCD, String l, String b, String h, String service_type,String pod,String edp) {
        this.client_code = client_code;
        this.client_name = client_name;
        this.consignor_code = consignor_code;
        this.consignor_name = consignor_name;
        this.consignor_add = consignor_add;
        this.consignor_city_loc = consignor_city_loc;
        this.consignor_city = consignor_city;
        this.consignor_pin = consignor_pin;
        this.consignor_state = consignor_state;
        this.consignee_code = consignee_code;
        this.consignee_name = consignee_name;
        this.consignee_add = consignee_add;
        this.consignee_city_loc = consignee_city_loc;
        this.consignee_pin = consignee_pin;
        this.consignee_state = consignee_state;
        this.payment_mode = payment_mode;
        this.XPAYVALUE = XPAYVALUE;
        this.product_mode = product_mode;
        this.shipping_type = shipping_type;
        this.pick_date = pick_date;
        this.pick_time = pick_time;
        this.expected_dt = expected_dt;
        this.pickedby = pickedby;
        this.expected_tm = expected_tm;
        this.pieces = pieces;
        this.actual_value = actual_value;
        this.gross_wt = gross_wt;
        this.bill_wt = bill_wt;
        this.dim_wt = dim_wt;
        this.XPRTCD = XPRTCD;
        this.l = l;
        this.b = b;
        this.h = h;
        this.pod = pod;
        this.edp = edp;
        this.service_type = service_type;
    }

    public String getClient_code() {
        return client_code;
    }

    public String getClient_name() {
        return client_name;
    }

    public String getConsignor_code() {
        return consignor_code;
    }

    public String getConsignor_name() {
        return consignor_name;
    }

    public String getConsignor_add() {
        return consignor_add;
    }

    public String getConsignor_city_loc() {
        return consignor_city_loc;
    }

    public String getConsignor_city() {
        return consignor_city;
    }

    public String getConsignor_pin() {
        return consignor_pin;
    }

    public String getConsignor_state() {
        return consignor_state;
    }

    public String getConsignee_code() {
        return consignee_code;
    }

    public String getConsignee_name() {
        return consignee_name;
    }

    public String getConsignee_add() {
        return consignee_add;
    }

    public String getConsignee_city_loc() {
        return consignee_city_loc;
    }

    public String getConsignee_pin() {
        return consignee_pin;
    }

    public String getConsignee_state() {
        return consignee_state;
    }

    public String getPod() {
        return pod;
    }

    public String getEdp() {
        return edp;
    }

    public String getBagNumber() {
        return BagNumber;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public String getXPAYVALUE() {
        return XPAYVALUE;
    }

    public String getProduct_mode() {
        return product_mode;
    }

    public String getShipping_type() {
        return shipping_type;
    }

    public String getPick_date() {
        return pick_date;
    }

    public String getPick_time() {
        return pick_time;
    }

    public String getExpected_dt() {
        return expected_dt;
    }

    public String getPickedby() {
        return pickedby;
    }

    public String getExpected_tm() {
        return expected_tm;
    }

    public String getPieces() {
        return pieces;
    }

    public String getActual_value() {
        return actual_value;
    }

    public String getGross_wt() {
        return gross_wt;
    }

    public String getBill_wt() {
        return bill_wt;
    }

    public String getDim_wt() {
        return dim_wt;
    }

    public String getXPRTCD() {
        return XPRTCD;
    }

    public String getL() {
        return l;
    }

    public String getB() {
        return b;
    }

    public String getH() {
        return h;
    }

    public String getService_type() {
        return service_type;
    }

    @Override
    public String toString() {
        return "ConnoteDetails{" +
                "client_code='" + client_code + '\'' +
                ", client_name='" + client_name + '\'' +
                ", consignor_code='" + consignor_code + '\'' +
                ", consignor_name='" + consignor_name + '\'' +
                ", consignor_add='" + consignor_add + '\'' +
                ", consignor_city_loc='" + consignor_city_loc + '\'' +
                ", consignor_city='" + consignor_city + '\'' +
                ", consignor_pin='" + consignor_pin + '\'' +
                ", consignor_state='" + consignor_state + '\'' +
                ", consignee_code='" + consignee_code + '\'' +
                ", consignee_name='" + consignee_name + '\'' +
                ", consignee_add='" + consignee_add + '\'' +
                ", consignee_city_loc='" + consignee_city_loc + '\'' +
                ", consignee_pin='" + consignee_pin + '\'' +
                ", consignee_state='" + consignee_state + '\'' +
                ", pod='" + pod + '\'' +
                ", edp='" + edp + '\'' +
                ", BagNumber='" + BagNumber + '\'' +
                ", payment_mode='" + payment_mode + '\'' +
                ", XPAYVALUE='" + XPAYVALUE + '\'' +
                ", product_mode='" + product_mode + '\'' +
                ", shipping_type='" + shipping_type + '\'' +
                ", pick_date='" + pick_date + '\'' +
                ", pick_time='" + pick_time + '\'' +
                ", expected_dt='" + expected_dt + '\'' +
                ", pickedby='" + pickedby + '\'' +
                ", expected_tm='" + expected_tm + '\'' +
                ", pieces='" + pieces + '\'' +
                ", actual_value='" + actual_value + '\'' +
                ", gross_wt='" + gross_wt + '\'' +
                ", bill_wt='" + bill_wt + '\'' +
                ", dim_wt='" + dim_wt + '\'' +
                ", XPRTCD='" + XPRTCD + '\'' +
                ", l='" + l + '\'' +
                ", b='" + b + '\'' +
                ", h='" + h + '\'' +
                ", service_type='" + service_type + '\'' +
                '}';
    }
}

