package com.criticalog.ecritica.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PartDetails implements Serializable {

    @Expose
    @SerializedName("XDDQTY")
    private String pcs;
    @Expose
    @SerializedName("XDDDESC")
    private String desc;

    @Expose
    @SerializedName("XDDIMLENGTH")
    private String length;

    @Expose
    @SerializedName("XDDIMBREADTH")
    private String breadth;

    @Expose
    @SerializedName("XDDIMHEIGHT")
    private String height;

    @Expose
    @SerializedName("XINVOICENM")
    private String invoiceno;

    public PartDetails(String pcs, String desc, String length, String breadth, String height,String invoiceno) {
        this.pcs = pcs;
        this.desc = desc;
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        this.invoiceno = invoiceno;
    }

    public String getPcs() {
        return pcs;
    }

    public void setPcs(String pcs) {
        this.pcs = pcs;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getInvoiceno() {
        return invoiceno;
    }

    public void setInvoiceno(String invoiceno) {
        this.invoiceno = invoiceno;
    }
}
