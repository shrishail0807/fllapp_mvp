package com.criticalog.ecritica.ModelClasses;

import java.io.File;
import java.io.Serializable;

public class DeliveryService implements Serializable {

    private String taskId;
    private String userId;
    private String receiverName;
    private String status;
    private String refCount;
    private String bulkFlag;
    private File imageFile;
    private File signatureFile;

    public DeliveryService(String taskId, String userId, String receiverName, String status, String refCount, String bulkFlag, File imageFile, File signatureFile) {
        this.taskId = taskId;
        this.userId = userId;
        this.receiverName = receiverName;
        this.status = status;
        this.refCount = refCount;
        this.bulkFlag = bulkFlag;
        this.imageFile = imageFile;
        this.signatureFile = signatureFile;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getUserId() {
        return userId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public String getStatus() {
        return status;
    }

    public String getRefCount() {
        return refCount;
    }

    public String getBulkFlag() {
        return bulkFlag;
    }

    public File getImageFile() {
        return imageFile;
    }

    public File getSignatureFile() {
        return signatureFile;
    }
}
