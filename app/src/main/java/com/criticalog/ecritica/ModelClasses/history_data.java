package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 8/7/2018.
 */

public class history_data {
    private String task_id, task_status;

    public history_data(String task_id, String task_status) {
        this.task_id = task_id;
        this.task_status = task_status;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }


}
