package com.criticalog.ecritica.ModelClasses;

/**
 * Created by DELL1 on 7/12/2018.
 */

public class task_status {
    String taskid, status;

    public task_status(String taskid, String status) {
        this.taskid = taskid;
        this.status = status;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
