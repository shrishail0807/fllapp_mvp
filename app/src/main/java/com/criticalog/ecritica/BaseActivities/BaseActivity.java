package com.criticalog.ecritica.BaseActivities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.NetworkClass;
import com.criticalog.ecritica.Utils.SignalStrengthChecker;
import com.criticalog.ecritica.Utils.StaticUtils;

import com.google.android.gms.vision.barcode.Barcode;
import com.zain.android.internetconnectivitylibrary.ConnectionUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

//import io.realm.Realm;


//import com.example.criticalog.Utils.CriticalogSharedPreferences;

/**
 * Created by DELL1 on 6/28/2018.
 */

public class BaseActivity extends AppCompatActivity {
    public NetworkClass mNetworkClass;
   // public Realm mRealm,mRealm2;
    public DatabaseHelper dbHelper;
    public SignalStrengthChecker mSignalStrengthChecker;
    public ServiceConnection serviceconnection = null;
    private static boolean mbound;
    static BaseActivity instance;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int REQUEST_TAKE_PHOTO1 = 101;
    private String mCurrentPhotoPath;
    public ConnectionUtil connectionUtil;


    public Bitmap bitmap,imageBitmap;
    File upload_cameraFile1 = null;


    CriticalogSharedPreferences criticalogSharedPreferences;
    public  static BaseActivity getInstance() {
        return instance;
    }

    public static  boolean isbackground;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        criticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        serviceconnection= new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
              /*  binder= (IMyBinder) iBinder;
                Log.d("binder","connected");
                mbound = true;*/
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.e("disbinder","disconnected");
                mbound = false;
            }
        };
        Log.d("service","connection"+serviceconnection);


        try {
            //Bind the service to get the ImyBinder object
          /*  Intent intent=new Intent(this, PosprinterService.class);
            bindService(intent, serviceconnection, BIND_AUTO_CREATE);

            Log.d("service","binded"+serviceconnection);
            instance = this;*/
        }catch (Exception e) {
            Log.d("exception ","printer");
        }


        mNetworkClass = new NetworkClass(BaseActivity.this);
        connectionUtil = new ConnectionUtil(BaseActivity.this);




        /*Realm.init(getApplicationContext());
        mRealm = Realm.getDefaultInstance();

        mRealm2 = Realm.getDefaultInstance();*/



        // Create the Database helper object
        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();

        mSignalStrengthChecker = new SignalStrengthChecker(this);



    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }



    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }




    public void mShowDialog(String title, String message, String positiveButton, final String negativeButton, final Boolean closeActivity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BaseActivity.this);

        //dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);


        dialogBuilder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (closeActivity) {
                    finish();
                }
                dialogInterface.cancel();
            }
        });

        if (negativeButton != null) {
            dialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    if (negativeButton.equalsIgnoreCase(StaticUtils.NETWORK_BUTTON)) {
                        dialogInterface.cancel();
                        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    }
                }
            });
        }

        final AlertDialog dialog = dialogBuilder.create();
        if (!(this.isFinishing())) {
            //show dialog
            dialog.show();
        }
    }

    public void network_save_Dialog(String title, String message, String positiveButton, final String negativeButton, final Boolean closeActivity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BaseActivity.this);

        //dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);


        dialogBuilder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (closeActivity) {
                    finish();
                }
                dialogInterface.cancel();

              //  startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                dialogInterface.cancel();
            }
        });

        if (negativeButton != null) {
            dialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    if (negativeButton.equalsIgnoreCase(StaticUtils.NETWORK_BUTTON)) {
                        dialogInterface.cancel();
                        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    }
                }
            });
        }

        final AlertDialog dialog = dialogBuilder.create();
        if (!(this.isFinishing())) {
            //show dialog
            dialog.show();
        }
    }

    public void mShowDialog1(String title, String message, String positiveButton, final Boolean closeActivity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BaseActivity.this);

        //dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);

        dialogBuilder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

              /*  if (closeActivity) {
                   startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                }*/

               // startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                dialogInterface.cancel();
            }
        });

      /*  if (negativeButton != null) {
            dialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    if (negativeButton.equalsIgnoreCase(StaticUtils.NETWORK_BUTTON)) {
                        dialogInterface.cancel();
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    }
                }
            });
        }
*/
        final AlertDialog dialog = dialogBuilder.create();
        if (!(this.isFinishing())) {
            //show dialog
            dialog.show();
        }
    }

    public File bytearrytofile(byte[] bytes) throws IOException {

        //below is the different part
        File file = new File("java2.pdf");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bytes);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return file;
    }

   /* public File createImageFile(String taskId) throws IOException {

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String task_id = taskId + ".jpeg-";
        //String imageFileName = task_id;

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                task_id,
                "",
                storageDir
        );

        // mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }*/
    public void toast(String message)
    {
        Toast toast = new Toast(getApplicationContext());
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.image_custom, null);
        TextView textView = (TextView) view.findViewById(R.id.custom_toast_text);
        textView.setText(message);
        toast.setView(view);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //mRealm.close();
      //  unbindService(serviceconnection);
    }

    public Address getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        Barcode.GeoPoint p1 = null;

        Address location = null;
        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return null;
            }
            location=address.get(0);

        }catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }




  /*  public void printlabel(String boxnumber,int count,int totalcount,String docketno,String source,String dest) {



        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {

            }

            @Override
            public void onfailed() {

            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                ArrayList<byte[]> list=new ArrayList<byte[]>();
                DataForSendToPrinterTSC.setCharsetName("gbk");
                //set label size
                //  byte[] data= DataForSendToPrinterTSC.sizeBymm(76.2,50.8);
                byte[] data= DataForSendToPrinterTSC.sizeBymm(75,50);

                list.add(data);
                list.add(DataForSendToPrinterTSC.gapBymm(5,5));
                list.add(DataForSendToPrinterTSC.cls());
                list.add(DataForSendToPrinterTSC.bar(15, 50, 550, 3));
                list.add(DataForSendToPrinterTSC.bar(15, 50, 3, 350));
                Bitmap b = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.new_logo);

                list.add(DataForSendToPrinterTSC.bitmap(180,65,0,b, BitmapToByteData.BmpType.Dithering));
                list.add(DataForSendToPrinterTSC.bar(15, 125, 550, 3));
                list.add(DataForSendToPrinterTSC.barCode(45, 135, "128", 40, 1, 0, 2, 2,
                        boxnumber));
                byte[] data1 = DataForSendToPrinterTSC.text(310, 150, "4", 0, 1, 1, boxnumber);
                list.add(DataForSendToPrinterTSC.bar(15, 210, 550, 3));
                list.add(DataForSendToPrinterTSC.bar(250,210,3,60));
                byte[] data2 = DataForSendToPrinterTSC.text(30, 240, "3", 0, 1, 1, "Package no");
                byte[] data3 = DataForSendToPrinterTSC.text(290, 240, "3", 0, 1, 1, String.valueOf(count));
                list.add(DataForSendToPrinterTSC.bar(330,210,3,60));
                byte[] data4 = DataForSendToPrinterTSC.text(345, 240, "3", 0, 1, 1, "of");
                list.add(DataForSendToPrinterTSC.bar(380,210,3,60));
                byte[] data5 = DataForSendToPrinterTSC.text(400, 240, "3", 0, 1, 1, String.valueOf(totalcount));
                list.add(DataForSendToPrinterTSC.bar(470,210,3,60));
                byte[] data6 = DataForSendToPrinterTSC.text(485, 240, "3", 0, 1, 1, "Pkgs");
                list.add(DataForSendToPrinterTSC.bar(15, 270, 550, 3));
                list.add(DataForSendToPrinterTSC.bar(250,270,3,80));
                byte[] data7 = DataForSendToPrinterTSC.text(30, 300, "3", 0, 1, 1, "Connote no");
                list.add(DataForSendToPrinterTSC.barCode(295, 285, "128", 40, 1, 0, 2, 2,
                        docketno));
                list.add(DataForSendToPrinterTSC.bar(15, 350, 550, 3));
                list.add(DataForSendToPrinterTSC.bar(250,350,3,60));
                byte[] data9 = DataForSendToPrinterTSC.text(30, 370, "3", 0, 1, 1,"Origin/dest.");
                byte[] data10 = DataForSendToPrinterTSC.text(295, 370, "3", 0, 1, 1,source +"/"+dest);
                list.add(DataForSendToPrinterTSC.bar(15, 400, 550, 3));
                list.add(DataForSendToPrinterTSC.bar(565, 50, 3, 350));
                list.add(data1);
                list.add(data2);
                list.add(data3);
                list.add(data4);
                list.add(data5);
                list.add(data6);
                list.add(data7);
                list.add(data9);
                list.add(data10);
                list.add(DataForSendToPrinterTSC.print(1));
                list.add(DataForSendToPrinterTSC.cut());
                return list;
            }
        });
    }*/

    public double distance(double lat1, double lon1, double lat2, double lon2,String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist/0.62137;

       /* String num = String.valueOf(dist);
        new BigDecimal(num).toPlainString();
        return new BigDecimal(num).toPlainString();*/
        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public String converttorealnumber(double distance){
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }


    public double meterDistanceBetweenPoints(float lat_a, float lng_a, float lat_b, float lng_b) {
        float pk = (float) (180.f/Math.PI);

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }







    public File startCamera(String docket,int count) {
        Log.d("docketstr","start camera"+docket);

      File file =   mCaptureImage(docket,count);


      return file;
    }



    public File mCaptureImage(String docket,int count) {

        Log.d("docketstr","mCaptureImage"+docket);



        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getApplicationContext().getPackageManager()) != null) {

            //  File photoFile = null;
            try {
                upload_cameraFile1 = createImageFile(docket);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }

            Uri apkURI = FileProvider.getUriForFile(this, this
                            .getPackageName() + ".provider", upload_cameraFile1);

            if (upload_cameraFile1 != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        apkURI);

                startActivityForResult(takePictureIntent, count);

            }
        }

        return upload_cameraFile1;
    }


    private File createImageFile(String docket) throws IOException {
        Log.d("inside","createimgfile"+docket);

        // Create an image file name
        // String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String task_id = docket + ".jpeg-";
        //String imageFileName = task_id;

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                task_id,  /* prefix */
                "",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("return image","="+image);

        String thing = image.toString();
        int index = thing.lastIndexOf("-");
        String yourCuttedString = thing.substring(0, index);
        Log.d("after rename",yourCuttedString);
        File newfile = new File(yourCuttedString);
        Log.d("file name",":"+ image.renameTo(newfile));

        return image;
    }


    private boolean checkPermissions() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    public Bitmap setPic(ImageView imageView,String docket) {
        // Get the dimensions of the View
        int targetW = 300;
        int targetH = 300;

        Bitmap bitmapnew;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        //bmOptions.inSampleSize = 8;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
       /* int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;*/

        int photoW = 300;
        int photoH = 300;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor << 1;
        bmOptions.inPurgeable = true;

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            // options.inSampleSize = 8;
            //options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

            Matrix mtx = new Matrix();
            mtx.postRotate(0);
            // RESIZE THE BIT MAP
            //mtx.postScale(300, 300);
            // Rotating Bitmap
         /*   int width = mcapture_image.getWidth();
            int height = mcapture_image.getHeight();*/

            imageBitmap = Bitmap.createBitmap(bitmap, 0, 0, 200, 200, mtx, true);


            OutputStream os;
            try {

                os = new FileOutputStream(upload_cameraFile1);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);


                os.flush();
                os.close();

            } catch (Exception e) {

                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);

            }

            compressImage(upload_cameraFile1.getAbsolutePath(),docket);

            //Check the condition as
            if (Build.VERSION.SDK_INT > 22) {
                Glide.with(this).load(upload_cameraFile1).fitCenter().into(imageView);
            } else {
                if (bitmap != null) {
                    Bitmap image_Bitmap = BITMAP_RESIZER(bitmap, 200, 200);
                    // mcapture_image.setImageBitmap(image_Bitmap);
                    Glide.with(this).asBitmap().load(image_Bitmap).into(imageView);
                }

            }
           /* mimage_cancel.setVisibility(View.VISIBLE);

            mimage_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mcapture_image.setImageBitmap(null);
                    mcapture_image.setImageResource(R.drawable.image_capture);
                    mimage_cancel.setVisibility(View.INVISIBLE);

                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageBitmap;

    }


    public String compressImage(String filePath,String docket) {
        Log.d("inside compressimage",docket);

        //String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612
        float maxHeight = 800.0f;
        float maxWidth = 600.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = true;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        //    canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename(docket);
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    public Bitmap BITMAP_RESIZER(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public String getFilename(String docket) {

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String task_id = docket + ".jpeg-";
        Log.d("inside get file name",task_id);
        File image = null;
        try {
            image = File.createTempFile(
                    task_id,  /* prefix */
                    "",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        String uriSting = (image.getAbsolutePath());
        return uriSting;
    }

    /* public byte[] convertfiletobytearry(File file) {

        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }*/

    public ProgressDialog showProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }


    @Override
    protected void onResume() {
        super.onResume();

        isbackground = false;

    }

    @Override
    protected void onPause() {
        super.onPause();

        isbackground = true;
    }






}
