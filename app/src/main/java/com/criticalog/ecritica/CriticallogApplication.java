package com.criticalog.ecritica;

import android.app.Application;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.criticalog.ecritica.ModelClasses.UserDetailsBulk;
import com.criticalog.ecritica.Utils.ForceUpdateChecker;
import com.criticalog.ecritica.Utils.PodClaimPojo;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*import io.realm.Realm;
import io.realm.RealmConfiguration;*/

/**
 * Created by DELL1 on 7/12/2018.
 */
public class CriticallogApplication extends Application {

    public static ArrayList<UserDetailsBulk> userDetailsBulksList = new ArrayList<>();
    public static ArrayList<String> TaskIdList = new ArrayList<>();

    public static final String CHANNEL_ID = "exampleServiceChannel";
    public static String bulkStatusHistory;
    public static boolean synchedtoserver = false;
    public static String pageNumStatus="false";
    public static String fragmentStatusDelivery;

    public static int scanCount=0;
    public static int duplicateboxscancount = 0;
    public static ArrayList<PodClaimPojo> latlonglist =  new ArrayList<>();
    public static ArrayList<PodClaimPojo> activity_point_list =  new ArrayList<>();
    public static double distance_traveled =0.0;
    public static  boolean flag = false;
    public static int clientclosecount = 0;
    public static int drsclosecount = 0;

    public static  int pending_count = 0;
    private FirebaseRemoteConfig firebaseRemoteConfig;

    public static String appversion;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();
        FirebaseApp.initializeApp(this);

        Log.d("firebase","initializes" );
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();

        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        Map<String, Object> remoteConfigDefaults = new HashMap();
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_REQUIRED, true);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_CURRENT_VERSION, "2.0.52");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL,
                "https://play.google.com/store/apps/details?id=com.criticalog.ecritica&hl=en_IN");

        firebaseRemoteConfig.setDefaultsAsync(remoteConfigDefaults);


        firebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                if (task.isSuccessful()) {
                    boolean updated = task.getResult();
                    Log.d("CriticalogApplication", "Config params updated: " + updated);

                }
            }
        });

     /*   firebaseRemoteConfig.setDefaultsAsync (remoteConfigDefaults);
        firebaseRemoteConfig.fetch(60) // fetch every minutes
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("TAG", "remote config is fetched.");
                            firebaseRemoteConfig.activate();
                        }
                    }
                });
*/

       /* Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("eCritica.realm")
                .schemaVersion(101)
                .deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);*/




    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Example Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }



}
