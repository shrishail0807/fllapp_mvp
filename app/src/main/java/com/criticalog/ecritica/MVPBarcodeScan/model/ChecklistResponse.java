package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChecklistResponse {
    @SerializedName("Rule")
    @Expose
    private String rule;
    @SerializedName("Flag")
    @Expose
    private String flag;
    @SerializedName("image")
    @Expose
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ChecklistResponse() {
    }

    public ChecklistResponse(String rule, String flag,String image) {
        this.rule = rule;
        this.flag = flag;
        this.image=image;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
