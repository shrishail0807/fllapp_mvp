package com.criticalog.ecritica.MVPBarcodeScan.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBarcodeScan.model.ChecklistResponse;
import com.criticalog.ecritica.MVPHomeScreen.HomeActivity;
import com.criticalog.ecritica.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RulesHorizontalAdapter extends RecyclerView.Adapter<RulesHorizontalAdapter.ViewHolder> {
    List<ChecklistResponse> prsRules;
    RulesClicked rulesClicked;
    Context context;

    public RulesHorizontalAdapter(Context context, List<ChecklistResponse> prsRules, RulesClicked rulesClicked) {
        this.prsRules = prsRules;
        this.rulesClicked = rulesClicked;
        this.context = context;
    }

    @NonNull
    @Override
    public RulesHorizontalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row_with_swap, parent, false);
        return new RulesHorizontalAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RulesHorizontalAdapter.ViewHolder holder, int position) {

        holder.mRuleText.setText(String.valueOf(position + 1) + " )" + prsRules.get(position).getRule());

        if (prsRules.get(position).getImage() != null) {
            if (!prsRules.get(position).getImage().equalsIgnoreCase("")) {
                holder.mQcImage.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(prsRules.get(position).getImage().toString())
                        .resize(400, 400)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .placeholder(R.drawable.brokenimage)
                        .into(holder.mQcImage);
            } else {
                holder.mQcImage.setVisibility(View.GONE);
            }
        }
        if (prsRules.get(position).getFlag().equalsIgnoreCase("1")) {
            holder.mYes.setBackgroundResource(R.drawable.rounded_bacground_green);
            holder.mNO.setBackgroundResource(R.drawable.rounded_background_border);
        } else if (prsRules.get(position).getFlag().equalsIgnoreCase("0")) {
            holder.mNO.setBackgroundResource(R.drawable.rounded_bachground_red);
            holder.mYes.setBackgroundResource(R.drawable.rounded_background_border);
        } else {
            holder.mNO.setBackgroundResource(R.drawable.rounded_background_border);
            holder.mYes.setBackgroundResource(R.drawable.rounded_background_border);
        }

        if (position == 0) {
            holder.mFastRewind.setVisibility(View.INVISIBLE);
            holder.mFastForwad.setVisibility(View.VISIBLE);
        } else if (position == prsRules.size() - 1) {
            holder.mFastRewind.setVisibility(View.VISIBLE);
            holder.mFastForwad.setVisibility(View.INVISIBLE);
        } else {
            holder.mFastRewind.setVisibility(View.VISIBLE);
            holder.mFastForwad.setVisibility(View.VISIBLE);
        }

        holder.mFastForwad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rulesClicked.onclickRulesList("scrollto", position);
            }
        });

        holder.mFastRewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rulesClicked.onclickRulesList("scrollback", position);
            }
        });


        holder.mQcImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBannerDialog(prsRules.get(position).getImage());
            }
        });

        holder.mYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                holder.mYes.setBackgroundResource(R.drawable.rounded_bacground_green);
                holder.mNO.setBackgroundResource(R.drawable.rounded_background_border);

                rulesClicked.onclickRulesList("yes", position);
                //  notifyDataSetChanged();

            }
        });

        holder.mNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.mNO.setBackgroundResource(R.drawable.rounded_bachground_red);
                holder.mYes.setBackgroundResource(R.drawable.rounded_background_border);

                rulesClicked.onclickRulesList("no", position);
                //notifyDataSetChanged();

            }
        });
    }

    public void showBannerDialog(String bannerURL) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(context);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_banner);
        // Set dialog title
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // set values for custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.mBannerText);
        ImageView image = (ImageView) dialog.findViewById(R.id.mImageBanner);
        ImageView cancelDialog = dialog.findViewById(R.id.mDialogCancel);
        TextView moreUpdates = dialog.findViewById(R.id.mMoreUpdates);

        moreUpdates.setText("OK");
        moreUpdates.setVisibility(View.GONE);
        cancelDialog.setVisibility(View.VISIBLE);

        Picasso.with(context).load(bannerURL).into(image);

        dialog.show();

        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        moreUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return prsRules.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mRuleText, mYes, mNO;
        CheckBox mCheckYes, mCheckNo;
        ImageView mFastForwad, mFastRewind, mQcImage;

        public ViewHolder(View itemView) {
            super(itemView);

            mRuleText = itemView.findViewById(R.id.mRuleText);
            mYes = itemView.findViewById(R.id.mYes);
            mNO = itemView.findViewById(R.id.mNO);
            mFastForwad = itemView.findViewById(R.id.mFastForwad);
            mFastRewind = itemView.findViewById(R.id.mFastRewind);
            mQcImage = itemView.findViewById(R.id.mQcImage);
        }
    }
}

/*
package com.criticalog.ecritica.MVPBarcodeScan.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Adapters.RulesAdapterWithFlag;
import com.criticalog.ecritica.Interface.BulkListWsCalled;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBarcodeScan.model.ChecklistResponse;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.model.PrsRule;

import java.util.ArrayList;
import java.util.List;

public class RulesHorizontalAdapter extends RecyclerView.Adapter<RulesHorizontalAdapter.ViewHolder> {
    List<PrsRule> prsRules;
    RulesClicked rulesClicked;
    public RulesHorizontalAdapter(List<PrsRule> prsRules,RulesClicked rulesClicked) {
        this.prsRules = prsRules;
        this.rulesClicked=rulesClicked;
    }
    @NonNull
    @Override
    public RulesHorizontalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rules_row_with_swap, parent, false);
        return new RulesHorizontalAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RulesHorizontalAdapter.ViewHolder holder, int position) {

        holder.mRuleText.setText(String.valueOf(position+1)+" )"+prsRules.get(position).getRule());

        if(position==0)
        {
            holder.mFastRewind.setVisibility(View.GONE);
        }
        if(position==prsRules.size()-1)
        {
            holder.mFastForwad.setVisibility(View.GONE);
        }

        holder.mFastForwad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rulesClicked.onclickRulesList("scrollto",position);
            }
        });

        holder.mFastRewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rulesClicked.onclickRulesList("scrollback",position);
            }
        });

        holder.mYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rulesClicked.onclickRulesList("yes",position);
                holder.mYes.setBackgroundResource(R.drawable.rounded_bacground_green);
                holder.mNO.setBackgroundResource(R.drawable.rounded_background_border);
              //  notifyDataSetChanged();

            }
        });

        holder.mNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rulesClicked.onclickRulesList("no",position);
                holder.mNO.setBackgroundResource(R.drawable.rounded_bachground_red);
                holder.mYes.setBackgroundResource(R.drawable.rounded_background_border);
                //notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        return prsRules.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mRuleText,mYes,mNO;
        CheckBox mCheckYes,mCheckNo;
        ImageView mFastForwad,mFastRewind;

        public ViewHolder(View itemView) {
            super(itemView);

            mRuleText = itemView.findViewById(R.id.mRuleText);
            mYes=itemView.findViewById(R.id.mYes);
            mNO=itemView.findViewById(R.id.mNO);
            mFastForwad=itemView.findViewById(R.id.mFastForwad);
            mFastRewind=itemView.findViewById(R.id.mFastRewind);
        }
    }
}
*/
