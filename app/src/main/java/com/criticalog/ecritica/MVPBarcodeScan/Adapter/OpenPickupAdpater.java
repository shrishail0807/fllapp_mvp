package com.criticalog.ecritica.MVPBarcodeScan.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.OnclickOpenAdapter;
import com.criticalog.ecritica.ModelClasses.OpenImage;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

public class OpenPickupAdpater extends RecyclerView.Adapter<OpenPickupAdpater.OpenPickupViewHolder> {
    private OnclickOpenAdapter onclickOpenAdapter;
    private Context context;
    List<OpenImage> openImageList = new ArrayList<>();

    public OpenPickupAdpater(Context context, OnclickOpenAdapter onclickOpenAdapter, List<OpenImage> openImageList) {
        this.onclickOpenAdapter = onclickOpenAdapter;
        this.context = context;
        this.openImageList = openImageList;
    }

    @NonNull
    @Override
    public OpenPickupAdpater.OpenPickupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.open_pickup_item, parent, false);
        return new OpenPickupAdpater.OpenPickupViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OpenPickupAdpater.OpenPickupViewHolder holder, int position) {
        holder.mTextMultipleOpenPickup.setTextColor(Color.BLACK);
        holder.mTextMultipleOpenPickup.setText(openImageList.get(position).getLabel());

        if (!openImageList.get(position).getImage().equalsIgnoreCase("")) {
            byte[] decodedString = Base64.decode(openImageList.get(position).getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.mCaptureMultipleOpenPickupImage.setImageBitmap(decodedByte);
        }

        holder.mCaptureMultipleOpenPickupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclickOpenAdapter.openPickupAdapter(position, openImageList.get(position).getLabel());
            }
        });
    }

    @Override
    public int getItemCount() {
        return openImageList.size();
    }

    public class OpenPickupViewHolder extends RecyclerView.ViewHolder {
        ImageView mCaptureMultipleOpenPickupImage;
        TextView mTextMultipleOpenPickup;

        public OpenPickupViewHolder(@NonNull View itemView) {
            super(itemView);

            mCaptureMultipleOpenPickupImage = itemView.findViewById(R.id.mCaptureMultipleOpenPickupImage);
            mTextMultipleOpenPickup = itemView.findViewById(R.id.mTextMultipleOpenPickup);
        }
    }
}
