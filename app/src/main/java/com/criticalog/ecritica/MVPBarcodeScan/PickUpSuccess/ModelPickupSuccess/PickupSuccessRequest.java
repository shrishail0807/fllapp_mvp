package com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess;

import com.criticalog.ecritica.MVPBarcodeScan.model.ChecklistResponse;
import com.criticalog.ecritica.ModelClasses.OpenImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PickupSuccessRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("boxnumber")
    @Expose
    private String boxnumber;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("docketnumber")
    @Expose
    private String docketnumber;
    @SerializedName("pre_docket")
    @Expose
    private String preDocket;
    @SerializedName("prs_number")
    @Expose
    private String prsNumber;
    @SerializedName("source_pin")
    @Expose
    private String sourcePin;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("dest_pin")
    @Expose
    private String destPin;
    @SerializedName("actual_value")
    @Expose
    private String actualValue;
    @SerializedName("ischecked")
    @Expose
    private String ischecked;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("prs_id")
    @Expose
    private String prsId;
    @SerializedName("hub_id")
    @Expose
    private String hubId;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("dest_hub_id")
    @Expose
    private String destHubId;
    @SerializedName("dest_mail")
    @Expose
    private String destMail;
    @SerializedName("destination_loc")
    @Expose
    private String destinationLoc;

    @SerializedName("image_1")
    @Expose
    private String image1;

    @SerializedName("ewaybill_scan")
    @Expose
    private List<Long> ewaybillScan = null;

    @SerializedName("image_2")
    @Expose
    private String image2;
    @SerializedName("checklist_response")
    @Expose
    private List<ChecklistResponse> checklistResponse = null;

    @SerializedName("remark")
    @Expose
    private String remark;

    @SerializedName("gatepass_image")
    @Expose
    private String gatepass_image;

    public String getGatepass_image() {
        return gatepass_image;
    }

    public void setGatepass_image(String gatepass_image) {
        this.gatepass_image = gatepass_image;
    }

    @SerializedName("open_images")
    @Expose
    private List<OpenImage> openImageList = null;

    public List<OpenImage> getOpenImageList() {
        return openImageList;
    }

    public void setOpenImageList(List<OpenImage> openImageList) {
        this.openImageList = openImageList;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Long> getEwaybillScan() {
        return ewaybillScan;
    }

    public void setEwaybillScan(List<Long> ewaybillScan) {
        this.ewaybillScan = ewaybillScan;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBoxnumber() {
        return boxnumber;
    }

    public void setBoxnumber(String boxnumber) {
        this.boxnumber = boxnumber;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDocketnumber() {
        return docketnumber;
    }

    public void setDocketnumber(String docketnumber) {
        this.docketnumber = docketnumber;
    }

    public String getPreDocket() {
        return preDocket;
    }

    public void setPreDocket(String preDocket) {
        this.preDocket = preDocket;
    }

    public String getPrsNumber() {
        return prsNumber;
    }

    public void setPrsNumber(String prsNumber) {
        this.prsNumber = prsNumber;
    }

    public String getSourcePin() {
        return sourcePin;
    }

    public void setSourcePin(String sourcePin) {
        this.sourcePin = sourcePin;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getDestPin() {
        return destPin;
    }

    public void setDestPin(String destPin) {
        this.destPin = destPin;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }

    public String getIschecked() {
        return ischecked;
    }

    public void setIschecked(String ischecked) {
        this.ischecked = ischecked;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getDestHubId() {
        return destHubId;
    }

    public void setDestHubId(String destHubId) {
        this.destHubId = destHubId;
    }

    public String getDestMail() {
        return destMail;
    }

    public void setDestMail(String destMail) {
        this.destMail = destMail;
    }

    public String getDestinationLoc() {
        return destinationLoc;
    }

    public void setDestinationLoc(String destinationLoc) {
        this.destinationLoc = destinationLoc;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public List<ChecklistResponse> getChecklistResponse() {
        return checklistResponse;
    }

    public void setChecklistResponse(List<ChecklistResponse> checklistResponse) {
        this.checklistResponse = checklistResponse;
    }
}
