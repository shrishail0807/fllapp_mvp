package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocketData {
    @SerializedName("totalbox")
    @Expose
    private String totalbox;
    @SerializedName("actual_value")
    @Expose
    private String actualValue;
    @SerializedName("source_hub")
    @Expose
    private String sourceHub;
    @SerializedName("dest_hub")
    @Expose
    private String destHub;

    @SerializedName("gatepass")
    @Expose
    private String gatepass;

    public String getGatepass() {
        return gatepass;
    }

    public void setGatepass(String gatepass) {
        this.gatepass = gatepass;
    }

    public String getTotalbox() {
        return totalbox;
    }

    public void setTotalbox(String totalbox) {
        this.totalbox = totalbox;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }

    public String getSourceHub() {
        return sourceHub;
    }

    public void setSourceHub(String sourceHub) {
        this.sourceHub = sourceHub;
    }

    public String getDestHub() {
        return destHub;
    }

    public void setDestHub(String destHub) {
        this.destHub = destHub;
    }

}
