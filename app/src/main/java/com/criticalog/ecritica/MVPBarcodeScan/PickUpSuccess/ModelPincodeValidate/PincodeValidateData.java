package com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PincodeValidateData {
    @SerializedName("dest_hub_id")
    @Expose
    private String destHubId;
    @SerializedName("dest_mail")
    @Expose
    private String destMail;
    @SerializedName("destination_loc")
    @Expose
    private String destinationLoc;

    @SerializedName("city_name")
    @Expose
    private String city_name;

    @SerializedName("controlling_hub")
    @Expose
    private String controlling_hub;

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getControlling_hub() {
        return controlling_hub;
    }

    public void setControlling_hub(String controlling_hub) {
        this.controlling_hub = controlling_hub;
    }

    public String getDestHubId() {
        return destHubId;
    }

    public void setDestHubId(String destHubId) {
        this.destHubId = destHubId;
    }

    public String getDestMail() {
        return destMail;
    }

    public void setDestMail(String destMail) {
        this.destMail = destMail;
    }

    public String getDestinationLoc() {
        return destinationLoc;
    }

    public void setDestinationLoc(String destinationLoc) {
        this.destinationLoc = destinationLoc;
    }
}
