package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataActualValue {
    @SerializedName("ewaybill")
    @Expose
    private String ewaybill;

    public String getEwaybill() {
        return ewaybill;
    }

    public void setEwaybill(String ewaybill) {
        this.ewaybill = ewaybill;
    }
}
