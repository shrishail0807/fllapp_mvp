package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActualValueCheckRequest {
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("actual_val")
    @Expose
    private String actualVal;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("dest")
    @Expose
    private String dest;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("actual_value")
    @Expose
    private String actualValue;

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getActualVal() {
        return actualVal;
    }

    public void setActualVal(String actualVal) {
        this.actualVal = actualVal;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }
}
