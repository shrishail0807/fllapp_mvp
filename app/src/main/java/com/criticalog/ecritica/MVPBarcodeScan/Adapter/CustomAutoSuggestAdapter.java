package com.criticalog.ecritica.MVPBarcodeScan.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestDatum;
import com.criticalog.ecritica.R;

import java.util.ArrayList;
import java.util.List;

public class CustomAutoSuggestAdapter extends ArrayAdapter<DocketListWithOriginDestDatum> {

    List<DocketListWithOriginDestDatum>  userDetails, tempUserDetails, suggestions;

    public CustomAutoSuggestAdapter(Context context, List<DocketListWithOriginDestDatum> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.userDetails = objects;
        this.tempUserDetails = new ArrayList<DocketListWithOriginDestDatum>(objects);
        this.suggestions = new ArrayList<DocketListWithOriginDestDatum>(objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DocketListWithOriginDestDatum currentUserDetails = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_details, parent, false);
        }
        TextView tvUserName = convertView.findViewById(R.id.tvUserName);
        TextView tvUserNumber = convertView.findViewById(R.id.tvUserNumber);

        if (currentUserDetails != null) {
            if (tvUserName != null) {
                tvUserName.setText(currentUserDetails.getDocketNum());
            }

            if (tvUserNumber != null) {
                tvUserNumber.setText(currentUserDetails.getDocketNum());
            }
        }


        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }


    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            DocketListWithOriginDestDatum userDetails = (DocketListWithOriginDestDatum) resultValue;
            return userDetails.getDocketNum();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (DocketListWithOriginDestDatum userDetails : tempUserDetails) {
                    if (userDetails.getDocketNum().toLowerCase().contains(constraint.toString().toLowerCase())
                            || userDetails.getDocketNum().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(userDetails);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<DocketListWithOriginDestDatum> c = (ArrayList<DocketListWithOriginDestDatum>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (DocketListWithOriginDestDatum cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
