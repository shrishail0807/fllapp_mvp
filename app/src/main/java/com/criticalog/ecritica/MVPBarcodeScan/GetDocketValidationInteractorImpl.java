package com.criticalog.ecritica.MVPBarcodeScan;

import android.util.Log;

import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidationRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.OtpVliadationChecklistRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.OtpVliadationChecklistResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateChecklistYesNoResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateCheclistYesNoRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetDocketValidationInteractorImpl implements DocketValidationContract.GetDocketValidationIntractor, DocketValidationContract.GetPickupSuccessIntractor, DocketValidationContract.GetPincodeValidateIntractor, DocketValidationContract.GetCloseDocketIntractor,
        DocketValidationContract.GetCloseClientIntractor, DocketValidationContract.GetQCRulesIntractor, DocketValidationContract.GetQCItemListIntractor,
        DocketValidationContract.GetQCRulesCheckIntractor, DocketValidationContract.GetQCUpdateIntractor, DocketValidationContract.GetQCNDCUpdateIntractor, DocketValidationContract.GetDocketListWithOriginDestIntractor, DocketValidationContract.ActualValueCheckIntractor,
        DocketValidationContract.OtpValidationChecklistIntractor, DocketValidationContract.ValidateChecklistYesNoIntractor {

    RestServices service = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void docketValidationSuccessful(DocketValidationContract.GetDocketValidationIntractor.OnFinishedListener onFinishedListener, String token, DocketValidationRequest docketValidationRequest) {

        Call<DocketValidateResponse> docketValidateResponseCall = service.docketValidation(docketValidationRequest);
        docketValidateResponseCall.enqueue(new Callback<DocketValidateResponse>() {
            @Override
            public void onResponse(Call<DocketValidateResponse> call, Response<DocketValidateResponse> response) {

                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DocketValidateResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void pickupSuccessful(DocketValidationContract.GetPickupSuccessIntractor.OnFinishedListener onFinishedListener, String token, PickupSuccessRequest pickupSuccessRequest) {
        Call<PickupSuccessResponse> pickupSuccessResponseCall = service.pickupSuccess(pickupSuccessRequest);
        pickupSuccessResponseCall.enqueue(new Callback<PickupSuccessResponse>() {
            @Override
            public void onResponse(Call<PickupSuccessResponse> call, Response<PickupSuccessResponse> response) {
                Log.e("pick_response", response.toString());
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PickupSuccessResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void pincodeValidateSuccessful(DocketValidationContract.GetPincodeValidateIntractor.OnFinishedListener onFinishedListener, String token, PincodeValidateRequest pincodeValidateRequest) {

        Call<PincodeValidateResponse> pincodeValidateResponseCall = service.pincodeValidation(pincodeValidateRequest);
        pincodeValidateResponseCall.enqueue(new Callback<PincodeValidateResponse>() {
            @Override
            public void onResponse(Call<PincodeValidateResponse> call, Response<PincodeValidateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<PincodeValidateResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });

    }

    @Override
    public void closeDocketSuccessful(DocketValidationContract.GetCloseDocketIntractor.OnFinishedListener onFinishedListener, String token, CloseDocketRequest closeDocketRequest) {

        Call<CloseDocketResponse> closeDocketResponseCall = service.closeDocket(closeDocketRequest);
        closeDocketResponseCall.enqueue(new Callback<CloseDocketResponse>() {
            @Override
            public void onResponse(Call<CloseDocketResponse> call, Response<CloseDocketResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CloseDocketResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void closeClientSuccessful(DocketValidationContract.GetCloseClientIntractor.OnFinishedListener onFinishedListener, String token, CloseClientRequest closeClientRequest) {

        Call<CloseClientResponse> closeClientResponseCall = service.closeClient(closeClientRequest);
        closeClientResponseCall.enqueue(new Callback<CloseClientResponse>() {
            @Override
            public void onResponse(Call<CloseClientResponse> call, Response<CloseClientResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<CloseClientResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });

    }

    @Override
    public void qcRulesSuccessful(DocketValidationContract.GetQCRulesIntractor.OnFinishedListener onFinishedListener, QCRulesRequest qcRulesRequest) {
        Call<QCRulesResponse> qcRulesResponseCall = service.getQcRules(qcRulesRequest);
        qcRulesResponseCall.enqueue(new Callback<QCRulesResponse>() {
            @Override
            public void onResponse(Call<QCRulesResponse> call, Response<QCRulesResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<QCRulesResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void qcItemListSuccessful(DocketValidationContract.GetQCItemListIntractor.OnFinishedListener onFinishedListener, ItemListRequest itemListRequest) {
        Call<ItemListResponse> itemListResponseCall = service.getItemsList(itemListRequest);
        itemListResponseCall.enqueue(new Callback<ItemListResponse>() {
            @Override
            public void onResponse(Call<ItemListResponse> call, Response<ItemListResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ItemListResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void qcRulesCheckSuccessful(DocketValidationContract.GetQCRulesCheckIntractor.OnFinishedListener onFinishedListener, QcRulesCheckRequest qcRulesCheckRequest) {
        Call<QcRulesCheckResponse> qcRulesCheckResponseCall = service.qcCheckSubmit(qcRulesCheckRequest);
        qcRulesCheckResponseCall.enqueue(new Callback<QcRulesCheckResponse>() {
            @Override
            public void onResponse(Call<QcRulesCheckResponse> call, Response<QcRulesCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<QcRulesCheckResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void qcUpdateSuccessful(DocketValidationContract.GetQCUpdateIntractor.OnFinishedListener onFinishedListener, QcRulesUpdateRequest qcRulesUpdateRequest) {
        Call<QcRulesUpdateResponse> qcRulesUpdateResponseCall = service.qcUpdate(qcRulesUpdateRequest);
        qcRulesUpdateResponseCall.enqueue(new Callback<QcRulesUpdateResponse>() {
            @Override
            public void onResponse(Call<QcRulesUpdateResponse> call, Response<QcRulesUpdateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<QcRulesUpdateResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void qcNDCUpdateSuccessful(DocketValidationContract.GetQCNDCUpdateIntractor.OnFinishedListener onFinishedListener, QCPassFailRequest qcPassFailRequest) {
        Call<QCPassFailResponse> qcRulesResponseCall = service.qcNDCUpdate(qcPassFailRequest);
        qcRulesResponseCall.enqueue(new Callback<QCPassFailResponse>() {
            @Override
            public void onResponse(Call<QCPassFailResponse> call, Response<QCPassFailResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<QCPassFailResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getDocketListWithOriginDestSuccessful(DocketValidationContract.GetDocketListWithOriginDestIntractor.OnFinishedListener onFinishedListener, DocketListWithOriginDestRequest docketListWithOriginDestRequest) {

        Call<DocketListWithOriginDestResponse> docketListWithOriginDestResponseCall = service.getDocketListWithOriginDest(docketListWithOriginDestRequest);
        docketListWithOriginDestResponseCall.enqueue(new Callback<DocketListWithOriginDestResponse>() {
            @Override
            public void onResponse(Call<DocketListWithOriginDestResponse> call, Response<DocketListWithOriginDestResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<DocketListWithOriginDestResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getActualValueSuccessful(DocketValidationContract.ActualValueCheckIntractor.OnFinishedListener onFinishedListener, ActualValueCheckRequest actualValueCheckRequest) {


        Call<ActualValueCheckResponse> actualValueCheckResponseCall = service.actualValueCheck(actualValueCheckRequest);
        actualValueCheckResponseCall.enqueue(new Callback<ActualValueCheckResponse>() {
            @Override
            public void onResponse(Call<ActualValueCheckResponse> call, Response<ActualValueCheckResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ActualValueCheckResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getOtpValidationChecklistSuccessful(DocketValidationContract.OtpValidationChecklistIntractor.OnFinishedListener onFinishedListener, OTPRequest otpVliadationChecklistRequest) {
        Call<OTPResponse> otpVliadationChecklistResponseCall = service.otpValidationRequest(otpVliadationChecklistRequest);

        otpVliadationChecklistResponseCall.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getValidateChecklistYesNoSuccessful(DocketValidationContract.ValidateChecklistYesNoIntractor.OnFinishedListener onFinishedListener, ValidateCheclistYesNoRequest validateCheclistYesNoRequest) {
        Call<ValidateChecklistYesNoResponse> validateChecklistYesNoResponseCall = service.validateChecklistYesNo(validateCheclistYesNoRequest);
        validateChecklistYesNoResponseCall.enqueue(new Callback<ValidateChecklistYesNoResponse>() {
            @Override
            public void onResponse(Call<ValidateChecklistYesNoResponse> call, Response<ValidateChecklistYesNoResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ValidateChecklistYesNoResponse> call, Throwable t) {

                onFinishedListener.onFailure(t);
            }
        });
    }
}
