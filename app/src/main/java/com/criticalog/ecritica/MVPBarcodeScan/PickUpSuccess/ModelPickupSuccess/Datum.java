package com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable {
    @SerializedName("picked_connote_flag")
    @Expose
    private Integer pickedConnoteFlag;

    public Integer getPickedConnoteFlag() {
        return pickedConnoteFlag;
    }

    public void setPickedConnoteFlag(Integer pickedConnoteFlag) {
        this.pickedConnoteFlag = pickedConnoteFlag;
    }
}
