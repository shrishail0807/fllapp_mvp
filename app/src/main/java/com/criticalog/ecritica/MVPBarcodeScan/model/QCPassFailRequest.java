package com.criticalog.ecritica.MVPBarcodeScan.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class QCPassFailRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("qc_item_id")
    @Expose
    private String qcItemId;
    @SerializedName("qc_pass_fail")
    @Expose
    private String qcPassFail;

    @SerializedName("unique_id")
    @Expose
    private String unique_id;

    @SerializedName("docket_no")
    @Expose
    private String docket_no;

    public String getDocket_no() {
        return docket_no;
    }

    public void setDocket_no(String docket_no) {
        this.docket_no = docket_no;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQcItemId() {
        return qcItemId;
    }

    public void setQcItemId(String qcItemId) {
        this.qcItemId = qcItemId;
    }

    public String getQcPassFail() {
        return qcPassFail;
    }

    public void setQcPassFail(String qcPassFail) {
        this.qcPassFail = qcPassFail;
    }
}
