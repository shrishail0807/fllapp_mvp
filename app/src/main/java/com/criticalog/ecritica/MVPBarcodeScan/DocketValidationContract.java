package com.criticalog.ecritica.MVPBarcodeScan;

import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidationRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.OtpVliadationChecklistRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.OtpVliadationChecklistResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateChecklistYesNoResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateCheclistYesNoRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;

public interface DocketValidationContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void docketValidationRequest(String token, DocketValidationRequest docketValidationRequest);

        void pickupRequest(String token, PickupSuccessRequest pickupSuccessRequest);

        void picodeValidationRequest(String token, PincodeValidateRequest pincodeValidateRequest);

        void closeDocketRequest(String token, CloseDocketRequest closeDocketRequest);

        void closeClientRequest(String token, CloseClientRequest closeClientRequest);

        void qcItemListRequest(ItemListRequest itemListRequest);

        void requestQCrules(QCRulesRequest qcRulesRequest);

        void qcCheck(QcRulesCheckRequest qcRulesCheckRequest);

        void qcNDCUpdate(QCPassFailRequest qcPassFailRequest);

        void qcUpdateRequest(QcRulesUpdateRequest qcRulesUpdateRequest);

        void actualValueCheck(ActualValueCheckRequest actualValueCheckRequest);

        void otpValidationForChecklist(OTPRequest otpVliadationChecklistRequest);

        void validateChecklistYesNo(ValidateCheclistYesNoRequest validateCheclistYesNoRequest);

        void getDocketListWithOriginDestRequest(DocketListWithOriginDestRequest docketListWithOriginDestRequest);


    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToViews(DocketValidateResponse docketValidateResponse);

        void pickUpSuccessViews(PickupSuccessResponse pickupSuccessResponse);

        void pincodeValidateSuccessViews(PincodeValidateResponse pincodeValidateResponse);

        void closeDocketSuccessViews(CloseDocketResponse closeDocketResponse);

        void closeClientSuccessViews(CloseClientResponse closeClientResponse);

        void qcItems(ItemListResponse itemListResponse);

        void qcRules(QCRulesResponse qcRulesResponse);

        void qcCheckResponse(QcRulesCheckResponse qcRulesCheckResponse);

        void qcUpdateResponse(QcRulesUpdateResponse qcRulesUpdateResponse);

        void qcNDCUpdateResponse(QCPassFailResponse qcPassFailResponse);

        void getDocketListWithOriginDestResponse(DocketListWithOriginDestResponse docketListWithOriginDestResponse);

        void actualValueResponse(ActualValueCheckResponse actualValueCheckResponse);

        void otpValidationChecklistResponse(OTPResponse otpVliadationChecklistResponse);

        void validateChecklistYesNoResponse(ValidateChecklistYesNoResponse validateChecklistYesNoResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetDocketValidationIntractor {

        interface OnFinishedListener {
            void onFinished(DocketValidateResponse docketValidateResponse);

            void onFailure(Throwable t);
        }

        void docketValidationSuccessful(DocketValidationContract.GetDocketValidationIntractor.OnFinishedListener onFinishedListener, String token, DocketValidationRequest docketValidationRequest);
    }

    interface GetPickupSuccessIntractor {

        interface OnFinishedListener {
            void onFinished(PickupSuccessResponse pickupSuccessResponse);

            void onFailure(Throwable t);
        }

        void pickupSuccessful(DocketValidationContract.GetPickupSuccessIntractor.OnFinishedListener onFinishedListener, String token, PickupSuccessRequest pickupSuccessRequest);
    }

    interface GetPincodeValidateIntractor {
        interface OnFinishedListener {
            void onFinished(PincodeValidateResponse pincodeValidateResponse);

            void onFailure(Throwable t);
        }

        void pincodeValidateSuccessful(DocketValidationContract.GetPincodeValidateIntractor.OnFinishedListener onFinishedListener, String token, PincodeValidateRequest pincodeValidateRequest);
    }

    interface GetCloseDocketIntractor {
        interface OnFinishedListener {
            void onFinished(CloseDocketResponse closeDocketResponse);

            void onFailure(Throwable t);
        }

        void closeDocketSuccessful(DocketValidationContract.GetCloseDocketIntractor.OnFinishedListener onFinishedListener, String token, CloseDocketRequest closeDocketRequest);
    }

    interface GetCloseClientIntractor {
        interface OnFinishedListener {
            void onFinished(CloseClientResponse closeClientResponse);

            void onFailure(Throwable t);
        }

        void closeClientSuccessful(DocketValidationContract.GetCloseClientIntractor.OnFinishedListener onFinishedListener, String token, CloseClientRequest closeClientRequest);
    }

    interface GetQCRulesIntractor {
        interface OnFinishedListener {
            void onFinished(QCRulesResponse qcRulesResponse);

            void onFailure(Throwable t);
        }

        void qcRulesSuccessful(DocketValidationContract.GetQCRulesIntractor.OnFinishedListener onFinishedListener, QCRulesRequest qcRulesRequest);
    }

    interface GetQCItemListIntractor {
        interface OnFinishedListener {
            void onFinished(ItemListResponse itemListResponse);

            void onFailure(Throwable t);
        }

        void qcItemListSuccessful(DocketValidationContract.GetQCItemListIntractor.OnFinishedListener onFinishedListener, ItemListRequest itemListRequest);
    }

    interface GetQCRulesCheckIntractor {
        interface OnFinishedListener {
            void onFinished(QcRulesCheckResponse qcRulesCheckResponse);

            void onFailure(Throwable t);
        }

        void qcRulesCheckSuccessful(DocketValidationContract.GetQCRulesCheckIntractor.OnFinishedListener onFinishedListener, QcRulesCheckRequest qcRulesCheckRequest);
    }

    interface GetQCUpdateIntractor {
        interface OnFinishedListener {
            void onFinished(QcRulesUpdateResponse qcRulesUpdateResponse);

            void onFailure(Throwable t);
        }

        void qcUpdateSuccessful(DocketValidationContract.GetQCUpdateIntractor.OnFinishedListener onFinishedListener, QcRulesUpdateRequest qcRulesUpdateRequest);
    }

    interface GetQCNDCUpdateIntractor {
        interface OnFinishedListener {
            void onFinished(QCPassFailResponse qcPassFailResponse);

            void onFailure(Throwable t);
        }

        void qcNDCUpdateSuccessful(DocketValidationContract.GetQCNDCUpdateIntractor.OnFinishedListener onFinishedListener, QCPassFailRequest qcPassFailRequest);
    }

    interface GetDocketListWithOriginDestIntractor {
        interface OnFinishedListener {
            void onFinished(DocketListWithOriginDestResponse docketListWithOriginDestResponse);

            void onFailure(Throwable t);
        }

        void getDocketListWithOriginDestSuccessful(DocketValidationContract.GetDocketListWithOriginDestIntractor.OnFinishedListener onFinishedListener, DocketListWithOriginDestRequest docketListWithOriginDestRequest);
    }

    interface ActualValueCheckIntractor {
        interface OnFinishedListener {
            void onFinished(ActualValueCheckResponse actualValueCheckResponse);

            void onFailure(Throwable t);
        }

        void getActualValueSuccessful(DocketValidationContract.ActualValueCheckIntractor.OnFinishedListener onFinishedListener, ActualValueCheckRequest actualValueCheckRequest);
    }

    interface OtpValidationChecklistIntractor {
        interface OnFinishedListener {
            void onFinished(OTPResponse otpVliadationChecklistResponse);

            void onFailure(Throwable t);
        }

        void getOtpValidationChecklistSuccessful(DocketValidationContract.OtpValidationChecklistIntractor.OnFinishedListener onFinishedListener, OTPRequest otpVliadationChecklistRequest);
    }

    interface ValidateChecklistYesNoIntractor {
        interface OnFinishedListener {
            void onFinished(ValidateChecklistYesNoResponse validateChecklistYesNoResponse);

            void onFailure(Throwable t);
        }

        void getValidateChecklistYesNoSuccessful(DocketValidationContract.ValidateChecklistYesNoIntractor.OnFinishedListener onFinishedListener, ValidateCheclistYesNoRequest validateCheclistYesNoRequest);
    }
}
