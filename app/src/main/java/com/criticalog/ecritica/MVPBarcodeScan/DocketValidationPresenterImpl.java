package com.criticalog.ecritica.MVPBarcodeScan;

import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidationRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.OtpVliadationChecklistRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.OtpVliadationChecklistResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateChecklistYesNoResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateCheclistYesNoRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;

public class DocketValidationPresenterImpl implements DocketValidationContract.presenter, DocketValidationContract.GetDocketValidationIntractor.OnFinishedListener, DocketValidationContract.GetPickupSuccessIntractor.OnFinishedListener, DocketValidationContract.GetPincodeValidateIntractor.OnFinishedListener, DocketValidationContract.GetCloseDocketIntractor.OnFinishedListener, DocketValidationContract.GetCloseClientIntractor.OnFinishedListener, DocketValidationContract.GetQCRulesIntractor.OnFinishedListener, DocketValidationContract.GetQCItemListIntractor.OnFinishedListener, DocketValidationContract.GetQCRulesCheckIntractor.OnFinishedListener, DocketValidationContract.GetQCUpdateIntractor.OnFinishedListener, DocketValidationContract.GetQCNDCUpdateIntractor.OnFinishedListener, DocketValidationContract.GetDocketListWithOriginDestIntractor.OnFinishedListener, DocketValidationContract.ActualValueCheckIntractor.OnFinishedListener, DocketValidationContract.OtpValidationChecklistIntractor.OnFinishedListener, DocketValidationContract.ValidateChecklistYesNoIntractor.OnFinishedListener {

    private DocketValidationContract.MainView mainView;
    private DocketValidationContract.GetDocketValidationIntractor getDocketValidationIntractor;
    private DocketValidationContract.GetPickupSuccessIntractor getPickupSuccessIntractor;
    private DocketValidationContract.GetPincodeValidateIntractor getPincodeValidateIntractor;
    private DocketValidationContract.GetCloseDocketIntractor getCloseDocketIntractor;
    private DocketValidationContract.GetCloseClientIntractor getCloseClientIntractor;
    private DocketValidationContract.GetQCRulesIntractor getQCRulesIntractor;
    private DocketValidationContract.GetQCItemListIntractor getQCItemListIntractor;
    private DocketValidationContract.GetQCRulesCheckIntractor getQCRulesCheckIntractor;
    private DocketValidationContract.GetQCUpdateIntractor getQCUpdateIntractor;
    private DocketValidationContract.GetQCNDCUpdateIntractor getQCNDCUpdateIntractor;
    private DocketValidationContract.GetDocketListWithOriginDestIntractor getDocketListWithOriginDestIntractor;
    private DocketValidationContract.ActualValueCheckIntractor actualValueCheckIntractor;
    private DocketValidationContract.OtpValidationChecklistIntractor otpValidationChecklistIntractor;
    private DocketValidationContract.ValidateChecklistYesNoIntractor validationChecklistIntractor;


    public DocketValidationPresenterImpl(DocketValidationContract.MainView mainView, DocketValidationContract.GetDocketValidationIntractor getDocketValidationIntractor, DocketValidationContract.GetPickupSuccessIntractor getPickupSuccessIntractor, DocketValidationContract.GetPincodeValidateIntractor getPincodeValidateIntractor, DocketValidationContract.GetCloseDocketIntractor getCloseDocketIntractor,
                                         DocketValidationContract.GetCloseClientIntractor getCloseClientIntractor, DocketValidationContract.GetQCRulesIntractor getQCRulesIntractor,
                                         DocketValidationContract.GetQCItemListIntractor getQCItemListIntractor,
                                         DocketValidationContract.GetQCRulesCheckIntractor getQCRulesCheckIntractor,
                                         DocketValidationContract.GetQCUpdateIntractor getQCUpdateIntractor,
                                         DocketValidationContract.GetQCNDCUpdateIntractor getQCNDCUpdateIntractor,
                                         DocketValidationContract.GetDocketListWithOriginDestIntractor getDocketListWithOriginDestIntractor,
                                         DocketValidationContract.ActualValueCheckIntractor actualValueCheckIntractor,
                                         DocketValidationContract.OtpValidationChecklistIntractor otpValidationChecklistIntractor,
                                         DocketValidationContract.ValidateChecklistYesNoIntractor validationChecklistIntractor) {
        this.mainView = mainView;
        this.getDocketValidationIntractor = getDocketValidationIntractor;
        this.getPickupSuccessIntractor = getPickupSuccessIntractor;
        this.getPincodeValidateIntractor = getPincodeValidateIntractor;
        this.getCloseDocketIntractor = getCloseDocketIntractor;
        this.getCloseClientIntractor = getCloseClientIntractor;
        this.getQCRulesIntractor = getQCRulesIntractor;
        this.getQCItemListIntractor = getQCItemListIntractor;
        this.getQCRulesCheckIntractor = getQCRulesCheckIntractor;
        this.getQCUpdateIntractor = getQCUpdateIntractor;
        this.getQCNDCUpdateIntractor = getQCNDCUpdateIntractor;
        this.getDocketListWithOriginDestIntractor = getDocketListWithOriginDestIntractor;
        this.actualValueCheckIntractor = actualValueCheckIntractor;
        this.otpValidationChecklistIntractor = otpValidationChecklistIntractor;
        this.validationChecklistIntractor = validationChecklistIntractor;
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void docketValidationRequest(String token, DocketValidationRequest docketValidationRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getDocketValidationIntractor.docketValidationSuccessful(this, token, docketValidationRequest);
    }

    @Override
    public void pickupRequest(String token, PickupSuccessRequest pickupSuccessRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getPickupSuccessIntractor.pickupSuccessful(this, token, pickupSuccessRequest);
    }

    @Override
    public void picodeValidationRequest(String token, PincodeValidateRequest pincodeValidateRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getPincodeValidateIntractor.pincodeValidateSuccessful(this, token, pincodeValidateRequest);
    }

    @Override
    public void closeDocketRequest(String token, CloseDocketRequest closeDocketRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getCloseDocketIntractor.closeDocketSuccessful(this, token, closeDocketRequest);

    }

    @Override
    public void closeClientRequest(String token, CloseClientRequest closeClientRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getCloseClientIntractor.closeClientSuccessful(this, token, closeClientRequest);
    }

    @Override
    public void qcItemListRequest(ItemListRequest itemListRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getQCItemListIntractor.qcItemListSuccessful(this, itemListRequest);
    }

    @Override
    public void requestQCrules(QCRulesRequest qcRulesRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getQCRulesIntractor.qcRulesSuccessful(this, qcRulesRequest);
    }

    @Override
    public void qcCheck(QcRulesCheckRequest qcRulesCheckRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }

        getQCRulesCheckIntractor.qcRulesCheckSuccessful(this, qcRulesCheckRequest);
    }

    @Override
    public void qcNDCUpdate(QCPassFailRequest qcPassFailRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getQCNDCUpdateIntractor.qcNDCUpdateSuccessful(this, qcPassFailRequest);
    }

    @Override
    public void qcUpdateRequest(QcRulesUpdateRequest qcRulesUpdateRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }

        getQCUpdateIntractor.qcUpdateSuccessful(this, qcRulesUpdateRequest);
    }

    @Override
    public void actualValueCheck(ActualValueCheckRequest actualValueCheckRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        actualValueCheckIntractor.getActualValueSuccessful(this, actualValueCheckRequest);
    }

    @Override
    public void otpValidationForChecklist(OTPRequest otpVliadationChecklistRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        otpValidationChecklistIntractor.getOtpValidationChecklistSuccessful(this, otpVliadationChecklistRequest);
    }

    @Override
    public void validateChecklistYesNo(ValidateCheclistYesNoRequest validateCheclistYesNoRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        validationChecklistIntractor.getValidateChecklistYesNoSuccessful(this, validateCheclistYesNoRequest);
    }

    @Override
    public void getDocketListWithOriginDestRequest(DocketListWithOriginDestRequest docketListWithOriginDestRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        getDocketListWithOriginDestIntractor.getDocketListWithOriginDestSuccessful(this, docketListWithOriginDestRequest);
    }

    @Override
    public void onFinished(DocketValidateResponse docketValidateResponse) {
        if (mainView != null) {
            mainView.setDataToViews(docketValidateResponse);
            mainView.hideProgress();
        }
    }


    @Override
    public void onFinished(PickupSuccessResponse pickupSuccessResponse) {
        if (mainView != null) {
            mainView.pickUpSuccessViews(pickupSuccessResponse);
            if (pickupSuccessResponse.getStatus() == 200) {
                mainView.hideProgress();
            } else {
                mainView.hideProgress();
            }

        }

    }

    @Override
    public void onFinished(PincodeValidateResponse pincodeValidateResponse) {
        if (mainView != null) {
            mainView.pincodeValidateSuccessViews(pincodeValidateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CloseDocketResponse closeDocketResponse) {
        if (mainView != null) {
            mainView.closeDocketSuccessViews(closeDocketResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(CloseClientResponse closeClientResponse) {
        if (mainView != null) {
            mainView.closeClientSuccessViews(closeClientResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(QCRulesResponse qcRulesResponse) {
        if (mainView != null) {
            mainView.qcRules(qcRulesResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(ItemListResponse itemListResponse) {
        if (mainView != null) {
            mainView.qcItems(itemListResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(QcRulesCheckResponse qcRulesCheckResponse) {
        if (mainView != null) {
            mainView.qcCheckResponse(qcRulesCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(QcRulesUpdateResponse qcRulesUpdateResponse) {
        if (mainView != null) {
            mainView.qcUpdateResponse(qcRulesUpdateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(QCPassFailResponse qcPassFailResponse) {
        if (mainView != null) {
            mainView.qcNDCUpdateResponse(qcPassFailResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(DocketListWithOriginDestResponse docketListWithOriginDestResponse) {
        if (mainView != null) {
            mainView.getDocketListWithOriginDestResponse(docketListWithOriginDestResponse);
            mainView.hideProgress();

        }
    }

    @Override
    public void onFinished(ActualValueCheckResponse actualValueCheckResponse) {
        if (mainView != null) {
            mainView.actualValueResponse(actualValueCheckResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(OTPResponse otpVliadationChecklistResponse) {
        if (mainView != null) {
            mainView.otpValidationChecklistResponse(otpVliadationChecklistResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(ValidateChecklistYesNoResponse validateChecklistYesNoResponse) {
        if (mainView != null) {
            mainView.validateChecklistYesNoResponse(validateChecklistYesNoResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
