package com.criticalog.ecritica.MVPBarcodeScan.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Interface.IClickOnDocket;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestDatum;
import com.criticalog.ecritica.MVPCashHandover.Adapters.HandedOverDetailsNewAdapter;
import com.criticalog.ecritica.MVPCashHandover.model.DocketJson;
import com.criticalog.ecritica.MVPPickup.ClientDetail;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DocketWithOriginDestAdapter extends RecyclerView.Adapter<DocketWithOriginDestAdapter.DocketWithOriginDestAdapterViewHolder> implements Filterable {

    IClickOnDocket iClickOnDocket;
    List<DocketListWithOriginDestDatum> docketListWithOriginDestDataList;
    private List<DocketListWithOriginDestDatum> mfilteredlist = new ArrayList<>();

    private Context mContext;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    public DocketWithOriginDestAdapter(Context mContext,List<DocketListWithOriginDestDatum> docketListWithOriginDestDataList,IClickOnDocket iClickOnDocket) {
        this.iClickOnDocket = iClickOnDocket;
        this.docketListWithOriginDestDataList=docketListWithOriginDestDataList;
        this.mContext=mContext;
        mCriticalogSharedPreferences=CriticalogSharedPreferences.getInstance(mContext);
    }


    @NonNull
    @Override
    public DocketWithOriginDestAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.docket_list_item, parent, false);
        return new DocketWithOriginDestAdapter.DocketWithOriginDestAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DocketWithOriginDestAdapterViewHolder holder, int position) {
        holder.mDocket.setText(docketListWithOriginDestDataList.get(position).getDocketNum());
        holder.mOrigin.setText(docketListWithOriginDestDataList.get(position).getOrigin());
        holder.mDestination.setText(docketListWithOriginDestDataList.get(position).getDestination());

        holder.mLayoutList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iClickOnDocket.clickOnDcocketList(holder.mDocket.getText().toString(),holder.mOrigin.getText().toString(),holder.mDestination.getText().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return docketListWithOriginDestDataList.size();

    }

    public class DocketWithOriginDestAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView mDocket, mOrigin, mDestination;
        LinearLayout mLayoutList;

        public DocketWithOriginDestAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            mDocket = itemView.findViewById(R.id.mDocket);
            mOrigin = itemView.findViewById(R.id.mOrigin);
            mDestination = itemView.findViewById(R.id.mDestination);
            mLayoutList=itemView.findViewById(R.id.mLayoutList);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Toast.makeText(mContext, charString, Toast.LENGTH_SHORT).show();
                if (charString.isEmpty()) {
                    mfilteredlist = docketListWithOriginDestDataList;
                } else {
                    List<DocketListWithOriginDestDatum> filteredList = new ArrayList<>();
                    for (DocketListWithOriginDestDatum row : docketListWithOriginDestDataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getDocketNum().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mfilteredlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mfilteredlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mfilteredlist = (ArrayList<DocketListWithOriginDestDatum>) filterResults.values;
                // refresh the list with filtered data
               // notifyDataSetChanged();

                ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                        //context.refreshInbox();
                    }
                });

            }
        };

    }


}
