package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ValidateCheclistYesNoRequest {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("client_code")
    @Expose
    private String clientCode;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("check_type")
    @Expose
    private String checkType;

    @SerializedName("remark")
    @Expose
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @SerializedName("checklist_response")
    @Expose
    private List<ChecklistResponse> checklistResponse;

    @SerializedName("connote_no")
    @Expose
    private String connote_no;

    public String getConnote_no() {
        return connote_no;
    }

    public void setConnote_no(String connote_no) {
        this.connote_no = connote_no;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public List<ChecklistResponse> getChecklistResponse() {
        return checklistResponse;
    }

    public void setChecklistResponse(List<ChecklistResponse> checklistResponse) {
        this.checklistResponse = checklistResponse;
    }
}
