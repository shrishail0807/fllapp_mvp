package com.criticalog.ecritica.MVPBarcodeScan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.criticalog.ecritica.Activities.TorchOnCaptureActivity;
import com.criticalog.ecritica.Adapters.InscanBoxAdapter;
import com.criticalog.ecritica.Adapters.RulesAdapterWithFlag;
import com.criticalog.ecritica.BuildConfig;
import com.criticalog.ecritica.Dao.DatabaseHelper;
import com.criticalog.ecritica.Interface.IClickOnDocket;
import com.criticalog.ecritica.Interface.OnclickOpenAdapter;
import com.criticalog.ecritica.Interface.QCAdapterClicked;
import com.criticalog.ecritica.Interface.QCitemAdaterClick;
import com.criticalog.ecritica.Interface.RulesClicked;
import com.criticalog.ecritica.MVPBarcodeScan.Adapter.CustomAutoSuggestAdapter;
import com.criticalog.ecritica.MVPBarcodeScan.Adapter.DocketWithOriginDestAdapter;
import com.criticalog.ecritica.MVPBarcodeScan.Adapter.OpenPickupAdpater;
import com.criticalog.ecritica.MVPBarcodeScan.Adapter.RulesHorizontalAdapter;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseClientModel.CloseClientResponse;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketRequest;
import com.criticalog.ecritica.MVPBarcodeScan.CloseDocketModel.CloseDocketResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPickupSuccess.PickupSuccessResponse;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.PickUpSuccess.ModelPincodeValidate.PincodeValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ActualValueCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ChecklistResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DatumItemQc;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestDatum;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketListWithOriginDestResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.DocketValidationRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.ItemListResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCPassFailResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QCRulesResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesCheckResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateRequest;
import com.criticalog.ecritica.MVPBarcodeScan.model.QcRulesUpdateResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateChecklistYesNoResponse;
import com.criticalog.ecritica.MVPBarcodeScan.model.ValidateCheclistYesNoRequest;
import com.criticalog.ecritica.MVPDRS.Adapter.RulesAdapter;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogRequest;
import com.criticalog.ecritica.MVPHomeScreen.Model.RiderLogResponse;
import com.criticalog.ecritica.MVPInscan.InscanContract;
import com.criticalog.ecritica.MVPInscan.InscanInteractorImpl;
import com.criticalog.ecritica.MVPInscan.InscanPresenterImpl;
import com.criticalog.ecritica.MVPInscan.Model.ActualValueInscanResponse;
import com.criticalog.ecritica.MVPInscan.Model.ChecklistScan;
import com.criticalog.ecritica.MVPInscan.Model.DocketCheckResponse;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateRequest;
import com.criticalog.ecritica.MVPInscan.Model.EwayBillValidateResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanBoxResponse;
import com.criticalog.ecritica.MVPInscan.Model.InScanSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanGetItemDetailsResponse;
import com.criticalog.ecritica.MVPInscan.Model.InscanNegativeStatusResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionDoneResponse;
import com.criticalog.ecritica.MVPInscan.Model.InspectionNegSubmitResponse;
import com.criticalog.ecritica.MVPInscan.Model.OriginPincodeValidateResponse;
import com.criticalog.ecritica.MVPLinehaul.LinehaulModel.SampleSearchModel;
import com.criticalog.ecritica.MVPOTP.models.OTPRequest;
import com.criticalog.ecritica.MVPOTP.models.OTPResponse;
import com.criticalog.ecritica.MVPPickup.PickupActivity;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel.PickupCancelRequest;
import com.criticalog.ecritica.MVPPickup.PickupCancel.PickupCancelModel.PickupCancelResponse;
import com.criticalog.ecritica.ModelClasses.OpenImage;
import com.criticalog.ecritica.QCModel.ItemsAdapter;
import com.criticalog.ecritica.QCModel.QCAdapter;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.PodClaimPojo;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.connotegeneration.ConnoteGenerationActivity;
import com.criticalog.ecritica.model.PrsRulesQc;
import com.criticalog.ecritica.model.RulesRequest;
import com.criticalog.ecritica.model.RulesResponse;
import com.criticalog.ecritica.mvvmtatacliqqc.TataCliqQcActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OtpTextView;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarcodeScanActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, DocketValidationContract.MainView, InscanContract.MainView, RulesClicked, QCitemAdaterClick, QCAdapterClicked, IClickOnDocket, OnclickOpenAdapter {
    private Dialog dialogManualEntry, closeDocketClientDialog;
    private LinearLayout mMainBarcodeScanLayout;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String navigationStatus;
    private String docket, itemIdToSend, qcIdToSned;
    private EditText mDocketEntry, mTotalBox, mActualValue, mDestPincode, mEwayBillNo;
    private TextView mAddDocket, mScannedResult, mControllingHub, mCity, mConnoteGeneration;
    public DatabaseHelper dbHelper;
    DocketValidationContract.presenter mPresenter;
    String token, userId, openPickupFlag, noOfCons = "", gatePassFlag = "";
    private SearchView searchViewDocket;
    private String gatePassImageIRL="";

    private TextView xtv_name, xtv_id, clientCode, mGatePassHeaderText;
    private RestServices mRestServices;

    private ImageView mPickupImage1, mPickupImage2, mGatePassImage;
    File photoFile = null;
    private static int CAMERA_REQUEST = 1888;
    private static int CAMERA_REQUEST_QC = 1889;
    private static int CAMERA_REQUEST_MULT = 1890;
    private static int CAMERA_REQUEST_GATEPASS = 1891;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private String base64CapturedImage1 = "", base64CapturedImage2 = "", base64CapturedQCImage = "", base64CapturedMultImage = "", base64CapturedGatePass = "";
    private Button mSubmit;
    private CheckBox mNfoCheckbox;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private boolean isGPS = false;
    private double latitude = 0.0, longitude = 0.0;
    private String totalBox;
    String nfoFlag = "0";
    private AlertDialog DialogFinish;
    SimpleArcDialog mProgressDialog;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private SimpleArcDialog mProgressDialog1;

    private Dialog dialogRules, dialogQcItems, dialohShowControllingHub;
    private ImageView mCancelDialog;
    private long mLastClickTime = 0;
    private LinearLayout mEwayBillEnterLayout;
    private TextView mEwayBillText, mCloseClientDirectly;
    InscanContract.presenter mPresenterEwayBill;
    private ImageView mAddEwayBill, mBarcodeScanBag;
    private EditText mEwayBill;
    List<PrsRulesQc> prsRules = new ArrayList<>();
    List<OpenImage> openImageList = new ArrayList<>();
    RecyclerView mRvRules;
    private Dialog docketListDialog;

    ArrayList ewayBillListToSend = new ArrayList();
    private String scanWhat = "";
    private RecyclerView mRvEwayBill;
    InscanBoxAdapter inscanBoxAdapter;
    private int prsRuleIndex = 0;
    private String forcePrsCheck, qc_rules;
    List<ChecklistResponse> prsRulesList = new ArrayList<>();
    List<ChecklistScan> prsRulesListPreview = new ArrayList<>();
    private String remarks = "";
    TextView mOkRule;
    RulesHorizontalAdapter rulesHorizontalAdapter;
    LinearLayoutManager layoutManager;

    List<String> stringList = new ArrayList<>();

    TextView mProductCode, mProductDescription, mSplitPcs, mLength, mB, mH;

    RecyclerView mQCRV;
    QCAdapter mQcAdapter;
    Dialog qcNDCUpdateDialog;
    Location gps_loc;
    Location network_loc;
    Location final_loc;
    private Dialog dialogProductDelivered;
    ArrayList<SampleSearchModel> createConnoteList = new ArrayList();
    ArrayList<String> stringArrayListConnote = new ArrayList<>();
    DocketWithOriginDestAdapter docketWithOriginDestAdapter;

    AutoCompleteTextView autotext;
    private String actualValueRequired;
    private RecyclerView mRvOpenPickup;
    private OpenPickupAdpater mOpenPickupAdpater;
    private LinearLayout mOpenPickupImageLayout;
    private int positionClicked;
    private String openPickupText;

    private String adapterRefreshFlag = "", mobielNumber = "",
            forceChecListOTPEnabled = "", connoteAuomation = "";
    private Dialog dialogCheckListWithFlag;
    private String verifyOrSendOtp = "";
    LinearLayout mChecklistWithFalgLinLayout, mOtpLinLayout;
    List<DocketListWithOriginDestDatum> suggestindList = new ArrayList<>();
    CustomAutoSuggestAdapter adapter;
    TextView scanWithManualyOption, mSelectYesNoAlert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_mvp);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");
        mRestServices = RestClient.getRetrofitInstance().create(RestServices.class);
        // Create the Database helper object
        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();


        mPresenterEwayBill = new InscanPresenterImpl(BarcodeScanActivity.this, new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl(), new InscanInteractorImpl());

        mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl());

        checkGpsEnabledOrNot();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        //Dialog Manual Entry
        dialogManualEntry = new Dialog(BarcodeScanActivity.this);
        dialogManualEntry.setContentView(R.layout.dialog_manula_scan_autocomplete);
        dialogManualEntry.setCanceledOnTouchOutside(false);
        dialogManualEntry.setCancelable(false);

        autotext = dialogManualEntry.findViewById(R.id.autoCompleteTextView);


        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(100); // 10 seconds
        locationRequest.setFastestInterval(100); // 5 seconds

        autotext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                autotext.showDropDown();
            }
        });

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.d("barcode oncreate :" + "lat " + latitude, "long" + longitude);
                        //  Toast.makeText(BarcodeScanActivity.this,"lat"+latitude + "long"+longitude,Toast.LENGTH_SHORT).show();

                        if (mfusedLocationproviderClient != null) {
                            mfusedLocationproviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        getLocation();

        closeDocketClientDialog = new Dialog(BarcodeScanActivity.this);
        closeDocketClientDialog.setContentView(R.layout.dialog_docket_client_close);

        mMainBarcodeScanLayout = findViewById(R.id.mMainBarcodeScanLayout);


        mCriticalogSharedPreferences.saveData("destination_loc", "");
        mCriticalogSharedPreferences.saveData("dest_mail", "");
        mCriticalogSharedPreferences.saveData("dest_hub_id", "");
        token = mCriticalogSharedPreferences.getData("token");
        userId = mCriticalogSharedPreferences.getData("userId");
        noOfCons = mCriticalogSharedPreferences.getData("cons_count");
        gatePassFlag = mCriticalogSharedPreferences.getData("gatepass_upload");



        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey = mCriticalogSharedPreferences.getData("distance_api_key");
        StaticUtils.distance_api_url = mCriticalogSharedPreferences.getData("distance_api_url");

        openPickupFlag = mCriticalogSharedPreferences.getData("open_pickup_flag");
        forcePrsCheck = mCriticalogSharedPreferences.getData("force_prs_check");
        mCriticalogSharedPreferences.saveData("coming_from_qc", "");

        mobielNumber = mCriticalogSharedPreferences.getData("mobile_number_for_otp");
        forceChecListOTPEnabled = mCriticalogSharedPreferences.getData("force_prs_checklist_otp");
        connoteAuomation = mCriticalogSharedPreferences.getData("generate_cit");

      /*  connoteAuomation = "1";
        noOfCons = "1";*/

        mDocketEntry = dialogManualEntry.findViewById(R.id.mDocketEntry);
        mAddDocket = dialogManualEntry.findViewById(R.id.mAddDocket);
        mConnoteGeneration = dialogManualEntry.findViewById(R.id.mConnoteGeneration);
        mCancelDialog = dialogManualEntry.findViewById(R.id.mCancelDialog);
        mCloseClientDirectly = dialogManualEntry.findViewById(R.id.mCloseClientDirectly);
        scanWithManualyOption = dialogManualEntry.findViewById(R.id.mScanBarcode);


        mEwayBillText = findViewById(R.id.mEwayBillText);


        if (connoteAuomation.equalsIgnoreCase("1")) {
            mConnoteGeneration.setVisibility(View.VISIBLE);
        } else {
            mConnoteGeneration.setVisibility(View.GONE);
        }
        if (noOfCons.equalsIgnoreCase("1")) {
            autotext.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdown, 0);
        } else {
            autotext.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        mSubmit = findViewById(R.id.mSubmit);
        mNfoCheckbox = findViewById(R.id.mNfoCheckbox);
        xtv_name = findViewById(R.id.xtv_name);

        mTotalBox = findViewById(R.id.mTotalBox);
        mDestPincode = findViewById(R.id.mDestPincode);
        mActualValue = findViewById(R.id.mActualValue);
        mScannedResult = findViewById(R.id.mScannedResult);
        clientCode = findViewById(R.id.clientCode);
        xtv_id = findViewById(R.id.xtv_id);
        mEwayBillNo = findViewById(R.id.mEwayBillNo);
        mEwayBillEnterLayout = findViewById(R.id.mEwayBillEnterLayout);
        mAddEwayBill = findViewById(R.id.mAddEwayBill);
        mBarcodeScanBag = findViewById(R.id.mBarcodeScanBag);
        mEwayBill = findViewById(R.id.mEwayBill);
        mRvEwayBill = findViewById(R.id.mRvEwayBill);
        mGatePassImage = findViewById(R.id.mGatePassImage);
        mGatePassHeaderText = findViewById(R.id.mGatePassHeaderText);


        navigationStatus = mCriticalogSharedPreferences.getData("navigationStatus");

        mPickupImage1 = findViewById(R.id.mPickupImage1);
        mPickupImage2 = findViewById(R.id.mPickupImage2);
        mControllingHub = findViewById(R.id.mControllingHub);
        mCity = findViewById(R.id.mCity);
        mRvOpenPickup = findViewById(R.id.mRvOpenPickup);
        mOpenPickupImageLayout = findViewById(R.id.mOpenPickupImageLayout);

        inscanBoxAdapter = new InscanBoxAdapter(ewayBillListToSend, this);
        mRvEwayBill.setLayoutManager(new GridLayoutManager(this, 2));
        mRvEwayBill.setAdapter(inscanBoxAdapter);


        if (gatePassFlag.equalsIgnoreCase("0")) {
            mGatePassImage.setVisibility(View.GONE);
            mGatePassHeaderText.setVisibility(View.GONE);
        } else {
            mGatePassImage.setVisibility(View.VISIBLE);
            mGatePassHeaderText.setVisibility(View.VISIBLE);
        }

        mCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogManualEntry.dismiss();
                finish();
                Intent intent = new Intent(BarcodeScanActivity.this, PickupActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        xtv_name.setText(mCriticalogSharedPreferences.getData("username"));
        xtv_id.setText(userId);
        clientCode.setText(mCriticalogSharedPreferences.getData("client_code"));
        if (navigationStatus.equalsIgnoreCase("manual")) {

            if (!noOfCons.equalsIgnoreCase("0")) {
                DocketListWithOriginDestRequest docketListWithOriginDestRequest = new DocketListWithOriginDestRequest();
                docketListWithOriginDestRequest.setAction("get_docket_list");
                docketListWithOriginDestRequest.setUserId(userId);
                docketListWithOriginDestRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
                docketListWithOriginDestRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                mPresenter.getDocketListWithOriginDestRequest(docketListWithOriginDestRequest);

                mDocketEntry.setHint("Click Here to Get Dockets");
                mDocketEntry.setFocusable(false);

                mDocketEntry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createConnoteList.clear();
                        //showHandedOverDetailsDialog();
                        //Toast.makeText(BarcodeScanActivity.this, "Click for Get Docktes", Toast.LENGTH_SHORT).show();
                        DocketListWithOriginDestRequest docketListWithOriginDestRequest = new DocketListWithOriginDestRequest();
                        docketListWithOriginDestRequest.setAction("get_docket_list");
                        docketListWithOriginDestRequest.setUserId(userId);
                        docketListWithOriginDestRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
                        docketListWithOriginDestRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                        mPresenter.getDocketListWithOriginDestRequest(docketListWithOriginDestRequest);
                    }
                });

            }

            Window window = dialogManualEntry.getWindow();
            // Set the dialog size and position
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            // Set the soft input mode to adjust the dialog size when the keyboard is shown
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialogManualEntry.show();
        } else if (navigationStatus.equalsIgnoreCase("con_generation")) {
            docket = mCriticalogSharedPreferences.getData("generated_con");
            docketValidationCheck(docket);
        } else {
            startBarcodeScan();
        }

        mAddEwayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ewayBillNum = mEwayBill.getText().toString();
                String noOfEwayBill = mEwayBillNo.getText().toString();
                if (ewayBillNum.equals("")) {
                    Toasty.warning(BarcodeScanActivity.this, R.string.enter_no_of_eway, Toast.LENGTH_SHORT, true).show();
                } else if (ewayBillNum.length() != 12) {
                    Toasty.warning(BarcodeScanActivity.this, R.string.enter_valid_eway, Toast.LENGTH_SHORT, true).show();
                } else if (noOfEwayBill.equals("")) {
                    Toasty.warning(BarcodeScanActivity.this, R.string.enter_no_of_eway, Toast.LENGTH_SHORT, true).show();
                } else {
                    callEwayBillVlaidate(ewayBillNum);
                }
            }
        });

        mBarcodeScanBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String noOfEwayBill = mEwayBillNo.getText().toString();
                if (noOfEwayBill.equals("")) {
                    Toasty.warning(BarcodeScanActivity.this, R.string.enter_no_of_eway, Toast.LENGTH_SHORT, true).show();
                } else {
                    scanWhat = "ewayBill";
                    startBarcodeScan();
                }
            }
        });
        mDestPincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String pincode = s.toString();

                if (pincode.length() == 6) {

                    pincodeValidation(pincode);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEwayBillNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ewaybillNo = s.toString();

                if (ewaybillNo.equalsIgnoreCase("0")) {

                } else if (ewaybillNo.equalsIgnoreCase("00")) {

                } else if (ewaybillNo.equalsIgnoreCase("")) {
                    //  Toast.makeText(BarcodeScanActivity.this, "No. of Eway bill cannot be zero", Toast.LENGTH_SHORT).show();
                } else {
                    // mEwayBillNo.setEnabled(false);
                    mEwayBillEnterLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mActualValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    // cursor is out of the EditText
                    // do something here

                    String actualValue = mActualValue.getText().toString();

                    if (!actualValue.equals("")) {

                        if (Integer.parseInt(actualValue) != 0) {
                            if (Integer.parseInt(actualValue) >= 1) {

                                ActualValueCheckRequest actualValueCheckRequest = new ActualValueCheckRequest();
                                actualValueCheckRequest.setAction("inscan_actualvalue_new");
                                actualValueCheckRequest.setActualVal(String.valueOf(actualValue));
                                actualValueCheckRequest.setUserId(userId);
                                actualValueCheckRequest.setOrigin(mCriticalogSharedPreferences.getData("srcPin"));
                                actualValueCheckRequest.setDest(mDestPincode.getText().toString());
                                actualValueCheckRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                                mPresenter.actualValueCheck(actualValueCheckRequest);

                            } else {
                                mEwayBillNo.setText("");
                                mEwayBillNo.setHint(R.string.enter_no_of_eway_bill);
                                actualValueRequired = "Eway bill not required";
                                // ewayBillListToSend.clear();
                                // inscanBoxAdapter.notifyDataSetChanged();
                            }
                        } else {
                            mActualValue.setText("");
                            mActualValue.setHint(R.string.enter_actual_value);
                            Toast.makeText(BarcodeScanActivity.this, "Actual Value can't be Zero", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        mActualValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String actualValue = s.toString();

                if (!actualValue.equals("")) {

                    if (Integer.parseInt(actualValue) != 0) {
                        if (Integer.parseInt(actualValue) >= 50000) {

                            ActualValueCheckRequest actualValueCheckRequest = new ActualValueCheckRequest();
                            actualValueCheckRequest.setAction("inscan_actualvalue_new");
                            actualValueCheckRequest.setActualVal(String.valueOf(actualValue));
                            actualValueCheckRequest.setUserId(userId);
                            actualValueCheckRequest.setOrigin(mCriticalogSharedPreferences.getData("srcPin"));
                            actualValueCheckRequest.setDest(mDestPincode.getText().toString());
                            actualValueCheckRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                            mPresenter.actualValueCheck(actualValueCheckRequest);

                        } else {

                            mEwayBillNo.setText("");
                            mEwayBillNo.setHint(R.string.enter_no_of_eway_bill);
                            actualValueRequired = "Eway bill not required";
                            // ewayBillListToSend.clear();
                            //  inscanBoxAdapter.notifyDataSetChanged();
                        }
                    } else {
                        mActualValue.setText("");
                        mActualValue.setHint(R.string.enter_actual_value);
                        Toast.makeText(BarcodeScanActivity.this, "Actual Value can't be Zero", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mTotalBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String pincode = s.toString();

                if (pincode.equalsIgnoreCase("0")) {
                    Toast.makeText(BarcodeScanActivity.this, "Enter Valid Box number!!", Toast.LENGTH_SHORT).show();
                } else {
                    mDestPincode.setActivated(true);
                    mDestPincode.setPressed(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mGatePassImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gatePassImageIRL="https://source.unsplash.com/user/c_v_r/1900x800";
                if(!gatePassImageIRL.equalsIgnoreCase(""))
                {
                    showGatePassDialog(gatePassImageIRL);
                }else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        } else {
                            // mCriticalogSharedPreferences.saveData("image", "first");
                            openCameraIntent(CAMERA_REQUEST_GATEPASS);
                        }
                    } else {
                        // mCriticalogSharedPreferences.saveData("image", "first");
                        openCameraIntent(CAMERA_REQUEST_GATEPASS);
                    }
                }



               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        // mCriticalogSharedPreferences.saveData("image", "first");
                        openCameraIntent(CAMERA_REQUEST_GATEPASS);
                    }
                } else {
                    // mCriticalogSharedPreferences.saveData("image", "first");
                    openCameraIntent(CAMERA_REQUEST_GATEPASS);
                }*/
            }
        });
        mPickupImage1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        mCriticalogSharedPreferences.saveData("image", "first");
                        openCameraIntent(CAMERA_REQUEST);
                    }
                } else {
                    mCriticalogSharedPreferences.saveData("image", "first");
                    openCameraIntent(CAMERA_REQUEST);
                }

            }
        });
        mPickupImage2.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        mCriticalogSharedPreferences.saveData("image", "second");
                        openCameraIntent(CAMERA_REQUEST);
                    }
                } else {
                    mCriticalogSharedPreferences.saveData("image", "second");
                    openCameraIntent(CAMERA_REQUEST);
                }

            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                // do your magic here

                getLocation();


                String totalBox = mTotalBox.getText().toString();
                String destinationPincode = mDestPincode.getText().toString();
                String actualValue = mActualValue.getText().toString();
                if (mNfoCheckbox.isChecked()) {
                    nfoFlag = "1";
                } else {
                    nfoFlag = "0";
                }
                if (totalBox.equals("")) {
                    Toast.makeText(BarcodeScanActivity.this, R.string.enter_total_number_of_box, Toast.LENGTH_SHORT).show();
                } else if (destinationPincode.equals("")) {
                    Toast.makeText(BarcodeScanActivity.this, R.string.enter_dest_pincode, Toast.LENGTH_SHORT).show();
                } else if (destinationPincode.length() != 6) {
                    Toast.makeText(BarcodeScanActivity.this, R.string.enter_dest_pincode, Toast.LENGTH_SHORT).show();
                } else if (actualValue.equals("")) {
                    Toast.makeText(BarcodeScanActivity.this, R.string.enter_actual_value, Toast.LENGTH_SHORT).show();
                } else if (totalBox.equals("0")) {
                    Toast.makeText(BarcodeScanActivity.this, "Total Box can't be Zero", Toast.LENGTH_SHORT).show();
                } else if (actualValue.equals("0")) {
                    Toast.makeText(BarcodeScanActivity.this, "Actual value can't be Zero!", Toast.LENGTH_SHORT).show();
                } else {
                    if (actualValueRequired.equalsIgnoreCase("Eway bill required")) {
                        String eway = mEwayBillNo.getText().toString();
                        if (eway.equals("")) {
                            Toasty.warning(BarcodeScanActivity.this, "Enter Number of EwayBills", Toast.LENGTH_LONG, true).show();
                        } else {
                            if (ewayBillListToSend.size() == Integer.parseInt(mEwayBillNo.getText().toString())) {

                                if (openPickupFlag.equalsIgnoreCase("1")) {
                                    if (base64CapturedImage1.equals("")) {

                                        Toasty.warning(BarcodeScanActivity.this, "Upload Open Pickup Image", Toast.LENGTH_LONG, true).show();
                                    } else {
                                        callPickupSuccess();
                                    }

                                } else {
                                    callPickupSuccess();
                                }
                            } else {
                                Toasty.warning(BarcodeScanActivity.this, "Scan all Eway Bill Numbers!!", Toast.LENGTH_LONG, true).show();

                            }
                        }

                    } else {

                        String totalEway = mEwayBillNo.getText().toString();

                        if (totalEway.equalsIgnoreCase("")) {
                            totalEway = "0";
                        }
                        if (ewayBillListToSend.size() == Integer.parseInt(totalEway)) {

                            if (openPickupFlag.equalsIgnoreCase("1")) {
                                if (base64CapturedImage1.equals("")) {

                                    Toasty.warning(BarcodeScanActivity.this, "Upload Open Pickup Image", Toast.LENGTH_LONG, true).show();
                                } else {
                                    callPickupSuccess();

                                }

                            } else {
                                callPickupSuccess();
                            }
                        } else {
                            Toasty.warning(BarcodeScanActivity.this, "Scan all Eway Bill Numbers!!", Toast.LENGTH_LONG, true).show();

                        }

                    }
                }
            }

        });

        mConnoteGeneration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BarcodeScanActivity.this, ConnoteGenerationActivity.class));
            }
        });
        mAddDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docket = autotext.getText().toString();
                if (docket.equals("")) {
                    Toast.makeText(BarcodeScanActivity.this, "Enter 9 digit number", Toast.LENGTH_SHORT).show();
                    dialogManualEntry.show();
                } else if (docket.length() == 9) {

                    docketValidationCheck(docket);
                } else {
                    Toast.makeText(BarcodeScanActivity.this, "Enter 9 digit number", Toast.LENGTH_SHORT).show();
                    dialogManualEntry.show();
                }
            }
        });

        scanWithManualyOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBarcodeScan();
            }
        });

        mCloseClientDirectly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clientFinish("true");
            }
        });
    }


    public void showGatePassDialog(String bannerURL) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_gate_pass);
        // Set dialog title
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // set values for custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.mBannerText);
        ImageView image = (ImageView) dialog.findViewById(R.id.mImageBanner);
        ImageView cancelDialog = dialog.findViewById(R.id.mDialogCancel);
        TextView moreUpdates = dialog.findViewById(R.id.mMoreUpdates);

        moreUpdates.setText("OK");
        moreUpdates.setVisibility(View.GONE);
        cancelDialog.setVisibility(View.VISIBLE);

       // bannerURL = "https://source.unsplash.com/user/c_v_r/1900x800";
        Picasso.with(this).load(bannerURL).into(image);

        dialog.show();

        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        moreUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    public void checkGpsEnabledOrNot() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (checkPermission()) {
                //  getLocationRiderLog();
            } else {
                requestPermission();
            }
        } else {
            showSettingAlert();
        }
    }

    public void showSettingAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle("GPS setting!");
        alertDialog.setMessage("GPS is not enabled, Go to settings and enable GPS and Location? ");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();
    }


    public void callEwayBillVlaidate(String ewayBillNo) {
        ArrayList ewayBillList = new ArrayList();
        ewayBillList.add(ewayBillNo);
        EwayBillValidateRequest ewayBillValidateRequest = new EwayBillValidateRequest();
        ewayBillValidateRequest.setAction("validate_ewaybill_with_doc_portal");
        ewayBillValidateRequest.setUserId(userId);
        ewayBillValidateRequest.setDocket(docket);
        ewayBillValidateRequest.setLatitude(String.valueOf(latitude));
        ewayBillValidateRequest.setLongitude(String.valueOf(longitude));
        ewayBillValidateRequest.setScannedEwaybill(ewayBillList);
        mPresenterEwayBill.ewayBillValidate(ewayBillValidateRequest);
    }

    public void callPickupSuccess() {
        getLocation();
        if (openImageList.size() > 0) {
            for (int i = 0; i < openImageList.size(); i++) {
                if (openImageList.get(i).getImage().equalsIgnoreCase("")) {
                    Toast.makeText(this, "Capture All Open Pickup Images", Toast.LENGTH_SHORT).show();
                    break;
                }

               /* if(gatePassFlag.equalsIgnoreCase("1"))
                {
                    if(base64CapturedGatePass.equalsIgnoreCase(""))
                    {
                        Toasty.warning(BarcodeScanActivity.this, "Capture Gate Pass Image", Toast.LENGTH_LONG, true).show();
                        break;
                    }
                }*/

                if (i == openImageList.size() - 1) {
                    PickupSuccessRequest pickupSuccessRequest = new PickupSuccessRequest();
                    pickupSuccessRequest.setUserId(userId);
                    pickupSuccessRequest.setUniqueId(mCriticalogSharedPreferences.getData("unique_id"));
                    pickupSuccessRequest.setAction("pickupsuccess");
                    pickupSuccessRequest.setActualValue(mActualValue.getText().toString());
                    pickupSuccessRequest.setBoxnumber(mTotalBox.getText().toString());
                    pickupSuccessRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                    pickupSuccessRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
                    pickupSuccessRequest.setClientName(mCriticalogSharedPreferences.getData("client_name"));
                    pickupSuccessRequest.setSourcePin(mCriticalogSharedPreferences.getData("srcPin"));
                    pickupSuccessRequest.setDestPin(mDestPincode.getText().toString());
                    pickupSuccessRequest.setDocketnumber(docket);
                    pickupSuccessRequest.setPrsNumber(mCriticalogSharedPreferences.getData("prs"));
                    pickupSuccessRequest.setImage1(base64CapturedImage1);
                    pickupSuccessRequest.setImage2(base64CapturedImage2);
                    pickupSuccessRequest.setLat(String.valueOf(latitude));
                    pickupSuccessRequest.setLng(String.valueOf(longitude));
                    pickupSuccessRequest.setIschecked(nfoFlag);
                    pickupSuccessRequest.setPreDocket(mCriticalogSharedPreferences.getData("connote_number"));
                    pickupSuccessRequest.setPrsId(mCriticalogSharedPreferences.getData("prs_id"));
                    pickupSuccessRequest.setDestinationLoc(mCriticalogSharedPreferences.getData("destination_loc"));
                    pickupSuccessRequest.setDestMail(mCriticalogSharedPreferences.getData("dest_mail"));
                    pickupSuccessRequest.setDestHubId(mCriticalogSharedPreferences.getData("dest_hub_id"));
                    pickupSuccessRequest.setHubId(mCriticalogSharedPreferences.getData("hub_id"));
                    pickupSuccessRequest.setEwaybillScan(ewayBillListToSend);
                    pickupSuccessRequest.setOpenImageList(openImageList);
                    pickupSuccessRequest.setGatepass_image(base64CapturedGatePass);
                    pickupSuccessRequest.setRemark(remarks);

                    mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                            new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
                    mPresenter.pickupRequest(token, pickupSuccessRequest);
                }
            }
        } else {

           /* if(gatePassFlag.equalsIgnoreCase("1"))
            {
                if(base64CapturedGatePass.equalsIgnoreCase(""))
                {
                    Toasty.warning(BarcodeScanActivity.this, "Capture Gate Pass Image", Toast.LENGTH_LONG, true).show();
                    return;
                }
            }*/
            PickupSuccessRequest pickupSuccessRequest = new PickupSuccessRequest();
            pickupSuccessRequest.setUserId(userId);
            pickupSuccessRequest.setUniqueId(mCriticalogSharedPreferences.getData("unique_id"));
            pickupSuccessRequest.setAction("pickupsuccess");
            pickupSuccessRequest.setActualValue(mActualValue.getText().toString());
            pickupSuccessRequest.setBoxnumber(mTotalBox.getText().toString());
            pickupSuccessRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
            pickupSuccessRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
            pickupSuccessRequest.setClientName(mCriticalogSharedPreferences.getData("client_name"));
            pickupSuccessRequest.setSourcePin(mCriticalogSharedPreferences.getData("srcPin"));
            pickupSuccessRequest.setDestPin(mDestPincode.getText().toString());
            pickupSuccessRequest.setDocketnumber(docket);
            pickupSuccessRequest.setPrsNumber(mCriticalogSharedPreferences.getData("prs"));
            pickupSuccessRequest.setImage1(base64CapturedImage1);
            pickupSuccessRequest.setImage2(base64CapturedImage2);
            pickupSuccessRequest.setLat(String.valueOf(latitude));
            pickupSuccessRequest.setLng(String.valueOf(longitude));
            pickupSuccessRequest.setIschecked(nfoFlag);
            pickupSuccessRequest.setPreDocket(mCriticalogSharedPreferences.getData("connote_number"));
            pickupSuccessRequest.setPrsId(mCriticalogSharedPreferences.getData("prs_id"));
            pickupSuccessRequest.setDestinationLoc(mCriticalogSharedPreferences.getData("destination_loc"));
            pickupSuccessRequest.setDestMail(mCriticalogSharedPreferences.getData("dest_mail"));
            pickupSuccessRequest.setDestHubId(mCriticalogSharedPreferences.getData("dest_hub_id"));
            pickupSuccessRequest.setHubId(mCriticalogSharedPreferences.getData("hub_id"));
            pickupSuccessRequest.setEwaybillScan(ewayBillListToSend);
            pickupSuccessRequest.setOpenImageList(openImageList);
            pickupSuccessRequest.setGatepass_image(base64CapturedGatePass);
            pickupSuccessRequest.setRemark(remarks);

            mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                    new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
            mPresenter.pickupRequest(token, pickupSuccessRequest);
        }

    }


    public void pincodeValidation(String pincode) {
        PincodeValidateRequest pincodeValidateRequest = new PincodeValidateRequest();
        pincodeValidateRequest.setPincode(mDestPincode.getText().toString());
        pincodeValidateRequest.setAction("pincode_validation");
        pincodeValidateRequest.setUserId(userId);
        pincodeValidateRequest.setLatitude(String.valueOf(latitude));
        pincodeValidateRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this,
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(),
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
        mPresenter.picodeValidationRequest(token, pincodeValidateRequest);
    }

    public void callCheckListApi() {

        RulesRequest rulesRequest = new RulesRequest();
        rulesRequest.setUserId(userId);
        rulesRequest.setAction("client_fll_rules");
        rulesRequest.setClientId(mCriticalogSharedPreferences.getData("client_code"));
        rulesRequest.setLatitude(String.valueOf(latitude));
        rulesRequest.setLongitude(String.valueOf(longitude));
        rulesRequest.setPrsDrs("PRS");
        rulesRequest.setConnote_no(docket);
        Call<RulesResponse> rulesResponseCall = mRestServices.getClientRules(rulesRequest);

        rulesResponseCall.enqueue(new Callback<RulesResponse>() {
            @Override
            public void onResponse(Call<RulesResponse> call, Response<RulesResponse> response) {
                if (response.body().getStatus() == 200) {

                    if (response.body().getData().getOpenImages().size() != 0) {
                        openImageList = response.body().getData().getOpenImages();
                        mRvOpenPickup.setVisibility(View.VISIBLE);
                        mOpenPickupImageLayout.setVisibility(View.GONE);

                        mOpenPickupAdpater = new OpenPickupAdpater(BarcodeScanActivity.this, BarcodeScanActivity.this, openImageList);
                        mRvOpenPickup.setLayoutManager(new GridLayoutManager(BarcodeScanActivity.this, 3));
                        mRvOpenPickup.setAdapter(mOpenPickupAdpater);

                    } else {
                        mRvOpenPickup.setVisibility(View.GONE);
                        mOpenPickupImageLayout.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getData().getPrsRule().size() != 0 || response.body().getData().getPrsRule() != null) {
                        prsRules = response.body().getData().getPrsRulesQc();
                        if (forcePrsCheck.equals("0") || forcePrsCheck.equals("")) {
                            prsRulesList = new ArrayList<>();
                            prsRulesListPreview = new ArrayList<>();
                            if (prsRules.size() > 0) {
                                dialogRules1(prsRules, 0);
                            }
                        } else if (forcePrsCheck.equals("1")) {
                            // prsRulesList = new ArrayList(Arrays.asList(new Integer[prsRules.size()]));

                            for (int i = 0; i < prsRules.size(); i++) {
                                ChecklistResponse checklistResponse = new ChecklistResponse();
                                checklistResponse.setRule(prsRules.get(i).getRule());
                                checklistResponse.setFlag("");
                                checklistResponse.setImage(prsRules.get(i).getImage());

                                ChecklistScan checklistResponse1 = new ChecklistScan();
                                checklistResponse1.setRule(prsRules.get(i).getRule());
                                checklistResponse1.setFlag("");
                                // checklistResponse1.setImage(prsRules.get(i).getImage());

                                prsRulesList.add(i, checklistResponse);
                                prsRulesListPreview.add(i, checklistResponse1);
                            }
                            mMainBarcodeScanLayout.setVisibility(View.GONE);
                            //    dialogRules1(prsRules, 1);
                            if (response.body().getData().getPrsResponseFlag().equalsIgnoreCase("0")) {
                                dialogRules2(prsRulesList, 1);
                            } else {
                                // mChecklistWithFalgLinLayout.setVisibility(View.GONE);
                                mMainBarcodeScanLayout.setVisibility(View.VISIBLE);
                            }
                            //dialogRules2(prsRulesList, 1);
                            // Toast.makeText(BarcodeScanActivity.this, "Force checklist enabled", Toast.LENGTH_LONG).show();
                        }
                    }

                } else {
                    getQCItemList();
                    Toast.makeText(BarcodeScanActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RulesResponse> call, Throwable t) {
                Toast.makeText(BarcodeScanActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void dialogRules(List<DatumItemQc> qcItemList, int flag) {
        ImageView mCloseItemDialog;
        RecyclerView mRVItemList;
        ItemsAdapter itemsAdapter;
        TextView mQCItemStatusCheckSubmit;
        ArrayList<String> stringArrayList = new ArrayList<>();
        dialogQcItems = new Dialog(BarcodeScanActivity.this);
        dialogQcItems.setContentView(R.layout.dialog_rule);

        Window window = dialogQcItems.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mOkRule = dialogQcItems.findViewById(R.id.mYes);
        mRVItemList = dialogQcItems.findViewById(R.id.mRVItemList);
        mQCItemStatusCheckSubmit = dialogQcItems.findViewById(R.id.mQCItemStatusCheckSubmit);
        mCloseItemDialog = dialogQcItems.findViewById(R.id.mCloseItemDialog);
        dialogQcItems.setCancelable(false);
        dialogQcItems.show();

        mCloseItemDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogQcItems.dismiss();
                finish();
            }
        });

        if (qcItemList.size() > 0) {
            itemsAdapter = new ItemsAdapter(BarcodeScanActivity.this, qcItemList, this);
            mRVItemList.setLayoutManager(new LinearLayoutManager(this));
            mRVItemList.setAdapter(itemsAdapter);
            itemsAdapter.notifyDataSetChanged();
        }

        mQCItemStatusCheckSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stringArrayList.clear();

                for (int i = 0; i < qcItemList.size(); i++) {

                    stringArrayList.add(qcItemList.get(i).getQc_status());

                }

                if (stringArrayList.contains("Pending")) {
                    Toasty.warning(BarcodeScanActivity.this, "Complete all QC's!", Toast.LENGTH_SHORT, true).show();
                    //   Toast.makeText(BarcodeScanActivity.this, "Complete all QCs!!", Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.success(BarcodeScanActivity.this, "QC Updated Successfully!!", Toast.LENGTH_SHORT, true).show();
                    dialogQcItems.dismiss();
                }
            }
        });
    }

    public void dialogRules2(List<ChecklistResponse> prsRules, int flag) {
        TextView mRuleText, mOkRemark;

        ImageView mCloseRulesDialog;
        RulesAdapter rulesAdapter;
        EditText mRemarkFLLRule;

        dialogRules = new Dialog(BarcodeScanActivity.this);
        dialogRules.setContentView(R.layout.dialog_rule_normal);
        Window window = dialogRules.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mOkRule = dialogRules.findViewById(R.id.mOkRule);
        mRvRules = dialogRules.findViewById(R.id.mRvRules);
        mRuleText = dialogRules.findViewById(R.id.mRuleText);
        mRemarkFLLRule = dialogRules.findViewById(R.id.mRemarkFLLRule);
        mOkRemark = dialogRules.findViewById(R.id.mOkRemark);
        mCloseRulesDialog = dialogRules.findViewById(R.id.mCloseRulesDialog);
        mSelectYesNoAlert = dialogRules.findViewById(R.id.mSelectYesNoAlert);


        if (flag == 1) {
            mOkRule.setVisibility(View.GONE);
            rulesHorizontalAdapter = new RulesHorizontalAdapter(this, prsRules, BarcodeScanActivity.this);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            mRvRules.setLayoutManager(layoutManager);
            mRvRules.setAdapter(rulesHorizontalAdapter);
        }
        mCloseRulesDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRules.dismiss();
                startActivity(new Intent(BarcodeScanActivity.this, PickupActivity.class));
                finish();
            }
        });
        mOkRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    dialogRules.dismiss();
                    getQCItemList();
                } else {
                    mRuleText.setVisibility(View.VISIBLE);
                    mRemarkFLLRule.setVisibility(View.VISIBLE);
                    mOkRemark.setVisibility(View.VISIBLE);
                    mRvRules.setVisibility(View.GONE);
                    mOkRule.setVisibility(View.GONE);
                }
            }
        });
        mOkRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remarks = mRemarkFLLRule.getText().toString();
                dialogRules.dismiss();
                mMainBarcodeScanLayout.setVisibility(View.VISIBLE);
                dialogCheckListWithFlag(prsRulesListPreview, forceChecListOTPEnabled);

            }
        });
        dialogRules.setCancelable(false);
        dialogRules.show();
    }

    public void dialogCheckListWithFlag(List<ChecklistScan> prsRules, String generateOtp) {
        TextView mDone, xTvTaskId, mFailed;
        RecyclerView mRvRules;
        RulesAdapterWithFlag rulesAdapter;

        ImageView mOtpDialogBack;
        Button mVerifyOTPChecklist;
        OtpTextView mOtpTextView;

        CheckBox mCheckYes, mCheckNo;
        dialogCheckListWithFlag = new Dialog(BarcodeScanActivity.this);
        dialogCheckListWithFlag.setContentView(R.layout.dialog_rule_with_flag);
        Window window = dialogCheckListWithFlag.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mDone = dialogCheckListWithFlag.findViewById(R.id.mDone);
        mFailed = dialogCheckListWithFlag.findViewById(R.id.mFailed);
        mRvRules = dialogCheckListWithFlag.findViewById(R.id.mRvRules);
        xTvTaskId = dialogCheckListWithFlag.findViewById(R.id.xTvTaskId);
        mChecklistWithFalgLinLayout = dialogCheckListWithFlag.findViewById(R.id.mChecklistWithFalgLinLayout);
        mOtpLinLayout = dialogCheckListWithFlag.findViewById(R.id.mOtpLinLayout);
        mOtpDialogBack = dialogCheckListWithFlag.findViewById(R.id.mOtpDialogBack);
        mVerifyOTPChecklist = dialogCheckListWithFlag.findViewById(R.id.mVerifyOTPChecklist);
        mOtpTextView = dialogCheckListWithFlag.findViewById(R.id.mOtpTextView);

        xTvTaskId.setText("Checklist");
        //generateOtp="1";
        if (generateOtp.equalsIgnoreCase("1")) {
            mDone.setText("Generate OTP");
        } else {
            mDone.setText("DONE");
        }

        mFailed.setText("Modify");

        dialogCheckListWithFlag.setCancelable(false);
        rulesAdapter = new RulesAdapterWithFlag(prsRules);
        mRvRules.setLayoutManager(new LinearLayoutManager(this));
        mRvRules.setAdapter(rulesAdapter);

        mVerifyOTPChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = mOtpTextView.getOTP().toString();

                if (otp.equalsIgnoreCase("")) {
                    Toasty.error(BarcodeScanActivity.this, "Enter OTP", Toast.LENGTH_SHORT, true).show();
                } else if (otp.length() < 4) {
                    Toasty.error(BarcodeScanActivity.this, "Enter Valid OTP", Toast.LENGTH_SHORT, true).show();
                } else {
                    verifyOrSendOtp = "verifyotp";

                    OTPRequest otpVliadationChecklistRequest = new OTPRequest();
                    otpVliadationChecklistRequest.setAction("validate_otp");
                    otpVliadationChecklistRequest.setDocket_no(docket);
                    otpVliadationChecklistRequest.setOtpMobile(mobielNumber);
                    otpVliadationChecklistRequest.setUserId(userId);
                    otpVliadationChecklistRequest.setOtp(otp);
                    otpVliadationChecklistRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                    otpVliadationChecklistRequest.setRound(mCriticalogSharedPreferences.getData("ROUND_NO"));
                    otpVliadationChecklistRequest.setFor_type("qc");
                    otpVliadationChecklistRequest.setIsResend("1");
                    otpVliadationChecklistRequest.setLatitude(String.valueOf(latitude));
                    otpVliadationChecklistRequest.setLongitude(String.valueOf(longitude));
                    mPresenter.otpValidationForChecklist(otpVliadationChecklistRequest);
                }
            }
        });
        mOtpDialogBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChecklistWithFalgLinLayout.setVisibility(View.VISIBLE);
                mOtpLinLayout.setVisibility(View.GONE);
            }
        });

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String checkString = mDone.getText().toString();
                if (checkString.equalsIgnoreCase("DONE")) {

                    ValidateCheclistYesNoRequest validateCheclistYesNoRequest = new ValidateCheclistYesNoRequest();
                    validateCheclistYesNoRequest.setAction("validate_response_checklist");
                    validateCheclistYesNoRequest.setChecklistResponse(prsRulesList);
                    validateCheclistYesNoRequest.setUserId(userId);
                    validateCheclistYesNoRequest.setCheckType("prs");
                    validateCheclistYesNoRequest.setConnote_no(docket);
                    validateCheclistYesNoRequest.setRemark(remarks);
                    validateCheclistYesNoRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                    mPresenter.validateChecklistYesNo(validateCheclistYesNoRequest);
                } else {
                    verifyOrSendOtp = "getotp";

                    ValidateCheclistYesNoRequest validateCheclistYesNoRequest = new ValidateCheclistYesNoRequest();
                    validateCheclistYesNoRequest.setAction("validate_response_checklist");
                    validateCheclistYesNoRequest.setChecklistResponse(prsRulesList);
                    validateCheclistYesNoRequest.setUserId(userId);
                    validateCheclistYesNoRequest.setCheckType("prs");
                    validateCheclistYesNoRequest.setConnote_no(docket);
                    validateCheclistYesNoRequest.setRemark(remarks);
                    validateCheclistYesNoRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                    mPresenter.validateChecklistYesNo(validateCheclistYesNoRequest);
                }

            }
        });

        mFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCheckListWithFlag.dismiss();

                dialogRules2(prsRulesList, 1);
                // startActivity(new Intent(BarcodeScanActivity.this,PickupActivity.class));
            }
        });
        dialogCheckListWithFlag.show();

    }

    public void dialogRules1(List<PrsRulesQc> prsRules, int flag) {
        TextView mRuleText, mOkRemark;

        ImageView mCloseRulesDialog;
        RulesAdapter rulesAdapter;
        EditText mRemarkFLLRule;

        dialogRules = new Dialog(BarcodeScanActivity.this);
        dialogRules.setContentView(R.layout.dialog_rule_normal);
        Window window = dialogRules.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mOkRule = dialogRules.findViewById(R.id.mOkRule);
        mRvRules = dialogRules.findViewById(R.id.mRvRules);
        mRuleText = dialogRules.findViewById(R.id.mRuleText);
        mRemarkFLLRule = dialogRules.findViewById(R.id.mRemarkFLLRule);
        mOkRemark = dialogRules.findViewById(R.id.mOkRemark);
        mCloseRulesDialog = dialogRules.findViewById(R.id.mCloseRulesDialog);
        mCloseRulesDialog.setVisibility(View.INVISIBLE);

        if (flag == 1) {
         /*   mOkRule.setVisibility(View.GONE);
            rulesHorizontalAdapter = new RulesHorizontalAdapter(prsRules, BarcodeScanActivity.this);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            mRvRules.setLayoutManager(layoutManager);
            mRvRules.setAdapter(rulesHorizontalAdapter);*/
        } else {
            rulesAdapter = new RulesAdapter(prsRules);
            mRvRules.setLayoutManager(new LinearLayoutManager(this));
            mRvRules.setAdapter(rulesAdapter);
        }

        mOkRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    dialogRules.dismiss();
                    getQCItemList();
                } else {
                    mRuleText.setVisibility(View.VISIBLE);
                    mRemarkFLLRule.setVisibility(View.VISIBLE);
                    mOkRemark.setVisibility(View.VISIBLE);
                    mRvRules.setVisibility(View.GONE);
                    mOkRule.setVisibility(View.GONE);
                }
                //dialogRules.dismiss();
            }
        });
        mOkRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remarks = mRemarkFLLRule.getText().toString();
                dialogRules.dismiss();
                mMainBarcodeScanLayout.setVisibility(View.VISIBLE);
                getQCItemList();
            }
        });
        dialogRules.setCancelable(false);
        dialogRules.show();
    }

    public void dialogQC(QCRulesResponse qcRulesResponse) {

        Button mUpdate;
        dialogRules = new Dialog(BarcodeScanActivity.this);
        dialogRules.setContentView(R.layout.qc_activity);
        Window window = dialogRules.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mQCRV = dialogRules.findViewById(R.id.mQCRV);
        mProductCode = dialogRules.findViewById(R.id.mProductCode);
        mProductDescription = dialogRules.findViewById(R.id.mProductDescription);
        mSplitPcs = dialogRules.findViewById(R.id.mSplitPcs);
        mLength = dialogRules.findViewById(R.id.mLength);
        mB = dialogRules.findViewById(R.id.mB);
        mH = dialogRules.findViewById(R.id.mH);
        mUpdate = dialogRules.findViewById(R.id.mUpdate);

        dialogRules.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mQcAdapter = new QCAdapter(this, BarcodeScanActivity.this, qcRulesResponse.getData().getQcRules());
        mQCRV.setLayoutManager(new LinearLayoutManager(this));

        mQCRV.setAdapter(mQcAdapter);
        mQcAdapter.notifyDataSetChanged();


        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // QCNdcUpdate();
                getQCCheckSubmit();

            }
        });

        dialogRules.show();
    }

    private boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }

    }

    private static double distance1(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (unit.equals("K")) {
                dist = dist * 1.609344;
            } else if (unit.equals("N")) {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }

    public static float distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    private double distanceCalc(double lat1, double lon1, double lat2, double lon2) {
        double distance;
        double distance1;
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat1);
        startPoint.setLongitude(lon1);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat2);
        endPoint.setLongitude(lon2);

        distance = startPoint.distanceTo(endPoint);

        distance1 = distance / 1000;

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        System.out.println(numberFormat.format(distance1));

        return Double.parseDouble(numberFormat.format(distance1));
    }

    public double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist / 0.62137;

        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(BarcodeScanActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(BarcodeScanActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BarcodeScanActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(BarcodeScanActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    private void startBarcodeScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan Number");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(TorchOnCaptureActivity.class);
        integrator.setCameraId(0);
        integrator.setTimeout(20000);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1888) {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                //  Bitmap photo = (Bitmap) data.getExtras().get("data");

                File absoluteFile = photoFile.getAbsoluteFile();

                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (mCriticalogSharedPreferences.getData("image").equalsIgnoreCase("first")) {
                        base64CapturedImage1 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        mPickupImage1.setImageBitmap(myBitmap);
                        Toast.makeText(BarcodeScanActivity.this, "First Image", Toast.LENGTH_SHORT).show();
                    } else if (mCriticalogSharedPreferences.getData("image").equalsIgnoreCase("second")) {
                        mPickupImage2.setImageBitmap(myBitmap);
                        base64CapturedImage2 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        Toast.makeText(BarcodeScanActivity.this, "Second Image", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else if (requestCode == 1889) {
            File absoluteFile = photoFile.getAbsoluteFile();
            if (resultCode == 0) {
                Toast.makeText(this, "You cannot Press Back!!", Toast.LENGTH_SHORT).show();
            } else {
                if (absoluteFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    base64CapturedQCImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    getQCUpdate(qcIdToSned, "3", "", base64CapturedQCImage, "");

                }
            }


        } else if (requestCode == 1890) {
            File absoluteFile = photoFile.getAbsoluteFile();
            if (resultCode == 0) {
                Toast.makeText(this, "You cannot Press Back!!", Toast.LENGTH_SHORT).show();
            } else {
                if (absoluteFile.exists()) {

                    try {
                        Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64CapturedMultImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                        OpenImage openImage = new OpenImage();
                        openImage.setImage(base64CapturedMultImage);
                        openImage.setLabel(openPickupText);

                        openImageList.set(positionClicked, openImage);

                        mOpenPickupAdpater.notifyDataSetChanged();
                    } catch (Exception e) {

                    }
                }
            }
        } else if (requestCode == 1891) {
            File absoluteFile = photoFile.getAbsoluteFile();
            if (resultCode == 0) {
                Toast.makeText(this, "You cannot Press Back!!", Toast.LENGTH_SHORT).show();
            } else {
                if (absoluteFile.exists()) {

                    try {
                        Bitmap myBitmap = BitmapFactory.decodeFile(absoluteFile.getAbsolutePath());

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64CapturedGatePass = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                        mGatePassImage.setImageBitmap(myBitmap);

                    } catch (Exception e) {

                    }
                }
            }
        } else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                try {
                    if (scanWhat.equalsIgnoreCase("ewayBill")) {
                        String MobilePattern = "[0-9]{9}";
                        String ewayNumber = result.getContents().toString();
                        Log.d("docket", "BarcodeScan" + docket);
                        if (ewayNumber.equals("")) {
                            scanWhat = "ewayBill";
                            startBarcodeScan();
                        } else if (ewayNumber.length() == 12) {
                            callEwayBillVlaidate(ewayNumber);
                        } else {
                            Toast.makeText(this, "Scan 12 digit eway number", Toast.LENGTH_SHORT).show();
                            //startBarcodeScan();
                        }
                    } else {
                        String MobilePattern = "[0-9]{9}";
                        docket = result.getContents().toString();
                        Log.d("docket", "BarcodeScan" + docket);
                        mCriticalogSharedPreferences.saveData("docketNumber", docket);
                        if (docket.equals("")) {
                            startBarcodeScan();
                        } else if (docket.length() == 9) {
                            docketValidationCheck(docket);
                        } else {
                            Toast.makeText(this, "Scan 9 digit number", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                    //startBarcodeScan();
                }
            }
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public void docketValidationCheck(String docket) {

        DocketValidationRequest docketValidationRequest = new DocketValidationRequest();
        docketValidationRequest.setAction("docketstatus");
        docketValidationRequest.setDocket(docket);
        docketValidationRequest.setPreDocket(mCriticalogSharedPreferences.getData("connote_number"));
        docketValidationRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
        docketValidationRequest.setClientcode(mCriticalogSharedPreferences.getData("client_code"));
        docketValidationRequest.setPrs(mCriticalogSharedPreferences.getData("prs"));
        docketValidationRequest.setSourcePin(mCriticalogSharedPreferences.getData("srcPin"));
        docketValidationRequest.setUserId(userId);
        docketValidationRequest.setLatitude(String.valueOf(latitude));
        docketValidationRequest.setLongitude(String.valueOf(longitude));

        // mProgressDialog1.show();
        mPresenter = new DocketValidationPresenterImpl(this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
        mPresenter.docketValidationRequest(token, docketValidationRequest);
    }

    @Override
    public void showProgress() {
        mProgressDialog1.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog1.dismiss();
    }

    @Override
    public void setDocketCheckDataToViews(DocketCheckResponse docketCheckResponse) {

    }

    @Override
    public void setOriginPincodeResponse(OriginPincodeValidateResponse originPincodeResponse) {

    }

    @Override
    public void setActualValueInscanResponse(ActualValueInscanResponse actualValueInscanResponse) {

    }

    @Override
    public void setInscanBoxDataToViews(InScanBoxResponse inScanBoxResponse) {

    }

    @Override
    public void setInScanSubmitDataToViews(InScanSubmitResponse inScanSubmitResponse) {

    }

    @Override
    public void setEwayBillValidateResponseToViews(EwayBillValidateResponse ewayBillValidateResponse) {
        if (ewayBillValidateResponse.getStatus() == 200) {
            if (ewayBillListToSend.contains(ewayBillValidateResponse.getData().get(0).getScannedEwaybill())) {
                mEwayBill.setText("");
                mEwayBill.setHint(R.string.enter_no_of_eway);
                Toasty.warning(BarcodeScanActivity.this, "Eway Bill already present!..enter new one", Toast.LENGTH_LONG, true).show();
            } else {
                if (ewayBillListToSend.size() == Integer.parseInt(mEwayBillNo.getText().toString())) {

                    mEwayBill.setText("");
                    mEwayBill.setHint(R.string.enter_no_of_eway);
                    Toasty.warning(BarcodeScanActivity.this, "All Eway Bill Numbers Already Scanned!!", Toast.LENGTH_LONG, true).show();

                } else {
                    mEwayBill.setText("");
                    mEwayBill.setHint(R.string.enter_no_of_eway);
                    ewayBillListToSend.add(ewayBillValidateResponse.getData().get(0).getScannedEwaybill());
                    inscanBoxAdapter.notifyDataSetChanged();
                    Toasty.success(BarcodeScanActivity.this, "Eway Bill " + String.valueOf(ewayBillListToSend.size()) + " of " + mEwayBillNo.getText().toString() + " Scanned", Toast.LENGTH_LONG, true).show();

                    mEwayBillNo.setEnabled(false);
                    if (ewayBillListToSend.size() == Integer.parseInt(mEwayBillNo.getText().toString())) {
                        mEwayBill.setEnabled(false);
                    }
                }
            }

        } else {
            Toasty.warning(BarcodeScanActivity.this, ewayBillValidateResponse.getMessage(), Toast.LENGTH_LONG, true).show();

        }
    }

    @Override
    public void inspectionDoneResponseToData(InspectionDoneResponse inspectionDoneResponse) {

    }

    @Override
    public void inspectionNegativeStatusData(InscanNegativeStatusResponse inspectionNegativeStatusResponse) {

    }

    @Override
    public void inspectionNegSubmitDataToViews(InspectionNegSubmitResponse inspectionNegSubmitResponse) {

    }

    @Override
    public void inscanItemDetailsResponseDataToVews(InscanGetItemDetailsResponse inscanGetItemDetailsResponse) {

    }

    @Override
    public void setDataToViews(DocketValidateResponse docketValidateResponse) {

        if (docketValidateResponse.getStatus() == 200) {
            String actualVal = docketValidateResponse.getData().getActualValue();
            mMainBarcodeScanLayout.setVisibility(View.VISIBLE);
            mTotalBox.setText(docketValidateResponse.getData().getTotalbox());

            gatePassImageIRL=docketValidateResponse.getData().getGatepass().toString();

           // gatePassImageIRL="https://source.unsplash.com/user/c_v_r/1900x800";
            if(!gatePassImageIRL.equalsIgnoreCase(""))
            {
                Picasso.with(this)
                        .load(gatePassImageIRL)
                        .resize(400, 400)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .placeholder(R.drawable.brokenimage)
                        .into(mGatePassImage);
            }
            if (!docketValidateResponse.getData().getTotalbox().equalsIgnoreCase("")) {
                //  mTotalBox.setEnabled(false);
                // mTotalBox.setFocusable(false);
            }
            if (docketValidateResponse.getData().getDestHub().equalsIgnoreCase("")) {
                mDestPincode.setText(docketValidateResponse.getData().getDestHub());

            } else {
                mDestPincode.setText(docketValidateResponse.getData().getDestHub());
                mDestPincode.setEnabled(false);
                mDestPincode.setFocusable(false);
            }

            if (!actualVal.equals("")) {
                String decimalString = actualVal;
                String[] decimalSplit = decimalString.split("\\.");
                System.out.println("Decimal Number: " + decimalString);
                System.out.println("Whole Number: " + decimalSplit[0]);

                if (decimalSplit.length > 0) {

                    mActualValue.setText(String.valueOf(decimalSplit[0]));
                    //  mActualValue.setEnabled(false);
                    // mActualValue.setFocusable(false);
                }
            } else {
                mActualValue.setText("");
            }
            mScannedResult.setText(docket);
            Toast.makeText(BarcodeScanActivity.this, docketValidateResponse.getMessage(), Toast.LENGTH_SHORT).show();
            dialogManualEntry.dismiss();
            Log.e("forece_update_rules", forcePrsCheck);
            callCheckListApi();

        } else {
            Toast.makeText(BarcodeScanActivity.this, docketValidateResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    public String converttorealnumber(double distance) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String convertedstring = formatter.format(distance);
        return convertedstring;
    }

    @Override
    public void pickUpSuccessViews(PickupSuccessResponse pickupSuccessResponse) {
        Log.e("pick_response", pickupSuccessResponse.toString());
        // mProgressDialog1.dismiss();
        double start_lat = Double.parseDouble(mCriticalogSharedPreferences.getData("start_lat"));
        double start_long = Double.parseDouble(mCriticalogSharedPreferences.getData("start_long"));
        double distance = 0;
        String convertedString = "";

        Date c1 = Calendar.getInstance().getTime();
        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = df3.format(c1);

        SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
        String time1 = df4.format(c1);
        if (pickupSuccessResponse.getStatus() == 200) {
            Toast.makeText(BarcodeScanActivity.this, pickupSuccessResponse.getMessage(), Toast.LENGTH_SHORT).show();
            try {
                closeDocketClient();
            } catch (WindowManager.BadTokenException e) {
                //use a log message
            }
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ArrayList<PodClaimPojo> latlonglist = dbHelper.getAllLatLongList();
                    Log.d("inside", "pickupsuccessfail" + 1);

                    if (latlonglist.size() > 0) {
                        Log.d("inside", "pickupsuccessfail" + "if");
                        int listsize = latlonglist.size();
                        PodClaimPojo podClaimPojo;

                        double distnce = distanceCalc(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);
                        String convertedstring = converttorealnumber(distnce);

                        //Next Billion Distance calulation below
                        String convertedstring_g = getDistanceFromGoogleAPI(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude);

                        if (convertedstring_g.equalsIgnoreCase("0.0")) {
                            convertedstring_g = convertedstring;
                        }
                        podClaimPojo = new PodClaimPojo(latlonglist.get(listsize - 1).getEdn_lat(), latlonglist.get(listsize - 1).getEnd_long(), latitude, longitude, convertedstring, convertedstring_g, date1, time1, "1", docket, "prs", c1.toString(), 200);

                        dbHelper.insertLatLOngDetails(podClaimPojo);

                    } else {

                        PodClaimPojo podClaimPojo = null;

                        if (start_lat == 0 || start_long == 0 || latitude == 0 || longitude == 0) {
                            String lastLatLongList = dbHelper.getLastLatLongDetails();
                            if (!lastLatLongList.equals("-")) {
                                String[] bits = lastLatLongList.split("-");
                                String lastOne = bits[bits.length - 1];
                                double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                String convertedstring = converttorealnumber(distnce);

                                //Next Billion Distance calulation below
                                String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);

                                if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                    convertedstring_g = convertedstring;
                                }

                                podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, convertedstring, convertedstring_g, date1, time1, "1", docket, "prs", c1.toString(), 200);
                            }

                        } else {

                            String lastLatLongList = dbHelper.getLastLatLongDetails();
                            if (!lastLatLongList.equals("-")) {
                                String[] bits = lastLatLongList.split("-");
                                String lastOne = bits[bits.length - 1];

                                double distnce = distanceCalc(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                String convertedstring = converttorealnumber(distnce);

                                //Next Billion Distance calulation below
                                String convertedstring_g = getDistanceFromGoogleAPI(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude);
                                if (convertedstring_g.equalsIgnoreCase("0.0")) {
                                    convertedstring_g = convertedstring;
                                }
                                podClaimPojo = new PodClaimPojo(Double.valueOf(bits[0]), Double.valueOf(bits[1]), latitude, longitude, convertedstring, convertedstring_g, date1, time1, "1", docket, "prs", c1.toString(), 200);
                            }

                        }

                        latlonglist.add(podClaimPojo);
                        dbHelper.insertLatLOngDetails(podClaimPojo);
                    }
                }
            });
            userRiderLog();
            thread.start();
        } else {
            Toast.makeText(BarcodeScanActivity.this, pickupSuccessResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void pincodeValidateSuccessViews(PincodeValidateResponse pincodeValidateResponse) {
        Log.e("LAT_LONG_SHREE", String.valueOf(latitude) + "  " + String.valueOf(longitude));

        if (pincodeValidateResponse.getStatus() == 200) {
            Toast.makeText(BarcodeScanActivity.this, pincodeValidateResponse.getMessage(), Toast.LENGTH_SHORT).show();

            mCriticalogSharedPreferences.saveData("destination_loc", pincodeValidateResponse.getData().getDestinationLoc());
            mCriticalogSharedPreferences.saveData("dest_mail", pincodeValidateResponse.getData().getDestMail());
            mCriticalogSharedPreferences.saveData("dest_hub_id", pincodeValidateResponse.getData().getDestHubId());

            mActualValue.setActivated(true);
            mActualValue.setPressed(true);
            mSubmit.setVisibility(View.VISIBLE);
            mControllingHub.setVisibility(View.VISIBLE);

            String controllingHubText = "Hub Code: " + pincodeValidateResponse.getData().getControlling_hub().toString() + " - " + "Dest City: " + pincodeValidateResponse.getData().getCity_name().toString();
            mControllingHub.setText(Html.fromHtml(controllingHubText));

        } else {
            mSubmit.setVisibility(View.GONE);
            mControllingHub.setVisibility(View.GONE);
            mCity.setVisibility(View.GONE);
            mDestPincode.setText("");
            mDestPincode.setHint("Enter pincode");
            Toast.makeText(BarcodeScanActivity.this, pincodeValidateResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void closeDocketSuccessViews(CloseDocketResponse closeDocketResponse) {
        //  mProgressDialog1.dismiss();
        if (closeDocketResponse.getStatus() == 200) {
            if (mCriticalogSharedPreferences.getData("close_client").equalsIgnoreCase("true")) {
                closeClient();
            } else {
                mCriticalogSharedPreferences.saveData("navigationStatus", "manual");
                Intent intent = new Intent(this, BarcodeScanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        } else {
            Toast.makeText(BarcodeScanActivity.this, closeDocketResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void closeClientSuccessViews(CloseClientResponse closeClientResponse) {
        if (closeClientResponse.getStatus() == 200) {
            if (DialogFinish != null) {
                DialogFinish.dismiss();
            }
            if (closeDocketClientDialog != null) {
                closeDocketClientDialog.dismiss();
            }
            Intent intent = new Intent(this, PickupActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            Toast.makeText(BarcodeScanActivity.this, closeClientResponse.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(BarcodeScanActivity.this, closeClientResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void qcItems(ItemListResponse itemListResponse) {
        String comingFrom = mCriticalogSharedPreferences.getData("coming_from_qc");
        if (itemListResponse.getStatus() == 200) {
            if (comingFrom.equalsIgnoreCase("true")) {
                for (int i = 0; i < itemListResponse.getData().size(); i++) {
                    if (itemListResponse.getData().get(i).getQc_status().equalsIgnoreCase("Pending")) {
                        dialogRules(itemListResponse.getData(), 1);
                        break;
                    } else {

                    }
                }
            } else {
                dialogRules(itemListResponse.getData(), 1);
            }
        } else {
            Toasty.warning(BarcodeScanActivity.this, itemListResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    @Override
    public void qcRules(QCRulesResponse qcRulesResponse) {
        if (qcRulesResponse.getStatus() == 200) {

            dialogQC(qcRulesResponse);
        } else {
            Toasty.warning(BarcodeScanActivity.this, qcRulesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    public void qcNDCDialog() {
        TextView mConfirm, mUpdate;
        //Dialog Manual Entry
        qcNDCUpdateDialog = new Dialog(BarcodeScanActivity.this);
        qcNDCUpdateDialog.setContentView(R.layout.qc_ndc_dialog);
        mConfirm = qcNDCUpdateDialog.findViewById(R.id.mConfirm);
        mUpdate = qcNDCUpdateDialog.findViewById(R.id.mUpdate);

        qcNDCUpdateDialog.setCancelable(false);
        qcNDCUpdateDialog.show();

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qcNDCUpdateDialog.dismiss();
            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QCNdcUpdate("0");
            }
        });
    }

    @Override
    public void qcCheckResponse(QcRulesCheckResponse qcRulesCheckResponse) {
        if (qcRulesCheckResponse.getStatus() == 200) {

            mCriticalogSharedPreferences.saveData("coming_from_qc", "true");
            Toasty.success(BarcodeScanActivity.this, qcRulesCheckResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            if (qcRulesCheckResponse.getData().getQualityCheck().equalsIgnoreCase("failed")) {
                qcNDCDialog();
            } else {
                QCNdcUpdate("1");
                dialogRules.dismiss();
                if (dialogQcItems.isShowing()) {
                    dialogQcItems.dismiss();
                }
                getQCItemList();
            }

        } else {
            Toasty.warning(BarcodeScanActivity.this, qcRulesCheckResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void qcUpdateResponse(QcRulesUpdateResponse qcRulesUpdateResponse) {
        if (qcRulesUpdateResponse.getStatus() == 200) {

            Toasty.warning(BarcodeScanActivity.this, qcRulesUpdateResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        } else {
            Toasty.warning(BarcodeScanActivity.this, qcRulesUpdateResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void qcNDCUpdateResponse(QCPassFailResponse qcPassFailResponse) {

        if (qcPassFailResponse.getStatus() == 200) {
            dialogRules.dismiss();
            // mCriticalogSharedPreferences.saveData("coming_from_qc", "true");
            if (qcNDCUpdateDialog != null) {
                if (qcNDCUpdateDialog.isShowing()) {
                    qcNDCUpdateDialog.dismiss();
                    dialogRules.dismiss();
                    dialogQcItems.dismiss();

                    getQCItemList();
                }
            }
            if (qcPassFailResponse.getMessage().equalsIgnoreCase("NDC updated successfully")) {
                Intent newIntent = new Intent(BarcodeScanActivity.this, PickupActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
                //prsCancelCall();
            }

            Toasty.success(BarcodeScanActivity.this, qcPassFailResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        } else {
            Toasty.warning(BarcodeScanActivity.this, qcPassFailResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void getDocketListWithOriginDestResponse(DocketListWithOriginDestResponse docketListWithOriginDestResponse) {

        if (docketListWithOriginDestResponse.getStatus() == 200) {

            for (int i = 0; i < docketListWithOriginDestResponse.getData().size(); i++) {
                createConnoteList.add(new SampleSearchModel(docketListWithOriginDestResponse.getData().get(i).getDocketNum() + "  " + docketListWithOriginDestResponse.getData().get(i).getOrigin() + " ➔ " + docketListWithOriginDestResponse.getData().get(i).getDestination()));
                stringArrayListConnote.add(docketListWithOriginDestResponse.getData().get(i).getDocketNum());
            }
            suggestindList = docketListWithOriginDestResponse.getData();
            adapter = new CustomAutoSuggestAdapter(BarcodeScanActivity.this,
                    docketListWithOriginDestResponse.getData());

            autotext.setAdapter(adapter);

        } else {
            Toasty.warning(BarcodeScanActivity.this, docketListWithOriginDestResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }

    }

    @Override
    public void actualValueResponse(ActualValueCheckResponse actualValueCheckResponse) {

        if (actualValueCheckResponse.getMessage().equalsIgnoreCase("Eway bill required")) {
            actualValueRequired = actualValueCheckResponse.getMessage().toString();
            mEwayBillText.setVisibility(View.VISIBLE);
            mEwayBillNo.setVisibility(View.VISIBLE);
            mEwayBillNo.setEnabled(true);
            mEwayBillNo.setText("");
            mEwayBillNo.setHint(R.string.enter_no_of_eway_bill);
            ewayBillListToSend.clear();
            inscanBoxAdapter.notifyDataSetChanged();
        } else if (actualValueCheckResponse.getMessage().equalsIgnoreCase("Eway bill not required")) {
            actualValueRequired = actualValueCheckResponse.getMessage().toString();
            mEwayBillNo.setText("");
            mEwayBillNo.setHint(R.string.enter_no_of_eway_bill);

        } else if (actualValueCheckResponse.getMessage().equalsIgnoreCase("Actual value cannot exceed")) {
            actualValueRequired = actualValueCheckResponse.getMessage().toString();
            mActualValue.setText("");
            mActualValue.setHint("Enter Actual Value");
        }
        Toast.makeText(this, actualValueCheckResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void otpValidationChecklistResponse(OTPResponse otpVliadationChecklistResponse) {
        if (otpVliadationChecklistResponse.getStatus() == 200) {

            if (verifyOrSendOtp.equalsIgnoreCase("verifyotp")) {
                dialogCheckListWithFlag.dismiss();
                getQCItemList();
            } else if (verifyOrSendOtp.equalsIgnoreCase("getOtp")) {
                mChecklistWithFalgLinLayout.setVisibility(View.GONE);
                mOtpLinLayout.setVisibility(View.VISIBLE);
            }

        } else {
            Toasty.error(this, otpVliadationChecklistResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void validateChecklistYesNoResponse(ValidateChecklistYesNoResponse validateChecklistYesNoResponse) {
        if (validateChecklistYesNoResponse.getStatus() == 200) {

            if (verifyOrSendOtp.equalsIgnoreCase("getotp")) {
                OTPRequest otpVliadationChecklistRequest = new OTPRequest();
                otpVliadationChecklistRequest.setAction("validate_otp");
                otpVliadationChecklistRequest.setDocket_no(docket);
                otpVliadationChecklistRequest.setOtpMobile(mobielNumber);
                otpVliadationChecklistRequest.setUserId(userId);
                otpVliadationChecklistRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
                otpVliadationChecklistRequest.setRound(mCriticalogSharedPreferences.getData("ROUND_NO"));
                otpVliadationChecklistRequest.setFor_type("qc");
                otpVliadationChecklistRequest.setIsResend("2");
                otpVliadationChecklistRequest.setLatitude(String.valueOf(latitude));
                otpVliadationChecklistRequest.setLongitude(String.valueOf(longitude));
                mPresenter.otpValidationForChecklist(otpVliadationChecklistRequest);
            } else {
                mChecklistWithFalgLinLayout.setVisibility(View.GONE);
                dialogCheckListWithFlag.dismiss();
            }
            Toasty.success(BarcodeScanActivity.this, validateChecklistYesNoResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        } else {
            Toasty.error(BarcodeScanActivity.this, validateChecklistYesNoResponse.getMessage(), Toast.LENGTH_LONG, true).show();
        }
    }

    public void showOriginDestination(ArrayList<SampleSearchModel> searchData, String fromWhere) {

        new SimpleSearchDialogCompat(BarcodeScanActivity.this, fromWhere,
                fromWhere, null, searchData,
                new SearchResultListener<SampleSearchModel>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SampleSearchModel item, int position) {
                        // If filtering is enabled, [position] is the index of the item in the filtered result, not in the unfiltered source

                        String splittt = item.getTitle();
                        String[] parts = splittt.split(" ");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1];
                        mDocketEntry.setText(part1);
                        dialog.dismiss();
                    }
                }).show();
    }


    public void prsCancelCall() {
        PickupCancelRequest pickupCancelRequest = new PickupCancelRequest();
        pickupCancelRequest.setAction("pickupcancel");
        pickupCancelRequest.setClientcode(mCriticalogSharedPreferences.getData("client_code"));
        pickupCancelRequest.setPicktype(mCriticalogSharedPreferences.getData("picktype"));
        pickupCancelRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
        pickupCancelRequest.setLatitude(String.valueOf(latitude));
        pickupCancelRequest.setLongitude(String.valueOf(latitude));
        pickupCancelRequest.setPrs(mCriticalogSharedPreferences.getData("prs"));
        pickupCancelRequest.setPrsId(mCriticalogSharedPreferences.getData("prs_id"));
        pickupCancelRequest.setReasoncode("QCInspectionFailed");
        pickupCancelRequest.setUniqueId(mCriticalogSharedPreferences.getData("client_uniqueid"));
        pickupCancelRequest.setUserId(mCriticalogSharedPreferences.getData("userId"));
        pickupCancelRequest.setScheduleDate(mCriticalogSharedPreferences.getData("R_DATE_PICKUP"));
        pickupCancelRequest.setScheduleTime(mCriticalogSharedPreferences.getData("R_TIME_PICKUP"));
        mProgressDialog1 = new SimpleArcDialog(this);
        mProgressDialog1.setConfiguration(new ArcConfiguration(this));
        mProgressDialog1.setCancelable(false);
        mProgressDialog1.show();
        Call<PickupCancelResponse> pickupCancelResponseCall = mRestServices.pickupCancel(pickupCancelRequest);

        pickupCancelResponseCall.enqueue(new Callback<PickupCancelResponse>() {
            @Override
            public void onResponse(Call<PickupCancelResponse> call, Response<PickupCancelResponse> response) {
                mProgressDialog1.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //   Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Toasty.success(BarcodeScanActivity.this, response.body().getMessage(), Toast.LENGTH_LONG, true).show();
                        finish();
                        //  startActivity(new Intent(BarcodeScanActivity.this,PickupActivity.class));
                        Intent newIntent = new Intent(BarcodeScanActivity.this, PickupActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);

                    } else {

                        Toasty.warning(BarcodeScanActivity.this, response.body().getMessage(), Toast.LENGTH_LONG, true).show();
                    }
                } else {
                    Toast.makeText(BarcodeScanActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PickupCancelResponse> call, Throwable t) {
                mProgressDialog1.dismiss();
                Toast.makeText(BarcodeScanActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        //  mProgressDialog1.dismiss();
        Toast.makeText(BarcodeScanActivity.this, throwable.toString(), Toast.LENGTH_SHORT).show();
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            openCameraIntent(CAMERA_REQUEST);
        }
    }

    private void openCameraIntent(int CAMERA_REQUEST) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile();
                Log.e("Image_File", photoFile.getAbsolutePath());
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imagePath;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                "imagename",
                ".jpg",
                storageDir
        );

        imagePath = image.getAbsolutePath();
        return image;
    }

    public void closeDocketClient() {
        TextView scanNextDocket, closeClient, mCloseLater, mGenerateConnote;
        closeDocketClientDialog.show();
        closeDocketClientDialog.setCancelable(false);
        closeClient = closeDocketClientDialog.findViewById(R.id.mCloseClient);
        scanNextDocket = closeDocketClientDialog.findViewById(R.id.mScanNextDocket);
        mCloseLater = closeDocketClientDialog.findViewById(R.id.mCloseLater);
        mGenerateConnote = closeDocketClientDialog.findViewById(R.id.mGenerateConnote);

        if (connoteAuomation.equalsIgnoreCase("1")) {
            mGenerateConnote.setVisibility(View.VISIBLE);

        } else {
            mGenerateConnote.setVisibility(View.GONE);

        }

        mGenerateConnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BarcodeScanActivity.this, ConnoteGenerationActivity.class));
            }
        });
        mCloseLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDocketClientDialog.dismiss();
                Intent pickList = new Intent(BarcodeScanActivity.this, PickupActivity.class);
                pickList.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(pickList);
                finish();
            }
        });

        scanNextDocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("close_client", "false");
                closeDocket();
            }
        });

        closeClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clientFinish("false");
            }
        });
    }

    public void closeClient() {
        CloseClientRequest closeClientRequest = new CloseClientRequest();
        closeClientRequest.setAction("closeclient_new");
        closeClientRequest.setClientCode(mCriticalogSharedPreferences.getData("client_code"));
        closeClientRequest.setBookingId(mCriticalogSharedPreferences.getData("bookingno"));
        closeClientRequest.setPrs(mCriticalogSharedPreferences.getData("prs"));
        closeClientRequest.setLatitude(String.valueOf(latitude));
        closeClientRequest.setLongitude(String.valueOf(longitude));
        closeClientRequest.setPickupRegularId(mCriticalogSharedPreferences.getData("pickup_regular_id"));
        closeClientRequest.setPrsId(mCriticalogSharedPreferences.getData("prs_id"));
        closeClientRequest.setUniqueId(mCriticalogSharedPreferences.getData("unique_id"));
        closeClientRequest.setUserId(userId);

        mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this,
                new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
        mPresenter.closeClientRequest(token, closeClientRequest);
    }

    public void userRiderLog() {
        String androidOS = Build.VERSION.RELEASE;
        String versionName = BuildConfig.VERSION_NAME;
        RiderLogRequest riderLogRequest = new RiderLogRequest();
        riderLogRequest.setAction("rider_log");
        riderLogRequest.setAppVersion(versionName);
        riderLogRequest.setBookingNo(mCriticalogSharedPreferences.getData("bookingno"));
        riderLogRequest.setRoundId(mCriticalogSharedPreferences.getData("round"));
        riderLogRequest.setClientId(mCriticalogSharedPreferences.getData("client_code"));
        riderLogRequest.setDeviceId(mCriticalogSharedPreferences.getData("deviceID"));
        riderLogRequest.setDocketNo(docket);
        riderLogRequest.setHubId(mCriticalogSharedPreferences.getData("XHUBID"));
        riderLogRequest.setLat(String.valueOf(latitude));
        riderLogRequest.setLng(String.valueOf(longitude));
        riderLogRequest.setMarkerId("3");
        riderLogRequest.setmLastKm("");
        riderLogRequest.setmStartStopKm("");
        riderLogRequest.setUserId(userId);
        riderLogRequest.setType("2");
        riderLogRequest.setActionNumber(3);
        riderLogRequest.setOsVersion(androidOS);

        dbHelper.insertLastLatLong(userId, String.valueOf(latitude), String.valueOf(longitude));
        Call<RiderLogResponse> riderLogResponseCall = mRestServices.userRiderLog(riderLogRequest);
        riderLogResponseCall.enqueue(new Callback<RiderLogResponse>() {
            @Override
            public void onResponse(Call<RiderLogResponse> call, Response<RiderLogResponse> response) {

            }

            @Override
            public void onFailure(Call<RiderLogResponse> call, Throwable t) {

            }
        });
    }

    public void getQCRules(String itemId, String fromWhere) {
        adapterRefreshFlag = fromWhere;
        QCRulesRequest qcRulesRequest = new QCRulesRequest();
        qcRulesRequest.setAction("get_qc_rules");
        qcRulesRequest.setUserId(userId);
        qcRulesRequest.setQcItemId(itemId);
        qcRulesRequest.setLatitude(String.valueOf(latitude));
        qcRulesRequest.setLongitude(String.valueOf(longitude));
        mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
        mPresenter.requestQCrules(qcRulesRequest);
    }

    public void getQCItemList() {
        mCriticalogSharedPreferences.saveData("docket_check_list", docket);
        ItemListRequest itemListRequest = new ItemListRequest();
        itemListRequest.setAction("get_qc_items");
        itemListRequest.setUserId(userId);
        itemListRequest.setDocketNo(docket);
        itemListRequest.setLatitude(String.valueOf(latitude));
        itemListRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new DocketValidationPresenterImpl(BarcodeScanActivity.this, new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl(), new GetDocketValidationInteractorImpl());
        mPresenter.qcItemListRequest(itemListRequest);

    }

    public void getQCCheckSubmit() {
        QcRulesCheckRequest qcRulesCheckRequest = new QcRulesCheckRequest();
        qcRulesCheckRequest.setAction("qc_rule_check");
        qcRulesCheckRequest.setUserId(userId);
        qcRulesCheckRequest.setQcItemId(itemIdToSend);
        qcRulesCheckRequest.setLatitude(String.valueOf(latitude));
        qcRulesCheckRequest.setLongitude(String.valueOf(longitude));

        mPresenter.qcCheck(qcRulesCheckRequest);

    }

    public void getQCUpdate(String qcId, String qcType, String qcBolean, String image, String customText) {

        QcRulesUpdateRequest qcRulesUpdateRequest = new QcRulesUpdateRequest();
        qcRulesUpdateRequest.setAction("qc_rule_update");
        qcRulesUpdateRequest.setUserId(userId);
        qcRulesUpdateRequest.setQcItemId(itemIdToSend);
        qcRulesUpdateRequest.setQcId(qcId);
        qcRulesUpdateRequest.setQcType(qcType);
        qcRulesUpdateRequest.setQcBoolReponse(qcBolean);
        qcRulesUpdateRequest.setImage1(image);
        qcRulesUpdateRequest.setCheckStatus("");
        qcRulesUpdateRequest.setQcUploadedImg("");
        qcRulesUpdateRequest.setQcCustomText("");
        qcRulesUpdateRequest.setQcInputReposne(customText);
        qcRulesUpdateRequest.setDocketNo(docket);
        qcRulesUpdateRequest.setLatitude(String.valueOf(latitude));
        qcRulesUpdateRequest.setLongitude(String.valueOf(longitude));


        mPresenter.qcUpdateRequest(qcRulesUpdateRequest);

    }

    public void QCNdcUpdate(String passFail) {
        QCPassFailRequest qcPassFailRequest = new QCPassFailRequest();
        qcPassFailRequest.setAction("qc_pass_fail_update");
        qcPassFailRequest.setUserId(userId);
        qcPassFailRequest.setQcItemId(itemIdToSend);
        qcPassFailRequest.setQcPassFail(passFail);
        qcPassFailRequest.setUnique_id(mCriticalogSharedPreferences.getData("unique_id"));
        qcPassFailRequest.setDocket_no(docket);

        mPresenter.qcNDCUpdate(qcPassFailRequest);

    }

    public void closeDocket() {
        CloseDocketRequest closeDocketRequest = new CloseDocketRequest();
        closeDocketRequest.setAction("closedocket");
        closeDocketRequest.setDocket(docket);
        closeDocketRequest.setClientcode(mCriticalogSharedPreferences.getData("client_code"));
        closeDocketRequest.setUserId(userId);
        closeDocketRequest.setPrs(mCriticalogSharedPreferences.getData("prs"));
        closeDocketRequest.setLatitude(String.valueOf(latitude));
        closeDocketRequest.setLongitude(String.valueOf(longitude));
        mPresenter.closeDocketRequest(token, closeDocketRequest);
    }

    private void clientFinish(String directClose) {
        LayoutInflater finishDialog = LayoutInflater.from(BarcodeScanActivity.this);
        final View finishDialogView = finishDialog.inflate(R.layout.finish_dialog, null);
        DialogFinish = new AlertDialog.Builder(BarcodeScanActivity.this).create();
        DialogFinish.setView(finishDialogView);
        DialogFinish.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DialogFinish.show();
        DialogFinish.setCanceledOnTouchOutside(false);

        finishDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCriticalogSharedPreferences.saveData("close_client", "true");
                DialogFinish.dismiss();
                if (directClose.equalsIgnoreCase("true")) {
                    closeClient();
                } else {
                    closeDocket();
                }
                getLocation();
            }
        });
        finishDialogView.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFinish.dismiss();
            }
        });


    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    showLocationDialog("");
                }
                break;
        }
    }

    public void showLocationDialog(String close) {

        androidx.appcompat.app.AlertDialog.Builder builder;
        builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        //Uncomment the below code to Set the message and title from the strings.xml file
        builder.setMessage(R.string.update).setTitle(R.string.upload_payment_proof);

        //Setting message manually and performing action on button click
        builder.setMessage("Location is off...Go to app settings and Allow Location!!")
                .setCancelable(false)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finish();
                        // redirectStore("https://play.google.com/store/apps/details?id=com.criticalog.ecritica");
                    }
                })
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                        finish();
                    }
                });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Location Permission!!");
        alert.show();

    }

    @Override
    public void onclickRulesList(String yesNo, int position) {

        if (yesNo.equalsIgnoreCase("scrollto")) {
            mRvRules.scrollToPosition(position + 1);
        }

        if (yesNo.equalsIgnoreCase("scrollback")) {
            mRvRules.scrollToPosition(position - 1);
        }
        if (position <= prsRules.size()) {
            if (yesNo.equalsIgnoreCase("yes")) {
                prsRulesList.set(position, new ChecklistResponse(prsRules.get(position).getRule(), "1", prsRules.get(position).getImage()));
                prsRulesListPreview.set(position, new ChecklistScan(prsRules.get(position).getRule().toString(), "1"));

                rulesHorizontalAdapter.notifyDataSetChanged();

            } else if (yesNo.equalsIgnoreCase("no")) {

                prsRulesList.set(position, new ChecklistResponse(prsRules.get(position).getRule(), "0", prsRules.get(position).getImage()));
                prsRulesListPreview.set(position, new ChecklistScan(prsRules.get(position).getRule().toString(), "0"));

                rulesHorizontalAdapter.notifyDataSetChanged();

            }
        }
        for (int i = 0; i < prsRulesList.size(); i++) {
            if (prsRulesList.get(i).getFlag().equalsIgnoreCase("")) {
                mOkRule.setVisibility(View.GONE);
                mSelectYesNoAlert.setVisibility(View.VISIBLE);
                break;
            } else {
                mSelectYesNoAlert.setVisibility(View.GONE);
                mOkRule.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void adapterClick(int pos, String itemId) {
        itemIdToSend = itemId;
        mCriticalogSharedPreferences.saveData("item_id", itemId);
        startActivity(new Intent(this, TataCliqQcActivity.class));
    }

    @Override
    public void qcAdpaterClick(String qcId, String qcType, String qcBolean, String image, String customText) {
        qcIdToSned = qcId;
        if (image.equalsIgnoreCase("upload")) {
            openCameraIntent(CAMERA_REQUEST_QC);
        } else {
            getQCUpdate(qcId, qcType, qcBolean, image, customText);
        }
    }

    public String getDistanceFromGoogleAPI(final double lat1, final double lon1, final double lat2, final double lon2) {
        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);
        String distnace = "0";

        float distanceInKm = 0;
        String nextBillionHitStatus = mCriticalogSharedPreferences.getData("distance_api_status");
        if (nextBillionHitStatus.equalsIgnoreCase("1")) {

            distnace = mCriticalogSharedPreferences.getDistanceFromGoogelBillionAPI(lat1, lon1, lat2, lon2);

            distanceInKm = convertMeterToKilometer(Float.valueOf(distnace));
        } else {
            distanceInKm = 0;
        }

        return String.valueOf(distanceInKm);

    }

    public static float convertMeterToKilometer(float meter) {
        return (float) (meter * 0.001);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (dialogQcItems != null) {
            if (dialogQcItems.isShowing()) {
                dialogQcItems.dismiss();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (dialogQcItems != null) {
            if (dialogQcItems.isShowing()) {
                dialogQcItems.dismiss();

            }
        }
    }

    @Override
    public void clickOnDcocketList(String docket, String origin, String destination) {
        mDocketEntry.setText(docket);
        if (docketListDialog != null) {
            docketListDialog.dismiss();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (docketWithOriginDestAdapter != null) {
            docketWithOriginDestAdapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public void openPickupAdapter(int position, String message) {

        positionClicked = position;
        openPickupText = message;
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        openCameraIntent(CAMERA_REQUEST_MULT);
    }
}
