package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QcRulesUpdateRequest {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;
    @SerializedName("qc_item_id")
    @Expose
    private String qcItemId;
    @SerializedName("qc_id")
    @Expose
    private String qcId;
    @SerializedName("qc_type")
    @Expose
    private String qcType;
    @SerializedName("qc_uploaded_img")
    @Expose
    private String qcUploadedImg;
    @SerializedName("qc_custom_text")
    @Expose
    private String qcCustomText;
    @SerializedName("qc_input_response")
    @Expose
    private String qcInputReposne;
    @SerializedName("qc_bool_response")
    @Expose
    private String qcBoolReponse;
    @SerializedName("check_status")
    @Expose
    private String checkStatus;
    @SerializedName("image_1")
    @Expose
    private String image1;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getQcItemId() {
        return qcItemId;
    }

    public void setQcItemId(String qcItemId) {
        this.qcItemId = qcItemId;
    }

    public String getQcId() {
        return qcId;
    }

    public void setQcId(String qcId) {
        this.qcId = qcId;
    }

    public String getQcType() {
        return qcType;
    }

    public void setQcType(String qcType) {
        this.qcType = qcType;
    }

    public String getQcUploadedImg() {
        return qcUploadedImg;
    }

    public void setQcUploadedImg(String qcUploadedImg) {
        this.qcUploadedImg = qcUploadedImg;
    }

    public String getQcCustomText() {
        return qcCustomText;
    }

    public void setQcCustomText(String qcCustomText) {
        this.qcCustomText = qcCustomText;
    }

    public String getQcInputReposne() {
        return qcInputReposne;
    }

    public void setQcInputReposne(String qcInputReposne) {
        this.qcInputReposne = qcInputReposne;
    }

    public String getQcBoolReponse() {
        return qcBoolReponse;
    }

    public void setQcBoolReponse(String qcBoolReponse) {
        this.qcBoolReponse = qcBoolReponse;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }
}
