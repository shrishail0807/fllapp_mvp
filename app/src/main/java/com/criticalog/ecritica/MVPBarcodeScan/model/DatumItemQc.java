package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumItemQc {
    @SerializedName("qc_item_id")
    @Expose
    private String qcItemId;
    @SerializedName("length")
    @Expose
    private String length;
    @SerializedName("breadth")
    @Expose
    private String breadth;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_desc")
    @Expose
    private String productDesc;
    @SerializedName("invoice_number")
    @Expose
    private String invoiceNumber;

    @SerializedName("qc_status")
    @Expose
    private String qc_status;

    @SerializedName("qc_pass_fail")
    @Expose
    private String qc_pass_fail;

    public String getQc_pass_fail() {
        return qc_pass_fail;
    }

    public void setQc_pass_fail(String qc_pass_fail) {
        this.qc_pass_fail = qc_pass_fail;
    }

    public String getQc_status() {
        return qc_status;
    }

    public void setQc_status(String qc_status) {
        this.qc_status = qc_status;
    }

    public String getQcItemId() {
        return qcItemId;
    }

    public void setQcItemId(String qcItemId) {
        this.qcItemId = qcItemId;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}
