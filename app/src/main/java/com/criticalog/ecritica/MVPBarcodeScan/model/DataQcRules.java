package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DataQcRules implements Serializable {
    @SerializedName("qc_rules")
    @Expose
    private List<QcRule> qcRules = null;

    public List<QcRule> getQcRules() {
        return qcRules;
    }

    public void setQcRules(List<QcRule> qcRules) {
        this.qcRules = qcRules;
    }
}
