package com.criticalog.ecritica.MVPBarcodeScan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocketListWithOriginDestDatum {
    @SerializedName("docket_num")
    @Expose
    private String docketNum;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;

    public String getDocketNum() {
        return docketNum;
    }

    public void setDocketNum(String docketNum) {
        this.docketNum = docketNum;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
