package com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NDCReasonsRequest {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("negative_at")
    @Expose
    private String negativeAt;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getNegativeAt() {
        return negativeAt;
    }

    public void setNegativeAt(String negativeAt) {
        this.negativeAt = negativeAt;
    }
}
