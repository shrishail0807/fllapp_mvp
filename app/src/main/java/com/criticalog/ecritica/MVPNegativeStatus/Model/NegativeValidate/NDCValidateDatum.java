package com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NDCValidateDatum {
    @SerializedName("customer_code")
    @Expose
    private String customerCode;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("consigne_address")
    @Expose
    private String consigneAddress;
    @SerializedName("radio_type")
    @Expose
    private String radioType;
    @SerializedName("docket_no")
    @Expose
    private String docketNo;

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getConsigneAddress() {
        return consigneAddress;
    }

    public void setConsigneAddress(String consigneAddress) {
        this.consigneAddress = consigneAddress;
    }

    public String getRadioType() {
        return radioType;
    }

    public void setRadioType(String radioType) {
        this.radioType = radioType;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }
}
