package com.criticalog.ecritica.MVPNegativeStatus;

import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateResponse;

public class NegativeStatusPresenterImpl implements NegativeStatusContract.presenter, NegativeStatusContract.OriginHubIntractor.OnFinishedListener, NegativeStatusContract.NDCReasonsIntractor.OnFinishedListener, NegativeStatusContract.NegativeValidateIntractor.OnFinishedListener, NegativeStatusContract.NegativeSubmitIntractor.OnFinishedListener {

    private NegativeStatusContract.MainView mainView;
    private NegativeStatusContract.OriginHubIntractor originHubIntractor;
    private NegativeStatusContract.NDCReasonsIntractor ndcReasonsIntractor;
    private NegativeStatusContract.NegativeValidateIntractor negativeValidateIntractor;
    private NegativeStatusContract.NegativeSubmitIntractor negativeSubmitIntractor;

    public NegativeStatusPresenterImpl(NegativeStatusContract.MainView mainView,
                                       NegativeStatusContract.OriginHubIntractor originHubIntractor,
                                       NegativeStatusContract.NDCReasonsIntractor ndcReasonsIntractor,
                                       NegativeStatusContract.NegativeValidateIntractor negativeValidateIntractor,
                                       NegativeStatusContract.NegativeSubmitIntractor negativeSubmitIntractor) {
        this.mainView = mainView;
        this.originHubIntractor = originHubIntractor;
        this.ndcReasonsIntractor = ndcReasonsIntractor;
        this.negativeValidateIntractor=negativeValidateIntractor;
        this.negativeSubmitIntractor=negativeSubmitIntractor;
    }

    @Override
    public void onDestroy() {
        if (mainView != null) {
            mainView = null;
        }
    }

    @Override
    public void negativeOriginHubs(String token, NegativeOriginHubRequest negativeOriginHubRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        originHubIntractor.getOriginHubRequest(this, token, negativeOriginHubRequest);
    }

    @Override
    public void reasonsNDCCancel(String token, NDCReasonsRequest ndcReasonsRequest) {
        if (mainView != null) {
            mainView.showProgress();
        }
        ndcReasonsIntractor.ndcReasonsRequest(this,token,ndcReasonsRequest);
    }

    @Override
    public void negativeValidateCheck(String token, NegativeValidateRequest negativeValidateRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }
        negativeValidateIntractor.negativeValidateRequest(this,token,negativeValidateRequest);
    }

    @Override
    public void negativeSubmit(String token, NegativeSubmitRequest negativeSubmitRequest) {
        if(mainView!=null)
        {
            mainView.showProgress();
        }

        negativeSubmitIntractor.negativeSubmitRequest(this,token,negativeSubmitRequest);

    }

    @Override
    public void onFinished(NegativeOriginHubResponse originHubResponse) {

        if (mainView != null) {
            mainView.setOriginHubResponse(originHubResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(NDCReasonsResponse ndcReasonsResponse) {

        if(mainView!=null)
        {
            mainView.ndcReasonsSetToVoews(ndcReasonsResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(NegativeValidateResponse negativeValidateResponse) {
        if(mainView!=null)
        {
            mainView.negativeValidateResponseSetToViews(negativeValidateResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(NegativeSubmitResponse negativeSubmitResponse) {
        if(mainView!=null)
        {
            mainView.setNegativeSubmitResponseToViews(negativeSubmitResponse);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainView != null) {
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
