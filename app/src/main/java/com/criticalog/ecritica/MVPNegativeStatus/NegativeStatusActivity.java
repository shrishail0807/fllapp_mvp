package com.criticalog.ecritica.MVPNegativeStatus;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.criticalog.ecritica.Adapters.Negative_Entry_adapter;
import com.criticalog.ecritica.Interface.Icancelinteface;
import com.criticalog.ecritica.MVPNegativeStatus.Model.HubDetail;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NegativeDetail;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateResponse;
import com.criticalog.ecritica.R;
import com.criticalog.ecritica.Utils.CriticalogSharedPreferences;
import com.criticalog.ecritica.Utils.StaticUtils;
import com.criticalog.ecritica.model.BoxOrConDetails;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class NegativeStatusActivity extends AppCompatActivity implements NegativeStatusContract.MainView, View.OnClickListener, Icancelinteface {
    private NegativeStatusContract.presenter mPresenter;
    private CriticalogSharedPreferences mCriticalogSharedPreferences;
    private String userId, token;
    private SimpleArcDialog mProgressBar;
    private Spinner hubcode;
    private EditText negativestatus;
    private List<NegativeDetail> reasons = new ArrayList<>();
    List<String> reasonList = new ArrayList<>();
    private String reasonCode = "", reason = "";
    private RadioGroup mRadioGroup;
    private String selectedoption, selectedhubcode, selectednegativeat;

    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, consignmentdetails;
    LinearLayout ll, subll;
    private RadioGroup rg;
    RadioButton rb;
    // HorizontalScrollView hsv ;
    private RecyclerView recyclerView;
    private Spinner negativeat;
    // EditText editText;
    private Negative_Entry_adapter adapter;
    ArrayList list = new ArrayList();
    ArrayList docketdetails = new ArrayList<>();
    private EditText editText;
    private String radiotype = "";
    private String action = "";
    private String hubCode = "";
    private Button check, submit;
    private LinearLayout includelayout;
    private TextView backbutton;
    private String nagativeAtString;
    private String nagativeAtStringGetNegStatus;
    private FusedLocationProviderClient mfusedLocationproviderClient;
    private LocationRequest locationRequest;
    private double latitude, longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_negative_entry);

        mfusedLocationproviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        getLocation();
        //Loader
        mProgressBar = new SimpleArcDialog(this);
        mProgressBar.setConfiguration(new ArcConfiguration(this));
        mProgressBar.setCancelable(false);

        hubcode = findViewById(R.id.hubcode);
        negativestatus = findViewById(R.id.negativestatus);
        mRadioGroup = findViewById(R.id.mRadioGroup);

        negativeat = findViewById(R.id.negativeat);
        negativestatus = findViewById(R.id.negativestatus);
        hubcode = findViewById(R.id.hubcode);
        editText = findViewById(R.id.editTextText);

        check = findViewById(R.id.check);
        submit = findViewById(R.id.button3);
        includelayout = findViewById(R.id.llProgressBar);
        backbutton = findViewById(R.id.textView5);


        rg = findViewById(R.id.mRadioGroup);

        recyclerView = findViewById(R.id.rv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(decoration);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        tv1 = new TextView(this);
        tv2 = new TextView(this);
        tv3 = new TextView(this);
        tv4 = new TextView(this);
        tv5 = new TextView(this);
        tv6 = new TextView(this);
        tv7 = new TextView(this);
        tv8 = new TextView(this);
        tv9 = new TextView(this);

        tv1.setPadding(5, 5, 5, 5);
        tv2.setPadding(5, 5, 5, 5);
        tv3.setPadding(5, 5, 5, 5);
        tv4.setPadding(5, 5, 5, 5);
        tv5.setPadding(5, 5, 5, 5);
        tv6.setPadding(5, 5, 5, 5);
        tv7.setPadding(5, 5, 5, 5);

        consignmentdetails = findViewById(R.id.textView8);

        consignmentdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedoption == null) {
                    Toast.makeText(NegativeStatusActivity.this, "Please select one option", Toast.LENGTH_SHORT).show();
                } else {
                    addrow();
                }

            }
        });

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        negativestatus.setOnClickListener(this);
        check.setOnClickListener(this);
        negativestatus.setFocusable(false);

        mCriticalogSharedPreferences = CriticalogSharedPreferences.getInstance(this);

        StaticUtils.BASE_URL_DYNAMIC = mCriticalogSharedPreferences.getData("base_url_dynamic");

        userId = mCriticalogSharedPreferences.getData("userId");
        token = mCriticalogSharedPreferences.getData("token");

        StaticUtils.TOKEN = mCriticalogSharedPreferences.getData("token");
        StaticUtils.billionDistanceAPIKey=mCriticalogSharedPreferences.getData("distance_api_key");

        NegativeOriginHubRequest negativeOriginHubRequest = new NegativeOriginHubRequest();
        negativeOriginHubRequest.setAction("negative_orghub");
        negativeOriginHubRequest.setUserId(userId);
        negativeOriginHubRequest.setLatitude(String.valueOf(latitude));
        negativeOriginHubRequest.setLongitude(String.valueOf(longitude));

        mPresenter = new NegativeStatusPresenterImpl(NegativeStatusActivity.this, new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl());
        mPresenter.negativeOriginHubs(token, negativeOriginHubRequest);

        negativeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.getItemAtPosition(position);

                Log.d("selected ", "negative at" + parent.getItemAtPosition(position));

                //api call fetch response and show in negative status spinner
                selectednegativeat = parent.getItemAtPosition(position).toString();

                if (selectednegativeat.equalsIgnoreCase("ORIGIN")) {
                    nagativeAtString = "ORIGIN";
                    nagativeAtStringGetNegStatus = "O";
                } else if (selectednegativeat.equalsIgnoreCase("LINEHAUL")) {
                    nagativeAtString = "LINEHAUL";
                    nagativeAtStringGetNegStatus = "L";
                } else if (selectednegativeat.equalsIgnoreCase("DESTINATION")) {
                    nagativeAtString = "DESTINATION";
                    nagativeAtStringGetNegStatus = "D";
                } else if (selectednegativeat.equalsIgnoreCase("DAMAGE")) {
                    nagativeAtString = "DAMAGE";
                    nagativeAtStringGetNegStatus = "DG";
                } else if (selectednegativeat.equalsIgnoreCase("OTHERS")) {
                    nagativeAtString = "OTHERS";
                    nagativeAtStringGetNegStatus = "OH";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectednegativeat = parent.getItemAtPosition(0).toString();
            }
        });
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.boxwise:

                        selectedoption = "Box Number";
                        radiotype = "1";
                        action = "negative_submit_box";
                        radiooptiom(selectedoption);

                        break;
                    case R.id.conwise:

                        selectedoption = "Con Wise";
                        radiotype = "2";
                        action = "negative_submit_connote";
                        radiooptiom(selectedoption);
                        break;
                    case R.id.mbagwise:

                        selectedoption = "MBag Wise";
                        radiotype = "3";
                        action = "negative_submit_bag";
                        radiooptiom(selectedoption);
                        break;
                    case R.id.mawbwise:
                        selectedoption = "MAWB Wise";
                        radiotype = "4";
                        action = "negative_submit_mawb";
                        radiooptiom(selectedoption);
                        break;
                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectednegativeat.equalsIgnoreCase("select")) {
                    Toast.makeText(NegativeStatusActivity.this, "Please Select Negative At ", Toast.LENGTH_LONG).show();
                } else if (docketdetails.size() > 0) {
                    includelayout.setVisibility(View.VISIBLE);

                    NegativeSubmitRequest negativeSubmitRequest = new NegativeSubmitRequest();
                    negativeSubmitRequest.setAction(action);
                    negativeSubmitRequest.setNegativeat(nagativeAtString);
                    negativeSubmitRequest.setNegative(reasonCode);
                    negativeSubmitRequest.setNegativestuatus(reason);
                    negativeSubmitRequest.setRadioType(radiotype);
                    negativeSubmitRequest.setUserId(userId);
                    negativeSubmitRequest.setDocketlist(docketdetails);
                    negativeSubmitRequest.setHubcode(hubCode);
                    negativeSubmitRequest.setLatitude(String.valueOf(latitude));
                    negativeSubmitRequest.setLongitude(String.valueOf(longitude));

                    mPresenter = new NegativeStatusPresenterImpl(NegativeStatusActivity.this, new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl());
                    mPresenter.negativeSubmit(token, negativeSubmitRequest);

                } else {
                    Toast.makeText(NegativeStatusActivity.this, "Please enter valid input first", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(NegativeStatusActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(NegativeStatusActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NegativeStatusActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    StaticUtils.LOCATION_REQUEST);

        } else {
            mfusedLocationproviderClient.getLastLocation().addOnSuccessListener(NegativeStatusActivity.this, location -> {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("BarcodeScan getloc :" + "lat" + latitude, "long" + longitude);
                } else {
                    //  mfusedLocationproviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    void addrow() {
        Log.d("inside", "addrow");
        editText.setText("");
        editText.setVisibility(View.VISIBLE);
        check.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.dismiss();
    }

    @Override
    public void setOriginHubResponse(NegativeOriginHubResponse originHubResponse) {
        if (originHubResponse.getStatus() == 200) {
            List<HubDetail> originHubDetails = originHubResponse.getData();
            ArrayList hublist = new ArrayList();
            ArrayList hubIdsList = new ArrayList();
            for (int i = 0; i < originHubDetails.size(); i++) {


                hublist.add(originHubDetails.get(i).getHubCode());
                hubIdsList.add(originHubDetails.get(i).getHubId());

            }

            ArrayAdapter<HubDetail> adapter = new ArrayAdapter<HubDetail>(this,
                    android.R.layout.simple_spinner_item, hublist);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            hubcode.setAdapter(adapter);
            hubcode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String user = parent.getSelectedItem().toString();
                    hubCode = hubIdsList.get(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            Toast.makeText(NegativeStatusActivity.this, originHubResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void ndcReasonsSetToVoews(NDCReasonsResponse response) {

        if (response.getStatus() == 200) {
            reasons = response.getData();
            for (int i = 0; i < reasons.size(); i++) {

                reasonList.add(reasons.get(i).getNegativeDesc());
            }
            showCancelDialog();
        } else {

        }
    }

    @Override
    public void negativeValidateResponseSetToViews(NegativeValidateResponse negativeValidateResponse) {
        BoxOrConDetails boxOrConDetails;
        if (negativeValidateResponse.getStatus() == 200) {
            editText.setVisibility(View.GONE);
            check.setVisibility(View.GONE);
            String docket = negativeValidateResponse.getData().get(0).getDocketNo();
            String radiotype = negativeValidateResponse.getData().get(0).getRadioType();
            if (radiotype.equalsIgnoreCase("1") || radiotype.equalsIgnoreCase("2")) {

                boxOrConDetails = new BoxOrConDetails(docket, negativeValidateResponse.getData().get(0).getCustomerCode(), negativeValidateResponse.getData().get(0).getCustomerName(), negativeValidateResponse.getData().get(0).getConsigneAddress());
                list.add(0, boxOrConDetails);
                docketdetails.add(boxOrConDetails.getNumber());

            } else if (radiotype.equalsIgnoreCase("3") || radiotype.equalsIgnoreCase("4")) {
                boxOrConDetails = new BoxOrConDetails(docket, negativeValidateResponse.getData().get(0).getCustomerCode(), negativeValidateResponse.getData().get(0).getCustomerName(), negativeValidateResponse.getData().get(0).getConsigneAddress());
                list.add(0, boxOrConDetails);
                docketdetails.add(boxOrConDetails.getNumber());
            }

            adapter = new Negative_Entry_adapter(list, this, selectedoption, this);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        } else {
            Toast.makeText(this, negativeValidateResponse.getMessage(), Toast.LENGTH_LONG).show();
        }

        if (docketdetails.size() > 0) {
            submit.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void setNegativeSubmitResponseToViews(NegativeSubmitResponse negativeSubmitResponse) {

        if (negativeSubmitResponse.getStatus() == 200) {
            Toasty.success(this, negativeSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
            finish();
            // startActivity(new Intent(this, HomeActivity.class));
        } else {
            Toasty.error(this, negativeSubmitResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(NegativeStatusActivity.this, R.string.re_try, Toast.LENGTH_SHORT).show();
    }

    private void showCancelDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(NegativeStatusActivity.this)
                .title(R.string.select_cancel_reason)
                .titleGravity(GravityEnum.CENTER)
                .items(reasonList)
                .positiveText(R.string.yes)
                .positiveColor(Color.BLUE)
                .negativeText("NO")
                .choiceWidgetColor(ColorStateList.valueOf(Color.BLUE))
                .negativeColor(Color.BLUE)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                        reasonCode = reasons.get(which).getNegativeCode();
                        reason = reasons.get(which).getNegativeDesc();
                        negativestatus.setText(reasons.get(which).getNegativeDesc());
                        return true;
                    }
                })
                .show();
    }

    void radiooptiom(String option) {
        editText.setVisibility(View.VISIBLE);
        check.setVisibility(View.VISIBLE);

        Log.d("inside ", "radio option" + option);

        if (option.equalsIgnoreCase("Box Number")) {
            mCriticalogSharedPreferences.saveData("neg_status_of", "Box Number");
            selectedoption = "Box Number";
            editText.setHint("Enter Box No.");

            tv1.setText("Box Number");
            tv2.setText("Cust Code");
            tv3.setText("Cust Name");
            tv4.setText("Consignee Add");

        } else if (option.equalsIgnoreCase("Con Wise")) {
            mCriticalogSharedPreferences.saveData("neg_status_of", "Con Number");

            selectedoption = "Con Number";
            editText.setHint("Enter Con No.");

            tv1.setText("Connote Number");
            tv2.setText("Cust Code");
            tv3.setText("Cust Name");
            tv4.setText("Consignee Add");

        } else if (option.equalsIgnoreCase("MBag Wise")) {
            editText.setHint("Enter MBag No.");
            mCriticalogSharedPreferences.saveData("neg_status_of", "MBag Number");

            selectedoption = "MBag Number";
            tv1.setText("MBag Number");
            tv2.setText("From City");
            tv3.setText("To City");
            tv4.setText("Connote No");

        } else if (option.equalsIgnoreCase("MAWB Wise")) {
            editText.setHint("Enter MAWB No.");
            mCriticalogSharedPreferences.saveData("neg_status_of", "MAWB Number");
            selectedoption = "MAWB Number";
            tv1.setText("MAWB Numberr");
            tv2.setText("Origin");
            tv3.setText("Destination");
            tv4.setText("Connote No");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.negativestatus:
                reasonList.clear();
                reasons.clear();
                if (nagativeAtStringGetNegStatus != null) {
                    NDCReasonsRequest ndcReasonsRequest = new NDCReasonsRequest();
                    ndcReasonsRequest.setAction("negative_negstatus");
                    ndcReasonsRequest.setNegativeAt(nagativeAtStringGetNegStatus);
                    ndcReasonsRequest.setUserId(userId);
                    ndcReasonsRequest.setLatitude(String.valueOf(latitude));
                    ndcReasonsRequest.setLatitude(String.valueOf(longitude));
                    mPresenter = new NegativeStatusPresenterImpl(this, new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl());
                    mPresenter.reasonsNDCCancel(token, ndcReasonsRequest);
                } else {
                    Toasty.warning(this, "Please select Neagative At!!", Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.check:

                String input = editText.getText().toString().trim();

                if (input.equalsIgnoreCase("") || input.matches("") || input.length() == 0) {
                    Toast.makeText(NegativeStatusActivity.this, "Please enter valid " + selectedoption, Toast.LENGTH_LONG).show();
                } else {
                    if (docketdetails.contains(input)) {
                        Toast.makeText(NegativeStatusActivity.this, "Already added", Toast.LENGTH_LONG).show();

                    } else {
                        NegativeValidateRequest negativeValidateRequest = new NegativeValidateRequest();
                        negativeValidateRequest.setAction("negative_validate");
                        negativeValidateRequest.setUserId(userId);
                        negativeValidateRequest.setRadioType(radiotype);
                        negativeValidateRequest.setDocketNo1(input);
                        negativeValidateRequest.setLatitude(String.valueOf(latitude));
                        negativeValidateRequest.setLongitude(String.valueOf(longitude));


                        mPresenter = new NegativeStatusPresenterImpl(this, new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl(), new NegaticeStatusInteractorImpl());
                        mPresenter.negativeValidateCheck(token, negativeValidateRequest);

                    }
                }


                break;
        }
    }

    @Override
    public void removerow(String docket) {

    }
}
