package com.criticalog.ecritica.MVPNegativeStatus;

import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateResponse;
import com.criticalog.ecritica.Rest.RestClient;
import com.criticalog.ecritica.Rest.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NegaticeStatusInteractorImpl implements NegativeStatusContract.OriginHubIntractor, NegativeStatusContract.NDCReasonsIntractor, NegativeStatusContract.NegativeValidateIntractor, NegativeStatusContract.NegativeSubmitIntractor {
    RestServices services = RestClient.getRetrofitInstance().create(RestServices.class);

    @Override
    public void getOriginHubRequest(NegativeStatusContract.OriginHubIntractor.OnFinishedListener onFinishedListener, String token, NegativeOriginHubRequest negativeOriginHubRequest) {
        Call<NegativeOriginHubResponse> negativeOriginHubResponseCall = services.negativeOriginHub(negativeOriginHubRequest);

        negativeOriginHubResponseCall.enqueue(new Callback<NegativeOriginHubResponse>() {
            @Override
            public void onResponse(Call<NegativeOriginHubResponse> call, Response<NegativeOriginHubResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<NegativeOriginHubResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void ndcReasonsRequest(NegativeStatusContract.NDCReasonsIntractor.OnFinishedListener onFinishedListener, String token, NDCReasonsRequest ndcReasonsRequest) {
        Call<NDCReasonsResponse> ndcReasonsResponseCall = services.getNDCReasons(ndcReasonsRequest);
        ndcReasonsResponseCall.enqueue(new Callback<NDCReasonsResponse>() {
            @Override
            public void onResponse(Call<NDCReasonsResponse> call, Response<NDCReasonsResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<NDCReasonsResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void negativeValidateRequest(NegativeStatusContract.NegativeValidateIntractor.OnFinishedListener onFinishedListener, String token, NegativeValidateRequest negativeValidateRequest) {

        Call<NegativeValidateResponse> negativeOriginHubResponseCall = services.negativeValidate(negativeValidateRequest);
        negativeOriginHubResponseCall.enqueue(new Callback<NegativeValidateResponse>() {
            @Override
            public void onResponse(Call<NegativeValidateResponse> call, Response<NegativeValidateResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<NegativeValidateResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void negativeSubmitRequest(NegativeStatusContract.NegativeSubmitIntractor.OnFinishedListener onFinishedListener, String token, NegativeSubmitRequest negativeSubmitRequest) {
        Call<NegativeSubmitResponse> negativeSubmitResponseCall = services.negativeSubmit(negativeSubmitRequest);
        negativeSubmitResponseCall.enqueue(new Callback<NegativeSubmitResponse>() {
            @Override
            public void onResponse(Call<NegativeSubmitResponse> call, Response<NegativeSubmitResponse> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<NegativeSubmitResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
