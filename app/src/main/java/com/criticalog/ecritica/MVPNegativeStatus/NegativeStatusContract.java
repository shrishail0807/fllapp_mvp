package com.criticalog.ecritica.MVPNegativeStatus;

import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NDCREason.NDCReasonsResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeOriginHubResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeSubmitResponse;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateRequest;
import com.criticalog.ecritica.MVPNegativeStatus.Model.NegativeValidate.NegativeValidateResponse;

public interface NegativeStatusContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     */
    interface presenter {

        void onDestroy();

        void negativeOriginHubs(String token, NegativeOriginHubRequest negativeOriginHubRequest);

        void reasonsNDCCancel(String token, NDCReasonsRequest ndcReasonsRequest);

        void negativeValidateCheck(String token, NegativeValidateRequest negativeValidateRequest);

        void negativeSubmit(String token, NegativeSubmitRequest negativeSubmitRequest);

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setOriginHubResponse(NegativeOriginHubResponse originHubResponse);

        void ndcReasonsSetToVoews(NDCReasonsResponse ndcReasonsResponse);

        void negativeValidateResponseSetToViews(NegativeValidateResponse negativeValidateResponse);

        void setNegativeSubmitResponseToViews(NegativeSubmitResponse negativeSubmitResponse);

        void onResponseFailure(Throwable throwable);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface OriginHubIntractor {

        interface OnFinishedListener {
            void onFinished(NegativeOriginHubResponse originHubResponse);

            void onFailure(Throwable t);
        }

        void getOriginHubRequest(NegativeStatusContract.OriginHubIntractor.OnFinishedListener onFinishedListener, String token, NegativeOriginHubRequest negativeOriginHubRequest);
    }

    interface NDCReasonsIntractor {

        interface OnFinishedListener {
            void onFinished(NDCReasonsResponse ndcReasonsResponse);

            void onFailure(Throwable t);
        }

        void ndcReasonsRequest(NegativeStatusContract.NDCReasonsIntractor.OnFinishedListener onFinishedListener, String token, NDCReasonsRequest ndcReasonsRequest);
    }

    interface NegativeValidateIntractor {

        interface OnFinishedListener {
            void onFinished(NegativeValidateResponse negativeValidateResponse);

            void onFailure(Throwable t);
        }

        void negativeValidateRequest(NegativeStatusContract.NegativeValidateIntractor.OnFinishedListener onFinishedListener, String token, NegativeValidateRequest negativeValidateRequest);
    }

    interface NegativeSubmitIntractor {

        interface OnFinishedListener {
            void onFinished(NegativeSubmitResponse negativeSubmitResponse);

            void onFailure(Throwable t);
        }

        void negativeSubmitRequest(NegativeStatusContract.NegativeSubmitIntractor.OnFinishedListener onFinishedListener, String token, NegativeSubmitRequest negativeSubmitRequest);
    }
}
