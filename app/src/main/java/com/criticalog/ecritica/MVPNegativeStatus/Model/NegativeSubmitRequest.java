package com.criticalog.ecritica.MVPNegativeStatus.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NegativeSubmitRequest implements Serializable {
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("radio_type")
    @Expose
    private String radioType;
    @SerializedName("hubcode")
    @Expose
    private String hubcode;
    @SerializedName("negativeat")
    @Expose
    private String negativeat;
    @SerializedName("negative")
    @Expose
    private String negative;
    @SerializedName("negativestuatus")
    @Expose
    private String negativestuatus;

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("docketlist")
    @Expose
    private List<Integer> docketlist = null;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRadioType() {
        return radioType;
    }

    public void setRadioType(String radioType) {
        this.radioType = radioType;
    }

    public String getHubcode() {
        return hubcode;
    }

    public void setHubcode(String hubcode) {
        this.hubcode = hubcode;
    }

    public String getNegativeat() {
        return negativeat;
    }

    public void setNegativeat(String negativeat) {
        this.negativeat = negativeat;
    }

    public String getNegative() {
        return negative;
    }

    public void setNegative(String negative) {
        this.negative = negative;
    }

    public String getNegativestuatus() {
        return negativestuatus;
    }

    public void setNegativestuatus(String negativestuatus) {
        this.negativestuatus = negativestuatus;
    }

    public List<Integer> getDocketlist() {
        return docketlist;
    }

    public void setDocketlist(List<Integer> docketlist) {
        this.docketlist = docketlist;
    }
}
